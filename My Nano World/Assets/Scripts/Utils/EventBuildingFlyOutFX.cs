using DG.Tweening;
using UnityEngine;

public class EventBuildingFlyOutFX : MonoBehaviour
{
    [SerializeField] private RectTransform _buildingTransform;
    [SerializeField] private GameObject _vfxGameObject;
    [SerializeField] private Transform _target;

    [SerializeField] private float _vfxTime = 2f;
    [SerializeField] private float _flyOutTime = 1f;

    private float _vfxPunchScale = 0.25f;
    private int _vfxVibrato = 3;
    private float _vfxElasticity = 1f;
    private float _flyOutAmplitude = 1f;
    private float _flyOutPeriod = 1f;

    private void Start()
    {
        EnableParticlesVFX();
        
        DoPushScaleFX();
        DoMoveToShopFX();

        Destroy(gameObject, _vfxTime + _flyOutTime);
    }

    private void EnableParticlesVFX()
    {
        if (!_vfxGameObject) return;

        _vfxGameObject.SetActive(true);
    }

    private void DoPushScaleFX()
    {
        _buildingTransform.DOPunchScale(Vector3.one * _vfxPunchScale, _vfxTime, _vfxVibrato, _vfxElasticity);
    }

    private void DoMoveToShopFX()
    {
        var targetPosition = _target.position;

        _buildingTransform.DOMove(targetPosition, _flyOutTime).SetDelay(_vfxTime).SetEase(Ease.InElastic, _flyOutAmplitude, _flyOutPeriod);
        _buildingTransform.DOScale(Vector3.zero, _flyOutTime).SetDelay(_vfxTime).SetEase(Ease.InElastic, _flyOutAmplitude, _flyOutPeriod);
    }
}
