﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace NanoReality.Utils
{
    public class DragEventRepeater : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler
    {
        [SerializeField]
        private List<UIBehaviour> _linkedScrollRects;

        public void OnBeginDrag(PointerEventData eventData)
        {
            for (int i = 0; i < _linkedScrollRects.Count; i++)
            {
                var scrollRect = _linkedScrollRects[i] as IBeginDragHandler;
                if (scrollRect != null)
                {
                    scrollRect.OnBeginDrag(eventData);
                }                
            }
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            for (int i = 0; i < _linkedScrollRects.Count; i++)
            {
                var scrollRect = _linkedScrollRects[i] as IEndDragHandler;
                if (scrollRect != null)
                {
                    scrollRect.OnEndDrag(eventData);
                }                
            }
        }

        public void OnDrag(PointerEventData eventData)
        {
            for (int i = 0; i < _linkedScrollRects.Count; i++)
            {
                var scrollRect = _linkedScrollRects[i] as IDragHandler;
                if (scrollRect != null)
                {
                    scrollRect.OnDrag(eventData);
                }                
            }
        }
    }
}
