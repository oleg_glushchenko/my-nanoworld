using UnityEditor;
using UnityEditor.UI;

namespace NanoReality.Core.UI
{
    [CustomEditor(typeof(TextButton))]
    public class TextButtonInspector : ButtonEditor
    {
        private SerializedProperty _buttonText;

        protected override void OnEnable()
        {
            base.OnEnable();
            _buttonText = serializedObject.FindProperty("_buttonText");
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            _buttonText.Dispose();
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            serializedObject.Update();
            
            EditorGUI.BeginChangeCheck();
            EditorGUILayout.PropertyField(_buttonText);
            EditorGUI.EndChangeCheck();
            
            serializedObject.ApplyModifiedProperties();
        }
    }
}