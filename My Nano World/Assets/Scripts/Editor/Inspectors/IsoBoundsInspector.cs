﻿
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof (IsoBounds))]
[CanEditMultipleObjects]
public class IsoBoundsInspector : UnityEditor.Editor
{

    public override void OnInspectorGUI()
    {
        if (GUILayout.Button("Set iso position"))
        {
            for (int i = 0; i < targets.Length; i++)
            {
                var bounds = (IsoBounds) targets[i];
                var mapObjectView = bounds.GetComponent<MapObjectView>();
                var pos = mapObjectView.transform.localPosition;
                pos.x = Mathf.Floor(pos.x);
                pos.z = Mathf.Floor(pos.z);
                mapObjectView.transform.localPosition = pos;
                mapObjectView.GridPosition = new Vector2Int((int)pos.x, (int)pos.z);
            }
        }
    }
}
