﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class UseSpriteMeshChecker
{
    private static GameObject _selection;
    private static List<GameObject> _listOfChildren;
    private static List<Image> _images;

    [MenuItem("GameObject/CheckUseSpriteMesh", false, 0)]
    private static void Execute()
    {
        _selection = Selection.activeGameObject;
        _listOfChildren = new List<GameObject>();
        GetChildRecursive(_selection);
        _images = new List<Image>();

        foreach (var child in _listOfChildren)
        {
            Debug.Log(child.name);
            var image = child.GetComponent<Image>();
            if (image != null)
            {
                _images.Add(image);
            }
        }
        _images.Add(_selection.GetComponent<Image>());

        foreach (var image in _images)
        {
            var currentFlag = typeof(Image).GetField("m_UseSpriteMesh", System.Reflection.BindingFlags.NonPublic |System.Reflection.BindingFlags.Instance);

            currentFlag.SetValue(image, true);
        }
    }

    private static void GetChildRecursive(GameObject obj)
    {
        if (obj == null)
            return;

        foreach (RectTransform child in obj.transform)
        {
            if (null == child)
                continue;
            //child.gameobject contains the current child you can do whatever you want like add it to an array
            _listOfChildren.Add(child.gameObject);
            GetChildRecursive(child.gameObject);
        }
    }
}