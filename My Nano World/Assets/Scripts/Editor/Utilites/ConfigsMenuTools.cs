using UnityEditor;

namespace Assets.Editor.Utilities
{
    public class ConfigsMenuTools
    {
        [MenuItem("Tools/NanoReality/UI/UI Links Asset", priority = 0)]
        public static void SelectUILinksAsset()
        {
            Selection.activeObject = AssetDatabase.LoadMainAssetAtPath("Assets/Resources/UIPrefabLinks.asset");
        }
    }
}