﻿using System.IO;
using System.Linq;
using MobileConsole;
using UnityEditor;
using UnityEngine;

namespace Editor.Utilites.SfxSoundTypesGenerator
{
    public static class SfxEnumGenerator
    {
	    private static readonly string SoundPath = $"{Application.dataPath}\\Resources\\Sound\\SFX";
	    private static readonly string DestinationPath = $"{Application.dataPath}\\Scripts\\Nanolib\\Core\\Services\\Sound\\";
	    private const string OutputName = "SfxSoundTypes.cs";
	    
	    private static readonly string[] Extensions = { ".mp3", ".wav" };
	    private static readonly char[] Separators = {'_', ' '};
	    
	    [MenuItem("Tools/NanoReality/SFX/Generate SoundSFX Enum")]
        private static void Generate()
        {
	        var dirInfo = new DirectoryInfo(SoundPath);

			var audioFilesInfo = dirInfo.GetFiles()
				.Where(file => Extensions.Contains(file.Extension.ToLower()));

            var fileNames = audioFilesInfo.Select(fileInfo => 
	            Path.GetFileNameWithoutExtension(fileInfo.Name)).ToArray();

            var soundNum = fileNames.Length;

            for (var i = 0; i < soundNum; i++)
            {
	            fileNames[i] = UpgradeName(fileNames[i]);
            }

            var templateInstance = new SfxSoundTypesTemplate() {SfxNames = fileNames};
            var pageContent = templateInstance.TransformText();
			File.WriteAllText(DestinationPath + OutputName, pageContent);

			Debug.LogFormat("[Sound] Generated new Enumerator for SFX sounds. {0} sounds found.", soundNum);
		}
        
        private static string UpgradeName(string input)
        {
	        var tempArray = input.Split(Separators);
	        
	        for (var i = 0; i < tempArray.Length; i++)
	        {
		        var wordLength = tempArray[i].Length;
		        if (wordLength > 0)
					tempArray[i] = tempArray[i][0].ToUpper() + (wordLength > 1 ? tempArray[i].Substring(1) : string.Empty);
	        }

	        return string.Join(string.Empty, tempArray);
        }
    }
}