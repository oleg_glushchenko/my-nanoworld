using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace Editor
{
    public class SwitchToNonMaskableImage : EditorWindow
    {
        [MenuItem("Window/ExtraLayout/Open")]
        static void Init()
        {
            // Get existing open window or if none, make a new one:
            SwitchToNonMaskableImage window = (SwitchToNonMaskableImage) GetWindow(typeof(SwitchToNonMaskableImage));
            window.Show();
        }

        private static GameObject tempGO;

        [MenuItem("Window/Image optimization/SwitchToNonMaskable &n")]
        public static void SwitchToNonMaskable()
        {
            tempGO = new GameObject("temp");
            SwitchToNonMaskable(Selection.activeGameObject);
            StartSwitch(Selection.activeGameObject.transform);
        }

        private static void StartSwitch(Transform transform)
        {
            Transform temp;
            for (int i = 0; i < transform.childCount; i++)
            {
                temp = transform.GetChild(i);
                StartSwitch(temp);
            }

            Debug.Log($"Switching... {transform.name} ");
            SwitchToNonMaskable(transform.gameObject);
            Debug.Log($"{transform.name} : Switched");
        }

        private static void SwitchToNonMaskable(GameObject go)
        {
            var image = go.GetComponent<Image>();
            if (image != null)
            {
                var newImg = CopyComponent(image, tempGO.gameObject);
                string name = image.name;
                DestroyImmediate(go.GetComponent<Image>());
                var nonMaskableImage = go.AddComponent<NonMaskableImage>();
                nonMaskableImage.name = name;
                nonMaskableImage.material = newImg.material;
                nonMaskableImage.sprite = newImg.sprite;
                nonMaskableImage.type = newImg.type;
                nonMaskableImage.fillAmount = newImg.fillAmount;
                nonMaskableImage.fillCenter = newImg.material;
                nonMaskableImage.fillClockwise = newImg.fillClockwise;
                nonMaskableImage.fillMethod = newImg.fillMethod;
                nonMaskableImage.fillOrigin = newImg.fillOrigin;
                nonMaskableImage.overrideSprite = newImg.overrideSprite;
                nonMaskableImage.preserveAspect = newImg.preserveAspect;
                nonMaskableImage.useSpriteMesh = newImg.useSpriteMesh;
                nonMaskableImage.pixelsPerUnitMultiplier = newImg.pixelsPerUnitMultiplier;
                nonMaskableImage.alphaHitTestMinimumThreshold = newImg.alphaHitTestMinimumThreshold;
                nonMaskableImage.color = newImg.color;
                nonMaskableImage.tag = newImg.tag;
                nonMaskableImage.hideFlags = newImg.hideFlags;
                nonMaskableImage.raycastTarget = newImg.raycastTarget;
                nonMaskableImage.runInEditMode = newImg.runInEditMode;
                nonMaskableImage.useGUILayout = newImg.useGUILayout;
            }
        }
        
         static T CopyComponent<T>(T original, GameObject destination) where T : Component
        {
            System.Type type = original.GetType();
            var dst = destination.GetComponent(type) as T;
            if (!dst) dst = destination.AddComponent(type) as T;
            var fields = type.GetFields();
            foreach (var field in fields)
            {
                if (field.IsStatic) continue;
                field.SetValue(dst, field.GetValue(original));
            }
            var props = type.GetProperties();
            foreach (var prop in props)
            {
                if (!prop.CanWrite || !prop.CanWrite || prop.Name == "name") continue;
                prop.SetValue(dst, prop.GetValue(original, null), null);
            }

            return dst;
        }
    }
}