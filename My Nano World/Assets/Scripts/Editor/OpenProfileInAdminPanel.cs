﻿using NanoReality.Engine.Settings.BuildSettings;
using UnityEngine;
using UnityEditor;

public class OpenProfileInAdminPanel : ScriptableObject
{
    [MenuItem("Tools/Open profile in admin panel")]
    static void JustDoIt()
    {
        var path = AssetDatabase.GUIDToAssetPath(AssetDatabase.FindAssets("t:NetworkSettingsObject")[0]);
        var networkSettingsObject = AssetDatabase.LoadAssetAtPath<NetworkSettings>(path);
        var serverAddress = networkSettingsObject.ServerAddress.Replace("http://", "http://admin.").Replace("api.", "");
        var playerID = PlayerPrefs.GetInt("PlayerID");

        if (playerID <= 0)
        {
            Debug.Log("PlayerID = " + playerID + ", это значит, что после последнего сброса профиля игрок ещё не авторизовался. Что бы этот пункт работал - необходимо запустить игру хотя-бы раз");
        }
        else
        {
            Application.OpenURL(string.Format("{0}/player/view?id={1}", serverAddress, playerID));
        }
    }
}