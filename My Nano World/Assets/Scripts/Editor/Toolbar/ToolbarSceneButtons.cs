using System.IO;
using UnityEditor;
using UnityEngine;
using System;
using System.Reflection;
using UnityEngine.UIElements;

namespace NanoReality.Editor
{
    [InitializeOnLoad]
    public class ToolbarSceneButtons
    {
       private  static ScriptableObject m_currentToolbar;
       private static Type m_toolbarType = typeof(EditorGUI).Assembly.GetType("UnityEditor.Toolbar");
       private static VisualElement parent;
       private static int lastInstanceID;

        static ToolbarSceneButtons()
        {
            m_currentToolbar = null;
            EditorApplication.update -= OnUpdate;
            EditorApplication.update += OnUpdate;
        }

        private static  void OnUpdate()
        {
            if (m_currentToolbar == null)
            {
                var toolbars = Resources.FindObjectsOfTypeAll(m_toolbarType);
                m_currentToolbar = toolbars.Length > 0 ? (ScriptableObject)toolbars[0] : null;
            }

            // If the windows layour reloaded, we need to re create our GUI
            if (m_currentToolbar != null && parent != null && m_currentToolbar.GetInstanceID() != lastInstanceID)
            {
                parent.RemoveFromHierarchy();
                parent = null;
                lastInstanceID = m_currentToolbar.GetInstanceID();
            }

            if (m_currentToolbar != null && parent == null)
            {
                var root = m_currentToolbar.GetType().GetField("m_Root", BindingFlags.NonPublic | BindingFlags.Instance);
                if (root != null)
                {
                    var rawRoot = root.GetValue(m_currentToolbar);
                    if (rawRoot != null)
                    {
                        var mRoot = rawRoot as VisualElement;
                        var toolbarZoneLeftAlign = mRoot.Q("ToolbarZoneLeftAlign");
                        if (parent != null)
                        {
                            parent.RemoveFromHierarchy();
                        }

                        parent = null;
                        parent = new VisualElement()
                        {
                            style ={
                                flexGrow = 1,
                                flexDirection = FlexDirection.Row,
                            }
                        };

                        OnAttachToToolbar(parent);
                        toolbarZoneLeftAlign.Add(parent);
                    }
                }
            }
        }

        private static void OnAttachToToolbar(VisualElement parent)
        {
            parent.Add(CreateTextureButton("Settings", BuildSettings));
            parent.Add(CreateTextButton("LOG", LoggerSettings));
            parent.Add(CreateTextButton("NET", NetworkSettings));
            parent.Add(CreateTextButton("DEL", DeleteLocalData));
        }

        private static void DeleteLocalData()
        {
            if (!EditorApplication.isPlaying)
            {
                if (!EditorUtility.DisplayDialog("Game Prefs", "Are you sure to delete all player data?", "OK", "CANCEL"))
                {
                    return;
                }
                PlayerPrefs.DeleteAll();
                var aPath = Path.Combine(Application.persistentDataPath, "AppData.bin");
                if (!File.Exists(aPath)) return;
                File.Delete(aPath);
            }
        }

        private static void BuildSettings()
        {
                Selection.activeObject = AssetDatabase.LoadMainAssetAtPath("Assets/Resources/BuildSettings.asset");
        }     

        private static void LoggerSettings()
        {
                Selection.activeObject = AssetDatabase.LoadMainAssetAtPath("Assets/Resources/LoggingSettings.asset");
        }

        private static void NetworkSettings()
        {
                Selection.activeObject = AssetDatabase.LoadMainAssetAtPath("Assets/Resources/NetworkSettings.asset");
        }

        private static VisualElement CreateTextureButton(string icon, Action onClick)
        {
            Button buttonVE = new Button(onClick);
            FitChildrenStyle(buttonVE);
            VisualElement iconVE = new VisualElement();
            iconVE.AddToClassList("unity-editor-toolbar-element__icon");
            iconVE.style.backgroundImage = Background.FromTexture2D(EditorGUIUtility.FindTexture(icon));
            buttonVE.Add(iconVE);
            return buttonVE;
        }

        private static VisualElement CreateTextButton(string label, Action onClick)
        {
            Button buttonVE = new Button(onClick);
            FitChildrenStyle(buttonVE);
            VisualElement iconVE = new VisualElement();
            iconVE.AddToClassList("unity-editor-toolbar-element__icon");
            iconVE.Add(new TextField(label));
            iconVE.style.width = 35;
            buttonVE.Add(iconVE);
            return buttonVE;
        }

        private static void FitChildrenStyle(VisualElement element)
        {
            element.AddToClassList("unity-toolbar-button");
            element.AddToClassList("unity-editor-toolbar-element");
            element.RemoveFromClassList("unity-button");
            element.style.marginRight = 0;
            element.style.marginLeft = 0;
        }
    }
}