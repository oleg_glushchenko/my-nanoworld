﻿using System.Collections.Generic;
using System.Linq;
using NanoLib.Core.Logging;
using UnityEditor;
using UnityEngine;

namespace NanoReality.Editor.Defines
{
    public class DefinesController : EditorWindow
    {
        private Dictionary<string, bool> _defines;

        [MenuItem("Tools/NanoReality/Defines Controller &#d")]
        private static void OpenWindow()
        {
            GetWindow<DefinesController>("Defines Controller");
        }

        private void OnGUI()
        {
            if(_defines == null)
                _defines = GetAvailableDefines();
            
            foreach (var define in new Dictionary<string, bool>(_defines))
                _defines[define.Key] = GUILayout.Toggle(define.Value, define.Key);

            if (GUILayout.Button("Save"))
            {
                UpdateActiveDefines();
                "Defines successfully updated!".Log(LoggingChannel.Editor);
            }

            if (GUILayout.Button("Easy development"))
            {
                SetDefines(DefinesSetup.Development);
                Close();
                
                "Enjoy :3".Log(LoggingChannel.Editor);
            }
        }
        
        private static Dictionary<string, bool> GetAvailableDefines()
        {
            var activeDefines = PlayerSettings.GetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup)
                .Split(';');

            return DefinesSetup.Defines.ToDictionary(define => define, define => activeDefines.Contains(define));
        }

        private void UpdateActiveDefines()
        {
            var defines = _defines.Where(defineSetting => defineSetting.Value)
                .Select(define => define.Key);

            SetDefines(defines);
        }

        public static void SetDefines(IEnumerable<string> defines)
        {
            PlayerSettings.SetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup, 
                string.Join(";", defines));
        }
    }
}