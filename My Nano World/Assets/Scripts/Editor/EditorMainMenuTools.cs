﻿using NanoLib.SoundManager;
using NanoReality.Engine.Settings.BuildSettings;
using UnityEditor;
using UnityEngine;

public class EditorMainMenuTools
{
    [MenuItem("Tools/NanoReality/Clear Cache", false, int.MaxValue)]
    private static void ClearAssetBundlesCache()
    {
        if (EditorUtility.DisplayDialog("Asset Bundle Caching", 
            "Are you sure you want to clean asset bundle cache?", "Ok", "Cancel"))
        {
            Caching.ClearCache();
        }
    }
    
    [MenuItem("Tools/Open Build Settings")]
    private static void OpenBuildSettings()
    {
        Selection.activeObject = AssetDatabase.LoadAssetAtPath<BuildSettings>("Assets/Resources/BuildSettings.asset");
    }
    
    [MenuItem("Tools/NanoReality/Settings/Sound Settings")]
    private static void OpenSoundSettings()
    {
        Selection.activeObject = AssetDatabase.LoadAssetAtPath<SoundSettings>("Assets/Resources/Sound/SoundSettings.asset");
    }
}