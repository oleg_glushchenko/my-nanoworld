using UnityEditor;
using UnityEngine;
using UnityEngine.U2D;
using UnityEditor.U2D;

public class CustomPadding
{
    [MenuItem("Assets/SpriteAtlas Set Padding 16")]
    public static void SpriteAtlasCustomPadding()
    {
        Object[] objs = Selection.objects;

        foreach (var obj in objs)
        {
            SpriteAtlas sa = obj as SpriteAtlas;
            if (sa)
            {
                var ps = sa.GetPackingSettings();
                ps.padding = 16;
                sa.SetPackingSettings(ps);
            }
        }
        AssetDatabase.SaveAssets();
    }
}