using NanoLib.Core.Services.Sound;
using Assets.NanoLib.UI.Core.Signals;
using Assets.NanoLib.UtilitesEngine;
using Assets.Scripts.Engine.Analytics.Model.api;
using Assets.Scripts.Engine.UI.Views.Popups;
using NanoLib.Core.Timers;
using NanoReality.Game.Advertisements;
using NanoReality.Game.OrderDesk;
using NanoReality.GameLogic.Common.Controllers.Signals;
using NanoReality.GameLogic.Configs;
using NanoReality.GameLogic.OrderDesk;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.ServerCommunication.Models;
using NanoReality.GameLogic.Services;
using NanoReality.UI.Components;

namespace NanoReality.Game.UI
{
    public sealed class MetroPanelContext
    {
        [Inject] public IMetroOrderDeskService MetroOrderDeskService { get; set; }
        [Inject] public ITimerManager jTimerManager { get; set; }
        [Inject] public IAdsService jAdsService { get; set; }
        [Inject] public IPlayerProfile PlayerProfile { get; set; }
        [Inject] public BuyProductsActionSignal BuyProductsActionSignal { get; set; }
        [Inject] public AddRewardItemSignal jAddRewardItemSignal { get;set; }
        [Inject] public ServerTimeTickSignal jServerTimeTickSignal { get; set; }
        [Inject] public IGameConfigData jGameConfigData { get; set; }
        [Inject] public IPopupManager PopupManager { get; set; }
        [Inject] public ISkipIntervalsData SkipIntervalsData { get; set; }
        [Inject] public BalancePanelModel BalancePanelModel { get; set; }
        [Inject] public INanoAnalytics jAnalytics { get; set; }
        [Inject] public MetroOrderDeskUpdatedSignal jMetroOrderDeskUpdatedSignal { get; set; }
        [Inject] public ISoundManager jSoundManager { get; set; }
        [Inject] public SignalWarehouseProductsAmountChanged jSignalWarehouseProductsAmountChanged { get; set; }

        public IPlayerResources PlayerResources => PlayerProfile.PlayerResources;

        public MetroConfig MetroConfig => jGameConfigData.MetroConfig;
    }
}
