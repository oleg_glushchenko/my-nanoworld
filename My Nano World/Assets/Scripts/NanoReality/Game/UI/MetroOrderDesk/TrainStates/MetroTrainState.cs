using System;
using System.Linq;
using NanoLib.Core.StateMachine;
using NanoReality.GameLogic.OrderDesk;
using UniRx;

namespace NanoReality.Game.UI
{
    public abstract class MetroTrainState : State<MetroTrainStateArgs>
    {
        protected MetroTrainView TrainView => Args.TrainView;
        protected MetroOrderTrainModel TrainModel => Args.TrainView.Model;
        protected MetroPanelContext Context => Args.Context;
        protected IMetroOrderDeskService MetroService => Context.MetroOrderDeskService;

        private int _waitingDoorsCount;
        private bool _isDoorOpenAnimation;
        private Action _doorAnimationComplete;
        private readonly CompositeDisposable _doorAnimationsDisposable = new CompositeDisposable();

        protected MetroTrainState(IStateMachine parent, StateArgs args) : base(parent, args)
        {
        }
        
        public override void OnStateEnter()
        {
            
        }

        public override void OnStateExit()
        {
            _doorAnimationsDisposable.Clear();
        }

        protected void PlayTrainDoorsAnimation(bool isOpen, Func<MetroSlotItemModel, bool> predicate, Action onComplete = null)
        {
            _isDoorOpenAnimation = isOpen;
            _waitingDoorsCount = 0;
            _doorAnimationComplete = onComplete;
            
            foreach (MetroSlotItemModel slotModel in TrainModel.Slots.Value.Where(predicate))
            {
                slotModel.OpenDoorCommand.Execute(isOpen);
                slotModel.OpenDoorStatus.Subscribe(OnCloseAllDoorsAnimationFinished).AddTo(_doorAnimationsDisposable);
                _waitingDoorsCount++;
            }
        }

        private void OnCloseAllDoorsAnimationFinished(bool isOpen)
        {
            if (isOpen != _isDoorOpenAnimation)
            {
                return;
            }
            
            _waitingDoorsCount--;
            bool allDoorsClosed = _waitingDoorsCount == 0;
            
            if (allDoorsClosed)
            {
                _doorAnimationsDisposable.Clear();
                _doorAnimationComplete?.Invoke();
                _doorAnimationComplete = null;
            }
        }
        
        protected int CalculateSkipWaitingPrice(int leftTime)
        {
            return Context.SkipIntervalsData.GetCurrencyForTime(leftTime);
        }
    }
}