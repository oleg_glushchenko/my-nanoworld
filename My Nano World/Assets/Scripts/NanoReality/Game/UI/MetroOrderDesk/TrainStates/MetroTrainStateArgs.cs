using NanoLib.Core.StateMachine;

namespace NanoReality.Game.UI
{
    public class MetroTrainStateArgs : StateArgs
    {
        public readonly MetroTrainView TrainView;
        public readonly MetroPanelContext Context;

        public MetroTrainStateArgs(MetroTrainView trainView, MetroPanelContext context)
        {
            TrainView = trainView;
            Context = context;
        }
    }
}