using System;
using System.Collections.Generic;
using System.Linq;
using Assets.NanoLib.Utilities;
using NanoLib.Core.Services.Sound;
using NanoLib.Core.StateMachine;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using NanoReality.GameLogic.OrderDesk;
using UniRx;
using UnityEngine;

namespace NanoReality.Game.UI
{
    public sealed class IdleMetroTrainState : MetroTrainState
    {
        /// <summary>
        /// Keeps item models that waiting for server response.S
        /// </summary>
        private readonly Dictionary<int, IDisposable> _loadProductsAnimations 
            = new Dictionary<int, IDisposable>();

        private readonly CompositeDisposable _compositeDisposable = new CompositeDisposable();

        public IdleMetroTrainState(IStateMachine parent, StateArgs args) : base(parent, args)
        {
        }

        public override void OnStateEnter()
        {
            //TODO: return it to fully activate the shuffle logic again
            // TrainModel.ShuffleVisible.SetValueAndForceNotify(true);
            // TrainModel.ShuffleInteractable.SetValueAndForceNotify(true);
            // TrainModel.ShuffleClicked.Subscribe(OnShuffleSlotsClicked).AddTo(_compositeDisposable);
            
            // Subscribe only on not filled slots.
            
            foreach (MetroSlotItemModel slotItemModel in TrainModel.Slots.Value.Where(c => !c.IsEmpty && !c.IsFilled.Value))
            {
                MetroSlotItemModel itemModel = slotItemModel;
                slotItemModel.Clicked.Subscribe(unit => { OnSlotItemClicked(itemModel); })
                    .AddTo(_compositeDisposable);
            }
        }

        public override void OnStateExit()
        {
            foreach (KeyValuePair<int,IDisposable> keyValue in _loadProductsAnimations)
            {
                keyValue.Value?.Dispose();
            }
            
            _loadProductsAnimations.Clear();
            _compositeDisposable.Clear();
        }

        private void OnShuffleSlotsClicked(Unit unit)
        {
            Context.jSoundManager.Play(SfxSoundTypes.MadeShuffle, SoundChannel.SoundFX);
            PushTransitionEvent("Shuffle");
        }

        private void OnSlotItemClicked(MetroSlotItemModel slotModel)
        {
            var productData = slotModel.ProductData.Value;
            if (Context.PlayerResources.HasEnoughProduct(productData.ProductId, productData.Count))
            {
                Context.jSoundManager.Play(SfxSoundTypes.CrateLoaded, SoundChannel.SoundFX);
                LoadSlotProducts(slotModel);
            }
            else
            {
                Context.jSoundManager.Play(SfxSoundTypes.ClickButton, SoundChannel.SoundFX);
                BuySlotProducts(slotModel);
            }
        }

        private void LoadSlotProducts(MetroSlotItemModel slotItemModel)
        {
            var loadSlotProductsAsyncSubject = new AsyncSubject<Unit>();
            loadSlotProductsAsyncSubject.OnNext(Unit.Default);
            var closeDoorAminationAsyncSubject = new AsyncSubject<Unit>();
            closeDoorAminationAsyncSubject.OnNext(Unit.Default);
            
            int slotId = slotItemModel.Id;
            bool isLastNotLoadedSlot = TrainModel.IsLastSlot(slotId);
            var networkTrain =
                Context.MetroOrderDeskService.MetroOrderDesk.OrderTrains.FirstOrDefault(t => t.Id == TrainModel.Id);
            var networkSlot = networkTrain?.OrderSlots.FirstOrDefault(t => t.Id == slotId);
            
            if (networkSlot != null)
            {
                networkSlot.IsFilled = true;
            }

            Context.MetroOrderDeskService.LoadSlotProducts(slotId, (_) =>loadSlotProductsAsyncSubject.OnCompleted());

            SetSlotsInteractable(false);
            TrainModel.ShuffleInteractable.SetValueAndForceNotify(false);

            if (isLastNotLoadedSlot)
            {
                slotItemModel.IsFilled.Value = true;
                Context.jSoundManager.Play(SfxSoundTypes.TrainSended, SoundChannel.SoundFX);
                PlayTrainDoorsAnimation(false, c => c.IsFilled.Value, () =>
                {
                    var hideWaitingSubject = TrainView.PlayHideAnimation();
                    hideWaitingSubject.Subscribe((_) => PushTransitionEvent("Reward"));
                });
            }
            else
            {
                IDisposable animationListener = slotItemModel.OpenDoorStatus.Subscribe(state =>
                {
                    if (state == false)
                    {
                        closeDoorAminationAsyncSubject.OnCompleted();
                    }
                });

                _loadProductsAnimations.Add(slotId, animationListener);
                slotItemModel.OpenDoorCommand.Execute(false);
            }
            Observable.WhenAll(loadSlotProductsAsyncSubject, closeDoorAminationAsyncSubject)
                .Subscribe((_) => OnCloseSingleDoorAnimationFinished(slotItemModel));
            Context.jSignalWarehouseProductsAmountChanged.Dispatch();
        }


        private void OnCloseSingleDoorAnimationFinished(MetroSlotItemModel model)
        {
            model.IsFilled.SetValueAndForceNotify(true);

            if (_loadProductsAnimations.ContainsKey(model.Id))
            {
                IDisposable animationListener = _loadProductsAnimations[model.Id];
                animationListener.Dispose();
                _loadProductsAnimations.Remove(model.Id);
            }

            model.OpenDoorCommand.Execute(true);
            SetFilledSlotsInteractable();
            TrainModel.ShuffleInteractable.SetValueAndForceNotify(true);
        }

        private void BuySlotProducts(MetroSlotItemModel slotModel)
        {
            slotModel.Interactable.SetValueAndForceNotify(false);
            ProductData productData = slotModel.ProductData.Value;
            var resultPrice = new Price
            {
                SoftPrice = 0,
                HardPrice = 0,
                ProductsPrice = new Dictionary<Id<Product>, int> {{productData.ProductId, productData.Count}},
                PriceType = PriceType.Resources
            };

            Context.BuyProductsActionSignal.Dispatch(resultPrice, result => OnProductsBoughtCallback(slotModel, result));
        }

        private void OnProductsBoughtCallback(MetroSlotItemModel itemModel, bool isBought)
        {
            if (isBought)
            {
                LoadSlotProducts(itemModel);
            }
            else
            {
                itemModel.Interactable.SetValueAndForceNotify(true);
            }
        }

        private void SetSlotsInteractable(bool interactable)
        {
            foreach (MetroSlotItemModel slotModel in TrainModel.Slots.Value)
            {
                slotModel.Interactable.SetValueAndForceNotify(interactable);
            }
        }

        private void SetFilledSlotsInteractable()
        {
            foreach (MetroSlotItemModel slotModel in TrainModel.Slots.Value)
            {
                slotModel.Interactable.SetValueAndForceNotify(!slotModel.IsFilled.Value);
            }
        }
    }
}