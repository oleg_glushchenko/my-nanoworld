using NanoLib.Core.StateMachine;
using NanoReality.GameLogic.OrderDesk;
using UniRx;

namespace NanoReality.Game.UI
{
    public sealed class ShippingMetroTrainState : MetroTrainState
    {
        public ShippingMetroTrainState(IStateMachine parent, StateArgs args) : base(parent, args)
        {
        }

        public override void OnStateEnter()
        {
            var updatedTrainModel = new NetworkMetroOrderTrain();
            
            var metroOrderShippingAsyncSubject = new AsyncSubject<Unit>();
            metroOrderShippingAsyncSubject.OnNext(Unit.Default);

            MetroService.ShippingOrder(TrainModel.Id, t =>
            {
                updatedTrainModel = t;
                metroOrderShippingAsyncSubject.OnCompleted();
            });

            var playHideAnimationAsyncSubject = TrainView.PlayHideAnimation();

            Observable.WhenAll(metroOrderShippingAsyncSubject, playHideAnimationAsyncSubject)
                .Subscribe((_) => ToWaitingNewTrainState(updatedTrainModel));
        }

        private void ToWaitingNewTrainState(NetworkMetroOrderTrain updatedTrainModel)
        {
            TrainModel.Id = updatedTrainModel.Id;
            TrainModel.Reward = updatedTrainModel.Reward;
            TrainModel.ShuffleSkipPrice.Value = Context.MetroConfig.ShuffleSkipPrice;
            TrainModel.WaitingStartTime = updatedTrainModel.WaitingStartTime;
            TrainModel.ShuffleStartTime = updatedTrainModel.ShuffleStartTime;
            TrainModel.WaitingEndTime = updatedTrainModel.WaitingEndTime;
            TrainModel.WaitingSkipPrice.Value = updatedTrainModel.SkipPrice;
            TrainModel.WaitingLeftTime.Value = updatedTrainModel.LeftTime;

            PushTransitionEvent("Waiting");
        }
    }
}