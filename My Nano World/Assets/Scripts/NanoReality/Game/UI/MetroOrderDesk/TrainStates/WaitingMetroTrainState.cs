using System;
using System.Linq;
using Assets.Scripts.Engine.UI.Views.Popups;
using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using NanoLib.Core.Services.Sound;
using NanoLib.Core.StateMachine;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.OrderDesk;
using UniRx;
using UnityEngine;

namespace NanoReality.Game.UI
{
    public sealed class WaitingMetroTrainState : MetroTrainState
    {
        private readonly CompositeDisposable _compositeDisposable = new CompositeDisposable();
        private Action _onFinishState;
        
        private int SkipShufflePrice
        {
            get
            {
                int leftTime = CalculateWaitingLeftTime();
                int clampedTime = Mathf.Clamp(leftTime, 0, int.MaxValue);
                return CalculateSkipWaitingPrice(clampedTime);
            }
        }
        
        public WaitingMetroTrainState(IStateMachine parent, StateArgs args) : base(parent, args)
        {
        }

        public override void OnStateEnter()
        {
            Context.jServerTimeTickSignal.AddListener(OnServerTick);
            Context.jMetroOrderDeskUpdatedSignal.AddListener(OnMetroOrderDeskUpdated);

            int leftTime = CalculateWaitingLeftTime();
            _onFinishState += OnShowAnimationComplete;
            TrainModel.WaitingLeftTime.SetValueAndForceNotify(leftTime);
            TrainModel.WaitingSkipPrice.SetValueAndForceNotify(CalculateSkipWaitingPrice(leftTime));
            TrainModel.WaitingActive.SetValueAndForceNotify(true);
            TrainModel.SkipWaitingClicked.Subscribe(OnSkipWaitingClicked).AddTo(_compositeDisposable);
            TrainModel.SkipWaitingInteractable.SetValueAndForceNotify(true);
            TrainView.SetHidden();
        }

        public override void OnStateExit()
        {
            Context.jMetroOrderDeskUpdatedSignal.RemoveListener(OnMetroOrderDeskUpdated);
            _onFinishState -= OnShowAnimationComplete;
            TrainModel.WaitingActive.SetValueAndForceNotify(false);
            TrainModel.WaitingEndTime = default;
            TrainModel.WaitingStartTime = default;
            Context.jServerTimeTickSignal.RemoveListener(OnServerTick);
            _compositeDisposable.Clear();
        }

        private void OnServerTick(long serverTime)
        {
            int leftTime = CalculateWaitingLeftTime();

            TrainModel.WaitingSkipPrice.SetValueAndForceNotify(SkipShufflePrice);
            TrainModel.WaitingLeftTime.SetValueAndForceNotify(leftTime);

            if (leftTime >= 0)
            {
                return;
            }
            
            Context.jServerTimeTickSignal.RemoveListener(OnServerTick);

            TrainModel.SkipWaitingInteractable.SetValueAndForceNotify(true);
            TrainModel.WaitingActive.SetValueAndForceNotify(false);
        }

        private void OnSkipWaitingClicked(Unit unit)
        {
            void ConfirmSkip()
            {
                Context.MetroOrderDeskService.SkipWaitingTime(TrainModel);
                Context.jServerTimeTickSignal.RemoveListener(OnServerTick);
                TrainModel.SkipWaitingInteractable.SetValueAndForceNotify(false);
                TrainModel.WaitingActive.SetValueAndForceNotify(false);
            }

            int skipShufflePrice = SkipShufflePrice;
            
            if (skipShufflePrice <= 0)
            {
                ConfirmSkip();
                return;
            }
            
            if (Context.PlayerResources.HardCurrency < skipShufflePrice)
            {
                Context.PopupManager.Show(new NotEnoughMoneyPopupSettings(PriceType.Hard, skipShufflePrice));
                return;
            }

            var popupSettings = new PurchaseByPremiumPopupSettings(skipShufflePrice);
            
            void UpdatePrice(long _)
            {
                popupSettings?.InvokePriceChangeAction(SkipShufflePrice);
            }
            
            Context.jServerTimeTickSignal.AddListener(UpdatePrice);
                        
            Context.PopupManager.Show(popupSettings, result => 
            {
                Context.jServerTimeTickSignal.RemoveListener(UpdatePrice);
                                
                if (result == PopupResult.Ok)
                {
                    ConfirmSkip();
                }
            });
        }

        private int CalculateWaitingLeftTime()
        {
            return Convert.ToInt32(TrainModel.WaitingEndTime - Context.jTimerManager.CurrentUTC);
        }

        private void OnMetroOrderDeskUpdated(NetworkMetroOrderDesk _)
        {
            AsyncSubject<Unit> showWaitingSubject = TrainView.PlayShowAnimation();
            showWaitingSubject.Subscribe((__) =>
            {
                Context.jSoundManager.Play(SfxSoundTypes.TrainSended, SoundChannel.SoundFX);
                _onFinishState?.Invoke();
            });
        }

        private void OnShowAnimationComplete()
        {
            NetworkMetroOrderTrain networkModel = MetroService.MetroOrderDesk.OrderTrains.FirstOrDefault();
            TrainModel.ParseNetworkModel(networkModel);

            PlayTrainDoorsAnimation(true, (doorModel) => !doorModel.IsEmpty,
                () => { PushTransitionEvent("Idle"); });
        }
    }
}
