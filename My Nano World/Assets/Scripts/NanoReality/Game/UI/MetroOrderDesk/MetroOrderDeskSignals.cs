using NanoReality.GameLogic.OrderDesk;
using strange.extensions.signal.impl;

namespace NanoReality.Game.UI
{
    /// <summary>
    /// Called when metro train shipped.
    /// First arg - old, shipped train id. Second arg - new train model.
    /// </summary>
    public sealed class MetroOrderShippingSignal : Signal<int, NetworkMetroOrderTrain> { }

    public sealed class MetroOrderRewardCollectedSignal : Signal<NetworkMetroOrderTrain> { }

    public sealed class MetroOrderSlotLoadedSignal : Signal<NetworkOrderSlot> { }
}