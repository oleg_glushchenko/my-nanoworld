﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NanoReality.Game.UI.SettingsPanel;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UIElements;

namespace NanoReality.Game.UI.Utility
{
    [CreateAssetMenu(fileName = "ExtraLayoutStorage", menuName = "NanoReality/UI/ExtraLayoutStorage", order = 8)]
    public class ExtraLayoutStorage : SerializedScriptableObject
    {
        [PropertyOrder(-2)]
        public bool DebugMode;
        private SettingsPanelMediator spm;
        private SettingsPanelMediator Spm
        {
            get
            {
                if (spm == null)
                {
                    spm = FindObjectOfType<SettingsPanelMediator>();
                }

                return spm;
            }
        }
        
        [Button(ButtonSizes.Large), GUIColor(0.4f, 0.8f, 1)]
        [PropertyOrder(-3)]
        private void UpdateExtraLayout()
        {
            Spm.call("OnLanguageChanged1", LocalizationMLS.Instance.Language);
        }

        [SerializeField]
        [PropertyOrder(-1)]
        [ShowIf("DebugMode")]
        private Dictionary<LayoutKey, ExtraLayoutItem> SettingsList = new Dictionary<LayoutKey, ExtraLayoutItem>();
        
        [SerializeField]
        [PropertyOrder(0)]
        [OnValueChanged("UseKey")]
        private Dictionary<LayoutKeyExt, ExtraLayoutItem> SettingsListFull = new Dictionary<LayoutKeyExt, ExtraLayoutItem>();

        [HideLabel]
        [PropertyOrder(2)]
        [ShowIf("enableSearchResult")]
        public Dictionary<LayoutKeyExt, ExtraLayoutItem> SearchResult = new Dictionary<LayoutKeyExt, ExtraLayoutItem>();
        [ValueDropdown("FindKey")]
        [OnValueChanged("UseKey")]
        [PropertyOrder(1)]
        public string filterPicker;

        private bool enableSearchResult;

        private IEnumerable FindKey()
        {
            return SettingsList.Keys.Select(x => x.PersistentParentType).Append("");
        }

        public void AddItem(LayoutKey key, ExtraLayoutItem value, GameObject gameObject)
        {
            SettingsListFull[new LayoutKeyExt{MainKey = key, PrefabLink = gameObject}] = value;
            SettingsList[key] = value;
        }

        public void DummyCopy()
        {
            SettingsList.Clear();
            foreach (var item in SettingsListFull)
            {
                SettingsList.Add(item.Key.MainKey, item.Value);
            }
        }

        public ExtraLayoutItem GetItem(LayoutKey key)
        {
            return SettingsList[key];
        }

        public void UseKey()
        {
            if (filterPicker == "")
            {
                enableSearchResult = false;
                return;
            }

            var findKey = SettingsListFull.Keys.First(x => x.MainKey.PersistentParentType == filterPicker);
            var result = SettingsListFull.Where(x => x.Key.MainKey.PersistentParentType == findKey.MainKey.PersistentParentType).ToDictionary( t => t.Key, v => v.Value);
            if (result.Any())
            {
                SearchResult = result;
                enableSearchResult = true;
                return;
            }

            enableSearchResult = false;
        }

    }

    [Serializable]
    public class ExtraLayoutItem
    {
        [SerializeField] public ExtraLayoutSettings ArabicSettings { get; set; }
        public ExtraLayoutItem(TextMeshProUGUI defaultSettings)
        {
            ArabicSettings = new ExtraLayoutSettings(defaultSettings);
        }
    }

    [Serializable]
    public struct LayoutKey
    {
        [HideLabel]
        public string PersistentParentType;
        [HideLabel]
        public string PersistentName;

    }

    [Serializable]
    public struct LayoutKeyExt
    {
        [HideLabel] 
        public LayoutKey MainKey;
        [HideLabel]
        public GameObject PrefabLink;

    }

    [Serializable]   
    [InlineProperty(LabelWidth = 100)]
    public class ExtraLayoutSettings
    {
        [SerializeField] 
        private float _fontSize;
        [SerializeField] 
        [HorizontalGroup]
        private bool _autoSize;
        [SerializeField] 
        [HorizontalGroup]
        private bool _wrapping;

        public float FontSize => _fontSize;

        public bool AutoSize => _autoSize;

        public bool Wrapping => _wrapping;

        public ExtraLayoutSettings(TextMeshProUGUI defaultSettings)
        {
            _fontSize = defaultSettings.fontSize;
            _autoSize = defaultSettings.enableAutoSizing;
            _wrapping = defaultSettings.enableWordWrapping;
        }
    }
    
    static class AccessExtensions
    {
        public static object call(this object o, string methodName, params object[] args)
        {
            var mi = o.GetType ().GetMethod (methodName, System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance );
            if (mi != null) {
                return mi.Invoke (o, args);
            }
            return null;
        }
    }
}