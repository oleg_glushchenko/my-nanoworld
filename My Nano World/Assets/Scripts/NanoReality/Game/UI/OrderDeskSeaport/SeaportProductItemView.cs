using System;
using Assets.Scripts.Engine.UI.Views.Tooltips;
using NanoReality.Core.Resources;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using strange.extensions.mediation.impl;
using strange.extensions.pool.api;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Game.UI
{
    public sealed class SeaportProductItemView : View, IPoolable
    {
        #region Layout

        [SerializeField] private Button _productButton;
        [SerializeField] private Button _buyButton;
        
        [Header("Item States")]
        [SerializeField] private GameObject _buyProductsState;
        [SerializeField] private GameObject _filledState;
        
        [Header("Counter")]
        [SerializeField] private Image _productIconImage;
        [SerializeField] private TextMeshProUGUI _productsCountText;
        [SerializeField] private Image _redCounterBackground;
        [SerializeField] private Image _greenCounterBackground;
        
        [SerializeField] private TextMeshProUGUI _buyButtonText;

        #endregion

        private bool _isInitialized;
        private SeaportProductItemModel _itemModel;
        
        public SeaportProductItemModel ItemModel => _itemModel ?? (_itemModel = new SeaportProductItemModel());
        public Button BuyButton => _buyButton;

        public event Action<SeaportProductItemView> ItemClicked;
        public event Action<SeaportProductItemView> BuyClicked;
        
        [Inject] public IResourcesManager jResourcesManager { get; set; }
        [Inject] public SignalOnNeedToShowTooltip jSignalOnNeedToShowTooltip { get; set; }
        
        protected override void Start()
        {
            base.Start();
            UpdateView();
        }
        
        private void OnEnable()
        {
            _productButton.onClick.AddListener(OnProductClicked);
            _buyButton.onClick.AddListener(OnBuyClicked);
        }

        private void OnDisable()
        {
            _productButton.onClick.RemoveListener(OnProductClicked);
            _buyButton.onClick.RemoveListener(OnBuyClicked);
        }

        [PostConstruct]
        public void PostConstruct()
        {
            _isInitialized = true;
        }

        public void UpdateView()
        {
            if (!_isInitialized)
            {
                return;
            }
            
            _productIconImage.sprite = jResourcesManager.GetProductSprite(ItemModel.ProductId);
            _productsCountText.text = $"{ItemModel.WarehouseCount}/{ItemModel.RequiredCount}";
                
            bool isProductsEnough = ItemModel.IsProductsEnough;
            _redCounterBackground.enabled = !isProductsEnough;
            _greenCounterBackground.enabled = isProductsEnough;
                
            _filledState.gameObject.SetActive(isProductsEnough);
            _buyProductsState.gameObject.SetActive(!isProductsEnough);
            _buyButtonText.text = ItemModel.BuyPrice.HardPrice.ToString();
        }
    
        private void ShowTooltip()
        {
            TooltipData _tooltipData = new TooltipData
            {
                ItemTransform = (RectTransform) _productButton.transform,
                Type = TooltipType.ProductItems,
                ProductItem = new Product {ProductTypeID = _itemModel.ProductId, HardPrice = _itemModel.BuyPrice.HardPrice},
                ProductAmount = default
            };
            
           jSignalOnNeedToShowTooltip.Dispatch(_tooltipData);
           
        }

        public void SetInteractable(bool state)
        {
            _productButton.interactable = state;
            _buyButton.interactable= state;
        }

        private void OnProductClicked()
        {
            ShowTooltip();
            ItemClicked?.Invoke(this);
        }
        
        private void OnBuyClicked()
        {
            BuyClicked?.Invoke(this);
        }
        
        #region IPoolable implementation

        public bool retain { get; }
        
        public void Restore() { }

        public void Retain()
        {
            gameObject.SetActive(true);
        }

        public void Release()
        {
            gameObject.SetActive(false);
            _productIconImage.sprite = null;
            _productsCountText.text = string.Empty;
        }

        #endregion
    }
    
    public class SeaportProductItemModel
    {
        public int ProductId;
        public int WarehouseCount;
        public int RequiredCount;
        public Price BuyPrice;

        public bool IsProductsEnough => WarehouseCount >= RequiredCount;
    }
}