using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using NanoReality.Game.Services.OrderDesk.SeaportOrderDesk;
using UniRx;

namespace NanoReality.Game.UI
{
    public class ReactiveSeaportCollection : IDisposable, IEnumerable<ISeaportOrderSlot>
    {
        private List<ISeaportOrderSlot> _collection = new List<ISeaportOrderSlot>();

        [NonSerialized] bool isDisposed = false;

        [NonSerialized] private Subject<CollectionReplaceEvent<ISeaportOrderSlot>> collectionShuffle;
        [NonSerialized] private Subject<CollectionReplaceEvent<ISeaportOrderSlot>> collectionSend;
        
        public void Add(ISeaportOrderSlot orderSlot)
        {
            _collection.Add(orderSlot);
        }

        public void Clear()
        {
            _collection.Clear();
        }

        public IObservable<CollectionReplaceEvent<ISeaportOrderSlot>> ObserveShuffle()
        {
            if(isDisposed) return Observable.Empty<CollectionReplaceEvent<ISeaportOrderSlot>>();
            return collectionShuffle ?? (collectionShuffle = new Subject<CollectionReplaceEvent<ISeaportOrderSlot>>());
        }

        public IObservable<CollectionReplaceEvent<ISeaportOrderSlot>> ObserveSend()
        {
            if(isDisposed) return Observable.Empty<CollectionReplaceEvent<ISeaportOrderSlot>>();
            return collectionSend ?? (collectionSend = new Subject<CollectionReplaceEvent<ISeaportOrderSlot>>());
        }

        public void UpdateAsShuffle(int index, ISeaportOrderSlot newOrderSlot)
        {
            ISeaportOrderSlot oldOrderSlot = _collection[index];
            _collection[index] = newOrderSlot;
            collectionShuffle?.OnNext(new CollectionReplaceEvent<ISeaportOrderSlot>(index, oldOrderSlot, newOrderSlot));
        }

        public void UpdateAsSend(int index, ISeaportOrderSlot newOrderSlot)
        {
            ISeaportOrderSlot oldOrderSlot = _collection[index];
            _collection[index] = newOrderSlot;
            collectionSend?.OnNext(new CollectionReplaceEvent<ISeaportOrderSlot>(index, oldOrderSlot, newOrderSlot));
        }

        public void Dispose()
        {
            collectionSend?.Dispose();
            collectionShuffle?.Dispose();
        }

        public int FindIndex(Predicate<ISeaportOrderSlot> match)
        {
            return _collection.FindIndex(match);
        }

        public IEnumerator<ISeaportOrderSlot> GetEnumerator()
        {
            return _collection.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}