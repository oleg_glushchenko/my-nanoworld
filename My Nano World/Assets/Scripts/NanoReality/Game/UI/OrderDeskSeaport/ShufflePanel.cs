using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Game.UI
{
    public class ShufflePanel : MonoBehaviour
    {
        [SerializeField] private Button _shuffleButton;
        [SerializeField] private TextMeshProUGUI _shuffleText;
        [SerializeField] private TextMeshProUGUI _shuffleTimer;
        [SerializeField] private TextMeshProUGUI _skipShuffleText;
        [SerializeField] private GameObject _shuffleTimerObject;

        public Button ShuffleButton => _shuffleButton;
        public TextMeshProUGUI ShuffleText => _shuffleText;
        public TextMeshProUGUI ShuffleTimer => _shuffleTimer;
        public TextMeshProUGUI SkipShuffleText => _skipShuffleText;
        public GameObject ShuffleTimerObject => _shuffleTimerObject;
    }
}