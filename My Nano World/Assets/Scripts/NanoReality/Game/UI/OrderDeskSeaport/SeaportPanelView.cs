using System.Collections.Generic;
using Assets.NanoLib.UI.Core.Views;
using NanoReality.Engine.UI.Views.Panels;
using NanoReality.Game.Services.OrderDesk.SeaportOrderDesk;
using NanoReality.GameLogic.OrderDesk;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Game.UI
{
    public sealed class SeaportPanelView : UIPanelView
    {
        [SerializeField] private SeaportOrderStatusPanelView _orderStatusPanel;
        [SerializeField] private List<SeaportOrderItemView> _orderItemsList;
        [SerializeField] private BalancePanelView _balancePanelView;
        [SerializeField] private Button _closeButton;
        [SerializeField] private GameObject _loadingOverlay;

        public SeaportOrderStatusPanelView OrderStatusPanel => _orderStatusPanel;
        public BalancePanelView BalancePanelView => _balancePanelView;
        public List<SeaportOrderItemView> OrderItemsList => _orderItemsList;

        private NetworkSeaportOrderDesk _orderDeskModel;

        private void OnEnable()
        {
            _closeButton.onClick.AddListener(Hide);
        }

        private void OnDisable()
        {
            _closeButton.onClick.RemoveListener(Hide);
        }

        public void SetOrderDeskModel(NetworkSeaportOrderDesk orderDesk)
        {
            _orderDeskModel = orderDesk;
            UpdatePanel();
        }

        private void UpdatePanel()
        {
            int counter = 0;

            for (; counter < _orderDeskModel.OrderSlots.Count; counter++)
            {
                SeaportOrderItemView orderItemView = _orderItemsList[counter];
                orderItemView.gameObject.SetActive(true);
            }

            if (counter <= (_orderItemsList.Count - 1))
            {
                for (int i = counter; i < _orderItemsList.Count; i++)
                {
                    SeaportOrderItemView orderItemView = _orderItemsList[i];
                    orderItemView.gameObject.SetActive(false);
                }
            }
        }

        public void SetActiveLoadingOverlay(bool isActive)
        {
            _loadingOverlay.gameObject.SetActive(isActive);
        }
    }
}