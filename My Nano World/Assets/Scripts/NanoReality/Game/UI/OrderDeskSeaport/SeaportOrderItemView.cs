using System;
using System.Collections.Generic;
using Assets.NanoLib.UI.Core.Signals;
using DG.Tweening;
using Engine.UI.Common;
using NanoReality.Core.Resources;
using NanoReality.Engine.UI.Components;
using NanoReality.Game.Services.OrderDesk.SeaportOrderDesk;
using NanoReality.GameLogic.Utilites;
using strange.extensions.mediation.impl;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Game.UI
{
	public sealed class SeaportOrderItemView : View
	{
		[SerializeField] private Button _selectOrderButton;

		[SerializeField] private TextMeshProUGUI _rewardCoinsText;
		[SerializeField] private GameObject _rewardCoinsObject;
		[SerializeField] private TextMeshProUGUI _rewardExperienceText;
		[SerializeField] private GameObject _rewardExperienceObject;
		[SerializeField] private TextMeshProUGUI _shuffleTimerLabel;
		[SerializeField] private TextMeshProUGUI _freeShufflesLeftText;
		[SerializeField] private PriceButton _priceButton;
		[SerializeField] private Button _skipByAdWatchButton;
		[SerializeField] private Button _skipByPremiumButton;
		[SerializeField] private Button _shuffleOrderButton;
		[SerializeField] private GridLayoutGroup _gridLayoutGroup;
		
		[Header("Common fields")]
		[SerializeField] private Image _selectedOrderImage;
		[SerializeField] private RectTransform _boxShadowImage;
		[SerializeField] private GameObject _VFX;
		[SerializeField] private GameObject _rewardPanel;
		[SerializeField] private GameObject _waitingPanel;
		[SerializeField] private List<SeaportRewardItem> _rewardItems = new List<SeaportRewardItem>();
		[SerializeField] private int _serverPosition;

		private Tween _scaleTween;
		private Tween _jumpTween;
		private Tween _cardSwapTween;
		private Tween _jumpCompensationTween;
		private Tween _scaleCompensationTween;

		public Transform SoftCurrencyRewardText => _rewardCoinsText.transform;
		public Transform ExperienceRewardText => _rewardExperienceText.transform;
		public Transform RewardProductTransform => _gridLayoutGroup.transform;
		public TextMeshProUGUI ShuffleTimerLabel => _shuffleTimerLabel;
		public TextMeshProUGUI FreeShufflesLeftText => _freeShufflesLeftText;
		public PriceButton PriceButton => _priceButton;
		public GameObject RewardPanel => _rewardPanel;
		public GameObject WaitingPanel => _waitingPanel;
		public Button SkipByPremiumButton => _skipByPremiumButton;
		public Button ShuffleOrderButton => _shuffleOrderButton;

		[Inject] public IResourcesManager jResourcesManager { get; set; }
		[Inject] public ISkipIntervalsData jSkipIntervalsData { get; set; }
		[Inject] public SignalOnSkipPriceChanged jSignalOnSkipPriceChanged { get; set; }

		public event Action<SeaportOrderItemView> ItemSelected;
		public Action ShuffleOrderClick;
		public Action SkipWaitingByAdWatch;
		public Action SkipWaitingByPremium;

		private bool _isSelectable;
		private IDisposable _shuffleDisposable;
		private int _singleProductSize = 175;
		private int _doubleProductSize = 140;
		
		public bool Selected => _selectedOrderImage.enabled;
		public int ServerPosition => _serverPosition;

		public bool IsSelectable
		{
			get => _isSelectable;
			set
			{
				_isSelectable = value;
				_selectOrderButton.interactable = value;
			}
		}

		private void OnEnable()
		{
			_selectOrderButton.onClick.AddListener(OnSelectOrderClicked);
			_skipByPremiumButton.onClick.AddListener(CallSkipWaitingByPremium);
			_skipByAdWatchButton.onClick.AddListener(CallSkipWaitingByAdWatch);
			ShuffleOrderButton.onClick.AddListener(CallShuffleOrder);
		}

		private void CallSkipWaitingByPremium()
		{
			OnSelectOrderClicked();
			SkipWaitingByPremium?.Invoke();
		}

		private void CallSkipWaitingByAdWatch()
		{
			OnSelectOrderClicked();
			SkipWaitingByAdWatch?.Invoke();
		}

		private void CallShuffleOrder()
		{
			OnSelectOrderClicked();
			ShuffleOrderClick?.Invoke();
		}

		private void OnDisable()
		{
			_selectOrderButton.onClick.RemoveListener(OnSelectOrderClicked);
			_skipByPremiumButton.onClick.RemoveListener(CallSkipWaitingByPremium);
			_skipByAdWatchButton.onClick.RemoveListener(CallSkipWaitingByAdWatch);
			ShuffleOrderButton.onClick.RemoveListener(CallShuffleOrder);

			_VFX.SetActive(false);
		}

		private void OnSelectOrderClicked()
		{
			if (!IsSelectable) return;

			ItemSelected?.Invoke(this);
			PlayScaleTween();
		}

		public void SetOrder(ISeaportOrderSlot orderSlot)
		{
			_shuffleDisposable?.Dispose();
			_shuffleDisposable = orderSlot.ShuffleTimeLeft.Subscribe(shuffleTime =>
			{
				bool isShuffling = shuffleTime > 0;
				int price = orderSlot.FreeSkip > 0 ? 0 : jSkipIntervalsData.GetCurrencyForTime(shuffleTime);
				
				_priceButton.Price = price;
				
				_freeShufflesLeftText.gameObject.SetActive(orderSlot.FreeSkip > 0);
				_freeShufflesLeftText.text = $"{orderSlot.FreeSkip}/{orderSlot.FreeSkipTotal} {LocalizationUtils.LocalizeL2("UICommon/FREE_SHUFFLE_COUNTER")}";
				_shuffleTimerLabel.text = UiUtilities.GetFormattedTimeFromSeconds_H_M_S(shuffleTime);
				RewardPanel.SetActive(!isShuffling);
				WaitingPanel.SetActive(isShuffling);

			});
			
			_rewardCoinsText.text = orderSlot.Awards.Coins.ToString();
			_rewardCoinsObject.SetActive(orderSlot.Awards.Coins != 0);
			_rewardExperienceText.text = orderSlot.Awards.Experience.ToString();
			_rewardExperienceObject.SetActive(orderSlot.Awards.Experience != 0);
			
			FillAwards(orderSlot);
		}
		
		private void FillAwards(ISeaportOrderSlot orderSlot)
		{
			_rewardItems.ForEach(item => item.ProductCard.SetActive(false));
			if (orderSlot.Awards.Products == null || _rewardItems.Count < orderSlot.Awards.Products.Count)
			{
				return;
			}

			_gridLayoutGroup.cellSize = orderSlot.Awards.Products.Count == 1 ? new Vector2(_singleProductSize, _singleProductSize) : new Vector2(_doubleProductSize, _doubleProductSize);
			for (var i = 0; i < orderSlot.Awards.Products.Count; i++)
			{
				SeaportRewardItem rewardItem = _rewardItems[i];
				AwardProduct awardProduct = orderSlot.Awards.Products[i];
				Sprite sprite = jResourcesManager.GetProductSprite(awardProduct.ProductId.Value);

				if (sprite != null)
				{
					rewardItem.ProductCard.SetActive(true);
					rewardItem.Image.sprite = sprite;
				}
				else
				{
					rewardItem.ProductCard.SetActive(false);
				}
				rewardItem.Count.text = awardProduct.Count.ToString();
			}
		}

		public void SetSelected(bool isSelected)
		{
			_selectedOrderImage.enabled = isSelected;
		}

		public void Lock()
		{
			ShuffleOrderButton.interactable = false;
			_isSelectable = false;
		}

		public void Unlock()
		{
			ShuffleOrderButton.interactable = true;
			_isSelectable = true;
		}

		private bool NeedToPlayAnimation()
		{
			return _scaleTween == null;
		}

		public void PlayContentUpdateTweens()
		{
			if (!NeedToPlayAnimation())
				return;

			const float scale = 0.05f;
			const float duration = 1.25f;
			const int vibrato = 0;
			const float elacity = 0.05f;
			const float jumpHeight = 12f;
			const int numberOfJumps = 3;
			const int numberOfScales = 3;

			_VFX.SetActive(false);
			_VFX.SetActive(true);

			_scaleTween = transform.DOPunchScale(scale * Vector3.one, duration / numberOfScales, vibrato, elacity).SetLoops(numberOfScales).OnComplete(() =>
			{
				_scaleTween = null;
			});
			_jumpTween = transform.DOJump(transform.position, jumpHeight, numberOfJumps, duration);
			_cardSwapTween = _gridLayoutGroup.GetComponent<RectTransform>().DOPunchScale(-Vector3.one, 1f, 0, 0);
			_jumpCompensationTween = _boxShadowImage.DOJump(_boxShadowImage.position, 0, numberOfJumps, duration);
			_scaleCompensationTween = _boxShadowImage.DOPunchScale(-scale * Vector3.one, duration / numberOfScales, vibrato, -elacity).SetLoops(numberOfScales);
		}

		private void PlayScaleTween()
		{
			if (!NeedToPlayAnimation())
				return;

			const float scale = 0.05f;
			const float duration = 0.25f;
			const int vibrato = 1;
			const float elacity = 0.1f;

			_scaleTween = transform.DOPunchScale(scale * Vector3.one, duration, vibrato, elacity).OnComplete(() =>
			{
				_scaleTween = null;
			});
			_scaleCompensationTween = _boxShadowImage.DOPunchScale(-scale * Vector3.one, duration, vibrato, -elacity);

		}
		
		[Serializable]
		public class SeaportRewardItem
		{
			public LoadableImage Image;
			public TextMeshProUGUI Count;
			public GameObject ProductCard;
		}
	}
}