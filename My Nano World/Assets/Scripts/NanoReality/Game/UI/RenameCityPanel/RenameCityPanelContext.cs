using NanoReality.GameLogic.PlayerProfile.Models.Api;

namespace NanoReality.Game.UI
{
    public class RenameCityPanelContext
    {
        
        [Inject]
        public IPlayerProfile jPlayerProfile { get; set; }
        
        public string CityName => jPlayerProfile.UserCity.CityName;
    }
}