using System.Text.RegularExpressions;
using Assets.NanoLib.UI.Core;
using Assets.Scripts.Extensions;
using NanoReality.Engine.UI.Extensions.UISignals;
using UnityEngine;

namespace NanoReality.Game.UI
{
    public class RenameCityPanelMediator : UIMediator<RenameCityPanelView>
    {
        [Inject] public SignalOnNeedToRenameCity jSignalOnNeedToRenameCity { get; set; }
        [Inject] public RenameCityPanelContext jRenameCityPanelContext { get; set; }
        
        private char _previousChar;
        private int _previousCharIndex;
        private readonly Regex _validateInputRegex = 
            new Regex("^[\u0621-\u064A0-9 ]+$");

        protected override void Show(object param)
        {
            base.Show(jRenameCityPanelContext.CityName);
        }

        protected override void OnShown()
        {
            base.OnShown();
            View.OkButton.onClick.AddListener(OnOkClick);
            View.CancelButton.onClick.AddListener(OnCancelClick);

            View.CityInputField.onValueChanged.AddListener(OnInputValueChanged);
            View.CityInputField.onValidateInput += OnValidateInput;
            View.CityInputField.onValueChanged.AddListener(View.UpdateInputFieldText);
            View.transform.SetAsLastSibling();
            
            View.CityInputField.ActivateInputField();
        }

        protected override void OnHidden()
        {
            View.OkButton.onClick.RemoveAllListeners();
            View.CancelButton.onClick.RemoveAllListeners();
            View.CityInputField.onValueChanged.RemoveAllListeners();
            View.CityInputField.onValidateInput = null;
            base.OnHidden();
        }

        private void OnOkClick()
        {
            jSignalOnNeedToRenameCity.Dispatch(View.CityInputField.text);
            Hide();
        }
        
        private char OnValidateInput(string text, int index, char addedChar)
        {
            if (ValidateSymbols(addedChar.ToString())) return addedChar;
            return '\0';
        }
        
        private bool ValidateSymbols(string newSymbol)
        {
            var value = _validateInputRegex.Match(newSymbol);
            Debug.Log($"ValidateSymbol:{newSymbol} isSuccess:{value.Success}");
            return value.Success;
        }
        
        private void OnInputValueChanged(string newValue)
        {
            View.OkButton.interactable = !string.IsNullOrEmpty(newValue) && ValidateSymbols(newValue);

            if (newValue.IsNullOrEmpty()) return;
         
            View.CityNameText = newValue;
            View.UpdateInputFieldText(newValue);
        }

        private void OnCancelClick()
        {
            Hide();
        }
    }
}