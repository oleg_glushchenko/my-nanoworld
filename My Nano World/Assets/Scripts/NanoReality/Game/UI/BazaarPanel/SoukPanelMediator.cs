﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.NanoLib.UI.Core;
using Assets.NanoLib.UtilitesEngine;
using Assets.Scripts.Engine.Analytics.Model.api;
using Assets.Scripts.Engine.UI.Views.Hud;
using Assets.Scripts.Engine.UI.Views.Popups;
using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using Assets.Scripts.Engine.UI.Views.Tooltips;
using Assets.Scripts.Engine.UI.Views.Warehouse;
using Assets.Scripts.GameLogic.Utilites;
using Engine.UI;
using GameLogic.Bazaar.Model.impl;
using GameLogic.Bazaar.Service;
using NanoLib.Core.Logging;
using NanoLib.Core.Pool;
using NanoLib.Core.Services.Sound;
using NanoLib.Core.Timers;
using NanoReality.Engine.Utilities;
using NanoReality.Game.UI.BazaarPanel.ProductView;
using NanoReality.GameLogic.BuildingSystem.CityCamera.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using NanoReality.GameLogic.Configs;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.Services;
using NanoReality.GameLogic.Utilites;
using NanoReality.UI.Components;
using strange.extensions.injector.api;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Game.UI.BazaarPanel
{
    public class SoukPanelMediator : UIMediator<SoukPanelView>
    {
        private ITimer _offerTickTimer;
        
        [Inject] public IBazaarService jBazaarService { get; set; }
        [Inject] public IPlayerProfile jPlayerProfile { get; set; }
        [Inject] public BalancePanelModel BalancePanelModel { get; set; }
        [Inject] public INanoAnalytics jNanoAnalytics { get; set; }
        [Inject] public OnHudBottomPanelIsVisibleSignal jOnHudBottomPanelIsVisibleSignal { get; set; }
        [Inject] public AddRewardItemSignal jAddRewardItemSignal { get; set; }
        [Inject] public IGameConfigData jGameConfigData { get; set; }
        [Inject] public IIconProvider jIconProvider { get; set; }
        [Inject] public ITimerManager jTimeManager { get; set; }
        [Inject] public IGameCamera jGameCamera { get; set; }

        private IPlayerResources PlayerResources => jPlayerProfile.PlayerResources;

        protected override void OnShown()
        {
            base.OnShown();
            
            View.SetOverlayActive(true);
            View.ShuffleButton.onClick.AddListener(OnShuffleBazaarButtonClick);

            OnBazaarCreated();
            jBazaarService.BazaarCreated += OnBazaarCreated;
            jBazaarService.FetchBazaar();

            View.BalancePanelView.Init(BalancePanelModel);
            BalancePanelModel.TempBalancePanel.Value = View.BalancePanelView;
            jSoundManager.Play(SfxSoundTypes.Bazaar, SoundChannel.SoundFX);

            jOnHudBottomPanelIsVisibleSignal.Dispatch(false);

            foreach (SoukBottomTradeItemView bottomTradeItem in View.BottomTradeItems)
            {
                bottomTradeItem.UnlockClicked += OnUnlockBottomSlotClicked;
                bottomTradeItem.BuyProductClicked += OnBuyBottomProductClicked;
            }
            
            foreach (SoukTopTradeItemView topTradeItem in View.TopTradeItems)
            {
                topTradeItem.UnlockClicked += OnUnlockTopSlotClicked;
                topTradeItem.BuyProductClicked += OnBuyTopProductClicked;
            }
            
            foreach (BazaarChestView chestView in View.BazaarChestViews)
            {
                chestView.BuyClicked += OnBuyLootBoxClicked;
            }
        }

        protected override void OnHidden()
        {
            base.OnHidden();
            
            ReleaseTopTradeline();
            View.ShuffleButton.onClick.RemoveListener(OnShuffleBazaarButtonClick);
            
            View.BalancePanelView.UnsubscribeBalancePanel();
            BalancePanelModel.TempBalancePanel.Value = null;
            
            jOnHudBottomPanelIsVisibleSignal.Dispatch(true);
            jBazaarService.BazaarCreated -= OnBazaarCreated;
            
            
            foreach (SoukBottomTradeItemView bottomTradeItem in View.BottomTradeItems)
            {
                bottomTradeItem.UnlockClicked -= OnUnlockBottomSlotClicked;
                bottomTradeItem.BuyProductClicked -= OnBuyBottomProductClicked;
            }

            foreach (SoukTopTradeItemView topTradeItem in View.TopTradeItems)
            {
                topTradeItem.UnlockClicked -= OnUnlockTopSlotClicked;
                topTradeItem.BuyProductClicked -= OnBuyTopProductClicked;
            }

            foreach (BazaarChestView chestView in View.BazaarChestViews)
            {
                chestView.BuyClicked -= OnBuyLootBoxClicked;
                chestView.Clear();
            }
        }

        private void StartOffersTimer()
        {
            _offerTickTimer?.CancelTimer();
            _offerTickTimer = jTimeManager.StartServerTimer(jBazaarService.OffersEndTime, OnTimerEnd, OnOfferTimerTick);

            void OnTimerEnd()
            {
                View.SetOverlayActive(true);
                View.TimerText.text = "00:00";
                jBazaarService.FetchBazaar();
            }
        }

        private void OnOfferTimerTick(float _)
        {
            var leftTime = jBazaarService.OffersEndTime - jTimeManager.CurrentUTC;
            View.TimerText.text = UiUtilities.GetFormattedTimeFromSeconds_M_S(leftTime);
        }

        private void OnBazaarCreated()
        {
            ReleaseTopTradeline();
            ShowTopTradeLine();
            ShowBottomTradeline();
            ShowLootBoxes();
            StartOffersTimer();
            View.ShufflePriceText.text = jBazaarService.SoukData.ShufflePrice == 0 
                ? LocalizationMLS.Instance.GetText(LocalizationKeyConstants.FREE_LABEL)
                : jBazaarService.SoukData.ShufflePrice.ToString();
            LayoutRebuilder.ForceRebuildLayoutImmediate(View.ShufflePriceText.gameObject.transform as RectTransform);
            View.SetOverlayActive(false);
        }

        private void ShowTopTradeLine()
        {
            var scenario = jBazaarService.TopTradeline;
            var topTradeItems = View.TopTradeItems;
            var totalProductsForPurchaseCount = scenario.SlotOffers.Length;
            
            for (int i = 0; i < topTradeItems.Count; i++)
            {
                SoukTopSlot productForPurchase = null;
                int unlockPrice = 0;
                int slotId = i;
                
                if (i >= totalProductsForPurchaseCount)
                {
                    unlockPrice = 
                        jGameConfigData.SoukConfig.TopTradeline[slotId].UnlockSlotPrice;
                }
                else
                {
                    unlockPrice = 0;
                    productForPurchase = scenario.SlotOffers[i];
                }

                var topItemView = topTradeItems[i];

                topItemView.Initialize(i, unlockPrice, productForPurchase);
            }
        }

        private void ShowBottomTradeline()
        {
            var bottomItemsViews = View.BottomTradeItems;
            var productsForPurchase = jBazaarService.BottomTradeline;

            for (int i = 0; i < bottomItemsViews.Count; i++)
            {
                var itemView = bottomItemsViews[i];
                var slotId = i;
                int unlockPrice = 0;
                SoukBottomOffer offer = null;
                
                if (i >= productsForPurchase.Length)
                {
                    unlockPrice = jBazaarService.SoukConfig.BottomTradeline[slotId].UnlockSlotPrice;
                }
                else
                {
                    offer = productsForPurchase[i];
                }
                
                itemView.Initialize(slotId, unlockPrice, offer);
            }
        }

        private void ShowLootBoxes()
        {
            var chests = jBazaarService.LootBoxes;
            var sortList =  chests.OrderBy(x => x.Id).ToArray();
            for (int i = 0; i <sortList.Count(); i++)
            {
                var lotBox = sortList[i];
                var view = View.BazaarChestViews[i];
                view.Initialize(lotBox);
            }
        }

        private void OnBuyLootBoxClicked(BazaarChestView productView)
        {
            BazaarLootBox product = productView.LootBoxOffer;
            if (PlayerResources.GoldCurrency < product.BoxPrice)
            {
                jPopupManager.Show(new NotEnoughMoneyPopupSettings(PriceType.Gold, product.BoxPrice - PlayerResources.GoldCurrency));
                return;
            }

            if (jBazaarService.BuyChests(product, productView, OnProductBought))
            {
                jSoundManager.Play(SfxSoundTypes.BuyAnyBazaarItem, SoundChannel.SoundFX);
                productView.Done();
            }
        }

        private void OnUnlockBottomSlotClicked(SoukBottomTradeItemView bottomSlotView)
        {
            var openedSlotsCount = jBazaarService.BottomTradeline.Length;
            var isItNextSlot = bottomSlotView.SlotId == openedSlotsCount;

            if (!isItNextSlot)
            {
                View.PanelTooltip.Show(new TooltipData
                {
                    ItemTransform = (RectTransform) bottomSlotView.UnlockSlotButton.transform,
                    ShowOnlySimpleTool = true,
                    Type = TooltipType.Simple,
                    Message = LocalizationUtils.LocalizeL2("UICommon/UNLOCK_PREVIOUS_SLOT")
                });
                return;
            }

            var unlockPrice = bottomSlotView.UnlockPrice;
            
            if (jPlayerProfile.PlayerResources.HardCurrency < unlockPrice)
            {
                var notEnoughValue = unlockPrice - jPlayerProfile.PlayerResources.HardCurrency;
                jPopupManager.Show(new NotEnoughMoneyPopupSettings(PriceType.Hard, notEnoughValue));
                return;
            }

            jPopupManager.Show(new PurchaseByPremiumPopupSettings(unlockPrice), () =>
            {
                View.SetOverlayActive(true);
                bottomSlotView.UnlockSlotButton.interactable = false;
                jSoundManager.Play(SfxSoundTypes.AddNewSlot, SoundChannel.SoundFX);
                jBazaarService.UnlockBottomSlot(bottomSlotView.SlotId, () =>
                {
                    bottomSlotView.UnlockSlotButton.interactable = true;
                    View.SetOverlayActive(false);
                });
            });
        }

        private void OnBuyBottomProductClicked(SoukBottomTradeItemView slotView)
        {
            var offerPrice = slotView.SlotOffer.GetPrice();
            if(!jPlayerProfile.PlayerResources.IsEnoughtForBuy(offerPrice))
            {
                ShowPopup(new NotEnoughMoneyPopupSettings(offerPrice));
                return;
            }
            
            var productsCount = slotView.SlotOffer.Products.Sum(product => product.Value);
            
            if (!jPlayerProfile.PlayerResources.IsCanPlaceToWarehouse(productsCount))
            {
                ShowPopup(new NotEnoughSpacePopupSettings(), () =>
                {
                    ShowWindowSignal.Dispatch(typeof(WarehouseView), null);
                });
            
                slotView.BuyProductButton.interactable = true;
                return;
            }

            View.SetOverlayActive(true);
            slotView.BuyProductButton.interactable = false;
            jSoundManager.Play(SfxSoundTypes.BuyAnyBazaarItem, SoundChannel.SoundFX);
            
            jBazaarService.BuyBottomProduct(slotView.SlotId, () =>
            {
                slotView.BuyProductButton.interactable = true;
                    
                foreach (KeyValuePair<int,int> productCountPair in slotView.SlotOffer.Products)
                {
                    var productId = productCountPair.Key;
                    var productCount = productCountPair.Value;
                    TradelineRewardAnimations(productId, productCount, slotView.transform.position);
                }

                View.SetOverlayActive(false);
            });
        }

        private void OnUnlockTopSlotClicked(SoukTopTradeItemView topSlotView)
        {
            var openedSlotsCount = jBazaarService.TopTradeline.SlotOffers.Length;
            var isItNextSlot = topSlotView.SlotId == openedSlotsCount;
            
            if (!isItNextSlot)
            {
                View.PanelTooltip.Show(new TooltipData
                {
                    ItemTransform = (RectTransform) topSlotView.UnlockSlotButton.transform,
                    ShowOnlySimpleTool = true,
                    Type = TooltipType.Simple,
                    Message = LocalizationUtils.LocalizeL2("UICommon/UNLOCK_PREVIOUS_SLOT")
                });
                return;
            }

            var price = topSlotView.UnlockPrice;
            if (PlayerResources.HardCurrency < price)
            {
                var notEnoughValue = price - PlayerResources.HardCurrency;
                jPopupManager.Show(new NotEnoughMoneyPopupSettings(PriceType.Hard, notEnoughValue));
                return;
            }

            jPopupManager.Show(new PurchaseByPremiumPopupSettings(price), result =>
            {
                // productView.BuyNewSlot.interactable = true;
                if (result == PopupResult.Ok)
                {
                    if (jBazaarService.UnlockTopSlot(topSlotView.SlotId, OnProductBought))
                    {
                        jSoundManager.Play(SfxSoundTypes.AddNewSlot, SoundChannel.SoundFX);
                        topSlotView.Done();
                    }
                }
            });
        }

        private void OnBuyTopProductClicked(SoukTopTradeItemView productView)
        {
            SoukTopSlot topSlot = productView.SlotOffer;
            if (jPlayerProfile.PlayerResources.GoldCurrency < topSlot.Price)
            {
                jPopupManager.Show(new NotEnoughMoneyPopupSettings(PriceType.Gold, topSlot.Price - jPlayerProfile.PlayerResources.GoldCurrency));
                return;
            }

            jSoundManager.Play(SfxSoundTypes.BuyAnyBazaarItem, SoundChannel.SoundFX);
            jBazaarService.BuyTopSlotProduct(topSlot, productView, OnProductBought);
            View.SetOverlayActive(true);
            TradelineRewardAnimations(topSlot.ProductId, topSlot.Count, productView.transform.position);
            productView.Done();
        }
        
        private void OnShuffleBazaarButtonClick()
        {
            void ConfirmShuffle()
            {            
                jSoundManager.Play(SfxSoundTypes.MadeShuffle, SoundChannel.SoundFX);
                View.SetOverlayActive(true);
                    
                jBazaarService.ShuffleSouk(() =>
                {
                    View.SetOverlayActive(false);
                });
            }
            
            var hardPrice = jBazaarService.SoukData.ShufflePrice;
            
            if (hardPrice <= 0)
            {
                ConfirmShuffle();
                return;
            }
            
            if (jPlayerProfile.PlayerResources.HardCurrency < hardPrice)
            {
                jPopupManager.Show(new NotEnoughMoneyPopupSettings(PriceType.Hard, hardPrice - jPlayerProfile.PlayerResources.HardCurrency));
                return;
            }
            
            jPopupManager.Show(new PurchaseByPremiumPopupSettings(hardPrice), result =>
            {
                if (result == PopupResult.Ok)
                { 
                    ConfirmShuffle();
                }
            });
        }
        
        private void OnProductBought()
        {
            OnBazaarCreated();
        }

        private void ReleaseTopTradeline()
        {
            // _productViews.ForEach(view => View.jBazaarProductViewPool.Push(view));
            //
            // foreach (var viewBazaarChestView in View.BazaarChestViews)
            // {
            //     viewBazaarChestView.PurchaseButton.onClick.RemoveAllListeners();
            // }
            //
            // _productViews.Clear();
        }
        
        private void TradelineRewardAnimations(int productId, int productCount, Vector3 startPosition)
        {
            Vector3 position = jGameCamera.ScreenToWorldPoint(startPosition);
            string productsAmount = productCount.ToString();
            Sprite productSprite = jIconProvider.GetProductSprite(productId);
            jAddRewardItemSignal.Dispatch(new AddItemSettings(FlyDestinationType.Warehouse, position, productsAmount,
                productSprite));
        }
    }
}
