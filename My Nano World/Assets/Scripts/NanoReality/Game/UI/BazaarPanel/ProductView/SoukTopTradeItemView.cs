﻿using System;
using Assets.Scripts.Engine.UI.Views.Trade.Items;
using Assets.Scripts.GameLogic.Utilites;
using GameLogic.Bazaar.Model.impl;
using NanoReality.Core.Resources;
using NanoReality.GameLogic.Services;
using strange.extensions.mediation.impl;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Game.UI.BazaarPanel.ProductView
{
    public class SoukTopTradeItemView : View
    {
        [SerializeField] private TextMeshProUGUI _priceText;
        [SerializeField] private TextMeshProUGUI _priceNewSlotText;
        [SerializeField] private GameObject _boughtText;
        [SerializeField] private Image _currencyIcon;
        [SerializeField] private TradeProductView _tradeProductView;
        [SerializeField] private Button _purchaseButton; // TODO: rename to "buy product button".
        [SerializeField] private Button _buyNewSlot;     // TODO: rename to "unlock slot".
        [SerializeField] private Transform _defaultItemView;
        [SerializeField] private Transform _newLotView;

        [Inject] public IResourcesManager jResourcesManager { get; set; }
        [Inject] public IProductService jProductService { get; set; }
        
        public int SlotId { get; private set; }
        public int UnlockPrice { get; private set; }
        public SoukTopSlot SlotOffer { get; private set; }

        public Button UnlockSlotButton => _buyNewSlot;
        
        public event Action<SoukTopTradeItemView> UnlockClicked;
        public event Action<SoukTopTradeItemView> BuyProductClicked;

        public void Initialize(int slotId, int unlockSlotPrice, SoukTopSlot soukTopSlot)
        {
            UnlockPrice = unlockSlotPrice;
            SlotOffer = soukTopSlot;
            SlotId = slotId;

            bool isUnlocked = soukTopSlot != null;
            if (isUnlocked)
            {
                if (soukTopSlot.SoldOut)
                {
                    _defaultItemView.gameObject.SetActive(true);
                    _newLotView.gameObject.SetActive(false);
                    _buyNewSlot.interactable = false;
                    _boughtText.SetActive(true);
                    _buyNewSlot.gameObject.SetActive(false);
                    _purchaseButton.interactable = false;
                    _purchaseButton.gameObject.SetActive(false);
                    _tradeProductView.gameObject.SetActive(false);
                    return;
                }
                
                var icon = jResourcesManager.GetProductSprite(soukTopSlot.ProductId);
                _tradeProductView.SetItem(icon, soukTopSlot.Count, jProductService.GetProductName(soukTopSlot.ProductId));
                _tradeProductView.gameObject.SetActive(true);
                _priceText.text = soukTopSlot.Price == 0 
                    ? LocalizationMLS.Instance.GetText(LocalizationKeyConstants.FREE_LABEL) 
                    : soukTopSlot.Price.ToString();
                _defaultItemView.gameObject.SetActive(true);
                _newLotView.gameObject.SetActive(false);
                _buyNewSlot.interactable = false;
                _boughtText.SetActive(false);
                _buyNewSlot.gameObject.SetActive(false);
                _purchaseButton.interactable = true;
                _purchaseButton.gameObject.SetActive(true);
            }
            else
            {
                _priceNewSlotText.text = unlockSlotPrice.ToString();
                _defaultItemView.gameObject.SetActive(false);
                _newLotView.gameObject.SetActive(true);
                _buyNewSlot.interactable = true;
                _boughtText.SetActive(false);
                _buyNewSlot.gameObject.SetActive(true);
                _purchaseButton.interactable = false;
                _purchaseButton.gameObject.SetActive(false);
            }
        }
        
        private void OnEnable()
        {
            _purchaseButton.onClick.AddListener(OnBuyProductClicked);
            _buyNewSlot.onClick.AddListener(OnUnlockClicked);
        }

        private void OnDisable()
        {
            _purchaseButton.onClick.RemoveListener(OnBuyProductClicked);
            _buyNewSlot.onClick.RemoveListener(OnUnlockClicked);
        }

        private void ChangeState(bool isAddSlot)
        {
            _defaultItemView.gameObject.SetActive(!isAddSlot);
            _newLotView.gameObject.SetActive(isAddSlot);
            _buyNewSlot.interactable = isAddSlot;
            _boughtText.SetActive(false);
            _buyNewSlot.gameObject.SetActive(isAddSlot);
            _purchaseButton.interactable = !isAddSlot;
            _purchaseButton.gameObject.SetActive(!isAddSlot);
        }

        public void Done()
        {
            _purchaseButton.gameObject.SetActive(false);
            _buyNewSlot.gameObject.SetActive(false);
            _newLotView.gameObject.SetActive(false);
            _tradeProductView.gameObject.SetActive(false);
            _boughtText.SetActive(true);
        }
        
        private void OnBuyProductClicked()
        {
            BuyProductClicked?.Invoke(this);
        }

        private void OnUnlockClicked()
        {
            UnlockClicked?.Invoke(this);
        }
    }
}
