﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.GameLogic.Utilites;
using GameLogic.Bazaar.Model.impl;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.Services;
using strange.extensions.mediation.impl;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SoukBottomTradeItemView : View
{
    [Header("Locked State")]
    [SerializeField] private GameObject _lockedPanel;
    [SerializeField] private Button _unlockButton;
    [SerializeField] private TextMeshProUGUI _unlockPriceText;

    [Header("Unlocked State")]
    [SerializeField] private GameObject _unlockedPanel;
    [SerializeField] private Button _buyProductButton;

    [SerializeField] private Image _currencyIconImage;
    [SerializeField] private TextMeshProUGUI _productPriceText;

    [Header("Multi Products")]
    [SerializeField] private GameObject _multiProductsPanel;
    [SerializeField] private List<ProductItem> _productItems;
    
    [Header("Single Product")]
    [SerializeField] private GameObject _singleProductsPanel;
    [SerializeField] private Image _productIconImage;
    [SerializeField] private TextMeshProUGUI _productCountText;

    [SerializeField] private GameObject _soldOutPanel;

    [Inject] public IIconProvider jIconProvider { get; set; }
    
    public int SlotId { get; private set; }
    public SoukBottomOffer SlotOffer { get; private set; }
    public int UnlockPrice { get; private set; }

    public Button BuyProductButton => _buyProductButton;
    public Button UnlockSlotButton => _unlockButton;
    
    public event Action<SoukBottomTradeItemView> UnlockClicked;
    public event Action<SoukBottomTradeItemView> BuyProductClicked;

    private void OnEnable()
    {
        _unlockButton.onClick.AddListener(OnUnlockClicked);
        _buyProductButton.onClick.AddListener(OnBuyProductClicked);
    }

    private void OnDisable()
    {
        _unlockButton.onClick.RemoveListener(OnUnlockClicked);
        _buyProductButton.onClick.RemoveListener(OnBuyProductClicked);
    }

    public void Initialize(int slotId, int unlockPrice, SoukBottomOffer slotOffer)
    {
        bool isLocked = slotOffer == null;
        SlotId = slotId;
        SlotOffer = slotOffer;
        UnlockPrice = unlockPrice;
        
        if (isLocked)
        {
            _unlockPriceText.text = unlockPrice == 0 
                ? LocalizationMLS.Instance.GetText(LocalizationKeyConstants.FREE_LABEL) 
                : unlockPrice.ToString();
            _soldOutPanel.gameObject.SetActive(false);
            _lockedPanel.gameObject.SetActive(true);
            _unlockedPanel.gameObject.SetActive(false);
        }
        else
        {
            if (slotOffer.SoldOut)
            {
                _soldOutPanel.gameObject.SetActive(true);
                _lockedPanel.gameObject.SetActive(false);
                _unlockedPanel.gameObject.SetActive(false);
                return;
            }
            
            _soldOutPanel.gameObject.SetActive(false);
            _lockedPanel.gameObject.SetActive(false);
            _unlockedPanel.gameObject.SetActive(true);
            
            if (slotOffer.SingleItemOffer)
            {
                DrawSingleProductItem();
            }
            else
            {
                DrawMultiProductItems();
            }

            var price = slotOffer.GetPrice();
            _productPriceText.text = price.GetPriceValue() == 0 
                ? LocalizationMLS.Instance.GetText(LocalizationKeyConstants.FREE_LABEL) 
                : price.GetPriceValue().ToString();
            _currencyIconImage.sprite = jIconProvider.GetCurrencySprite(price.PriceType);
        }
    }

    private void DrawSingleProductItem()
    {
        _singleProductsPanel.gameObject.SetActive(true);
        _multiProductsPanel.gameObject.SetActive(false);

        var targetProduct = SlotOffer.Products.First();
        var productId = targetProduct.Key;
        var productCount = targetProduct.Value;
        
        _productIconImage.sprite = jIconProvider.GetProductSprite(productId);
        _productCountText.text = productCount.ToString();
    }

    private void DrawMultiProductItems()
    {
        _singleProductsPanel.gameObject.SetActive(false);
        _multiProductsPanel.gameObject.SetActive(true);
        
        var slotOffer = SlotOffer;
        int counter = 0;
        foreach (KeyValuePair<int,int> offerProduct in slotOffer.Products)
        {
            if (counter < _productItems.Count)
            {
                var productItem = _productItems[counter];
                var productId = offerProduct.Key;
                var productCount = offerProduct.Value;
                    
                productItem.ProductImage.sprite = jIconProvider.GetProductSprite(productId);
                productItem.CountText.text = productCount.ToString();
                productItem.GameObject.SetActive(true);
            }
            counter++;
        }

        if (counter < _productItems.Count)
        {
            for (; counter < _productItems.Count; counter++)
            {
                _productItems[counter].GameObject.SetActive(false);
            }
        }
    }
    
    private void OnBuyProductClicked()
    {
        BuyProductClicked?.Invoke(this);
    }

    private void OnUnlockClicked()
    {
        UnlockClicked?.Invoke(this);
    }

    [Serializable]
    private sealed class ProductItem
    {
        public GameObject GameObject;
        public Image ProductImage;
        public TextMeshProUGUI CountText;
    }
}