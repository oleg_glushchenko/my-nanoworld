using System;
using System.Collections;
using Assets.NanoLib.UI.Core.Signals;
using Assets.Scripts.Engine.UI.Views.Popups;
using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using NanoReality.Core.Resources;
using NanoReality.Game.UI.SettingsPanel;
using NanoReality.GameLogic.Settings.GameSettings.Models.Api;
using strange.extensions.command.impl;
using UniRx;
using UnityEngine;

namespace NanoReality.Game.UI.Popups.DownloadPopup
{
	public class ChangeGraphicQualityCommand : Command
	{
		[Inject] public bool IsHighQuality { get; set; }
		[Inject] public IPopupManager jPopupManager { get; set; }
		[Inject] public IGameSettings jGameSettings { get; set; }
		[Inject] public HideWindowSignal jHideWindowSignal { get; set; }
		[Inject] public SignalReloadApplication jSignalReloadApplication { get; set; }

		public override void Execute()
		{
			Retain();
			
			if (IsHighQuality)
				Observable.FromCoroutine(() => DownloadRemoteBundle(AssetBundlesNames.ManifestAssetBundleName, onCompleteCallback: OnAssetBundlesManifestDownloaded)).Subscribe();
			else
				ShowQualityChangeWindow(true);
		}

        private void OnAssetBundlesManifestDownloaded(AssetBundle manifestBundle)
        {
            var manifest = manifestBundle.LoadAsset<AssetBundleManifest>(AssetBundlesNames.ManifestFileName);

            if (AssetBundleManager.IsBundlesCached(manifest))
	            ShowQualityChangeWindow(false);

            else if (SystemInfo.systemMemorySize > 3500)
            {
	            jPopupManager.Show(new DownloadPopupSettings(), OnDownloadPopupClosed);
            }
        }

		private void OnDownloadPopupClosed(PopupResult result)
		{
			switch (result)
			{
				case PopupResult.Ok:
					jGameSettings.SetEnabled(SettingsToggleType.GraphicQuality, true);
					Release();
					break;
				
				default:
					jGameSettings.SetEnabled(SettingsToggleType.GraphicQuality, false);
					jHideWindowSignal.Dispatch(typeof(SettingsPanelView));
					Fail();
					break;
			}
		}

		private IEnumerator DownloadRemoteBundle(string bundleName, Hash128 hash = default, Action<AssetBundle> onCompleteCallback = null)
		{
			var downloadOperation = new AssetBundleWebRequestOperation<AssetBundle>(bundleName, hash, onCompleteCallback, message => ShowNetworkErrorMessage("Asset Bundles Downloading", message));
			yield return downloadOperation.Download(AssetBundleManager.DownloadAssetBundle);
		}

		private void ShowQualityChangeWindow(bool isLowQuality)
		{
			var settings = new QualityPopupSettings {ChangeToLowQualityGraphic = isLowQuality};
			jPopupManager.Show(settings, result =>
			{
				if(result == PopupResult.Ok)
				{
					jGameSettings.SetEnabled(SettingsToggleType.GraphicQuality, !isLowQuality);
					Release();
				}
			});
		}

		private void ShowNetworkErrorMessage(string title, string message)
		{
			jPopupManager.Show(new NetworkErrorPopupSettings(title, message, onReloadApplicationCallback: OnReloadApplication));

			void OnReloadApplication()
			{
				jGameSettings.SetEnabled(SettingsToggleType.GraphicQuality, false);
				jSignalReloadApplication.Dispatch();
				Release();
			}
		}
	}
}