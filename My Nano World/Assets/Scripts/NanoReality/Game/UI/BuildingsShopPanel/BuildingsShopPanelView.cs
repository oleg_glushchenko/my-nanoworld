﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.NanoLib.UI.Core.Components;
using Assets.NanoLib.UI.Core.Views;
using Assets.NanoLib.Utilities;
using Assets.Scripts.MultiLanguageSystem.Logic;
using NanoReality.Engine.UI.Extensions.BuildingsShopPanel;
using NanoReality.Engine.UI.UIAnimations;
using NanoReality.Engine.UI.Views.BuildingsShopPanel;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;

namespace NanoReality.Game.UI.BuildingsShopPanel
{
    //TODO rewrite to BasicMediator
    [ShowOdinSerializedPropertiesInInspector]
    public class BuildingsShopPanelView : UIPanelView
    {
        #region Inspector

        public List<SectorDependencies> SectorDependencieses;
        public ItemContainer BuildingsContainer;
        [SerializeField] private List<SubCategoryFilterView> _subCategoryFilterViews;
        [SerializeField] public BuildingItem _buildingItemPrefab;
        [SerializeField] public ScrollSnap _scrollSnap;
        [SerializeField] public BuildingPropertiesTooltip _buildingPropertiesTooltip;
        [SerializeField] private TextMeshProUGUI _sectorName;
        [SerializeField] private CustomRaycast _customRaycast;
        [SerializeField] private Button _incrementSectorButton;
        [SerializeField] private Button _decrementSectorButton;
        #endregion

        private event Action OnShowEffectEnded;
        public event Action OnPointerDown;
        public Button IncrementSectorButton => _incrementSectorButton;
        public Button DecrementSectorButton => _decrementSectorButton;
        public TextMeshProUGUI SectorName => _sectorName;

        public IEnumerable<BuildingItem> BuildingItems => BuildingsContainer.CurrentItems.Cast<BuildingItem>();

        public CustomRaycast CustomRaycast => _customRaycast;

        public List<SubCategoryFilterView> SubCategoryFilterViews => _subCategoryFilterViews;

        public BuildingItem AddItem()
        {
            return BuildingsContainer.AddItem() as BuildingItem;
        }

        [PostConstruct]
        public void PostConstruct()
        {
            BuildingsContainer.InstaniteContainer(_buildingItemPrefab, 0);  
        }

        public void OnPointerDownExternal()
        {
            OnPointerDown?.Invoke();
        }
        
        public override void Initialize(object parameters)
        {
            base.Initialize(parameters);
            BusinessSectorsTypes id = BusinessSectorsTypes.Unsupported;

            switch (parameters)
            {
                case BuildingShopData data:
                    id = data.BusinessSector;
                    break;
                case IMapBuilding building:
                    id = building.SectorIds.First();
                    break;
            }
            
            _scrollSnap.UnlockScroll();
            UpdateScrolls();
        }

        public void ClearCurrentItems() => BuildingsContainer.ClearCurrentItems();

        public void ScrollToBuilding(Id_IMapObjectType type)
        {
            var building = BuildingItems.FirstOrDefault(x => x != null && x.CurrentMapObject.ObjectTypeID == type);
            if (building == null)
                return;

            ScrollTo(building);
            OnShowEffectEnded += () =>
            {
                building.PlayBounceAnimation();
                OnShowEffectEnded = null;
                building.IsSelected = true;
            };
        }

        protected override void ShowEffectEnded(ISpecialEffect specialEffect)
        {
            base.ShowEffectEnded(specialEffect);
            OnShowEffectEnded?.Invoke();
        }

        public void ScrollTo(BuildingItem item)
        {
            var focusPosition = 0f;

            var count = BuildingsContainer.CurrentItems.Count;
            for (var i = 0; i < count; i++)
            {
                var buildingItem = BuildingsContainer.CurrentItems[i] as BuildingItem;
                if (buildingItem != item) continue;
                
                focusPosition = Mathf.Clamp01(i / (float)count);
                break;
            }

            var page = Mathf.FloorToInt(_scrollSnap.PageCount * focusPosition);
            if (page >= _scrollSnap.PageCount && _scrollSnap.PageCount > 0)
            {
                page = _scrollSnap.PageCount - 1;
            }
            _scrollSnap.ChangePage(page);
        }
        
        public void ScrollToAndLock(BuildingItem item)
        {
            SubscribeOnSnapScrollPageChanged();
            ScrollTo(item);
        }

        private void SubscribeOnSnapScrollPageChanged()
        {
            _scrollSnap.onPageChange += OnPageChange;
            void OnPageChange(int _)
            {
                LockScroll();
                _scrollSnap.onPageChange -= OnPageChange;
            }
        }

        public void UpdateScrolls()
        {
            _scrollSnap.UpdateListItemPositions();
            _scrollSnap.ChangePage(0);

            if (_buildingPropertiesTooltip.Visible)
                _buildingPropertiesTooltip.Hide();
        }

        public void LockScroll()
        {
            _scrollSnap.LockScroll();
        }

        public void EnableBuildingFiltersToggles(bool enable)
        {
            foreach (var dependency in SectorDependencieses)
            {
                foreach (var filter in dependency.CategoriesFilter.Filters)
                {
                    filter.Toggle.enabled = enable;
                }
            }
        }
    }
}
