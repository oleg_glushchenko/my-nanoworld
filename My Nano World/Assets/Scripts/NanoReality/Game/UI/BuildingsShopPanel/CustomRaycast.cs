﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine.UI.Extensions;

namespace NanoReality.Game.UI.BuildingsShopPanel
{
    public class CustomRaycast : MonoBehaviour
    {
        private GraphicRaycaster _raycaster;
        private NonDrawingGraphic _canvas;
        private PointerEventData _pointerEventData;
        private EventSystem _eventSystem;
        [SerializeField] private LayerMask _targetLayer;

        public UnityEvent OnHitUIBlocker;

        public bool Active
        {
            get => _canvas.raycastTarget;
            set => _canvas.raycastTarget = value;
        }

        void Start()
        {
            _raycaster = GetComponent<GraphicRaycaster>();
            _canvas = GetComponent<NonDrawingGraphic>();
            _eventSystem = FindObjectOfType<EventSystem>();
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Mouse0) && Active)
            {
                _pointerEventData = new PointerEventData(_eventSystem);
                _pointerEventData.position = Input.mousePosition;
                List<RaycastResult> results = new List<RaycastResult>();
                _raycaster.Raycast(_pointerEventData, results);

                var needToRaise = true;
                for (var index = 0; index < results.Count; index++)
                {
                    if (GetMask(results[index].gameObject.layer) == _targetLayer.value)
                    {
                        needToRaise = false;
                    }
                }

                if (needToRaise)
                {
                    OnHitUIBlocker?.Invoke();
                    Active = false;
                }
            }
        }

        private int GetMask(int layer)
        {
            return 1 << layer;
        }
    }
}