using System.Linq;
using Assets.NanoLib.Utilities;
using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using Assets.Scripts.Engine.UI.Views.Warehouse;
using Assets.Scripts.GameLogic.Utilites;
using Engine.UI;
using I2.Loc;
using NanoLib.Core.Logging;
using NanoLib.Core.Services.Sound;
using NanoReality.Game.FoodSupply;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using NanoReality.GameLogic.OrderDesk;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.Utilites;
using UniRx;
using UnityEngine;

namespace NanoReality.Game.UI
{
    public sealed class FoodSupplyCookingPanelController : FoodSupplyPanelController
    {
        [SerializeField] private FoodSupplyCookingSubPanelView _view;

        public FoodSupplyCookingSubPanelView View => _view;
        
        private FoodSupplyOrderItemModel _selectedItemModel;
        private readonly CompositeDisposable _compositeDisposable = new CompositeDisposable();

        private IPlayerResources PlayerResources => Context.jPlayerProfile.PlayerResources;

        private const string ResultMealsCount = "1";

        public override void Show()
        {
            base.Show();
            
            foreach (var orderItemView in _view.OrderItemsList)
            {
                orderItemView.Model.Clicked.Subscribe(_ => { SetItemSelected(orderItemView.Model); }).AddTo(_compositeDisposable);
            }
            
            _view.CookFoodButton.onClick.AddListener(OnCookFoodClicked);
            Context.jWarehouseChangedSignal.AddListener(UpdateView);
            Context.jFoodSupplyService.FoodSupplyCityInfoChanged += UpdateView;
            
            UpdateView();
        }

        public override void Hide()
        {
            base.Hide();
            
            _view.CookFoodButton.onClick.RemoveListener(OnCookFoodClicked);
            Context.jWarehouseChangedSignal.RemoveListener(UpdateView);
            Context.jFoodSupplyService.FoodSupplyCityInfoChanged -= UpdateView;
            
            _compositeDisposable.Clear();
        }

        private void CookMeal(FoodSupplyOrderItemModel model)
        {
            var selectedView = _view.OrderItemsList.Find(c => c.Model == model);
            var productId = model.ResultProductRecipe.Id;
            var recipeId = model.ResultProductRecipe.RecipeId;

            var viewCookFoodButton = _view.CookFoodButton;
            viewCookFoodButton.interactable = false;

            var viewCookFoodButtonImage = _view.CookingAvailableButtonImage;
            viewCookFoodButtonImage.gameObject.SetActive(false);

            var cookingProgressButtonImage = _view.CookingInProgressButtonImage;
            cookingProgressButtonImage.gameObject.SetActive(true);
            cookingProgressButtonImage.fillAmount = 0;

            var cookLocalize = _view.CookButtonLocalize;
            cookLocalize.Term = ScriptTerms.UICommon.COOKING_BUTTON_TEXT;

            foreach (var itemView in _view.OrderItemsList)
            {
                itemView.Model.Interactable.SetValueAndForceNotify(false);
            }
            
            var waitResponseAsyncSubject = new AsyncSubject<Unit>();
            waitResponseAsyncSubject.OnNext(Unit.Default);
            
            var playCookingButtonVfx = _view.PlayCookingButtonFilling();
            var playCookingVfx = selectedView.PlayCookingVfx();

            Context.jFoodSupplyService.CookFoodSupplyProduct(CurrentModel.MapID, recipeId, () =>
            {
                waitResponseAsyncSubject.OnCompleted();
            });

            Context.jSoundManager.Play(SfxSoundTypes.ClickRestaurants, SoundChannel.SoundFX);

            var fromPosition = Context.jGameCamera.ScreenToWorldPoint(selectedView.transform.position);
            var productSprite = Context.jIconProvider.GetProductSprite(productId);
            var rewardItemSettings = new AddItemSettings(FlyDestinationType.Warehouse, fromPosition, ResultMealsCount, productSprite);
            Context.jAddRewardItemSignal.Dispatch(rewardItemSettings);
            
            void OnCookingOperationsComplete(Unit onComplete)
            {
                Context.jSoundManager.Play(SfxSoundTypes.CoockingMealFinished, SoundChannel.SoundFX);
                viewCookFoodButtonImage.gameObject.SetActive(true);
                cookingProgressButtonImage.gameObject.SetActive(false);
                viewCookFoodButton.interactable = true;
                cookLocalize.Term = ScriptTerms.UICommon.COOK_BUTTON_TEXT;
                UpdateView();
                
                foreach (var itemView in _view.OrderItemsList)
                {
                    itemView.Model.Interactable.SetValueAndForceNotify(true);
                }
            }

            Observable.WhenAll(waitResponseAsyncSubject, playCookingButtonVfx, playCookingVfx).Subscribe(OnCookingOperationsComplete);
        }

        private void UpdateView()
        {
            UpdateAvailableItems();

            var model = _selectedItemModel;
            if (model == null)
            {
                var itemWithRecipe = _view.OrderItemsList.FirstOrDefault(order => order.Model.ResultProductRecipe != null);
                if (itemWithRecipe != null)
                {
                    model = itemWithRecipe.Model;
                }
            }

            SetItemSelected(model);
        }

        private void UpdateAvailableItems()
        {
            var availableRecipes = Context.jFoodSupplyService.GetRecipesData();
            var itemViews = _view.OrderItemsList;
            for (var slotIndex = 0; slotIndex < itemViews.Count; slotIndex++)
            {
                var itemView = itemViews[slotIndex];
                var itemModel = itemView.Model;

                FoodSupplyProductRecipe productRecipe = null;
                foreach (var availableRecipe in availableRecipes)
                {
                    var product = Context.jFoodSupplyService.GetProduct(availableRecipe.Id);
                    if (product == null || product.SlotId - 1 != slotIndex) continue;

                    productRecipe = availableRecipe;
                    break;
                }
                
                if (productRecipe == null)
                {
                    itemModel.ResultProductProperty.SetValueAndForceNotify(null);
                    itemModel.IsActive.SetValueAndForceNotify(false);
                    continue;
                }

                itemModel.ResultProductProperty.SetValueAndForceNotify(productRecipe);
                itemModel.ProductData.SetValueAndForceNotify(Context.jFoodSupplyService.GetProduct(productRecipe.Id));
                itemModel.IsActive.SetValueAndForceNotify(true);
            }
        }

        private void UpdateSelectedOrderPanel()
        {
            if (_selectedItemModel == null)
            {
                "Selected Item is null. Can't update selected order panel.".LogError(LoggingChannel.Mechanics);
                return;
            }

            var productItems = _view.SelectedOrderProductItems;
            var resultProductRecipe = _selectedItemModel.ResultProductRecipe;
            var requiredProducts = resultProductRecipe.Products;
            
            _view.CaloriesText.text = string.Format(LocalizationUtils.LocalizeL2("Localization/FOOD_CALORIES"), 
                _selectedItemModel.ProductData.Value.Calories);
            
            for (var i = 0; i < productItems.Count; i++)
            {
                var productItemView = productItems[i];
                var itemModel = productItemView.Model;

                Sprite productIcon;
                bool isActive;
                int currentProductsCount;
                int requiredProductsCount;
                int requiredProductId;
                
                if (i >= requiredProducts.Count)
                {
                    isActive = false;
                    currentProductsCount = 0;
                    requiredProductsCount = 0;
                    requiredProductId = 0;
                    productIcon = null;
                }
                else
                {
                    var requiredProduct = requiredProducts[i];
                    isActive = true;
                    currentProductsCount = PlayerResources.GetProductCount(requiredProduct.ProductId);
                    requiredProductsCount = requiredProduct.Count;
                    productIcon = Context.jIconProvider.GetProductSprite(requiredProduct.ProductId);
                    requiredProductId = requiredProduct.ProductId;
                }

                itemModel.CurrentCountProperty.SetValueAndForceNotify(currentProductsCount);
                itemModel.RequiredCountProperty.SetValueAndForceNotify(requiredProductsCount);
                itemModel.RequiredIdProperty.SetValueAndForceNotify(requiredProductId);
                itemModel.ProductSpriteProperty.SetValueAndForceNotify(productIcon);
                itemModel.IsActiveProperty.SetValueAndForceNotify(isActive);
                itemModel.ShowProductTooltip.Subscribe(tooltipData => { Context.jSignalOnNeedToShowTooltip.Dispatch(tooltipData); }).AddTo(_compositeDisposable);
            }
        }

        private void SetItemSelected(FoodSupplyOrderItemModel model)
        {
            if (model == null) return;
            
            if (_selectedItemModel != model)
            {
                _selectedItemModel?.IsSelected.SetValueAndForceNotify(false);
                _selectedItemModel = model;
                _selectedItemModel.IsSelected.SetValueAndForceNotify(true);
            }
            
            _view.SelectedProductNameText.text = LocalizationUtils.GetProductText(_selectedItemModel.ResultProductRecipe.Id);
            UpdateSelectedOrderPanel();
        }

        private void OnCookFoodClicked()
        {
            if (_selectedItemModel == null) return;
            
            var isEnoughProducts = true;
            foreach (var productData in _selectedItemModel.ResultProductRecipe.Products) 
                isEnoughProducts &= PlayerResources.HasEnoughProduct(productData.ProductId, productData.Count);

            if (isEnoughProducts)
                CookMeal(_selectedItemModel);
            else
                BuySlotProducts(_selectedItemModel);
        }

        private void BuySlotProducts(FoodSupplyOrderItemModel model)
        {
            var requiredProducts = model.ResultProductRecipe.Products.ToDictionary<ProductData, Id<Product>, int>(product => product.ProductId, product => product.Count);
            var resultPrice = new Price
            {
                SoftPrice = 0,
                HardPrice = 0,
                ProductsPrice = requiredProducts,
                PriceType = PriceType.Resources
            };

            Context.jBuyProductsActionSignal.Dispatch(resultPrice, isSuccess =>
            {
                if (isSuccess)
                {
                    OnCookFoodClicked();
                }
            });
        }
    }
}