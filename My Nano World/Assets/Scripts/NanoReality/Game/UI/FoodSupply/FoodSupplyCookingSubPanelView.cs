using System.Collections.Generic;
using Assets.Scripts.Engine.UI.Views.Trade.Items;
using DG.Tweening;
using I2.Loc;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Game.UI
{
    public sealed class FoodSupplyCookingSubPanelView : MonoBehaviour
    {
        [Header("Available cooking products")]
        [SerializeField] private List<FoodSupplyCookingItemView> _orderItemsList;
        
        [Header("Selected product panel")]
        [SerializeField] private TextMeshProUGUI _selectedProductNameText;
        [SerializeField] private List<ProductItemView> _selectedOrderProductItems;
        
        [SerializeField] private TextMeshProUGUI _caloriesText;
        [SerializeField] private Image _cookingAvailableButtonImage;
        [SerializeField] private Image _cookingInProgressButtonImage;
        [SerializeField] private Localize _cookButtonLocalize;
        [SerializeField] private Button _cookFoodButton;
        [SerializeField] private Color _forbiddenCookButtonColor;

        public List<ProductItemView> SelectedOrderProductItems => _selectedOrderProductItems;
        public TextMeshProUGUI CaloriesText => _caloriesText;

        public Button CookFoodButton => _cookFoodButton;

        public Color ForbiddenCookButtonColor => _forbiddenCookButtonColor;

        public List<FoodSupplyCookingItemView> OrderItemsList => _orderItemsList;

        public Image CookingInProgressButtonImage => _cookingInProgressButtonImage;

        public Image CookingAvailableButtonImage => _cookingAvailableButtonImage;

        public Localize CookButtonLocalize => _cookButtonLocalize;

        public TextMeshProUGUI SelectedProductNameText => _selectedProductNameText;
        
        public AsyncSubject<Unit> PlayCookingButtonFilling()
        {
            var playCookingAnimationAsyncSubject = new AsyncSubject<Unit>();
            playCookingAnimationAsyncSubject.OnNext(Unit.Default);

            CookingInProgressButtonImage.DOFillAmount(1, 1.5f).OnComplete(() =>
            {
                playCookingAnimationAsyncSubject.OnCompleted();
            });

            return playCookingAnimationAsyncSubject;
        }
    }
}