using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Engine.UI.Views.Popups;
using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using Assets.Scripts.Engine.UI.Views.Tooltips;
using Engine.UI;
using NanoLib.Core.Services.Sound;
using NanoReality.GameLogic.Utilites;
using UniRx;
using UnityEngine;

namespace NanoReality.Game.UI
{
    public sealed class FoodSupplyFeedingPanelController : FoodSupplyPanelController
    {
        [SerializeField] private FoodSupplyFeedingPanelView _view;
        
        public FoodSupplyFeedingPanelView View => _view;
        
        private readonly CompositeDisposable _compositeDisposable = new CompositeDisposable();
        private float _loadedProductsCalories;
        private float _actualCaloriesMultiplier;
        private bool _isSlotBuyingLocked;
        private bool _isAnimating;

        public override void Show()
        {
            base.Show();

            if (_compositeDisposable.Count > 0)
            {
                RemoveListeners();
            }
            
            AddListeners();

            UpdateView();
        }

        private void AddListeners()
        {
            foreach (var orderItemView in _view.FeedingItemViews)
            {
                orderItemView.Model.Clicked.Subscribe(_ => { OnFeedingItemClicked(orderItemView); }).AddTo(_compositeDisposable);
                orderItemView.Model.UnlockSlotClicked.Subscribe(_ => { OnUnlockSlotClicked(orderItemView.Model); }).AddTo(_compositeDisposable);
            }

            _view.FeedCitizensButton.onClick.AddListener(OnFeedCitizensClicked);
            Context.jFoodSupplyService.FoodSupplyCityInfoChanged += UpdateFeedAvailability;
        }

        private void RemoveListeners()
        {
            _compositeDisposable.Clear();
            _view.FeedCitizensButton.onClick.RemoveListener(OnFeedCitizensClicked);
            Context.jFoodSupplyService.FoodSupplyCityInfoChanged -= UpdateFeedAvailability;
        }

        public override void Hide()
        {
            base.Hide();

            RemoveListeners();

            foreach (var orderItemView in _view.FeedingItemViews)
            {
                orderItemView.Model.LoadedProductProperty.SetValueAndForceNotify(null);
            }
            
            if (_view.HintTooltip.IsVisible)
            {
                _view.HintTooltip.Hide();
            }

            _view.AdditionalCoinsContainer.SetActive(false);
            ResetProgressBar();
        }

        private void UpdateView()
        {
            var foodSupplySlots = Context.jFoodSupplyService.GetSlotsData();
            var slotIndexCounter = 0;

            var availableToUnlock = true;
            foreach (var orderItemView in _view.FeedingItemViews)
            {
                var slot = foodSupplySlots[slotIndexCounter];
                var slotModel = orderItemView.Model;
                slotModel.SlotProperty.SetValueAndForceNotify(slot);
                if (slotModel.Slot.IsLocked)
                {
                    slotModel.AvailableToUnlock.SetValueAndForceNotify(availableToUnlock);
                    if (availableToUnlock)
                    {
                        // Only first locked slot can be available to unlock.
                        availableToUnlock = false;
                    }
                }
                else
                {
                    slotModel.AvailableToUnlock.SetValueAndForceNotify(false);
                }

                slotIndexCounter++;
            }
            
            CalculateAvailableToLoadSlots();
            UpdateExpectedFoodSupplyProgressBar();
            UpdateFeedButton();
        }

        private void UpdateFeedAvailability()
        {
            UpdateFeedButton();
            CalculateAvailableToLoadSlots();
        }

        private void UpdateFeedButton()
        {
            _view.SetFeedingAvailableActive(!Context.jFoodSupplyService.IsFilled && !_isAnimating);
        }

        private void OnFeedingItemClicked(FoodSupplyFeedingItemView itemView)
        {
            var itemModel = itemView.Model;
            var slot = itemModel.SlotProperty.Value;
            if (slot.IsLocked)
            {
                if(itemModel.AvailableToUnlock.Value)
                {
                    OnUnlockSlotClicked(itemModel);
                }
                
                return;
            }

            if (itemModel.AvailableToLoad.Value)
            {
                Context.jPopupManager.Show(new WarehouseSelectionPopupSettings(GetProductsToShow(), (productId) =>
                {
                    OnUserConfirmProductLoad(itemModel, productId);
                }));
            }
        }

        private Dictionary<int, int> GetProductsToShow()
        {
            var foodSupplyConfig = Context.jGameConfigData.FoodSupplyConfig;
            var warehouseFoodSupplyProducts = Context.PlayerResources.PlayerProducts.Where(c => foodSupplyConfig.IsFoodSupplyProduct(c.Key.Value));
            
            var alreadySelectedProducts = new List<int>();
            foreach (var feedingItemView in _view.FeedingItemViews)
            {
                var loadedProductProperty = feedingItemView.Model.LoadedProductProperty;
                if (loadedProductProperty.Value != default)
                {
                    alreadySelectedProducts.Add(loadedProductProperty.Value.Id);
                }
            }

            var productsToShow = new Dictionary<int, int>();
            var productsToHide = new List<int>();
            foreach (var productCountPair in warehouseFoodSupplyProducts)
            {
                var productId = productCountPair.Key;
                var productCount = productCountPair.Value;
                var selectedProductCount = alreadySelectedProducts.Count(c => c == productId.Value);
                if (selectedProductCount > 0)
                {
                    productCount -= selectedProductCount;
                    if (productCount <= 0)
                    {
                        productsToHide.Add(productId.Value);
                    }
                }

                productsToShow.Add(productId.Value, productCount);
            }

            foreach (var productId in productsToHide)
            {
                productsToShow.Remove(productId);
            }

            var orderedProducts = new Dictionary<int, int>();
            var productsCount = productsToShow.Count;
            for (var productIndex = 0; productIndex < productsCount; productIndex++)
            {
                var id = default(int);
                var minimumCalories = int.MaxValue;
                foreach (var product in productsToShow)
                {
                    var productData = foodSupplyConfig.GetProduct(product.Key);
                    if (productData.Calories >= minimumCalories) continue;

                    minimumCalories = productData.Calories;
                    id = product.Key;
                }

                orderedProducts.Add(id, productsToShow[id]);
                productsToShow.Remove(id);
            }

            return orderedProducts;
        }

        private void OnFeedCitizensClicked()
        {
            var loadedSlotsViews = _view.FeedingItemViews.Where(c => c.Model.LoadedProductProperty.Value != null);
            
            // If calories filled to maximum, show hint tooltip.
            if (Context.jFoodSupplyService.IsFilled)
            {
                if(!_isAnimating) return;
                ShowSimpleTooltip((RectTransform) _view.FeedCitizensButton.transform, LocalizationUtils.LocalizeL2("UICommon/FOOD_SUPPLY_ACTIVE"), true); //Food Bar is full!
                return;
            }

            // If user didn't load any slot, show hint tooltip.
            if (!loadedSlotsViews.Any())
            {
                ShowSimpleTooltip((RectTransform) _view.FeedCitizensButton.transform, LocalizationUtils.LocalizeL2("UICommon/FOOD_SUPPLY_FILL_SLOT"));
                return;
            }

            // If new food supply calories count more than maximum available, show popup to say that not used calories will convert to coins
            var resultCalories = Context.jFoodSupplyService.CurrentCalories + _loadedProductsCalories * _actualCaloriesMultiplier;
            if (resultCalories > Context.jFoodSupplyService.MaxAvailableCalories)
            {
                //TODO: need to use another (own)  window because it need to 2 line to arabic, but it works wrong 
                var settings = new ConfirmPopupSettings
                {
                    Title = LocalizationUtils.LocalizeL2("UICommon/FOOD_SUPPLY_TITLE"), //Restaurant
                    Message = string.Format(LocalizationUtils.LocalizeL2("UICommon/FOOD_SUPPLY_POINTS_CONVERT"), GetAdditionalCoinsCount()) //Extra Calories will be converted to {0} nanocoins. Are you sure?
                };

                Context.jPopupManager.Show(settings, result =>
                {
                    if(result == PopupResult.Ok)
                    {
                        FeedFoodSupplyProducts();
                    }
                });
            }
            else
            {
                FeedFoodSupplyProducts();
            }
        }

        private void FeedFoodSupplyProducts()
        {
            var loadedSlotsViews = _view.FeedingItemViews.Where(c => c.Model.LoadedProductProperty.Value != null).ToList();
            var selectedSlotProducts = new Dictionary<int, int>(loadedSlotsViews.Count);
            foreach (var itemView in loadedSlotsViews)
            {
                var selectedProduct = itemView.Model;
                selectedSlotProducts.Add(selectedProduct.Slot.Id, selectedProduct.LoadedProduct.Id);
            }

            // Create reward flying items.
            foreach (var itemView in loadedSlotsViews)
            {
                var itemModel = itemView.Model;
                
                var fromPosition = Context.jGameCamera.ScreenToWorldPoint(itemView.transform.position);
                var sprite = Context.jIconProvider.GetProductSprite(itemModel.LoadedProductProperty.Value.Id);
                var rewardItemSettings = new AddItemSettings(FlyDestinationType.FoodSupply, fromPosition, 1, sprite);
                Context.jAddRewardItemSignal.Dispatch(rewardItemSettings);
                Context.jSoundManager.Play(SfxSoundTypes.FlyMeal, SoundChannel.SoundFX);

                itemModel.LoadedProductProperty.SetValueAndForceNotify(null);
            }

            var waitResponseAsyncSubject = new AsyncSubject<Unit>();
            waitResponseAsyncSubject.OnNext(Unit.Default);
            var feedingButtonVfx = _view.PlayFeedingButtonFilling();
            _isAnimating = true;

            Context.jFoodSupplyService.FeedCitizens(CurrentModel.MapID, selectedSlotProducts, OnCompleteRequest);
            
            SetActive(false);
            ResetProgressBar();

            void OnCompleteRequest()
            {
                if(Context.jFoodSupplyService.IsFilled)
                    AddCoinsFromCalories();

                waitResponseAsyncSubject.OnCompleted();
            }

            void OnFeedingAsyncOperationComplete(Unit onComplete)
            {
                _isAnimating = false;

                SetActive(true);
                
                _view.AdditionalCoinsContainer.SetActive(false);
                UpdateView();
                _view.HintTooltip.Hide();
            }
            
            Observable.WhenAll(waitResponseAsyncSubject, feedingButtonVfx).Subscribe(OnFeedingAsyncOperationComplete);
        }

        private void AddCoinsFromCalories()
        {
            var coinsCount = GetAdditionalCoinsCount();
            var position = Context.jGameCamera.ScreenToWorldPoint(_view.AdditionalCoinsContainer.transform.position);
            
            Context.jAddRewardItemSignal.Dispatch(new AddItemSettings(FlyDestinationType.SoftCoins, position, coinsCount));
            Context.jPlayerProfile.PlayerResources.AddSoftCurrency(coinsCount);
        }

        private void OnUserConfirmProductLoad(FoodSupplyFeedingItemModel itemModel, int productId)
        {
            var previousCalories = itemModel.LoadedProduct?.Calories ?? default;

            var productConfig = Context.jGameConfigData.FoodSupplyConfig.GetProduct(productId);
            itemModel.LoadedProductProperty.SetValueAndForceNotify(productConfig);
            itemModel.ProductIconSprite.SetValueAndForceNotify(Context.jIconProvider.GetProductSprite(productId));
            CalculateAvailableToLoadSlots();

            _loadedProductsCalories += itemModel.LoadedProductProperty.Value.Calories - previousCalories;
            _actualCaloriesMultiplier = itemModel.Slot.CaloriesMultiplier;

            UpdateAdditionalCoinsPanel();
            UpdateExpectedFoodSupplyProgressBar();
        }

        private void UpdateAdditionalCoinsPanel()
        {
            var coinsCount = GetAdditionalCoinsCount();
            if (coinsCount > 0)
            {
                _view.AdditionalCoinsContainer.SetActive(true);
                _view.AdditionalCoinsCount.text = "+" + coinsCount;
            }
            else
            {
                _view.AdditionalCoinsContainer.SetActive(false);
            }
        }

        private void OnUnlockSlotClicked(FoodSupplyFeedingItemModel itemModel)
        {
            if (_isSlotBuyingLocked) return;
            
            _isSlotBuyingLocked = true;

            var unlockPrice = itemModel.Slot.UnlockPrice;
            var isEnoughForBuy = Context.jPlayerProfile.PlayerResources.IsEnoughtForBuy(unlockPrice);
            if (!isEnoughForBuy)    
            {
                Context.jPopupManager.Show(new NotEnoughMoneyPopupSettings(unlockPrice));
                
                _isSlotBuyingLocked = false;
            }
            else
            {
                UnlockSlot(itemModel);
            }
        }

        private void UnlockSlot(FoodSupplyFeedingItemModel itemModel)
        {
            void OnConfirmUnlock()
            {
                Context.jSoundManager.Play(SfxSoundTypes.AddNewSlot, SoundChannel.SoundFX);
                Context.jFoodSupplyService.UnlockFoodSupplySlot(CurrentModel.MapID, itemModel.Slot.Id,
                    () =>
                    {
                        UpdateView();
                        _isSlotBuyingLocked = false;
                    });
            }

            void OnCancelUnlock()
            {
                _isSlotBuyingLocked = false;
            }

            Context.jPopupManager.Show(new PurchaseByPremiumPopupSettings(itemModel.Slot.UnlockPrice), 
                OnConfirmUnlock, OnCancelUnlock);
        }

        private void CalculateAvailableToLoadSlots()
        {
            var isPreviousSlotAvailable = true;
            var isNotFilled = !Context.jFoodSupplyService.IsFilled;
            foreach (var viewFeedingItemView in _view.FeedingItemViews)
            {
                viewFeedingItemView.Model.AvailableToLoad.SetValueAndForceNotify(isPreviousSlotAvailable && isNotFilled);
                isPreviousSlotAvailable = viewFeedingItemView.Model.LoadedProduct != null;
            }
        }

        private void UpdateExpectedFoodSupplyProgressBar()
        {
            float progressBarValue;
            if (_loadedProductsCalories > 0f)
            {
                var service = Context.jFoodSupplyService;
                progressBarValue = Mathf.Clamp01((service.CurrentCalories + _loadedProductsCalories * _actualCaloriesMultiplier) / service.MaxAvailableCalories);
            }
            else
            {
                progressBarValue = default;
            }

            _view.ExpectedFoodSupplyProgressBar.Value = progressBarValue;
        }

        private int GetAdditionalCoinsCount()
        {
            var foodSupplyService = Context.jFoodSupplyService;
            var overfilledCalories = foodSupplyService.CurrentCalories + _loadedProductsCalories * _actualCaloriesMultiplier - foodSupplyService.MaxAvailableCalories;
            var coinsCount = Mathf.CeilToInt(overfilledCalories * foodSupplyService.CoinsPerCalorie);

            return coinsCount;
        }

        private void ShowSimpleTooltip(RectTransform parent, string message, bool showOnlySimpleTool = false)
        {
            var tooltipData = new TooltipData
            {
                ItemTransform = parent,
                ShowOnlySimpleTool = showOnlySimpleTool,
                Type = TooltipType.Simple,
                Message = message
            };
                
            _view.HintTooltip.Show(tooltipData);
        }
        
        private void SetAllSlotsInteractable(bool isInteractable)
        {
            foreach (var itemView in _view.FeedingItemViews)
            {
                itemView.Model.Interactable.SetValueAndForceNotify(isInteractable);
            }
        }

        private void SetActive(bool isActive)
        {
            _view.SetFeedingAvailableActive(isActive);
            _view.SetFeedingInProgressActive(!isActive);
            SetAllSlotsInteractable(isActive);
        }

        private void ResetProgressBar()
        {
            _loadedProductsCalories = default;
            _actualCaloriesMultiplier = default;
            _view.ExpectedFoodSupplyProgressBar.Value = default;
        }
    }
}