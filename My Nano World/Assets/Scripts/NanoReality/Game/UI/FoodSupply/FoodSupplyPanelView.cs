﻿using System;
using System.Collections.Generic;
using Assets.NanoLib.UI.Core.Views;
using DG.Tweening;
using NanoReality.Engine.UI.Views.Panels;
using NanoReality.UI.Components;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using TMPro;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Game.UI
{
    [ShowOdinSerializedPropertiesInInspector]
    public class FoodSupplyPanelView : UIPanelView, ISerializationCallbackReceiver, ISupportsPrefabSerialization
    {
        [SerializeField] private ToggleGroup _tabsToggleGroup;
        [OdinSerialize] private Dictionary<Toggle, FoodSupplyPanelController> _tabToggles;

        [SerializeField] private Button _closeButton;
        [SerializeField] private Button _presentButton;
        [SerializeField] private List<GameObject> _presentFxList;
        [SerializeField] private BalancePanelView _balancePanelView;
        [SerializeField] private GameObject _overlay;

        [SerializeField] private ResizableProgressBar _currentFoodSupplyProgressBar;
        [SerializeField] private TextMeshProUGUI _currentCaloriesHint;
        [SerializeField] private RectTransform _progressBarTransform;
        [SerializeField] private GameObject progressBarStatusEffectVFX;
        [SerializeField] private Transform _rewardTipsParent;
        [SerializeField] private FoodSupplyRewardItem _rewardItemPrefab;
        [SerializeField] private HudRewardsTooltip _rewardsTooltip;
        
        [SerializeField] private CanvasGroup _foodSupplyInfoPanel;
        [SerializeField] private Button _openInfoPanelButton;
        [SerializeField] private Button _closeInfoPanelButton;
        [SerializeField] private Transform _toolTipPanel;
            
        private readonly CompositeDisposable _compositeDisposable = new CompositeDisposable();

        public event Action<FoodSupplyPanelController> TabToggleSelected;

        public Button CloseButton => _closeButton;

        public BalancePanelView BalancePanelView => _balancePanelView;

        public GameObject Overlay => _overlay;
        
        public HudRewardsTooltip RewardsTooltip => _rewardsTooltip;

        public ResizableProgressBar CurrentFoodSupplyProgressBar => _currentFoodSupplyProgressBar;
        public TextMeshProUGUI CurrentCaloriesHint => _currentCaloriesHint;
        
        public RectTransform ProgressBarTransform => _progressBarTransform;
        
        public GameObject StatusEffectVFX => progressBarStatusEffectVFX;

        public CanvasGroup FoodSupplyInfoPanel => _foodSupplyInfoPanel;

        public Button CloseInfoPanelButton => _closeInfoPanelButton;
        public Button PresentButton => _presentButton;
        
        public Button OpenInfoPanelButton => _openInfoPanelButton;

        public Dictionary<Toggle, FoodSupplyPanelController> TabToggles => _tabToggles;

        public IEnumerable<UiSubPanelController> TabControllers => _tabToggles.Values;

        public List<FoodSupplyRewardItem> RewardItems { get; } = new List<FoodSupplyRewardItem>();
        
        public ToggleGroup TabsToggleGroup => _tabsToggleGroup;
        
        protected override void Awake()
        {
            base.Awake();
            foreach (var toggle in _tabToggles)
            {
                toggle.Key.OnSelectAsObservable().Subscribe(_ => OnTabToggleSelected(toggle.Value)).AddTo(_compositeDisposable)
                    .AddTo(_compositeDisposable);
            }
        }

        public FoodSupplyRewardItem InstantiateTip(int percent)
        {
            FoodSupplyRewardItem foodSupplyRewardItem = RewardItems.Find(e=>e.PercentValue==percent);

            if (foodSupplyRewardItem)
            {
                return foodSupplyRewardItem;
            }

            foodSupplyRewardItem = Instantiate(_rewardItemPrefab, _rewardTipsParent);
            RewardItems.Add(foodSupplyRewardItem);
            
            return foodSupplyRewardItem;
        }

        public void DestroyRewardTipsWithAnimation(IEnumerable<FoodSupplyRewardItem> tips)
        {
            foreach (var tip in tips)
            {
                tip.GetRewardFx();
            }
        }

        public void ActivateRewardTips(int filledPercent)
        {
            foreach (var tip in RewardItems)
            {
                if (tip.PercentValue > filledPercent)
                {
                    tip.ActivateFx();
                }
            }
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            _compositeDisposable.Dispose();
        }

        private void OnTabToggleSelected(FoodSupplyPanelController controller)
        {
            TabToggleSelected?.Invoke(controller);
            HideToggles(controller);
        }
        
        public void PlayGiftAnimation()
        {
            var duration = 5f;
            
            foreach (var fx in _presentFxList)
            {
                fx.gameObject.SetActive(true);
            }
            
            _presentButton.transform.localScale = Vector3.one;
            _presentButton.transform.DOPunchScale(Vector3.one * .1f, duration, 3);
        }
        
        public void StopGiftAnimation()
        {
            _presentButton.transform.localScale = Vector3.one;
            foreach (var fx in _presentFxList)
            {
                fx.gameObject.SetActive(false);
            } 
        }
        
        public void HideToggles(FoodSupplyPanelController controller)
        {
            // todo - this is a stupid solution to hide the toggle button and !not to brake tutorial! 
            foreach (var tabToggle in _tabToggles)
            {
                var canvasGroup = tabToggle.Key.GetComponent<CanvasGroup>();
                if (!canvasGroup) continue;
                
                if (tabToggle.Value == controller)
                {
                    canvasGroup.alpha = 0;
                }
                else
                {
                    canvasGroup.alpha = 1;
                }
            }
        }

        #region Odin Serialize implementation

        [SerializeField, HideInInspector]
        private SerializationData _serializationData;

        SerializationData ISupportsPrefabSerialization.SerializationData
        {
            get => _serializationData;
            set => _serializationData = value;
        }

        public Transform ToolTipPanel => _toolTipPanel;

        void ISerializationCallbackReceiver.OnAfterDeserialize()
        {
            UnitySerializationUtility.DeserializeUnityObject(this, ref _serializationData);
        }

        void ISerializationCallbackReceiver.OnBeforeSerialize()
        {
            UnitySerializationUtility.SerializeUnityObject(this, ref _serializationData);
        }
        
        #endregion

    }
}