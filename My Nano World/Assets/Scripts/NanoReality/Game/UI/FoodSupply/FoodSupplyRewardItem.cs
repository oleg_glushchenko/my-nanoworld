using System;
using DG.Tweening;
using NanoReality.GameLogic.Data;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Game.UI
{
	public class FoodSupplyRewardItem : MonoBehaviour
	{
		[SerializeField] private Button _tipButton;
		[SerializeField] private TextMeshProUGUI _coinsNumberText;
		[SerializeField] private Image _icon;
		[SerializeField] private Image _checkMark;
		[SerializeField] private ParticleSystem _rewardReceivedEffect;
		[SerializeField] private ParticleSystem _onApproachEffect;
		[SerializeField] private bool _disableTip = true;
		
		private const float ScaleFxDuration = 1f;

		public bool isDisabled { get; private set; }
		public int PercentValue { get; private set; }
		public Reward RewardData { get; private set; }

		public event Action<FoodSupplyRewardItem> Clicked;

		private void OnEnable()
		{
			_tipButton.onClick.AddListener(TipClicked);
		}

		private void OnDisable()
		{
			_tipButton.onClick.RemoveListener(TipClicked);
		}

		public void Initialize(Reward reward, int percent, Sprite sprite)
		{
			RewardData = reward;
			PercentValue = percent;
			_icon.sprite = sprite;
			_coinsNumberText.text = reward.softCurrencyCount.ToString();
		}

		public void DisableWithoutFx()
		{
			_rewardReceivedEffect.gameObject.SetActive(false);
			_coinsNumberText.gameObject.SetActive(false);
			transform.localScale = Vector3.one * .9f;
			_checkMark.transform.localScale = Vector3.one;
			_checkMark.gameObject.SetActive(true);
			_onApproachEffect.gameObject.SetActive(false);
			
			isDisabled = true;
		}

		public void ActivateFx()
		{
			_coinsNumberText.gameObject.SetActive(true);
			_coinsNumberText.transform.localScale = Vector3.one;
			transform.localScale = Vector3.one;
			_checkMark.gameObject.SetActive(false);
			_onApproachEffect.gameObject.SetActive(false);
			_rewardReceivedEffect.gameObject.SetActive(false);
			
			isDisabled = false;
		}

		public void GetRewardFx()
		{
			if (isDisabled)
			{
				return;
			}

			_rewardReceivedEffect.gameObject.SetActive(true);

			DOTween.Sequence().SetDelay(_rewardReceivedEffect.main.duration).OnComplete(() => { _rewardReceivedEffect.gameObject.SetActive(false); });

			transform.DOScale(Vector3.one * .9f, ScaleFxDuration).SetEase(Ease.OutElastic);

			_coinsNumberText.transform.DOScale(Vector3.zero, ScaleFxDuration).SetEase(Ease.OutElastic);

			_checkMark.transform.localScale = Vector3.zero;
			_checkMark.gameObject.SetActive(true);

			DOTween.Sequence().SetDelay(ScaleFxDuration).OnComplete(() => { _checkMark.transform.DOScale(Vector3.one, ScaleFxDuration).SetEase(Ease.OutElastic); });

			_onApproachEffect.transform.DOScale(Vector3.zero, ScaleFxDuration).SetEase(Ease.OutElastic).OnComplete(() => _onApproachEffect.gameObject.SetActive(false));

			isDisabled = true;
		}

		private void TipClicked()
		{
			if (_disableTip)
			{
				return;
			}

			Clicked?.Invoke(this);
		}

		public void ShowOnApproachFx()
		{
			_onApproachEffect.gameObject.SetActive(true);
		}
	}
}