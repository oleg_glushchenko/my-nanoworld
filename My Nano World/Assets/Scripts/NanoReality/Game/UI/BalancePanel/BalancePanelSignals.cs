using System;
using Engine.UI;
using strange.extensions.signal.impl;
using UnityEngine;

namespace NanoReality.UI.Components
{
    public class SignalWarehouseChangeState : Signal<bool> { }
    
    public class HideBalancePanelTooltipSignal : Signal { }

    public class SignalCityOrderDeskChangeState : Signal<bool> { }
    public class SignalBalancePanelViewChangeState : Signal<bool> { }  

    public class SignalExperienceUpdate : Signal { }

    public class SignalHardCurrencyUpdate : Signal { }

    public class SignalSoftCurrencyUpdate : Signal { }

    public class SignalGoldCurrencyUpdate : Signal { }

    public class SignalPopulationUpdate : Signal { }

    public class SignalInfoNanoGemsCurrencyUpdate : Signal { }

    public class SignalUpdateBalancePanelCounters : Signal { }

    public class AddRewardItemSignal : Signal<AddItemSettings> { }
    
    public class SignalWarehouseCapacityUpgrade : Signal { }

    public class SignalNanogemsIntro : Signal<Action<GameObject>> { }
}