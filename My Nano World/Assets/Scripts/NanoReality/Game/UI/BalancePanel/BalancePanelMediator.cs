﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.NanoLib.UI.Core;
using Assets.NanoLib.UI.Core.Signals;
using Assets.NanoLib.UI.Core.Views;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using Assets.Scripts.Engine.UI.Views.Hud;
using Assets.Scripts.Engine.UI.Views.Warehouse;
using Engine.UI;
using NanoLib.Core;
using NanoReality.Engine.UI.Views.Panels;
using NanoReality.Game.FoodSupply;
using NanoReality.Game.UI;
using NanoReality.Game.UI.BalancePanel;
using NanoReality.GameLogic.Bank;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityCamera.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityMap.Controllers;
using NanoReality.GameLogic.BuildingSystem.Happiness;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.SupplyFood;
using NanoReality.GameLogic.Common.Controllers.Signals;
using NanoReality.GameLogic.GameManager.Models.api;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using UniRx;
using UnityEngine;

namespace NanoReality.UI.Components
{
    public class BalancePanelMediator : UIMediator<BalancePanelView>, IObserver
    {
        #region Injects
        [Inject] public UserLevelUpSignal jUserLevelUpSignal { get; set; }
        [Inject] public RoadConstructSetActiveSignal jRoadConstructSetActiveSignal { get; set; }
        [Inject] public PlayerOpenedPanelSignal jSignalPlayerOpenedPanel { get; set; }
        [Inject] public SignalExperienceUpdate jSignalExperienceUpdate { get; set; }
        [Inject] public SignalBalancePanelViewChangeState jSignalBalancePanelViewChangeState { get; set; }
        [Inject] public SignalHardCurrencyUpdate jSignalHardCurrencyUpdate { get; set; }
        [Inject] public SignalSoftCurrencyUpdate jSignalSoftCurrencyUpdate { get; set; }
        [Inject] public SignalGoldCurrencyUpdate jSignalGoldCurrencyUpdate { get; set; }
        [Inject] public SignalWarehouseProductsAmountChanged jSignalWarehouseProductsAmountChanged { get; set; }
        [Inject] public SignalUpdateHappiness jSignalUpdateHappiness { get; set; }
        [Inject] public SignalOnCityPopulationUpdated jSignalOnCityPopulationUpdated { get; set; }
        [Inject] public BalancePanelContext PanelContext { get; set; }
        [Inject] public ShowWindowSignal jShowWindowSignal { get;set; }
        [Inject] public HideBalancePanelTooltipSignal jHideTooltipSignal { get; set; }
        [Inject] public BalancePanelModel PanelModel { get; set; }
        [Inject] public SignalOnSoftCurrencyStateChanged jSignalOnSoftCurrencyStateChanged { get; set; }
        [Inject] public SignalOnHardCurrencyStateChanged jSignalOnHardCurrencyStateChanged { get; set; }
        [Inject] public SignalOnGoldCurrencyStateChanged jSignalOnGoldCurrencyStateChanged { get; set; }
        [Inject] public IPlayerProfile jPlayerProfile { get; set; }
        [Inject] public IGameManager jGameManager { get; set; }
        [Inject] public IGameCamera jGameCamera { get; set; }
        
        [Inject] public OnHudButtonsIsClickableSignal jOnHudButtonsIsClickableSignal { get; set; }

        #endregion

        private BalancePanelView CurrentBalancePanelView { get; set; }

        private UIPanelView _activeTooltipPanel;
        private Dictionary<FlyDestinationType, Tuple<RectTransform, Sprite>> _destinationDependencies;
        
        private readonly CompositeDisposable _compositeDisposable = new CompositeDisposable();

        #region Methods

        protected override void OnShown()
        {
            base.OnShown();
            ViewChangeState(true);
            PanelModel.SoftClick.Subscribe(state => OnCurrencyBuyButtonClick(BankTabType.Soft)).AddTo(_compositeDisposable);
            PanelModel.HardClick.Subscribe(state => OnCurrencyBuyButtonClick(BankTabType.Hard)).AddTo(_compositeDisposable);
            PanelModel.InfoPanelClick.Subscribe(state => ShowProfileTooltip()).AddTo(_compositeDisposable);
            PanelModel.WarehouseClick.Subscribe(state => OnWarehouseButtonClick()).AddTo(_compositeDisposable);
            PanelModel.HappyClick.Subscribe(state => ShowHappinessTooltip()).AddTo(_compositeDisposable);
            PanelModel.FoodSupplyClick.Subscribe(state => ShowFoodSupplyTooltip()).AddTo(_compositeDisposable);
            PanelModel.OverlayClick.Subscribe(state => HideCurrentTooltip()).AddTo(_compositeDisposable);
            PanelModel.CloseButton.Subscribe(state => HideCurrentTooltip()).AddTo(_compositeDisposable);

            PanelModel.TempBalancePanel.Subscribe( tempView =>
            {
                CurrentBalancePanelView = tempView != null ? tempView : View;
            });
            
            PanelModel.LevelModelActive.Subscribe(isActive =>
            {
                CurrentBalancePanelView.LevelPanel.gameObject.SetActive(isActive);
                CurrentBalancePanelView.LevelButton.interactable = isActive;
                CurrentBalancePanelView.ProfileTooltipPlaceholder.gameObject.SetActive(isActive);
            });
            
            PanelModel.HappinessModelActive.Subscribe(isActive =>
            {
                return;
                //TODO: remove because of GD
                CurrentBalancePanelView.HappinessPanel.gameObject.SetActive(isActive);
                CurrentBalancePanelView.HappinessTooltipButton.interactable = isActive;
                CurrentBalancePanelView.HappinessTooltipPlaceholder.gameObject.SetActive(isActive);
            }); 
            
            PanelModel.PopulationHappinessModelActive.Subscribe(isActive =>
            {
                CurrentBalancePanelView.PopulationHappinessPanel.gameObject.SetActive(isActive);
            });

            PanelModel.WarehouseModelActive.Subscribe(isActive =>
            {
                CurrentBalancePanelView.WarehousePanel.gameObject.SetActive(isActive);
                CurrentBalancePanelView.WarehouseButton.interactable = isActive;
                CurrentBalancePanelView.WarehouseTooltipPlaceholder.gameObject.SetActive(isActive);
            });

            PanelModel.FoodSupplyPanelActive.Subscribe(isActive =>
            {
                CurrentBalancePanelView.FoodSupplyTransform.gameObject.SetActive(isActive);
                CurrentBalancePanelView.FoodSupplyButton.interactable = isActive;
                CurrentBalancePanelView.FoodSupplyTooltipPlaceholder.gameObject.SetActive(isActive);
            });
            
            PanelModel.BalanceModelActive.Subscribe(value =>
            {
                CurrentBalancePanelView.BalancePanel.gameObject.SetActive(value);
            });
            
            SubscribeSignals();

            UpdateView();
            View.Init(PanelModel);
        }
        
        protected override void OnHidden()
        {
            base.OnHidden();
            UnsubscribeSignals();
            UnsubscribeButtons();
        }
        
        private void SubscribeSignals()
        {
            jSignalHardCurrencyUpdate.AddListener(UpdateHard);
            jSignalOnHardCurrencyStateChanged.AddListener(UpdateHard);
            jSignalSoftCurrencyUpdate.AddListener(UpdateSoft);
            jSignalOnSoftCurrencyStateChanged.AddListener(UpdateSoft);
            jSignalGoldCurrencyUpdate.AddListener(UpdateGold);
            jSignalOnGoldCurrencyStateChanged.AddListener(UpdateGold);
            jUserLevelUpSignal.AddListener(UpdateUserLevel);
            jSignalUpdateHappiness.AddListener(OnHappinessChange);
            jSignalWarehouseProductsAmountChanged.AddListener(OnWarehouseProductCountChange);
            jSignalOnCityPopulationUpdated.AddListener(OnPopulationChange);
            jSignalExperienceUpdate.AddListener(ExperienceUpdate);
            jSignalBalancePanelViewChangeState.AddListener(ViewChangeState);
            jHideTooltipSignal.AddListener(HideCurrentTooltip);
            jOnHudButtonsIsClickableSignal.AddListener(SetButtonsClickable);
            PanelContext.jFoodSupplyService.FoodSupplyAmountChanged += OnFoodSupplyChanged;
        }

        private void UnsubscribeSignals()
        {
            jSignalWarehouseProductsAmountChanged.RemoveListener(OnWarehouseProductCountChange);
            jSignalUpdateHappiness.RemoveListener(OnHappinessChange);
            jSignalOnCityPopulationUpdated.RemoveListener(OnPopulationChange);
            jSignalBalancePanelViewChangeState.RemoveListener(ViewChangeState);
            jSignalHardCurrencyUpdate.RemoveListener(UpdateHard);
            jSignalSoftCurrencyUpdate.RemoveListener(UpdateSoft);
            jSignalExperienceUpdate.RemoveListener(ExperienceUpdate);
            jUserLevelUpSignal.RemoveListener(UpdateUserLevel);
            jHideTooltipSignal.RemoveListener(HideCurrentTooltip);
            jOnHudButtonsIsClickableSignal.RemoveListener(SetButtonsClickable);
            PanelContext.jFoodSupplyService.FoodSupplyAmountChanged -= OnFoodSupplyChanged;
        }

        private void UnsubscribeButtons()
        {
            _compositeDisposable.Dispose();
        }

        private void UpdateView()
        {
            UpdateSoft();
            UpdateHard();
            UpdateGold();
            OnPopulationChange();
            OnFoodSupplyChanged(PanelContext.FoodSupplyAmount);
            OnHappinessChange();
            OnWarehouseProductCountChange();
            UpdateUserLevel();
            ExperienceUpdate();
        }

        private void ViewChangeState(bool state)
        {
            View.CanvasGroup.alpha = state ? 1 : 0;
            View.CanvasGroup.interactable = state;
        }

        #endregion

        #region Animations

        public void OnCurrencyBuyButtonClick(BankTabType tabType)
        {
            jRoadConstructSetActiveSignal.Dispatch(false);
            jSignalPlayerOpenedPanel.Dispatch();
            jShowWindowSignal.Dispatch(typeof(BankView), tabType);
        }

        private void OnWarehouseButtonClick() 
        {
            HideCurrentTooltip();
            jShowWindowSignal.Dispatch(typeof(WarehouseView), null);
        }
        #endregion
 
        #region ReactiveMethods

        private void UpdateSoft(int _ )
        {
            UpdateSoft();
        }   
        private void UpdateSoft()
        {
            PanelModel.SoftCurrency.Value = PanelContext.SoftCurrency;
        }

        private void UpdateHard(int _ )
        {
            UpdateHard();
        } 
        
        private void UpdateHard()
        {
            PanelModel.HardCurrency.Value = PanelContext.HardCurrency;
        }
 
        private void UpdateGold(int _ )
        {
            UpdateGold();
        }  
        
        private void UpdateGold()
        {
            PanelModel.GoldCurrency.Value = PanelContext.GoldCurrency;
        }

        private void UpdateUserLevel()
        {
            PanelModel.UserLevel.Value = PanelContext.CurrentLevel;
            PanelModel.GoldBarAvailable.Value = PanelContext.GoldBarAvailable;
            PanelModel.FoodSupplyPanelActive.SetValueAndForceNotify(PanelContext.jFoodSupplyService.IsAvailable);
            ExperienceUpdate();
        }
        
        private void  OnHappinessChange()
        {
            PanelModel.HappinessValue.Value = jPlayerProfile.UserCity.Happiness;
        }

        private void OnWarehouseProductCountChange()
        {
            PanelModel.WarehouseTextValue.Value = $"{PanelContext.ProductsCount}/{PanelContext.MaxWarehouseCapacity}";
            PanelModel.WarehouseProgressBar.Value = Mathf.Clamp01(PanelContext.ProductsCount / PanelContext.MaxWarehouseCapacity);
        }

        private void OnPopulationChange()
        {
            PanelModel.PopulationTextValue.Value = $"{PanelContext.TotalPopulation}";
        }

        private void OnFoodSupplyChanged(float amount)
        {
            PanelModel.FoodSupplyValue.Value = amount;
        }
        
        private void ExperienceUpdate()
        {
            PanelModel.ExperienceСorrelation.Value = (float)PanelContext.CurrentExp / PanelContext.NextExp;
        }
        
        #endregion

        #region Tooltips

        private void ShowHappinessTooltip()
        {
            DelayedShowTooltip(CurrentBalancePanelView.HappinessTooltip);
        }

        private void ShowPopulationPopup()
        {
            DelayedShowTooltip(CurrentBalancePanelView.PopulationTooltip);
        }
        
        private void ShowProfileTooltip()
        {
            DelayedShowTooltip(CurrentBalancePanelView.ProfileTooltip);
        }
        
        private void ShowFoodSupplyTooltip()
        {
            var foodSupplyPanelType = typeof(FoodSupplyPanelView);
            if (!jUIManager.IsViewVisible(foodSupplyPanelType))
            {
                var foodSupplyBuilding = jGameManager.FindMapObjectByType<FoodSupplyBuilding>(MapObjectintTypes.FoodSupply);
                if (foodSupplyBuilding != null)
                {
                    if (foodSupplyBuilding.IsFunctional())
                    {
                        jShowWindowSignal.Dispatch(typeof(FoodSupplyPanelView), foodSupplyBuilding);
                    }
                    else
                    {
                        jGameCamera.FocusOnMapObject(foodSupplyBuilding);
                    }

                }
            }
            else
            {
                DelayedShowTooltip(CurrentBalancePanelView.FoodSupplyTooltip);
            }
        }

        private void DelayedShowTooltip(UIPanelView tooltipPanel)
        {
            StartCoroutine(Wait());

            IEnumerator Wait()
            {
                bool samePanel = _activeTooltipPanel == tooltipPanel;
                HideCurrentTooltip();

                if (!samePanel)
                {
                    yield return new WaitForEndOfFrame();
                    _activeTooltipPanel = tooltipPanel;
                    _activeTooltipPanel.gameObject.SetActive(true);
                    CurrentBalancePanelView.OverlayBalanceButton.gameObject.SetActive(true);
                    _activeTooltipPanel.Show();
                }
            }
        }

        private void HideCurrentTooltip()
        {
            if (_activeTooltipPanel != null)
            {
                _activeTooltipPanel.Hide();
                _activeTooltipPanel.gameObject.SetActive(false);
                CurrentBalancePanelView.OverlayBalanceButton.gameObject.SetActive(false);
                _activeTooltipPanel = null;
            }
        }
        #endregion

        private void SetButtonsClickable(bool isClickable)
        {
            CurrentBalancePanelView.SetButtonsClickable(isClickable);
        }
       
        public void OnChanged()
        {
            
        }
    }
}
