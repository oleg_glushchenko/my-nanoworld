using NanoReality.Engine.UI.Views.Panels;
using UniRx;

namespace NanoReality.UI.Components
{
    public sealed class BalancePanelModel 
    {
        public ReactiveCommand SoftClick { get; set; } =  new ReactiveCommand();
        public ReactiveCommand HardClick { get; set; } = new ReactiveCommand();
        public ReactiveCommand InfoPanelClick { get; set; } = new ReactiveCommand();
        public ReactiveCommand WarehouseClick { get; set; } = new ReactiveCommand();
        public ReactiveCommand HappyClick { get; set; } = new ReactiveCommand();
        public ReactiveCommand OverlayClick { get; set; } = new ReactiveCommand();
        public ReactiveCommand FoodSupplyClick { get; set; } = new ReactiveCommand();
        public ReactiveCommand CloseButton { get; set; } = new ReactiveCommand();

        public ReactiveProperty<int> SoftCurrency = new ReactiveProperty<int>();
        public ReactiveProperty<int> HardCurrency = new ReactiveProperty<int>();
        public ReactiveProperty<int> GoldCurrency = new ReactiveProperty<int>();
        
        public ReactiveProperty<int> UserLevel = new ReactiveProperty<int>();
        public ReactiveProperty<float> ExperienceСorrelation = new ReactiveProperty<float>();
        
        public ReactiveProperty<int> HappinessValue = new ReactiveProperty<int>();
        public ReactiveProperty<string> WarehouseTextValue = new ReactiveProperty<string>();
        public ReactiveProperty<string> PopulationTextValue = new ReactiveProperty<string>();
        public ReactiveProperty<float> WarehouseProgressBar = new ReactiveProperty<float>();
        public ReactiveProperty<float> FoodSupplyValue = new ReactiveProperty<float>();

        public ReactiveProperty<BalancePanelView> TempBalancePanel = new ReactiveProperty<BalancePanelView>();
      
        public ReactiveProperty<bool> LevelModelActive  = new ReactiveProperty<bool>(true);
        public ReactiveProperty<bool> HappinessModelActive  = new ReactiveProperty<bool>(true);
        public ReactiveProperty<bool> PopulationHappinessModelActive  = new ReactiveProperty<bool>(true);
        public ReactiveProperty<bool> WarehouseModelActive  = new ReactiveProperty<bool>(true);
        public ReactiveProperty<bool> BalanceModelActive  = new ReactiveProperty<bool>(true);
        public ReactiveProperty<bool> GoldBarAvailable = new ReactiveProperty<bool>(true);

        /// <summary>
        /// Is a food supply progress bar panel visible.
        /// </summary>
        public ReactiveProperty<bool> FoodSupplyPanelActive  = new ReactiveProperty<bool>(false);
    }
}