using System;
using Engine.UI;
using NanoReality.Game.FoodSupply;
using NanoReality.Game.Services.BuildingsUnlockService;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.GameManager.Models.api;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using UniRx;
using UnityEngine;

namespace NanoReality.Game.UI.BalancePanel
{
    public sealed class BalancePanelContext
    {
        [Inject] public IPlayerProfile jPlayerProfile { get; set; }
        [Inject] public IGameManager jGameManager { get; set; }
        [Inject] public IFoodSupplyService jFoodSupplyService { get; set; }
        [Inject] public IPlayerExperience jPlayerExperience { get; set; }
        [Inject] public IBuildingsUnlockService jBuildingsUnlockService { get; set; }
        
        public int SoftCurrency => jPlayerProfile.PlayerResources.SoftCurrency;

        public int HardCurrency => jPlayerProfile.PlayerResources.HardCurrency;

        public int GoldCurrency => jPlayerProfile.PlayerResources.GoldCurrency;

        public int NanoGems => jPlayerProfile.PlayerResources.NanoGems;
        public float FoodSupplyAmount => jFoodSupplyService.ProgressBarAmount;

        public int CurrentLevel => jPlayerExperience.CurrentLevel + 1;

        public bool GoldBarAvailable => jPlayerProfile.CurrentLevelData.CurrentLevel >=
                                        Math.Min(jBuildingsUnlockService.GetBuildingUnlockLevel(OrderDeskId), jBuildingsUnlockService.GetBuildingUnlockLevel(TradingShopId));

        public bool MaxLevelReached => jPlayerExperience.ObjectUserLevelsesBalanceData.Levels.Count <=
                                       jPlayerExperience.CurrentLevel;

        public int CurrentExp => jPlayerProfile.CurrentLevelData.CurrentExperience;

        public int NextExp => MaxLevelReached ? 
            jPlayerExperience.ObjectUserLevelsesBalanceData.Levels[jPlayerExperience.CurrentLevel-1].Experience :
            jPlayerExperience.ObjectUserLevelsesBalanceData.Levels[jPlayerExperience.CurrentLevel].Experience;

        public float MaxWarehouseCapacity => jPlayerProfile.PlayerResources.WarehouseCapacity;
        public float ProductsCount => jPlayerProfile.PlayerResources.ProductsCount;

        public float TotalPopulation => CityHall.TotalPopulation;
        public float MaxPopulation => CityHall.MaxPopulation;

        private ICityHall CityHall => _cityHall ?? (_cityHall = jGameManager.FindMapObjectByType<ICityHall>(MapObjectintTypes.CityHall));

        private CityCountersSprites _sprites;
        private ICityHall _cityHall;

        private const int OrderDeskId = 135;
        private const int TradingShopId = 156;
        
    }
}