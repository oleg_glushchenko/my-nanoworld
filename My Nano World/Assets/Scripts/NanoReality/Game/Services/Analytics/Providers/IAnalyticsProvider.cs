﻿using Assets.Scripts.Engine.Analytics.Model;

namespace NanoReality.Game.Analytics
{
    public interface IAnalyticsProvider
    {
        bool Initialized { get; }
        
        IAnalyticsProvider Initialize(string userId, bool isActive = true);

        void LogEvent(string name);
        void LogEvent(string name, string paramName, object paramValue);
        void LogEvent(CustomEventData customEventData);
    }
}
