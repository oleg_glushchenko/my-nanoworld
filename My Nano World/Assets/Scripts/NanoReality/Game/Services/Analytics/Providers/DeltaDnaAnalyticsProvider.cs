﻿using Assets.Scripts.Engine.Analytics.Model;
using DeltaDNA;
using NanoLib.Core.Logging;
using UnityEngine;

namespace NanoReality.Game.Analytics
{
    public class DeltaDnaAnalyticsProvider : IAnalyticsProvider
    {
        public bool Initialized { get; private set; }

        public IAnalyticsProvider Initialize(string userId, bool isActive = true)
        {
            if (Initialized)
            {
                Logging.LogError(LoggingChannel.Analytics, "DeltaDnaAnalyticsProvider is already initialized.");
                return this;
            }

            var imageMessageHandler = new ImageMessageHandler(DDNA.Instance, imageMessage => { imageMessage.Show(); });
            var gameParametersHandler = new GameParametersHandler(gameParameters => { });

            DDNA.Instance.Settings.DefaultImageMessageHandler = imageMessageHandler;
            DDNA.Instance.Settings.DefaultGameParameterHandler = gameParametersHandler;
            DDNA.Instance.ClientVersion = Application.version;
            DDNA.Instance.StartSDK(userId);

            Initialized = true;
            return this;
        }

        public void LogEvent(string name)
        {
            var optionsEvent = new GameEvent(name);
            DDNA.Instance.RecordEvent(optionsEvent);
        }

        public void LogEvent(string name, string paramName, object paramValue)
        {
            var optionsEvent = new GameEvent(name).AddParam(paramName, paramValue);

            DDNA.Instance.RecordEvent(optionsEvent);
        }

        public void LogEvent(CustomEventData customEventData)
        {
            var optionsEvent = new GameEvent(customEventData.EventName);

            foreach (var param in customEventData.Params)
            {
                optionsEvent.AddParam(param.Key, param.Value);
            }

            DDNA.Instance.RecordEvent(optionsEvent);
        }
    }
}