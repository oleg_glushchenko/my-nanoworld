namespace NanoReality.Game.Services
{
    public interface IGameService
    {
        void Init();

        void Dispose();
    }
}