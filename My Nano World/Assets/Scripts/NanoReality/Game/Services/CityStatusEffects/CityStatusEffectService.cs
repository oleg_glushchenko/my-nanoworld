﻿using System;
using System.Collections.Generic;
using Assets.NanoLib.UI.Core.Signals;
using NanoReality.Game.FoodSupply;
using NanoReality.Game.Services;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using UnityEngine;

namespace NanoReality.Game.CityStatusEffects
{
    public class CityStatusEffectService : ICityStatusEffectService
    {
        [Inject] public IServerCommunicator jServerCommunicator { get; set; }
        [Inject] public IFoodSupplyService jFoodSupplyService { get; set; }
        [Inject] public UserLevelUpSignal jUserLevelUpSignal { get; set; }
        
        private readonly Dictionary<int, CityStatusEffectSettings> _statusEffectBalanceDataList = new Dictionary<int, CityStatusEffectSettings>();
        
        public IEnumerable<CityStatusEffect> ActiveStatusEffects { get; private set; }
        
        public event Action StatusEffectsChanged;
        
        public void Init()
        {
            jServerCommunicator.GetStatusEffectsBalanceData((statusEffectsSettings) =>
            {
                _statusEffectBalanceDataList.Clear();
                foreach (CityStatusEffectSettings statusEffectData in statusEffectsSettings)
                {
                    _statusEffectBalanceDataList.Add(statusEffectData.Id, statusEffectData);
                }
                
                LoadActiveStatusEffects();
            });
            jFoodSupplyService.FoodSupplyCityInfoChanged += CityInfoChanged;
            jUserLevelUpSignal.AddListener(OnUserLevelUp);
        }

        public void Dispose()
        {
            jFoodSupplyService.FoodSupplyCityInfoChanged -= CityInfoChanged;
            jUserLevelUpSignal.RemoveListener(OnUserLevelUp);
        }

        private void OnUserLevelUp()
        {
            if(jFoodSupplyService.IsAvailable)
                LoadActiveStatusEffects();
        }

        /// <summary>
        /// Loads status effects from server.
        /// </summary>
        /// <param name="onComplete">Callback.</param>
        public void LoadActiveStatusEffects(Action onComplete = null)
        {
            jServerCommunicator.GetCityStatusEffects((statusEffectsList) =>
            {
                OnActiveStatusEffectsLoaded(statusEffectsList);
                onComplete?.Invoke();
            });
        }
        
        public CityStatusEffectSettings GetStatusEffectSettings(int effectId)
        {
            if (!_statusEffectBalanceDataList.TryGetValue(effectId, out CityStatusEffectSettings statusEffectData))
            {
                Debug.LogWarning($"Status effect balance data with id [{effectId}] not found.");
                return null;
            }

            return statusEffectData;
        }
        
        private void CityInfoChanged()
        {
            LoadActiveStatusEffects();
        }

        private void OnActiveStatusEffectsLoaded(CityStatusEffect[] statusEffectsList)
        {
            ActiveStatusEffects = statusEffectsList;
            StatusEffectsChanged?.Invoke();
        }
    }

    public interface ICityStatusEffectService : IGameService
    {
        event Action StatusEffectsChanged;
        
        IEnumerable<CityStatusEffect> ActiveStatusEffects { get; }
        
        /// <summary>
        /// Return status effect balance data by id.
        /// </summary>
        /// <param name="effectId">Database Status Effect Id.</param>
        /// <returns></returns>
        CityStatusEffectSettings GetStatusEffectSettings(int effectId);

        void LoadActiveStatusEffects(Action onComplete = null);
    }
}