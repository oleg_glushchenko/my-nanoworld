using Newtonsoft.Json;

namespace NanoReality.Game.CityStatusEffects
{
    /// <summary>
    /// Status effect balance data that configured from admin panel.
    /// </summary>
    public sealed class CityStatusEffectSettings
    {
        /// <summary>
        /// Database effect id.
        /// </summary>
        [JsonProperty("id")]
        public int Id;

        /// <summary>
        /// Type of effect.
        /// </summary>
        [JsonProperty("effect_type")]
        public StatusEffect EffectType;

        /// <summary>
        /// Type of influence.
        /// Increase, decrease or just ability for something.
        /// </summary>
        [JsonProperty("influence_type")]
        public StatusEffectInfluence EffectInfluence;

        /// <summary>
        /// How much effect affect to some city parameter.
        /// Empty for StatusEffect.Ability type.
        /// </summary>
        [JsonProperty("value")]
        public float EffectValue;
    }
    
    public enum StatusEffectInfluence
    {
        None = 0,
        Increase = 1,
        Decrease = 2,
        Ability = 3
    }

    public enum StatusEffect
    {
        None = 0,
        ProductionSpeed = 1,
        Happiness = 2,
        CitizenGift = 3
    }
}