﻿using System.Collections.Generic;
using System.Linq;
using NanoLib.Core.Logging;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.GameManager.Models.api;
using UnityEngine;

namespace NanoReality.Game.Services.BuildingsUnlockService
{
    public sealed class BuildingsUnlockService : IBuildingsUnlockService
    {
        // BuildingId, InstanceIndex, InstanceLevel, BuildingLevelData for target instance.
        private Dictionary<int, Dictionary<int, Dictionary<int, BuildingLevelData>>> _cachedInstancesUnlocks;

        /// <summary>
        /// Displays, on which player level, which buildings will be unlocked.
        /// Key - User Level.
        /// Value - List of new for this level buildings.
        /// </summary>
        private Dictionary<int, List<int>> _availableByLevelBuildings;
        private int CurrentPlayerLevel => jPlayerExperience.CurrentLevel;
        
        [Inject] public IPlayerExperience jPlayerExperience { get; set; }
        [Inject] public IGameManager jGameManager { get; set; }

        private const int FIRST_INSTANCE_INDEX = 1;
        private const int FIRST_BUILDING_LEVEL = 0;
        private const int NOT_VALID_INSTANCE_INDEX = int.MaxValue;
        
        public void Init(List<BuildingInstanceUnlockInfo> instancesUnlocksList)
        {
            _cachedInstancesUnlocks = new Dictionary<int, Dictionary<int, Dictionary<int, BuildingLevelData>>>(instancesUnlocksList.Count);
            _availableByLevelBuildings = new Dictionary<int, List<int>>(instancesUnlocksList.Count);
            
            foreach (BuildingInstanceUnlockInfo instanceUnlockInfo in instancesUnlocksList)
            {
                var buildingId = instanceUnlockInfo.BuildingId;
                var buildingInstances = instanceUnlockInfo.InstancesList;

                var instancesDictionary = new Dictionary<int, Dictionary<int, BuildingLevelData>>(buildingInstances.Count);
                _cachedInstancesUnlocks.Add(buildingId, instancesDictionary);

                foreach (List<BuildingLevelData> instanceLevelsList in buildingInstances)
                {
                    var levelsDictionary = new Dictionary<int, BuildingLevelData>(instanceLevelsList.Count);
                    var buildingInstanceIndex = NOT_VALID_INSTANCE_INDEX;
                    
                    foreach (BuildingLevelData instanceLevelData in instanceLevelsList)
                    {
                        var dataInstanceIndex = instanceLevelData.InstanceIndex;
                        var instanceBuildingLevel = instanceLevelData.BuildingLevel;
                        
                        if (buildingInstanceIndex == NOT_VALID_INSTANCE_INDEX)
                        {
                            buildingInstanceIndex = dataInstanceIndex;
                        }

                        if (levelsDictionary.ContainsKey(instanceBuildingLevel))
                        {
                            $"Building [{buildingId}] already contains config for level {instanceBuildingLevel}".LogError();
                            continue;
                        }
                        levelsDictionary.Add(instanceBuildingLevel, instanceLevelData);
                    }
                    
                    instancesDictionary.Add(buildingInstanceIndex, levelsDictionary);
                }
            }

            foreach (var buildingInstancesUnlock in _cachedInstancesUnlocks)
            {
                var buildingId = buildingInstancesUnlock.Key;
                if (!buildingInstancesUnlock.Value.TryGetValue(FIRST_INSTANCE_INDEX, out var instanceLevels))
                {
                    $"Building not contains first instance config {buildingId}".LogError();
                    continue;
                }

                if (!instanceLevels.TryGetValue(FIRST_BUILDING_LEVEL, out var firstLevelData))
                {
                    $"Building [{buildingId}] config for first level of first instance not found".LogError();
                    continue;
                }

                var requiredPlayerLevel = firstLevelData.PlayerLevel;

                if (!_availableByLevelBuildings.ContainsKey(requiredPlayerLevel))
                {
                    _availableByLevelBuildings.Add(requiredPlayerLevel, new List<int>(10));
                }
                _availableByLevelBuildings[requiredPlayerLevel].Add(buildingId);
            }
        }
        
        public int GetPlayerLevelForUpgrade(int buildingId, int instanceIndex, int currentBuildingLevel)
        {
            var instanceLevels = GetInstanceLevels(buildingId, instanceIndex);
            if (instanceLevels.Count == 0)
            {
                return int.MaxValue;
            }

            var nextBuildingLevel = currentBuildingLevel + 1;
            if (!instanceLevels.TryGetValue(nextBuildingLevel, out var nextLevelData))
            {
                return int.MaxValue;
            }

            return nextLevelData.PlayerLevel;
        }

        public int GetBuildingUnlockLevel(int buildingId)
        {
            var instancesCollection = GetInstancesOfBuilding(buildingId);
            if (instancesCollection == null)
            {
                return int.MaxValue;
            }

            if (!instancesCollection.TryGetValue(FIRST_INSTANCE_INDEX, out var buildingLevelsData))
            {
                return int.MaxValue;
            }

            if (!buildingLevelsData.TryGetValue(FIRST_BUILDING_LEVEL, out var firstLevelData))
            {
                return int.MaxValue;
            }
            
            return firstLevelData.PlayerLevel;
        }

        public IEnumerable<int> GetNewUnlockedBuildings(int playerLevel)
        {
            if (!_availableByLevelBuildings.TryGetValue(playerLevel, out List<int> newBuildingsIds))
            {
                return null;
            }
            
            return newBuildingsIds;
        }

        /// <summary>
        /// Returns maximum building instances count for current player level.
        /// </summary>
        /// <returns>Max instances count</returns>
        public int GetMaxAvailableInstancesCount(int buildingId, int playerLevel)
        {
            var instancesCollection = GetInstancesOfBuilding(buildingId);
            if(instancesCollection == null)
            {
                Logging.LogError(LoggingChannel.Core, $"Not found instance unlocks for building [{buildingId}]");
                return 0;
            }

            int unlockedInstancesCount = 0;
            
            foreach (KeyValuePair<int, Dictionary<int, BuildingLevelData>> instance in instancesCollection)
            {
                var instanceLevels = instance.Value;
                if (!instanceLevels.TryGetValue(FIRST_BUILDING_LEVEL, out var firstLevelData))
                {
                    Logging.LogError(LoggingChannel.Core,
                        $"Not found info for first level of Building. Id:[{buildingId}] InstanceIndex: {instance.Key}");
                    continue;
                }
                
                var requiredLevelForUnlock = firstLevelData.PlayerLevel;
                if (requiredLevelForUnlock <= playerLevel)
                {
                    unlockedInstancesCount++;
                }
            }
            
            return unlockedInstancesCount;
        }

        public Price GetBuildingInstancePrice(int buildingId, int instanceIndex)
        {
            var instancesCollection = GetInstancesOfBuilding(buildingId);
            if (!instancesCollection.TryGetValue(instanceIndex, out var instanceLevelsData))
            {
                return default;
            }
            
            return instanceLevelsData[FIRST_BUILDING_LEVEL].Price;
        }

        public Price GetNewBuildingInstancePrice(int buildingId)
        {
            int currentInstancesCount = jGameManager.GetMapObjectsCountInCity(buildingId);
            int nextInstanceIndex = currentInstancesCount + 1;

            return GetBuildingInstancePrice(buildingId, nextInstanceIndex);
        }

        public Price GetUpgradeBuildingPrice(int buildingId, int nextBuildingLevel, int instanceIndex)
        {
            var instanceLevelsCollection = GetInstanceLevels(buildingId, instanceIndex);
            if (!instanceLevelsCollection.TryGetValue(nextBuildingLevel, out BuildingLevelData upgradePrice))
            {
                return default;
            }

            return upgradePrice.Price;
        }

        public bool GetNextAvailableInstancesCount(int buildingId, out int nextAvailableCount, out int nextPlayerLevel)
        {
            var instancesCollection = GetInstancesOfBuilding(buildingId);
            if (instancesCollection == null)
            {
                $"Cant find price for building [{buildingId}].".LogError(LoggingChannel.Core);
                nextAvailableCount = 0;
                nextPlayerLevel = 0;
                return false;
            }

            var constructedInstancesCount = jGameManager.GetMapObjectsCountInCity(buildingId);
            var nextInstanceIndex = constructedInstancesCount + 1;

            if (!instancesCollection.TryGetValue(nextInstanceIndex, out var instanceLevels))
            {
                nextAvailableCount = 0;
                nextPlayerLevel = 0;
                return false;
            }

            if (!instanceLevels.TryGetValue(FIRST_BUILDING_LEVEL, out var instanceLevelData))
            {
                nextAvailableCount = 0;
                nextPlayerLevel = 0;
                $"First level for building:[{buildingId}] and instance:[{nextInstanceIndex}] not exists.".LogError();
                
                return false;
            }

            int nextCount = 1;
            int playerLevel = instanceLevelData.PlayerLevel;

            nextAvailableCount = nextCount;
            nextPlayerLevel = playerLevel;
            
            return jPlayerExperience.CurrentLevel < nextPlayerLevel;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="buildingsIdsList">Id's of map objects to check their available to build count.</param>
        /// <returns></returns>
        public int GetAvailableBuildingsCountFromList(List<int> buildingsIdsList)
        {
            var unlockedBuildings = new Dictionary<Id_IMapObjectType, int>(buildingsIdsList.Count);

            foreach (int buildingId in buildingsIdsList)
            {
                int totalAvailableInstances = GetAvailableBuildingInstancesCount(buildingId);
                unlockedBuildings.Add(buildingId, Mathf.Clamp(totalAvailableInstances, 0, int.MaxValue));
            }

            int totalCount = unlockedBuildings.Sum(c => c.Value);

            return totalCount;
        }

        public int GetAvailableBuildingInstancesCount(int buildingId)
        {
            var constructedInstancesCount = jGameManager.GetMapObjectsCountInCity(buildingId);
            var instancesCollection = GetInstancesOfBuilding(buildingId);

            if (instancesCollection == null || instancesCollection.Count == 0)
            {
                $"List of available instance for building {buildingId} in null or empty.".LogError();
                return 0;
            }
            
            if (instancesCollection.Count == constructedInstancesCount)
            {
                return 0;
            }

            var availableForConstructInstances = instancesCollection.Count(c =>
                c.Value[FIRST_BUILDING_LEVEL].PlayerLevel <= CurrentPlayerLevel);

            return availableForConstructInstances - constructedInstancesCount;
        }

        public int GetBuildingConstructionTime(int buildingId, int buildingLevel, int instanceIndex)
        {
            var instancesCollection = GetInstancesOfBuilding(buildingId);
            if (!instancesCollection.TryGetValue(instanceIndex, out var instanceLevels))
            {
                return int.MinValue;
            }

            if (!instanceLevels.TryGetValue(buildingLevel, out var firstLevelData))
            {
                return int.MinValue;
            }
            
            return firstLevelData.ConstructionTime;
        }

        public int GetExperienceReward(int buildingId, int buildingLevel, int instanceIndex)
        {
            var instancesCollection = GetInstancesOfBuilding(buildingId);
            if (!instancesCollection.TryGetValue(instanceIndex, out var instanceLevels))
            {
                return default;
            }

            if (!instanceLevels.TryGetValue(buildingLevel, out var instanceLevelData))
            {
                return default;
            }
            
            return instanceLevelData.ExperienceReward;
        }

        public int GetPopulationForDwelling(int buildingId, int buildingLevel, int instanceIndex)
        {
            var instanceLevels = GetInstanceLevels(buildingId, instanceIndex);
            if (!instanceLevels.TryGetValue(buildingLevel, out var instanceLevelData))
            {
                $"Not found level [{buildingLevel}] for building [{buildingLevel}] instance [{instanceIndex}]".LogError();
                return int.MinValue;
            }

            return instanceLevelData.Population;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="buildingId">Building Type Id</param>
        /// <param name="nextBuildingLevel">Next Building Level</param>
        /// <param name="instanceIndex"></param>
        /// <returns></returns>
        public bool IsUpgradeAvailable(int buildingId, int nextBuildingLevel, int instanceIndex)
        {
            var instancesCollection = GetInstanceLevels(buildingId, instanceIndex);
            if (!instancesCollection.TryGetValue(nextBuildingLevel, out var levelData))
            {
                return false;
            }

            return jPlayerExperience.CurrentLevel >= levelData.PlayerLevel;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="targetBuilding">Inspected building model.</param>
        /// <returns></returns>
        public bool IsUpgradeAvailable(IMapObject targetBuilding)
        {
            var buildingId = targetBuilding.ObjectTypeID;
            var nextLevel = targetBuilding.Level + 1;
            var instanceIndex = targetBuilding.InstanceIndex;

            return IsUpgradeAvailable(buildingId, nextLevel, instanceIndex);
        }

        private Dictionary<int, Dictionary<int, BuildingLevelData>> GetInstancesOfBuilding(int buildingId)
        {
            if(!_cachedInstancesUnlocks.TryGetValue(buildingId, out var instancesCollection))
            {
                Logging.LogError(LoggingChannel.Core, $"Not found instance unlocks for building [{buildingId}]");
                return null;
            }

            return instancesCollection;
        }

        private Dictionary<int, BuildingLevelData> GetInstanceLevels(int buildingId, int instanceIndex)
        {
            var instancesCollection = GetInstancesOfBuilding(buildingId);
            if (!instancesCollection.TryGetValue(instanceIndex, out var instanceLevelConfigs))
            {
                Logging.LogError(LoggingChannel.Core, $"Not found instance levels for buildingId:[{buildingId}] Instance:[{instanceIndex}]");
                return null;
            }

            return instanceLevelConfigs;
        }
    }
}