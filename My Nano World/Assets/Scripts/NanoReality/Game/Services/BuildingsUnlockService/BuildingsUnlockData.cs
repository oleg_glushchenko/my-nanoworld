﻿using System.Collections.Generic;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using Newtonsoft.Json;

namespace NanoReality.Game.Services.BuildingsUnlockService
{
    public sealed class BuildingInstanceData
    {
        [JsonProperty("instance_index", Required = Required.Always)]
        public int InstanceIndex;
        
        [JsonProperty("instance_levels", Required = Required.Always)]
        public List<BuildingLevelData> InstanceLevels;
    }

    public sealed class BuildingInstanceUnlockInfo
    {
        [JsonProperty("building_id", Required = Required.Always)]
        public int BuildingId;

        [JsonProperty("unlock_instances_conditions", Required = Required.Always)]
        public List<List<BuildingLevelData>> InstancesList;
    }
    
    /// <summary>
    /// Describe parameters for level of some building instance.
    /// Note: Building with same BuildingId can have different parameters for different instances of building.
    /// </summary>
    public sealed class BuildingLevelData
    {
        [JsonProperty("instance_index", Required = Required.Always)]
        public int InstanceIndex;
            
        [JsonProperty("player_level", Required = Required.Always)]
        public int PlayerLevel;

        [JsonProperty("building_level", Required = Required.Always)]
        public int BuildingLevel;
            
        [JsonProperty("price", Required = Required.Always)]
        public Price Price;
        
        [JsonProperty("construction_time", Required = Required.Always)]
        public int ConstructionTime;
        
        [JsonProperty("experience", Required = Required.Always)]
        public int ExperienceReward;

        /// <summary>
        /// Population for current level of building instance.
        /// Note: it uses only for dwelling buildings.
        /// </summary>
        [JsonProperty("population", Required = Required.Always)]
        public int Population;
    }
}