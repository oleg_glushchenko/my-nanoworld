using Newtonsoft.Json;

namespace NanoReality.Game.CitizenGifts
{
    public sealed class CitizenGift
    {
        /// <summary>
        /// Unique gift instance id.
        /// </summary>
        [JsonProperty("gift_id")]
        public int Id;
        
        [JsonProperty("map_building_id")]
        public int BuildingId;
        
        [JsonProperty("gift")]
        public IAwardItem Reward;

        public override bool Equals(object obj)
        {
            var castedObj = (CitizenGift) obj;
            if (castedObj == null)
            {
                return false;
            }

            return Id == castedObj.Id && BuildingId == castedObj.BuildingId && Equals(Reward, castedObj.Reward);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = Id;
                hashCode = (hashCode * 397) ^ BuildingId;
                hashCode = (hashCode * 397) ^ (Reward != null ? Reward.GetHashCode() : 0);
                return hashCode;
            }
        }
    }
}