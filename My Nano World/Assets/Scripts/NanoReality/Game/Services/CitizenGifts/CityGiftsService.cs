using System;
using System.Collections.Generic;
using System.Linq;
using Assets.NanoLib.UtilitesEngine;
using NanoLib.Core.Logging;
using NanoReality.Game.CityStatusEffects;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.GameManager.Models.api;
using NanoReality.GameLogic.ServerCommunication.Models.Api;

namespace NanoReality.Game.CitizenGifts
{
    public sealed class CityGiftsService : ICityGiftsService
    {
        [Inject] public IGameManager jGameManager { get; set; }
        [Inject] public ITimerManager jTimerManager { get; set; }
        [Inject] public IServerCommunicator jServerCommunicator { get; set; }
        [Inject] public ICityStatusEffectService jCityStatusEffectService { get; set; }
        
        private readonly Dictionary<int, IMapBuilding> _buildingsWithGifts = new Dictionary<int, IMapBuilding>();
        private readonly List<CitizenGift> _availableGifts = new List<CitizenGift>();
        private readonly List<int> _collectedGifts = new List<int>();
        private ITimer _updateGiftsTimer;
        
        public event Action GiftsUpdates;

        public bool IsAnyGiftsAvailable => _availableGifts.Count > 0;

        public void Init()
        {
            jCityStatusEffectService.StatusEffectsChanged += OnStatusEffectsChanged;
        }
        
        public void Dispose()
        {
            jCityStatusEffectService.StatusEffectsChanged -= OnStatusEffectsChanged;
            
            _buildingsWithGifts.Clear();
            _availableGifts.Clear();
            _collectedGifts.Clear();
            
            _updateGiftsTimer?.CancelTimer();
            _updateGiftsTimer = null;
        }

        public bool HasAvailableGifts(int buildingMapId)
        {
            return _buildingsWithGifts.ContainsKey(buildingMapId);
        }

        public IEnumerable<CitizenGift> GetGifts(int buildingMapId)
        {
            return HasAvailableGifts(buildingMapId) ? _buildingsWithGifts[buildingMapId].AvailableGifts : null;
        }
        
        public IMapBuilding GetBuildWithGift()
        {
            return _buildingsWithGifts.LastOrDefault().Value;
        }

        public void ClaimGift(int buildingMapId, int giftId, Action onSuccess = null)
        {
            if (!HasGift(buildingMapId, giftId) || _collectedGifts.Contains(giftId))
            {
                $"Gift {giftId} on building {buildingMapId} is already claimed!".Log(LoggingChannel.Mechanics);
                return;
            }
            
            _collectedGifts.Add(giftId);

            RemoveGiftFromBuilding(buildingMapId, giftId);
            
            jServerCommunicator.ClaimCitizenGift(giftId, () =>
            {
                GiftsUpdates?.Invoke();
                onSuccess?.Invoke();
            });
        }

        private void OnStatusEffectsChanged()
        {
            LoadAvailableGifts(null);
        }

        private void AddGiftToBuilding(int buildingMapId, CitizenGift gift)
        {
            if (HasGift(buildingMapId, gift.Id))
            {
                return;
            }

            if (!_buildingsWithGifts.TryGetValue(buildingMapId, out var building))
            {
                building = jGameManager.FindMapObjectById<IMapBuilding>(buildingMapId);
                _buildingsWithGifts.Add(buildingMapId, building);
            }
            
            if (building.AvailableGifts == null)
            {
                building.AvailableGifts = new List<CitizenGift>();
            }
            
            building.AvailableGifts.Add(gift);
            _availableGifts.Add(gift);
        }

        private void RemoveGiftFromBuilding(int buildingMapId, int giftId)
        {
            if (!_buildingsWithGifts.TryGetValue(buildingMapId, out var building))
            {
                $"Can't remove gift from building [{buildingMapId}]. It not contains in building with gifts list.".LogError(LoggingChannel.Mechanics);
                return;
            }
            
            var gift = building.AvailableGifts.FirstOrDefault(availableGift => availableGift.Id == giftId);
            if (gift == null) return;
            
            building.AvailableGifts.Remove(gift);
            if (building.AvailableGifts.Count == 0)
            {
                _buildingsWithGifts.Remove(buildingMapId);
            }
            
            _availableGifts.Remove(gift);
        }

        public void LoadAvailableGifts(Action onComplete)
        {
            onComplete?.Invoke();
            
            void OnGiftsLoaded(CitizenGift[] newGiftsList)
            {
                var oldGifts = _availableGifts;
                foreach (var citizenGift in newGiftsList)
                {
                    if (!oldGifts.Contains(citizenGift) && !_collectedGifts.Contains(citizenGift.Id))
                    {
                        AddGiftToBuilding(citizenGift.BuildingId, citizenGift);
                    }
                }

                GiftsUpdates?.Invoke();
            }
            
            jServerCommunicator.LoadAvailableCitizensGifts(OnGiftsLoaded);
        }
        
        private bool HasGift(int buildingMapId, int giftId)
        {
            var building = _buildingsWithGifts.ContainsKey(buildingMapId) ? _buildingsWithGifts[buildingMapId] : null;
            var gift = building?.AvailableGifts?.FirstOrDefault(availableGift => availableGift.Id == giftId);
            
            return gift != null;
        }
    }
}