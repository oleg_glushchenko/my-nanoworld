using Newtonsoft.Json;

namespace NanoReality.Game.FoodSupply
{
    /// <summary>
    /// Settings for every food supply time zone interval.
    /// </summary>
    public sealed class FoodSupplyZoneSettings
    {
        /// <summary>
        /// Zone order index on timeline.
        /// </summary>
        [JsonProperty("zone_index")]
        public int ZoneIndex;
        
        /// <summary>
        /// Database ids list of effect that affect to user city in this time zone.
        /// </summary>
        [JsonProperty("effects")]
        public int[] CityStatusEffectsIds;

        public override bool Equals(object obj)
        {
            return obj is FoodSupplyZoneSettings other 
                   && ZoneIndex == other.ZoneIndex
                   && Equals(CityStatusEffectsIds, other.CityStatusEffectsIds);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = ZoneIndex;
                hashCode = (hashCode * 397) ^ (CityStatusEffectsIds != null ? CityStatusEffectsIds.GetHashCode() : 0);
                return hashCode;
            }
        }
    }
}