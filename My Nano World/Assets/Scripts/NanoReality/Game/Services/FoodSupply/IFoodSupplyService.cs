using System;
using System.Collections.Generic;
using NanoReality.Game.Services;
using NanoReality.GameLogic.Data;

namespace NanoReality.Game.FoodSupply
{
    public interface IFoodSupplyService : IGameService
    {
        bool IsAvailable { get; }
        bool IsDataLoaded { get; }
        bool IsFilled { get; }
        int CurrentCalories { get; }
        int MaxAvailableCalories { get; }
        long StatusEffectEndTimeStamp { get; }
        int StatusEffectTotalTime { get; }
        float CoinsPerCalorie { get; }
        float ProgressBarAmount { get; }

        event Action FoodSupplyCityInfoChanged;
        event Action<bool> StatusEffectActivated; 

        /// <summary>
        /// Value - cooked product id.
        /// </summary>
        event Action<int> FoodSupplyProductCooked;
        
        /// <summary>
        /// Calls when the bar is full
        /// </summary>
        event Action FoodSupplyBarFilled;
        
        /// <summary>
        /// Value - amount of food supply [0..1].
        /// </summary>
        event Action<float> FoodSupplyAmountChanged;

        void CookFoodSupplyProduct(int buildingId, int foodSupplyRecipeId, Action onComplete = null);
        void FeedCitizens(int buildingId, Dictionary<int, int> productsInSlots, Action onSuccess);
        void UnlockFoodSupplySlot(int buildingId, int slotId, Action onComplete);

        /// <summary>
        /// Returns balance data of food supply product.
        /// </summary>
        /// <param name="productId">Product Id</param>
        FoodSupplyProduct GetProduct(int productId);

        /// <summary>
        /// Returns percents of the food bar fill and associated additional rewards data for each percent value.
        /// </summary>
        Dictionary<int, Reward> GetAdditionalRewards();

        /// <summary>
        /// Returns locked and unlocked feeding slots actual data.
        /// </summary>
        List<FoodSupplyFeedingSlot> GetSlotsData();

        /// <summary>
        /// Returns currently available recipes data
        /// </summary>
        List<FoodSupplyProductRecipe> GetRecipesData();
    }
}