using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.NanoLib.UtilitesEngine;
using Assets.Scripts.Engine.Analytics.Model.api;
using NanoLib.Core.Logging;
using NanoReality.Game.Tutorial.HintTutorial;
using NanoReality.GameLogic.Configs;
using NanoReality.GameLogic.Data;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using strange.extensions.signal.impl;

namespace NanoReality.Game.FoodSupply
{
    public sealed class FoodSupplyChangedSignal : Signal<FoodSupplyCityInfo> { }

    public sealed class FoodSupplyService : IFoodSupplyService
    {
        [Inject] public IServerCommunicator jServerCommunicator { get; set; }
        [Inject] public IPlayerProfile jPlayerProfile { get; set; }
        [Inject] public IPlayerExperience jPlayerExperience { get; set; }
        [Inject] public IGameConfigData jGameConfig { get; set; }
        [Inject] public ITimerManager jTimerManager { get; set; }
        [Inject] public INanoAnalytics jNanoAnalytics { get; set; }
        [Inject] public IHintTutorial jHintTutorial { get; set; }
        
        private const HintType GetFoodSupplyHint = HintType.CookFoodSupplyHint;
        private ITimer _statusEffectEndTimer;
        private FoodSupplyCityInfo _foodSupplyInfo;
        private FoodSupplyData _foodSupplyData;
        private bool IsTutorial => !jHintTutorial.IsHintCompleted(GetFoodSupplyHint);

        public bool IsAvailable => jPlayerExperience.CurrentLevel >= jGameConfig.FoodSupplyConfig.UnlockLevel;
        public bool IsDataLoaded => _foodSupplyInfo != null && _foodSupplyData != null;
        public bool IsFilled => _foodSupplyInfo.statusBarData.caloriesValue >= _foodSupplyInfo.statusBarData.maxCalories;
        public int CurrentCalories => _foodSupplyInfo.statusBarData.caloriesValue;
        public int MaxAvailableCalories => _foodSupplyInfo?.statusBarData.maxCalories ?? default;
        public long StatusEffectEndTimeStamp => _foodSupplyInfo.statusBarData.statusEffectEndTimeStamp;
        public int StatusEffectTotalTime => _foodSupplyInfo.statusBarData.statusEffectDuration;
        public float CoinsPerCalorie => _foodSupplyInfo.statusBarData.coinsPerCalorie;
        public float ProgressBarAmount { get; private set; }

        public event Action FoodSupplyCityInfoChanged;
        public event Action<bool> StatusEffectActivated;
        public event Action<int> FoodSupplyProductCooked;
        public event Action FoodSupplyBarFilled;
        public event Action<float> FoodSupplyAmountChanged;

        public void Init()
        {
            jPlayerExperience.LevelChanged += OnPlayerLevelChanged;

            UpdateActualFoodSupplyData();
        }
        
        public void UnlockFoodSupplySlot(int buildingId, int slotId, Action onComplete)
        {
            void OnSlotUnlocked(FoodSupplyData newData)
            {
                jNanoAnalytics.UnlockSlotEvent("FoodSupply", slotId);
                _foodSupplyData = newData;

                var slotData = newData.FeedingSlots.First(data => data.Id == slotId);
                jPlayerProfile.PlayerResources.BuyHardCurrency(slotData.UnlockPrice.HardPrice);
                
                onComplete?.Invoke();
            }
            
            jServerCommunicator.UnlockFoodSupplySlot(buildingId, slotId, OnSlotUnlocked);
        }

        public void CookFoodSupplyProduct(int buildingId, int foodSupplyRecipeId, Action onComplete)
        {
            var foodSupplyProductRecipe = _foodSupplyData.FindProduct(foodSupplyRecipeId);
          
            void OnFoodSupplyProductLoaded(FoodSupplyData newData)
            {
                jNanoAnalytics.FoodSupplyCook(foodSupplyRecipeId.ToString(), foodSupplyProductRecipe.Id.ToString());
                FoodSupplyProductCooked?.Invoke(foodSupplyRecipeId);

                _foodSupplyData = newData;

                jPlayerProfile.PlayerResources.RemoveProducts(foodSupplyProductRecipe.Products);
                onComplete?.Invoke();
            }

            if (IsTutorial)
            {
                jServerCommunicator.CookTutorialFoodSupplyProducts(foodSupplyRecipeId, OnFoodSupplyProductLoaded);
            }
            else
            {
                jServerCommunicator.CookFoodSupplyProducts(foodSupplyRecipeId, OnFoodSupplyProductLoaded);
            }

            jPlayerProfile.PlayerResources.AddProduct(foodSupplyProductRecipe.Id, 1);
        }

        public void FeedCitizens(int buildingId, Dictionary<int, int> productsInSlots, Action onSuccess)
        {
            var resourcesAmount = jPlayerProfile.PlayerResources.PlayerProducts.Count();
            var resourcesUsed = productsInSlots.Count;
            
            void OnComplete(FoodSupplyCityInfo foodSupplyData)
            {
                OnCityInfoLoaded(foodSupplyData);

                if(IsFilled)
                {
                    FoodSupplyBarFilled?.Invoke();
                }

                onSuccess?.Invoke();
                jNanoAnalytics.FoodSupplyFeed(resourcesAmount, resourcesUsed, ProgressBarAmount);
            }

            foreach (var slotProductPair in productsInSlots)
            {
                var productId = slotProductPair.Value;
                jPlayerProfile.PlayerResources.RemoveProduct(productId, 1);
            }
            
            jServerCommunicator.FoodSupplyFeedingProducts(buildingId, productsInSlots, OnComplete);
        }
        
        private void OnPlayerLevelChanged(int _)
        {
            UpdateActualFoodSupplyData();
        }
        
        private void UpdateActualFoodSupplyData()
        {
            if (IsAvailable)
            {
                if (IsTutorial)
                {
                    jServerCommunicator.GetTutorialFoodSupplyData(SetFoodSupplyDataOnTutorial);
                }
                else
                {
                    jServerCommunicator.GetFoodSupplyData(newData => _foodSupplyData = newData);
                }

                jServerCommunicator.GetFoodSupplyCityInfo(OnCityInfoLoaded);
            }
        }

        private void SetFoodSupplyDataOnTutorial(FoodSupplyData foodSupplyData)
        {
            _foodSupplyData = foodSupplyData;
            
            UpdateFoodSupplyAmount();
        }

        private void OnCityInfoLoaded(FoodSupplyCityInfo foodSupplyInfo)
        {
            _foodSupplyInfo = foodSupplyInfo;

            if (StatusEffectEndTimeStamp > jTimerManager.CurrentUTC)
            {
                RunStatusEffectTimer();
            }
            
            UpdateFoodSupplyAmount();
            WriteLog();

            FoodSupplyCityInfoChanged?.Invoke();
        }
        
        private void UpdateFoodSupplyAmount()
        {
            if (_foodSupplyInfo == null) return;

            if (IsFilled)
            {
                ProgressBarAmount = (float)(StatusEffectEndTimeStamp - jTimerManager.CurrentUTC) / StatusEffectTotalTime;
            }
            else if (MaxAvailableCalories != 0)
            {
                ProgressBarAmount = CurrentCalories / (float) MaxAvailableCalories;
            }
            else
            {
                ProgressBarAmount = 0f;
            }

            FoodSupplyAmountChanged?.Invoke(ProgressBarAmount);
        }
        
        private void RunStatusEffectTimer()
        {
            _statusEffectEndTimer?.CancelTimer();
            _statusEffectEndTimer = jTimerManager.StartServerTimer(StatusEffectEndTimeStamp, OnStatusEffectEnds, OnTimerTick);
            
            StatusEffectActivated?.Invoke(true);
        }

        private void OnTimerTick(float time)
        {
            UpdateFoodSupplyAmount();
        }

        private void OnStatusEffectEnds()
        {
            StatusEffectActivated?.Invoke(false);
            UpdateActualFoodSupplyData();
        }

        public FoodSupplyProduct GetProduct(int productId)
        {
            return jGameConfig.FoodSupplyConfig.GetProduct(productId);
        }

        public Dictionary<int, Reward> GetAdditionalRewards()
        {
            var result = new Dictionary<int, Reward>();
            if (_foodSupplyInfo?.statusBarData?.additionalRewards == null) return result;

            foreach (var additionalReward in _foodSupplyInfo.statusBarData.additionalRewards)
            {
                result.Add(additionalReward.openFromPercent, additionalReward.reward);
            }
            
            return result;
        }

        public List<FoodSupplyFeedingSlot> GetSlotsData()
        {
            return _foodSupplyData != null ? _foodSupplyData.FeedingSlots : new List<FoodSupplyFeedingSlot>();
        }

        public List<FoodSupplyProductRecipe> GetRecipesData()
        {
            return _foodSupplyData != null ? _foodSupplyData.AvailableProducts : new List<FoodSupplyProductRecipe>();
        }

        private void WriteLog()
        {
            var stringBuilder = new StringBuilder();
            stringBuilder.AppendLine("Food supply updated data:");
            stringBuilder.AppendLine($"Buff end time stamp: {StatusEffectEndTimeStamp}.");
            stringBuilder.AppendLine($"Buff duration: {StatusEffectTotalTime}s.");
            stringBuilder.AppendLine($"Filled food bar: {CurrentCalories}/{MaxAvailableCalories} calories.");
            stringBuilder.AppendLine("Additional rewards:");
            foreach (var additionalReward in _foodSupplyInfo.statusBarData.additionalRewards)
            {
                stringBuilder.AppendLine(additionalReward.reward.ToString());
            }

            stringBuilder.Log(LoggingChannel.Mechanics);
        }

        public void Dispose()
        {
            jPlayerExperience.LevelChanged -= OnPlayerLevelChanged;
            
            _statusEffectEndTimer?.CancelTimer();
            _statusEffectEndTimer = null;
        }
    }
}