using System;
using System.Collections.Generic;
using NanoReality.Game.Services;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Factories;
using Newtonsoft.Json;

namespace NanoReality.Game.Production
{
    public interface IProductionService : IGameService
    {
        float GetProductionTimeMultiplier(IProducingItemData product);

        void UpdateBuildingProductionSlots(Action onSuccess);
    }

    public sealed class BuildingProductionInfo
    {
        /// <summary>
        /// Unique building instance id on sector map.
        /// </summary>
        [JsonProperty("map_building_id")]
        public int BuildingId;

        /// <summary>
        /// All active production slots for current building.
        /// </summary>
        [JsonProperty("production")]
        public List<IProduceSlot> ProductionSlots;
    }
}