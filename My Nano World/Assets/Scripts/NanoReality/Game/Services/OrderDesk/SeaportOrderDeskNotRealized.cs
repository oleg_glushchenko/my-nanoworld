using System.Collections.Generic;
using Newtonsoft.Json;

namespace NanoReality.GameLogic.OrderDesk
{
    /// <summary>
    /// В этом классе зарыты нереализованные влажные мечты о том как я планировал SeaportOrderDesk.
    /// Но сделали мы его не как я планировал, а как вышло, впрочем как всегда.
    /// </summary>
    public sealed class SeaportOrderDeskNotRealized : ISeaportOrderDesk
    {
        [JsonProperty("orders")]
        public Dictionary<int, SeaportOrder> AvailableOrders { get; private set; }
    }

    public interface ISeaportOrderDesk
    {
        /// <summary>
        /// Unique order id
        /// Seaport order data.
        /// </summary>
        Dictionary<int, SeaportOrder> AvailableOrders { get; }
    }
    
    public sealed class SeaportOrder
    {
        [JsonProperty("order_id")]
        public int OrderId;
        
        [JsonProperty("order_size")]
        public OrderSizeType OrderSizeType;

        [JsonProperty("order_reward")]
        public SeaportOrderReward Reward;

        [JsonProperty("products")]
        public List<SeaportOrderProduct> Products;

        [JsonProperty("waiting_time")] 
        public int WaitingTime;

        [JsonProperty("skip_waiting_price")]
        public int SkipWaitingPrice;
    }
    
    public sealed class SeaportOrderReward
    {
        [JsonProperty("product_id")]
        public int ProductId;
        
        [JsonProperty("products_count")]
        public int ProductsCount;

        [JsonProperty("soft_currency")]
        public int SoftCurrency;

        [JsonProperty("hard_currency")]
        public int HardCurrency;
    }

    public sealed class SeaportOrderProduct
    {
        /// <summary>
        /// Unique id for each product slot in order
        /// </summary>
        [JsonProperty("id")]
        public int Id;
        
        [JsonProperty("product_id")]
        public int ProductId;
        
        [JsonProperty("loaded_products")]
        public int LoadedProductsCount;
        
        [JsonProperty("required_products")]
        public int RequiredProductsCount;

        public bool IsFilled => RequiredProductsCount == LoadedProductsCount;
    } 

    public enum OrderSizeType
    {
        None = 0,
        Small = 1,
        Medium = 2,
        Large = 3,
        Grand = 4
    }
}