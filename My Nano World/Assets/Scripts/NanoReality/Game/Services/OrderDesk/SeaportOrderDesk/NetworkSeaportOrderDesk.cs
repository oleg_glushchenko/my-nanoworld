using System.Collections.Generic;
using Newtonsoft.Json;

namespace NanoReality.Game.Services.OrderDesk.SeaportOrderDesk
{
    public sealed class NetworkSeaportOrderDesk
    {
        [JsonProperty("orders")]
        public List<SeaportOrderSlot> OrderSlots;

        [JsonProperty("skip_price")] 
        public int SkipPrice;

        [JsonProperty("shuffle_time")] 
        public int ShuffleTime = 10;

        [JsonProperty("created_at")] 
        public int CreatedAt = 0;
    }
}