using System.Collections.Generic;
using Assets.NanoLib.Utilities;
using Newtonsoft.Json;
using UniRx;

namespace NanoReality.Game.Services.OrderDesk.SeaportOrderDesk
{
    public class SeaportOrderSlot : ISeaportOrderSlot
    {
        [JsonProperty("id")]
        public Id<ISeaportOrderSlot> OrderId { get; set; }
        
        [JsonProperty("position")]
        public int ServerPosition { get; set; }

        [JsonProperty("count_free_shuffle_left")] 
        public int FreeSkip { get; set; }      
        
        [JsonProperty("count_free_shuffle_total")] 
        public int FreeSkipTotal { get; set; }
        
        [JsonProperty("awards")]
        public SeaportAward Awards { get; set; }
        
        [JsonProperty("products")]
        public List<ProductRequirementItem> ProductOrderItems { get; set; }

        /// <summary>
        /// Server time when shuffle waiting will end.
        /// </summary>
        [JsonProperty("left_time")]
        public long ShuffleEndTime { get; set; }

        public ReactiveProperty<long> ShuffleTimeLeft { get; } = new ReactiveProperty<long>(0);

        public bool IsShuffling => ShuffleTimeLeft.Value > 0;
    }
}