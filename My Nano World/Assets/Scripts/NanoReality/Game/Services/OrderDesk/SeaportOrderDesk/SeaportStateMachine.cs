namespace NanoReality.Game.Services.OrderDesk.SeaportOrderDesk
{
    public interface ISeaportStateMachine
    {
        SeaportState State { get; }
        void SetState(SeaportState state);
    }
}