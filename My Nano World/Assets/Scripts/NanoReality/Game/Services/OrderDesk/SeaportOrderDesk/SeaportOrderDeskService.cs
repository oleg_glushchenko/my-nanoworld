using System;
using System.Collections.Generic;
using System.Linq;
using Assets.NanoLib.UI.Core.Signals;
using Assets.NanoLib.UtilitesEngine;
using Assets.Scripts.Engine.Analytics.Model.api;
using FlyingWormConsole3;
using NanoReality.Game.UI;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.GameManager.Models.api;
using NanoReality.GameLogic.Orders;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.PlayerProfile.Models.Impl;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using UnityEngine;

namespace NanoReality.Game.Services.OrderDesk.SeaportOrderDesk
{
    public class SeaportOrderDeskService : ISeaportOrderDeskService
    {
        [Inject] public IServerCommunicator jServerCommunicator { get; set; }
        [Inject] public ITimerManager jTimerManager { get; set; }
        [Inject] public IPlayerProfile jPlayerProfile { get; set; }
        [Inject] public IGameManager jGameManager { get; set; }
        [Inject] public INanoAnalytics jNanoAnalytics { get; set; }
        [Inject] public UserLevelUpSignal jUserLevelUpSignal { get; set; }
        [Inject] public SignalSeaportOrderSent jSignalSeaportOrderSent { get; set; }

        [Inject] public LoadSeaportOrdersCommand.ExecuteCommandSignal jLoadSeaportOrdersCommandSignal { get; set; }
        [Inject] public SendSeaportOrderCommand.ExecuteCommandSignal jSendOrderCommandSignal { get; set; }
        [Inject] public ShuffleSeaportCommand.ExecuteCommandSignal jShuffleOrderCommandSignal { get; set; }

        public ReactiveSeaportCollection SeaportCollection { get; } = new ReactiveSeaportCollection();

        public int ShuffleTime { get; private set; }

        public event Action<ISeaportOrderSlot> OnSeaportSlotShuffleUpdated;

        private IMapBuilding _seaportBuilding;
        private ITimer _serverTickTimer;

        public void Init()
        {
            _seaportBuilding = (IMapBuilding)jGameManager.FindMapObjectOnMapByType(BusinessSectorsTypes.HeavyIndustry, MapObjectintTypes.OrderDesk);

            if (_seaportBuilding == null)
                return;

            jUserLevelUpSignal.AddListener(OnUserLevelUp);
            if (IsSeaportAvailable())
            {
                LoadSeaportOrders();
            }
        }

        public void Dispose()
        {
            jUserLevelUpSignal.RemoveListener(OnUserLevelUp);
            _serverTickTimer?.CancelTimer();
            _serverTickTimer = null;
        }

        public void LoadSeaportOrders(Action<NetworkSeaportOrderDesk> resultCallback = null)
        {
            void OnComplete(NetworkSeaportOrderDesk newOrderDesk)
            {
                if (newOrderDesk == null) return;

                SeaportCollection.Clear();
                foreach (SeaportOrderSlot orderSlot in newOrderDesk.OrderSlots)
                {
                    if (orderSlot.ShuffleEndTime != default)
                    {
                        int timeLeft = (int)(orderSlot.ShuffleEndTime - jTimerManager.CurrentUTC);
                        orderSlot.ShuffleTimeLeft.SetValueAndForceNotify(Mathf.Clamp(timeLeft, 0, int.MaxValue));
                    }

                    SeaportCollection.Add(orderSlot);
                }

                if (newOrderDesk.OrderSlots.Any(c => c.IsShuffling))
                {
                    StartServerTickTimer();
                }

                ShuffleTime = newOrderDesk.ShuffleTime;
                resultCallback?.Invoke(newOrderDesk);
            }
            
            jLoadSeaportOrdersCommandSignal.Dispatch(new LoadSeaportOrderCommandArgs(OnComplete));
        }

        public void SendOrder(int slotPosition)
        {
            ISeaportOrderSlot orderSlot = GetSlot(slotPosition);
            int orderId = orderSlot.OrderId.Value;

            var commandArgs = new SendSeaportOrderCommandArgs(orderId, OnComplete);

            void OnComplete(NetworkSeaportOrderDesk newOrderDesk)
            {
                foreach (SeaportOrderSlot newSlot in newOrderDesk.OrderSlots)
                {
                    if (newSlot.ServerPosition == slotPosition)
                    {
                        continue;
                    }
                    
                    if (newSlot.ShuffleEndTime != default)
                    {
                        int timeLeft = (int) (newSlot.ShuffleEndTime - jTimerManager.CurrentUTC);
                        newSlot.ShuffleTimeLeft.SetValueAndForceNotify(Mathf.Clamp(timeLeft, 0, int.MaxValue));
                    }

                    ISeaportOrderSlot slot = SeaportCollection.First(c => c.ServerPosition == newSlot.ServerPosition);
                    slot.ShuffleTimeLeft.SetValueAndForceNotify(newSlot.ShuffleTimeLeft.Value);
                    slot.OrderId = newSlot.OrderId;
                    slot.ProductOrderItems = newSlot.ProductOrderItems;
                    slot.Awards = newSlot.Awards;
                }

                if (newOrderDesk.OrderSlots.Any(c => c.IsShuffling))
                {
                    StartServerTickTimer();
                }

                ShuffleTime = newOrderDesk.ShuffleTime;
                UpdateAsSent(orderSlot, newOrderDesk);
                AddOrderRewardForUser(orderSlot);
                
                jSignalSeaportOrderSent.Dispatch(orderSlot);
            }

            jSendOrderCommandSignal.Dispatch(commandArgs);
        }

        public void SkipShuffleOrderWaiting(int serverPosition, bool skipByAds, Action resultCallback)
        {
            ISeaportOrderSlot slot = GetSlot(serverPosition);
            
            void OnComplete()
            {
                slot.ShuffleEndTime = jTimerManager.CurrentUTC;
                slot.ShuffleTimeLeft.SetValueAndForceNotify(0);
                UpdateAsShuffled(slot);
                resultCallback();
            }

            jServerCommunicator.ConfirmSkipShuffleOrder(slot.OrderId.Value, skipByAds, OnComplete);
        }

        public void StartShuffleOrder(int serverPosition, Action resultCallback)
        {
            ISeaportOrderSlot slot = GetSlot(serverPosition);
            
            void OnShuffleFinished(NetworkSeaportOrderDesk newOrderDesk)
            {
                Debug.Log("StartTimer " + ShuffleTime);
                
                if (newOrderDesk == null) return;

                SeaportCollection.Clear();
                foreach (SeaportOrderSlot orderSlot in newOrderDesk.OrderSlots)
                {
                    if (orderSlot.ShuffleEndTime != default)
                    {
                        int timeLeft = (int)(orderSlot.ShuffleEndTime - jTimerManager.CurrentUTC);
                        orderSlot.ShuffleTimeLeft.SetValueAndForceNotify(Mathf.Clamp(timeLeft, 0, int.MaxValue));
                    }
                    SeaportCollection.Add(orderSlot);
                }

                ShuffleTime = newOrderDesk.ShuffleTime;
                
                StartServerTickTimer();
                resultCallback();
                OnSeaportSlotShuffleUpdated?.Invoke(slot);
            }
            
            jShuffleOrderCommandSignal.Dispatch(new ShuffleSeaportOrderCommandArgs(slot.OrderId.Value, OnShuffleFinished));
        }

        public void RemoveProducts(int serverPosition)
        {
            ISeaportOrderSlot orderSlot = GetSlot(serverPosition);
            foreach (ProductRequirementItem productRequirementItem in orderSlot.ProductOrderItems)
            {
                jPlayerProfile.PlayerResources.RemoveProduct(productRequirementItem.ProductID, productRequirementItem.Count);
            }
        }

        private void UpdateAsShuffled(ISeaportOrderSlot orderSlot)
        {
            SeaportCollection.UpdateAsShuffle(SeaportCollection.FindIndex(c => c.OrderId == orderSlot.OrderId), orderSlot);
            OnSeaportSlotShuffleUpdated?.Invoke(orderSlot);
        }

        private void UpdateAsSent(ISeaportOrderSlot oldSlot, NetworkSeaportOrderDesk newOrderDesk)
        {
            SeaportOrderSlot newSlot = GetTargetSlot(oldSlot, newOrderDesk, out int index);
            SeaportCollection.UpdateAsSend(index, newSlot);
        }

        private SeaportOrderSlot GetTargetSlot(ISeaportOrderSlot oldSlot, NetworkSeaportOrderDesk newOrderDesk, out int index)
        {
            int oldSlotServerPosition = oldSlot.ServerPosition;
            SeaportOrderSlot updatedSlot = newOrderDesk.OrderSlots.Find(newSlot => newSlot.ServerPosition == oldSlotServerPosition);
            index = SeaportCollection.FindIndex(slot => slot.ServerPosition == oldSlotServerPosition);
            return updatedSlot;
        }

        private void AddOrderRewardForUser(ISeaportOrderSlot orderSlot)
        {
            SeaportAward slotAwards = orderSlot.Awards;
            int orderId = orderSlot.OrderId.Value;
            
            if (slotAwards.Products != null)
            {
                foreach (AwardProduct awardsProduct in slotAwards.Products)
                {
                    jPlayerProfile.PlayerResources.AddProduct(awardsProduct.ProductId, awardsProduct.Count);
                }
            }

            jPlayerProfile.CurrentLevelData.AddExperience(slotAwards.Experience, true);

            jNanoAnalytics.OnExperienceUpdate(AwardSourcePlace.SeaportReward, orderId.ToString(), slotAwards.Experience);
            jPlayerProfile.PlayerResources.AddSoftCurrency(slotAwards.Coins);
            jNanoAnalytics.OnPlayerCurrenciesIncrease(PlayerResources.CurencyType.Coins, slotAwards.Coins, AwardSourcePlace.SeaportReward,
                orderId.ToString());
        }

        private bool IsSeaportAvailable()
        {
            return _seaportBuilding.BuildingUnlockLevel <= jPlayerProfile.CurrentLevelData.CurrentLevel;
        }
        
        public float GetTimeLeft(int slotServerPosition)
        {
            return SeaportCollection.First(c => c.ServerPosition == slotServerPosition).ShuffleTimeLeft.Value;
        }

        public ISeaportOrderSlot GetSlot(int position)
        {
            return SeaportCollection.FirstOrDefault(slot => slot.ServerPosition == position);
        }

        private void StartServerTickTimer()
        {
            _serverTickTimer?.CancelTimer();
            _serverTickTimer = jTimerManager.StartServerTimer(float.MaxValue, null, OnServerTimerTick);
        }

        private void OnServerTimerTick(float currentTime)
        {
            List<ISeaportOrderSlot> finalizeShuffleSlots = null;
            foreach (ISeaportOrderSlot orderSlot in SeaportCollection.Where(c => c.IsShuffling))
            {
                var leftTime = (int)(orderSlot.ShuffleEndTime - jTimerManager.CurrentUTC);
                if (leftTime <= 0)
                {
                    if (finalizeShuffleSlots == null)
                    {
                        finalizeShuffleSlots = new List<ISeaportOrderSlot>();
                    }
                    finalizeShuffleSlots.Add(orderSlot);
                }
                orderSlot.ShuffleTimeLeft.SetValueAndForceNotify(Mathf.Clamp(leftTime, 0, int.MaxValue));
            }

            if (finalizeShuffleSlots != null)
            {
                foreach (ISeaportOrderSlot shuffleEndedSlot in finalizeShuffleSlots)
                {
                    UpdateAsShuffled(shuffleEndedSlot);
                    jNanoAnalytics.OnShuffle("Seaport", "By waiting", shuffleEndedSlot.ServerPosition.ToString());
                }
            }
        }

        private void OnUserLevelUp()
        {
            if (IsSeaportAvailable())
            {
                LoadSeaportOrders();
            }
        }
    }
}
