﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.NanoLib.ServerCommunication.Models;
using Assets.NanoLib.Utilities.Serializers;
using NanoLib.Core.Logging;
using NanoReality.Game.Tutorial.HintTutorial;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.Configs;
using NanoReality.GameLogic.GameManager.Models.api;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using NanoReality.StrangeIoC;
using strange.extensions.signal.impl;

namespace NanoReality.Game.Services.OrderDesk.SeaportOrderDesk
{
    public sealed class SendSeaportOrderCommandArgs : CommandArgs
    {
        public int OrderId;
        public Action<NetworkSeaportOrderDesk> CompleteCallback;

        public SendSeaportOrderCommandArgs(int orderId, Action<NetworkSeaportOrderDesk> completeCallback)
        {
            OrderId = orderId;
            CompleteCallback = completeCallback;
        }
    }

    public sealed class SendSeaportOrderCommand : AGameCommand
    {
        public sealed class ExecuteCommandSignal : Signal<SendSeaportOrderCommandArgs>
        {
        }

        [Inject] public IHintTutorial jHintTutorial { get; set; }
        [Inject] public IServerRequest jServerRequest { get; set; }
        [Inject] public IServerCommunicator jServerCommunicator { get; set; }
        [Inject] public IStringSerializer jStringSerializer { get; set; }
        [Inject] public SendSeaportOrderCommandArgs jCommandArgs { get; set; }
        [Inject] public IGameManager jGameManager { get; set; }
        [Inject] public IGameConfigData jGameConfigData { get; set; }

        private HintType _tutorialStep = HintType.SendSeaportOrder;
        
        private int OrderId => jCommandArgs.OrderId;

        public override void Execute()
        {
            base.Execute();

            if (!jHintTutorial.IsHintCompleted(_tutorialStep))
            {
                TutorialActionData actionData = jGameConfigData.TutorialConfigDate.HintStepActionData
                    ?.FirstOrDefault(a => a.StepId == (int) HintType.StartSeaport)?.ActionData[0];
                int buildingId = actionData.BuildingData[0].Id;
                var mapObjectType = actionData.BuildingData[0].MapObjectType;
                var businessSectorsType = actionData.BusinessSectorsType;
                IMapObject mapObject = jGameManager.FindMapObjectOnMapByType(businessSectorsType,
                    mapObjectType, buildingId);

                if (mapObject == null || mapObject.Level < 1)
                {
                    Logging.Log(LoggingChannel.Tutorial, $"Can't send tutorial seaport, building for upgrade not found. Id:[{buildingId.ToString()}]]");
                }
                else
                {
                    SendTutorialOrder(mapObject);
                    Retain();
                    return;
                }
            }

            SendOrder();
            Retain();
        }

        private void SendOrder()
        {
            string url = $"/v1/user/seaport/{OrderId.ToString()}";
            RequestMethod method = RequestMethod.PUT;

            jServerRequest.SendRequest(method, url, OnOrderDeskLoaded, OnRequestError);
        }

        private void SendTutorialOrder(IMapObject mapObject)
        {
            string url = $"/v1/tutorial/seaport/{OrderId.ToString()}";
            RequestMethod method = RequestMethod.PUT;

            List<int> requestedProducts = GetRequestedProducts();
            Dictionary<string, string> parameters = new Dictionary<string, string>
            {
                {"id", OrderId.ToString()},
                {"product_id", requestedProducts[0].ToString()}
            };

            List<int> GetRequestedProducts()
            {
                const int hammerId = 326;
                const int wiresId = 427;
                
                List<IProductForUpgrade> productsForUpgrade = ((IMapBuilding) mapObject).ProductsForUpgrade;

                return productsForUpgrade[0].ProductID == hammerId ? new List<int> {hammerId, wiresId} : new List<int> {wiresId, hammerId};
            }
            jServerRequest.SendRequest(method, parameters, url, OnOrderDeskLoaded, OnRequestError);
        }

        private void OnOrderDeskLoaded(string responseText)
        {
            var orderDesk = (NetworkSeaportOrderDesk) jStringSerializer.Deserialize(responseText, typeof(NetworkSeaportOrderDesk));
            jCommandArgs.CompleteCallback?.Invoke(orderDesk);
            Release();
        }

        private void OnRequestError(IErrorData errorData)
        {
            jServerCommunicator.OnRequestError(errorData);
        }
    }
}