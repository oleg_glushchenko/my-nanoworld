using System.Collections.Generic;

namespace NanoReality.Game.Services.OrderDesk.SeaportOrderDesk
{
    public interface ISeaportAward
    {
        int Coins { get; set; }
        int Experience { get; set; }
        AwardProduct Product { get; set; }
        List<AwardProduct> Products { get; set; }
    }
}