﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.NanoLib.ServerCommunication.Models;
using Assets.NanoLib.Utilities.Serializers;
using NanoLib.Core.Logging;
using NanoReality.Game.Tutorial.HintTutorial;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.Configs;
using NanoReality.GameLogic.GameManager.Models.api;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using NanoReality.StrangeIoC;
using strange.extensions.signal.impl;

namespace NanoReality.Game.Services.OrderDesk.SeaportOrderDesk
{
    public sealed class ShuffleSeaportOrderCommandArgs : CommandArgs
    {
        public readonly int OrderId;
        public readonly Action<NetworkSeaportOrderDesk> CompleteCallback;

        public ShuffleSeaportOrderCommandArgs(int orderId, Action<NetworkSeaportOrderDesk> completeCallback)
        {
            OrderId = orderId;
            CompleteCallback = completeCallback;
        }
    }
    
    public sealed class ShuffleSeaportCommand : AGameCommand
    {
        public sealed class ExecuteCommandSignal : Signal<ShuffleSeaportOrderCommandArgs> { }
        
        [Inject] public ShuffleSeaportOrderCommandArgs jCommandArgs { get; set; }
        
        [Inject] public IHintTutorial jHintTutorial { get; set; }
        [Inject] public IServerRequest jServerRequest { get; set; }
        [Inject] public IServerCommunicator jServerCommunicator { get; set; }
        [Inject] public IStringSerializer jStringSerializer { get; set; }
        [Inject] public IGameManager jGameManager { get; set; }
        [Inject] public ISeaportOrderDeskService jSeaportService { get; set; }
        [Inject] public IGameConfigData jGameConfigData { get; set; }
        
        private HintType _tutorialStep = HintType.SeaportOrderDialog;
        
        public override void Execute()
        {
            base.Execute();

            if (!jHintTutorial.IsHintCompleted(_tutorialStep))
            {
                TutorialActionData actionData = jGameConfigData.TutorialConfigDate.HintStepActionData
                    ?.FirstOrDefault(a => a.StepId == (int) HintType.StartSeaport)?.ActionData[0];
                int buildingId = actionData.BuildingData[0].Id;
                var mapObjectType = actionData.BuildingData[0].MapObjectType;
                var businessSectorsType = actionData.BusinessSectorsType;
                IMapObject mapObject = jGameManager.FindMapObjectOnMapByType(businessSectorsType,
                    mapObjectType, buildingId);
                
                if (mapObject == null || mapObject.Level < 1)
                {
                    Logging.Log(LoggingChannel.Tutorial, $"Can't shuffle tutorial seaport, building for upgrade not found. Id:[{buildingId.ToString()}]]");
                }
                else
                {
                    ShuffleTutorialOrder(mapObject);
                    Retain();
                    return;
                }
            }

            ShuffleOrder();
            Retain();
        }

        private void ShuffleOrder()
        {
            string url = $"/v1/user/seaport/extra/{jCommandArgs.OrderId.ToString()}";
            RequestMethod method = RequestMethod.POST;

            ISeaportOrderSlot order = jSeaportService.SeaportCollection.First(c => c.OrderId == jCommandArgs.OrderId);
            var parameters = new Dictionary<string, string>()
            {
                {"position", order.ServerPosition.ToString()}
            };

            jServerRequest.SendRequest(method, parameters, url, OnOrderDeskLoaded, jServerCommunicator.OnRequestError);
        }

        private void ShuffleTutorialOrder(IMapObject mapBuilding)
        {
            string url = $"/v1/tutorial/seaport/shuffle/{GetRequiredOrderItem(mapBuilding).ToString()}";
            RequestMethod method = RequestMethod.POST;

            ISeaportOrderSlot order = jSeaportService.SeaportCollection.First(c => c.OrderId == jCommandArgs.OrderId);
            var parameters = new Dictionary<string, string>()
            {
                {"position", order.ServerPosition.ToString()}
            };
            
            void OnOrderDeskLoaded(string responseText)
            {
                var orderDesk = (NetworkSeaportOrderDesk) jStringSerializer.Deserialize(responseText, typeof(NetworkSeaportOrderDesk));
                jCommandArgs.CompleteCallback?.Invoke(orderDesk);
            }

            jServerRequest.SendRequest(method, parameters, url, OnOrderDeskLoaded, jServerCommunicator.OnRequestError);
        }
        
        private void OnOrderDeskLoaded(string responseText)
        {
            var orderDesk = (NetworkSeaportOrderDesk)jStringSerializer.Deserialize(responseText, typeof(NetworkSeaportOrderDesk));
            jCommandArgs.CompleteCallback?.Invoke(orderDesk);
        }

        private int GetRequiredOrderItem(IMapObject mapBuilding)
        {
            const int hammerId = 326;
            const int wiresId = 427;
            
            List<IProductForUpgrade> productsForUpgrade = ((IMapBuilding)mapBuilding).ProductsForUpgrade;
            
            return productsForUpgrade[0].ProductID == hammerId ? hammerId : wiresId;
        }
    }
}