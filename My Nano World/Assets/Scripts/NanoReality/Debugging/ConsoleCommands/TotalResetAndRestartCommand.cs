﻿using MobileConsole;
using MobileConsole.UI;

namespace NanoReality.Debugging.ConsoleCommands
{
    [ExecutableCommand(name = "User/Total reset and restart")]
    public class TotalResetAndRestartCommand : Command
    {
        private TotalResetAndRestartViewBuilder _viewBuilder;

        public override void Execute()
        {
            if (_viewBuilder == null)
                _viewBuilder = new TotalResetAndRestartViewBuilder();
            info.shouldCloseAfterExecuted = false;
            LogConsole.PushSubView(_viewBuilder);
        }
    }

    public class TotalResetAndRestartViewBuilder : ViewBuilder
    {
        public override void OnPrepareToShow()
        {
            base.OnPrepareToShow();
            MobileConsoleContext.Instance.jDebugConsoleCommands.TotalResetAndRestart();
            LogConsole.PopSubView();
            LogConsole.Hide();
        }
    }
}