﻿using System;
using MobileConsole;
using MobileConsole.UI;
using NanoLib.Core.Logging;

namespace NanoReality.Debugging.ConsoleCommands
{
    [ExecutableCommand(name = "User/Edit Logger Masks")]
    public class EditLoggerMasks : Command
    {
        private EditLoggerViewBuilder _viewBuilder;

        public override void Execute()
        {
            if (_viewBuilder == null)
                _viewBuilder = new EditLoggerViewBuilder();
            info.shouldCloseAfterExecuted = false;
            LogConsole.PushSubView(_viewBuilder);
        }
    }

    public class EditLoggerViewBuilder : ViewBuilder
    {
        public EditLoggerViewBuilder()
        {
            closeAllSubViewOnAction = true;

            Draw();
        }

        private void Draw()
        {
            foreach (LoggingChannel channel in Enum.GetValues(typeof(LoggingChannel)))
            {
                if (channel == LoggingChannel.None || channel == LoggingChannel.All)
                    continue;
                
                AddCheckbox($"Add channel: {channel}", (Logging.ActiveChannels & channel) == channel, Callback, null);

                void Callback(CheckboxNodeView node)
                {
                    LoggingChannel activeChannels;
                    if (node.isOn)
                        activeChannels = Logging.ActiveChannels | channel;
                    else
                        activeChannels = Logging.ActiveChannels & ~channel;

                    Logging.SetChannels(activeChannels);
                }
            }
        }
    }
}