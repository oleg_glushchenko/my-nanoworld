﻿using MobileConsole;
using MobileConsole.UI;

namespace NanoReality.Debugging.ConsoleCommands
{
    [ExecutableCommand(name = "User/Clear asset bundles cache")]
    public class ClearAssetBundlesCacheCommand : Command
    {
        private ClearAssetBundlesCacheViewBuilder _viewBuilder;

        public override void Execute()
        {
            if (_viewBuilder == null)
                _viewBuilder = new ClearAssetBundlesCacheViewBuilder();
            info.shouldCloseAfterExecuted = true;
            LogConsole.PushSubView(_viewBuilder);
        }
    }

    public class ClearAssetBundlesCacheViewBuilder : ViewBuilder
    {
        public override void OnPrepareToShow()
        {
            base.OnPrepareToShow();
            MobileConsoleContext.Instance.jDebugConsoleCommands.ClearAssetBundlesCache();
            LogConsole.PopSubView();
			LogConsole.Hide();
        }
    }
}