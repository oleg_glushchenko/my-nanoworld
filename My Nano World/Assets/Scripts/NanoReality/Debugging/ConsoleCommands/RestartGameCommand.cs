﻿using MobileConsole;
using MobileConsole.UI;

namespace NanoReality.Debugging.ConsoleCommands
{
    [ExecutableCommand(name = "User/Restart game")]
    public class RestartGameCommand : Command
    {
        private RestartGameViewBuilder _viewBuilder;

        public override void Execute()
        {
            if(_viewBuilder == null)
                _viewBuilder = new RestartGameViewBuilder();
            info.shouldCloseAfterExecuted = true;
            LogConsole.PushSubView(_viewBuilder);
        }
    }
    
    public class RestartGameViewBuilder : ViewBuilder
    {
        public override void OnPrepareToShow()
        {
            base.OnPrepareToShow();
            MobileConsoleContext.Instance.jDebugConsoleCommands.RestartGame();
            LogConsole.PopSubView();
			LogConsole.Hide();
        }
    }
}