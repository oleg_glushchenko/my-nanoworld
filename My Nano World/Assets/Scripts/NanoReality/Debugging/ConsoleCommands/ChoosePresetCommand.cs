using MobileConsole;
using MobileConsole.UI;
using System.Linq;

namespace NanoReality.Debugging.ConsoleCommands
{
    [ExecutableCommand(name = "Cheats/Choose preset", order = 0)]
    public class ChoosePresetCommand : Command
    {
        private ChoosePresetViewBuilder _viewBuilder;

        public override void Execute()
        {
            _viewBuilder ??= new ChoosePresetViewBuilder();
            
            info.shouldCloseAfterExecuted = false;
            LogConsole.PushSubView(_viewBuilder);
        }
    }
    
    public class ChoosePresetViewBuilder : ViewBuilder
    {
        private PresetData[] _data;

        public ChoosePresetViewBuilder()
        {
            AddButtons();
        }

        private void AddButtons()
        {
            _data = MobileConsoleContext.Instance.jDebugConsoleCommands.PresetData;
            if (_data == null) return;
            
            foreach (var item in _data)
            {
                AddButton(item.name, "action", SetPreset);
            }
        }

        private void SetPreset(GenericNodeView node)
        {
            var id = _data.Where(data => data.name.Equals(node.name)).Select(data => data.id).FirstOrDefault();
            MobileConsoleContext.Instance.jDebugConsoleCommands.SetPreset(id,OnSetPresetSuccess);
            
            LogConsole.PopSubView();
            LogConsole.PopSubView();
            LogConsole.Hide();
        }

        private static void OnSetPresetSuccess()
        {
            MobileConsoleContext.Instance.jDebugConsoleCommands.RestartGame();
        }
    }
}