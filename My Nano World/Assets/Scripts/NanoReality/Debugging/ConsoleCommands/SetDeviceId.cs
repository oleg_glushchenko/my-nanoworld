﻿using MobileConsole;
using MobileConsole.UI;

namespace NanoReality.Debugging.ConsoleCommands
{
    [ExecutableCommand(name = "User/Set Device Id")]
    public class SetDeviceIdCommand : Command
    {
        private SetDeviceIdViewBuilder _viewBuilder;

        public override void Execute()
        {
            if (_viewBuilder == null)
                _viewBuilder = new SetDeviceIdViewBuilder();
            info.shouldCloseAfterExecuted = false;
            
            LogConsole.PushSubView(_viewBuilder);
        }
    }

    public class SetDeviceIdViewBuilder : ViewBuilder
    {
        private string _lastUserInput = "1";

        public SetDeviceIdViewBuilder()
        {
            DrawSearchControl();
            closeAllSubViewOnAction = true;
        }
        		
        void DrawSearchControl()
        {
            AddInput("New device ID", _lastUserInput, OnInputChanged);
            AddButton("Set  new ID", "action", SetDevieId);
        }

        private void OnInputChanged(InputNodeView node)
        {
            _lastUserInput = node.value;
        }

        private void SetDevieId(GenericNodeView node)
        {
            MobileConsoleContext.Instance.jDebugConsoleCommands.SetDeviceId(_lastUserInput);
            LogConsole.PopSubView();
            LogConsole.PopSubView();
            LogConsole.Hide();
        }
    }
}