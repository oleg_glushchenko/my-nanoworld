﻿using NanoLib.Core.Timers;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.ServerCommunication.Models;
using NanoReality.GameLogic.StartApplicationSequence.Controllers.Commands;
using NanoReality.StrangeIoC;
using TMPro;
using UniRx;
using UnityEngine;

namespace NanoReality.Debugging
{
    public class DebugBuildInfoView : AViewMediator
    {
        [SerializeField] private TextMeshProUGUI _playerText;
        [SerializeField] private TextMeshProUGUI _timeText;

        [Inject] public ServerTimeTickSignal jServerTimeTickSignal { get; set; }
        [Inject] public SignalOnPlayerProfileLoaded jSignalOnPlayerProfileLoaded { get; set; }
        
        protected override void PreRegister()
        {
            
        }

        protected override void OnRegister()
        {
#if !DebugLog
            Object.Destroy(gameObject);
#endif

            jServerTimeTickSignal.AddListener(OnTimerTick);
            jSignalOnPlayerProfileLoaded.AddOnce(OnPlayerProfileLoaded);
        }

        protected override void OnRemove()
        {
            jServerTimeTickSignal.RemoveListener(OnTimerTick);
        }

        private void OnTimerTick(long serverTime)
        {
            _timeText.text = serverTime.ToString();
        }

        private void OnPlayerProfileLoaded(IPlayerProfile playerProfile)
        {
            _playerText.text = "id:" + playerProfile.Id.Value;
        }
    }
}
