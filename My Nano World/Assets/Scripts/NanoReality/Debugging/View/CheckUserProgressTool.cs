﻿using System;
using System.Collections.Generic;
using Assets.NanoLib.ServerCommunication.Models;
using NanoReality.GameLogic.Common.Controllers.Signals;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using NanoReality.UI.Components;
using Newtonsoft.Json;
using Sirenix.OdinInspector;
using strange.extensions.mediation.impl;
using UnityEditor;
using UnityEngine;

namespace NanoReality.Debugging
{
    public class CheckUserProgressTool : View
    {
        [Header("client are not always synchronize with server")]
        [Header("some times client add sum before server send approve.")]
        [Header("you should always check!")]
        [ShowInInspector]
        private Dictionary<int, List<string>> _experienceLog = new Dictionary<int, List<string>>();

        [ShowInInspector] private Dictionary<int, List<string>> _softLog = new Dictionary<int, List<string>>();

        [ShowInInspector] private Dictionary<int, List<string>> _hardLog = new Dictionary<int, List<string>>();

        [ShowInInspector] private Dictionary<int, List<string>> _goldLog = new Dictionary<int, List<string>>();

        [ShowInInspector]
        private Dictionary<int, List<CompareMessage>> _compareLog = new Dictionary<int, List<CompareMessage>>();

        private readonly string _log = "Client: {0} Server: {1} Delta: [{2}]";

        private int CurrentClientLevel => jPlayerExperience.CurrentLevel + 1;
        private int CurrentExperience => jPlayerExperience.CurrentExperience;
        private int SoftCurrency => jPlayerProfile.PlayerResources.SoftCurrency;
        private int HardCurrency => jPlayerProfile.PlayerResources.HardCurrency;
        private int GoldCurrency => jPlayerProfile.PlayerResources.GoldCurrency;

        [Inject] public SignalExperienceUpdate jSignalExperienceUpdate { get; set; }
        [Inject] public IServerCommunicator jServerCommunicator { get; set; }
        [Inject] public IPlayerExperience jPlayerExperience { get; set; }
        [Inject] public IPlayerProfile jPlayerProfile { get; set; }
        [Inject] public SignalOnSoftCurrencyStateChanged jSignalOnSoftCurrencyStateChanged { get; set; }
        [Inject] public SignalOnHardCurrencyStateChanged jSignalOnHardCurrencyStateChanged { get; set; }
        [Inject] public SignalOnGoldCurrencyStateChanged jSignalOnGoldCurrencyStateChanged { get; set; }

        [PostConstruct]
        public void PostConstruct()
        {
            jSignalExperienceUpdate.AddListener(UpdateExperienceLog);
            jSignalOnSoftCurrencyStateChanged.AddListener(UpdateSoftLog);
            jSignalOnHardCurrencyStateChanged.AddListener(UpdateHardLog);
            jSignalOnGoldCurrencyStateChanged.AddListener(UpdateGoldLog);
        }

        private void UpdateExperienceLog()
        {
            LoadUserProfile(user =>
            {
                int serverExperience = user.CurrentExp;
                SendLog(_experienceLog, CurrentExperience, serverExperience);
            });
        }

        private void UpdateSoftLog(int _)
        {
            LoadUserProfile(user =>
            {
                int serverCurrency = user.NanoCoins;
                SendLog(_softLog, SoftCurrency, serverCurrency);
            });
        }

        private void UpdateHardLog(int _)
        {
            LoadUserProfile(user =>
            {

                int serverCurrency = user.PremiumCoins;
                SendLog(_hardLog, HardCurrency, serverCurrency);
            });
        }

        private void UpdateGoldLog(int _)
        {
            LoadUserProfile(user =>
            {
                int serverCurrency = user.Gold;
                SendLog(_goldLog, GoldCurrency, serverCurrency);
            });
        }

        private void SendLog(IDictionary<int, List<string>> logList, int currentValue, int serverValue)
        {
            if (!logList.ContainsKey(CurrentClientLevel))
            {
                logList.Add(CurrentClientLevel, new List<string>());
            }

            int delta = currentValue - serverValue;
            string message = string.Format(_log, currentValue, serverValue, delta);

            if (logList[CurrentClientLevel].Contains(message))
            {
                return;
            }

            logList[CurrentClientLevel].Add(message);
        }

        private void LoadUserProfile(Action<FakePlayerProfile> callback = null)
        {
            RequestCompleteDelegate onProfileLoaded = responseText =>
            {
                jServerCommunicator.jStringSerializer.DeserializeAsync(responseText, callback);
            };

            jServerCommunicator.jServerRequest.SendRequest(RequestMethod.GET, null, "/v1/user", onProfileLoaded,
                jServerCommunicator.OnRequestError);
        }

        [Button("Compare client and server user")]
        private void CompareUser()
        {
            LoadUserProfile(user =>
            {
                CompareMessage compareMessage = new CompareMessage();

                int serverExperience = user.CurrentExp;
                int experienceDelta = CurrentExperience - serverExperience;

                compareMessage.Experience = string.Format(_log, CurrentExperience, serverExperience, experienceDelta);

                int serverSoft = user.NanoCoins;
                int softDelta = SoftCurrency - serverSoft;

                compareMessage.Soft = string.Format(_log, SoftCurrency, serverSoft, softDelta);

                int serverHard = user.PremiumCoins;
                int hardDelta = HardCurrency - serverHard;

                compareMessage.Hard = string.Format(_log, HardCurrency, serverHard, hardDelta);

                int serverGold = user.Gold;
                int goldDelta = GoldCurrency - serverGold;

                compareMessage.Gold = string.Format(_log, GoldCurrency, serverGold, goldDelta);

                compareMessage.Status = (softDelta != 0 || hardDelta != 0 || goldDelta != 0 || experienceDelta != 0)
                    ? "check this!"
                    : "no Delta";

                if (!_compareLog.ContainsKey(CurrentClientLevel))
                {
                    _compareLog.Add(CurrentClientLevel, new List<CompareMessage>());
                }

                if (_compareLog[CurrentClientLevel].Contains(compareMessage))
                {
                    return;
                }

                _compareLog[CurrentClientLevel].Add(compareMessage);
            });
        }
        
#if UNITY_EDITOR
        [MenuItem("NanoReality/Tools/Debug/Add Check User Progress Tool", false, 1)]
        public static void AddCheckUserProgressTool()
        {
            GameObject tool = Instantiate(new GameObject());
            tool.name = "CheckUserProgressTool";
            tool.AddComponent<CheckUserProgressTool>();
        }
#endif
        
    }

    public class FakePlayerProfile
    {
        [JsonProperty("experience")]
        public int CurrentExp { get; set; }

        [JsonProperty("nano_coins")] 
        public int NanoCoins { get; private set; }

        [JsonProperty("premium_coins")] 
        public int PremiumCoins{ get; set; }

        [JsonProperty("gold")] 
        public int Gold { get; set; }

        [JsonProperty("nano_gems")] 
        public int NanoGems{ get; set; }
    }
    
   [Serializable]
   public class CompareMessage
   {
       public string Status;
       public string Experience;
       public string Soft;
       public string Hard;
       public string Gold;
   }
}