﻿using System;
using Assets.NanoLib.Utilities.DataManager.Models.api;
using Assets.Scripts.Engine.Analytics.Model.api;
using Assets.Scripts.Engine.UI.Views.Hud;
using NanoLib.Core.Logging;
using NanoLib.UI.Core;
using NanoReality.Core.Resources;
using NanoReality.Game.Services.SectorSevices;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using NanoReality.GameLogic.Settings.GamePrefs;
using NanoReality.Game.Tutorial;
using NanoReality.Game.Tutorial.HintTutorial;
using NanoReality.GameLogic.SocialCommunicator.api;
using NanoReality.GameLogic.PlayerProfile.Models.Impl;
using UnityEngine;

namespace NanoReality.Debugging
{
    public class DebugConsoleCommands
    {
        [Inject] public IGamePrefs jGamePrefs { get; set; }
        [Inject] public ResourcesPolicy ResourcesPolicy { get; set; }
        [Inject] public IServerCommunicator jServerCommunicator { get; set; }
        [Inject] public IPlayerProfile jPlayerProfile { get; set; }
        [Inject] public INanoAnalytics jNanoAnalytics { get; set; }
        [Inject] public IHardTutorial jHardTutorial { get; set; }
        [Inject] public IHintTutorial jHintTutorial { get; set; }
        [Inject] public IUIManager jUIManager { get; set; }
        [Inject] public ILocalDataManager jLocalDataManager { get; set; }
        [Inject] public IUnlockSectorService jUnlockSectorService { get; set; }

        [Inject] public IFacebookController jFacebookController { get; set; }

        [Inject] public SignalReloadApplication jSignalReloadApplication { get; set; }

        public PresetData[] PresetData { get; set; }

        public void RestartGame()
        {
            jSignalReloadApplication.Dispatch();
        }

        public void AddHardCurrency(int amount)
        {
            if (amount <= 0)
                return;

            jServerCommunicator.CheatAddValue("hard", amount, delegate
            {
                jPlayerProfile.PlayerResources.AddHardCurrency(amount);
                jNanoAnalytics.OnPlayerCurrenciesIncrease(PlayerResources.CurencyType.Bucks, amount, AwardSourcePlace.Cheating, "Cheating");
            });
        }

        public void AddGoldCurrency(int amount)
        {
            if (amount <= 0)
                return;

            jServerCommunicator.CheatAddAmount("gold", amount, delegate
            {
                jPlayerProfile.PlayerResources.AddGoldCurrency(amount);
                jNanoAnalytics.OnPlayerCurrenciesIncrease(PlayerResources.CurencyType.Gold, amount, AwardSourcePlace.Cheating, "Cheating");
            });
        }

        public void AddExperience(int amount)
        {
            if (amount <= 0)
                return;

            jServerCommunicator.CheatAddValue("exp", amount, delegate
            {
                jPlayerProfile.CurrentLevelData.AddExperience(amount, true);
                jNanoAnalytics.OnExperienceUpdate(AwardSourcePlace.Cheating, "cheating", amount);
            });
        }

        public void UnlockAll()
        {
            const int PickaxeId = 424;
            const int CoinsBundleIdBig = 6;
            var productsAmount = 0;
            var moneyAmount = 0;

            void OnUnlockAll()
            {
                jServerCommunicator.CheatAddProduct(PickaxeId, productsAmount, delegate { jUnlockSectorService.UnlockAllSectors(); });
            }

            foreach (var map in jPlayerProfile.UserCity.CityMaps)
            {
                foreach (var lockedSector in map.SubLockedMap.LockedSectors)
                {
                    productsAmount += lockedSector.LockedSector.Price.ProductsPrice[PickaxeId];
                    moneyAmount += lockedSector.LockedSector.Price.SoftPrice;
                }
            }

            if (jPlayerProfile.PlayerResources.SoftCurrency < moneyAmount)
            {
                jServerCommunicator.PurchaseSoftCurrencyBundle(CoinsBundleIdBig.ToString(), OnUnlockAll);
                return;
            }

            OnUnlockAll();
        }

        public void AddLevel(int amount)
        {
            if (amount <= 0)
                return;

            var currentLevel = jPlayerProfile.CurrentLevelData.CurrentLevel;
            var expToAdd = 0;

            for (var i = 0; i < amount; i++)
            {
                var userLevel = jPlayerProfile.CurrentLevelData.ObjectUserLevelsesBalanceData.GetLevel(currentLevel + i);
                if (userLevel != null)
                    expToAdd += userLevel.Experience;
            }

            jServerCommunicator.CheatAddValue("exp", expToAdd, delegate
            {
                jPlayerProfile.CurrentLevelData.AddExperience(expToAdd, true);

                jNanoAnalytics.OnExperienceUpdate(AwardSourcePlace.Cheating, "cheating by level", expToAdd);
            });
        }

        public void SetPreset(int presetId, Action onComplete)
        {
            jServerCommunicator.CheatSetPreset(presetId, onComplete);
        }

        public void AddProduct(int productId, int amount)
        {
            if (amount <= 0)
                return;

            if (ResourcesPolicy.GetProductSprite(productId) == null)
            {
                $"AddProduct: Product Id:{productId} not found".LogError();
                return;
            }

            jServerCommunicator.CheatAddProduct(productId, amount, delegate { jPlayerProfile.PlayerResources.AddProduct(productId, amount); });
        }

        public void ResetProfile()
        {
            PlayerPrefs.DeleteAll();
            PlayerPrefs.Save();
            RestartGame();
        }

        public void SkipHardTutorial()
        {
            jHardTutorial.SkipTutorial();
        }

        public void SkipHintTutorials()
        {
            jHintTutorial.SkipTutorial(RestartGame);
        }
        
        public void ActivateEditMode()
        {
            HudView hudView = jUIManager.GetView<HudView>();
            hudView.EditButton.gameObject.SetActive(true);
            hudView.EditButton.interactable = true;
        }

        public void SetDeviceId(string deviceId)
        {
            jGamePrefs.DeleteAll();
            jGamePrefs.DeviceId = deviceId;
            RestartGame();
        }

        public void ClearLocalData()
        {
            jLocalDataManager.ClearData();
            RestartGame();
        }

        public void ClearAssetBundlesCache()
        {
            Caching.ClearCache();
            RestartGame();
        }

        public void ClearAllAndReset()
        {
            jLocalDataManager.ClearData();
            SocialLogout();
            Caching.ClearCache();
            ResetProfile();
        }

        public void TotalResetAndRestart()
        {
            jLocalDataManager.ClearData();
            SocialLogout();
            Caching.ClearCache();
            PlayerPrefs.DeleteAll();
            PlayerPrefs.Save();
            ResetProfile();
        }

        private void SocialLogout()
        {
            if (jFacebookController.IsLoggedIn)
            {
                jFacebookController.Logout();
            }
        }
    }
}