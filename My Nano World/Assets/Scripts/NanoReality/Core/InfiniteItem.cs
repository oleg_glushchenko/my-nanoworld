using UnityEngine;

namespace NanoReality.Core
{
    public class InfiniteItem : MonoBehaviour, IInfiniteItem
    {
        public int Index { get; set; }

        public void SetActive(bool value) { }

        private RectTransform _rect;

        public RectTransform Rect
        {
            get
            {
                if (_rect == null)
                {
                    _rect = GetComponent<RectTransform>();
                }

                return _rect;
            }
        }

        public GameObject GameObject => gameObject;
    }
}