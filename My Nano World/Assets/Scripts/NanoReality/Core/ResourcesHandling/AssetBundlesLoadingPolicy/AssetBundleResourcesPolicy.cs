﻿using System;
using System.Collections.Generic;
using System.Linq;
using NanoLib.Core.Logging;
using NanoReality.Engine.BuildingSystem;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using UniRx;
using UnityEngine;
using UnityEngine.U2D;

namespace NanoReality.Core.Resources
{
    public class AssetBundleResourcesPolicy : ResourcesPolicy
    {
        [Inject] public BundlesLoadingFinishedSignal jAssetBundlesLoadedSignal { get; set; }
        [Inject] public GameResourcesLoadedSignal jGameResourcesLoadedSignal { get; set; }

        private readonly Dictionary<int, BuildingConfig> _loadedBuildingConfigs = new Dictionary<int, BuildingConfig>();
        private readonly Dictionary<int, Sprite> _productSprites = new Dictionary<int, Sprite>();
        private readonly Dictionary<int, Sprite> _eventSprites = new Dictionary<int, Sprite>();
        private GameObject _globalMap;
        private GameObject _storyLine;
        
        public override GameObject GlobalMap => _globalMap;

        [PostConstruct]
        public void PostConstruct()
        {
            jAssetBundlesLoadedSignal.AddListener(OnAssetBundlesLoaded);
        }        
        
        [Deconstruct]
        public void Deconstruct()
        {
            jAssetBundlesLoadedSignal.RemoveListener(OnAssetBundlesLoaded);
        }

        private void OnAssetBundlesLoaded(bool isSuccess)
        {
            if (!isSuccess)
            {
                return;
            }

            Observable.WhenAll(
                    ForEachInBundle<BuildingConfig>(AssetBundlesNames.BuildingResources, config => _loadedBuildingConfigs.Add(config.BuildingId, config)),
                    ForEachInBundle<SpriteAtlas>(AssetBundlesNames.ProductIcons, LoadSpritesFromSpriteAtlas),
                    ForEachInBundle<SpriteAtlas>(AssetBundlesNames.EventRewards, LoadEventSpritesFromSpriteAtlas),
                    LoadGameObject(AssetBundlesNames.MapGlobal, "Map", gameObject => _globalMap = gameObject))
                .Subscribe(c => jGameResourcesLoadedSignal.Dispatch());
        }
        
        public override Sprite GetProductSprite(int productId)
        {
            if (_productSprites.TryGetValue(productId, out var productIcon)) return productIcon;
            
            $"ProductIcon for Id {productId} not found.".LogError(LoggingChannel.AssetBundles);
            return null;
        }

        public override Sprite GetEventSprite(int index)
        {
            if (_eventSprites.TryGetValue(index, out var rewardIcon)) return rewardIcon;

            $"ProductIcon for Index {index} not found.".LogError(LoggingChannel.AssetBundles);
            return null;
        }

        private AsyncSubject<Unit> ForEachInBundle<T>(string bundleName, Action<T> getElement)
        {
            var executeSubject = new AsyncSubject<Unit>();
            executeSubject.OnNext(Unit.Default);
            
            var productsIconsBundle = AssetBundleManager.GetLoadedBundle(bundleName);
            var loadAsyncRequest = productsIconsBundle.LoadAllAssetsAsync<T>();

            void Complete(AsyncOperation asyncOperation)
            {
                var assets = loadAsyncRequest.allAssets.Cast<T>();
                foreach (var asset in assets)
                {
                    getElement?.Invoke(asset);
                }
                
                executeSubject.OnCompleted();
            }

            loadAsyncRequest.completed += Complete;

            return executeSubject;
        }
        
        private AsyncSubject<Unit> LoadGameObject(string bundleName, string gameObjectName, Action<GameObject> callback)
        {
            var executeSubject = new AsyncSubject<Unit>();
            executeSubject.OnNext(Unit.Default);
            
            var loadedBundle = AssetBundleManager.GetLoadedBundle(bundleName);
            var loadAsyncRequest = loadedBundle.LoadAssetAsync<GameObject>(gameObjectName);

            void Load(AsyncOperation asyncOperation)
            {
                var gameObject = (GameObject) loadAsyncRequest.asset;
                callback?.Invoke(gameObject);

                executeSubject.OnCompleted();
            }

            loadAsyncRequest.completed += Load;
            
            return executeSubject;
        }
        
        protected override BuildingConfig GetBuildingConfig(MapObjectId id)
        {
            if (_loadedBuildingConfigs.TryGetValue(id.Id, out var buildingConfig)) 
                return buildingConfig;
            
            $"Building configs loaded from AssetBundles not contains config with Id {id}".LogError(LoggingChannel.AssetBundles);
            return null;
        }
        
        private void LoadSpritesFromSpriteAtlas(SpriteAtlas atlas)
        {
            var sprites = new Sprite[atlas.spriteCount];
            atlas.GetSprites(sprites);
            
            foreach (var sprite in sprites)
            {
                _productSprites.Add(Product.ParseProductName(sprite.name), sprite);
            }
        }

        private void LoadEventSpritesFromSpriteAtlas(SpriteAtlas atlas)
        {
            var sprites = new Sprite[atlas.spriteCount];
            atlas.GetSprites(sprites);

            foreach (var sprite in sprites)
            {
                _eventSprites.Add(Product.ParseProductName(sprite.name), sprite);
            }
        }

        public override void UnloadBundle(string bundleName)
        {
            var loadedBundle = AssetBundleManager.GetLoadedBundle(bundleName);
            loadedBundle.Unload(true);
        }
    }
}