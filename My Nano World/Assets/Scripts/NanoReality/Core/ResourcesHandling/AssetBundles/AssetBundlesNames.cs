using System.Collections.Generic;

namespace NanoReality.Core.Resources
{
	public static class AssetBundlesNames
	{
		public const string ManifestFileName = "assetbundlemanifest";
		public const string ProductIcons = "products_atlases";
		public const string BuildingResources = "buildings_resources";
		public const string MapGlobal = "map";
		public const string EventRewards = "event_sprites";

		private static readonly Dictionary<string, string> _dependencies = new Dictionary<string, string>
		{
			{"CharacterSprites", "characters_atlases"},
			{"UiSprites", "ui_atlases"},
			{"ProductsSprites", "products_atlases"},
			{"BuildingsSprites", BuildingResources},
			{"MapSprites", MapGlobal},
            {"TheEventRewardsSprites",EventRewards}
		};
		
		public static string ManifestAssetBundleName => AssetBundlePath.GetPlatformFolder();

		public static string GetBundleName(string assetName)
		{
			return _dependencies.ContainsKey(assetName) ? _dependencies[assetName] : string.Empty;
		}

		public static IEnumerable<string> GetAssetsName(string bundleName)
		{
			var result = new List<string>();
			foreach (var dependency in _dependencies)
			{
				if(dependency.Value == bundleName)
					result.Add(dependency.Key);
			}

			return result;
		}
	}
}