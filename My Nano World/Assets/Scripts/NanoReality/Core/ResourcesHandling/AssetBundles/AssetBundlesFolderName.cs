namespace NanoReality.Core.Resources
{
	public enum AssetBundlesFolderName
	{
		Main,
		Reserve,
		Test
	}
}