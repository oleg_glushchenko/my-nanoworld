using System;
using System.Collections.Generic;
using NanoLib.Core.Logging;
using UnityEngine.U2D;

namespace NanoReality.Core.Resources
{
	public static class AtlasManager
	{
		private static readonly Dictionary<string, List<Action<SpriteAtlas>>> _requests = new Dictionary<string, List<Action<SpriteAtlas>>>();

		public static void Subscribe()
		{
			SpriteAtlasManager.atlasRequested += LoadAtlas;
			AssetBundleManager.OnAssetBundleLoaded += OnAssetBundleLoaded;
		}

		private static void LoadAtlas(string atlasName, Action<SpriteAtlas> callback)
		{
			var atlas = AssetBundleManager.GetAssetFromBundle<SpriteAtlas>(atlasName);
			if (atlas != null)
			{
				$"SpriteAtlas {atlasName} loaded successfully!".Log(LoggingChannel.SpriteAtlases);
				callback.Invoke(atlas);
			}
			else
			{
				$"SpriteAtlas {atlasName} added to requests list.".Log(LoggingChannel.SpriteAtlases);
				if (_requests.ContainsKey(atlasName))
					_requests[atlasName].Add(callback);
				else
					_requests.Add(atlasName, new List<Action<SpriteAtlas>> {callback});
			}
		}

		private static void OnAssetBundleLoaded(string bundleName)
		{
			var assetsNames = AssetBundlesNames.GetAssetsName(bundleName);
			foreach (var assetName in assetsNames)
			{
				if (!_requests.ContainsKey(assetName))
				{
					$"There is no requests for {assetName} asset.".LogWarning(LoggingChannel.SpriteAtlases);
					continue;
				}

				var atlas = AssetBundleManager.GetAssetFromBundle<SpriteAtlas>(assetName);
				if (atlas != null)
				{
					$"Sprite Atlas {atlas} loaded from Asset Bundle".Log(LoggingChannel.SpriteAtlases);
					foreach (var callback in _requests[assetName]) callback.Invoke(atlas);

					_requests.Remove(assetName);
				}
				else
					$"SpriteAtlas not found in the bundle with name {assetName}".LogError(LoggingChannel.SpriteAtlases);
			}
		}
		
		public static void Reset()
		{
			_requests.Clear();

			Unsubscribe();
		}

		public static void Unsubscribe()
		{
			SpriteAtlasManager.atlasRequested -= LoadAtlas;
			AssetBundleManager.OnAssetBundleLoaded -= OnAssetBundleLoaded;
		}
	}
}