using System.Collections.Generic;
using NanoLib.Core.Logging;
using UnityEngine;

namespace NanoReality.Core.Resources
{
	public static class ScriptableObjectsManager
	{
		private static readonly Dictionary<string, ScriptableObject> _scriptableObjects = new Dictionary<string, ScriptableObject>();
		private static ScriptableObjectsPathHandler _pathHandler;

		private static ScriptableObjectsPathHandler PathHandler => _pathHandler != null ? _pathHandler : UnityEngine.Resources.Load<ScriptableObjectsPathHandler>(nameof(ScriptableObjectsPathHandler));

		public static T Get<T>() where T : ScriptableObject
		{
			var type = typeof(T);
			var name = type.Name;

			return Get<T>(name);
		}
		
		public static T Get<T>(string name) where T : ScriptableObject
		{
			if (_scriptableObjects.ContainsKey(name))
				return (T) _scriptableObjects[name];

			var path = PathHandler.GetPath(name);
			if (path == null)
			{
				$"Path for ScriptableObject {name} not found!".LogError(LoggingChannel.Core);
				return null;
			}

			var scriptableObject = UnityEngine.Resources.Load<T>(path);
			if (scriptableObject == null)
			{
				$"ScriptableObject with path {path} not found!".LogError(LoggingChannel.Core);
				return null;
			}
			
			_scriptableObjects.Add(name, scriptableObject);

			return scriptableObject;
		}

		public static void Unload(this ScriptableObject scriptableObjectToUnload)
		{
			if (scriptableObjectToUnload == null)
			{
				"Specified ScriptableObject is null!".LogWarning(LoggingChannel.Core);
				return;
			}

			foreach (var loadedScriptableObject in _scriptableObjects)
			{
				if (loadedScriptableObject.Value != scriptableObjectToUnload) continue;
				
				UnityEngine.Resources.UnloadAsset(scriptableObjectToUnload);
				_scriptableObjects.Remove(loadedScriptableObject.Key);
				break;
			}
		}

		public static void UnloadAll()
		{
			foreach (var scriptableObject in _scriptableObjects) 
				UnityEngine.Resources.UnloadAsset(scriptableObject.Value);
			
			UnityEngine.Resources.UnloadAsset(_pathHandler);
			
			_scriptableObjects.Clear();
			_pathHandler = null;
		}
	}
}