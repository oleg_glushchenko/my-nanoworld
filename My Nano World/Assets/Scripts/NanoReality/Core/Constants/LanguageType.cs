namespace NanoReality.GameLogic.Constants
{
	public enum LanguageType
	{
		English,
		Arabic,
		German,
		Spanish,
		French,
		Russian
	}
}