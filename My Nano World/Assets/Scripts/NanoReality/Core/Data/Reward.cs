using System;
using System.Collections.Generic;
using System.Text;
using NanoReality.GameLogic.Constants;
using Newtonsoft.Json;

namespace NanoReality.GameLogic.Data
{
	[Serializable]
	public class Reward
	{
		private ResourcesType _type;

		[JsonProperty("reward_type")]
		public ResourcesType Type
		{
			get => _type;
			set
			{
				var intValue = (int) value;
				switch (intValue)
				{
					case 0: _type = ResourcesType.None; break;
					case 1: _type = ResourcesType.Soft; break;
					case 2: _type = ResourcesType.Hard; break;
					case 3: _type = ResourcesType.Materials; break;
					case 4: _type = ResourcesType.Soft | ResourcesType.Hard; break;
					case 5: _type = ResourcesType.Soft | ResourcesType.Materials; break;
					case 6: _type = ResourcesType.Hard | ResourcesType.Materials; break;
					case 7: _type = ResourcesType.Soft | ResourcesType.Hard | ResourcesType.Materials; break;
					case 9: _type = ResourcesType.Gold | ResourcesType.Materials; break;
					case 10: _type = ResourcesType.Gold; break;
				}
			}
		}

		[JsonProperty("reward_soft")] 
		public int softCurrencyCount;

		[JsonProperty("reward_gold")] 
		public int goldCurrencyCount;

		[JsonProperty("reward_hard")] 
		public int hardCurrencyCount;

		[JsonProperty("reward_resources")] 
		public string resourcesRewardRaw;

		public Dictionary<int, int> GetParsedRewards()
		{
			return string.IsNullOrEmpty(resourcesRewardRaw) ? null : JsonConvert.DeserializeObject<Dictionary<int, int>>(resourcesRewardRaw);
		}

		public override string ToString()
		{
			var builder = new StringBuilder();
			builder.AppendLine($"Reward type: {_type}.");
			builder.AppendLine($"Soft currency count: {softCurrencyCount}.");
			builder.AppendLine($"Hard currency count: {hardCurrencyCount}.");
			builder.AppendLine($"Gold count: {goldCurrencyCount}.");
			builder.AppendLine($"Resources: {resourcesRewardRaw}");

			return builder.ToString();
		}
	}
}