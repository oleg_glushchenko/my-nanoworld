namespace NanoReality.Core.RoadConstructor
{
    public class RoadPathfinderSquare
    {
        public enum SquareContent
        {
            Empty,
            Start,
            Finish,
            Obstacle
        }

        public SquareContent ContentCode { get; set; } = SquareContent.Empty;

        public int DistanceSteps { get; set; } = 100;

        public bool IsPath { get; set; } = false;

        public RoadPathfinderSquare SetState(SquareContent code)
        {
            ContentCode = code;
            return this;
        }
    }
}
