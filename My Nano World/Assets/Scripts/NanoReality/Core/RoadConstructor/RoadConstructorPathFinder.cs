﻿using NanoReality.GameLogic.BuildingSystem.CityMap.Controllers;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using System.Collections.Generic;
using UnityEngine;

namespace NanoReality.Core.RoadConstructor
{
    public class RoadConstructorPathFinder
    {
        private IMapObjectsGenerator MapObjectsGenerator => _context.MapObjectsGenerator;
        private MapObjectAddedSignal MapObjectAddedSignal => _context.MapObjectAddedSignal;
        private RoadConstructorModel ConstructorModel => _context.ConstructorModel;
        private TileOfRoadEditedSignal TileOfRoadEditedSignal => _context.TileOfRoadEditedSignal;
        private RoadSelectionService RoadSelectionService => _context.RoadSelectionService;
        private RoadSketchData RoadSketchData => _context.RoadSketchData;
        private RoadConstructorCheckers RoadConstructorCheckers => _context.RoadConstructorCheckers;

        private RoadConstructorContext _context;

        public RoadConstructorPathFinder(RoadConstructorContext context)
        {
            _context = context;
        }

        private int _tempMapId = -1;

        private List<Vector2Int> Directions { get; } = new List<Vector2Int>
        {
            Vector2Int.up,
            Vector2Int.down,
            Vector2Int.left,
            Vector2Int.right
        };

        private readonly Dictionary<Vector2Int, RoadPathfinderSquare> _squares =
            new Dictionary<Vector2Int, RoadPathfinderSquare>();

        private Vector2Int _lastStartPosition = Vector2Int.down;


        /// <summary>
        /// Launches path search process, renders route draft in PlaceRoadTile to RoadSegments
        /// </summary>
        public void FindPath()
        {
            // Calculate the route from the start point to the current position
            // if at least the start point set;
            // Either if this is a continuation
            // - then if it is the beginning next to the tail or the head of an existing segment 

            Vector2Int fromPoint = ConstructorModel.FromPoint;
            Vector2Int toPoint = ConstructorModel.ToPoint;

            if (fromPoint == Vector2Int.down)
            {
                return;
            }

            List<WayPoint> way;

            if (fromPoint == toPoint || toPoint == Vector2Int.down)
            {
                way = new List<WayPoint> {new WayPoint(fromPoint)}; //On Touch
            }
            else if (RoadConstructorCheckers.IsNeighbor(toPoint, fromPoint))
            {
                way = new List<WayPoint> {new WayPoint(fromPoint), new WayPoint(toPoint)};
            }
            else
            {
                way = FindAWay(fromPoint, toPoint); //On Swipe 
            }

            // if path not found - skip
            if (way == null || way.Count == 0)
            {
                return;
            }

            // build new path
            for (var index = 0; index < way.Count; index++)
            {
                PlaceRoadTile(way[index].Position);
            }

            RoadSelectionService.Select();
            ConstructorModel.LastCityMapView.RoadGrid?.UpdateRoads();
        }

        private void FindAWayFrom(Vector2Int start)
        {
            _squares.Clear();
            Vector2Int startingPoint = start;

            if (start == Vector2Int.down)
            {
                return;
            }

            if (!_squares.ContainsKey(startingPoint))
            {
                _squares.Add(startingPoint,
                    new RoadPathfinderSquare().SetState(RoadPathfinderSquare.SquareContent.Start));
            }

            _squares[startingPoint].DistanceSteps = 0;

            var dX = _context.ConstructorModel.LastCityMapView.RoadGrid.RoadGrid.GetLength(0);
            var dY = _context.ConstructorModel.LastCityMapView.RoadGrid.RoadGrid.GetLength(1);

            while (true)
            {
                var madeProgress = false;

                for (var iX = 0; iX < dX; iX++)
                {
                    for (var iY = 0; iY < dY; iY++)
                    {
                        var mainPoint = new Vector2Int(iX, iY);
                        if (RoadConstructorCheckers.IsPossibleToBuild(mainPoint))
                        {
                            if (!_squares.ContainsKey(mainPoint))
                            {
                                _squares.Add(mainPoint, new RoadPathfinderSquare());
                            }

                            var passHere = _squares[mainPoint].DistanceSteps;

                            for (var index = 0; index < Directions.Count; index++)
                            {
                                Vector2Int mPoint = Directions[index];
                                Vector2Int movePoint = mPoint + mainPoint;

                                if (!RoadConstructorCheckers.IsPossibleToBuild(movePoint))
                                {
                                    continue;
                                }

                                var newPass = passHere + 1;
                                if (!_squares.ContainsKey(movePoint))
                                {
                                    _squares.Add(movePoint, new RoadPathfinderSquare());
                                }

                                RoadPathfinderSquare roadPathfinderSquare = _squares[movePoint];
                                if (roadPathfinderSquare.DistanceSteps > newPass)
                                {
                                    roadPathfinderSquare.DistanceSteps = newPass;
                                    madeProgress = true;
                                }
                            }
                        }
                        else
                        {
                            if (!_squares.ContainsKey(mainPoint))
                            {
                                _squares.Add(mainPoint,
                                    new RoadPathfinderSquare().SetState(RoadPathfinderSquare.SquareContent.Obstacle));
                            }
                            else
                            {
                                _squares[mainPoint].SetState(RoadPathfinderSquare.SquareContent.Obstacle);
                            }
                        }
                    }
                }

                if (!madeProgress)
                {
                    break;
                }
            }
        }

        private List<WayPoint> FindAWay(Vector2Int start, Vector2Int end)
        {
            var outPath = new List<WayPoint>();
            Vector2Int startingPoint = end;
            if (end == Vector2Int.down)
            {
                return null;
            }

            if (_lastStartPosition != start || !_squares.ContainsKey(start) || !_squares.ContainsKey(end))
            {
                FindAWayFrom(start);
                _lastStartPosition = start;
            }

            if (!_squares.ContainsKey(start) || !_squares.ContainsKey(end))
            {
                return null;
            }

            _squares[end].SetState(RoadPathfinderSquare.SquareContent.Finish);
            outPath.Add(new WayPoint(end, 10000));

            while (true)
            {
                Vector2Int lowestPoint = Vector2Int.down;
                var lowest = 100;

                for (var index = 0; index < Directions.Count; index++)
                {
                    Vector2Int mPoint = Directions[index];
                    Vector2Int movePoint = mPoint + startingPoint;
                    if (!_squares.ContainsKey(movePoint))
                    {
                        continue;
                    }

                    var count = _squares[movePoint].DistanceSteps;
                    if (count < lowest)
                    {
                        lowest = count;
                        lowestPoint = movePoint;
                    }
                }

                if (lowest != 100)
                {
                    _squares[lowestPoint].IsPath = true;
                    startingPoint = lowestPoint;
                    outPath.Add(new WayPoint(lowestPoint, lowest));
                }
                else
                {
                    break;
                }

                if (_squares[startingPoint].ContentCode == RoadPathfinderSquare.SquareContent.Start)
                {
                    outPath.Sort((w1, w2) => w1.Distance.CompareTo(w2.Distance));
                    return outPath;
                }
            }

            return null;
        }

        private void PlaceRoadTile(Vector2Int position)
        {
            void AddToSketch(IMapObject mapObject)
            {
                RoadSketchData.AddToRoadSketch(mapObject);
            }

            // if road exist - send it to road sketch 
            IMapObject existsRoad = ConstructorModel.LastCityMapView.RoadGrid.RoadGrid[position.x, position.y]
                ?.MapObjectModel;

            if (existsRoad != null)
            {
                AddToSketch(existsRoad);
                return;
            }

            // if road NOT exist - create object and send it to road sketch 
            IMapObject newRoad =
                MapObjectsGenerator.GetMapObjectModel(ConstructorModel.SelectedModel.ObjectTypeID.Value, 0);
            newRoad.MapID = CalculateTempMapId();
            newRoad.SectorId = ConstructorModel.LastMapId;

            MapObjectAddedSignal.Dispatch(position, newRoad, () =>
            {
                newRoad.MapPosition = position;
                AddToSketch(newRoad);
            });
        }

        private int CalculateTempMapId()
        {
            _tempMapId -= 1;
            return _tempMapId;
        }
    }
}
