﻿using System.Collections.Generic;
using NanoReality.Engine.BuildingSystem.MapObjects;
using NanoReality.Engine.UI;
using NanoReality.GameLogic.BuildingSystem.CityMap.Controllers;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.GameManager.Models.api;
using UnityEngine;

namespace NanoReality.Core.RoadConstructor
{
    /// <summary>
    /// Here we coordinate all the multiple overlays as well as all the basic mechanics associated with road drawing 
    /// We basically adding objects to the tail, marking all bends along the way using Breakepoints list
    /// and if new input starts from tail - we simply flipping lists 
    /// </summary> 
    public class RoadSketchData
    {
        private IWorldSpaceCanvas WorldCanvas => _context.WorldCanvas;
        private IGameManager GameManager => _context.GameManager;
        private RoadSelectionService RoadSelectionService => _context.RoadSelectionService;
        private RoadConstructorModel ConstructorModel => _context.ConstructorModel;

        private RoadConstructorContext _context;

        public RoadSketchData(RoadConstructorContext context)
        {
            _context = context;
        }

        public void AddToRoadSketch(IMapObject mapObject)
        {
            if (mapObject == null || GameManager == null)
            {
                return;
            }

            ConstructorModel.RoadSketch.Add(mapObject);
            ConstructorModel.FromPoint = mapObject.MapPosition.ToVector2Int();
            RoadSelectionService.SetRoadMarker(mapObject, false, false, RoadTileState.New);
        } 

        #region Cleaning

        public void RemoveLastRoad()
        {
            if (ConstructorModel.RoadSketch.Count == 0) return;

            var index = ConstructorModel.RoadSketch.Count-1;
            IMapObject road = ConstructorModel.RoadSketch[index];

            
            var lastInstanceOfThisRoadObject = true;
            for (var j = 0; j < ConstructorModel.RoadSketch.Count; j++)
            {
                if (j != index && ConstructorModel.RoadSketch[j].MapPosition == road.MapPosition)
                {
                    lastInstanceOfThisRoadObject = false;
                    break;
                }
            }
            
            ConstructorModel.RoadSketch.RemoveAt(index);
            
            if(!lastInstanceOfThisRoadObject) return;
            
            var existsOnServer = road.MapID >= 0;
            if(existsOnServer)  
            {
                RoadSelectionService.SetRoadMarker(road, false, false, RoadTileState.Normal);
            }
            else
            {
                road.RemoveMapObject();
            } 
        }

        private void ClearSketchData()
        {
            ConstructorModel.RoadSketch.Clear();
            RoadSelectionService.RemoveFlags();
        }

        public void ClearAndRemoveNewRoadObjects()
        {
            for (var index = 0; index < ConstructorModel.RoadSketch.Count; index++)
            {
                IMapObject road = ConstructorModel.RoadSketch[index];
                if (road.MapID < 0)
                {
                    road.RemoveMapObject(); // todo   не удалять а возвращать в пул - road.FreeObject();
                }
            }

            ClearSketchData();
        }

        public void ClearAndShowEffects()
        {
            for (var index = 0; index < ConstructorModel.RoadSketch.Count; index++)
            {
                IMapObject mapObject = ConstructorModel.RoadSketch[index];
                if (!(mapObject is IRoad))
                {
                    continue;
                }

                var roadObjectView = (RoadMapObjectView) GameManager.GetMapObjectView(mapObject);
                roadObjectView.ShowRoadDust();
                roadObjectView.ShowRoadEnd(WorldCanvas, false, true, RoadTileState.Normal);
            }

            ClearSketchData();
        }

        #endregion
    }
}