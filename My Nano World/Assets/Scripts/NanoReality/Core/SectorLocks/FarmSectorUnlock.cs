﻿using System.Collections.Generic;
using NanoReality.Game.Services.BuildingsUnlockService;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.GameManager.Controllers;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using strange.extensions.mediation.impl;

namespace NanoReality.Core.SectorLocks
{
    public class FarmSectorUnlock : Mediator
    {
        [Inject] public FarmState FarmState { get; set; }
        [Inject] public IPlayerProfile jPlayerProfile { get; set; }
        [Inject] public IMapObjectsData jMapObjectsData { get; set; }
        [Inject] public IBuildingsUnlockService jBuildingsUnlockService { get; set; }
        [Inject] public SignalOnCityMapLoaded jSignalOnCityMapLoaded { get; set; }

        public override void OnRegister()
        {
            jSignalOnCityMapLoaded.AddListener((_, x) => WriteSectorUnlockInfo());
        }

        private void WriteSectorUnlockInfo()
        {
            var lowestUnlockLevelBuilding = int.MaxValue;
            foreach (var mapObject in GetMapObjects())
            {
                var unlockLevel = jBuildingsUnlockService.GetBuildingUnlockLevel(mapObject.ObjectTypeID);
                if (unlockLevel < lowestUnlockLevelBuilding)
                {
                    lowestUnlockLevelBuilding = unlockLevel + 1;
                    FarmState.UnlockLevel.Value = lowestUnlockLevelBuilding;
                }

                if (unlockLevel <= jPlayerProfile.CurrentLevelData.CurrentLevel)
                {
                    FarmState.FarmLocked.Value = false;
                }
            }
        }

        private List<IMapObject> GetMapObjects()
        {
            var mapObjects = new List<IMapObject>();
            var farmBuildings = jMapObjectsData.GetAllMapObjectsForSector((int) BusinessSectorsTypes.Farm);
            foreach (var sectorObject in farmBuildings)
            {
                if (!mapObjects.Contains(sectorObject) && sectorObject.Level == 0 && FilterMapObjectByTypes(sectorObject) && sectorObject.IsAvailableBuildingType())
                {
                    mapObjects.Add(sectorObject);
                }
            }

            return mapObjects;
        }
        
        private static bool FilterMapObjectByTypes(IMapObject mapObj) =>
            mapObj.MapObjectType != MapObjectintTypes.Road & mapObj.MapObjectType != MapObjectintTypes.Warehouse &
            mapObj.MapObjectType != MapObjectintTypes.CityHall & mapObj.MapObjectType != MapObjectintTypes.OrderDesk &
            mapObj.MapObjectType != MapObjectintTypes.CityOrderDesk & mapObj.MapObjectType != MapObjectintTypes.HeadQuarter & 
            mapObj.MapObjectType != MapObjectintTypes.GroundBlock;
    }
}