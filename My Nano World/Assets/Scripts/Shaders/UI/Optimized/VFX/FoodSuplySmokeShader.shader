﻿Shader "Unlit/FoodSuplySmokeShader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _WaveSpeed ("WaveSpeed (1 is Pi*2)", float) = 1
        _WaveAmplitude ("WaveAmplitude", float) = 1
        _WaveWidth ("WaveWidth", float) = 1
        
        _Intro ("Intro", float) = .1
        _Outro ("Outro", float) = .9
        
        _StepIn ("Step In", float) = .4
        _StepOut ("Step Out", float) = .6
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100
        Blend SrcAlpha OneMinusSrcAlpha 

		ZWrite Off
		
        Pass
        {
            CGPROGRAM 
            #pragma vertex vert alpha
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float4 color : COLOR;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                float4 color : COLOR;
                
               // float lifeTime;  
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            float _WaveAmplitude;
            float _WaveSpeed;
            float _WaveWidth;
            
            float _Intro;
            float _Outro;
            float _StepIn;
            float _StepOut;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
               o.color = v.color;
               // o.lifeTime = v.color.a;
                //o.color.a = 1;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                float intro = clamp( i.color.a/_Intro -i.uv.y ,0,1);
                float outro=clamp(( 1-i.color.a)/(1-_Outro) -(1- i.uv.y) ,0,1);
                
                float inOut=intro*outro;
                
                float4 color=float4(i.color.r,i.color.g,i.color.b,1);
                
                float waveSpeed=i.color.a*_WaveSpeed*(3.14159265*2);
                float waveAmplitude= i.uv.y*_WaveAmplitude;
                
                fixed4 col = tex2D(_MainTex,   i.uv  + float2(sin(waveSpeed+waveAmplitude )*_WaveWidth,0 ) )*color;
                col.a *=inOut;
                col.a = smoothstep(_StepIn,_StepOut,col.a);
                
                return col;
            }
            ENDCG
        }
    }
}
