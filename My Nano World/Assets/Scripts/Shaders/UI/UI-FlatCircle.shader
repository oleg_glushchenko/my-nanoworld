﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/Procedural Circle" {
	Properties {
		 [PerRendererData] _MainTex ("Source Image", 2D) = "white" {}
                [PerRendererData] _Color ("Color", Color) = (1,1,1,1)
                [MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
		_Distort("Distort", vector) = (0.5, 0.5, 1.0, 1.0)
		_OuterRadius ("Outer Radius", float) = 0.5
		_InnerRadius ("Inner Radius", float) = -0.5
		_Hardness("Hardness", float) = 1.0
	}
 
	SubShader {
		Tags { "RenderType"="Transparent" "Queue"="Transparent" "AllowProjectors"="False" }
 
		blend SrcAlpha OneMinusSrcAlpha
 
  Pass
                {
                CGPROGRAM
                        #pragma vertex vert
                        #pragma fragment frag
                        #pragma multi_compile DUMMY PIXELSNAP_ON
                        #include "UnityCG.cginc"
		 struct appdata_t
                        {
                                float4 vertex   : POSITION;
                                float4 color    : COLOR;
                                float2 texcoord : TEXCOORD0;
                        };
 
                        struct v2f
                        {
                                float4 vertex   : SV_POSITION;
                                fixed4 color    : COLOR;
                                half2 texcoord  : TEXCOORD0;
                        };
                       
                        fixed4 _Color;
 
                        v2f vert(appdata_t IN)
                        {
                                v2f OUT;
                                OUT.vertex = UnityObjectToClipPos(IN.vertex);
                                OUT.texcoord = IN.texcoord;
                                OUT.color = IN.color;
                                #ifdef PIXELSNAP_ON
                                OUT.vertex = UnityPixelSnap (OUT.vertex);
                                #endif
 
                                return OUT;
                        }
 
                        sampler2D _MainTex;
 
 
		fixed4 _Distort;
		float _OuterRadius, _InnerRadius, _Hardness;
		fixed4 frag(v2f IN) : SV_Target
		{
			half4 c = tex2D (_MainTex, IN.texcoord);
 
			float x = length((_Distort.xy - IN.texcoord.xy) * _Distort.zw);
 
			float rc = (_OuterRadius + _InnerRadius) * 0.5f; // "central" radius
			float rd = _OuterRadius - rc; // distance from "central" radius to edge radii
 
			float circleTest = saturate(abs(x - rc) / rd);
 
			fixed4 circle = _Color * c;
			circle.a = (1.0f - pow(circleTest, _Hardness)) * _Color.a * c.a;

			return circle;
		}

		ENDCG
		}
	} 
	FallBack "Diffuse"
}