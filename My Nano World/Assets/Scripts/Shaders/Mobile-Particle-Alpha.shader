// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/Transparent/Grid"
{
	Properties
	{
        _Color ("Main Color", Color) = (1,1,1,1)
		_MainTex("Base (RGB) Trans (A)", 2D) = "white" {}
		_MainTexBias("Mip Bias (-1 to 1)", float) = -1.65

	}
    SubShader
    {
        Tags
        {
            "IgnoreProjector" = "True"
            "RenderType" = "ReplacedGrid"
            "Queue" = "Geometry+40"
        }
        LOD 200

        Pass
        {

            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            ZTest Less
            Offset -2, -2

            CGPROGRAM
                #pragma vertex vert
                #pragma fragment frag
                #pragma target 3.0
                #include "UnityCG.cginc"

                uniform fixed4 _Color;
                uniform sampler2D _MainTex;
                uniform float4 _MainTex_ST;
                uniform float _MainTexBias;

                struct v2f
                {
                    float4 pos : SV_POSITION;
                    float2 uv : TEXCOORD0;
                };

                v2f vert(appdata_base i)
                {
                    v2f o;
                    o.uv = TRANSFORM_TEX(i.texcoord, _MainTex);
                    o.pos = UnityObjectToClipPos(i.vertex);
                    return o;
                }

                half4 frag(v2f i) : COLOR
                {
                    fixed4 c = tex2Dbias(_MainTex, half4(i.uv.x, i.uv.y,0.0, _MainTexBias));

                    return c * _Color;
                }
            ENDCG
        }
    }
}
