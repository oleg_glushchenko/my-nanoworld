﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'


//внитри внутренней границе будет прозрачность, за внешней границей - указанный цвет. Между ними - интерполированное значение.
  Shader "Custom/RadialGradientQuadShader" {
     Properties {
         _Color ("Color", Color) = (1, 1, 1, 1)
         _BorderIn ("Inner Radius", Float) = 0.5
         _BorderOut ("Outer Radius", Float) = 0.5
     }
 
     SubShader {
         Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" "PreviewType" = "Plane"}
         LOD 100
  
         Pass {
         Blend SrcAlpha OneMinusSrcAlpha

             CGPROGRAM
             #pragma vertex vert 
             #pragma fragment frag
 
             #include "UnityCG.cginc"
 
             struct appdata_t {
                 float4 vertex : POSITION;
                 float2 texcoord : TEXCOORD0;
             };
 
             struct v2f {
                 float4 vertex : SV_POSITION;
                 half2 texcoord : TEXCOORD0;
             };
 
             v2f vert (appdata_t v)
             {
                 v2f o;
                 o.vertex = UnityObjectToClipPos(v.vertex);
                 o.texcoord = v.texcoord;
                 return o;
             }
 
             fixed4 _Color;
             fixed4 _NonColor = (0, 0, 0, 0);
             float _Difference;
             float _LerpCoof;
              float _BorderIn;
              float _BorderOut;

             fixed4 frag (v2f i) : SV_Target
             {
                 float t = length(i.texcoord - float2(0.5, 0.5)) * 1.41421356237; // 1.141... = sqrt(2)
                 _LerpCoof = smoothstep(_BorderIn, _BorderOut, t);
                 return lerp(_NonColor, _Color, _LerpCoof);
             }
             ENDCG
         }
     }
 }