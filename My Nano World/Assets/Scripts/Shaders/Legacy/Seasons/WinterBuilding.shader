﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/Legacy/NanoReality/Buildings/Winter Building"
{
	Properties 
	{

		_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
		_MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
		_BuildingMask("Building Mask (R)", 2D) = "white" {}

		_WindowBright("Window Bright", Range(0,1))=1
		_BlikMask("Blik Mask (R)", 2D) = "white" {}




	}
	SubShader 
	{
		Tags 
		{
			"Queue"="AlphaTest" 
			"IgnoreProjector"="True" 
			"RenderType"="TransparentCutout"

		}
		LOD 200
	
		Pass
		{
			ZWrite On
			ZTest Less
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			 #include "UnityCG.cginc"
	        
			uniform sampler2D _MainTex;
			uniform sampler2D _BuildingMask;
			uniform sampler2D _BlikMask;


			uniform float4 _BlikMask_ST; 
			uniform float4 _MainTex_ST;

			uniform fixed _Cutoff;
			
			uniform float _WindowBright;

			




			struct vertIN
			{
				float4 pos :POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f 
			{
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
				float2 blickUV : TEXCOORD2;
			};



			v2f vert(vertIN i)
			{
				v2f o;
				o.uv = TRANSFORM_TEX (i.uv, _MainTex);
				o.blickUV = TRANSFORM_TEX (i.uv, _BlikMask);
				o.pos = UnityObjectToClipPos(i.pos);
				return o;
			}

			fixed HalfOverlay(fixed target, fixed blend)
			{
				fixed result = 0;
				if(blend<=0.5)
					return target;
				if(target>0.5f)
				{
					return (1-(1-2*(target-0.5f))*(1-blend));
				}
				return 2*target*blend;
			}

			fixed3 overlayColor(fixed3 target, fixed3 blend)
			{
				fixed3 result;
				result.r = HalfOverlay(target.r,blend.r);
				result.g = HalfOverlay(target.g,blend.g);
				result.b = HalfOverlay(target.b,blend.b);
				return result;
			}

			
			half4 frag(v2f i) : COLOR 
			{
				fixed4 result = tex2D(_MainTex, i.uv);
				fixed4 buildingMask = tex2D(_BuildingMask, i.uv);
				fixed4 blick = tex2D(_BlikMask,i.blickUV);
	
				

				// ************************* БЛИКИ НА СТЕКЛЕ *************************************
				fixed3 windowBlick = result.rgb;
				windowBlick = overlayColor(result.rgb,buildingMask.rrr*blick.r);
				result.rgb =  lerp(result.rgb, windowBlick, _WindowBright);
				// *********************************************************************************

				clip(result.a - _Cutoff);
				return result;
			}
			ENDCG
		}
	} 
}
