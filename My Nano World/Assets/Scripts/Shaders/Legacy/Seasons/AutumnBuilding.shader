﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/Legacy/NanoReality/Buildings/Autumn Building"
{
	Properties 
	{
		
		_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
		_MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
		_BuildingMask("Building Mask (R)", 2D) = "white" {}


		_WindowBright("Window Bright", Range(0,1))=1
		_BlikMask("Blik Mask (R)", 2D) = "white" {}
		
		_AutumnColor("Autumn trees Color", Color) = (1,1,1,1)
		_AutomBlend("Autumn Strengh", Range(0,1)) = 0
		_GrassBlend("Grass Blend", Range(0,1)) = 0


	}
	SubShader 
	{
		Tags 
		{
			"Queue"="AlphaTest" 
			"IgnoreProjector"="True" 
			"RenderType"="TransparentCutout"

		}
		LOD 200
	
		Pass
		{
			ZWrite On
			ZTest Less
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			 #include "UnityCG.cginc"
	        
			uniform sampler2D _MainTex;
			uniform sampler2D _BuildingMask;
			uniform sampler2D _BlikMask;


			uniform float4 _BlikMask_ST; 
			uniform float4 _MainTex_ST;

			uniform fixed _Cutoff;
			
			uniform float _WindowBright;

			uniform float4 _AutumnColor;
			uniform float _AutomBlend;

			uniform float _GrassBlend;

	



			struct vertIN
			{
				float4 pos :POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f 
			{
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
				float2 blickUV : TEXCOORD2;
			};



			v2f vert(vertIN i)
			{
				v2f o;
				o.uv = TRANSFORM_TEX (i.uv, _MainTex);
				o.blickUV = TRANSFORM_TEX (i.uv, _BlikMask);
				o.pos = UnityObjectToClipPos(i.pos);
				return o;
			}

			fixed HalfOverlay(fixed target, fixed blend)
			{
				fixed result = 0;
				if(blend<=0.5)
					return target;
				if(target>0.5f)
				{
					return (1-(1-2*(target-0.5f))*(1-blend));
				}
				return 2*target*blend;
			}

			fixed3 overlayColor(fixed3 target, fixed3 blend)
			{
				fixed3 result;
				result.r = HalfOverlay(target.r,blend.r);
				result.g = HalfOverlay(target.g,blend.g);
				result.b = HalfOverlay(target.b,blend.b);
				return result;
			}

			fixed Epsilon = 1e-10;
 
			fixed3 RGBtoHCV(in fixed3 RGB)
			{
				fixed4 P = (RGB.g < RGB.b) ? fixed4(RGB.bg, -1.0, 2.0/3.0) : float4(RGB.gb, 0.0, -1.0/3.0);
				fixed4 Q = (RGB.r < P.x) ? fixed4(P.xyw, RGB.r) : float4(RGB.r, P.yzx);
				fixed C = Q.x - min(Q.w, Q.y);
				fixed H = abs((Q.w - Q.y) / (6 * C + Epsilon) + Q.z);
				return fixed3(H, C, Q.x);
			}
			
			fixed3 HUEtoRGB(in fixed H)
			{
				fixed R = abs(H * 6 - 3) - 1;
				fixed G = 2 - abs(H * 6 - 2);
				fixed B = 2 - abs(H * 6 - 4);
				return saturate(fixed3(R,G,B));
			}

			fixed3 HSLtoRGB(in fixed3 HSL)
			{
				fixed3 RGB = HUEtoRGB(HSL.x);
				fixed C = (1 - abs(2 * HSL.z - 1)) * HSL.y;
				return (RGB - 0.5) * C + HSL.z;
			}


			fixed3 RGBtoHSL(in fixed3 RGB)
			{
				fixed3 HCV = RGBtoHCV(RGB);
				fixed L = HCV.z - HCV.y * 0.5;
				fixed S = HCV.y / (1 - abs(L * 2 - 1) + Epsilon);
				return fixed3(HCV.x, S, L);
			}

			fixed3 ColorBlend(fixed3 target, fixed3 blend)
			{
				fixed3 bottomHSL = RGBtoHSL(target);
				fixed3 topHSL = RGBtoHSL (blend);
				fixed3 resultHSL = float3(topHSL.xy,bottomHSL.z);
				return HSLtoRGB(resultHSL);
			}


			half4 frag(v2f i) : COLOR 
			{
				fixed4 result = tex2D(_MainTex, i.uv);
				fixed4 buildingMask = tex2D(_BuildingMask, i.uv);
				fixed4 blick = tex2D(_BlikMask,i.blickUV);
				
		
	 
				// ***************** ОСЕНЬ ******************************
				fixed3 resultAtm = ColorBlend(result.rgb,_AutumnColor.rgb); 
				result.rgb =  lerp(result.rgb,resultAtm,_AutomBlend*buildingMask.g); // деревья
				result.rgb =  lerp(result.rgb,resultAtm,_AutomBlend*_GrassBlend*buildingMask.b); // трава
				// ****************************************************	


				// ************************* БЛИКИ НА СТЕКЛЕ *************************************
				fixed3 windowBlick = result.rgb;
				windowBlick = overlayColor(result.rgb,buildingMask.rrr*blick.r);
				result.rgb =  lerp(result.rgb,windowBlick,_WindowBright);
				// *********************************************************************************

				clip(result.a - _Cutoff);
				return result;
			}
			ENDCG
		}
	} 
}
