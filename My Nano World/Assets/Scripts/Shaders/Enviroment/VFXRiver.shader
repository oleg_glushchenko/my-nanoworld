// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

//noxikid 
//this is 4 component river shader

Shader "VFX/River" {
Properties {
	_MainTex ("Base layer R Foam G Waves B ", 2D) = "white" {}	
	_Mask ("Wave Mask R, Color Mask G, Waves B, FoamMask A", 2D) = "white" {}
	_Scroll("River Scroll speed", Vector) = (1.0,1.0,1.0,1.0)
	
	_ColorDeep("Color Deep", Color) = (1,1,1,1)
	_ColorShore("Color Shore", Color) = (1,1,1,1)
	_ColorWave("Color Wave", Color) = (1,1,1,1)
	_ShoreTreshold("Shore Treshold", Float) = 1.0
	_DeepTreshold("Deep Treshold", Float) = 2.0
	_FoamPower("Foam Power", Float) = 1.0
	_MMultiplier ("Layer Multiplier", Float) = 2.0
}

	
SubShader
{
	Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="ReplacementRiverWater" }
	
	Blend SrcAlpha OneMinusSrcAlpha
	ZWrite Off

	
	Lighting Off  Fog { Color (0,0,0,0) }
	
	LOD 100
	
	
	
	CGINCLUDE
	#pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
	#include "UnityCG.cginc"
	sampler2D _MainTex;
	sampler2D _Mask;

	float4 _MainTex_ST;
	float4 _Mask_ST;
	
	float4 _Scroll;

	float _MMultiplier;
	float _ShoreTreshold;
	float _DeepTreshold;
	float _FoamPower;

	float4 _ColorDeep;
	float4 _ColorShore;
	float4 _ColorWave;

	struct v2f {
		float4 pos : SV_POSITION;
		float4 uv : TEXCOORD0;
		fixed4 color : TEXCOORD1;
		float4 uv2 : TEXCOORD2;
	};

	
	v2f vert (appdata_full v)
	{
		v2f o;
		o.pos = UnityObjectToClipPos(v.vertex);
		o.uv.xy = TRANSFORM_TEX(v.texcoord.xy,_MainTex) + frac(float2(_Scroll.x, _Scroll.y) * _Time);
		o.uv.zw = v.texcoord.xy*_MainTex_ST.zw + frac(float2(_Scroll.z, _Scroll.w) * _Time);
		o.uv2.xy = v.texcoord1.xy*_Mask_ST.xy + +frac(float2(_Scroll.x*_Mask_ST.x, 0) * _Time);
		o.uv2.zw = v.texcoord1.xy*_Mask_ST.zw + +frac(float2(_Scroll.z*_Mask_ST.z, 0) * _Time);
		
		o.color =  v.color;
		o.color.g *= _FoamPower;
		return o;
	}
	ENDCG


	Pass
	{
		CGPROGRAM
		#pragma vertex vert
		#pragma fragment frag
		fixed4 frag (v2f i) : COLOR
		{
			fixed4 o;
			fixed4 tex = tex2D (_MainTex, i.uv.xy);
			fixed4 tex2 = tex2D (_MainTex, i.uv.zw);
			fixed4 mask = tex2D (_Mask, i.uv2.xy);
			fixed4 mask2 = tex2D(_Mask, i.uv2.zw);

			//o = tex * tex2 * _MMultiplier * lerp(_ColorShore, _ColorDeep, mask.g*2.0-1.0) +_ColorWave*saturate(_ShoreTreshold - mask.g);
			o = tex.r * tex2.r * _MMultiplier * lerp(lerp(_ColorShore, _ColorDeep, saturate(mask.g- _DeepTreshold)), _ColorWave,saturate(_ShoreTreshold - mask.g)) + (tex.g + tex2.g)*  i.color.g;
			o.a *= (mask.r*mask2.r)*i.color.a;

			return o;
		}
		ENDCG 
	}	
}
}
