﻿//vertexcolor.r - waves area
//vertexcolor.g - foam area
//vertexcolor.a - alpha

Shader "NanoReality/VFX/WaterLite/Refraction"
{
	Properties
	{
		_ColorShore("ShoreColor", Color) = (1,1,1,1)
		[NoscaleOffset]
		_MainTex("WavesAndFoam", 2D) = "white" {} //waves, foam, blins, distortion  
		_ScaleWiggle("WiggleScale", Float) = 1.0
		[Space(20)]
		_ColorWaves("WavesColor", Color) = (1,1,1,1)
		_ScaleWaves("WavesScale", Float) = 1.0
		_SpeedWaves("WavesSpeed", Float) = 1.0
		_DistrWaves("WavesDistortionFactor", Float) = 1.0
		[Space(20)]
		_ColorFoam("FoamColor", Color) = (1,1,1,1)
		_ScaleFoam("FoamScale", Float) = 1.0
		_SpeedFoam("FoamSpeed", Float) = 1.0
		_DistrFoam("FoamDistortionFactor", Float) = 1.0
		[Space(20)]
		_DistrRefr("RefractioStrength", Float) = 1.0
		_ScaleRefraction("RefractionScale", Float) = 1.0
		_SpeedRefr("RefractioSpeed", Float) = 1.0
	}

		SubShader
		{
			Tags { "RenderType" = "Opaque" }
			LOD 100

			GrabPass { "_BackgroundTexture" }

			Pass
			{
				CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag      
				#include "UnityCG.cginc"

				struct appdata
				{
					float4 vertex : POSITION;
					float2 uv : TEXCOORD0;
					float2 uv2 : TEXCOORD1;
					fixed4 color : COLOR;
				};

				struct v2f
				{
					float2 uv : TEXCOORD0;
					float2 uv2 : TEXCOORD1;
					float4 vertex : SV_POSITION;
					float4 grabPos: TEXCOORD2;
					fixed4 color : COLOR;
				};

				sampler2D_half _MainTex;
				sampler2D_half _BackgroundTexture;
				half4 _MainTex_ST;
				half _ScaleWiggle;
				fixed4 _ColorShore;

				fixed4 _ColorWaves;
				half _ScaleWaves;
				half _SpeedWaves;
				half _DistrWaves;

				fixed4 _ColorFoam;
				half _ScaleFoam;
				half _SpeedFoam;
				half _DistrFoam;
				half _DistrRefr;
				half _ScaleRefraction;
				half _SpeedRefr;

				static const fixed alpha = 1;

				v2f vert(appdata v)
				{
					v2f o;
					o.vertex = UnityObjectToClipPos(v.vertex);
					o.uv = TRANSFORM_TEX(v.uv, _MainTex);
					o.uv2 = TRANSFORM_TEX(v.uv2, _MainTex);
					o.vertex = UnityObjectToClipPos(v.vertex);

					o.grabPos = ComputeGrabScreenPos(o.vertex);
					o.grabPos /= o.grabPos.w;
					float noise = tex2Dlod(_MainTex, float4(v.uv * _ScaleWiggle, 0,0)).a;

					o.color = v.color;
					return o;
				}

				float2 offsetUvY(float2 originalUv, float offsetY, fixed4 distortion, float distortionFactor) {
					float uvResultX = originalUv.x;
					float uvResultY = (originalUv.y + distortion / distortionFactor + _Time.w * offsetY) % 2;
					float2 result = float2(uvResultX, uvResultY);
					return result;
				}

				float2 offsetYnoDistr(float2 originalUv, float speed) {
					float uvResultX = originalUv.x;
					float uvResultY = (originalUv.y + _Time.w * speed) % 2;
					float2 result = float2(uvResultX, uvResultY);
					return result;
				}

				fixed4 frag(v2f i) : SV_Target
				{
					fixed3 shoreCol = (1- i.color.a) * _ColorShore;
					float2 distrScaled = i.uv * _ScaleWiggle;
					fixed distortion = tex2D(_MainTex, distrScaled).b;

					float2 wavesUvscaled = i.uv * _ScaleWaves;
					float2 vawesUvOffset = offsetUvY(wavesUvscaled, _SpeedWaves, distortion, _DistrWaves);
					float waves = tex2D(_MainTex, vawesUvOffset).r * (1-i.color.g) * _ColorWaves.a;
					fixed3 wavesColored = waves * _ColorWaves;

					float2 foamUvscaled = i.uv2 * _ScaleFoam;
					float2 foamUvOffset = offsetUvY(foamUvscaled, _SpeedFoam, distortion, _DistrFoam);
					float foam = tex2D(_MainTex, foamUvOffset).g * i.color.g * i.color.a * _ColorFoam.a;
					fixed3 foamColored = foam * _ColorFoam;

					fixed3 details = wavesColored + foamColored;
					fixed detailsMask = 1 - (waves + foam);

					float2 refrSacaled = i.uv * _ScaleRefraction;
					float2 distrOffset = offsetYnoDistr(refrSacaled, _SpeedRefr);
					float distrOfsetted = tex2D(_MainTex, distrOffset).b * i.color.a;

					float2 grabOffset = i.grabPos.xy + (distrOfsetted * 2 - 1) * _DistrRefr * distrOfsetted;
					fixed4 bgcolor = tex2D(_BackgroundTexture, grabOffset);
					bgcolor *= detailsMask;

					fixed4 result = fixed4 (bgcolor.rgb + details, alpha);

					return result;
				}
				ENDCG
			}
		}
}