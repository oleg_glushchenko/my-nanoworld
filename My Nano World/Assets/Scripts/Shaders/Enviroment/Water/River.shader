﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/Enviroment/River"
{
	Properties
	{
		_Mask ("Mask", 2D) = "white" {}
		_Waves("Wave", 2D) = "white" {}
		_DeepColor("DeepColor", Color) = (1,1,1,1)
		_LightColor("LightColor", Color) = (1,1,1,1)
		_WaveHeight("WaveHeight", float) = 0
		_WaveDirection("Wave Direction xy, z speed", Vector) = (1,1,0,0)
	}
	SubShader
	{
		Tags 
		{ 
			"RenderType"="Opaque" 
		}
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 texcoord : TEXCOORD0;
			};

			struct OUT
			{
				
				float4 vertex : SV_POSITION;
				float2 mask_UV : TEXCOORD0;
				float2 waves_UV : TEXCOORD1;
			};

			sampler2D _Mask;
			float4 _Mask_ST;
			sampler2D _Waves;
			float4 _Waves_ST;

			
			half4 _DeepColor;
			half4 _LightColor;
			half _WaveHeight;
			half4 _WaveDirection;
			
			OUT vert (appdata IN)
			{
				OUT o;
				float4 pos = IN.vertex;
				float speed = _Time.w / 4.0;
				float animTime = IN.texcoord.y *_WaveDirection.y * 20 + IN.texcoord.x* _WaveDirection.x * 20 + speed;
				float wave = cos(_WaveDirection.z*animTime);
				pos.y += wave * _WaveHeight;

				o.vertex = UnityObjectToClipPos(pos);
				o.mask_UV = TRANSFORM_TEX(IN.texcoord, _Mask);
				o.waves_UV = TRANSFORM_TEX(IN.texcoord, _Waves);


				return o;
			}
			
			fixed4 frag (OUT i) : SV_Target
			{
				fixed4 mask = tex2D(_Mask, i.mask_UV);
				fixed4 waves = tex2D(_Waves, i.waves_UV);
				// sample the texture
				fixed4 col =  lerp(_DeepColor, _LightColor, mask.r);
				//col.rgb += waves.rgb;

				return col;
			}
			ENDCG
		}
	}
}
