﻿Shader "Custom/Transparent/Building Fill Amount"
{
	Properties 
	{
		_Color ("Main Color", Color) = (1,1,1,1)
		_MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
		_Ramp("Ramp", 2D) = "white" {}
		_MainTexBias ("Mip Bias (-1 to 1)", float) = -1.65
		_FillAmount("_FillAmount", Range(0,1)) = 1
		_FillAmountX("_FillAmount X", Range(0,1)) = 1
		_AtlasUVs("x-min x, y - max x, z - min y, w -max y", Vector) = (0,1,0,1)
		_RotationAngle("Rotation Angle", Range(0,360)) = 0

	}
	SubShader 
	{
		Tags{ "Queue" = "Transparent" }
		Cull back
		Blend SrcAlpha OneMinusSrcAlpha
		Lighting Off
		

		Pass
		{
			
			ZWrite Off
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			 #include "UnityCG.cginc"
	        
			uniform sampler2D _MainTex;
			uniform sampler2D _Ramp;
			uniform float4 _MainTex_ST;
			uniform fixed4 _Color;	
			uniform float _MainTexBias;
			uniform fixed _FillAmount;
			uniform fixed _FillAmountX;
			uniform float4 _AtlasUVs;
			uniform float _RotationAngle;

			struct v2f 
			{
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
			};

			v2f vert(appdata_base i)
			{
				v2f o;
				o.uv = TRANSFORM_TEX (i.texcoord, _MainTex);
				o.pos = UnityObjectToClipPos(i.vertex);
				return o;
			}

			float2 rotatePoint(float2 p, float angle)
			{
				float redRotAngle = angle*0.0174533;
				float sinX = sin(redRotAngle);
				float cosX = cos(redRotAngle);
				float2x2 rotationMatrix = float2x2(cosX, -sinX, sinX, cosX);

				float2 result = mul(p, rotationMatrix);
				
				return result;
			}

			float GetNewUVCoord(float val, float min, float max)
            {
            	float oldRange = max - min;
            	return (val - min)/oldRange;
            }

			half4 frag(v2f i) : COLOR 
			{
				fixed4 col = tex2Dbias(_MainTex, half4(i.uv.x, i.uv.y,0.0, _MainTexBias)) * _Color; 

				fixed invertFill = 1-_FillAmount;
				fixed invertFillX = 1 - _FillAmountX;

				float2 c = i.uv;
				float2 uv = i.uv;

				float2 point1 = float2(0,0);
				float2 point2 = float2(1,0);
				point2 = rotatePoint(point2, _RotationAngle);

				float2 dir = point2-point1;
				float2 minPoint = float2(_AtlasUVs.x, _AtlasUVs.w);
				float2 maxPoint = float2(_AtlasUVs.y, _AtlasUVs.z);
				float2 point0 = lerp(minPoint, maxPoint, _FillAmount);
				float2 pointQ = float2(dir.x+point0.x,dir.y+point0.y);

				float2 a = point0;
				float2 b = pointQ;

				float d = (c.x - a.x)*(b.y-a.y) - (c.y-a.y)*(b.x-a.x);

				col.a *= step(d, 0);

				float newX = GetNewUVCoord(i.uv.x, _AtlasUVs.x, _AtlasUVs.y);
				float x = 1 - newX;

				fixed4 ramp = tex2D(_Ramp, half2(newX, 0));

				col.a *= step(invertFillX, ramp.r);
				
				return col;
			}
			ENDCG
		}
	} 
}
