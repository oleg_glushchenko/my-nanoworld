﻿Shader "NanoReality/Environment/Map_Paralax"
{
	Properties
	{
		_MainTex("ForegroundTexture", 2D) = "white" {}
		_BgTex("BackgroundTexture", 2D) = "white" {}
		_LayerMask("LayersMask", 2D) = "white" {}
		_Height("Height", Range(-100, 100)) = 1
		_VertOffset("VerticalOffset", Float) = 0
		_HorOffset("HorizontalOffset", Float) = 0
		_VertScale("VerticalScale", Float) = 1
		_HorScale("HorizontalScale", Float) = 1
	}
		SubShader
		{
			Tags { "RenderType" = "Map" }
			LOD 100
			Cull Off
			//ZWrite On
			ZWrite Off
			Blend SrcAlpha OneMinusSrcAlpha
			Pass
			{
				CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag

				#include "UnityCG.cginc"
				#include "../CGincludes/MerlinVertexTrix.cginc"
				#include "../CGincludes/MerlinUV.cginc"
				#include "../CGincludes/MerlinViewDirPos.cginc"
				#include "../CGincludes/CloudsAnimation.cginc"

				sampler2D_half _MainTex;
				fixed4 _MainTex_ST;
				sampler2D_half _BgTex;
				fixed4 _BgTex_ST;
				sampler2D_half _LayerMask;
				fixed4 _LayerMask_ST;
				half4 _CloudsTex_ST;

				half _Height;
				half _VertOffset;
				half _HorOffset;
				half _VertScale;
				half _HorScale;

				const fixed one = 1;
				const fixed zero = 0;

				struct appdata
				{
					half4 vertex    : POSITION;
					half3 normal    : NORMAL;
					half2 uv		: TEXCOORD0;
					half3 viewDir: TEXCOORD2;
				};

				struct v2f
				{
					half2 uv : TEXCOORD0;
					half2 uv2 : TEXCOORD1;
					half4 vertex : SV_POSITION;
					half3 viewDir: TEXCOORD2;
					half2 worldUv: TEXCOORD3;
					float4 time : TANGENT;
				};

				v2f vert(appdata v)
				{
					fixed4 worldPos = mul(unity_ObjectToWorld, v.vertex);
					v2f o;
					o.vertex = UnityObjectToClipPos(v.vertex);
					o.viewDir = normalize(UnityWorldSpaceViewDir(worldPos.xyz));
					o.uv = TRANSFORM_TEX(v.uv, _MainTex);
					half2 changedUV2 = half2(v.uv.x * _HorScale + _HorOffset, v.uv.y * _VertScale + _VertOffset);
					half2 p = ParallaxMapping_mp(changedUV2, float2(0,1), o.viewDir, _Height);
					o.uv2 = TRANSFORM_TEX(p, _BgTex);
					o.worldUv = CloudsUV(v.vertex);
					o.time = (_Time % 200) / 200;

					return o;
				}

				fixed4 frag(v2f i) : SV_Target
				{

					fixed4 foregroundTex = tex2D(_MainTex, i.uv);
					fixed3 foregroundColor = foregroundTex.rgb;
					fixed3 foregroundMask = tex2D(_LayerMask, i.uv);
					fixed3 foreground = foregroundColor * foregroundMask;

					fixed3 backgroundColor = tex2D(_BgTex, i.uv2).rgb; //i.uv2 = paralaxed in vert func
					fixed backgroundMask = 1 - foregroundMask;
					fixed3 background = backgroundColor * backgroundMask;

					fixed4 allLand = fixed4(background + foreground, one);

					//Clouds
					fixed4 result = CloudsMix(CloudsAmount(i.worldUv, i.time * 200) , allLand);

					result.a = // .5; 
					/**/	min(
						pow(clamp((i.uv.x) * 512 - .2, 0, 1), 2),
						pow(clamp((1 - i.uv.y) * 512 + .5, 0, 1), 4)
						);
					//,     pow(clamp( (1 - i.uv.y) * 512 + .7, 0, 1), 4)    );

				return result;
			}
			ENDCG
		}
		}
}
