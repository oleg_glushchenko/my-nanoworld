Shader "NanoReality/Environment/WaterLite/WaterfallFoam"
{
	Properties {
	    _TintColor ("Tint Color", Color) = (0.5,0.5,0.5,0.5)
	    _MainTex ("Particle Texture", 2D) = "white" {}
	    _InvFade ("Soft Particles Factor", Range(0.01,3.0)) = 1.0
	    _Speed("_Speed", float) = 1.0
	}
	
	Category {
	    Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" "PreviewType"="Plane" }
	    Blend SrcAlpha OneMinusSrcAlpha
	    ColorMask RGB
	    Cull Off Lighting Off ZWrite Off
	
	    SubShader {
	        Pass {
	
	            CGPROGRAM
	            #pragma vertex vert
	            #pragma fragment frag
	            #pragma multi_compile_particles
	
	            #include "UnityCG.cginc"
	
	            sampler2D _MainTex;
	            fixed4 _TintColor;
	
	            struct appdata_t {
	                float4 vertex : POSITION;
	                fixed4 color : COLOR;
	                float2 texcoord : TEXCOORD0;
	                UNITY_VERTEX_INPUT_INSTANCE_ID
	            };
	
	            struct v2f {
	                float4 vertex : SV_POSITION;
	                fixed4 color : COLOR;
	                float2 texcoord : TEXCOORD0;
	                #ifdef SOFTPARTICLES_ON
	                float4 projPos : TEXCOORD2;
	                #endif
	            };
	
	            float4 _MainTex_ST;
	
	            v2f vert (appdata_t v)
	            {
	                v2f o;
	                UNITY_SETUP_INSTANCE_ID(v);
	                o.vertex = UnityObjectToClipPos(v.vertex);
	                o.color = v.color * _TintColor;
	                o.texcoord = TRANSFORM_TEX(v.texcoord,_MainTex);
	                return o;
	            }
	
	            float _InvFade;
	            float _Speed;
	
						float mod(float x, float y = 1) {
							return x - y * floor(x / y);
						}
						
	            fixed4 frag (v2f i) : SV_Target
	            {
	                fixed4 col = 2.0f * i.color * tex2D(_MainTex, i.texcoord+float2(0,  _Time.x*_Speed));
	                float fadeByY = clamp(0,1,( 1- abs( i.texcoord.y -.5)*2)*4);
	                col.a = saturate(col.a)*fadeByY; 
	                return col;
	            }
	            ENDCG
	        }
	    }
	}
}
