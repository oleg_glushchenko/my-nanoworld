﻿Shader "NanoReality/VFX/TextureVertexAnim"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_PosTex("PositionTexture", 2D) = "black" {}
		_vertexFixer("VertexFixer", float) = 1
		_Length("AnimationLength", float) = 1
		_DT("DeltaTime", float) = 1
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
			#pragma multi_compile_instancing
			#pragma fragmentoption ARB_precision_hint_fastest

            #include "UnityCG.cginc"

	#define ts _PosTex_TexelSize

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
				float velocity : TEXCOORD1;
				fixed4 color : COLOR;
				UNITY_VERTEX_INPUT_INSTANCE_ID
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 position : SV_POSITION;
				float3 normal : NORMAL;
            };

            uniform sampler2D _MainTex;
            uniform float4 _MainTex_ST;
			uniform sampler2D _PosTex;
			uniform float4 _PosTex_ST;
			uniform float4 _PosTex_TexelSize;
			uniform float _Length, _DT, _vertexFixer;

			v2f vert(appdata v, uint vid : SV_VertexID)
			{
				v2f o;

				UNITY_SETUP_INSTANCE_ID(v);

				float t = (_Time.y - v.color.r ) / _Length; //v.color was _DT
				t = fmod(t, 1.0);

				float x = (vid + 0.5) * ts.y;
				float y = t;

				float4 pos = tex2Dlod(_PosTex, float4(x, y, 0, 0));
				pos.xyz += v.vertex;

				float4 transformedPos = mul(unity_ObjectToWorld, pos);
				o.position = UnityObjectToClipPos(pos);
				o.uv = v.uv;
				return o;
			}

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = tex2D(_MainTex, i.uv);
                return col;
            }
            ENDCG
        }
    }
			FallBack "Unlit/Transparent"
}
