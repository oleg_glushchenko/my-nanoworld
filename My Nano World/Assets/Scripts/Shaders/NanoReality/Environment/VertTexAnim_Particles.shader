﻿Shader "NanoReality/VFX/TextureVertexAnimParticles"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
		_PosTex("PositionTexture", 2D) = "black" {}
		_vertexFixer("VertexFixer", float) = 1
		_Length("AnimationLength", float) = 1
		_DT("DeltaTime", float) = 1
	}
		SubShader
		{
			Tags { "RenderType" = "ReplacementVertAnimParticles" }
			LOD 100

			Pass
			{
				CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#pragma multi_compile_instancing
				#pragma fragmentoption ARB_precision_hint_fastest

				#include "UnityCG.cginc"

		        #define ts _PosTex_TexelSize

				struct appdata
				{
					float4 vertex : POSITION;
					float4 uv : TEXCOORD0;
					float4 color : COLOR;
					float4 velocity : TEXCOORD1;
					UNITY_VERTEX_INPUT_INSTANCE_ID
				};

				struct v2f
				{
					float2 uv : TEXCOORD1;
					float4 position : SV_POSITION;
					float3 normal : NORMAL;
				};

				uniform sampler2D_half _MainTex;
				uniform float4 _MainTex_ST;
				uniform sampler2D_half _PosTex;
				uniform float4 _PosTex_ST;
				uniform float4 _PosTex_TexelSize;
				uniform float _Length, _DT, _vertexFixer;

				v2f vert(appdata v/*, uint vid : SV_VertexID*/)
				{
					v2f o;

					UNITY_SETUP_INSTANCE_ID(v);

					float t = (_Time.y - v.color.r) / _Length;
					t = fmod(t, 1.0);

					float x = (v.uv.z + 0.5) * ts.y;
					float y = t;

					float4 pos = tex2Dlod(_PosTex, float4(x, y, 0, 0));
					pos.xyz += v.vertex;

					o.position = UnityObjectToClipPos(pos);
					o.uv = v.uv;
					return o;
				}

				float4 frag(v2f i) : SV_Target
				{
					float4 col = tex2D(_MainTex, i.uv);
					return col;
				}
				ENDCG
			}
		}

			FallBack "Unlit/Transparent"
}
