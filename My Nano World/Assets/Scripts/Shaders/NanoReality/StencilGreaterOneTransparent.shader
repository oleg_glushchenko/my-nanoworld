﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/Stencil/Transparent Greater One Transparent"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_RenderQueue("Render Queue", Float) = 2003
	}
	SubShader
	{
		Tags { "RenderType"="StencilMasked" "Queue" = "Transparent" }
		LOD 100

		Cull back
		Blend SrcAlpha OneMinusSrcAlpha
		Lighting Off
			Zwrite Off

		Stencil 
		{
                Ref 1
                Comp Greater
                Pass keep 

       }


		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float4 color : COLOR;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
				float4 color : COLOR;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.color = v.color;
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				fixed4 col = tex2D(_MainTex, i.uv)*i.color;
				return col;
			}
			ENDCG
		}
	}
}
