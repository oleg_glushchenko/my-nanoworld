#if !defined(MERLINViEWDIRPOS_INCLUDED)
#define MERLINViEWDIRPOS_INCLUDED

//Get Paralax
float2 ParallaxMapping_hp(float2 uv, float2 axisMask, float3 viewDir, float height)
{
	float2 p = viewDir.xy / viewDir.z * height;
	float2 pMasked = p * axisMask;
	float2 offset = uv - pMasked;
	return float2 (uv.x, uv.y);
}

half2 ParallaxMapping_mp(half2 uv, half2 axisMask, half3 viewDir, half height)
{
	half2 p = viewDir.xy / viewDir.z * height;
	half2 pMasked = p * axisMask;
	half2 offset = uv - pMasked;
	return half2 (uv.x, uv.y);
}

fixed2 ParallaxMapping_lp(fixed2 uv, fixed2 axisMask, fixed3 viewDir, fixed height)
{
	fixed2 p = viewDir.xy / viewDir.z * height;
	fixed2 pMasked = p * axisMask;
	fixed2 offset = uv - pMasked;
	return fixed2 (uv.x, uv.y);
}

#endif