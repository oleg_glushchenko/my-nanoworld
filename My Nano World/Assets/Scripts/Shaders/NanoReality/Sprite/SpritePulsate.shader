﻿Shader "NanoReality/Sprite/Pulsate"
{
	Properties
	{
		[PerRendererData] _MainTex("Sprite Texture", 2D) = "white" {}
		_Color("Tint", Color) = (1,1,1,1)
		_PulseSpeed("Pulse Speed", Float) = 5
		[MaterialToggle] PixelSnap("Pixel snap", Float) = 0
		[HideInInspector] _RendererColor("RendererColor", Color) = (1,1,1,1)
		[HideInInspector] _Flip("Flip", Vector) = (1,1,1,1)
		[PerRendererData] _AlphaTex("External Alpha", 2D) = "white" {}
		[PerRendererData] _EnableExternalAlpha("Enable External Alpha", Float) = 0
		
		_ColorOut("OutlineColor", Color) = (1,1,1,1)
		_MeshScaleX("MeshScaleX", Range(0,10)) = 1 
		_MeshScaleY("MeshScaleY", Range(0,10)) = 1
		_MeshOffsetX("MeshOffsetX", Range(-2,2)) = 1
		_MeshOffsetY("MeshOffsetY", Range(-2,2)) = 1
	}

	SubShader
	{
		Tags
		{
			"Queue" = "Transparent+6"
			"IgnoreProjector" = "True"
			"RenderType" = "SpritePulsate"
			"PreviewType" = "Plane"
			"CanUseSpriteAtlas" = "True"
		}

		Cull Off
		Lighting Off
		ZWrite Off
		Ztest Always
		Blend One OneMinusSrcAlpha

		Pass
		{
		    Stencil 
		    {
                Ref 1
                Comp always
                Pass replace 
            }
		CGPROGRAM
			#pragma vertex SpriteVert
			#pragma fragment SpriteFrag
			#pragma target 2.0
			#pragma multi_compile_instancing
			#pragma multi_compile_local _ PIXELSNAP_ON
			#pragma multi_compile _ ETC1_EXTERNAL_ALPHA
				
		#ifndef UNITY_SPRITES_INCLUDED
		#define UNITY_SPRITES_INCLUDED

		#include "UnityCG.cginc"

		#ifdef UNITY_INSTANCING_ENABLED

		UNITY_INSTANCING_BUFFER_START(PerDrawSprite)
		// SpriteRenderer.Color while Non-Batched/Instanced.
		UNITY_DEFINE_INSTANCED_PROP(fixed4, unity_SpriteRendererColorArray)
		// this could be smaller but that's how bit each entry is regardless of type
		UNITY_DEFINE_INSTANCED_PROP(fixed2, unity_SpriteFlipArray)
		UNITY_INSTANCING_BUFFER_END(PerDrawSprite)

		#define _RendererColor  UNITY_ACCESS_INSTANCED_PROP(PerDrawSprite, unity_SpriteRendererColorArray)
		#define _Flip           UNITY_ACCESS_INSTANCED_PROP(PerDrawSprite, unity_SpriteFlipArray)

		#endif // instancing

		CBUFFER_START(UnityPerDrawSprite)
		#ifndef UNITY_INSTANCING_ENABLED
		fixed4 _RendererColor;
		fixed2 _Flip;
		#endif
		float _EnableExternalAlpha;
		CBUFFER_END

		// Material Color.
		fixed4 _Color;

        sampler2D _MainTex;
		sampler2D _AlphaTex;
		half _PulseSpeed;
		
		struct appdata_t
		{
			float4 vertex   : POSITION;
			float4 color    : COLOR;
			float2 texcoord : TEXCOORD0;
			UNITY_VERTEX_INPUT_INSTANCE_ID
		};

		struct v2f
		{
			float4 vertex   : SV_POSITION;
			fixed4 color : COLOR;
			float2 texcoord : TEXCOORD0;
			UNITY_VERTEX_OUTPUT_STEREO
		};

		

		inline float4 UnityFlipSprite(in float3 pos, in fixed2 flip)
		{
			return float4(pos.xy * flip, pos.z, 1.0);
		}

		v2f SpriteVert(appdata_t v)
		{
			v2f o;

			UNITY_SETUP_INSTANCE_ID(v);
			UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);

			o.vertex = UnityFlipSprite(v.vertex, _Flip);
			o.vertex = UnityObjectToClipPos(o.vertex);
			o.texcoord = v.texcoord;
			o.color = v.color * _Color * _RendererColor;

			#ifdef PIXELSNAP_ON
			o.vertex = UnityPixelSnap(o.vertex);
			#endif

			return o;
		}

		fixed4 SampleSpriteTexture(float2 uv)
		{
			fixed4 color = tex2D(_MainTex, uv);

			#if ETC1_EXTERNAL_ALPHA
			fixed4 alpha = tex2D(_AlphaTex, uv);
			color.a = lerp(color.a, alpha.r, _EnableExternalAlpha);
			#endif

			return color;
		}

		fixed4 SpriteFrag(v2f i) : SV_Target
		{
			
			fixed4 c = SampleSpriteTexture(i.texcoord) * i.color;
			c.rgb *= c.a;

			fixed4 pulsColor = fixed4(c.rgb * 0.1 + _Color.rgb * c.a, c.a);
			half oscilate = (sin( 1 * _Time.y * _PulseSpeed ) + 1) / 2;
			half4 oscilatedColor = lerp(c, pulsColor, oscilate);
			fixed4 result = fixed4(c.rgb + pulsColor.rgb, c.a);
			
			return oscilatedColor;
		}

		#endif // UNITY_SPRITES_INCLUDED

		ENDCG
		}
	}
}
