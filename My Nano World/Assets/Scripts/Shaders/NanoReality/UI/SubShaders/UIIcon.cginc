#if !defined(UIICON_INCLUDED)
#define UIICON_INCLUDED 

#pragma target 2.0
#include "UnityCG.cginc"
  

sampler2D _MainTex;
fixed4 _Color;

struct appdata
{
	float4 vertex   : POSITION;
	float2 texcoord : TEXCOORD0;
	float4 color : COLOR;
};

struct v2f
{
	float4 vertex   : SV_POSITION;
	float2 texcoord : TEXCOORD0; 
	fixed4 color : COLOR;
};

v2f vert(appdata v)
{
	v2f o;

#ifndef _grayscale
	v.vertex.z = 0.002;
#endif
	o.vertex = UnityObjectToClipPos(v.vertex);
	o.texcoord = v.texcoord; 
	o.color = v.color; 

	return o;
}

fixed4 frag(v2f i) : Color
{
	fixed4 tex1 = tex2D(_MainTex, i.texcoord)* i.color;
	tex1.a = pow(tex1.a , 4);

#ifdef _grayscale
	fixed4 grayscaled = Grayscale(tex1);
	tex1 = lerp(tex1, grayscaled, _grayscale);
#endif

	return tex1;
}
 

#endif