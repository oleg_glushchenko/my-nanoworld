Shader "NanoReality/UI/ScrollTextureFX"
{
    Properties
    {
        [PerRendererData] _MainTex ("Texture", 2D) = "white" {}
        _ScrolledTex ("Scrolled Texture", 2D) = "white" {}
        
        _MaskColor ("MaskColor", Color) = (0,0,0,0)
        
        _StripsScaleX ("Scale X", float) = 1
        _StripsScaleY ("Scale Y", float) = 1
        _StripsShiftY ("Shift Y", float) = 1
        
        _Speed ("_Speed", float) = -2
        
        _ScrolledTex2 ("Scrolled Texture2", 2D) = "white" {}
        _StripsShiftY2 ("Shift Y2", float) = 1
        _Speed2 ("_Speed2", float) = -2
        
		_StencilComp ("Stencil Comparison", Float) = 8
		_Stencil ("Stencil ID", Float) = 0
		_StencilOp ("Stencil Operation", Float) = 0
		_StencilWriteMask ("Stencil Write Mask", Float) = 255
		_StencilReadMask ("Stencil Read Mask", Float) = 255

		_ColorMask ("Color Mask", Float) = 15
    }
    SubShader
    {
        Tags
        {
            "Queue"="Transparent"
            "IgnoreProjector"="True"
            "RenderType"="Transparent"
            "PreviewType"="Plane"
            "CanUseSpriteAtlas"="True"
        }

        LOD 100

		Stencil
		{
			Ref [_Stencil]
			Comp [_StencilComp]
			Pass [_StencilOp] 
			ReadMask [_StencilReadMask]
			WriteMask [_StencilWriteMask]
		}

		Cull Off
		Lighting Off
		ZWrite Off
		ZTest [unity_GUIZTestMode]
		Blend SrcAlpha OneMinusSrcAlpha
		ColorMask [_ColorMask]
		
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float4 color    : COLOR; 
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                half2 screenPos  : TEXCOORD1;
            };

            sampler2D _MainTex;
            sampler2D _ScrolledTex;
            sampler2D _ScrolledTex2;
            
            float4 _MainTex_ST;
                        
            float4 _MaskColor;

            fixed _StripsScaleX;
            fixed _StripsScaleY;
            fixed _StripsShiftY;
            fixed _StripsShiftY2;
            fixed _Speed;
            fixed _Speed2;
            
            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                
                o.screenPos = ComputeScreenPos(o.vertex);
                o.screenPos =(v.vertex)*.001;
                
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = tex2D(_MainTex, i.uv);
                
                float maskColor = length(_MaskColor.rgb-col.rgb) ;
                float mask = smoothstep(.6,.3,maskColor);
                
                fixed4 clouds = tex2D(_ScrolledTex,  float2(frac(i.screenPos.x*_StripsScaleX + _Time.x*_Speed) , clamp(i.screenPos.y*_StripsScaleY +_StripsShiftY ,0,1) ) )*mask; 
                fixed4 clouds2 = tex2D(_ScrolledTex2,  float2(frac(i.screenPos.x*_StripsScaleX + _Time.x*_Speed2) , clamp(i.screenPos.y*_StripsScaleY +_StripsShiftY2 ,0,1) ) )*mask; 
                
                col.rgb = lerp( col.rgb , clouds2.rgb, clouds2.a * clouds2.a); 
                col.rgb = lerp( col.rgb , clouds.rgb, clouds.a * clouds.a); 
                
                return  col  ;
            }
            ENDCG
        }
    }
}
