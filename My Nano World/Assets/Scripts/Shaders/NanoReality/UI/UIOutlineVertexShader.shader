﻿Shader "NanoReality/UI/NanoUIOutlineByVertex"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_OutlineWidth("Width", Float) = 1
		_ColorOutline("Outline Color", Color) = (0,0,0,1)
		[PerRendererData] _Color("Tint Color", Color) = (1,1,1,1)
    } 
	SubShader
	{ 
		Tags
		{
			"Queue" = "Transparent"
			"IgnoreProjector" = "True"
			"RenderType" = "ReplacementBuildingOutline"
			"PreviewType" = "Plane"
			"CanUseSpriteAtlas" = "True"
		}

		Cull Off 
		Lighting Off
		Blend SrcAlpha OneMinusSrcAlpha
		Zwrite Off
		Ztest Less

		 
		Pass
		{  
			Name "Outline - L"
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#define SHIFT float2(1, 0)
			#include "SubShaders/UIOutline.cginc"

			ENDCG
		} 
		Pass
		{
			Name "Outline - R" 

			 CGPROGRAM
			 #pragma vertex vert
			 #pragma fragment frag
			 #define SHIFT float2(-1, 0)
			 #include "SubShaders/UIOutline.cginc"

			 ENDCG
		 }
		 Pass
		 {
			 Name "Outline - U" 

			  CGPROGRAM
			  #pragma vertex vert
			  #pragma fragment frag
			  #define SHIFT float2(0, 1)
			  #include "SubShaders/UIOutline.cginc"

			  ENDCG
		  }
		  Pass
		  {
			  Name "Outline - D" 

			   CGPROGRAM
			   #pragma vertex vert
			   #pragma fragment frag
			   #define SHIFT float2(0, -1)
			   #include "SubShaders/UIOutline.cginc"

			   ENDCG
		   }
		   Pass
		   {
			   Name "Default"
				 
			  CGPROGRAM
			   #pragma vertex vert
			   #pragma fragment frag
			   #include "SubShaders/UIIcon.cginc" 
			   ENDCG
		   }
	}
	FallBack "NanoReality/UI/NanoOutline" 
}
