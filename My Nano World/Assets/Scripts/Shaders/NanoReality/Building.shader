﻿Shader "Custom/Transparent/Building"
{
	Properties 
	{
		_Color ("Main Color", Color) = (1,1,1,1)
		_MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
		_MainTexBias ("Mip Bias (-1 to 1)", float) = -1.65
	}
	SubShader 
	{

		Tags{ "Queue" = "Transparent"  "RenderType"="ReplacementBuilding"}
		Cull back
		Blend SrcAlpha OneMinusSrcAlpha
		Lighting Off

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
	        #include "UnityCG.cginc"
	        
			uniform sampler2D _MainTex;
			uniform float4 _MainTex_ST;
			uniform fixed4 _Color;	
			uniform fixed _MainTexBias;

			struct v2f 
			{
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
			};

			v2f vert(appdata_base i)
			{
				v2f o;
				o.uv = TRANSFORM_TEX (i.texcoord, _MainTex);
				o.pos = UnityObjectToClipPos(i.vertex);
				return o;
			}

			half4 frag(v2f i) : COLOR 
			{
				return  tex2Dbias(_MainTex, half4(i.uv.x, i.uv.y,0.0, _MainTexBias)) * _Color;
			}
			ENDCG
		}
	} 
}
