#if !defined(BIRDS_INCLUDED)
#define BIRDS_INCLUDED 

#pragma multi_compile_instancing
#pragma fragmentoption ARB_precision_hint_fastest

#include "UnityCG.cginc"
#define ts _PosTex_TexelSize

	struct appdata
	{
		float4 vertex : POSITION;
		float4 uv : TEXCOORD0;
		fixed4 color : COLOR;
		float4 velocity : TEXCOORD1;
		UNITY_VERTEX_INPUT_INSTANCE_ID
	};

	struct v2f
	{
		float2 uv : TEXCOORD1;
		float4 position : SV_POSITION;
		float2 screenUv : TEXCOORD2;
	};

	uniform sampler2D _PosTex;
	uniform float4 _PosTex_ST;
	uniform float4 _PosTex_TexelSize;
	uniform float _Length, _DT, _vertexFixer;

	v2f vert(appdata v)
	{
		v2f o;
		UNITY_SETUP_INSTANCE_ID(v);
		
		float t = (_Time.y - v.color.r) / _Length;
		t = fmod(t, 1.0);
		float x = (v.uv.z + 0.5) * ts.y;
		float y = t;
		float4 pos = tex2Dlod(_PosTex, float4(x, y, 0, 0));
		pos.xyz += v.vertex;
		o.position = UnityObjectToClipPos(pos);
		o.uv = v.uv;
		o.screenUv = GetScreenSpace(o.position);
		return o;
	}

	fixed4 frag(v2f i) : SV_Target
	{
		fixed4 col = tex2D(_MainTex, i.uv);
	    return Grayscale(col, i.screenUv);
	}
#endif