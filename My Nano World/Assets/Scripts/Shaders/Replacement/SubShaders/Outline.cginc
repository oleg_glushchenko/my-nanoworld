#if !defined(OUTLINE_INCLUDED)
    #define OUTLINE_INCLUDED 
    
    #pragma target 2.0
    #include "UnityCG.cginc"
    
    fixed4 _ColorOut;
    fixed _Clipping;
    float _OutlineWidth;
    float _UvOffset;
    float _MeshScaleX;
    float _MeshScaleY;
    float _MeshOffsetX;
    float _MeshOffsetY;
    
    struct appdata
    {
        float4 vertex   : POSITION;
        float2 texcoord : TEXCOORD0;
    };
    
    struct v2f
    {
        float4 vertex   : SV_POSITION;
        float2 texcoord : TEXCOORD0;
        fixed4 color : COLOR;
    };
    
    v2f SpriteVert(appdata v)
    {
        v2f o;
        float s = 1.3;
        v.vertex.z = .001;
        v.vertex.x += SHIFT.x*_OutlineWidth*s;
        v.vertex.y += SHIFT.y*_OutlineWidth*s; 
        o.vertex = UnityObjectToClipPos(v.vertex);
        o.texcoord = v.texcoord;
        o.color = _ColorOut;
        return o;
    }
    
    fixed4 SpriteFrag(v2f i) : Color
    {
        fixed4 tex1 = tex2D(_MainTex, i.texcoord);
        float a = pow(tex1.a , 4) ;
        clip(a-_Clipping);
        return fixed4(i.color.rgb, a);
    }
#endif