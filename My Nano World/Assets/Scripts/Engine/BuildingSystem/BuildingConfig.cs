using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using UnityEngine;

namespace NanoReality.Engine.BuildingSystem
{
    [CreateAssetMenu(fileName = "NewBuildingConfig", menuName = "NanoReality/Buildings/BuildingConfig", order = 1)]
    public sealed class BuildingConfig : ScriptableObject
    {
        [SerializeField] private int _buildingId;

        [SerializeField] private List<BuildingLevelViewData> _viewsList;

        public BuildingConfig()
        {
            _viewsList = new List<BuildingLevelViewData>();
        }

        public BuildingConfig(int buildingId)
        {
            _buildingId = buildingId;
            _viewsList = new List<BuildingLevelViewData>();
        }
        
        public Int32 BuildingId => _buildingId;

        public List<BuildingLevelViewData> ViewsList => _viewsList;

        public MapObjectView GetBuildingView(int buildingLevel)
        {
            return _viewsList.FirstOrDefault(c => c.Level == buildingLevel)?.View;
        }

        public Sprite GetBuildingSprite(int buildingLevel)
        {
            return _viewsList.FirstOrDefault(c => c.Level == buildingLevel)?.Sprite;
        }
    }

    [Serializable]
    public sealed class BuildingLevelViewData
    {
        [SerializeField] private int _level;
        [SerializeField] private MapObjectView _view;
        [SerializeField] private Sprite _sprite;

        public Int32 Level => _level;

        public MapObjectView View => _view;

        public Sprite Sprite
        {
            get => _sprite;
            set => _sprite = value;
        }

        public BuildingLevelViewData()
        {
            
        }

        public BuildingLevelViewData(int level, MapObjectView view)
        {
            _level = level;
            _view = view;
        }


    }
}