using System.Collections.Generic;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Assets.Scripts.Engine.BuildingSystem.CityMap.Highlightings
{
    [CreateAssetMenu(fileName = "AoeMaterials", menuName = "NanoReality/Settings/AoeMaterials", order = 1)]
    public class AoeMaterials : SerializedScriptableObject
    {
        [SerializeField] private Material _defaultMaterial;
        [SerializeField] private Dictionary<MapObjectintTypes, Material> _aoeMaterial = new Dictionary<MapObjectintTypes, Material>();

        public Material GetMaterial(MapObjectintTypes type)
        {
            if (_aoeMaterial.TryGetValue(type, out var targetMaterial)) return targetMaterial;
            Debug.LogError($"No such material for building type {type}");
            return _defaultMaterial;
        }
    }
}