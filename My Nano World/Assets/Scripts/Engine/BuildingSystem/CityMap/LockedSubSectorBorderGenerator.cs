﻿using Assets.Scripts.Engine.BuildingSystem.CityMap;
using System.Collections.Generic;
using System.Linq;
using strange.extensions.mediation.impl;
using UnityEngine;
using NanoReality.GameLogic.Utilities;

public class LockedSubSectorBorderGenerator : View
{
    public float BorderWidth = 0.2f;
    public float TilingPerUnit = 4f;
    public MeshFilter MeshFilter;

    private readonly List<Vector3> _vertexes = new List<Vector3>();
    private readonly List<Vector2> _uv = new List<Vector2>();

    public void ShowBorders(IEnumerable<LockedSectorView> sectors)
    {
        foreach (var sector in sectors)
            sector.ShowFences();
    }

    public void CreateMesh(IEnumerable<LockedSectorView> sectors, float depth)
    {
        if(sectors.Count() > 0)
        {
            var firstSector = sectors.FirstOrDefault();
            if(firstSector != null && firstSector.HavePrefab)
            {
                ShowBorders(sectors);
                return;
            }
        }

        var mesh = new Mesh();

        _vertexes.Clear();
        _uv.Clear();
        foreach (var sector in sectors)
        {
            AddVertex(sector.WorldSpacePosition.x, sector.WorldSpacePosition.y, sector.WorldSpaceDimensions.x, sector.WorldSpaceDimensions.y);
        }

        var triangles = new int[_vertexes.Count/4*6];

        var index = 0;

        for (var i = 0; i < _vertexes.Count/4*6;)
        {
            triangles[i++] = index;
            triangles[i++] = index + 1;
            triangles[i++] = index + 2;
            triangles[i++] = index + 2;
            triangles[i++] = index + 3;
            triangles[i++] = index;
            index += 4;
        }

        mesh.vertices = _vertexes.ToArray();
        mesh.uv = _uv.ToArray();
        mesh.triangles = triangles;
        mesh.RecalculateBounds();
        mesh.RecalculateNormals();
        mesh.name = "borderMesh";

        MeshFilter.mesh = mesh;

        // TODO: magic variable. Need for correct little borders between locked subsectors.
        const float depthConst = 1.01f;
        var height = depth + depthConst;

        var izoPosition = Vector3.zero;
        ;
        var depthOffset = MapObjectGeometry.GetHeightOffset(height, 30);
        izoPosition.x -= depthOffset;
        izoPosition.z -= depthOffset;
        izoPosition.y = height;
        transform.localPosition = izoPosition;

    }

	private void AddVertex(float posX, float posY, float sizeX, float sizeY)
    {
        var border = BorderWidth/2.0f;

        // add left border

        _vertexes.Add(new Vector3(posX  - border, 0, posY ));
        _vertexes.Add(new Vector3(posX  - border, 0, posY + sizeY));
        _vertexes.Add(new Vector3(posX  + border, 0, posY + sizeY));
        _vertexes.Add(new Vector3(posX  + border, 0, posY ));

        _uv.Add(Vector2.zero);
        _uv.Add(new Vector2(0, sizeY*TilingPerUnit));
        _uv.Add(new Vector2(1, sizeY*TilingPerUnit));
        _uv.Add(new Vector2(1, 0));

        // add right border

        _vertexes.Add(new Vector3(posX +  sizeX - border, 0, posY ));
        _vertexes.Add(new Vector3(posX +  sizeX - border, 0, posY +  sizeY));
        _vertexes.Add(new Vector3(posX +  sizeX + border, 0, posY + sizeY));
        _vertexes.Add(new Vector3(posX +  sizeX + border, 0, posY ));

        _uv.Add(Vector2.zero);
        _uv.Add(new Vector2(0, sizeY*TilingPerUnit));
        _uv.Add(new Vector2(1, sizeY*TilingPerUnit));
        _uv.Add(new Vector2(1, 0));

        // add top border

        _vertexes.Add(new Vector3(posX , 0, posY +  sizeY - border));
        _vertexes.Add(new Vector3(posX , 0, posY +  sizeY + border));
        _vertexes.Add(new Vector3(posX +  sizeX, 0, posY +  sizeY + border));
        _vertexes.Add(new Vector3(posX +  sizeX, 0, posY +  sizeY - border));

        _uv.Add(Vector2.zero);
        _uv.Add(new Vector2(0, 1));
        _uv.Add(new Vector2(sizeX*TilingPerUnit, 1));
        _uv.Add(new Vector2(sizeX*TilingPerUnit, 0));

        // add bottom border

        _vertexes.Add(new Vector3(posX , 0, posY  - border));
        _vertexes.Add(new Vector3(posX , 0, posY  + border));
        _vertexes.Add(new Vector3(posX +  sizeX, 0, posY  + border));
        _vertexes.Add(new Vector3(posX +  sizeX, 0, posY  - border));

        _uv.Add(Vector2.zero);
        _uv.Add(new Vector2(0, 1));
        _uv.Add(new Vector2(sizeX*TilingPerUnit, 1));
        _uv.Add(new Vector2(sizeX*TilingPerUnit, 0));
    }
}
