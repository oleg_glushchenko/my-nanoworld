﻿using System.Linq;
using Assets.NanoLib.Utilities.Pulls;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Impl;
using NanoReality.GameLogic.Settings.BuildSettings.Models.Api;
using NanoReality.GameLogic.Utilities;
using strange.extensions.mediation.impl;
using UnityEngine;


namespace Assets.Scripts.Engine.BuildingSystem.CityMap
{
    public class LockedSectorView : View, IPullableObject
    {
        [SerializeField] private Material _lockedSectorMaterial;

        [SerializeField] private Material _readySectorMaterial;
        [SerializeField] private Material _havePrefabMaterial;
        [SerializeField] private MeshFilter _mesh;
        [SerializeField] private Renderer _renderer;
        
        private ICityMapSettings _cityMapSettings;
        private CityLockedSector _model;
        private bool _havePrefab;
        public bool HavePrefab => _havePrefab;
        
        public MeshFilter Mesh => _mesh;
        public readonly float Depth = 0.45f;
        private GameObject _sectorObject;
        private Transform _sign;

        public Vector2 LeftBottomGridPos => _model.LockedSector.LeftBottomGridPos;

        public Vector2 WorldSpacePosition => _cityMapSettings.GridCellScale * LeftBottomGridPos;

        public Vector2 GridDimensions => _model.LockedSector.GridDimensions;

        public Vector2 WorldSpaceDimensions => _cityMapSettings.GridCellScale * GridDimensions;

        public CityLockedSector Model => _model;

        public void Initialize(CityLockedSector cityLockedSector, ICityMapSettings cityMapSettings)
        {
            _model = cityLockedSector;
            _cityMapSettings = cityMapSettings;
        }
        
        public void UpdateView()
        {
            if (!_model.LockedSector.Unlockable)
            {
                   HideAllFences();
            }
                
            _renderer.material = _havePrefab ? _havePrefabMaterial :
                _model.LockedSector.Unlockable ? _readySectorMaterial : _lockedSectorMaterial;
        }

        public void UpdateDepth()
        {
            var transformLocalScale = new Vector3(WorldSpaceDimensions.x, WorldSpaceDimensions.y);
            transform.localScale = transformLocalScale;
            
            var height = Depth;
            var izoPosition = new Vector3(WorldSpacePosition.x, 0, WorldSpacePosition.y);
            var depthOffset = MapObjectGeometry.GetHeightOffset(height, 30);
            izoPosition.x -= depthOffset;
            izoPosition.z -= depthOffset;
            izoPosition.y = height;

            float localPositionX = izoPosition.x + 0.5f * (WorldSpaceDimensions.x);
            float localPositionY = izoPosition.y;
            float localPositionZ = izoPosition.z + 0.5f * (WorldSpaceDimensions.y);
            
            transform.localPosition = new Vector3(localPositionX, localPositionY, localPositionZ);
            
            TryToInsertPrefab(izoPosition, transformLocalScale);
        }

        private GameObject GetAreaPrefab()
        {
            var sectorName = $"{(BusinessSectorsTypes) _model.LockedSector.BusinessSectorId.Value}_{_model.LockedSector.X}x{_model.LockedSector.Y}";
            var sectorPrefab = _cityMapSettings.LockedSectorsPrefabs.FirstOrDefault(s => s.name.Contains(sectorName));
            if(!sectorPrefab)
                return null;
            return Instantiate(sectorPrefab);
        }

        private void TryToInsertPrefab(Vector3 izoPosition, Vector3 transformLocalScale)
        {
            _sectorObject = GetAreaPrefab();
            if (!_sectorObject)
            {
                return;
            }

            _havePrefab = true;

            transform.localScale = Vector3.one;
            transform.localPosition = izoPosition;

            var collider = GetComponent<BoxCollider>();
            collider.size = transformLocalScale;
            collider.center = transformLocalScale / 2;

            _sectorObject.transform.SetParent(transform);
            _mesh.transform.localScale = transformLocalScale;
            _mesh.transform.localPosition = transformLocalScale / 2;

            _sectorObject.transform.rotation = Quaternion.Euler(30, 45, 0);
            _sectorObject.transform.position = MapObjectGeometry.PlaceObjectInDepth(transform.position, 1.4f);
            _sign = _sectorObject.transform.GetChild(0).GetChild(4);
        }

        public void HideAllFences()
        {
            _sign?.gameObject.SetActive(false);
        }

        public void ShowFences()
        {
            _sign?.gameObject.SetActive(true);
        } 

        #region Pullable
        public object Clone()
        {
            return Instantiate(this);
        }

        public IObjectsPull pullSource { get; set; }
        public void OnPop()
        {
            gameObject.SetActive(true);
            enabled = true;
        }

        public void OnPush()
        {
            gameObject.SetActive(false);
        }

        public void FreeObject()
        {
            if (pullSource != null)
            {
                pullSource.PushInstance(this);
            }
            else
            {
                DestroyObject();
            }
        }

        public void DestroyObject()
        {
            Destroy(this);
        }
        #endregion
    }
}