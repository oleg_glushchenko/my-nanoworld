﻿using System;
using Assets.NanoLib.UI.Core.Signals;
using Assets.NanoLib.UI.Core.Views;
using Assets.Scripts.Engine.BuildingSystem.CityMap;
using Assets.Scripts.Engine.UI.Views.Popups;
using DG.Tweening;
using Engine.UI.Views.Hud.ButtonNotifications;
using NanoLib.Services.InputService;
using NanoLib.UI.Core;
using NanoReality.Engine.UI.Extensions.AchievementPanel;
using NanoReality.Engine.UI.Extensions.Agricultural;
using NanoReality.Engine.UI.Extensions.Hud;
using NanoReality.Engine.UI.Extensions.MapObjects.CityHallTaxes;
using NanoReality.Engine.UI.Views.Hud;
using NanoReality.Engine.UI.Views.LevelUpPanel;
using NanoReality.Game.Tutorial;
using NanoReality.GameLogic.BuildingSystem;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityMap.Controllers;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Impl;
using NanoReality.GameLogic.Common.Controllers.Signals;
using NanoReality.GameLogic.GameManager.Models.api;
using NanoReality.GameLogic.MinePanel;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace NanoReality.Engine.BuildingSystem.CityMap
{
    public sealed class CityMapMediator : Mediator
    {
        private bool _animationInProgress;
        private Tween _changeStateAnimation;
        
        #region Injections
        
        [Inject] public IUIManager jUIManager { get; set; }
        [Inject] public IHardTutorial jHardTutorial { get; set; }
        [Inject] public CityMapView jMapView { get; set; }
        [Inject] public IGameManager jGameManager { get; set; }
        [Inject] public MapObjectAddedSignal MapObjectAddedSignal { get; set; }
        [Inject] public IConstructionController jConstructionController { get; set; } 
        [Inject] public LockedSectorAddedSignal jLockedSectorAddedSignal { get; set; }
        [Inject] public SignalRemoveMapObjectView SignalRemoveMapObjectView { get; set; }
        [Inject] public SignalMoveMapObjectView jSignalMoveMapObjectView { get; set; }
        [Inject] public StartCityMapInitializationSignal StartCityMapInitializationSignal { get; set; }
        [Inject] public SignalOnFullClearMap SignalOnFullClearMap { get; set; }
        [Inject] public SignalOnMapObjectUpgradedOnMap jSignalOnMapObjectUpgradedOnMap { get; set; }
        [Inject] public FinishCityMapInitSignal jFinishCityMapInitSignal { get; set; }
		[Inject] public SignalOnMapObjectRemoved SignalOnMapObjectRemoved { get; set; }
        [Inject] public SignalOnSubSectorsUpdated jSignalOnSubSectorsUpdated { get; set; }
        [Inject] public SignalOnSubSectorUnlocked jSignalOnSubSectorUnlocked { get; set; }
        [Inject] public SignalOnLockedSubSectorTap jSignalOnLockedSubSectorTap { get; set; }
        [Inject] public ShowWindowSignal jShowWindowSignal { get; set; }
        [Inject] public PlayerOpenedPanelSignal jUiPanelOpenedSignal { get; set; }

        #endregion

        /// <summary>
        /// Регистрируемся на сигналы приложения
        /// </summary>
        public override void OnRegister()
        {
            base.OnRegister();
            
            jGameManager.RegisterMapView(jMapView);
            
            StartCityMapInitializationSignal.AddListener(OnInitMap);
            SignalOnFullClearMap.AddListener(OnFullClearMap);
            SignalRemoveMapObjectView.AddListener(OnRemoveMapObjectView);
           	MapObjectAddedSignal.AddListener(AddObjectOnMap);
            jSignalOnMapObjectUpgradedOnMap.AddListener(jMapView.UpgradeMapObject);
            jSignalMoveMapObjectView.AddListener(MoveObjectOnMap);
            jFinishCityMapInitSignal.AddListener(OnBuildingSettingOnMap);
			SignalOnMapObjectRemoved.AddListener (OnRemoveMapObject);
            jLockedSectorAddedSignal.AddListener(OnLockedSectorAdded);
            jSignalOnSubSectorsUpdated.AddListener(OnSubSectorsUpdated);
            jSignalOnSubSectorUnlocked.AddListener(OnSubSectorUnlocked);
            jSignalOnLockedSubSectorTap.AddListener(OnLockedSubSectorTap);
        }

        public override void OnRemove()
        {
            base.OnRemove();

            StartCityMapInitializationSignal.RemoveListener(OnInitMap);
            SignalOnFullClearMap.RemoveListener(OnFullClearMap);
            SignalRemoveMapObjectView.RemoveListener(OnRemoveMapObjectView);
            MapObjectAddedSignal.RemoveListener(AddObjectOnMap);
            jSignalOnMapObjectUpgradedOnMap.RemoveListener(jMapView.UpgradeMapObject);
            jSignalMoveMapObjectView.RemoveListener(MoveObjectOnMap);
            jFinishCityMapInitSignal.RemoveListener(OnBuildingSettingOnMap);
            SignalOnMapObjectRemoved.RemoveListener(OnRemoveMapObject);
            jLockedSectorAddedSignal.RemoveListener(OnLockedSectorAdded);
            jSignalOnSubSectorsUpdated.RemoveListener(OnSubSectorsUpdated);
            jSignalOnSubSectorUnlocked.RemoveListener(OnSubSectorUnlocked);
            jSignalOnLockedSubSectorTap.RemoveListener(OnLockedSubSectorTap);
        }

        private void OnBuildingSettingOnMap(ICityMap cityMap)
        {
            if(cityMap.BusinessSector != jMapView.BusinessSector) return;

            jMapView.RoadGrid.SetUpdateRoadTiles(true);
            jMapView.RoadGrid.UpdateRoads();

            jMapView.IsSortEnabled = true;
            jMapView.SortDepthObjects();
        }

        private void OnFullClearMap(ICityMap map)
        {
            if(map.BusinessSector != jMapView.BusinessSector) return;
            jMapView.ClearMap();
        }

        /// <summary>
        /// Задаем настройки вьюшки карты, по сигналу инициализации модели карты
        /// </summary>
        /// <param name="map"></param>
        private void OnInitMap(ICityMap map)
        {
            if (map.BusinessSector != jMapView.BusinessSector)
            {
                return;
            }
            
            jMapView.Initialize(map.jCityGrid.Height, map.jCityGrid.Width); 
            jMapView.RoadGrid.SetUpdateRoadTiles(false);
        }

        private void OnRemoveMapObjectView(IMapObject mapObject)
        {
            if(mapObject.SectorId.ToBusinessSectorType() != jMapView.SectorID) return;
            jMapView.RemoveMapObject(mapObject.MapID);
        }
        
        /// <summary>
        /// Добавляем вьюшку объекта на карту по сигналу от модели
        /// </summary>
        /// <param name="position">Позиция на карте</param>
        /// <param name="mapObject">Модель объекта который нужно добавить</param>
        /// <param name="callback">Коллбек вызываемый после добавления</param>
        private void AddObjectOnMap(Vector2Int position, IMapObject mapObject, Action callback)
        {
            if(mapObject.SectorId.ToBusinessSectorType() != jMapView.SectorID) return;
            jMapView.SetMapObject(position, mapObject, callback);
        }

        /// <summary>
        /// Двигаем вьюшку по карте
        /// </summary>
        /// <param name="newGridPos">Новая позиция на карте</param>
        /// <param name="mapObject">модель объекта который двигаем</param>
        private void MoveObjectOnMap(Vector2Int newGridPos, IMapObject mapObject, Action callback = null)
        {
            if (jMapView.SectorID != mapObject.SectorId.ToBusinessSectorType())
            {
                return;
            }
            
            jMapView.MoveMapObject(newGridPos, mapObject, callback);
        }

        /// <summary>
        /// Коллбек сигнала удаления объекта с карты
        /// </summary>
        /// <param name="mapObject"></param>
        private void OnRemoveMapObject(IMapObject mapObject)
		{
		    if(mapObject.SectorId.ToBusinessSectorType() != jMapView.SectorID) return;
			jMapView.RemoveMapObject (mapObject.MapID);
		}

        private void OnLockedSectorAdded(CityLockedSector sectorModel)
        {
            if (sectorModel.BusinessSectorId.ToBusinessSectorType() != jMapView.SectorID)
            {
                return;
            }
            
            jMapView.AddLockedSector(sectorModel);
        }
        
        private void OnSubSectorsUpdated()
        {
            jMapView.LockedSubMapView.UpdateMap();
        }
        
        private void OnSubSectorUnlocked(CityLockedSector cityLockedSector)
        {
            jMapView.LockedSubMapView.UnlockSector(cityLockedSector);
            jMapView.RedrawSubSectorsBorders(jMapView.LockedSubMapView.Views);
        }
        
        private void OnLockedSubSectorTap(CityLockedSector cityLockedSector)
        {
            if (!jHardTutorial.IsTutorialCompleted)
            {
                return;
            }
            
            if (jConstructionController.IsCurrentlySelectObject || jConstructionController.IsRoadBuildingMode)
            {
                return;
            }

            if (cityLockedSector.BusinessSectorId.ToBusinessSectorType() != jMapView.SectorID)
            {
                return;
            }


            if (cityLockedSector.LockedSector.Unlockable)
            {
                if (_animationInProgress || jUIManager.IsViewVisible(typeof(UnlockAreaPopupView)))
                {
                    return;
                }

                LockedSectorView lockedSectorView = jMapView.LockedSubMapView.GetSectorRenderers(cityLockedSector);

                if (lockedSectorView == null)
                {
                    return;
                }

                void ForceKillAnimation(Type _, object __)
                {
                    _changeStateAnimation?.Kill();
                    _animationInProgress = false;
                    jShowWindowSignal.RemoveListener(ForceKillAnimation);
                }

                jShowWindowSignal.AddListener(ForceKillAnimation);
                _animationInProgress = true;
                _changeStateAnimation = lockedSectorView.transform.GetChild(1).DOPunchScale(Vector3.one * .1f, .5f, 10, 2f)
                    .SetEase(Ease.OutElastic)
                    .OnComplete(() =>
                    {
                        _animationInProgress = false;
                        jShowWindowSignal.RemoveListener(ForceKillAnimation);
                        
                        jShowWindowSignal.Dispatch(typeof(UnlockAreaPopupView), cityLockedSector);
                    });
            }
            else
            {
                jMapView.LockedSubMapView.TapSector(cityLockedSector);
            }
        }
    }
}
