﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.NanoLib.Utilities.Pulls;
using Assets.Scripts.Engine.BuildingSystem.CityMap;
using NanoLib.Services.InputService;
using NanoReality.Engine.UI;
using NanoReality.Engine.UI.Views.WorldSpaceUI;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Impl;
using NanoReality.GameLogic.Settings.BuildSettings.Models.Api;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace Engine.BuildingSystem.CityMap
{
    public class LockedSubMapView : View
    {
        [SerializeField] private LockedSectorView _lockedSectorView;

        [Inject] public ICityMapSettings jCityMapSettings { get; set; }
        [Inject] public IObjectsPull jLockesSubSectorPull { get; set; }
        [Inject] public IWorldSpaceCanvas jWorldSpaceCanvas { get; set; }
        [Inject] public SignalOnLockedSubSectorTap jSignalOnLockedSubSectorTap { get; set; }
    
        public IEnumerable<LockedSectorView> Views => _renderers.Select(entry => entry.Value.Item1);
        public IEnumerable<LockedSectorLabel> Labels => _renderers.Select(entry => entry.Value.Item2);

        private SubSectorUnlockEffect _unlockEffect;
        private readonly Dictionary<int, ValueTuple<LockedSectorView, LockedSectorLabel>> _renderers = new Dictionary<int, (LockedSectorView, LockedSectorLabel)>();

        private SubSectorUnlockEffect UnlockEffect => _unlockEffect != null ? _unlockEffect : _unlockEffect = jWorldSpaceCanvas.GetSubSectorUnlockItem();

        [PostConstruct]
        public void PostConstruct()
        {
            jLockesSubSectorPull.InstanitePull(_lockedSectorView, 0);
        }

        [Deconstruct]
        public void Deconstruct()
        {
            jLockesSubSectorPull.ClearPull();
            jLockesSubSectorPull = null;
        }

        public void Add(CityLockedSector cityLockedSector)
        {
            LockedSectorView view = (LockedSectorView)jLockesSubSectorPull.GetInstance();
            view.transform.SetParent(transform);
            view.Initialize(cityLockedSector, jCityMapSettings);
            view.UpdateDepth();

            LockedSectorLabel label = jWorldSpaceCanvas.GetLockedSubSectorBar();
            label.SetTarget(cityLockedSector, view);
            label.OnClick += OnLabelClick;

            _renderers.Add(cityLockedSector.LockedSector.Id.Value, (view, label));
        }

        public  LockedSectorView  GetSectorRenderers(CityLockedSector cityLockedSector)
        {
            
            int sectorId = cityLockedSector.LockedSector.Id.Value;
            if (_renderers.ContainsKey(sectorId))
                return _renderers[sectorId].Item1;

            return null;
        }
        public void UnlockSector(CityLockedSector cityLockedSector)
        {
            int sectorId = cityLockedSector.LockedSector.Id.Value;
            if (!_renderers.ContainsKey(sectorId))
                return;

            var valueTuple = _renderers[sectorId];
            valueTuple.Item1.FreeObject();
            valueTuple.Item2.FreeObject();
            
            UnlockEffect.transform.position = valueTuple.Item1.transform.position;
            UnlockEffect.SetTarget(valueTuple.Item1);
            UnlockEffect.Play();
            
            _renderers.Remove(sectorId);
        }

        public void Clear()
        {
            foreach (var entry in _renderers)
            {
                entry.Value.Item1.FreeObject();
                entry.Value.Item2.FreeObject();
            }

            _renderers.Clear();
        }

        public void UpdateMap()
        {
            foreach (var entry in _renderers)
            {
                entry.Value.Item1.UpdateView();
                entry.Value.Item2.UpdateView();
            }
        }
    
        public void TapSector(CityLockedSector cityLockedSector)
        {
            int sectorId = cityLockedSector.LockedSector.Id.Value;
            if (!_renderers.ContainsKey(sectorId))
                return;
            
            _renderers[sectorId].Item2.ShowBlockTooltip();
        }

        private void OnLabelClick(CityLockedSector cityLockedSector)
        {
            jSignalOnLockedSubSectorTap.Dispatch(cityLockedSector);
        }
    }
}