using Assets.NanoLib.Utilities;
using UniRx;

namespace NanoReality.Engine.BuildingSystem.CityCamera
{
    public class GameCameraModel : IGameCameraModel
    {
        public ReactiveProperty<bool> GrayscaleActiveProperty { get; } = new ReactiveProperty<bool>(false);
        public ReactiveProperty<bool> IsNeedToLockSwipeProperty { get; } = new ReactiveProperty<bool>();
        public ReactiveProperty<Vector3F> PositionProperty { get; } = new ReactiveProperty<Vector3F>();
        public ReactiveProperty<float> AngleDegreesProperty { get; } = new ReactiveProperty<float>();
        public ReactiveProperty<float> CurrentZoomProperty { get; } = new ReactiveProperty<float>();
        public ReactiveProperty<float> DefaultZoomProperty { get; } = new ReactiveProperty<float>();
        public ReactiveProperty<float> MinZoomProperty { get; } = new ReactiveProperty<float>();
        public ReactiveProperty<float> MaxZoomProperty { get; } = new ReactiveProperty<float>();
        public ReactiveProperty<float> CurrentZoomPercentProperty { get; } = new ReactiveProperty<float>();


        public bool GrayscaleActive => GrayscaleActiveProperty.Value;
        public Vector3F Position => PositionProperty.Value;
        public float AngleDegrees => AngleDegreesProperty.Value;
        public float CurrentZoom => CurrentZoomProperty.Value;
        public float DefaultZoom => DefaultZoomProperty.Value;
        public float MinZoom => MinZoomProperty.Value;
        public float MaxZoom => MaxZoomProperty.Value;
        public float CurrentZoomPercent => CurrentZoomPercentProperty.Value;
    }
}