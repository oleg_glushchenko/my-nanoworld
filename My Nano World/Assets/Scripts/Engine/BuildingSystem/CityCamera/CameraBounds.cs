﻿using UnityEngine;

public class CameraBounds : MonoBehaviour
{
    public Bounds Bounds;

    private Vector3[] vertexes = new Vector3[4];
    private Vector3 project = Vector3.zero;
    private const float vAspect = 1.5f;
    private const float vSize = 0.75f;

    //cached this array, because method using this array is called every frame on user input, and mustn't alloc new memory every time
    private Vector3[] vertices = new Vector3[4];

    public Vector3 ClampByBorders(Vector3 position, float zoom)
    {
        var boundsSize = Bounds.size;
        var aspect = ((float)Screen.width)/Screen.height;
        var hOffset = aspect*zoom*2;
        var vOffset = hOffset/((aspect*vSize)/vAspect);
        var size = Bounds.size;
        size.x -= hOffset + 0.5f;
        size.z -= vOffset + 0.5f;
        Bounds.size = size;

        vertexes = GetColliderVertexPositions();
        var c = position;
        ClampLine(vertexes[0], vertexes[1], ref c); // left
        ClampLine(vertexes[1], vertexes[2], ref c); // top
        ClampLine(vertexes[2], vertexes[3], ref c); // right
        ClampLine(vertexes[3], vertexes[0], ref c); //bottom
        project = c;
        Bounds.size = boundsSize;
        return c;
    }

    private void ClampLine(Vector3 a, Vector3 b,ref Vector3 c)
    {

        var d = (c.x - a.x)*(b.z - a.z) - (c.z - a.z)*(b.x - a.x);
        if (d < 0)
        {
            
            var ac = a- c;
            var ab = a-b;
            var angle = Vector3.Angle(ac, ab);
            var aq = Mathf.Cos(angle*Mathf.Deg2Rad)*ac.magnitude;
            c = a-(ab.normalized* aq);
            project = c;
        }
    }

    private Vector3[] GetColliderVertexPositions()
    {
        var thisMatrix = transform.localToWorldMatrix;
        var storedRotation = transform.rotation;
        transform.rotation = Quaternion.identity;

        var extents = Bounds.extents;
        vertices[0] = thisMatrix.MultiplyPoint3x4(new Vector3(-extents.x, extents.y, -extents.z)); // BL
        vertices[1] = thisMatrix.MultiplyPoint3x4(new Vector3(-extents.x, extents.y, extents.z)); // TL
        vertices[2] = thisMatrix.MultiplyPoint3x4(extents); // tr

        vertices[3] = thisMatrix.MultiplyPoint3x4(new Vector3(extents.x, extents.y, -extents.z)); // BR
      

        transform.rotation = storedRotation;
        return vertices;
    }

    private void OnDrawGizmosSelected()
    {
        vertexes = GetColliderVertexPositions();
        Gizmos.color = Color.red; // left
        Gizmos.DrawLine(vertexes[0], vertexes[1]);
        Gizmos.color = Color.green; // top
        Gizmos.DrawLine(vertexes[1], vertexes[2]);
        Gizmos.color = Color.blue; // right
        Gizmos.DrawLine(vertexes[2], vertexes[3]);
        Gizmos.color = Color.yellow; // bottom
        Gizmos.DrawLine(vertexes[3], vertexes[0]);
        
        Gizmos.color = Color.black;
        Gizmos.DrawSphere(project,0.5f);
    }
}
