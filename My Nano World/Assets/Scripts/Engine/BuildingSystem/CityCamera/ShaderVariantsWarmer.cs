﻿using UnityEngine;

public class ShaderVariantsWarmer : MonoBehaviour
{
    private void Start()
    {
        var collection = Resources.Load<ShaderVariantCollection>("Shaders/PreloadedShaders");
        if (collection == null)
        {
            return;
        }

        collection.WarmUp();
        Resources.UnloadAsset(collection);
    }
}