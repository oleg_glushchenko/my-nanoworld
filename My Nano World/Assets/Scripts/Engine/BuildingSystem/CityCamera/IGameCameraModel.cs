using Assets.NanoLib.Utilities;
using UniRx;
using UnityEngine;

namespace NanoReality.Engine.BuildingSystem.CityCamera
{
    public interface IGameCameraModel
    {
        ReactiveProperty<bool> GrayscaleActiveProperty { get; }
        ReactiveProperty<bool> IsNeedToLockSwipeProperty { get; }
        ReactiveProperty<Vector3F> PositionProperty { get; }
        ReactiveProperty<float> AngleDegreesProperty { get; }
        ReactiveProperty<float> CurrentZoomProperty { get; }
        ReactiveProperty<float> DefaultZoomProperty { get; }
        ReactiveProperty<float> MinZoomProperty { get; }
        ReactiveProperty<float> MaxZoomProperty { get; }
        ReactiveProperty<float> CurrentZoomPercentProperty { get; }
        bool GrayscaleActive { get; }
        Vector3F Position { get; }
        float AngleDegrees { get; }
        float CurrentZoom { get; }
        float DefaultZoom { get; }
        float MinZoom { get; }
        float MaxZoom { get; }
        float CurrentZoomPercent { get; }
    }
}