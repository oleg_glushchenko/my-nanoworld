﻿using System;
using System.Collections.Generic;
using Assets.NanoLib.Utilities;
using NanoReality.GameLogic.Utilites;
using UnityEngine;

namespace NanoLib.GeneratedTileMap
{
    public class TileMapMulti : MonoBehaviour
    {
        public Vector2 TilesInChunk { get; set; }
        public Vector2 TileSize { get; set; }

        /// <summary>
        /// Ширина всего поля
        /// </summary>
        public int TilesPerGridX = 100;

        /// <summary>
        /// Высота всего поля
        /// </summary>
        public int TilesPerGridY = 100;

        /// <summary>
        /// Количество чанков по горизонтали
        /// </summary>
        private int _chunksPerGridX;

        /// <summary>
        /// Количество чанков по вертикали
        /// </summary>
        private int _chunksPerGridY;

        public Color32 GridColor = Color.white;

        /// <summary>
        /// Текстура каждого тайла
        /// </summary>
        public Texture2D Texture;

        /// <summary>
        /// Шейдер каждого тайла
        /// </summary>
        public Shader Shader;

        /// <summary>
        /// Контейнер для всех чанков
        /// </summary>
        private List<Chunk> _chunks;

        /// <summary>
        /// Необходимо что бы обезопаситься от нескольких выполнений Start()
        /// </summary>
        private bool _startedOnce;

        public Vector2 TilesInSpriteSheet { get; set; }
        
        public Material GridMaterial { get; set; }

        public void ClearChunks()
        {
            foreach (var chunk in _chunks)
                Destroy(chunk.gameObject);
            
            _chunks.Clear();
            _chunks = null;

            _startedOnce = false;
        }

        /// <summary>
        /// Входная точка
        /// </summary>
        public void BuildTileMap()
        {
            // предотвращаем повторную инициализацию
            if (_startedOnce) return;
            _startedOnce = true;

            int tilesPerChunkX = Convert.ToInt32(TilesInChunk.x);
            int tilesPerChunkY = Convert.ToInt32(TilesInChunk.y);

            // определяем количество чанков, и создаем их список
            _chunksPerGridX =  Mathf.CeilToInt(TilesPerGridX / (float)tilesPerChunkX);
            _chunksPerGridY = Mathf.CeilToInt(TilesPerGridY / (float)tilesPerChunkY);
            _chunks = new List<Chunk>(new Chunk[_chunksPerGridX*_chunksPerGridY]);

            // пробегаемся по каждому чанку
            for (int i = 0; i < _chunksPerGridX; i++)
            {
                for (int j = 0; j < _chunksPerGridY; j++)
                {
                    // создаем и настраиваем игровой объект чанка
                    var chunkGameObject = new GameObject("part_" + i + "_" + j);
                    chunkGameObject.AddComponent<MeshFilter>();
                    var chunkRenderer = chunkGameObject.AddComponent<MeshRenderer>();
                    chunkRenderer.material = GridMaterial;

                    // добавляем чанку собственно компонент чанка и копируем туда данные
                    var chunkComponent = chunkGameObject.AddComponent<Chunk>();
                    chunkComponent.TilesInSpriteSheet = TilesInSpriteSheet;
                    chunkComponent.DefaultTileX = -1;
                    chunkComponent.DefaultTileY = -1;

                    // устанавливаем правильную позицию и родительский геймобъект
                    chunkGameObject.transform.SetParent(transform);
                    chunkGameObject.transform.localPosition = new Vector3(i* tilesPerChunkX * TileSize.x, 0,
                        j* tilesPerChunkY * TileSize.y);

                    // отправляем новосозданный чанк в массив чанков
                    SaveChunk(chunkComponent, i, j);

                    chunkComponent.CreatePlane(TileSize.x, TileSize.y, tilesPerChunkX, tilesPerChunkY, GridMaterial);
                }
            }
        }

        /// <summary>
        /// Назначает созданный чанк соответствующему элементу в списке
        /// </summary>
        /// <param name="chunk">созданный чанк</param>
        /// <param name="x">позиция чанка по X</param>
        /// <param name="y">позиция чанка по Y</param>
        private void SaveChunk(Chunk chunk, int x, int y)
        {
            var index = x + y*_chunksPerGridX;
            _chunks[index] = chunk;
        }

        /// <summary>
        /// Возвращает чанк по переданым индексам
        /// </summary>
        /// <param name="x">позиция чанка по X</param>
        /// <param name="y">позиция чанка по Y</param>
        /// <returns>Объект чанка</returns>
        private Chunk GetChunk(Vector2 position)
        {
            var q = position.GetIntX() + position.GetIntY() * _chunksPerGridX;
            return q<_chunks.Count ? _chunks[q] : null;
        }

        /// <summary>
        /// Обновляет одну клетку сетки
        /// </summary>
        /// <param name="position"></param>
        /// <param name="tile">координата тайла текстуры</param>
        public void UpdateGrid(Vector2 position, Vector2 tile)
        {
            int tilesPerChunkX = Convert.ToInt32(TilesInChunk.x);
            int tilesPerChunkY = Convert.ToInt32(TilesInChunk.y);
            
            int partX = (position.GetIntX() / tilesPerChunkX);
            int partY = (position.GetIntY() / tilesPerChunkY);

            int x = (position.GetIntX() % tilesPerChunkX);
            int y = (position.GetIntY() % tilesPerChunkY);

            Chunk part = GetChunk(new Vector2(partX, partY)); // 2

            part.UpdateGrid(new Vector2(x, y), tile, tilesPerChunkX);
        }

        /// <summary>
        /// Обновляет все клетки со стартовой до конечной позиции (прямоугольник)
        /// </summary>
        /// <param name="startPosition">начальная клетка с которой будут обновлятся клетки</param>
        /// <param name="endPosition">конечная клетка</param>
        /// <param name="tileX">x координата тайла текстуры</param>
        /// <param name="tileY">y координата тайла текстуры</param>
        public void UpdateGrid(Vector2Int startPosition, Vector2Int endPosition, int tileX, int tileY)
        {
            int tilesPerChunkX = Convert.ToInt32(TilesInChunk.x);
            int tilesPerChunkY = Convert.ToInt32(TilesInChunk.y);

            Dictionary<Chunk, List<Vector2F>> chunks = new Dictionary<Chunk, List<Vector2F>>();
            for (int i = startPosition.x; i < endPosition.x; i++)
            {
                for (int j = startPosition.y; j < endPosition.y; j++)
                {
                    var partX = (i / tilesPerChunkX);
                    var partY = (j / tilesPerChunkY);
                    
                    var x = (i % tilesPerChunkX);
                    var y = (j % tilesPerChunkY);
                    
                    var part = GetChunk(new Vector2(partX, partY));
                    
                    if (part == null)
                    {
                        continue;
                    }

                    var pointValue = new Vector2F(x, y);

                    if (chunks.ContainsKey(part))
                    {
                        chunks[part].Add(pointValue);
                    }
                    else
                    {
                        chunks.Add(part, new List<Vector2F>() { pointValue });
                    }
                }
            }
            
            foreach (var chunkPoint in chunks)
            {
                chunkPoint.Key.UpdateGrid(chunkPoint.Value, tileX, tileY, tilesPerChunkX);
            }
        }

        /// <summary>
        /// Обновляет список клеток на карте
        /// </summary>
        /// <param name="points">клетки которые нужно обновить</param>
        /// <param name="colors">цвет этих клеток</param>
        public void UpdateGrid(List<Vector2F> points, List<Vector2F> colors)
        {
            int tilesPerChunkX = Convert.ToInt32(TilesInChunk.x);
            int tilesPerChunkY = Convert.ToInt32(TilesInChunk.y);

            Dictionary<Chunk, List<Vector2F>> chunkPoints = new Dictionary<Chunk, List<Vector2F>>();
            Dictionary<Chunk, List<Vector2F>> chunkColors = new Dictionary<Chunk, List<Vector2F>>();
            for (int i = 0; i < points.Count; i++)
            {
                var partX = (points[i].intX / tilesPerChunkX);
                var partY = (points[i].intY / tilesPerChunkY);

                var x = (points[i].intX % tilesPerChunkX);
                var y = (points[i].intY % tilesPerChunkY);

                var part = GetChunk(new Vector2(partX, partY));
                if (part == null) continue;
                if (chunkPoints.ContainsKey(part))
                {
                    chunkPoints[part].Add(new Vector2F(x, y));
                    chunkColors[part].Add(colors[i]);
                }
                else
                {
                    chunkPoints.Add(part, new List<Vector2F>()
                    {
                        new Vector2F(x, y)
                    });
                    chunkColors.Add(part, new List<Vector2F>()
                    {
                        colors[i]
                    });
                }
            }

            foreach (var chunkPoint in chunkPoints)
            {
                chunkPoint.Key.UpdateGrid(chunkPoint.Value, chunkColors[chunkPoint.Key], tilesPerChunkX);
            }
        }

        /// <summary>
        /// Сбрасывает текущую сетку в начальное состоянии (все клетки отмечены как свободные)
        /// </summary>
        public void ClearGrid()
        {
            for (int i = 0; i < _chunks.Count; i++)
            {
                _chunks[i].ClearChank();
            }
        }
    }
}