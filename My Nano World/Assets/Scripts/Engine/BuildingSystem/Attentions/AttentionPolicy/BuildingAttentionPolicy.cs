using System.Collections.Generic;
using NanoReality.Game.Services.BuildingsUnlockService;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.Utilities;

namespace NanoReality.Game.Attentions
{
    public abstract class BuildingAttentionPolicy
    {
        public IMapObject Model { get; private set; }

        public abstract AttentionData CalculateAttention();

        public virtual void Initialize(IMapObject buildingModel)
        {
            Model = buildingModel;
        }
    }

    public abstract class BuildingAttentionPolicy<TModelType> : BuildingAttentionPolicy where TModelType : IMapBuilding
    {
        public TModelType BuildingModel;

        [Inject] public IPlayerProfile jPlayerProfile { get; set; }
        [Inject] public IMapObjectsData jMapObjectsData { get; set; }
        [Inject] public IBuildingsUnlockService jBuildingUnlockService { get; set; }

        private IPlayerResources PlayerResources => jPlayerProfile.PlayerResources;

        public override void Initialize(IMapObject buildingModel)
        {
            base.Initialize(buildingModel);
            BuildingModel = (TModelType) buildingModel;
        }

        protected void AddBasicAttention(AttentionData data)
        {
            IMapBuilding model = BuildingModel;

            AddLockedBuildingAttention(model, data);

            if (!model.IsUpgradingNow)
            {
                if (model.IsNeedRoad && !model.IsConnectedToGeneralRoad)
                {
                    FlagsHelper.Set(ref data.AttentionType, AttentionsTypes.Road);
                }

                var electricity = model as IBuildingWithElectricity;
                if (electricity != null && !electricity.IsEnabledEnergy)
                {
                    FlagsHelper.Set(ref data.AttentionType, AttentionsTypes.Electricity);
                }
            }
        }
        
        protected virtual void AddUpgradeAttention(AttentionData data)
        {
            IMapBuilding model = BuildingModel;
            if (model.CanBeUpgraded)
            {
                var nextLevel = model.Level + 1;
                var price = jBuildingUnlockService.GetUpgradeBuildingPrice(model.ObjectTypeID, nextLevel, model.InstanceIndex);
                
                if (PlayerResources.IsEnoughtForBuy(price) && PlayerResources.HasEnoughProductsForUpgrade(model.ProductsForUpgrade))
                {
                    FlagsHelper.Set(ref data.AttentionType, AttentionsTypes.ReadyForUpgrade);
                }
                else
                {
                    FlagsHelper.Set(ref data.AttentionType, AttentionsTypes.NotEnoughProducts);
                }
            }
        }

        protected virtual void AddLockedBuildingAttention(IMapBuilding model, AttentionData data)
        {
            if (!model.IsLocked)
            {
                return;
            }

            if (jPlayerProfile.CurrentLevelData.CurrentLevel < model.BuildingUnlockLevel)
            {
                return;
            }
            
            var unlockProducts = model.ProductsToUnlock;
            if (unlockProducts.Products != null)
            {
                if (HasProductsForLockedBuilding(unlockProducts.Products))
                {
                    FlagsHelper.Set(ref data.AttentionType, AttentionsTypes.ReadyForUpgrade);
                }
                else
                {
                    FlagsHelper.Set(ref data.AttentionType, AttentionsTypes.NotEnoughProducts);
                }
            }
            else
            {
                FlagsHelper.Set(ref data.AttentionType, AttentionsTypes.BuildingLocked);
            }
        }

        private bool HasProductsForLockedBuilding(IEnumerable<IProductsToUnlockItem> products)
        {
            foreach (IProductsToUnlockItem unlockItem in products)
            {
                if (!jPlayerProfile.PlayerResources.HasEnoughProduct(unlockItem.ProductID, unlockItem.Count))
                {
                    return false;
                }
            }

            return true;
        }
    }
}