using Assets.NanoLib.UtilitesEngine;
using Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Models.api.CityOrderDesks;
using NanoReality.GameLogic.OrderDesk;
using NanoReality.GameLogic.ServerCommunication.Models;
using NanoReality.GameLogic.Utilities;

namespace NanoReality.Game.Attentions
{
    public sealed class MetroAttentionPolicy : BuildingAttentionPolicy<CityOrderDeskMapBuilding>
    {
        [Inject] public IMetroOrderDeskService jMetroOrderDeskService { get; set; }
        [Inject] public ITimerManager jTimeManager { get; set; }
        
        public override AttentionData CalculateAttention()
        {
            AttentionData data = new AttentionData(AttentionsTypes.None);

            AddCityOrderDeskAttention(data);
            AddBasicAttention(data);
            AddUpgradeAttention(data);

            return data;
        }

        private void AddCityOrderDeskAttention(AttentionData data)
        {
            if (jMetroOrderDeskService?.MetroOrderDesk != null)
                
                foreach (var train in jMetroOrderDeskService.MetroOrderDesk?.OrderTrains)
                {
                    if (train?.OrderSlots == null)
                        return;

                    if (train.ShuffleEndTime > jTimeManager.CurrentUTC)
                        return;
                    
                    if (IsEnoughProductsCount(train))
                    {
                        FlagsHelper.Set(ref data.AttentionType, AttentionsTypes.OrderDesk);
                        return;
                    }
                }
        }
        
        private bool IsEnoughProductsCount(NetworkMetroOrderTrain train)
        {
            foreach (var slot in train.OrderSlots)
            {
                if(slot == null)
                    continue;

                if (slot.IsFilled)
                {
                    continue;
                }
                
                int hasCount = jPlayerProfile.PlayerResources.GetProductCount(slot.ProductId);
                if (slot.Count > hasCount)
                    return false;
            }

            return true;
        }
    }
}