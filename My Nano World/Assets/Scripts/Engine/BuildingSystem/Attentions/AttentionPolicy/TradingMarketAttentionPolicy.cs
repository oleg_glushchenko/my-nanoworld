using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.impl;

namespace NanoReality.Game.Attentions
{
    public sealed class TradingMarketAttentionPolicy : BuildingAttentionPolicy<TradingMarketBuilding>
    {
        public override AttentionData CalculateAttention()
        {
            AttentionData data = new AttentionData(AttentionsTypes.None);
            
            AddBasicAttention(data);
            AddUpgradeAttention(data);

            return data;
        }
    }
}