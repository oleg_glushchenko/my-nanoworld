using NanoReality.Game.CitizenGifts;
using NanoReality.Game.FoodSupply;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.SupplyFood;
using NanoReality.GameLogic.Utilities;

namespace NanoReality.Game.Attentions
{
    public sealed class FoodSupplyAttentionPolicy : BuildingAttentionPolicy<FoodSupplyBuilding>
    {
        [Inject] public ICityGiftsService jCityGiftsService { get; set; }  
        [Inject] public IFoodSupplyService jFoodSupplyService { get; set; }
        
        public override AttentionData CalculateAttention()
        {
            var data = new AttentionData(AttentionsTypes.None);

            if (jCityGiftsService.HasAvailableGifts(BuildingModel.MapID))
            {
                AddCoinsAttention(data);
            }
            AddCanCookAttention(data);

            return data;
        }

        private void AddCoinsAttention(AttentionData data)
        {
            data.AttentionType |= AttentionsTypes.Coin;
        }

        private void AddCanCookAttention(AttentionData data)
        {
            if (BuildingModel.IsLocked)
            {
                return;
            }
            
            var recipesData = jFoodSupplyService.GetRecipesData();
            foreach (var recipe in recipesData)
            {
                if (CanBeCooked(recipe))
                {
                    data.AttentionType |= AttentionsTypes.OrderDesk;
                }
            }
        }

        private bool CanBeCooked(FoodSupplyProductRecipe recipe)
        {
            foreach (var product in recipe.Products)
            {
                int hasCount = jPlayerProfile.PlayerResources.GetProductCount(product.ProductId);
                if (product.Count > hasCount)
                {
                    return false;
                }
            }

            return true;
        }
    }
}