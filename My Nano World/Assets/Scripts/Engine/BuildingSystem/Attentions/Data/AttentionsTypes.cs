﻿using System;

namespace NanoReality.Game.Attentions
{
    /// <summary>
    /// Building attention types enum.
    /// </summary>
    [Flags]
    public enum AttentionsTypes // 4 bytes or 32 bits
    {
        None = 0,
        Road = 1 << 0,
        Electricity = 1 << 1,
        BuildingLocked = 1 << 2,
        OrderDesk = 1 << 3,
        Crystals = 1 << 4,
        Achievement = 1 << 5,
        Coin = 1 << 6,
        CinemaAd = 1 << 7,
        CityOrderDesk = 1 << 8,
        CustomProductToLoad = 1 << 9,
        TradeProductBought = 1 << 10,
        /// <summary>
        /// Using when building can upgrade and player enough resources for upgrade.
        /// </summary>
        ReadyForUpgrade = 1 << 11,
        QuestsAchieved = 1 << 12,
        QuestsAvailable = 1 << 13,
        CasinoSpinAvailable = 1 << 14,
        Reward = 1 << 15,
        HungryPopulation = 1 << 16,
        NeedWaterSupply = 1 << 17,
        NeedWasteManagement = 1 << 18,
        NotEnoughProducts = 1 << 19
    }
}