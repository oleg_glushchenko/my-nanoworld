﻿using System.Collections.Generic;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using NanoReality.Engine.BuildingSystem.MapObjects.Components;

namespace NanoReality.Engine.BuildingSystem.MapObjects
{
    public abstract class ProductionMapObjectView : NeedPowerMapObjectView
    {
        public ReadyProductsComponent ReadyProductsComponent => GetCachedComponent<ReadyProductsComponent>();
        
        protected override List<string> GetComponentsNames()
        {
            var result = base.GetComponentsNames();
            result.Add("ProductionBuildingAnimationComponent");
            result.Add(ReadyProductsComponent.Key);
            return result;
        }
    }
}
