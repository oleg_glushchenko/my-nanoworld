﻿using System.Collections.Generic;
using System.Linq;
using Assets.NanoLib.Utilities;
using Assets.Scripts.Extensions;
using Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Models.api.OrderDesks;
using NanoReality.Engine.BuildingSystem.GlobalCityMap;
using NanoReality.Game.Services.OrderDesk.SeaportOrderDesk;
using NanoReality.Game.UI;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;

public class OrderDeskMapObjectView : AGlobalCityBuildingView
{
	private IOrderDeskMapBuilding OrderDeskModel => (IOrderDeskMapBuilding)MapBuildingModel;
    
    public override bool IsMovable => false;
    
    [Inject] public ISeaportOrderDeskService jSeaportOrderDeskService { get; set; }

    protected override List<string> GetComponentsNames ()
	{
		var r = base.GetComponentsNames ();
		r.Remove("DragComponent");
		return r;
	}

    protected override void AddListeners()
    {
	    base.AddListeners();
	    jSeaportOrderDeskService.OnSeaportSlotShuffleUpdated += OnSeaportSlotShuffleUpdated;
    }

    protected override void RemoveListeners()
    {
	    base.RemoveListeners();
	    jSeaportOrderDeskService.OnSeaportSlotShuffleUpdated -= OnSeaportSlotShuffleUpdated;
    }

	protected override void ShowAttentionIfNeeded()
    {
        if (!jHardTutorial.IsTutorialCompleted && AttentionComponent != null)
        {
            AttentionComponent.FreeView(true);
            return;
        }
        
        base.ShowAttentionIfNeeded();
    }

	protected override void OnBuildingTap()
	{
		if(jSeaportOrderDeskService.SeaportCollection.IsNullOrEmpty())
		{
			return;
		}
		
		jShowWindowSignal.Dispatch(typeof(SeaportPanelView) , OrderDeskModel);
	}

	private void OnSectorsOrderDeskAttentionTap(Id<IBusinessSector> sector)
	{
		var orderDeskBuilding = (IOrderDeskMapBuilding)MapBuildingModel;
		if (sector.ToBusinessSectorType() == orderDeskBuilding.OrderDeskSectorType)
		{
			jGameCamera.FocusOnMapObject(orderDeskBuilding);
		}
	}
    
    private void OnSeaportSlotShuffleUpdated(ISeaportOrderSlot slot)
    { 
	    ShowAttentionIfNeeded();
    }
}