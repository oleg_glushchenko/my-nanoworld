using Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Models.api.CityOrderDesks;
using NanoReality.Engine.BuildingSystem.GlobalCityMap;
using NanoReality.Game.UI;
using NanoReality.GameLogic.OrderDesk;
using NanoReality.GameLogic.Orders;

public class CityOrderDeskMapObjectView : AGlobalCityBuildingView
{
    [Inject] public SignalOnOrderTruckTripFinished jSignalOnOrderTruckTripFinished { get; set; }
    [Inject] public SignalOnCityOrdersLoadedSignal jSignalOnCityOrdersLoadedSignal { get; set; }
    [Inject] public SignalOnCityOrderAwardTaken jSignalOnCityOrderAwardTaken { get; set; }
    
    [Inject] public MetroOrderDeskUpdatedSignal jMetroOrderDeskUpdatedSignal { get; set; }
    [Inject] public MetroOrderSlotWaitingFinishedSignal jMetroOrderSlotWaitingFinishedSignal { get; set; }
    [Inject] public MetroOrderSlotLoadedSignal jMetroOrderLoadedSignal { get; set; }
    [Inject] public MetroOrderRewardCollectedSignal jMetroOrderRewardCollectedSignal { get; set; }
    [Inject] public MetroOrderShippingSignal jMetroOrderShippingSignal { get; set; }
    [Inject] public MetroOrderShuffledSignal jMetroOrderShuffleSignal { get; set; }
    [Inject] public IMetroOrderDeskService jMetroOrderDeskService { get; set; }
    
    public override bool IsMovable => false;

    #region Methods

    protected override void AddListeners()
    {
        base.AddListeners();

        jSignalOnCityOrdersLoadedSignal.AddListener(OnOrdersLoadedFromServerSignal);
        jSignalOnOrderTruckTripFinished.AddListener(OnTruckTripFinished);
        jSignalOnCityOrderAwardTaken.AddListener(OnCityOrderAwardTaken);
        
        jMetroOrderDeskUpdatedSignal.AddListener(OnMetroOrderDeskUpdated);
        jMetroOrderSlotWaitingFinishedSignal.AddListener(OnMetroOrderSlotWaitingFinished);
        jMetroOrderLoadedSignal.AddListener(OnMetroOrderLoaded);
        jMetroOrderRewardCollectedSignal.AddListener(OnMetroOrderRewardCollected);
        jMetroOrderShippingSignal.AddListener(OnMetroOrderShipping);
        jMetroOrderShuffleSignal.AddListener(OnMetroOrderShuffle);
    }

    protected override void RemoveListeners()
    {
        base.RemoveListeners();

        jSignalOnCityOrdersLoadedSignal.RemoveListener(OnOrdersLoadedFromServerSignal);
        jSignalOnOrderTruckTripFinished.RemoveListener(OnTruckTripFinished);
        jSignalOnCityOrderAwardTaken.RemoveListener(OnCityOrderAwardTaken);
        
        jMetroOrderDeskUpdatedSignal.RemoveListener(OnMetroOrderDeskUpdated);
        jMetroOrderSlotWaitingFinishedSignal.RemoveListener(OnMetroOrderSlotWaitingFinished);
        jMetroOrderLoadedSignal.RemoveListener(OnMetroOrderLoaded);
        jMetroOrderRewardCollectedSignal.RemoveListener(OnMetroOrderRewardCollected);
        jMetroOrderShippingSignal.RemoveListener(OnMetroOrderShipping);
        jMetroOrderShuffleSignal.RemoveListener(OnMetroOrderShuffle);
    }

    protected override void ShowAttentionIfNeeded()
    {
        if (!jHardTutorial.IsTutorialCompleted && AttentionComponent != null)
        {
            AttentionComponent.FreeView(true);
            return;
        }
        
        base.ShowAttentionIfNeeded();
    }

    protected override void OnBuildingTap()
    {
        if (jMetroOrderDeskService.MetroOrderDesk == null)
        {
            return;
        }
        
        jShowWindowSignal.Dispatch(typeof(MetroOrderDeskPanelView), MapBuildingModel);
    }

    private void OnTruckTripFinished(int obj)
    {
        ShowAttentionIfNeeded();
    }

    /// <summary>
    /// Проверить заказы по завершению загрузки данных о заказах
    /// </summary>
    private void OnOrdersLoadedFromServerSignal()
    {
        ShowAttentionIfNeeded();
    }
    
    private void OnCityOrderAwardTaken()
    {
        ShowAttentionIfNeeded();
    }

    private void OnCityOrderDeskAttentionTap()
    {
        jGameCamera.FocusOnMapObject(MapBuildingModel);
        jShowWindowSignal.Dispatch(typeof(MetroOrderDeskPanelView), (ICityOrderDeskMapBuilding) MapObjectModel);
    }
    
    private void OnMetroOrderDeskUpdated(NetworkMetroOrderDesk obj)
    {
        ShowAttentionIfNeeded();
    }

    private void OnMetroOrderSlotWaitingFinished(int obj)
    {
        ShowAttentionIfNeeded();
    }

    private void OnMetroOrderLoaded(NetworkOrderSlot obj)
    {
        ShowAttentionIfNeeded();
    }

    private void OnMetroOrderRewardCollected(NetworkMetroOrderTrain obj)
    {
        ShowAttentionIfNeeded();
    }

    private void OnMetroOrderShipping(int arg1, NetworkMetroOrderTrain arg2)
    {
        ShowAttentionIfNeeded();
    }

    private void OnMetroOrderShuffle(int obj)
    {
        ShowAttentionIfNeeded();
    }

    #endregion
}