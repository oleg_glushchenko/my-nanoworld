﻿using System.Collections.Generic;
using Assets.Scripts.Engine.BuildingSystem.MapObjects.Components;
using NanoReality.Engine.BuildingSystem.GlobalCityMap;
using NanoReality.Engine.BuildingSystem.MapObjects.Components;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.TheEvent.Signals;
using NanoReality.GameLogic.Upgrade;
using UnityEngine;

namespace Assets.Scripts.Engine.BuildingSystem.MapObjects
{
    public class EventBuildingMapObjectView : AGlobalCityBuildingView
    {
        [Inject] public SignalOpenUpgradeWindowEventBuilding jSignalOpenUpgradeWindowEventBuilding { get; set; }
        [Inject] public SignalCheckUnclaimedStageRewards jSignalCheckUnclaimedStageRewards { get; set; }
        [Inject] public SignalClickOnTheEventBuildingWhenCannotUpgrade jSignalClickOnTheEventBuildingWhenCannotUpgrade { get; set; }
        [Inject] public SignalDisableAttentionViewOnEventBuilding SignalDisableAttentionViewOnEventBuilding { get; set; }

        [SerializeField] private List<ParticleSystem> _particlesToPlayOnTap = new List<ParticleSystem>();
        
        public override bool KeepConstructAnimation => MapObjectModel != null && MapObjectModel.Level < 1;
        private IEventBuilding TargetModel => MapObjectModel as IEventBuilding;
        private int _maxLevelBuilding = 7;
        private const string EventBuildingTag = "EventBuilding";

        public override void InitializeView(IMapObject model)
        {
            base.InitializeView(model);
            jSignalOpenUpgradeWindowEventBuilding.AddListener(OpenUpgradePanel);
            SignalDisableAttentionViewOnEventBuilding.AddListener(ChangeAttentionViewState);
            GameObject.FindGameObjectWithTag(EventBuildingTag)?.SetActive(false);

            if (!TargetModel.ReadyToUpgrade)
            {
                ChangeAttentionViewState(true);
            }
        }

        public override void DisposeView()
        {
            Components.Remove(typeof(BuildingAttentionComponent));
            jSignalOpenUpgradeWindowEventBuilding.RemoveAllListeners();
            SignalDisableAttentionViewOnEventBuilding.RemoveAllListeners();
            base.DisposeView();
        }

        protected override void OnBuildingTap()
        {
            base.OnBuildingTap();

            if (!TargetModel.ReadyToUpgrade && TargetModel.CanBeUpgraded)
            {
                jSignalClickOnTheEventBuildingWhenCannotUpgrade.Dispatch();
            }
            else
            {
                jSignalCheckUnclaimedStageRewards.Dispatch();
            }

            if (TargetModel.Level == _maxLevelBuilding)
            {
                foreach (var particleSystem in _particlesToPlayOnTap)
                {
                    if (particleSystem == null) continue;

                    particleSystem.gameObject.SetActive(true);
                    particleSystem.Play();
                }
            }
        }

        private void OpenUpgradePanel()
        {
            if (TargetModel.CanBeUpgraded)
            {
                jShowWindowSignal.Dispatch(typeof(UpgradeWithProductsPanelView), TargetModel);
            }
        }

        private void ChangeAttentionViewState(bool disable)
        {
            if (disable)
            {
                GetViewComponent<BuildingAttentionComponent>().DisposeComponent();
            }
            else
            {
                IMapObjectViewComponent component = Components[typeof(BuildingAttentionComponent)];
                component.InitializeComponent(this);
            }
        }
    }
}