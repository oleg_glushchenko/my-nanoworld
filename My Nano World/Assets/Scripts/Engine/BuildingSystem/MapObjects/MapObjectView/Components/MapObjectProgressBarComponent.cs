﻿using System;
using Assets.NanoLib.UI.Core.Signals;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using Assets.Scripts.Engine.UI.Views.Popups;
using NanoReality.Engine.UI.Views.WorldSpaceUI;
using NanoReality.Engine.UI;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoLib.Services.InputService;
using NanoReality.Engine.BuildingSystem.CityCamera;
using NanoReality.Game.Attentions;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;


namespace NanoReality.Engine.BuildingSystem.MapObjects.Components
{
	/// <summary>
	/// Компонент прогресс бара постройки
	/// </summary>
	public class MapObjectProgressBarComponent : MapObjectViewComponent
	{
		/// <summary>
		/// Прогрессбар постройки здания
		/// </summary>
		public ConstructionProgressBar ConstructionProgressBar { get { return _constructionProgressBar; } }
		protected ConstructionProgressBar _constructionProgressBar;

		#region Injects
		[Inject] public IWorldSpaceCanvas jWorldSpaceCanvas { get; set; }
		[Inject] public SignalOnMapObjectViewTap jSignalOnMapObjectViewTap { get; set; }
		[Inject] public SignalOnGroundTap jSignalOnGroundTap { get; set; }
		[Inject] public WindowOpenedSignal jWindowOpenedSignal { get; set; }
		#endregion

        public override void InitializeComponent (MapObjectView target)
		{
			base.InitializeComponent(target);

			if (TargetModel.MapObjectType == MapObjectintTypes.Road) return;

			if (TargetModel != null)
			{
				TargetModel.EventOnMapObjectBuildStarted.AddListener(OnMapObjectBuildingStarted);
				TargetModel.EventOnMapObjectBuildFinished.AddListener(OnMapObjectBuildingFinished);
				TargetModel.EventOnMapObjectBuildMoved.AddListener(OnMapObjectBuildingMoved);
			}

			jSignalOnMapObjectViewTap.AddListener(OnTapMapObjectView);
			jSignalOnGroundTap.AddListener(OnTapGroud);
			jWindowOpenedSignal.AddListener(OnViewOpen);

			if (Target.MapObjectModel == null) return;

			// если объект не построен и время у него больше 0, или же если объект сейчас апгрейдится,
			// добавляем прогрессбар
			if ((!Target.MapObjectModel.IsConstructed && Target.MapObjectModel.CurrentConstructTime > 0) ||
			    (Target.MapObjectModel.IsUpgradingNow))
			{
				AddProgressBar();
			}
		}

        public override void DisposeComponent ()
		{
			if (TargetModel != null)
			{
				TargetModel.EventOnMapObjectBuildStarted.RemoveListener(OnMapObjectBuildingStarted);
				TargetModel.EventOnMapObjectBuildFinished.RemoveListener(OnMapObjectBuildingFinished);
				TargetModel.EventOnMapObjectBuildMoved.RemoveListener(OnMapObjectBuildingMoved);
			}

			jSignalOnMapObjectViewTap.RemoveListener(OnTapMapObjectView);
			jSignalOnGroundTap.RemoveListener(OnTapGroud);
			jWindowOpenedSignal.RemoveListener(OnViewOpen);

			if (_constructionProgressBar != null)
			{
				_constructionProgressBar.FreeObject();
				_constructionProgressBar = null;
			}

			base.DisposeComponent();
		}

		/// <summary>
		/// Добавляет прогрессбар на здание
		/// </summary>
		public virtual void AddProgressBar()
		{
			if (_constructionProgressBar != null)
				RemoveProgressBar();

			_constructionProgressBar = jWorldSpaceCanvas.GetConstractionProgressBar();
			_constructionProgressBar.StartTiming(Target);
			_constructionProgressBar.Show();
			_constructionProgressBar.TurnSpeedUpButton(false);
            jWorldSpaceCanvas.OrderProgressBars();
		}

		/// <summary>
		/// Удаляет прогрессбар со здания
		/// </summary>
		public virtual void RemoveProgressBar()
		{
			if (_constructionProgressBar != null)
			{
				jWorldSpaceCanvas.RemoveConstractionProgressBar(_constructionProgressBar);
				_constructionProgressBar = null;
			}
		}

		/// <summary>
		/// Включает/выключает кнопку скипа на прогрессбаре
		/// </summary>
		protected virtual void SetActiveSkipButton(bool isActive)
		{
			if (_constructionProgressBar != null)
			{
				if (isActive)
				{
					_constructionProgressBar.TrySetActiveSkipButton();
				}
				else
				{
					_constructionProgressBar.TurnSpeedUpButton(false);
				}
			}	
		}

		/// <summary>
		/// Add a progress bar when the building of map object has started
		/// </summary>
		protected virtual void OnMapObjectBuildingStarted(IMapObject mapObject)
		{
			if (mapObject.CurrentConstructTime > 0)
			{
				AddProgressBar();
			}
		}

        protected virtual void OnMapObjectBuildingMoved(IMapObject mapObject)
		{
            jWorldSpaceCanvas.OrderProgressBars();
		}

        /// <summary>
        /// remove the progress bar when the building is ready
        /// </summary>
        /// <param name="mapObject"></param>
        protected virtual void OnMapObjectBuildingFinished(IMapObject mapObject)
		{	
			RemoveProgressBar();
		}

		/// <summary>
		/// Коллбек тапа по зданию
		/// </summary>
		/// <param name="tapped">Tapped.</param>
		protected void OnTapMapObjectView(MapObjectView tapped)
		{
			var isTarget = tapped == Target;

			SetActiveSkipButton(isTarget);
		}

		/// <summary>
		/// Коллбек тапа по земле
		/// </summary>
		protected  void OnTapGroud()
		{
			SetActiveSkipButton (false);
		}

		protected void OnViewOpen(Type _)
		{
			SetActiveSkipButton(false);
		}
	}
}

