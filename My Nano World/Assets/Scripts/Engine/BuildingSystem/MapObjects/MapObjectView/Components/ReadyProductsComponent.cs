﻿using System;
using System.Collections.Generic;
using Assets.Scripts.GameLogic.BuildingSystem.Products.Models.api;
using Assets.Scripts.GameLogic.Utilites;
using Engine.UI;
using NanoLib.Core;
using NanoLib.Core.Pool;
using NanoLib.Core.Services.Sound;
using NanoReality.Core.Resources;
using NanoReality.Engine.BuildingSystem.CityCamera;
using NanoReality.Engine.UI;
using NanoReality.Engine.UI.Extensions.EditModePanels;
using NanoReality.Engine.UI.Views.Panels;
using NanoReality.Engine.UI.Views.WorldSpaceUI;
using NanoReality.GameLogic.BuildingSystem.CityMap.Controllers;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Controllers.Factories;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Factories;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Production;
using NanoReality.GameLogic.GameManager.Controllers;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.UI.Components;
using UniRx;
using UnityEngine;
using Random = UnityEngine.Random;

namespace NanoReality.Engine.BuildingSystem.MapObjects.Components
{
    public class ReadyProductsComponent : MapObjectViewComponent
    {
        public static string Key => "ReadyProductsComponent";

        public IDictionary<int, ReadyProductView> ReadyViews => _readyViews;
        
        private readonly IUnityPool<ReadyProductView> _readyProductsPool = new UnityPool<ReadyProductView>();
        private readonly IDictionary<int, ReadyProductView> _readyViews = new Dictionary<int, ReadyProductView>();
        private IProductionBuilding _productionBuilding;
        private bool _isTutorialAllowToShow = true;
        private bool _isProducePanelAllowToShow = true;
        private IDisposable _grayscaleEffectDisposable;
        
        private static readonly int GrayscaleMode = Shader.PropertyToID("_GrayscaleMode");
        
        [Inject] public IWorldSpaceCanvas jWorldSpaceCanvas { get; set; }
        [Inject] public IPlayerProfile jPlayerProfile { get; set; }
        [Inject] public IProductsData jProductsData { get; set; }
        [Inject] public IResourcesManager jResourcesManager { get; set; }
        [Inject] public AddRewardItemSignal jAddRewardItemSignal { get; set; }
        [Inject] public ApplyFilterToReadyProducts jApplyFilterToReadyProducts  { get; set; }
        [Inject] public SignalOnConstructControllerChangeState jSignalOnConstructControllerChangeState { get; set; }
        [Inject] public IConstructionController jConstructionController { get; set; }
        [Inject] public IGameCameraModel jGameCameraModel { get; set; }
        [Inject] public IEditModeService jEditModeService { get; set; }
        [Inject] public ISoundManager jSoundManager { get; set; }

        public override void InitializeComponent(Assets.Scripts.Engine.BuildingSystem.MapObjects.MapObjectView target)
        {
            base.InitializeComponent(target);
            
            _productionBuilding = (IProductionBuilding)target.MapObjectModel;
            _readyProductsPool.InstanceProvider = new PrefabInstanceProvider(jWorldSpaceCanvas.ReadyProductViewPrefab);

            _productionBuilding.OnConfirmFinishProduction += OnConfirmFinishProduction;
            _productionBuilding.OnSlotShipped += OnSlotShipped;
            
            jApplyFilterToReadyProducts.AddListener(OnApplyFilterToReadyProducts);
            jSignalOnConstructControllerChangeState.AddListener(OnConstructControllerChangeState);

            _grayscaleEffectDisposable = jGameCameraModel.GrayscaleActiveProperty.Subscribe(OnSetActiveCameraGrayscaleEffect);

            foreach (IProduceSlot produceSlot in _productionBuilding.CurrentProduceSlots)
            {
                produceSlot.IsFinishedProduceConfirm = produceSlot.CurrentProducingTime <= 0;
                
                if (produceSlot.IsFinishedProduce)
                {
                    ShowFinishedSlotProduction(produceSlot, produceSlot.ItemDataIsProducing);
                }
            }
        }

        public override void DisposeComponent()
        {
            base.DisposeComponent();

            _readyProductsPool.Dispose();
            _grayscaleEffectDisposable.Dispose();
            _readyViews.Clear();

            _productionBuilding.OnConfirmFinishProduction -= OnConfirmFinishProduction;
            _productionBuilding.OnSlotShipped -= OnSlotShipped;

            jApplyFilterToReadyProducts.RemoveListener(OnApplyFilterToReadyProducts);
            jSignalOnConstructControllerChangeState.RemoveListener(OnConstructControllerChangeState);
        }
        
        private void OnConstructControllerChangeState(bool state)
        {
            SwitchVisibleState();
        }

        private void OnApplyFilterToReadyProducts(Id_IMapObject mapId, bool isProducePanelForbidToShow)
        {
            if (mapId != TargetModel.MapID) return;

            _isProducePanelAllowToShow = !isProducePanelForbidToShow;
            SwitchVisibleState();
        }

        private void SwitchVisibleState()
        {
            foreach (var readyView in _readyViews)
            {
                readyView.Value.SwitchVisibleState(ReadyProductsVisibilityCheck());
            }
        }
        
        private void OnSetActiveCameraGrayscaleEffect(bool obj)
        {
            Shader.SetGlobalFloat(GrayscaleMode, obj ? 1 : 0);
        }

        private bool ReadyProductsVisibilityCheck()
        {
            if (jConstructionController.IsBuildModeEnabled || jEditModeService.IsEditModeEnable)
                return false;

            return _isTutorialAllowToShow && _isProducePanelAllowToShow;
        }

        private void OnSlotShipped(IProduceSlot slot, IProducingItemData producingItemData)
        {
            if(_readyViews.ContainsKey(producingItemData.OutcomingProducts))//todo fix this check
                _readyViews[producingItemData.OutcomingProducts].Remove(slot);
        }
        
        private void OnConfirmFinishProduction(IProduceSlot produceSlot, IProducingItemData producingData)
        {
            ShowFinishedSlotProduction(produceSlot, producingData);
        }

        private void ShowFinishedSlotProduction(IProduceSlot produceSlot, IProducingItemData producingData)
        {
            IntValue outcomeProducts = producingData.OutcomingProducts;
            if (_readyViews.ContainsKey(outcomeProducts))
            {
                _readyViews[outcomeProducts].AddProduceSlot(produceSlot);
            }
            else
            {
                ReadyProductView readyView = _readyProductsPool.Pop();
                
                readyView.transform.SetParent(jWorldSpaceCanvas.ReadyProductsParent);
                Vector3 position = CalculateProductItemPosition(_readyViews.Count);
                _readyViews.Add(outcomeProducts, readyView);
                readyView.PlayAnimation();
                readyView.SetTexture(jResourcesManager.GetProductSprite(outcomeProducts));
                readyView.AddFirstProduceSlot(Target, produceSlot, position);
                readyView.SwitchVisibleState(ReadyProductsVisibilityCheck());
                
                readyView.OnProduceSlotClick += OnProduceSlotClick;
                readyView.OnBecameEmpty += OnBecameEmpty;
            }
        }

        private void OnBecameEmpty(IntValue id)
        {
            ReadyProductView readyView = _readyViews[id];
            
            readyView.OnProduceSlotClick -= OnProduceSlotClick;
            readyView.OnBecameEmpty -= OnBecameEmpty;
            
            _readyProductsPool.Push(readyView);
            _readyViews.Remove(id);
        }

        private void OnProduceSlotClick(IntValue id, IProduceSlot produceSlot, Assets.Scripts.Engine.BuildingSystem.MapObjects.MapObjectView targetView)
        {
            if (produceSlot.IsWaitingResponse)
            {
                return;
            }

            if (produceSlot.ItemDataIsProducing == null)
                return;

            if (!jPlayerProfile.PlayerResources.IsCanPlaceToWarehouse(produceSlot.ItemDataIsProducing.Amount))
            {
                string localizationText = LocalizationMLS.Instance.GetText(LocalizationKeyConstants.BUILDING_HINT_WAREHOUSE_FULL);

                HintOverObjectUI hintOverObject = jWorldSpaceCanvas.GetHintOverOject();
                hintOverObject.Init(targetView, localizationText);

                return;
            }

            StartCollectProduct(_readyViews[id], produceSlot, targetView);
        }
        
        private void StartCollectProduct(ReadyProductView view, IProduceSlot produceSlot, Assets.Scripts.Engine.BuildingSystem.MapObjects.MapObjectView targetView)
        {
            PlayCollectProductAnimations(view, produceSlot);
            jSoundManager.Play(SfxSoundTypes.CollectItems, SoundChannel.SoundFX);
            IProductionBuilding productionBuilding = (IProductionBuilding)targetView.MapObjectModel;
            productionBuilding.ShipProduct(produceSlot, null);
        }
        
        private void PlayCollectProductAnimations(ReadyProductView itemView, IProduceSlot slot)
        {
            IProducingItemData itemData = slot.ItemDataIsProducing;
            string xpAmount = (jProductsData.GetProduct(itemData.OutcomingProducts).ExperienceUp * itemData.Amount).ToString();
            string productsAmount = itemData.Amount.ToString();

            Sprite productSprite = jResourcesManager.GetProductSprite(itemData.OutcomingProducts);


            Vector3 startPosition = itemView.transform.position;
            
            jAddRewardItemSignal.Dispatch(new AddItemSettings(FlyDestinationType.Warehouse,startPosition, productsAmount, productSprite));
            jAddRewardItemSignal.Dispatch(new AddItemSettings(FlyDestinationType.Experience,startPosition, xpAmount));
        }

        private static readonly Vector3[] ProductItemsPositions = {
            new Vector3(0, 0, 0),
            new Vector3(0.6f, 0.4f ,0),
            new Vector3(0.9f, -0.8f, 0),
            new Vector3(-0.2f, -0.9f, 0),
            new Vector3(-1.2f, -0.1f, 0),
            new Vector3(-1.4f, 0.9f, 0),
            new Vector3(-0.7f, 1.3f, 0),
            new Vector3(0.2f, 1.5f, 0),
            new Vector3(1.2f, 1.0f, 0),
            new Vector3(1.9f, 0.1f, 0),
            new Vector3(2.3f, -0.7f, 0),
            new Vector3(2.5f, -1.4f, 0),
            new Vector3(2.3f, -1.9f, 0),
            new Vector3(1.6f, -2.1f, 0),
            new Vector3(1.0f, -2.3f, 0),
            new Vector3(0.1f, -2.1f, 0),
            new Vector3(-0.6f, -1.4f, 0)
        };

        private Vector3 CalculateProductItemPosition(int itemIndex)
        {
            if (itemIndex < ProductItemsPositions.Length)
            {
                return ProductItemsPositions[itemIndex];
            }
            else
            {
                const float maxOffset = 1.2f;
                return new Vector3(Random.Range(-maxOffset, maxOffset), Random.Range(-maxOffset, maxOffset));
            }
        }
    }
}
