﻿using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using NanoReality.Engine.BuildingSystem.CityCamera;
using NanoReality.Engine.UI.Extensions.EditModePanels;
using strange.extensions.signal.impl;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using UnityEngine;

namespace NanoReality.Engine.BuildingSystem.MapObjects.Components
{
	public class MapObjectSemiTransparentComponent : MapObjectViewComponent
	{
		private bool _isSemiTransparentActive;
		
		private Material _regularMaterial; 
		private Material _semiTransparentMaterial; 
		
		public bool IsSemiTransparentActive
		{
			get
			{
				return _isSemiTransparentActive;
			}
			set
			{
				if (_isSemiTransparentActive != value)
				{
					_isSemiTransparentActive = value;
					SignalOnSemiTransparentChanged.Dispatch (Target, value);
				}
			}
		}

		public Signal<MapObjectView, bool> SignalOnSemiTransparentChanged  = new Signal<MapObjectView, bool>();

		#region Injects

		[Inject] public SignalOnConstructControllerChangeState jSignalOnConstructControllerChangeState { get; set; }
		[Inject] public IGlobalMapObjectViewSettings jGlobalMapObjectViewSettings { get; set; }
		[Inject] public IGameCameraModel jGameCameraModel { get; set; }
		[Inject] public IEditModeService jEditModeService { get; set; }

		#endregion

		public override void InitializeComponent (MapObjectView target)
		{
			base.InitializeComponent (target);
			jSignalOnConstructControllerChangeState.AddListener (OnConstructionControllerStateChangedSignal);
			
			_regularMaterial = target.SpriteRenderer.sharedMaterial;
			_semiTransparentMaterial = jGlobalMapObjectViewSettings.BuildingSemiTransparentMaterial;
		}

		public override void DisposeComponent ()
		{
			if (IsSemiTransparentActive)
				RemoveSemiTransparentMaterial();

			jSignalOnConstructControllerChangeState.RemoveListener (OnConstructionControllerStateChangedSignal);
			SignalOnSemiTransparentChanged.RemoveAllListeners ();
			SignalOnSemiTransparentChanged.RemoveAllOnceListeners ();

			base.DisposeComponent();
		}

		protected virtual void SetSemiTransparentMaterial()
		{

			if (Target == null || Target.SelectComponent != null && Target.SelectComponent.IsSelected)
			{
				return;
			}

			if (jGameCameraModel.GrayscaleActiveProperty.Value)
			{
				return;
			}

			Target.SetColliderActive(false);

			if (Target.StaticAnimationsComponent != null)
			{
				Target.StaticAnimationsComponent.SetMaterialToAllSprites(_semiTransparentMaterial);
				Target.StaticAnimationsComponent.SwitchVfxParticles(false);
			}

			if (Target.ConstructionComponent != null && Target.ConstructionComponent.ConstructView != null)
			{
				Target.ConstructionComponent.ConstructView.SetMaterialToAllSprites(_semiTransparentMaterial);
				Target.ConstructionComponent.ConstructView.SwitchVfxParticles(false);
			}

			IsSemiTransparentActive = true;
		}

		protected virtual void RemoveSemiTransparentMaterial()
		{
			if (Target == null) return;

			Target.SetColliderActive(true);

			if (Target.StaticAnimationsComponent != null)
			{
				Target.StaticAnimationsComponent.SetMaterialToAllSprites(_regularMaterial);
				Target.StaticAnimationsComponent.SwitchVfxParticles(true);
			}

			if (Target.ConstructionComponent != null && Target.ConstructionComponent.ConstructView != null)
			{
				Target.ConstructionComponent.ConstructView.SetMaterialToAllSprites(_regularMaterial);
				Target.ConstructionComponent.ConstructView.SwitchVfxParticles(true);
			}

			IsSemiTransparentActive = false;
		}

		protected virtual void OnConstructionControllerStateChangedSignal(bool isEnabledConstruct)
		{
			if (jEditModeService.IsEditModeEnable && jEditModeService.IsAOEEnable) return;

			if (isEnabledConstruct)
			{
				SetSemiTransparentMaterial();
			}
			else
			{
				RemoveSemiTransparentMaterial();
			}
		}
	}
}
