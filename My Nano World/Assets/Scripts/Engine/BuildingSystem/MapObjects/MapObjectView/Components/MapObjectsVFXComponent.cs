﻿using NanoReality.Engine.UI;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using Assets.NanoLib.UI.Core.Signals;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using Assets.Scripts.Engine.UI.Extensions.BuildFx;
using Assets.Scripts.Engine.UI.Extensions.FireFx;
using Assets.Scripts.Engine.UI.Extensions.Highlight;
using NanoReality.Engine.UI.Extensions.EditModePanels;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;

namespace NanoReality.Engine.BuildingSystem.MapObjects.Components
{
	public class MapObjectsVFXComponent : MapObjectViewComponent
	{
		#region Injects

        [Inject] public IPlayerProfile jPlayerProfile { get; set; }
        [Inject] public IUserLevelsData jUserLevelsData { get; set; }
        [Inject] public IWorldSpaceCanvas jWorldSpaceCanvas {get; set;}
        [Inject] public UserLevelUpSignal jUserLevelUpSignal { get; set; }
        [Inject] public SignalOnConstructControllerChangeState jSignalOnConstructControllerChangeState { get; set; }
        [Inject] public IEditModeService jEditModeService { get; set; }
        
        #endregion

        private BuildingBuildEffect BuildingBuildEffect { get; set; }

        private HighlightEffect HighlightEffect { get; set; }

        private FireEffect FireEffect { get; set; }

        private BuildingSpeedUpEffect BuildingSpeedUpEffect { get; set; }

        private BuildingUpgradeEffect BuildingUpgradeEffect { get; set; }

		private BuildingSatisfactionChangeEffect BuildingSatisfactionChangeEffect { get; set; }

		public override void InitializeComponent(MapObjectView target)
		{
			base.InitializeComponent(target);

            if(TargetModel != null)
            {
                TargetModel.EventOnMapObjectBuildStarted.AddListener(OnMapObjectBuildingStarted);
                TargetModel.EventOnMapObjectBuildFinished.AddListener(OnMapObjectBuildingFinished);                
            }
		}	
		
		public override void DisposeComponent()
		{
			if (TargetModel != null)
			{
				TargetModel.EventOnMapObjectBuildStarted.RemoveListener(OnMapObjectBuildingStarted);
				TargetModel.EventOnMapObjectBuildFinished.RemoveListener(OnMapObjectBuildingFinished);
			}

			if (BuildingSatisfactionChangeEffect != null)
			{
				BuildingSatisfactionChangeEffect.FreeObject();
				BuildingSatisfactionChangeEffect = null;
			}
			
			base.DisposeComponent();
		}

	    public void ShowSpeedUpEffect()
		{
			if(jEditModeService.IsEditModeEnable) return;
			
			if (BuildingSpeedUpEffect == null)
			{
				BuildingSpeedUpEffect = jWorldSpaceCanvas.GetSpeedFxItem();
			}

			if (BuildingSpeedUpEffect != null)
			{
				BuildingSpeedUpEffect.SetTarget(Target);
				BuildingSpeedUpEffect.Play();
			}
		}

	    public void ShowUpgradeFxEffect()
		{
			if (BuildingUpgradeEffect == null)
			{
				BuildingUpgradeEffect = jWorldSpaceCanvas.GetUpgradeFxItem();
			}

			if (BuildingUpgradeEffect != null)
			{
				BuildingUpgradeEffect.SetTarget(Target);
				BuildingUpgradeEffect.Play();
			}
		}

        /// <summary>
        /// Показать эффект удовлетворенности жителей
        /// </summary>
        /// <param name="isSatisfied">If set to <c>true</c> is satisfied.</param>
        public void ShowSatisfactionIndicator(SatisfactionState state)
        {
            if (!Target.MapObjectModel.IsConstructed || jEditModeService.IsEditModeEnable)
                return;

            if (jPlayerProfile.CurrentLevelData.CurrentLevel < jUserLevelsData.LevelToOpenHappiness)
            {
	            return;
            }
            
            if (BuildingSatisfactionChangeEffect == null)
            {
                BuildingSatisfactionChangeEffect = jWorldSpaceCanvas.GetSatisfactionChangeItem();
            }

            if (BuildingSatisfactionChangeEffect != null)
            {
                BuildingSatisfactionChangeEffect.SatisfactionState = state;
                BuildingSatisfactionChangeEffect.SetTarget(Target);
                BuildingSatisfactionChangeEffect.Play();
            }
        }
        
        public void HideSatisfactionIndicator()
        {
	        if (BuildingSatisfactionChangeEffect == null)
	        {
		        BuildingSatisfactionChangeEffect = jWorldSpaceCanvas.GetSatisfactionChangeItem();
	        }

	        if (BuildingSatisfactionChangeEffect != null)
	        {
		        BuildingSatisfactionChangeEffect.Stop();
	        }
        }

        public void ShowHighlightEffect()
        {
            if (HighlightEffect == null)
            {
                HighlightEffect = jWorldSpaceCanvas.GetHighlightFxItem();
            }

            if (HighlightEffect != null)
            {
                HighlightEffect.SetTarget(Target);
                HighlightEffect.Play();
            }
        }

        public void HideHighlightEffect()
        {
            if (HighlightEffect != null)
            {
                HighlightEffect.Stop();
                HighlightEffect = null;
            }
        }

        public void ShowFireEffect()
        {
            if (FireEffect == null)
            {
                FireEffect = jWorldSpaceCanvas.GetFireFxItem();
            }

            if (FireEffect != null)
            {
                FireEffect.SetTarget(Target);
                FireEffect.Play();
            }
        }

        public void HideFireEffect()
        {
            if (FireEffect != null)
            {
                FireEffect.Stop();
                FireEffect = null;
            }
        }

        /// <summary>
		/// Add a progress bar when the building of map object has started
		/// </summary>
		protected virtual void OnMapObjectBuildingStarted(IMapObject mapObject)
		{
			ShowBuildingStartEffect(mapObject);
		}

	    private void ShowBuildingStartEffect(IMapObject mapObject)
	    {
	        if (mapObject.MapObjectType == MapObjectintTypes.Road)
	        {
	            return;
	        }
            if (BuildingBuildEffect == null)
            {
                BuildingBuildEffect = jWorldSpaceCanvas.GetBuildEffect();
            }

            if (BuildingBuildEffect != null)
            {
                BuildingBuildEffect.SetTarget(Target);
                BuildingBuildEffect.Play();
            }
        }


	    /// <summary>
		/// remove the progress bar when the building is ready
		/// </summary>
		/// <param name="mapObject"></param>
		protected virtual void OnMapObjectBuildingFinished(IMapObject mapObject)
		{
		    if (mapObject.IsSkipped)
		    {
		        ShowSpeedUpEffect();
		    }
		    else
		    {
		        if (!(Target.MapObjectModel is IRoad))
		            ShowUpgradeFxEffect();
		    }
		    mapObject.IsSkipped = false;
		}
	}
}

