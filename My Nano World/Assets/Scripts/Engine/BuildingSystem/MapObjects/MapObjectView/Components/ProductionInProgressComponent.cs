﻿using System;
using Assets.NanoLib.Utilities;
using NanoReality.Engine.BuildingSystem.CityCamera;
using NanoReality.Engine.UI;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Controllers.Agricultural;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Factories;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Production;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using UniRx;

namespace NanoReality.Engine.BuildingSystem.MapObjects.Components
{
    public class ProductionInProgressComponent : MapObjectViewComponent
    {
        [Inject] public IWorldSpaceCanvas jWorldSpaceCanvas {get; set;}
        [Inject] public IConstructionController jConstructionController { get; set; }
        [Inject] public SignalOnConstructControllerChangeState jSignalOnConstructControllerChangeState { get; set; }
        [Inject] public OnProductProduceAndCollectSignal jOnProductProduceAndCollectSignal { get; set; }
        [Inject] public IGameCameraModel jGameCameraModel { get; set; }

        private BuildingDowntimeEffect _downtimeEffect;
        private WorkInProgressEffect _workInProgressEffect;
        private IProductionBuilding _productionBuilding;
        
        private IDisposable _grayscaleEffectDisposable;
        
        public override void InitializeComponent(Assets.Scripts.Engine.BuildingSystem.MapObjects.MapObjectView target)
        {
            base.InitializeComponent(target);
            
            _productionBuilding = (IProductionBuilding)target.MapObjectModel;

            TargetModel.EventOnMapObjectBuildStarted.AddListener(OnMapObjectBuildingStarted);
            TargetModel.EventOnMapObjectBuildFinished.AddListener(OnMapObjectBuildingFinished);
            jSignalOnConstructControllerChangeState.AddListener(OnChangeConstractController);
            jOnProductProduceAndCollectSignal.AddListener(OnProductCollect);
            Target.AttentionComponent.SignalOnAttentionViewStateChanged += OnAttenitionViewStateChanged;
            
            _grayscaleEffectDisposable = jGameCameraModel.GrayscaleActiveProperty.Subscribe(OnSetActiveCameraGrayscaleEffect);

            _productionBuilding.StartedProduction += OnStartProduction;
            _productionBuilding.OnConfirmFinishProduction += OnConfirmFinishProduction;
        }

        public override void DisposeComponent()
        {
            TargetModel.EventOnMapObjectBuildStarted.RemoveListener(OnMapObjectBuildingStarted);
            TargetModel.EventOnMapObjectBuildFinished.RemoveListener(OnMapObjectBuildingFinished);
            jSignalOnConstructControllerChangeState.RemoveListener(OnChangeConstractController);
            jOnProductProduceAndCollectSignal.RemoveListener(OnProductCollect);
            Target.AttentionComponent.SignalOnAttentionViewStateChanged -= OnAttenitionViewStateChanged;
            
            _grayscaleEffectDisposable.Dispose();
            
            _productionBuilding.StartedProduction -= OnStartProduction;
            _productionBuilding.OnConfirmFinishProduction -= OnConfirmFinishProduction;
            
            StopEffects();
            
            base.DisposeComponent();
        }
        
        public void UpdateEffects()
        {
            if (IsPreConditionSatisfied())
            {
                PlayEffects();
            }
            else
            {
                StopEffects();
            }
        }

        private void OnConfirmFinishProduction(IProduceSlot produceSlot, IProducingItemData producingItemData)
        {
            UpdateEffects();
        }

        private void OnStartProduction(IProduceSlot produceSlot, IProducingItemData producingItemData)
        {
            UpdateEffects();
        }

        private void OnProductCollect(IMapObject mapObj, Id<Product> productId, int amount)
        {
            if (mapObj == Target.MapObjectModel)
            {
                UpdateEffects();
            }
        }

        private void OnChangeConstractController(bool isBuildModeEnable)
        {
            UpdateEffects();
        }
        
        private void OnAttenitionViewStateChanged(bool isAttentinShown)
        {
            UpdateEffects();
        }
        
        private void OnSetActiveCameraGrayscaleEffect(bool showGrayScale)
        {
            UpdateEffects();
        }

        private void OnMapObjectBuildingFinished(IMapObject obj)
        {
            UpdateEffects();
        }

        private void OnMapObjectBuildingStarted(IMapObject obj)
        {
            StopEffects();
        }

        private bool IsPreConditionSatisfied()
        {
            if (TargetModel.IsUpgradingNow)
                return false;
            
            if (jConstructionController.IsBuildModeEnabled)
                return false;

            if (Target.AttentionComponent.IsShown)
                return false;

            if (jGameCameraModel.GrayscaleActive)
                return false;

            if (_productionBuilding.HasProductsToCollect())
                return false;

            return true;
        }

        private void PlayEffects()
        {
            if (_productionBuilding.IsProduced())
            {
                StopDowntimeEffect();

                if (_workInProgressEffect == null)
                {
                    _workInProgressEffect = jWorldSpaceCanvas.GetWorkInProgressItem();
                    _workInProgressEffect.SetTarget(Target);
                    _workInProgressEffect.Play();
                }
            }
            else
            {
                StopWorkInProgressEffect();

                if (_downtimeEffect == null)
                {
                    _downtimeEffect = jWorldSpaceCanvas.GetDowntimeItem();
                    _downtimeEffect.SetTarget(Target);
                    _downtimeEffect.Play();
                }
            }
        }

        private void StopEffects()
        {
            StopWorkInProgressEffect();
            StopDowntimeEffect();
        }

        private void StopWorkInProgressEffect()
        {
            if (_workInProgressEffect != null)
            {
                _workInProgressEffect.Stop();
                _workInProgressEffect = null;
            }
        }

        private void StopDowntimeEffect()
        {
            if (_downtimeEffect != null)
            {
                _downtimeEffect.Stop();
                _downtimeEffect = null;
            }
        }
    }
}
