﻿using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using NanoReality.Engine.BuildingSystem.MapObjects.ConstractAnimations;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.Settings.BuildSettings.Models.Api;
using UnityEngine;

namespace NanoReality.Engine.BuildingSystem.MapObjects.Components
{
    public class ConstructionComponent : MapObjectViewComponent
    {
        #region Injects

        [Inject] public IBuildingSitesContainer jConstractAnimationsContainer { get; set; }

        #endregion

        private Renderer ViewRenderer => Target == null ? null : Target.SpriteRenderer;

        private MeshCollider ViewCollider => Target.MeshCollider;
        
        public BuildingSiteView ConstructView { get; private set; }

        private Mesh _targetViewMesh;

        public override void InitializeComponent(MapObjectView target)
        {
            base.InitializeComponent(target);
            TargetModel?.EventOnMapObjectBuildStarted.AddListener(OnMapObjectBuildingStarted);
            TargetModel?.EventOnMapObjectBuildFinished.AddListener(OnMapObjectBuildingFinished);
            
            // стартуем анимацию постойки сразу после инициализации компонента, если объект строится
            if (Target.MapObjectModel.CurrentConstructTime > 0 && !Target.MapObjectModel.IsConstructed && Target.MapObjectModel.MapID.Value > 0)
            {
                ShowConstractionAnimation();
            }
        }

        public override void DisposeComponent()
        {
            TargetModel?.EventOnMapObjectBuildStarted.RemoveListener(OnMapObjectBuildingStarted);
            TargetModel?.EventOnMapObjectBuildFinished.RemoveListener(OnMapObjectBuildingFinished);
            base.DisposeComponent();
        }

        public void ShowConstructionView()
        {
            if (ConstructView == null)
            {
                ConstructView = jConstractAnimationsContainer.GetAnimationView(Target.MapObjectModel.Dimensions.intX);
            }
            
            ConstructView.SetTarget(Target);
            ConstructView.SwitchVfxParticles(true);
            ViewRenderer.enabled = false;

            ApplyBuildingSiteCollider();
        }

        public void HideConstractionView()
        {
            if (ConstructView != null)
            {
                ConstructView.FreeObject();
                ConstructView = null;
            }

            ViewRenderer.enabled = true;

            ApplyBuildingViewCollider();
        }
        
        public void ShowConstractionAnimation()
        {
            Target.StaticAnimationsComponent?.SwitchAnimationsRenders(false);
            ShowConstructionView();
        }
        
        public void HideConstractionAnimation()
        {
            Target.StaticAnimationsComponent?.SwitchAnimationsRenders(true);
            HideConstractionView();
        }

        protected virtual void OnMapObjectBuildingStarted(IMapObject mapObject)
        {
            if (mapObject.CurrentConstructTime > 0)
            {
                ShowConstractionAnimation();
            }
        }
        
        protected virtual void OnMapObjectBuildingFinished(IMapObject mapObject)
        {
            if (!Target.KeepConstructAnimation)
            {
                HideConstractionAnimation();
            }
        }

        private void ApplyBuildingSiteCollider()
        {
            Mesh buildingSiteMesh = jConstractAnimationsContainer.GetBuildingSiteMesh(ConstructView.ViewSize);
            ViewCollider.sharedMesh = buildingSiteMesh;
        }

        private void ApplyBuildingViewCollider()
        {
            Target.SetColliderMesh();
        }
    }
}