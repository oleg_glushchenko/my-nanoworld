﻿using Assets.Scripts.Engine.BuildingSystem.MapObjects.Components;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using UnityEngine;

namespace NanoReality.Engine.BuildingSystem.MapObjects.Components
{
    public class DwellingObjectAnimationComponent : MapObjectViewComponent
    {
        [Inject] public SignalOnMapBuildingUpgradeVerificated jSignalOnMapBuildingUpgradeVerificated { get; set; }
        
        private static readonly int ConstructedFinishedAnimationId = Animator.StringToHash("Work");
        private static readonly int StartConstructionAnimationId = Animator.StringToHash("Idle");
        public override void InitializeComponent(Assets.Scripts.Engine.BuildingSystem.MapObjects.MapObjectView target)
        {
            base.InitializeComponent(target);

            if (target.Animator == null)
            {
                return;
            }

            SetTrigger();
            
            jSignalOnMapBuildingUpgradeVerificated.AddListener(OnMapBuildingUpgradeVerificated);
        }

        public override void DisposeComponent()
        {
            base.DisposeComponent();
            
            jSignalOnMapBuildingUpgradeVerificated.RemoveListener(OnMapBuildingUpgradeVerificated);
        }

        private void OnMapBuildingUpgradeVerificated(IMapBuilding mapBuilding)
        {
            if (mapBuilding.MapID == Target.MapObjectModel.MapID)
            {
                SetTrigger();
            }
        }

        private void SetTrigger()
        {
            if (IsConstructedFinished())
            {
                Target.Animator.ResetTrigger(StartConstructionAnimationId);
                Target.Animator.SetTrigger(ConstructedFinishedAnimationId);
            }
            else
            {
                Target.Animator.ResetTrigger(ConstructedFinishedAnimationId);
                Target.Animator.SetTrigger(StartConstructionAnimationId);
            }
        }

        private bool IsConstructedFinished()
        {
            return Target.MapObjectModel.Level > 0;
        }
    }
}
