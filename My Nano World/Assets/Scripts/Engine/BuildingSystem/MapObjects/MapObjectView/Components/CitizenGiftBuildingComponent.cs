using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Engine.Analytics.Model.api;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using Engine.UI;
using NanoReality.Core.Resources;
using NanoReality.Engine.UI;
using NanoReality.Engine.UI.Extensions.EditModePanels;
using NanoReality.Engine.UI.Views.WorldSpaceUI;
using NanoReality.Game.CitizenGifts;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.PlayerProfile.Models.Impl;
using NanoReality.GameLogic.Services;
using NanoReality.UI.Components;
using UnityEngine;
using Random = UnityEngine.Random;

namespace NanoReality.Engine.BuildingSystem.MapObjects.Components
{
    public sealed class CitizenGiftBuildingComponent : MapObjectViewComponent
    {
        [Inject] public ICityGiftsService jCityGiftService { get; set; }
        [Inject] public IWorldSpaceCanvas jWorldSpaceCanvas { get; set; }
        [Inject] public IResourcesManager jResourcesManager { get; set; }
        [Inject] public IPlayerProfile jPlayerProfile { get; set; }
        [Inject] public IIconProvider jIconProvider { get; set; }
        [Inject] public INanoAnalytics jNanoAnalytics { get; set; }
        [Inject] public AddRewardItemSignal jAddRewardItemsSignal { get; set; }
        [Inject] public IEditModeService jEditModeService { get; set; }

        public Action OnGiftClaimed;

        private readonly List<GiftAttention> _giftsAttentions = new List<GiftAttention>();

        private const float MaxOffset = 0.8f;

        public override void InitializeComponent(MapObjectView target)
        {
            base.InitializeComponent(target);

            jCityGiftService.GiftsUpdates += OnGiftsUpdated;
        }

        public override void DisposeComponent()
        {
            base.DisposeComponent();

            jCityGiftService.GiftsUpdates -= OnGiftsUpdated;

            if (_giftsAttentions != null)
            {
                DisposeAttentions();
            }
        }

        public CitizenGiftAttentionView GetFirstGiftView()
        {
            GiftAttention first = null;
            
            foreach (var attention in _giftsAttentions)
            {
                first = attention;
                break;
            }

            return first?.attentionView;
        }

        private void OnGiftsUpdated()
        {
            if (jEditModeService.IsEditModeEnable || !jCityGiftService.HasAvailableGifts(Target.MapId) || !TargetModel.IsFunctional())
            {
                return;
            }

            foreach (var citizenGift in jCityGiftService.GetGifts(Target.MapId))
            {
                var giftAttention = _giftsAttentions.FirstOrDefault(attention => attention.gift.Id == citizenGift.Id);
                if (giftAttention != null) continue;

                var randomOffset = new Vector3(Random.Range(-MaxOffset, MaxOffset), Random.Range(-MaxOffset, MaxOffset));
                var view = jWorldSpaceCanvas.PopCitizenGiftAttention();
                view.Init(citizenGift.Id, Target, randomOffset);
                view.RewardIconSprite = jIconProvider.GetSprite(citizenGift.Reward);
                view.ClaimClicked += OnClaimGiftClicked;
                view.Show(() => { view.Interactable = true; });

                giftAttention = new GiftAttention
                {
                    attentionView = view,
                    gift = citizenGift
                };

                _giftsAttentions.Add(giftAttention);
            }
        }

        private void OnClaimGiftClicked(int giftId)
        {
            var giftToClaim = _giftsAttentions.FirstOrDefault(giftAttention => giftAttention.gift.Id == giftId);
            if (giftToClaim == null) return;

            _giftsAttentions.Remove(giftToClaim);

            var view = giftToClaim.attentionView;
            view.Interactable = false;
            view.ClaimClicked -= OnClaimGiftClicked;
            view.Hide(() =>
            {
                CreateFlyRewardItems(giftToClaim);

                giftToClaim.attentionView.Dispose();
                giftToClaim.attentionView.ClaimClicked -= OnClaimGiftClicked;
                jWorldSpaceCanvas.PushCitizenGiftAttention(giftToClaim.attentionView);
            });

            var gift = giftToClaim.gift;
            jCityGiftService.ClaimGift(gift.BuildingId, gift.Id, () =>
            {
                OnGiftClaimed?.Invoke();
                jNanoAnalytics.FoodSupplyGift(gift.Id);
            });
        }

        private void CreateFlyRewardItems(GiftAttention giftAttention)
        {
            var gift = giftAttention.gift;
            var fromPosition = giftAttention.attentionView.transform.position;
            var giftReward = gift.Reward;

            var coinsCount = giftReward.Coins;
            var nanoBucksCount = giftReward.NanoBucks;
            var goldBarsCount = giftReward.GoldBars;
            var product = giftReward.Product;

            if (giftReward.HaveCoins())
            {
                var addItemSettings = new AddItemSettings(FlyDestinationType.SoftCoins, fromPosition, coinsCount);
                jAddRewardItemsSignal.Dispatch(addItemSettings);
                jPlayerProfile.PlayerResources.AddSoftCurrency(coinsCount);
                jNanoAnalytics.OnPlayerCurrenciesIncrease(PlayerResources.CurencyType.Coins, coinsCount, AwardSourcePlace.FoodSupplyReward, gift.Id.ToString());
            }

            if (giftReward.HaveNanoBucks())
            {
                var addItemSettings = new AddItemSettings(FlyDestinationType.PremiumCoins, fromPosition, nanoBucksCount);
                jAddRewardItemsSignal.Dispatch(addItemSettings);
                jPlayerProfile.PlayerResources.AddHardCurrency(nanoBucksCount);
                jNanoAnalytics.OnPlayerCurrenciesIncrease(PlayerResources.CurencyType.Bucks, nanoBucksCount, AwardSourcePlace.FoodSupplyReward, gift.Id.ToString());
            }

            if (giftReward.HaveGoldBars())
            {
                var addItemSettings = new AddItemSettings(FlyDestinationType.GoldCurrency, fromPosition, goldBarsCount);
                jAddRewardItemsSignal.Dispatch(addItemSettings);
                jPlayerProfile.PlayerResources.AddGoldCurrency(goldBarsCount);
                jNanoAnalytics.OnPlayerCurrenciesIncrease(PlayerResources.CurencyType.Gold, goldBarsCount, AwardSourcePlace.FoodSupplyReward, gift.Id.ToString());
            }

            if (giftReward.HaveProduct())
            {
                var productSprite = jResourcesManager.GetProductSprite(product.ProductId.Value);
                var addItemSettings = new AddItemSettings(FlyDestinationType.Warehouse, fromPosition, product.Count, productSprite);
                jAddRewardItemsSignal.Dispatch(addItemSettings);
                jPlayerProfile.PlayerResources.AddProduct(product.ProductId, product.Count);
            }
        }

        private void DisposeAttentions()
        {
            foreach (var giftAttention in _giftsAttentions)
            {
                giftAttention.attentionView.Dispose();
                giftAttention.attentionView.ClaimClicked -= OnClaimGiftClicked;

                jWorldSpaceCanvas.PushCitizenGiftAttention(giftAttention.attentionView);
            }

            _giftsAttentions.Clear();
        }

        private class GiftAttention
        {
            public CitizenGiftAttentionView attentionView;
            public CitizenGift gift;
        }
    }
}