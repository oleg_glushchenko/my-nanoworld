using System.Collections.Generic;
using Assets.Scripts.Engine.UI.Views.Popups;
using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using NanoReality.Engine.BuildingSystem.MapObjects.Components;
using NanoReality.Game.Ui;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.Upgrade;

namespace Assets.Scripts.Engine.BuildingSystem.MapObjects
{
    public class DwellingMapObjectView : AMapBuildingView, INeedPowerMapObjectView
    {
        private IDwellingHouseMapObject TargetModel => MapObjectModel as IDwellingHouseMapObject;

        public override bool KeepConstructAnimation => MapObjectModel != null && MapObjectModel.Level < 1;
        
        [Inject] public IPopupManager jPopupManager { get; set; }

        protected override List<string> GetComponentsNames()
        {
            var result = base.GetComponentsNames();
            result.Add(nameof(DwellingObjectAnimationComponent));
            result.Add(nameof(HighlightHappinesComponent));
            result.Add(nameof(CitizenGiftBuildingComponent));
            return result;
        }

        public override void InitializeView(IMapObject model)
        {
            base.InitializeView(model);

            if (KeepConstructAnimation)
            {
                ConstructionComponent.ShowConstructionView();
            }
            else if(!model.IsConstructed)
            {
                ConstructionComponent.ShowConstructionView();
            }
            model.EventOnMapObjectBuildFinished.AddListener(OnBuildingConstructed);
        }

        public override void DisposeView()
        {
            MapBuildingModel.EventOnMapObjectBuildFinished.RemoveListener(OnBuildingConstructed);
            VFXComponent?.HideSatisfactionIndicator();
            base.DisposeView();
        }
        
        protected override void OnComponentsInitialize()
        {
            base.OnComponentsInitialize();
            TargetModel.OnHappinessChanged += OnHappinessChanged;
            OnHappinessChanged(TargetModel.IsHappinessConnected);
        }

        protected override void OnComponentsDispose()
        {
            base.OnComponentsDispose();
            TargetModel.OnHappinessChanged -= OnHappinessChanged;
        }

        protected void OnHappinessChanged(SatisfactionState state)
        {
            if (TargetModel.MaxPopulation > 0)
            {
                VFXComponent.ShowSatisfactionIndicator(state);
            }
        }

        private void OnBuildingConstructed(IMapObject obj)
        {
            OnHappinessChanged(TargetModel.IsHappinessConnected);
        }
        
        protected override void OnBuildingTap()
        {
            base.OnBuildingTap();
            if (TargetModel.Level >= 1)
            {
                jShowWindowSignal.Dispatch(typeof(DwellingPanelView), MapBuildingModel);
            }
            else
            {
                if (TargetModel.CanBeUpgraded)
                {
                    jShowWindowSignal.Dispatch(typeof(UpgradeWithProductsPanelView), TargetModel);
                }
                else
                {
                    jPopupManager.Show(new PopulationLimitPopupSettings());
                }
            }
        }
    }
}