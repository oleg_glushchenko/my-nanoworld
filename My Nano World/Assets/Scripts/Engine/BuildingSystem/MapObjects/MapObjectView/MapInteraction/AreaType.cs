﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NanoReality.Engine.BuildingSystem.MapObjects
{
    public enum AreaType
    {
        None,
        Ground,
        Dust,
        Road,
        Water
    }
}
