﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using Assets.Scripts.GameLogic.BuildingSystem.Products.Models.api;
using Engine.UI;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Controllers.Agricultural;
using UnityEngine;
using NanoReality.Engine.BuildingSystem.MapObjects.AgroField;
using NanoReality.Engine.UI.Extensions.Agricultural;
using NanoReality.GameLogic.BuildingSystem.MapObjects;
using NanoReality.UI.Components;

namespace NanoReality.Engine.BuildingSystem.MapObjects
{
    public class AgroFieldBuildingView : AMapBuildingView
    {
	    [Inject] public IProductsData jProductsData { get; set; }
	    [Inject] public OnChangeGrowingState jOnChangeGrowingState { get; set; }
	    [Inject] public AddRewardItemSignal jAddRewardItemSignal { get;set; }

        #region InspectorFields
        
        [SerializeField] private FieldSateView[] fields;
	    
	    [SerializeField] private List<GameObject> _domeObjects;
	    [SerializeField] private SpriteRenderer _productIconSpriteRenderer;
	    [SerializeField] private GameObject _OnCompleatFx;

        #endregion

        private IAgroFieldBuilding _currentModel;
        private AgriculturalFieldState _currentState;

        /// <summary>
        /// Устанавливается в true, когда анимация косилки достигла нужного состояния для исчезновения урожая с поля
        /// </summary>
        private bool _isCanShowCroppedField;
        
		protected override void AddListeners ()
		{
			base.AddListeners ();

		    jOnChangeGrowingState.AddListener(UpdateFieldView);

		    var model = (IAgroFieldBuilding)MapObjectModel;
		    model.OnHarvestStarted.AddListener(OnHarvestStarted);
		}

        protected override void RemoveListeners()
        {
            base.RemoveListeners();

            jOnChangeGrowingState.RemoveListener(UpdateFieldView);

            var model = (IAgroFieldBuilding)MapObjectModel;
            model.OnHarvestStarted.RemoveListener(OnHarvestStarted);
        }

        protected override void OnBuildingTap()
        {
	        jShowWindowSignal.Dispatch(typeof(AgroFieldPanelView), MapBuildingModel);
        }

        protected override List<string> GetComponentsNames()
        {
            var list = base.GetComponentsNames();
            list.Remove("VFXComponent");
            return list;
        }

        public override void InitializeView(IMapObject model)
        {
            base.InitializeView(model);

            _currentModel = (IAgroFieldBuilding)MapObjectModel;

            // включаем купол вручную, если в предыдущей сессии началось производство растения
            if (_currentModel.GrowingTimeLeft > 0)
	        {
		        for (int i = 0; i < _domeObjects.Count; i++)
		        {
			        _domeObjects[i].SetActive(true);
		        }
	        }
	        
            UpdateFieldView(_currentModel);
        }

        private void ClearField()
        {
	        foreach (var stateView in fields)
	        {
		        stateView.Hide();
		        _productIconSpriteRenderer.enabled = false;
		        _productIconSpriteRenderer.sprite = null;
	        }
        }

	    private void UpdateFieldView(IAgroFieldBuilding agroField)
        {
            if(MapId != agroField.MapID) return;

            var state = agroField.CurrentState;
            if (_currentState == AgriculturalFieldState.Completed && state == AgriculturalFieldState.Empty && !_isCanShowCroppedField)
            {
                return;
            }

            ClearField();
            _currentState = state;
            _OnCompleatFx.SetActive(_currentState == AgriculturalFieldState.Completed);
	        
            if (_currentState == AgriculturalFieldState.Empty)
            { 
                return;
            }

            var field = fields.FirstOrDefault(v =>  v.State == state);
            if (field != null)
            {
                var productSprite = jResourcesManager.GetProductSprite(_currentModel.CurrentGrowingPlant.OutcomingProducts);
	            field.ShowState(_currentModel.CurrentGrowingPlant.OutcomingProducts);
	            _productIconSpriteRenderer.enabled = true;
	            _productIconSpriteRenderer.sprite = productSprite;
            }
        }

	    private void OnHarvestStarted()
        {
            var productId = _currentModel.CurrentGrowingPlant.OutcomingProducts;
            var productAmount = _currentModel.CurrentGrowingPlant.Amount;

            var xpAmount = jProductsData.GetProduct(productId).ExperienceUp * productAmount;
            var texture = jResourcesManager.GetProductSprite(productId);
            
            jAddRewardItemSignal.Dispatch(new AddItemSettings(FlyDestinationType.Warehouse, transform.position, productAmount.ToString(), texture));
            jAddRewardItemSignal.Dispatch(new AddItemSettings(FlyDestinationType.Experience, transform.position, xpAmount.ToString()));

            ClearField();
            
            _isCanShowCroppedField = false;
            _currentState = AgriculturalFieldState.Empty;
        }
    }
}