﻿using NanoReality.Engine.BuildingSystem.GlobalCityMap;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using UnityEngine;

namespace Assets.Scripts.Engine.BuildingSystem.MapObjects
{
    public class UnknownBuildingView : AGlobalCityBuildingView
    {
        public override void InitializeView(IMapObject model)
        {
            IsGlobal = model.SectorId.ToBusinessSectorType() == BusinessSectorsTypes.Global;

            if (!registeredWithContext)
            {
                Awake();
            }

            base.InitializeView(model);

            if (!IsGlobal)
            {
                GridDimensions = Vector2Int.RoundToInt(model.Dimensions.ToVector2());
            }

            if (!HasGraphics)
            {
                var meshCollider = MeshCollider;
                if (meshCollider != null)
                {
                    var objectColliderBounds = meshCollider.bounds;
                    objectColliderBounds.size = (Vector2)GridDimensions;
                }
            }
        }

        protected override void OnBuildingTap()
        {
            var message =
                $"Unknown building (id: {MapBuildingModel.ObjectTypeID.Value} type: {MapBuildingModel.MapObjectType})";
            jWorldSpaceCanvas.ShowHint(WorldPosition, message);
        }
    }
}