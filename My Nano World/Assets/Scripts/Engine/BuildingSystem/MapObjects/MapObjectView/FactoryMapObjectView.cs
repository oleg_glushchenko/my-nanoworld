﻿using NanoReality.Engine.UI.Views.Panels;

namespace NanoReality.Engine.BuildingSystem.MapObjects
{
    public class FactoryMapObjectView : ProductionMapObjectView
    {
        protected override void OnBuildingTap()
        {
            base.OnBuildingTap();
            jShowWindowSignal.Dispatch(typeof(FactoryPanelView), MapBuildingModel);
        }
    }
}