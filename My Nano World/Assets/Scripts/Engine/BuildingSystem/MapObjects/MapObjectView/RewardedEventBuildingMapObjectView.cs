using Assets.Scripts.Engine.UI.Views.Popups;
using NanoReality.Engine.BuildingSystem.MapObjects.Components;
using System.Collections.Generic;
using Engine.UI.Extensions.TrophyInfoPanel;

namespace Assets.Scripts.Engine.BuildingSystem.MapObjects
{
    public class RewardedEventBuildingMapObjectView : AMapBuildingView
    {
        [Inject] public IPopupManager jPopupManager { get; set; }

        private bool _disableTap;
        private CitizenGiftBuildingComponent _giftComponent;
        private IEventBuildingReward TargetModel => MapObjectModel as IEventBuildingReward;

        protected override List<string> GetComponentsNames()
        {
            var result = base.GetComponentsNames();
            result.Add(nameof(CitizenGiftBuildingComponent));
            return result;
        }
     
        public override void DisposeView()
        {
            base.DisposeView();
            _giftComponent.OnGiftClaimed -= OnClaimGift;
            jSignalOnConstructControllerChangeState.RemoveListener(OnStateChanged);
        }

        protected override void OnComponentsInitialize()
        {
            base.OnComponentsInitialize();
            _giftComponent = GetViewComponent<CitizenGiftBuildingComponent>();
            _giftComponent.OnGiftClaimed += OnClaimGift;
            jSignalOnConstructControllerChangeState.AddListener(OnStateChanged);
        }

        private void OnStateChanged(bool state)
        {
            _disableTap = state;
        }

        public void OnClaimGift()
        {
            TargetModel.VerifyBuilding();
        }

        public override void OnMapObjectViewTap()
        {
            base.OnMapObjectViewTap();
            if (_disableTap) return;
            jShowWindowSignal.Dispatch(typeof(TrophyInfoPanel), MapBuildingModel);
        }
    }
}