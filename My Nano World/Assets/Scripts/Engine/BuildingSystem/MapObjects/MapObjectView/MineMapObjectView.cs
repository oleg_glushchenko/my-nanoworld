﻿using NanoReality.GameLogic.MinePanel;

namespace NanoReality.Engine.BuildingSystem.MapObjects
{
    public sealed class MineMapObjectView : ProductionMapObjectView
    {
        protected override void OnBuildingTap()
        {
            base.OnBuildingTap();
            jShowWindowSignal.Dispatch(typeof(MinePanelView), MapBuildingModel);
        }
    }
}