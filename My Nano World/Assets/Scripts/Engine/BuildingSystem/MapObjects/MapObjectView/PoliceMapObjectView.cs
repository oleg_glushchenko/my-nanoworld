using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;

namespace NanoReality.Engine.BuildingSystem.MapObjects
{
    public class PoliceMapObjectView : ProduceHappinessMapObjectView
    {
        public override MapObjectintTypes MapObjectIntType => MapObjectintTypes.Police;
    }
}