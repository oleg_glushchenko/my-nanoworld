﻿using Assets.NanoLib.UI.Core.Signals;
using Assets.Scripts.Engine.UI.Views.Trade;
using Assets.Scripts.Engine.UI.Views.Trade.Items;
using NanoReality.Game.UI.BazaarPanel;
using NanoReality.GameLogic.BuildingSystem.CityMap.Controllers;

namespace NanoReality.Engine.BuildingSystem.GlobalCityMap
{
    public class TradingShopBuildingView : AGlobalCityBuildingView
    {
        #region Injects
        
        [Inject]
        public RoadConstructSetActiveSignal jRoadConstructSetActiveSignal { get; set; }

        [Inject]
        public PlayerOpenedPanelSignal JSignalPlayerOpenedPanel { get; set; }

        #endregion

        #region Overrides of AGlobalCityBuildingView

        protected override void OnBuildingTap()
        {
            jShowWindowSignal.Dispatch(typeof(SoukPanelView), null);
            
            jRoadConstructSetActiveSignal.Dispatch(false);
            JSignalPlayerOpenedPanel.Dispatch();
        }

        #endregion
    }
}
