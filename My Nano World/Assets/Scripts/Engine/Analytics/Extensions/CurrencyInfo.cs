﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Engine.Analytics.Model;
using Assets.Scripts.Engine.Analytics.Model.api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.Quests;

namespace Assets.Scripts.Engine.Analytics.Extensions.CurrencyInfo
{
    public abstract class CurrencyInfo
    {
        public string Title { get; set; }

        protected CurrencyInfo() { }

        protected CurrencyInfo(string title)
        {
            Title = title;
        }

        public abstract void SendEvent(INanoAnalytics nanoAnalytics, int userLevel, AnalyticsType analyticsType);

        public static CurrencyInfo GetInfoFromAwardType(IAwardAction awardAction, CurrencyIncomesActions currencyIncomes)
        {
            return GetInfoFromAwardType(awardAction, currencyIncomes, currencyIncomes.ToString());
        }

        public static CurrencyInfo GetInfoFromAwardType(IAwardAction awardAction, CurrencyIncomesActions currencyIncomes, string title)
        {
            if (awardAction.AwardType == AwardActionTypes.SoftCurrency)
                return new SoftInfo(CurrencyIncomesActions.Quest,
                    new Price() { SoftPrice = awardAction.AwardCount }, title);
            if (awardAction.AwardType == AwardActionTypes.PremiumCurrency)
                return new HardInfo(CurrencyIncomesActions.Quest,
                    new Price() { HardPrice = awardAction.AwardCount }, title);
            if (awardAction.AwardType == AwardActionTypes.NanoGems)
                return new GemsInfo(CurrencyIncomesActions.Quest,
                    awardAction.AwardCount, title);

            return new DummyInfo();
        }
    }

    #region Currencies

    public enum CurrencyIncomesActions
    {
        None, Auction, Quest, DailyBonus, Store, Harvester, HarvesterSeek, Entertainment, Achievements, LvlUp
    }

    public enum CurrencyConsumeActions
    {
        None, Auction, Build, UpgradeBuilding, SkipBuilding, UpgradeWarehouse, SpeedUp, PremiumStore, Store, SlotUnlock, BuyProduct, Education, SkipEducation
    }

    public class SoftInfo : CurrencyInfo
    {
        public Price Price;
        public CurrencyIncomesActions SoftIncomesActions { get; set; }
        public CurrencyConsumeActions SoftConsumeActions { get; set; }

        public SoftInfo(CurrencyIncomesActions softIncomesActions, Price price, string title)
            : base(title)
        {
            SoftIncomesActions = softIncomesActions;
            Price = price;
        }

        public SoftInfo(CurrencyConsumeActions softConsumeActions, Price price, string title)
            : base(title)
        {
            SoftConsumeActions = softConsumeActions;
            Price = price;
        }

        public SoftInfo(CurrencyIncomesActions actionsWithCurrency, Price price)
            : this(actionsWithCurrency, price, actionsWithCurrency.ToString())
        {
        }

        public SoftInfo(CurrencyConsumeActions actionsWithCurrency, Price price)
            : this(actionsWithCurrency, price, actionsWithCurrency.ToString())
        {
        }

        public override void SendEvent(INanoAnalytics nanoAnalytics, int userLevel, AnalyticsType analyticsType)
        {
            string eventName = SoftIncomesActions != CurrencyIncomesActions.None ? "softIncomes" : "softConsumes";
            CustomEventData eventData = new CustomEventData()
            {
                EventName = eventName,
                Params = new Dictionary<string, object>
                {
                    {"currencyAmount", Price.SoftPrice},
                    {"userLevel", userLevel},
                    {"actionType", SoftIncomesActions != CurrencyIncomesActions.None ? SoftIncomesActions.ToString() : SoftConsumeActions.ToString()},
                    {"actionTitle", Title}
                }
            };

            //if price - hard + soft
            if (Price.HardPrice > 0 && SoftIncomesActions != CurrencyIncomesActions.None)
                new HardInfo(SoftIncomesActions, new Price() { HardPrice = Price.HardPrice}, Title).SendEvent(nanoAnalytics, userLevel, analyticsType);
            else if (Price.HardPrice > 0)
                new HardInfo(SoftConsumeActions, new Price() { HardPrice = Price.HardPrice }, Title).SendEvent(nanoAnalytics, userLevel, analyticsType);

            nanoAnalytics.SendEvent(eventData, analyticsType);
        }
    }

    public class HardInfo : CurrencyInfo
    {
        public Price Price;
        public CurrencyIncomesActions HardIncomesActions { get; set; }
        public CurrencyConsumeActions HardConsumesActions { get; set; }

        public HardInfo(CurrencyIncomesActions hardIncomesActions, Price price, string title)
            : base(title)
        {
            HardIncomesActions = hardIncomesActions;
            Price = price;
        }

        public HardInfo(CurrencyConsumeActions hardConsumesActions, Price price, string title)
            : base(title)
        {
            HardConsumesActions = hardConsumesActions;
            Price = price;
        }

        public HardInfo(CurrencyIncomesActions actionsWithCurrency, Price price)
            : this(actionsWithCurrency, price, actionsWithCurrency.ToString())
        {
        }

        public HardInfo(CurrencyConsumeActions actionsWithCurrency, Price price)
            : this(actionsWithCurrency, price, actionsWithCurrency.ToString())
        {
        }

        public override void SendEvent(INanoAnalytics nanoAnalytics, int userLevel, AnalyticsType analyticsType)
        {
            string eventName = HardIncomesActions != CurrencyIncomesActions.None ? "hardIncomes" : "hardConsumes";
            CustomEventData eventData = new CustomEventData()
            {
                EventName = eventName,
                Params = new Dictionary<string, object>
                {
                    {"currencyAmount", Price.HardPrice},
                    {"userLevel", userLevel},
                    {"actionType", HardIncomesActions != CurrencyIncomesActions.None ? HardIncomesActions.ToString() : HardConsumesActions.ToString()},
                    {"actionTitle", Title}
                }
            };

            //if price - hard + soft
            if (Price.HardPrice > 0 && HardIncomesActions != CurrencyIncomesActions.None)
                new SoftInfo(HardIncomesActions, new Price() { SoftPrice = Price.SoftPrice }, Title).SendEvent(nanoAnalytics, userLevel, analyticsType);
            else if (Price.HardPrice > 0)
                new SoftInfo(HardConsumesActions, new Price() { SoftPrice = Price.SoftPrice }, Title).SendEvent(nanoAnalytics, userLevel, analyticsType);

            nanoAnalytics.SendEvent(eventData, analyticsType);
        }
    }

    public class GemsInfo : CurrencyInfo
    {
        public int Amount;
        public CurrencyIncomesActions GemsIncomesActions { get; set; }

        public GemsInfo(CurrencyIncomesActions gemsIncomesActions, int amount, string title)
            : base(title)
        {
            GemsIncomesActions = gemsIncomesActions;
            Amount = amount;
        }

        public GemsInfo(CurrencyIncomesActions actionsWithCurrency, int amount)
            : this(actionsWithCurrency, amount, actionsWithCurrency.ToString())
        {
        }

        public override void SendEvent(INanoAnalytics nanoAnalytics, int userLevel, AnalyticsType analyticsType)
        {
            CustomEventData eventData = new CustomEventData()
            {
                EventName = "gemsIncomes",
                Params = new Dictionary<string, object>
                {
                    {"currencyAmount", Amount},
                    {"userLevel", userLevel},
                    {"actionType", GemsIncomesActions.ToString()},
                    {"actionTitle", Title}
                }
            };

            nanoAnalytics.SendEvent(eventData, analyticsType);
        }
    }

    public class DummyInfo : CurrencyInfo
    {
        public override void SendEvent(INanoAnalytics nanoAnalytics, int userLevel, AnalyticsType analyticsType)
        {
            
        }
    }

    #endregion

}
