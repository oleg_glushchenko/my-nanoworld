﻿namespace Assets.Scripts.Engine.Analytics.Model
{
    public class CrystalInfo
    {
        /// <summary>
        /// ID здания откуда кристалл
        /// </summary>
        public string BuildingId { get; set; }

        /// <summary>
        /// тип кристала (зеленый, красный, синий)
        /// </summary>
        public string CrystalType { get; set; }

        public CrystalInfo(string buildingId, string crystalType)
        {
            BuildingId = buildingId;
            CrystalType = crystalType;
        }
    }
}