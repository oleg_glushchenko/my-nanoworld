﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Engine.Analytics.Model.api;
using UnityEngine;
using UnityEngine.Analytics;


namespace Assets.Scripts.Engine.Analytics.Model.impl
{

	/*Temprory
    /// <summary>
    /// Sends events to GameAnalytics
    /// </summary>
    public class GameAnalyticsWrapper : IAnalytics
    {
		public void InitAnalitics ()
		{
		}

        /// <summary>
        /// Monetization event
        /// </summary>
        /// <param name="eventData">Represents monetization event data</param>
        public void SendEvent(IApBillingEventData eventData)
        {
            GameAnalytics.NewBusinessEvent(eventData.Currency, eventData.Amount, eventData.Receipt, eventData.ItemId.Value.ToString(), null);
        }

        /// <summary>
        /// User profile event
        /// </summary>
        /// <param name="eventData">Represents user profile event data (name, surname, year)</param>
        public void SendEvent(UserProfileEventData eventData)
        {
            if (eventData.Gender != Gender.Unknown)
                GameAnalytics.SetGender(
                    (GA_Setup.GAGender) Enum.Parse(typeof (GA_Setup.GAGender), eventData.Gender.ToString(), true));

            GameAnalytics.SetBirthYear(eventData.BirthYear);
        }

        /// <summary>
        /// Custom event
        /// </summary>
        /// <param name="eventData">Represents EventData params</param>
        public void SendEvent(CustomEventData eventData)
        {
            foreach (KeyValuePair<string, object> pair in eventData.Params)
            {
                float? value = pair.Value as float?;

                if (value == null)
                    GameAnalytics.NewDesignEvent(eventData.EventName + ':' + pair.Key + ':' + pair.Value);
                else
                    GameAnalytics.NewDesignEvent(eventData.EventName + ':' + pair.Key, value.Value);
            }
        }
    }
    */
}