﻿using System.Collections.Generic;

namespace Assets.Scripts.Engine.Analytics.Model
{
    public class InviteInfo
    {
        /// <summary>
        /// Тип (e.g., FB)
        /// </summary>
        public string InviteType { get; set; }

        /// <summary>
        /// Список друзей (кому посылаем запрос)
        /// </summary>
        public List<string> RecipientIds { get; set; }

        /// <summary>
        /// Уникальный таг (nanoWorld)
        /// </summary>
        public string UniqueTracking { get; set; }

        public InviteInfo(string inviteType, List<string> recipientIds, string uniqueTracking)
        {
            InviteType = inviteType;
            RecipientIds = recipientIds;
            UniqueTracking = uniqueTracking;
        }
    }
}