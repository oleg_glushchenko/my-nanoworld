﻿

using Assets.NanoLib.Utilities;
using UnityEngine;

public static  class VectorExtensions 
{
    public static Vector2F ToVector2F(this Vector2 vector2)
    {
        return new Vector2F(vector2.x,vector2.y);
    }

    public static Vector2F ToVector2F(this Vector3 vector2)
    {
        return new Vector2F(vector2.x,vector2.y);
    }

    public static Vector3F ToVector3F(this Vector3 vector3)
    {
        return new Vector3F(vector3.x,vector3.y,vector3.z);
    }

    public static Vector2 ToVector2(this Vector2F v)
    {
        return new Vector2(v.X, v.Y);
    }
    
    public static Vector2Int ToVector2Int(this Vector2F v)
    {
        return new Vector2Int(v.intX, v.intY);
    }

    public static Vector3 ToVector3(this Vector3F v)
    {
        return new Vector3(v.X, v.Y, v.Z);
    }

}
