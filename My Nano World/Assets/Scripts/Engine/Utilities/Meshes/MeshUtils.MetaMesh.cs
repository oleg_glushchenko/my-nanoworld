﻿using System.Collections.Generic;
using UnityEngine;


namespace Assets.Scripts.Engine.Utilities.Meshes
{
    public static partial class MeshUtils
    {
        public class MetaMesh
        {
            public List<Vector3> Vertices { get; private set; }
            public List<int> Triangles { get; private set; }
            public List<Color> Colors { get; private set; }

            public MetaMesh ()
            {
                Vertices = new List<Vector3>();
                Triangles = new List<int>();
                Colors = new List<Color>();
            }

            #region api

            public void Add (MetaMesh metaMesh)
            {
                var numVertices = Vertices.Count;
                Vertices.AddRange (metaMesh.Vertices);
                Colors.AddRange (metaMesh.Colors);
                for (var i = 0; i < metaMesh.Triangles.Count; i++) Triangles.Add(metaMesh.Triangles[i] + numVertices);
            }

            /// <summary>
            /// Add vertice to MetaMesh
            /// </summary>
            /// <param name="vertice"></param>
            /// <param name="color"></param>
            /// <returns>Index of added vertice</returns>
            public int Add(Vector3 vertice, Color color)
            {
                Vertices.Add(vertice);
                Colors.Add(color);
                return Vertices.Count - 1;
            }

            public VerticeIndexes Add(ICollection<Vector3> vertices, Color color)
            {
                Vertices.AddRange(vertices);
                AddColors (Colors, color, vertices.Count);
                return new VerticeIndexes(Vertices.Count - vertices.Count, Vertices.Count - 1);
            }

            /// <summary>
            /// Add triangle for already existing vertices
            /// </summary>
            /// <param name="index0">First vertice index (returned when call Add(Vector3 vertice, Color color))</param>
            /// <param name="index1">Second vertice index</param>
            /// <param name="index2">Third vertice index</param>
            public void AddTraingleFor(int index0, int index1, int index2)
            {
                Triangles.Add(index0);
                Triangles.Add(index1);
                Triangles.Add(index2);
            }
            
            /// <summary>
            /// Triangulate and add triangles
            /// </summary>
            /// <param name="centerVerticeIndex">central vertice, example : circle center</param>
            /// <param name="contour">external contour vertices</param>
            /// <param name="completed">is contour completed (f.e: circle) or not (f.e.: corner, sector, half circle) </param>
            public void AddTrinaglesFor(int centerVerticeIndex, VerticeIndexes contour, bool completed = true)
            {
                for (var i = contour.StartIndex; i <= (completed? contour.EndIndex : contour.EndIndex - 1); i++)
                {
                    Triangles.Add(centerVerticeIndex);
                    Triangles.Add(i);
                    Triangles.Add(i < contour.EndIndex ? i + 1 : contour.StartIndex);
                }
            }

            /// <summary>
            /// Add 'border' triangles
            /// </summary>
            /// <param name="innerContour"></param>
            /// <param name="outerContour"></param>
            /// <remarks>Note: innerContour and innerContour must have the same number of vertices</remarks>
            public void AddTrinaglesFor(VerticeIndexes innerContour, VerticeIndexes outerContour)
            {
                for (var innerIndex = innerContour.StartIndex; innerIndex <= innerContour.EndIndex; innerIndex++)
                {
                    var outerIndex = outerContour.StartIndex + innerIndex - innerContour.StartIndex;
                    Triangles.Add(innerIndex);
                    Triangles.Add(outerIndex);
                    Triangles.Add(innerIndex < innerContour.EndIndex ? innerIndex + 1 : innerContour.StartIndex);
                    Triangles.Add(innerIndex < innerContour.EndIndex ? innerIndex + 1 : innerContour.StartIndex);
                    Triangles.Add(outerIndex);
                    Triangles.Add(outerIndex < outerContour.EndIndex ? outerIndex + 1 : outerContour.StartIndex);
                }
            }

            #endregion
        }
    }
}