﻿using UnityEngine;

/// <summary>
/// Скрипт ячейки для редактора
/// </summary>
public class PlaneEditorCell : MonoBehaviour
{
    /// <summary>
    /// Материал свободной ячейки
    /// </summary>
    public Material EmptyCellMaterial;

    /// <summary>
    /// Материал занятой ячейки
    /// </summary>
    public Material FilledCellMaterial;

    /// <summary>
    /// x координата ячейки
    /// </summary>
    private int _xCoordinate;

    /// <summary>
    /// y координата ячейки
    /// </summary>
    private int _yCoordinate;

    /// <summary>
    /// Заполнена ли ячейка?
    /// </summary>
    private bool _filled;

    /// <summary>
    /// Закешированный рендерер, что бы каждый раз не добираться к нему
    /// через GetComponent
    /// </summary>
    private Renderer _cachedRenderer;

    /// <summary>
    /// Заполнена ли ячейка? + Сеттер с настройкой материала к ней
    /// </summary>
    public bool Filled
    {
        get { return _filled; }
        set
        {
            _filled = value;
            if (_cachedRenderer == null) _cachedRenderer = GetComponent<Renderer>();

            _cachedRenderer.material = _filled ? FilledCellMaterial : EmptyCellMaterial;
        }
    }

    /// <summary>
    /// Инициализация ячейки, обязательна при её создании, иначе Json
    /// правильно не возвратится
    /// </summary>
    /// <param name="xCoord">x координата ячейки</param>
    /// <param name="yCoord">y координата ячейки</param>
    public void Init(int xCoord, int yCoord)
    {
        _xCoordinate = xCoord;
        _yCoordinate = yCoord;
    }

    /// <summary>
    /// Возвращает координаты ячейки в Json формате
    /// </summary>
    /// <returns></returns>
    public string GetCoordinatesInJsonFormat()
    {
        return "{\"x\":" + _xCoordinate + ",\"y\":" + _yCoordinate + "}";
    }

    /// <summary>
    /// Инвертирует статус заполненности ячейки
    /// </summary>
    public void ToogleFilled()
    {
        Filled = !Filled;
    }
}