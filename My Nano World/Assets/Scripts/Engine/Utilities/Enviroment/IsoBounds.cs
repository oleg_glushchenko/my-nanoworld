﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;

[ExecuteInEditMode]
[RequireComponent(typeof(MapObjectView))]
public class IsoBounds : MonoBehaviour
{

    void OnDrawGizmosSelected()
    {
        var view = GetComponent<MapObjectView>();
        Gizmos.color = new Color(1,0.6f,0);
        var size = new Vector3(view.GridDimensions.x, 0, view.GridDimensions.y);
        Gizmos.DrawWireCube(transform.position+ (size*0.5f), size);
    }
}
