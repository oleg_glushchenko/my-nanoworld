﻿using System;
using System.Collections.Generic;
using System.Linq;
using NanoLib.AssetBundles;
using NanoReality.GameLogic.Constants;
using NanoLib.Core.Logging;
using NanoReality.Game.Data;
using Sirenix.OdinInspector;
using UnityEngine;

namespace NanoReality.Engine.Settings.BuildSettings
{
    [CreateAssetMenu(fileName = "BuildSettings", menuName = "NanoReality/Settings/BuildSettings", order = 1)]
    [ShowOdinSerializedPropertiesInInspector]
    public class BuildSettings : SerializedScriptableObject
    {
        [SerializeField] private int _bundleVersion;
        
        [Header("Asset Bundles")] 
        [SerializeField] private ResourcesPolicyType _resourcesPolicyType;

        #region Debug

        [Header("Debugging")]
        [SerializeField] private bool _debugAds;

        public string CustomUserId;

        #endregion

        #region GameConfig

        /// <summary>
        /// Продолжительность долгого тача для включения режима строительства
        /// </summary>
        [Header("Game Config")]
        [SerializeField]
        [Tooltip("Timeout in seconds after which the game is reloaded")] 
        private float _applicationPauseTimeout = 60f;

        [SerializeField]
        [Tooltip("Local Data validity time in hours, if there was no writes to local data file more than given time, then it gets cleaned")]
        private int _localDataValidityHours = 24;

        [Tooltip("Time out in milliseconds")]
        public int LongTouchDuration;

          [Tooltip("Delay before appears long touch progress bar in milliseconds")] 
        public int LongTouchProgressDelay;

        public bool SkipStoryline;
        
        public bool DisableHardTutorial;
        
        public bool DisableSoftTutorials;

        public bool ShowLookerTouchZone;

        public bool EnablePayments;

        /// <summary>
        /// Проверить версию игры, если устарела - обновить
        /// </summary>
        public bool CheckBuildVersion;

        [SerializeField]
        private int _screenSleepTimeout = 600;

        [SerializeField]
        private int _videoAdsRefreshInterval = 20;

        #endregion
        
        #region Localization

        [Header("Localization")]
        [SerializeField]
        private List<LanguageType> _availableLanguages;

        [SerializeField]
        private bool _forceEnglishLanguage;

        [SerializeField]
        private LanguageType _firstStartLanguage;
        
        #endregion

        #region Notifications

        /// <summary>
        /// Частота обновления нотификаций
        /// </summary>
        [Header("Notifications")]
        public float UpdateNotificationsFrequency;

        #endregion

        [Header("Unity Ads")]
        [SerializeField] private bool _useTestAds;
        [SerializeField] private string _unityAdsPlacementId;
        [SerializeField] private string _unityAdsAndroidId;
        [SerializeField] private string _unityAdsIOSId;
        
        [Header("Google Ads")]
        [SerializeField] private string _googleRewardedAdsAndroidId;
        [SerializeField] private string _googleRewardedAdsIOSId;

        [Header("Test")]
        [SerializeField] private string[] _iOSTestDeviceIds;
        [SerializeField] private string[] _androidTestDeviceIds;

        [Header("Analytics")]
        public AnalyticsType UseAnalytics;
        public bool EnableLiveAnalytics;
        public bool EnableAnalyticsDebug;
[SerializeField] private string _appsFlyerDevKey;
        [SerializeField] private bool _isAppsFlyerDebug;
        public List<LanguageType> AvailableLanguages
        {
            get => _availableLanguages;
            set => _availableLanguages = value;
        }
        
        public ResourcesPolicyType ResourcesPolicyType
        {
            get => _resourcesPolicyType;
            set
            {
                _resourcesPolicyType = value;
                _resourcesPolicyType.Log();
            }
        }

        public IBuildSettings ConvertToBuildSettings()
        {
            return new NanoReality.Game.Data.BuildSettings
            {
                BundleVersion = _bundleVersion,
                DebugAds = _debugAds,
                EnableLiveAnalytics = EnableLiveAnalytics,
                CustomUdid = CustomUserId,
                UpdateNotificationsFrequency = UpdateNotificationsFrequency,
                ApplicationPauseTimeout = _applicationPauseTimeout,
                LongTouchDuration = LongTouchDuration,
                AnalyticsType = UseAnalytics,
                EnableAnalyticsDebug = EnableAnalyticsDebug,
                SkipStoryline = SkipStoryline,
                DisableHardTutorial = DisableHardTutorial,
                DisableSoftTutorials = DisableSoftTutorials,
                ShowLookerTouchZone = ShowLookerTouchZone,
                CheckBuildVersion = CheckBuildVersion,
                ScreenSleepTimeout = _screenSleepTimeout,
                VideoAdsRefreshInterval = _videoAdsRefreshInterval,
                LocalDataValidityHours = _localDataValidityHours,
                ResourcesPolicy = _resourcesPolicyType,
                UnityAdsAndroidId = _unityAdsAndroidId,
                UnityAdsPlacementId = _unityAdsPlacementId,
                UnityAdsIOSId = _unityAdsIOSId,
                UseTestAds = _useTestAds,
                GoogleRewardedAdsAndroidId = _googleRewardedAdsAndroidId,
                GoogleRewardedAdsIOSId = _googleRewardedAdsIOSId,
                AndroidTestDeviceIds = _androidTestDeviceIds,
                IOSTestDeviceIds = _iOSTestDeviceIds,
                LongTouchProgressDelay = LongTouchProgressDelay,
                EnablePayments = EnablePayments,
                ForceEnglishLanguage = _forceEnglishLanguage,
                FirstStartLanguage = ConvertLanguageTypeToCode(_firstStartLanguage),
                AvailableLanguages = _availableLanguages.Select(ConvertLanguageTypeToCode).Where(language => !string.IsNullOrEmpty(language)).ToList()
            };
        }

        public void SetBundleVersion(int version)
        {
            _bundleVersion = version;
        }

        private static string ConvertLanguageTypeToCode(LanguageType type)
        {
            switch (type)
            {
                case LanguageType.English:
                    return "en";
                case LanguageType.Arabic:
                    return "ar";
                case LanguageType.German:
                    return "de";
                case LanguageType.Spanish:
                    return "es";
                case LanguageType.French:
                    return "fr";
                case LanguageType.Russian:
                    return "ru";
            }

            return string.Empty;
        }

        [Serializable]
        public class DeviceUnqueIdSettings
        {
            public string Name;
            public string Udid;
        }
    }
}
