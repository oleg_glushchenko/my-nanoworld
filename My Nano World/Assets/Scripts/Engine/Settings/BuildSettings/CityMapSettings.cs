using System.Collections.Generic;
using NanoReality.GameLogic.Settings.BuildSettings.Models.Api;
using UnityEngine;

namespace NanoReality.Engine.Settings.BuildSettings
{
    [CreateAssetMenu(fileName = "CityMapSettings", menuName = "NanoReality/Settings/CityMapSettings", order = 1)]
    public class CityMapSettings : ScriptableObject, ICityMapSettings
    {
        [SerializeField] private float _gridCellScale;
        
        [SerializeField] private float _buildingViewScale;
        
        [Tooltip("In grid cells")]
        [SerializeField] private float _buildingViewPositionOffset;
        
        [SerializeField] private float _roadViewScale;

        [SerializeField] private float _cellSize;
        
        [SerializeField] private List<GameObject> _lockedSectorsPrefabs = new List<GameObject>();

        public float GridCellScale => _gridCellScale;

        public float RoadViewScale => _roadViewScale;

        public float BuildingViewPositionOffset => _buildingViewPositionOffset;

        public float BuildingViewScale => _buildingViewScale;

        public float CellSize => _cellSize;
        
        public List<GameObject> LockedSectorsPrefabs => _lockedSectorsPrefabs;
    }
}