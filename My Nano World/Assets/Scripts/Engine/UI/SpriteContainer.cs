using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Engine.UI
{
    public interface ISpriteContainer
    {
        string GetPath<T>();
        T GetContainer<T>() where T : class;
    }

    [CreateAssetMenu(fileName = "SpriteContainer", menuName = "NanoReality/UI/SpriteContainer", order = 1)]
    public class SpriteContainer : ScriptableObject, ISpriteContainer
    {
        public List<SpriteDataPath> DataPaths = new List<SpriteDataPath>();

        public T GetContainer<T>() where T : class
        {
            return DataPaths.Find(x => x.SpriteData is T).SpriteData as T;
        }

        public string GetPath<T>()
        {
            return DataPaths.Find(x => x.SpriteData is T).SpritePath;
        }
        
        #if UNITY_EDITOR
        public void OnValidate()
        {
            foreach (var dataPath in DataPaths)
            {
                if(dataPath == null) continue;
                dataPath.SpriteName = dataPath.SpriteData.name;
                dataPath.SpritePath = AssetDatabase.GetAssetPath(dataPath.SpriteData);
            }
        }
        
        #endif
    }

    [Serializable]
    public class SpriteDataPath
    {
        public string SpritePath;
        public UnityEngine.Object SpriteData;
        public string SpriteName;
    }
}