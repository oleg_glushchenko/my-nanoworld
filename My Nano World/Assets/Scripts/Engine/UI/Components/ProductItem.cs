﻿using System;
using Assets.NanoLib.Utilities.Pulls;
using UnityEngine;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using strange.extensions.signal.impl;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace NanoReality.Engine.UI.Components
{
    /// <summary>
    /// Объект товара
    /// </summary>
    public class ProductItem : GameObjectPullable, IPointerDownHandler, IPointerUpHandler
    {
        #region InspectorFields
        
        /// <summary>
        /// Иконка товара
        /// </summary>
        [SerializeField]
        private Image _productIcon;

        /// <summary>
        /// Используется для выделения итема по тапу
        /// </summary>
        [SerializeField]
        private GameObject _checkMark;

        /// <summary>
        /// Задний фон итема
        /// </summary>
        [SerializeField]
        private Image _productBackImage;

        /// <summary>
        /// Внутрений фон итема
        /// </summary>
        [SerializeField]
        private Image _productInnerBackImage;

        /// <summary>
        /// спрайт для заднего фона (продукт готов)
        /// </summary>
        [SerializeField]
        private Sprite _readyBackSprite;

        /// <summary>
        /// спрайт для заднего фона (продукт в производстве)
        /// </summary>
        [SerializeField]
        private Sprite _notReadyBackSprite;

        /// <summary>
        /// спрайт для внутреннего фона (продукт готов)
        /// </summary>
        [SerializeField]
        private Sprite _readyInnerBackSprite;

        /// <summary>
        /// спрайт для внутреннего фона (продукт готов)
        /// </summary>
        [SerializeField]
        private Sprite _notReadyInnerBackSprite;
        #endregion

        /// <summary>
        /// Текущий товар
        /// </summary>
        public Product Product { get; private set; }

        /// <summary>
        /// Вызывается по лонг тапу на айтем
        /// </summary>
        public Signal<ProductItem> OnLongPress = new Signal<ProductItem>();

        public Signal SignalOnPointerDown = new Signal();
        public Signal SignalOnPointerUp = new Signal();

        /// <summary>
        /// Long press component
        /// </summary>
        private LongPressEventTrigger _longPress;

        protected virtual void Awake()
        {
            _longPress = GetComponent<LongPressEventTrigger>();

            if (_longPress != null)
                _longPress.onLongPress.AddListener(OnLongPressCallback);
        }

        private void OnLongPressCallback()
        {
            OnLongPress.Dispatch(this);

            if (_checkMark!= null)
                _checkMark.SetActive(true);
        }

        /// <summary>
        /// Показывает товар
        /// </summary>
        /// <param name="product">Товар</param>
        /// <param name="icon">Иконка товара</param>
        /// <exception cref="ArgumentNullException"></exception>
        public void SetProduct(Product product, Sprite icon)
        {
            if(product == null)
                throw new ArgumentNullException("product");
            if(icon == null)
                throw new ArgumentNullException("icon");

            Product = product;
            _productIcon.sprite = icon;
        }

        public override void OnPush()
        {
            if (_checkMark != null)
                _checkMark.SetActive(false);

            _productIcon.sprite = null;
            OnLongPress.RemoveAllListeners();
            SignalOnPointerDown.RemoveAllListeners();
            SignalOnPointerUp.RemoveAllListeners();
            base.OnPush();
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            SignalOnPointerDown.Dispatch();
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            SignalOnPointerUp.Dispatch();

            if (_checkMark != null)
                _checkMark.SetActive(false);
        }

        /// <summary>
        /// Выставляем задний фон итема в зависимости от его состояния (производится товар или нет)
        /// </summary>
        public void SetProductBack(bool state)
        {
            if (state)
            {
                _productBackImage.sprite = _readyBackSprite;
                _productInnerBackImage.sprite = _readyInnerBackSprite;
            }
            else
            {
                _productBackImage.sprite = _notReadyBackSprite;
                _productInnerBackImage.sprite = _notReadyInnerBackSprite;
            }
        }
    }
}