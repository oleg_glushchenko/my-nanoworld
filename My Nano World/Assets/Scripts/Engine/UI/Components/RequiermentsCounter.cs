﻿using TMPro;
using UnityEngine;

namespace NanoReality.Engine.UI.Components
{
    /// <summary>
    /// Счетчик необходимых айтемов
    /// </summary>
    public class RequiermentsCounter : MonoBehaviour
    {
        /// <summary>
        /// Лейбла счетчика
        /// </summary>
        [SerializeField]
        private TextMeshProUGUI _counter;

        /// <summary>
        /// Плашка отображающая нехватку товара
        /// </summary>
        [SerializeField]
        private GameObject _enoughtCheckmark;

        /// <summary>
        /// Текущее кол-во айтемов
        /// </summary>
        public int Current
        {
            get { return _current; }
            set
            {
                if (_current != value)
                {
                    _current = value;
                    UpdateCounter();
                }
            }
        }

        private int _current;

        /// <summary>
        /// Необходимое кол-во айтемов
        /// </summary>
        public int Need
        {
            get
            {
                return _need;
            }
            set
            {
                if (_need != value)
                {
                    _need = value;
                    UpdateCounter();
                }
            }
        }

        private int _need;

        private void UpdateCounter()
        {
            _enoughtCheckmark.SetActive(_current>=_need);
            _counter.text = string.Format("{0}/{1}", _current, _need);
        }
    }
}