﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace NanoReality.Engine.UI.Components.Scalers
{
    [RequireComponent(typeof(HorizontalOrVerticalLayoutGroup))]
    public class HorizontalOrVerticalLayoutScaler : TargetRatioUiScaler
    {

        public RectOffset Padding;
        public float Spacing;

        private HorizontalOrVerticalLayoutGroup _targetGroup;

        protected override void Awake()
        {
            _targetGroup = GetComponent<HorizontalOrVerticalLayoutGroup>();
            base.Awake();

        }

        protected override void ApplyState()
        {
            base.ApplyState();
            _targetGroup.padding = Padding;
            _targetGroup.spacing = Spacing;
        }
    }
}
