﻿using UnityEngine;
using System.Collections;

namespace NanoReality.Engine.UI.Components.Scalers
{
    public class TransformScalerUi : TargetRatioUiScaler
    {
        protected RectTransform TargetTransform;

        protected override void Awake()
        {
            TargetTransform = GetComponent<RectTransform>();
            base.Awake();
           
        }

        public Vector3 TargetScale;
        public Vector3 TargetLocalPositon;
        public bool IsUsePosition = false;

        protected override void ApplyState()
        {
            base.ApplyState();
            TargetTransform.localScale = TargetScale;
            if (IsUsePosition)
                TargetTransform.anchoredPosition = TargetLocalPositon;

        }
    }
}
