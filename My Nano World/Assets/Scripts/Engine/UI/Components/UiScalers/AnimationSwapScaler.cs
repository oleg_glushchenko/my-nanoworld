﻿using UnityEngine;
using System.Collections;

namespace NanoReality.Engine.UI.Components.Scalers
{
    public class AnimationSwapScaler : TargetRatioUiScaler
    {
        private Animation _targetAnimation;

        public AnimationClip Clip;

        protected override void Awake()
        {
            _targetAnimation = GetComponent<Animation>();
            base.Awake();
        }

        protected override void ApplyState()
        {
            base.ApplyState();
            _targetAnimation.clip = Clip;
        }
    }
}
