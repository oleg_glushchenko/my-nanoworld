﻿using TMPro;
using UnityEngine;

namespace NanoReality.Engine.UI.Components
{
    /// <summary>
    /// Гейм. обжект с текстом
    /// </summary>
    public class TextGameObject : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _text;

        /// <summary>
        /// Текстовая лейбла
        /// </summary>
        public TextMeshProUGUI Text
        {
            get { return _text; }
        }
    }
}