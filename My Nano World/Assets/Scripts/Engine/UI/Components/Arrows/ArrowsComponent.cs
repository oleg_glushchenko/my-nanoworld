﻿using System.Collections.Generic;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using NanoReality.Engine.UI.Components;
using UnityEngine;

namespace Assets.Scripts.Engine.UI.Components.Arrows
{
    /// <summary>
    /// Компонент стрелок возле здания в момент строительства
    /// </summary>
    public class ArrowsComponent : MonoBehaviour
    {
        public static ArrowsComponent Instance { get; private set; }

        public List<ConstructMapObjectArrow> Arrows;

        private void Awake()
        {
            Instance = this;
        }

        public void SetArrows(MapObjectView target, float cameraAngleDereees)
        {
            gameObject.SetActive(true);

            transform.localRotation = Quaternion.Euler(0, 0, 0);
            transform.localPosition = new Vector3(0, 0, 0);

            foreach (var arrow in Arrows)
            {
                arrow.SetTarget(target);
                arrow.SetMapData(cameraAngleDereees);
                arrow.Show();
            }
        }

        public void RemoveArrows()
        {
            transform.parent = null;
            gameObject.SetActive(false);
            foreach (var arrow in Arrows)
            {
                arrow.Hide();
            }
        }
    }
}