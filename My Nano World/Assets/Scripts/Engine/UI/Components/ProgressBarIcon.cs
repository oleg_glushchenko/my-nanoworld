using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Engine.UI.Components
{
    public class ProgressBarIcon : MonoBehaviour
    {
        [SerializeField] private ProgressBar _targetProgressBar;
        [SerializeField] private Image _targetIconImage;
        [SerializeField] private List<ProgressIconSettings> _iconSettings = new List<ProgressIconSettings>();

        private void OnEnable()
        {
            _targetProgressBar.ProgressChanged += OnProgressChanged;
            UpdateIconUi();
        }

        private void OnDisable()
        {
            _targetProgressBar.ProgressChanged -= OnProgressChanged;
        }

        public void SetNewIconSettings(List<float> accumulativeValues)
        {
            for (var valueIndex = 0; valueIndex < accumulativeValues.Count; valueIndex++)
            {
                if (valueIndex >= _iconSettings.Count) break;

                var settings = _iconSettings[valueIndex];
                settings.FromValue = valueIndex > 0 ? accumulativeValues[valueIndex - 1] : default;
                settings.ToValue = Mathf.Clamp01(accumulativeValues[valueIndex]);
            }
        }

        private void OnProgressChanged(float _)
        {
            UpdateIconUi();
        }

        private void UpdateIconUi()
        {
            _targetIconImage.sprite = GetSettingsForValue(_targetProgressBar.Value);
        }

        private Sprite GetSettingsForValue(float progressValue)
        {
            for (int i = 0; i < _iconSettings.Count; i++)
            {
                ProgressIconSettings iconSettings = _iconSettings[i];
                bool isRequiredSettings = progressValue >= iconSettings.FromValue  && progressValue <= iconSettings.ToValue;
                
                if (isRequiredSettings)
                {
                    return iconSettings.IconSprite;
                }
            }
            
            Debug.LogWarning($"ProgressIcon for value {progressValue} not found.");
            return null;
        }
        
        [Serializable]
        private sealed class ProgressIconSettings
        {
            [SerializeField] private float _fromValue;
            [SerializeField] private float _toValue;
            [SerializeField] private Sprite _iconSprite;

            public Sprite IconSprite => _iconSprite;

            public float ToValue
            {
                get => _toValue;
                set => _toValue = value;
            }

            public float FromValue
            {
                get => _fromValue;
                set => _fromValue = value;
            }
        }
    }
}