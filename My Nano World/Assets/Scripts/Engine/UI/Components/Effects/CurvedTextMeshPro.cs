﻿using UnityEngine;
using TMPro;

[ExecuteInEditMode]
[RequireComponent(typeof(TextMeshProUGUI))]
public class CurvedTextMeshPro : MonoBehaviour
{
    [SerializeField]
    private AnimationCurve _vertexCurve = AnimationCurve.Linear(0f, 0f, 1f, 0f);
    [SerializeField]
    private Vector3 _vertexOffset = Vector3.zero;
    [SerializeField]
    private float _curveScale = 1f;
    
    private TMP_Text _textComponent;
    private bool _isValidated;
    private bool _skipOneFrame;

#if UNITY_EDITOR
    private void OnValidate()
    {
        _isValidated = false;

        if (_textComponent != null && _textComponent.mesh != null)
        {
            _textComponent.havePropertiesChanged = true;
            LateUpdate();
            _isValidated = false;
        }
    }
#endif

    private void Awake()
    {
        _textComponent = GetComponent<TMP_Text>();
    }

    private void OnEnable()
    {
        _skipOneFrame = true;
        _isValidated = false;

#if UNITY_EDITOR
        // extra validation in edit mode
        if (!UnityEditor.EditorApplication.isPlaying)
        {
            OnValidate();
        }
#endif
    }

    private void Start()
    {
        _isValidated = false;

#if UNITY_EDITOR
        // extra validation in edit mode
        if (!UnityEditor.EditorApplication.isPlaying)
        {
            OnValidate();
        }
#endif
    }

    private void LateUpdate()
    {
        if (!_textComponent.havePropertiesChanged && _isValidated)
        {
            return;
        }

        _textComponent.ForceMeshUpdate(); // Generate the mesh and populate the textInfo with data we can use and manipulate.

        // check if warping was started on enable method
        if (_skipOneFrame)
        {
            // if so, we need to wait till next frame for text component to update mesh
            _skipOneFrame = false;
            return;
        }

        _isValidated = true;

        var textInfo = _textComponent.textInfo;
        var characterCount = textInfo.characterCount;

        if (characterCount == 0)
        {
            return;
        }

        float boundsMinX = _textComponent.bounds.min.x;  //textInfo.meshInfo[0].mesh.bounds.min.x;
        float boundsMaxX = _textComponent.bounds.max.x;  //textInfo.meshInfo[0].mesh.bounds.max.x;

        int visibleCharactersCount = 0;

        for (int i = 0; i < textInfo.characterInfo.Length; i++)
        {
            var charInfo = textInfo.characterInfo[i];

            // Skip characters that are not visible and thus have no geometry to manipulate.
            if (!charInfo.isVisible)
            {
                continue;
            }

            visibleCharactersCount++;

            // Get the index of the first vertex used by this text element.
            var vertexIndex = charInfo.vertexIndex;

            // Get the index of the material used by the current character.
            var materialIndex = charInfo.materialReferenceIndex;

            // Get the vertices of the mesh used by this text element (character or sprite).
            var vertices = textInfo.meshInfo[materialIndex].vertices;

            // Compute the baseline mid point for each character
            Vector3 offsetToMidBaseline = new Vector2((vertices[vertexIndex + 0].x + vertices[vertexIndex + 2].x) / 2, charInfo.baseLine);

            // Apply offset to adjust our pivot point.
            vertices[vertexIndex + 0] += -offsetToMidBaseline;
            vertices[vertexIndex + 1] += -offsetToMidBaseline;
            vertices[vertexIndex + 2] += -offsetToMidBaseline;
            vertices[vertexIndex + 3] += -offsetToMidBaseline;

            // Compute the angle of rotation for each character based on the animation curve
            var x0 = (offsetToMidBaseline.x - boundsMinX) / (boundsMaxX - boundsMinX); // Character's position relative to the bounds of the mesh.
            var x1 = x0 + 0.0001f;
            var y0 = _vertexCurve.Evaluate(x0) * _curveScale;
            var y1 = _vertexCurve.Evaluate(x1) * _curveScale;

            var horizontal = new Vector3(1, 0, 0);
            var tangent = new Vector3(x1 * (boundsMaxX - boundsMinX) + boundsMinX, y1) - new Vector3(offsetToMidBaseline.x, y0);

            var dot = Mathf.Acos(Vector3.Dot(horizontal, tangent.normalized)) * 57.2957795f;
            var cross = Vector3.Cross(horizontal, tangent);
            var angle = cross.z > 0 ? dot : 360 - dot;

            var position = _vertexOffset;
            position.y += y0;

            var rotation = Quaternion.Euler(0, 0, angle);

            var matrix = Matrix4x4.TRS(position, rotation, Vector3.one);

            vertices[vertexIndex + 0] = matrix.MultiplyPoint3x4(vertices[vertexIndex + 0]);
            vertices[vertexIndex + 1] = matrix.MultiplyPoint3x4(vertices[vertexIndex + 1]);
            vertices[vertexIndex + 2] = matrix.MultiplyPoint3x4(vertices[vertexIndex + 2]);
            vertices[vertexIndex + 3] = matrix.MultiplyPoint3x4(vertices[vertexIndex + 3]);

            vertices[vertexIndex + 0] += offsetToMidBaseline;
            vertices[vertexIndex + 1] += offsetToMidBaseline;
            vertices[vertexIndex + 2] += offsetToMidBaseline;
            vertices[vertexIndex + 3] += offsetToMidBaseline;
        }

        if (visibleCharactersCount > 0)
        {
            // Upload the mesh with the revised information
            _textComponent.UpdateVertexData();
        }
    }
}
