using System.Collections.Generic;
using UnityEngine;

namespace Engine.UI.Components
{
    [CreateAssetMenu(fileName = "ProgressBarStates", menuName = "NanoReality/UI/ProgressBarStates", order = 1)]
    public class ProgressBarStates : ScriptableObject
    {
        [SerializeField]
        private List<SliderFill> _sliderFills;

        public Sprite GetSprite(float fillAmount)
        {
            var percentageSliderValue = (int) (fillAmount * 100);

            for (var i = 0; i < _sliderFills.Count; i++)
            {
                var constraint = _sliderFills[i];
                if (percentageSliderValue <= constraint.MaxValueToShowInPercents &&
                    percentageSliderValue >= constraint.MinValueToShowInPercents)
                {
                    return constraint.Fill;
                }
            }

            return _sliderFills[0].Fill;
        }
    }
}