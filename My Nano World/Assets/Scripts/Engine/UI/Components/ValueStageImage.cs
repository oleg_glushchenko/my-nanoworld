﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

[ExecuteInEditMode]
public class ValueStageImage : MonoBehaviour
{
    public float Value
    {
        get { return _value; }
        set { _value = value; }
    }

    /// <summary>
    /// Список всех вариантов заливки
    /// </summary>
    public List<SliderFill> SliderFills;

    /// <summary>
    /// Закешированное изображение, которому будет назначаться спрайт
    /// </summary>
    private Image _cachedImage;

    [SerializeField]
    [Range(0,1)]
    private float _value;

    /// <summary>
    /// Вызывается при включении этого объекта
    /// </summary>
    public void OnEnable()
    {
        _cachedImage = GetComponent<Image>();
    }

    /// <summary>
    /// Вызывается каждый фрейм
    /// </summary>
    public void Update()
    {
        if (SliderFills != null && _cachedImage != null )
        {
            var percentageSliderValue = (int)(Value * 100f);
            for (int i = 0; i < SliderFills.Count; i++)
            {
                if (percentageSliderValue <= SliderFills[i].MaxValueToShowInPercents &&
                    percentageSliderValue >= SliderFills[i].MinValueToShowInPercents)
                {
                    _cachedImage.sprite = SliderFills[i].Fill;
                    return;
                }
            }
        }
    }
}
