using System;
using UnityEngine;
using UnityEngine.UI;

namespace Engine.UI.Components
{
    public sealed class FillImageProgressBar : ProgressBar 
    {
        [SerializeField] private Image _progressFillImage;

        private float _progressValue = -1;
        private const float ValueDelta = 0.005f;
        
        public override float Value
        {
            get => _progressValue;
            set
            {
                if (Math.Abs(value - _progressValue) > ValueDelta)
                {
                    _progressValue = value;
                    _progressFillImage.fillAmount = _progressValue;
                    InvokeProgressChanged();
                }
            }
        }
    }
}