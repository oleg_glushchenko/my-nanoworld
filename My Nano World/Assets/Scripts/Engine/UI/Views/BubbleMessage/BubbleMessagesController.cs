﻿using UnityEngine;
using System.Collections.Generic;
using NanoReality.StrangeIoC;
using Assets.NanoLib.UI.Core.Signals;
using Assets.Scripts.Engine.UI.Views.BubbleMessage;

namespace Engine.UI.Views.BubbleMessage
{
    public class BubbleMessagesController : AViewMediator
    {
        #region Inject

        [Inject]
        public ShowBubbleMessageSignal jShowBubleMessageSignal { get; set; }

        [Inject]
        public HideBubbleMessageSignal jHideBubleMessageSignal { get; set; }

        #endregion

        [SerializeField] BubbleMessageView _oneLineTextPrefab;
        [SerializeField] BubbleMessageView _textWithImagePrefab;

        private Dictionary<BubbleMessageType, BubbleMessageView> _prefabs;
        private Dictionary<BubbleMessageType, List<BubbleMessageView>> _avalibleBubbles;
        private Dictionary<Transform, BubbleMessageView> _runningBubbles;
        private bool _isPanelsClosed;

        protected override void PreRegister()
        {
            _prefabs = new Dictionary<BubbleMessageType, BubbleMessageView>();
            _prefabs.Add(BubbleMessageType.TextWithImage, _textWithImagePrefab);
            _prefabs.Add(BubbleMessageType.OneLineText, _oneLineTextPrefab);

            _avalibleBubbles = new Dictionary<BubbleMessageType, List<BubbleMessageView>>();

            _runningBubbles = new Dictionary<Transform, BubbleMessageView>();
        }

        protected override void OnRegister()
        {
            jShowBubleMessageSignal.AddListener(ShowBubble);
            jHideBubleMessageSignal.AddListener(HideBubble);

            _isPanelsClosed = true;
        }        

        protected override void OnRemove()
        {
            jShowBubleMessageSignal.RemoveListener(ShowBubble);
            jHideBubleMessageSignal.RemoveListener(HideBubble);
        }

        private void ShowBubble(ShowBubbleMessageParametrs parametrs)
        {
            var bubble = GetBubble(parametrs.MessageType);
            if (_runningBubbles.ContainsKey(parametrs.Anchor))
            {
                _runningBubbles[parametrs.Anchor].Hide();
            }
            _avalibleBubbles[parametrs.MessageType].Remove(bubble);
            _runningBubbles.Add(parametrs.Anchor, bubble);
            parametrs.HideCallback += () =>
            {
                _avalibleBubbles[parametrs.MessageType].Add(bubble);
                _runningBubbles.Remove(parametrs.Anchor);
            };
            bubble.Show(parametrs);
        }

        private void HideBubble(HideBubbleMessageParameters parameters)
        {
            if (_runningBubbles.ContainsKey(parameters.Anchor))
            {
                _runningBubbles[parameters.Anchor].Hide();
            }
        }

        private BubbleMessageView GetBubble(BubbleMessageType type)
        {
            if (!_avalibleBubbles.ContainsKey(type))
            {
                _avalibleBubbles.Add(type, new List<BubbleMessageView>());
            }
            if (_avalibleBubbles[type].Count == 0)
            {
                _avalibleBubbles[type].Add(GameObject.Instantiate(_prefabs[type]));
                _avalibleBubbles[type][0].Initialize(transform);
            }
            return _avalibleBubbles[type][0];
        }
    }
}