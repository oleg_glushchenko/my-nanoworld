﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Engine.UI.Views.BubbleMessage
{
    public struct HideBubbleMessageParameters
    {
        public Transform Anchor;
    }
}
