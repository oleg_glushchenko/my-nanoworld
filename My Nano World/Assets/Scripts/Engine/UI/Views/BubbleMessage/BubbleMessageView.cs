﻿using strange.extensions.mediation.impl;
using UnityEngine.UI;
using UnityEngine;
using DG.Tweening;
using MultiLanguageSystem;
using NanoReality.GameLogic.Utilities;
using TMPro;

namespace Engine.UI.Views.BubbleMessage
{
    public class BubbleMessageView : View
    {
        [SerializeField] private TextMeshProUGUI _message;
        [SerializeField] private Button _button;
        [SerializeField] private RectTransform _transform;

        private ShowBubbleMessageParametrs _parametrs;
        private bool _isRunning;
        private Transform _rootTransformForHiding;
        private Tween _delayedHide;

        protected override void Awake()
        {
            base.Awake();
            _button.onClick.AddListener(OnButtonClick);
            
            if (!LocalizationMLS.IsQuitting && LocalizationMLS.Instance != null)
            {
                LocalizationMLS.Instance.OnChangedLanguage += SetText;
            }
        }

        private void SetText()
        {
            Utility.SetValueText(_message, LocalizationMLS.Instance.GetText(_parametrs.Message));
        }

        public void Initialize(Transform rootTransformForHiding)
        {
            _rootTransformForHiding = rootTransformForHiding;
        }

        public void Show(ShowBubbleMessageParametrs parameters)
        {
            _parametrs = parameters;
            
            Show();
            StartDelayedHide();
        }

        public void Hide()
        {
            if (_isRunning)
            {
                _isRunning = false;
                transform.SetParent(_rootTransformForHiding);
                 gameObject.SetActive(false);
                _message.text = string.Empty;
                _parametrs.HideCallback.SafeRaise();
                StopDelayedHide();
            }
        }

        private void OnEnable()
        {
            if (_isRunning)
            {
                StartDelayedHide();
            }
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            if (!LocalizationMLS.IsQuitting && LocalizationMLS.Instance != null)
            {
                LocalizationMLS.Instance.OnChangedLanguage -= SetText;
            }
            StopDelayedHide();
        }

        private void Show()
        {
            _isRunning = true;
            transform.SetParent(_parametrs.Anchor, false);
            _transform.anchoredPosition = _parametrs.AnchorShift;
            _transform.localScale = Vector3.one;
            gameObject.SetActive(true);
            SetText();
        }

        private void OnButtonClick()
        {
            _parametrs.ClickCallback.SafeRaise();
        }

        private void StartDelayedHide()
        {
            _delayedHide = DOVirtual.DelayedCall(_parametrs.Time, Hide);
        }

        private void StopDelayedHide()
        {
            _delayedHide?.Kill();
            _delayedHide = null;
        }
    }
}