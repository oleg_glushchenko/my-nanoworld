﻿using System;
using strange.extensions.signal.impl;
using UnityEngine;

namespace Engine.UI.Views.BubbleMessage
{
    public class ShowBubbleMessageSignal : Signal<ShowBubbleMessageParametrs>{}
}