﻿namespace Engine.UI.Views.BubbleMessage
{
    public enum BubbleMessageType
    {
        OneLineText,
        TextWithImage
    }
}