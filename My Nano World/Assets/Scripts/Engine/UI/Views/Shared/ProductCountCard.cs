﻿using Assets.NanoLib.Utilities.Pulls;
using Assets.Scripts.Engine.UI.Views.Tooltips;
using Assets.Scripts.GameLogic.Utilites;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using strange.extensions.signal.impl;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Assets.Scripts.Engine.UI.Views.Trade.Items
{
    public enum TooltipProductType
    {
        Product,
        Soft,
        Hard
    }
    
    public class ProductCountCard : GameObjectPullable, IBeginDragHandler, IPointerDownHandler, IPointerUpHandler, IPointerClickHandler
    {
        #region Layout

        [SerializeField] RectMask2D _productImageMask;
        [SerializeField] private Image _backgroundImage;
        
        [SerializeField] private Sprite _normalBgSprite;
        [SerializeField] private Sprite _specialBgSprite;
        [SerializeField] private Sprite _activeBgSprite;
        [SerializeField] private Sprite _enoughBgSprite;
        [SerializeField] private Image _backgroundInnerImage;
        [SerializeField] private Sprite _normalInnerBgSprite;
        [SerializeField] private Sprite _specialInnerBgSprite;
        [SerializeField] private Sprite _activeInnerBgSprite;
        [SerializeField] private Sprite _enoughInnerBgSprite;
        [SerializeField] private Image _counterBackgroundImage;
        [SerializeField] private Sprite _normalCounterSprite;
        [SerializeField] private Sprite _specialCounterSprite;
        
        [SerializeField] private Sprite _enoughCounterSprite;
        [SerializeField] private Sprite _notEnoughCounterSprite;
        
        [SerializeField] private Image _productImage;
        [SerializeField] private Image _specialOrderImage;
        [SerializeField] private TextMeshProUGUI _productItemCountText;
        
        [SerializeField] private Image _readyMark;

        #endregion

        #region Fields

        public Image BackgroundImage => _backgroundImage;

        private bool IsSpecialProduct
        {
            get
            {
                if (Product != null)
                {
                    return Product.SectorID == 0;
                }
                return false;
            }
        }

        private Sprite EnoughBgSprite => IsSpecialProduct && _specialBgSprite != null ? _specialBgSprite : _enoughBgSprite;

        private Sprite NotEnoughBgSprite => IsSpecialProduct && _specialBgSprite != null ? _specialBgSprite : _normalBgSprite;

        private Sprite EnoughInnerBgSprite => IsSpecialProduct && _specialInnerBgSprite != null ? _specialInnerBgSprite : _enoughInnerBgSprite;

        private Sprite NotEnoughInnerBgSprite => IsSpecialProduct && _specialInnerBgSprite != null ? _specialInnerBgSprite : _normalInnerBgSprite;

        private Sprite NormalCounterBgSprite => IsSpecialProduct && _specialCounterSprite != null ? _specialCounterSprite : _normalCounterSprite;

        private Sprite EnoughCounterBgSprite => IsSpecialProduct && _specialCounterSprite != null ? _specialCounterSprite : _enoughCounterSprite;

        private Sprite NotEnoughCounterBgSprite => _notEnoughCounterSprite;

        public ProductCountCardInitData Model { get; private set; }
        
        public Product Product { get; private set; }

        private int _needProductsCount;

        private TooltipProductType TooltipType { get; set; }

        /// <summary>
        /// true, если отображается необходимое и имеющееся количество продуктов, false - если отображается только количество одной цифрой
        /// </summary>
        private bool _isCountNeed;

        /// <summary>
        /// true, если целевого продукта для этой плашки достаточно
        /// </summary>
        private bool _isEnough;
        
        public Signal<TooltipData> SignalOnSelected = new Signal<TooltipData>();

        public Signal SignalOnHideTooltip = new Signal();
        
        private bool _interactable;

        public bool Interactable
        {
            get => _interactable;
            set => _interactable = value;
        }

        #endregion

        #region Methods

        private TooltipData _tooltipData;

        private void ShowTooltip()
        {
            switch (TooltipType)
            {
                case TooltipProductType.Hard:
                    _tooltipData.Type = Tooltips.TooltipType.Simple;
                    _tooltipData.Message = LocalizationMLS.Instance.GetText(LocalizationKeyConstants.SHARED_TOOLTIP_LABEL_NANO_PREM);
                    break;
                case TooltipProductType.Product:
                    _tooltipData.Type = Tooltips.TooltipType.ProductItems;
                    _tooltipData.ProductItem = Product;
                    _tooltipData.ProductAmount = _needProductsCount;
                    break;
            }
            SignalOnSelected.Dispatch(_tooltipData);
        }
        

        private void OnSelected()
        {
            if(_backgroundImage!= null)
                _backgroundImage.sprite = _activeBgSprite;

            if(_backgroundInnerImage!= null)
                _backgroundInnerImage.sprite = _activeInnerBgSprite;
        }

        /// <summary>
        /// Вызывается когда элемент отжимается
        /// </summary>
        private void OnDeselected()
        {
            if (_isCountNeed)
            {
                if(_backgroundImage != null)
                    _backgroundImage.sprite = _isEnough ? EnoughBgSprite : NotEnoughBgSprite;

                if (_backgroundInnerImage != null)
                    _backgroundInnerImage.sprite = _isEnough ? EnoughInnerBgSprite : NotEnoughInnerBgSprite;
            }
            else
            {
                if (_backgroundImage != null)
                    _backgroundImage.sprite = NotEnoughBgSprite;

                if (_backgroundInnerImage != null)
                    _backgroundInnerImage.sprite = NotEnoughInnerBgSprite;
            }
        }

        /// <summary>
        /// Вызывается при возвращении элемента в пул
        /// </summary>
        public override void OnPush()
        {
            OnDeselected();
            
            base.OnPush();
        }

        private void UpdateView()
        {
            Product = Model.ProductObject;
            if (Model.NeedProductsCount != null)
                _needProductsCount = Model.NeedProductsCount.Value;

            if (Model.Type != null)
                TooltipType = Model.Type.Value;

            _isCountNeed = Model.IsCountNeed;

            if (_counterBackgroundImage != null)
                _counterBackgroundImage.gameObject.SetActive(Model.IsCountNeed);

            if (_productImage != null)
                _productImage.sprite = Model.ProductSprite;

            if (_specialOrderImage != null)
            {
                _specialOrderImage.sprite = Model.SpecialOrderSprite;
                if (Model.SpecialOrderSprite != null)
                {
                    _specialOrderImage.enabled = Model.SpecialOrderSprite != null;
                }
                else
                {
                    _specialOrderImage.enabled = false;
                }
            }

            if (Model.ProductsCount != null)
            {
                if (_productItemCountText != null)
                    _productItemCountText.text = Model.ProductsCount.Value.ToString();
            }

            if (Model.NeedProductsCount != null) //4
            {

                if (_productItemCountText != null)
                    _productItemCountText.text = Model.NeedProductsCount.Value.ToString();

                if (Model.CurrentProductsCount != null)//3
                {
                    if (_productItemCountText != null)
                    _productItemCountText.text = string.Format("{0}/{1}", Model.CurrentProductsCount.Value, Model.NeedProductsCount.Value);

                    _isEnough = Model.CurrentProductsCount.Value >= Model.NeedProductsCount.Value;

                    if (_readyMark != null)
                        _readyMark.enabled = _isEnough;
                    if (_backgroundImage != null)
                        _backgroundImage.sprite = _isEnough ? EnoughBgSprite : NotEnoughBgSprite;
                    if (_backgroundInnerImage != null)
                        _backgroundInnerImage.sprite = _isEnough ? EnoughInnerBgSprite : NotEnoughInnerBgSprite;
                    if (_counterBackgroundImage != null)
                    _counterBackgroundImage.sprite = _isEnough ? EnoughCounterBgSprite : NotEnoughCounterBgSprite;
                }
            }
            else
            {
                if (_backgroundImage != null)
                    _backgroundImage.sprite = NotEnoughBgSprite;
                if (_backgroundInnerImage != null)
                    _backgroundInnerImage.sprite = NotEnoughInnerBgSprite;
                if (_counterBackgroundImage != null)
                    _counterBackgroundImage.sprite = NormalCounterBgSprite;
            }

            CreateTooltipData(Model.ShowOnlySimpleTools.Value);
        }

        //Universal Initialize
        public void Initialize(ProductCountCardInitData productCountCardInitData)
        {
            Model = productCountCardInitData;
            _interactable = true;
            UpdateView();
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            SignalOnHideTooltip.Dispatch();
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            SignalOnHideTooltip.Dispatch();
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            SignalOnHideTooltip.Dispatch();
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (_interactable)
            {
                ShowTooltip();
            }
        }

        #endregion

        private void CreateTooltipData(bool showOnlySimpleTools)
        {
            var tooltipImageTransform = _backgroundImage ? _backgroundImage : _productImage;

            _tooltipData = new TooltipData
            {
                ItemTransform = (RectTransform) tooltipImageTransform.transform,
                ShowOnlySimpleTool = showOnlySimpleTools,
                OnTooltipShowAction = OnSelected,
                OnTooltipHideAction = OnDeselected
            };
        }
    }
}
