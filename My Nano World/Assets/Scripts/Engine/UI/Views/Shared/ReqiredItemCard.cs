﻿using Assets.NanoLib.Utilities.Pulls;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Engine.UI.Views.Shared
{
    public class ReqiredItemCard : GameObjectPullable
    {
        #region InspectorFields

        [SerializeField]
        protected Image ProductImage;

        [SerializeField]
        protected TextMeshProUGUI RequirementsCounter;

        [SerializeField]
        protected Image RequirementsCounterBack;

        [SerializeField]
        protected Sprite LessThanRequiredBackgr;

        [SerializeField]
        protected Sprite EqualToRequiredBackgr;

        #endregion

        public virtual void UpdateProduct(int current, int needed, Sprite itemIcon)
        {
            ProductImage.sprite = itemIcon;
            RequirementsCounter.text = string.Format("{0}/{1}", current, needed);
            RequirementsCounterBack.sprite = current >= needed ? EqualToRequiredBackgr : LessThanRequiredBackgr;
        }
    }
}