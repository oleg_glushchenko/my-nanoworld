﻿using System.Text;
using Assets.NanoLib.Utilities.Pulls;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Engine.UI.Views.Shared
{
    public class TextIconItemCard : GameObjectPullable
    {
        public string Text
        {
            get { return _text.text; }
            set { _text.text = value; }
        }

        public Texture Icon
        {
            get { return _icon.texture; }
            set { _icon.texture = value; }
        }

        [SerializeField]
        private Text _text;

        [SerializeField]
        private RawImage _icon;
    }
}
