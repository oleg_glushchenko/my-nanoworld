﻿using System;
using Assets.Scripts.Engine.UI.Views.Popups;
using I2.Loc;
using NanoReality.GameLogic.Services;
using NanoReality.StrangeIoC;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Engine.UI.Views.Buttons
{
    public class RewardedAdButtonView : AViewMediator
    {
        [SerializeField] private Button _button;
        [SerializeField] private AdTypePanel _adType;

        private bool _isButtonViewRegistered;

        [Inject] public IAdsService jAdsService { get; set; }
        [Inject] public IPopupManager jPopupManager { get; set; }
        
        protected override void PreRegister() { }

        protected override void OnRegister()
        {
            _button.onClick.AddListener(OnButtonClick);
            
            jAdsService.VideoWatched += OnVideoWatched;
            jAdsService.VideoLoaded += OnVideoLoaded;
            jAdsService.VideoSkipped += OnVideoSkipped;

            UpdateView();
            _isButtonViewRegistered = true;
        }

        protected override void OnRemove()
        {
            _button.onClick.AddListener(OnButtonClick);
            
            jAdsService.VideoWatched -= OnVideoWatched;
            jAdsService.VideoLoaded -= OnVideoLoaded;
            jAdsService.VideoSkipped -= OnVideoSkipped;
        }
        
        private void OnEnable()
        {
            if (!_isButtonViewRegistered)
                return;
            
            UpdateView();
        }

        private void UpdateView()
        {
            if (jAdsService.IsVideoLoaded)
            {
                _button.interactable = true;
            }
            else
            {
                _button.interactable = false;
                jAdsService.LoadVideo();
            }
        }

        private void OnButtonClick()
        {
            if (jAdsService.HasAvailableAdsByPanelType(_adType))
            {
                jAdsService.ShowVideo(_adType);
            }
            else
            {
                jPopupManager.Show(new InfoPopupSettings(ScriptLocalization.UICommon.ADS_LIMIT_TITLE, ScriptTerms.UICommon.ADS_LIMIT_DESCRIPTION));
            }
        }
        
        private void OnVideoLoaded(object sender, EventArgs e)
        {
            if(gameObject.activeInHierarchy)
                UpdateView();
        }

        private void OnVideoWatched(object sender, EventArgs e)
        {
            if(gameObject.activeInHierarchy)
                UpdateView();
        }
        
        private void OnVideoSkipped(object sender, EventArgs e)
        {
            if(gameObject.activeInHierarchy)
                UpdateView();
        }
    }
}