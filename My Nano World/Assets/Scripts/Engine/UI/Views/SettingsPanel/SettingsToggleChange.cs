﻿using NanoReality.Game.UI.SettingsPanel;
using UnityEngine;
using NanoReality.GameLogic.Settings.GameSettings.Models.Api;
using strange.extensions.mediation.impl;
using UnityEngine.UI;

public class SettingsToggleChange : View
{
    [SerializeField] private Toggle _toggle;
    [SerializeField] private GameObject _toggleOn;
    [SerializeField] private GameObject _toggleOff;

    private IGameSettings _gameSettings;
    private SettingsToggleType _settingsToggleType;
    
    public void InitToggle(SettingsToggleType toggleType, IGameSettings gameSettings)
    {
        _gameSettings = gameSettings;
        
        _settingsToggleType = toggleType;
        SetInitialToggleValue();
        
        _toggle.onValueChanged.AddListener(OnToggleValueChange);
    }
    
    private void SetInitialToggleValue()
    {
        _toggle.isOn = _gameSettings.IsEnabled(_settingsToggleType);
        _toggleOn.SetActive(_toggle.isOn);
        _toggleOff.SetActive(!_toggle.isOn);
    }

    private void OnToggleValueChange(bool value)
    {
        _gameSettings.SetEnabled(_settingsToggleType, value);
        _toggleOn.SetActive(value);
        _toggleOff.SetActive(!value);
    }
}