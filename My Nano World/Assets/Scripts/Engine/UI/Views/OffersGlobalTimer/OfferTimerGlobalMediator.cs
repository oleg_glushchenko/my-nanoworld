using Assets.NanoLib.UI.Core.Signals;
using Assets.NanoLib.UtilitesEngine;
using Assets.Scripts.Engine.UI.Views.Hud;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using NanoReality.GameLogic.Common.Controllers;
using NanoReality.GameLogic.Common.Controllers.Signals;
using NanoReality.GameLogic.IAP;
using strange.extensions.mediation.impl;
using System;
using System.Linq;
using NanoReality.Engine.Utilities;

namespace NanoReality.Engine.UI.Views.OffersTimer
{
    public class OfferTimerGlobalMediator : Mediator
    {
        [Inject] public IIapService jIapService { get; set; }
        [Inject] public ITimerManager jTManager { get; set; }
        [Inject] public UserLevelUpSignal jUserLevelUpSignal { get; set; }
        [Inject] public SignalOffersUpdated jSignalOffersUpdated { get; set; }
        [Inject] public OnHudBottomPanelIsVisibleSignal jOnHudBottomPanelIsVisibleSignal { get; set; }
        [Inject] public SignalOnConstructControllerChangeState jSignalOnConstructControllerChangeState { get; set; }
        [Inject] public OffersValidationController jOffersValidationController { get; set; }
        [Inject] public DisableOffersButtonSignal jDisableOffersButtonSignal { get; set; }

        private OffersTimerGlobalView _view;
        private ITimer _timer;
        private bool _canBeHided = true;
        private bool _disableHideAnyCase;
        private bool _dontShowButton;
        private const int Delay = 20;

        private void Start()
        {
            _view = GetComponent<OffersTimerGlobalView>();
            jUserLevelUpSignal.AddListener(DisableButtonAndTimer);
            jSignalOffersUpdated.AddListener(TimerUpdate);
            // jOnHudBottomPanelIsVisibleSignal.AddListener(Hide);
            // jSignalOnConstructControllerChangeState.AddListener(HideOnStartBuilding);
            jDisableOffersButtonSignal.AddListener(DisableButtonAndTimer);
            TimerUpdate();
        }

        private void Hide(bool hide)
        {
            if (!_disableHideAnyCase)
            {
                _dontShowButton = !hide; //Prevent display timer button if shop is opened
                if (_canBeHided)
                {
                    gameObject.SetActive(hide);
                }
            }
        }

        private void HideOnStartBuilding(bool hide)
        {
            _canBeHided = !hide;
            Hide(!hide);
        }

        private void Hide(HudType type, bool visible)
        {
            Hide(visible);
        }

        [Deconstruct]
        public void Deconstruct()
        {
            jUserLevelUpSignal.RemoveListener(DisableButtonAndTimer);
            jSignalOffersUpdated.RemoveListener(TimerUpdate);
            // jOnHudBottomPanelIsVisibleSignal.RemoveListener(Hide);
            // jSignalOnConstructControllerChangeState.RemoveListener(HideOnStartBuilding);
            jDisableOffersButtonSignal.RemoveListener(DisableButtonAndTimer);
        }

        private void DisableButtonAndTimer()
        {
            _disableHideAnyCase = true;
            _dontShowButton = false;
            jOffersValidationController.HasValidOffers = false;
            _view.SetActiveTimerGameObject(false);
            gameObject.SetActive(false);
        }

        private void TimerUpdate()
        {
            if (jIapService.InAppShopOffers != null)
            {
                jOffersValidationController.HasValidOffers = false;

                if (jIapService.InAppShopOffers.Products.Length == 0)
                {
                    DisableButtonAndTimer();
                }
                else
                {
                    var currentTime = ((DateTimeOffset) DateTime.Now).ToUnixTimeSeconds();

                    //Offers Validation   
                    //First pass, let's check for a valid offers with timer
                    //Get list of offers with valid finish time
                    var offersWithValidFinishTime = jIapService.InAppShopOffers.Products
                        .Where(offer => offer.FinishTime > 0 && currentTime < offer.FinishTime)
                        .OrderBy(offer => offer.FinishTime).ToList();

                    if (offersWithValidFinishTime.Any())
                    {
                        //There is available offer with finish time and finish time has not come yet
                        foreach (var item in offersWithValidFinishTime)
                        {
                            if (HasValidStartTime(item.StartTime, currentTime))
                            {
                                jOffersValidationController.HasValidOffers = true;
                                _canBeHided = true;
                                _disableHideAnyCase = false;
                                InitTimer(item.FinishTime);
                                break;
                            }
                        }
                    }

                    if (jOffersValidationController.HasValidOffers)
                        return;

                    //Second pass, let's check for a valid offers without timer
                    var validOffersNoFinishTime = jIapService.InAppShopOffers.Products
                        .Where(offer => offer.FinishTime == 0)
                        .OrderBy(offer => offer.FinishTime);


                    if (validOffersNoFinishTime.Any(item => HasValidStartTime(item.StartTime, currentTime)))
                    {
                        jOffersValidationController.HasValidOffers = true;
                        _disableHideAnyCase = false;
                        _canBeHided = true;
                        _view.SetActiveTimerGameObject(false);
                        gameObject.SetActive(true);
                    }

                    if (jOffersValidationController.HasValidOffers == false)
                    {
                        //The client has the offers,  but hey are not valid. 
                        _canBeHided = false;
                        gameObject.SetActive(false);
                    }
                }
            }
        }

        private void InitTimer(double finishTime)
        {
            _timer?.Dispose();
            _timer = null;
            
            var _dateTime = DateTimeUtils.UnixStartTime.AddSeconds(finishTime).ToLocalTime();
            var secondsLeft = (_dateTime - DateTime.Now).TotalSeconds;
            _view.SetActiveTimerGameObject(true);
            _timer = jTManager.StartServerTimer((float) secondsLeft, OnTimerFinish, elapsed => { _view.SetText(UiUtilities.GetFormattedTimeFromSecondsShorted((int) (secondsLeft - elapsed))); });
            _view.SetText(UiUtilities.GetFormattedTimeFromSecondsShorted((int) secondsLeft));

            //If shop is opened no need to display it
            if (_dontShowButton == false)
                gameObject.SetActive(true);
        }

        private void OnTimerFinish()
        {
            _timer?.Dispose();
            _timer = null;
            TimerUpdate();
        }
        
        private static bool HasValidStartTime(long startTime, long currentTime) => currentTime + Delay > startTime;
    }
}