﻿using NanoReality.GameLogic.Utilites;
using System;
using System.Linq;
using System.Text;
using TMPro;
using Unity.Cloud.UserReporting;
using Unity.Cloud.UserReporting.Client;
using Unity.Cloud.UserReporting.Plugin;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Engine.UI.Views.Popups
{
    public class UserReportPopupView : BasePopupView
    {
        [SerializeField] private Button _sendButton;
        [SerializeField] private Button _closeButton;
        [SerializeField] private Button _backgroundButton;
        [SerializeField] private TextMeshProUGUI _titleText;
        [SerializeField] private TextMeshProUGUI _messagePlaceholderText;
        [SerializeField] private TextMeshProUGUI _sendButtonText;
        [SerializeField] private TMP_InputField _messageInput;
        [SerializeField] private GameObject _sendingInfoObject;
        [SerializeField] private TextMeshProUGUI _sendingInfoOSendButtonText;

        [Inject] public SignalReloadApplication jSignalReloadApplication { get; set; }

        private string TextMessage => _messageInput.text;

        private UnityUserReportingUpdater _unityUserReportingUpdater = new UnityUserReportingUpdater();

        private void OnEnable()
        {
            SetupPopup();

            _sendButton.onClick.AddListener(OnClickSendButton);
            _closeButton.onClick.AddListener(OnCancelButtonClick);
            _backgroundButton.onClick.AddListener(OnCancelButtonClick);

            UpdateLocalization();

            InitUserReport();
        }

        private void OnDisable()
        {
            _sendButton.onClick.RemoveListener(OnClickSendButton);
            _closeButton.onClick.RemoveListener(OnCancelButtonClick);
            _backgroundButton.onClick.RemoveListener(OnCancelButtonClick);
        }

        private void Update()
        {
            // Update Client
            // The UnityUserReportingUpdater updates the client at multiple points during the current frame.
            _unityUserReportingUpdater.Reset();
            StartCoroutine(_unityUserReportingUpdater);
        }

        private void UpdateLocalization()
        {
            var title = LocalizationUtils.LocalizeL2("UICommon/USER_REPORTING_TITLE");
            var enterMessage = LocalizationUtils.LocalizeL2("UICommon/USER_REPORTING_ENTER_MESSAGE");
            var sendButton = LocalizationUtils.LocalizeL2("UICommon/USER_REPORTING_SEND_BUTTON");

            SetTextMessagePlaceHolder(enterMessage);
            SetTextSendButton(sendButton);
            SetTextTitle(title);
        }

        private void SetupPopup()
        {
            SetVisibleSendingInfoObject(false);
            SetTextMessage(string.Empty);
        }

        private void BeginSend()
        {
            SetVisibleSendingInfoObject(true);
        }

        private void OnEndSendSuccess()
        {
            OnCancelButtonClick();
            jSignalReloadApplication.Dispatch();
        }

        private void OnEndSendError()
        {
            SetVisibleSendingInfoObject(false);
            jSignalReloadApplication.Dispatch();
        }

        private void OnClickSendButton()
        {
            BeginSend();
            SendReport(TextMessage, onSuccess: OnEndSendSuccess, onError: OnEndSendError);
        }

        private void SetTextTitle(string text) => _titleText.text = text;
        private void SetTextMessagePlaceHolder(string text) => _messagePlaceholderText.text = text;
        private void SetTextSendButton(string text)
        {
            _sendButtonText.text = text;
            _sendingInfoOSendButtonText.text = text;
        }

        private void SetTextMessage(string text) => _messageInput.text = text;

        private void SetVisibleSendingInfoObject(bool isVisible) => _sendingInfoObject.SetActive(isVisible);

        /// <summary>
        /// Send user report
        /// </summary>
        /// <param name="onProgress">(uploadProgress, downloadProgress) percent => [0.0 .. 1.0] </param>
        private void SendReport(string message, Action<float, float> onProgress = default, Action onSuccess = default, Action onError = default)
        {
            var msg = message.Trim();
            var isValidMessage = !string.IsNullOrEmpty(msg);

            if (isValidMessage)
            {
                UnityUserReporting.CurrentClient.CreateUserReport((userReport) =>
                {
                    if (string.IsNullOrEmpty(userReport.ProjectIdentifier))
                        Debug.LogWarning("The user report's project identifier is not set. Please setup cloud services using the Services tab or manually specify a project identifier when calling UnityUserReporting.Configure().");

                    userReport.Dimensions.Add(new UserReportNamedValue("Platform.Version", string.Format("{0}.{1}", GetPlatform(userReport), GetVersion(userReport))));
                    userReport.Fields.Add(new UserReportNamedValue("Description", message));

                    UnityUserReporting.CurrentClient.SendUserReport(userReport, onProgress, (isSuccess, _) =>
                    {
                        if (isSuccess)
                            onSuccess?.Invoke();
                        else
                            onError?.Invoke();
                    });
                });
            }
            else
            {
                onError?.Invoke();
            }
        }

        private string GetVersion(UserReport userReport)
        {
            var versions = userReport.DeviceMetadata.Where(x => x.Name == "Version");
            return versions.Any()
            ? versions.First().Value
            : "0.0";
        }

        private string GetPlatform(UserReport userReport)
        {
            var platforms = userReport.DeviceMetadata.Where(x => x.Name == "Platform");
            return platforms.Any()
            ? platforms.First().Value
            : "Unknown";
        }

        private void InitUserReport()
        {
            UnityUserReporting.Configure(new UserReportingClientConfiguration());

            UnityUserReporting.CurrentClient.IsSelfReporting = true;
            UnityUserReporting.CurrentClient.SendEventsToAnalytics = true;

            // Ping
            var url = string.Format("https://userreporting.cloud.unity3d.com/api/userreporting/projects/{0}/ping", UnityUserReporting.CurrentClient.ProjectIdentifier);
            UnityUserReporting.CurrentClient.Platform.Post(url, "application/json", Encoding.UTF8.GetBytes("\"Ping\""), (upload, download) => { }, (result, bytes) => { });
        }

    }
}
