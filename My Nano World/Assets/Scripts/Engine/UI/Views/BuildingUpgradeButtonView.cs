﻿using System;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using strange.extensions.mediation.impl;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Engine.UI.Views
{
    [RequireComponent(typeof(Button))]
    [RequireComponent(typeof(LongPressEventTrigger))]
    public class BuildingUpgradeButtonView : View
    {
        [SerializeField] private Button _button;

        private IMapBuilding _building;
        private Action _onClick;

        public Button Button => _button;

        public void Set(IMapBuilding building, Action onClick)
        {
            _building = building;
            _onClick = onClick;

            _button.interactable = _building.CanBeUpgraded;
        }

        private void OnEnable()
        {
            _button.onClick.AddListener(OnButtonClick);
        }

        private void OnDisable()
        {
            _button.onClick.RemoveListener(OnButtonClick);
        }

        private void OnButtonClick()
        {
            _onClick?.Invoke();
        }
    }
}