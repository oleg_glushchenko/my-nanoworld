﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Engine.UI.Views.Popups;
using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using NanoLib.Core.Pool;
using NanoReality.GameLogic.Services;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Game.UI
{
    /// <summary>
    /// Allows the user to select resources from the warehouse.
    /// User can select only one product, in future need expand logic to possibility to select couple of products.
    /// </summary>
    public class WarehouseSelectionPopupView : BasePopupView
    {
        [SerializeField] private WarehouseSelectionProductView _productViewPrefab;
        [SerializeField] private Transform _productsParentPanel;
        [SerializeField] private Button _closeButton;
        [SerializeField] private Button _confirmButton;
        
        private WarehouseSelectionPopupSettings _settings;
        private WarehouseSelectionProductModel _selectedProductModel;
        private readonly CompositeDisposable _compositeDisposable = new CompositeDisposable();
        
        private readonly UnityPool<WarehouseSelectionProductView> _productItemsPool = new UnityPool<WarehouseSelectionProductView>();
        private readonly List<WarehouseSelectionProductView> _visibleProductViews = new List<WarehouseSelectionProductView>();

        public List<WarehouseSelectionProductView> VisibleProductViews => _visibleProductViews;
        public Button ConfirmButton => _confirmButton;
        
        [Inject] public IIconProvider jIconProvider { get; set; }

        protected override void Awake()
        {
            base.Awake();
            
            _productItemsPool.InstanceProvider = new PrefabInstanceProvider(_productViewPrefab);
        }
        
        public override void Initialize(IPopupSettings settings)
        {
            base.Initialize(settings);
            
            _settings = (WarehouseSelectionPopupSettings)settings;
        }

        public override void Show(Action<PopupResult> callback)
        {
            base.Show(callback);
            
            _closeButton.onClick.AddListener(Hide);
            _confirmButton.onClick.AddListener(OnConfirmClicked);
            
            UpdateView();
            _confirmButton.interactable = false;
        }

        public override void Hide()
        {
            base.Hide();
            
            _closeButton.onClick.RemoveListener(Hide);
            _confirmButton.onClick.RemoveListener(OnConfirmClicked);
            
            foreach (var productView in _visibleProductViews)
            {
                _productItemsPool.Push(productView);
            }
            
            _visibleProductViews.Clear();
            _compositeDisposable.Clear();
            _selectedProductModel = null;
        }

        private void UpdateView()
        {
            var settingsTotalWarehouseProducts = _settings.WarehouseProducts;

            for (var productIndex = 0; productIndex < settingsTotalWarehouseProducts.Count; productIndex++)
            {
                var product = settingsTotalWarehouseProducts.ElementAt(productIndex);
                var id = product.Key;
                var count = product.Value;
                var productSprite = jIconProvider.GetProductSprite(id);
                
                var itemInstance = _productItemsPool.Pop();
                itemInstance.transform.SetParent(_productsParentPanel, false);
                itemInstance.transform.SetSiblingIndex(productIndex);
                
                var instanceModel = itemInstance.Model;
                instanceModel.ProductCount.SetValueAndForceNotify(count);
                instanceModel.ProductSprite.SetValueAndForceNotify(productSprite);
                instanceModel.ProductId.SetValueAndForceNotify(id);
                instanceModel.Clicked.Subscribe(_ => { OnProductClicked(instanceModel);}).AddTo(_compositeDisposable);
                
                _visibleProductViews.Add(itemInstance);
            }
        }

        private void OnProductClicked(WarehouseSelectionProductModel clickedModel)
        {
            foreach (var productView in _visibleProductViews.Where(productView => productView.Model != clickedModel))
            {
                productView.Model.Selected.SetValueAndForceNotify(false);
            }

            var isSelected = !clickedModel.Selected.Value;
            clickedModel.Selected.SetValueAndForceNotify(isSelected); 
            _selectedProductModel = isSelected ? clickedModel : null;
            _confirmButton.interactable = isSelected;
        }

        private void OnConfirmClicked()
        {
            CurrentPopupResult = PopupResult.Ok;

            _settings.ProductSelectedCallback?.Invoke(_selectedProductModel.ProductId.Value);
            Hide();
        }
    }
    
    public sealed class WarehouseSelectionPopupSettings : BasePopupSettings
    {
        public override Type PopupType { get; } = typeof(WarehouseSelectionPopupView);

        public Dictionary<int, int> WarehouseProducts { get; }

        public Action<int> ProductSelectedCallback { get; }

        public WarehouseSelectionPopupSettings(Dictionary<int, int> warehouseProducts, Action<int> productSelectedCallback)
        {
            ProductSelectedCallback = productSelectedCallback;
            WarehouseProducts = warehouseProducts;
        }
    }

    public sealed class WarehouseSelectionProductModel
    {
        public ReactiveProperty<int> ProductId = new ReactiveProperty<int>();
        public ReactiveProperty<int> ProductCount = new ReactiveProperty<int>();
        public ReactiveProperty<Sprite> ProductSprite = new ReactiveProperty<Sprite>();
        public ReactiveProperty<bool> Selected = new ReactiveProperty<bool>();

        /// <summary>
        /// Calls when user select item.
        /// </summary>
        public ReactiveCommand Clicked = new ReactiveCommand();
    }
}



