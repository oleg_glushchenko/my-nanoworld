﻿using Assets.NanoLib.UI.Core.Signals;
using Assets.NanoLib.UI.Core.Views;
using Assets.Scripts.Engine.UI.Views.Hud;
using DG.Tweening;
using NanoReality.Engine.UI.Views.ConstractConfirmPanel;
using NanoReality.Game.Tutorial;
using NanoReality.Game.Tutorial.HintTutorial;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityMap.Controllers;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using NanoReality.StrangeIoC;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Engine.UI.Views.Hud
{
    public sealed class HudRoadPanelView : AViewMediator
    {
        [Inject] public PlayerOpenedPanelSignal jSignalPlayerOpenedPanel { get; set; }

        [Inject] public SignalOnCancelConstruct jSignalOnCancelConstruct { get; set; }

        [Inject] public SignalOnConfirmConstructSucceed jSignalOnConfirmConstructSucceed { get; set; }

        [Inject] public SignalOnRoadControlsSetVisible jSignalOnRoadControlsSetVisible { get; set; }

        [Inject] public RoadConstructSetActiveSignal jRoadConstructSetActiveSignal { get; set; }

        [Inject] public IConstructionController jConstructionController { get; set; }
        [Inject] public IHintTutorial jHintTutorial { get; set; }
        [Inject] public IHardTutorial jHardTutorial { get; set; }

        [Inject] public SignalOnCancelConstractWithoutExit jSignalOnCancelConstractWithoutExit { get; set; }
        [Inject] public OnHudBottomPanelWithoutPanelVisibleSignal jOnHudBottomPanelWithoutPanelVisibleSignal { get; set; }
        [Inject] public OnHudButtonsIsClickableSignal jOnHudButtonsIsClickableSignal { get; set; }
        [Inject] public SignalOnShownPanel jUiPanelOpenedSignal { get; set; }
        
        #region RoadSection

        [SerializeField] private Button _openConstructRoadPanelButton;
        [SerializeField] private Button _closeConstructRoadPanelButton;

        [SerializeField] private Button _constuctRoadButton;
        [SerializeField] private Button _destoryRoadButton;

        [SerializeField] private GameObject _gameObjectParticlesRoad;

        [SerializeField] private GameObject _roadContainer;

        [SerializeField] private Image _addRoadImage;
        [SerializeField] private Image _removeRoadImage;

        [SerializeField] private Material _uiDefaultSpriteMaterial;
        [SerializeField] private Material _uiGrayscaleMaterial;

        [SerializeField] private GameObject _hiddingButtonForRoad;

        [SerializeField] private Button _overlayHideButton;

        #endregion

        public Button CloseConstructRoadPanelButton => _closeConstructRoadPanelButton;

        public GameObject GameObjectParticlesRoad { get { return _gameObjectParticlesRoad; } }
        public Button OpenConstructRoadPanelButton { get { return _openConstructRoadPanelButton; } }
        public Button ConstuctRoadButton { get { return _constuctRoadButton; } }
        public Button DestoryRoadButton => _destoryRoadButton;

        private bool IsRoadContainerEnabled
        {
            get { return _isRoadContainerEnabled; }
            set
            {
                if (_isRoadContainerEnabled != value)
                    jSignalOnRoadControlsSetVisible.Dispatch(value);

                _isRoadContainerEnabled = value;
                _hiddingButtonForRoad.SetActive(value);

                if (!_isRoadContainerEnabled)
                {
                    SetImageButtonUntoggled(_addRoadImage);
                    SetImageButtonUntoggled(_removeRoadImage);
                }
            }
        }

        #region PrivateSection

        private bool _isRoadContainerEnabled;
        private bool _roadCreationEnabled;
        private bool _roadRemoveEnabled;
        private bool _roadBuildModeEnabled;
        private Tween _changeStateAnimation;
        private Tween _openConstructRoadAnimation;
        private bool _isClickLock;

        private Ease _animationEase = Ease.OutBounce;
        private float _animationDuration = .5f;
        
        private const float FinishPanelPosition = 10;
        private const float StartPanelPosition = 385; 

        #endregion

        private void CloseBuildMode()
        {
            if (IsRoadContainerEnabled)
            {
                OnCloseConstrcutPanelClicked();
            }
        }

        private void SetImageButtonToggled(Image img)
        {
            img.material = _uiGrayscaleMaterial;
            img.color = new Color(1f, 1f, 1f, 0.5f);
        }

        private void SetImageButtonUntoggled(Image img)
        {
            img.material = _uiDefaultSpriteMaterial;
            img.color = Color.white;
        }

        protected override void PreRegister()
        {

        }

        protected override void OnRegister()
        {
            jSignalPlayerOpenedPanel.AddListener(CloseBuildMode);
            jSignalOnCancelConstruct.AddListener(OnCloseConstrcutPanelClicked);

            jSignalOnConfirmConstructSucceed.AddListener(OnCloseConstrcutPanelClicked);

            jSignalOnCancelConstractWithoutExit.AddListener(CancelConstructionWithoutModeChange);

            _constuctRoadButton.onClick.AddListener(OnConstructRoadClicked);
            _destoryRoadButton.onClick.AddListener(OnDestroyRoadClicked);
            
            _overlayHideButton.onClick.AddListener(OnCloseConstrcutPanelClicked);

            _openConstructRoadPanelButton.onClick.AddListener(OnOpenConstuctPanelClicked);
            _closeConstructRoadPanelButton.onClick.AddListener(OnCloseConstrcutPanelClicked);
            jUiPanelOpenedSignal.AddListener(ForceHidePanel);
        }

        protected override void OnRemove()
        {
            jUiPanelOpenedSignal.RemoveListener(ForceHidePanel);
            _constuctRoadButton.onClick.RemoveListener(OnConstructRoadClicked);
            _destoryRoadButton.onClick.RemoveListener(OnDestroyRoadClicked);

            _openConstructRoadPanelButton.onClick.RemoveListener(OnOpenConstuctPanelClicked);
            _closeConstructRoadPanelButton.onClick.RemoveListener(OnCloseConstrcutPanelClicked);

            jSignalOnCancelConstractWithoutExit.RemoveListener(CancelConstructionWithoutModeChange);
        }
        
        private void OnDisable()
        {
            CloseConstruct();
        }

        #region RoadSection

        private void OnCloseConstrcutPanelClicked()
        {
            if (_isClickLock) return;

            jOnHudButtonsIsClickableSignal.Dispatch(true);
            jOnHudBottomPanelWithoutPanelVisibleSignal.Dispatch(HudType.Roads, true);
            jConstructionController.CancelChangeObject();
            CloseConstruct();
        }

        private void ForceHidePanel(UIPanelView _)
        {
            if (!_roadContainer.activeInHierarchy || !jHardTutorial.IsTutorialCompleted || jHintTutorial.IsHintTutorialActive ||  _roadRemoveEnabled || _roadCreationEnabled) return;
            
            jOnHudButtonsIsClickableSignal.Dispatch(true);
            jOnHudBottomPanelWithoutPanelVisibleSignal.Dispatch(HudType.Roads, true);
            jConstructionController.CancelChangeObject();
            ClosePanel();
        }

        private void ClosePanel()
        {
            IsRoadContainerEnabled = false;
            
            var rectTransform = _roadContainer.transform as RectTransform;
           
            var stop = new Vector2(FinishPanelPosition, rectTransform.sizeDelta.y);
            var start = new Vector2(StartPanelPosition, rectTransform.sizeDelta.y);

            DoAnimateSizeDelta(rectTransform, start, stop, () =>
            {
                _roadContainer.SetActive(false);
                _overlayHideButton.gameObject.SetActive(false);
            });
        } 
        
        private void CloseConstruct()
        {
            IsRoadContainerEnabled = false;
            
            var rectTransform = _roadContainer.transform as RectTransform;

            var stop = new Vector2(FinishPanelPosition, rectTransform.sizeDelta.y);
            var start = new Vector2(StartPanelPosition, rectTransform.sizeDelta.y);

            DoAnimateSizeDelta(rectTransform, start, stop, () =>
            {
                _roadContainer.SetActive(false);
                _overlayHideButton.gameObject.SetActive(false);
            });

            if (_roadRemoveEnabled)
            {
                _roadRemoveEnabled = false;
                _roadBuildModeEnabled = false;
                jRoadConstructSetActiveSignal.Dispatch(false);
            }
            else if (_roadCreationEnabled)
            {
                _roadCreationEnabled = false;
                jRoadConstructSetActiveSignal.Dispatch(false);
            }
        }

        private void OnOpenConstuctPanelClicked()
        {
            if (_isClickLock) return;
            
            jOnHudButtonsIsClickableSignal.Dispatch(false);
            jOnHudBottomPanelWithoutPanelVisibleSignal.Dispatch(HudType.Roads, false);
            IsRoadContainerEnabled = true;
            _overlayHideButton.gameObject.SetActive(true);
            _roadContainer.SetActive(IsRoadContainerEnabled);

            var rectTransform = _roadContainer.transform as RectTransform;

            var start = new Vector2(FinishPanelPosition, rectTransform.sizeDelta.y);
            var stop = new Vector2(StartPanelPosition, rectTransform.sizeDelta.y);

            DoAnimateSizeDelta(rectTransform, start, stop);
        }

        private void DoAnimateSizeDelta(RectTransform target, Vector2 start, Vector2 stop, TweenCallback action = null)
        {
            _isClickLock = true;
            target.sizeDelta = start;
            _changeStateAnimation = target.DOSizeDelta(stop, _animationDuration).SetEase(_animationEase).OnComplete(() =>
            {
                _isClickLock = false;
                action?.Invoke();
            });
        }

        private void OnConstructRoadClicked()
        {
            _overlayHideButton.gameObject.SetActive(false);

            if (_roadRemoveEnabled)
            {
                _roadBuildModeEnabled = false;
                _roadRemoveEnabled = false;
            }

            _roadCreationEnabled = !_roadCreationEnabled;
            
            jRoadConstructSetActiveSignal.Dispatch(_roadCreationEnabled);
            jRoadConstructSetActiveSignal.AddOnce(isActive =>
            {
                if (isActive) return;
                
                _roadBuildModeEnabled = false;
                _roadRemoveEnabled = false;
                _roadCreationEnabled = false;
            });

            if (_roadCreationEnabled)
            {
                jConstructionController.CreateMapObject(31, BusinessSectorsTypes.Town);

                SetImageButtonUntoggled(_removeRoadImage);
                SetImageButtonToggled(_addRoadImage);
            }
            else
            {
                _overlayHideButton.gameObject.SetActive(true);
                jConstructionController.CancelChangeObject();
                SetImageButtonUntoggled(_addRoadImage);
            }
            
            _hiddingButtonForRoad.SetActive(!_roadCreationEnabled);
            jOnHudButtonsIsClickableSignal.Dispatch(false);
            jOnHudBottomPanelWithoutPanelVisibleSignal.Dispatch(HudType.Roads, false);
        }

        private void OnDestroyRoadClicked()
        {
            _overlayHideButton.gameObject.SetActive(false);

            if (_roadCreationEnabled)
            {
                _roadCreationEnabled = false;
                jConstructionController.CancelChangeObject();

                SetImageButtonUntoggled(_addRoadImage);
            }

            _roadRemoveEnabled = !_roadRemoveEnabled;
            _roadBuildModeEnabled = _roadRemoveEnabled;
            _hiddingButtonForRoad.SetActive(!_roadRemoveEnabled);
            jRoadConstructSetActiveSignal.Dispatch(_roadRemoveEnabled);

            jRoadConstructSetActiveSignal.AddOnce((b) =>
            {
                if (!b)
                {
                    _roadBuildModeEnabled = false;
                    _roadRemoveEnabled = false;
                    _roadCreationEnabled = false;
                }
            });

            if (_roadRemoveEnabled)
            {
                SetImageButtonToggled(_removeRoadImage);
            }
            else
            {
                _overlayHideButton.gameObject.SetActive(true);
                SetImageButtonUntoggled(_removeRoadImage);
            }
            
            jOnHudButtonsIsClickableSignal.Dispatch(false);
            jOnHudBottomPanelWithoutPanelVisibleSignal.Dispatch(HudType.Roads, false);
        }

        #endregion

        private void CancelConstructionWithoutModeChange()
        {
            if (!_roadRemoveEnabled)
            {
                jRoadConstructSetActiveSignal.Dispatch(false);
                OnConstructRoadClicked();
            }
            else
            {
                jRoadConstructSetActiveSignal.Dispatch(false);
                OnDestroyRoadClicked();
            }
        }

        public void PlayOpenRoadConstructButtonAnimation(bool isPlay)
        {
            if (_openConstructRoadAnimation != null)
            {
                _openConstructRoadAnimation.Kill(true);
            }

            if (!isPlay)
            {
                return;
            }

            Vector2 punchScale = new Vector2(0.1f, 0.1f);
            float duration = 1.0f;
            int vibrato = 1;
            float elasticity = 0.0f;

            _openConstructRoadAnimation = _openConstructRoadPanelButton.transform.DOPunchScale(punchScale, duration, vibrato, elasticity);
        }
    }

    public sealed class HudRoadPanelMediator : AMediator<HudRoadPanelView>
    {
        public override void PreRegister()
        {
        }

        public override void OnRegister()
        {

        }

        public override void OnRemove()
        {
        }
    }
}