﻿using Assets.Scripts.GameLogic.Utilites;
using Assets.Scripts.MultiLanguageSystem.Logic;
using DG.Tweening;
using TMPro;
using UnityEngine;

namespace NanoReality.Engine.UI.Extensions.MainPanel
{
    public class HudViewTooltip : MonoBehaviour
    {

        [SerializeField] private TextMeshProUGUI _text;
        
        private RectTransform _rectTransform;
        private CanvasGroup _canvasGroup;
        private Tweener _tweener;
        private Tweener _secondaryTweener;

        private string _unlockLevel;

        private void Start()
        {
            _rectTransform = GetComponent<RectTransform>();
            _canvasGroup = GetComponent<CanvasGroup>();
            LocalizationMLS.Instance.OnChangedLanguage += OnLanguageChange;
            OnLanguageChange();
        }

        public void AnimationIn()
        {
            if (_tweener != null && _tweener.active || _secondaryTweener != null && _secondaryTweener.active)
            {
                return;
            }

            _canvasGroup.alpha = 1;
            _tweener = _rectTransform.DOScale(Vector3.zero, 0.16f).From().OnComplete(() =>
            {
                _secondaryTweener = _canvasGroup.DOFade(0, 0.1f).SetDelay(1f);
            });
        }

        public void SetUnlockLevel(string unlockLevel)
        {
            _unlockLevel = unlockLevel;
        }

        private void OnLanguageChange()
        {
            _text.SetLocalizedText(string.Format(LocalizationMLS.Instance.GetText(LocalizationKeyConstants.HINT_UNLOCK_SECTOR_LEVEL), _unlockLevel));
        }
    }
}