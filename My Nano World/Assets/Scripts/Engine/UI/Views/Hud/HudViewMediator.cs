﻿using System;
using Assets.NanoLib.UI.Core;
using Assets.Scripts.Engine.UI.Views.Hud;
using NanoLib.Core;
using NanoReality.Core.SectorLocks;
using UniRx;

namespace NanoReality.Engine.UI.Extensions.MainPanel
{
    public class HudViewMediator : UIMediator<HudView>, IObserver
    {
        [Inject] public HudPanelModel jPanelModel { get; set; }
        [Inject] public FarmState FarmState { get; set; }

        private IDisposable _farmLockedState;
        private IDisposable _farmUnlockLevel;

        [PostConstruct]
        public void OnPostConstruct()
        {
            jPanelModel.AddListener(this);
        }
        
        [Deconstruct]
        public void OnDeconstruct()
        {
            jPanelModel.RemoveListener(this);
            _farmLockedState?.Dispose();
            _farmUnlockLevel?.Dispose();
        }

        protected override void OnShown()
        {
            _farmLockedState = FarmState.FarmLocked.Subscribe(value => { View.HudBuildingsPanel.FarmLocked = value; });
            _farmUnlockLevel = FarmState.UnlockLevel.Subscribe(unlockLevel => { View.HudBuildingsPanel.SetUnlockGetter(unlockLevel.ToString()); });

            SetSubPanelsActive();
        }

        private void SetSubPanelsActive()
        {
            View.HudRoadPanel.gameObject.SetActive(jPanelModel.RoadPanelModel.IsActive);
            View.HudBuildingsPanel.gameObject.SetActive(jPanelModel.BuildingsPanelModel.IsActive);
            View.HudQuestsPanel.gameObject.SetActive(jPanelModel.QuestsPanelModel.IsActive);
            View.SettingsButton.gameObject.SetActive(jPanelModel.SettingsModel.IsActive);
            // View.EditButton.gameObject.SetActive(jPanelModel.EditModel.IsActive);
            // View.OffersButton.gameObject.SetActive(jPanelModel.OffersModel.IsActive);
            View.TheEventButton.gameObject.SetActive(jPanelModel.TheEventModel.IsActive);
        }

        public void OnChanged()
        {
            if (View == null)
            {
                return;
            }

            SetSubPanelsActive();
        }
    }
}