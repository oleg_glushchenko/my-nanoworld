using TMPro;
using UnityEngine;

namespace Engine.UI.Views.Hud
{
    public interface IUISlider
    {
        void UpdateSlider(int current, int max);
    }

    public class UISlider : MonoBehaviour, IUISlider
    {
        [SerializeField]
        private HorizontalProgressBar _slider;
        [SerializeField]
        private TextMeshProUGUI _counter;
        
        public void UpdateSlider(int current, int max)
        {
            _counter.text = max > 0 ? $"{current}/{max}" : current.ToString();
            _slider.FillAmount = (float)current / max;
        }
    }
}