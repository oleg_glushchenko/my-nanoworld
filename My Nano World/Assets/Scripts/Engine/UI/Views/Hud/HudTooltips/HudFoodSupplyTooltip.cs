﻿using System.Collections.Generic;
using System.Linq;
using Assets.NanoLib.UI.Core.Views;
using Assets.NanoLib.UtilitesEngine;
using Engine.UI.Components;
using I2.Loc;
using NanoLib.Core.Logging;
using NanoLib.Core.Services.Sound;
using NanoLib.Core.Timers;
using NanoReality.Game.CityStatusEffects;
using NanoReality.Game.FoodSupply;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Game.Ui.Hud
{
    public class HudFoodSupplyTooltip : UIPanelView
    {
        [SerializeField] private RectTransform _nextZoneLeftTimePanel;
        [SerializeField] private TextMeshProUGUI _nextZoneLeftTimeText;
        
        [SerializeField] private RectTransform _totalLeftTimePanel;
        [SerializeField] private TextMeshProUGUI _totalLeftTimeText;
        [SerializeField] private RectTransform _progressPointerTransform;
        [SerializeField] private RectTransform _progressPointerBoundsTransform;
        [SerializeField] private List<TooltipStatusEffectItem> _statusEffectItems;
        [SerializeField] private ProgressBar _progressBar;

        [Inject] public IFoodSupplyService jFoodSupplyService { get; set; }
        [Inject] public ServerTimeTickSignal jServerTimeTickSignal { get; set; }
        [Inject] public ITimerManager jTimerManager { get; set; }
        [Inject] public ICityStatusEffectService jStatusEffectService { get; set; }
        [Inject] public ISoundManager jSoundManager { get; set; }

        public override void Show()
        {
            base.Show();
            OnServerTimeTick(jTimerManager.CurrentUTC);

            jStatusEffectService.StatusEffectsChanged += UpdateStatusEffectsItems;
            jFoodSupplyService.FoodSupplyAmountChanged += OnFoodSupplyAmountChanged;
            jServerTimeTickSignal.AddListener(OnServerTimeTick);
            jSoundManager.Play(_progressBar.Value >= 0.5 ? SfxSoundTypes.HappinessBarMore50 : SfxSoundTypes.HappinessBarLess50, SoundChannel.SoundFX);

            UpdateView();
        }

        public override void Hide()
        {
            base.Hide();
            jStatusEffectService.StatusEffectsChanged -= UpdateStatusEffectsItems;
            jFoodSupplyService.FoodSupplyAmountChanged -= OnFoodSupplyAmountChanged;
            jServerTimeTickSignal.RemoveListener(OnServerTimeTick);
        }

        private void UpdateView()
        {
            UpdateStatusEffectsItems();
            OnFoodSupplyAmountChanged(jFoodSupplyService.ProgressBarAmount);
        }

        private void OnServerTimeTick(long serverTime)
        {
            var totalLeftTime = Mathf.Clamp(jFoodSupplyService.StatusEffectTotalTime, 0, float.MaxValue);
            var nextZoneLeftTime = Mathf.Clamp(jFoodSupplyService.StatusEffectEndTimeStamp - serverTime, 0f, float.MaxValue);
            
            // TODO: change time format to HH:mm.
            _nextZoneLeftTimeText.text = UiUtilities.GetFormattedTimeFromSeconds_H_M_S(nextZoneLeftTime);
            _totalLeftTimeText.text = UiUtilities.GetFormattedTimeFromSeconds_H_M_S(totalLeftTime);

            var isNextZoneTimeVisible = nextZoneLeftTime > 0;
            _nextZoneLeftTimePanel.gameObject.SetActive(isNextZoneTimeVisible);
            
            LayoutRebuilder.ForceRebuildLayoutImmediate(_nextZoneLeftTimePanel);
            LayoutRebuilder.ForceRebuildLayoutImmediate(_totalLeftTimePanel);
        }

        private void OnFoodSupplyAmountChanged(float amount)
        {
            var pointerPosition = _progressPointerTransform.anchoredPosition;
            pointerPosition.x = _progressPointerBoundsTransform.rect.width * amount;
            _progressPointerTransform.anchoredPosition = pointerPosition;
        }

        private void UpdateStatusEffectsItems()
        {
            var activeStatusEffects = jStatusEffectService.ActiveStatusEffects;

            var lastActiveViewIndex = 0;
            foreach (var statusEffect in activeStatusEffects)
            {
                var itemView = _statusEffectItems[lastActiveViewIndex];
                var statusEffectId = statusEffect.EffectId;
                var effectSettings = jStatusEffectService.GetStatusEffectSettings(statusEffectId);

                var effectPercents = 100 * effectSettings.EffectValue;
                var effectValueText = string.Empty;
                var textColor = Color.black;
                switch (effectSettings.EffectInfluence)
                {
                    case StatusEffectInfluence.Increase:
                        effectValueText = $"+{effectPercents}%";
                        textColor = Color.green;
                        break;
                    
                    case StatusEffectInfluence.Decrease:
                        effectValueText = $"-{effectPercents}%";
                        textColor = Color.red;
                        break;
                    
                    case StatusEffectInfluence.Ability:
                        break;
                    
                    default:
                        $"Not valid status effect influence type [{effectSettings.EffectInfluence}]".LogWarning(LoggingChannel.Mechanics);
                        break;
                }

                itemView.StatusEffectValueText.color = textColor;
                itemView.StatusEffectValueText.text = effectValueText;
                itemView.StatusEffectName.text = LocalizationManager.GetTranslation($"UICommon/STATUS_EFFECT_{(int)effectSettings.EffectType}");
                itemView.Show();
                lastActiveViewIndex++;
            }

            for (var viewIndex = lastActiveViewIndex; viewIndex < _statusEffectItems.Count; viewIndex++)
            {
                _statusEffectItems[viewIndex].Hide();
            }
            
            LayoutRebuilder.ForceRebuildLayoutImmediate(_statusEffectItems.First().transform.parent as RectTransform);
        }
    }
}