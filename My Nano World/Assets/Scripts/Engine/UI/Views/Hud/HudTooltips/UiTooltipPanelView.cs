﻿using Assets.NanoLib.UI.Core.Views;
using NanoLib.Services.InputService;

namespace NanoReality.Game.Ui.Hud
{
	public class UiTooltipPanelView : UIPanelView 
	{
		[Inject] public SignalOnBeginTouch jSignalOnBeginTouch { get; set; }
		
		public bool IsClosing = true;
		
		public override void Show()
		{
			base.Show();
			if (!Visible)
			{		
				Visible = true;
				OnShown.Dispatch(this);
			}
		
			jSignalOnBeginTouch.AddListener(Hide);
		}
		
		public override void Hide()
		{
			base.Hide();

			jSignalOnBeginTouch.RemoveListener(Hide);
        
			if (Visible)
			{
				Visible = false;
				OnHide.Dispatch(this);
			}
		}
	}
}