﻿using UnityEngine;
using NanoReality.GameLogic.BuildingSystem.Happiness;
using System.Collections.Generic;
using GameLogic.Taxes.Service;
using NanoLib.Core.Services.Sound;
using NanoReality.Engine.UI.Extensions.MapObjects.CityHallTaxes;
using TMPro;

namespace NanoReality.Game.Ui.Hud
{
	public class HudTooltipSatisfaction : UiTooltipPanelView
    {
        [SerializeField] private List<HappyColumn> _happinessColumns;

        [Inject] public ISoundManager jSoundManager { get; set; }
        [Inject] public IHappinessData jHappinessData { get; set; }
        [Inject] public ICityTaxesService jCityTaxesService { get; set; }

        public sealed override void Show()
		{
			base.Show();
            int satisfaction = jHappinessData.TotalHappinessPercent;
            jSoundManager.Play(satisfaction >= 50 ? SfxSoundTypes.HappinessBarMore50 : SfxSoundTypes.HappinessBarLess50, SoundChannel.SoundFX);

            UpdateToolTip();
        }
        
        public void UpdateToolTip()
        {
            //выставляем минимальные и максимальные граници для колонок
            _happinessColumns[0].SetMinBound(0);
            _happinessColumns[_happinessColumns.Count - 1].SetMaxBound(100);

            var satisfaction = jHappinessData.TotalHappinessPercent;
            foreach(var column in _happinessColumns)
            {
                column.SetColumn(satisfaction);
            }
        }
    }
}