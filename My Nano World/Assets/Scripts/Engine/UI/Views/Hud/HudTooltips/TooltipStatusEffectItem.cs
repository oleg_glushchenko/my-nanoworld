using NanoReality.Game.CityStatusEffects;
using strange.extensions.mediation.impl;
using TMPro;
using UnityEngine;

namespace NanoReality.Game.Ui.Hud
{
    public sealed class TooltipStatusEffectItem : View
    {
        [SerializeField] private TextMeshProUGUI _statusEffectName;
        [SerializeField] private TextMeshProUGUI _statusEffectValueText;

        public TextMeshProUGUI StatusEffectName => _statusEffectName;

        public TextMeshProUGUI StatusEffectValueText => _statusEffectValueText;

        public void Show()
        {
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}