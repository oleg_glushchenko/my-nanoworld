﻿using System.Collections.Generic;
using Assets.NanoLib.UI.Core.Views;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.Constants;
using NanoReality.GameLogic.Data;
using NanoReality.GameLogic.Services;
using TMPro;
using UnityEngine;

namespace NanoReality.Game.UI
{
	public class HudRewardsTooltip : UIPanelView
	{
		[SerializeField] private TMP_Text _title;
		[SerializeField] private Transform _gridTransform;
		[SerializeField] private RewardTooltipItem _prefab;
		
		[Inject] public IIconProvider jIconProvider { get; set; }

		private readonly List<RewardTooltipItem> _items = new List<RewardTooltipItem>();

		public string Title
		{
			set => _title.text = value;
		}

		public override void Show()
		{
			gameObject.SetActive(true);
			
			base.Show();
		}
		
		public override void Hide() //Calling from the Unity
		{
			Clear();
			
			base.Hide();
			
			gameObject.SetActive(false);
		}

		public void SetRewards(Reward rewards)
		{
			Clear();
			
			if (rewards.Type.HasFlag(ResourcesType.Soft))
			{
				var rewardGameObject = GetItemGameObject();
				rewardGameObject.Initialize(jIconProvider.GetCurrencySprite(PriceType.Soft), rewards.softCurrencyCount);
			}

			if (rewards.Type.HasFlag(ResourcesType.Hard))
			{
				var rewardGameObject = GetItemGameObject();
				rewardGameObject.Initialize(jIconProvider.GetCurrencySprite(PriceType.Hard), rewards.hardCurrencyCount);
			}

			if (rewards.Type.HasFlag(ResourcesType.Materials))
			{
				foreach (var parsedReward in rewards.GetParsedRewards())
				{
					var rewardGameObject = GetItemGameObject();
					var sprite = jIconProvider.GetProductSprite(parsedReward.Key);
					rewardGameObject.Initialize(sprite, parsedReward.Value);
				}
			}
		}

		private RewardTooltipItem GetItemGameObject()
		{
			var rewardGameObject = Instantiate(_prefab, _gridTransform);
			_items.Add(rewardGameObject);

			return rewardGameObject;
		}

		private void Clear()
		{
			foreach (var item in _items)
			{
				Destroy(item.gameObject);
			}
			
			_items.Clear();
		}
	}
}