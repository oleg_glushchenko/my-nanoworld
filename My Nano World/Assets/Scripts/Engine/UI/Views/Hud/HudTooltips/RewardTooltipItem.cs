using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Game.UI
{
	public class RewardTooltipItem : MonoBehaviour
	{
		[SerializeField] private Image _icon;
		[SerializeField] private TMP_Text _count;

		public void Initialize(Sprite sprite, int count)
		{
			_icon.sprite = sprite;
			_count.text = "x" + count;
		}
	}
}