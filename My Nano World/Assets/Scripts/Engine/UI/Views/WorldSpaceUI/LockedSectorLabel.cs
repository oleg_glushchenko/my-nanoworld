﻿using System;
using Assets.NanoLib.Utilities.Pulls; 
using Assets.Scripts.Engine.BuildingSystem.CityMap;
using NanoLib.Core.Services.Sound;
using NanoReality.Engine.UI.Components;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Impl;
using UnityEngine; 
using UnityEngine.UI;
using strange.extensions.mediation.impl;

#pragma warning disable 649 

namespace NanoReality.Engine.UI.Views.WorldSpaceUI 
{
	public class LockedSectorLabel : View, IPullableObject 
	{
		[SerializeField] private TooltipSettings _hintPrefub;
		[SerializeField] private Transform _hintHolder;
		[SerializeField] private Image _lockedTexture;
		[SerializeField] private GameObject _lockRendererObj;
		[SerializeField] private SubSectorLooker _looker;
		[Inject] public ISoundManager jSoundManager { get; set; }

		public event Action<CityLockedSector> OnClick;
		private CityLockedSector _model;

		private const int SizeDeltaScale = 100;

        private readonly Quaternion _rotation = Quaternion.Euler(new Vector3(90, 45, 0));

        public void SetTarget(CityLockedSector lockedSector, LockedSectorView target)
        {
	        _model = lockedSector;
	        
	        _looker.SetTarget(target);
	        _looker.enabled = true;
			
			var rect = (RectTransform)_lockedTexture.transform;
            rect.sizeDelta = SizeDeltaScale * target.GridDimensions;
			rect.rotation = _rotation;

			UpdateView();
        }
        public void UpdateView()
        {
	        _lockRendererObj.SetActive(false);
        }

        public void ShowBlockTooltip()
	    {
		    jSoundManager.Play(SfxSoundTypes.TapEarth, SoundChannel.SoundFX);
		    Transform tr = Instantiate(_hintPrefub.gameObject).transform;
		    tr.SetParent(_hintHolder);
		    tr.GetComponent<TooltipSettings>().Init();
	    }

	    public void OnLockClick()
	    {
		    OnClick?.Invoke(_model);
	    }
	    
	    #region Pullable 
		public object Clone() 
		{ 
			return Instantiate(this); 
		} 

		public IObjectsPull pullSource { get; set; } 
		public void OnPop() 
		{ 
            gameObject.SetActive(true);
			enabled = true; 
		} 

		public void OnPush() 
		{
			enabled = false;
			_looker.SetTarget(null);
			_looker.enabled = false;
            gameObject.SetActive(false);
            OnClick = null;
		} 

		public void FreeObject()
		{
			pullSource?.PushInstance(this);
		} 

		public void DestroyObject() 
		{ 
			Destroy(this); 
		} 
		#endregion
	} 
}