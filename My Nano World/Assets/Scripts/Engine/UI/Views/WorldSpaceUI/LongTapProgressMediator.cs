using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using NanoLib.Services.InputService;
using NanoReality.Engine.UI;
using NanoReality.Engine.UI.Components;
using NanoReality.Game.Data;
using strange.extensions.mediation.impl;

public class LongTapProgressMediator : Mediator
{
    #region Injections
    [Inject] public SignalOnStartLongTapMapObjectView jSignalOnStartLongTapMapObjectView { get; set; }
    [Inject] public SignalOnEndLongTapMapObjectView jSignalOnEndLongTapMapObjectView { get; set; }
    [Inject] public LongTapProgressView jView { get; set; }
    [Inject] public IWorldSpaceCanvas jWorldSpaceCanvas { get; set; }
    [Inject] public IBuildSettings jBuildSettings { get; set; }
    #endregion

    private MapObjectLooker _objLocker;
    private int _fillTime;

    #region Construct implementation
    [PostConstruct]
    public void PostConstruct()
    {
        jSignalOnStartLongTapMapObjectView.AddListener(OnStartLongPress);
        jSignalOnEndLongTapMapObjectView.AddListener(OnEndLongPress);
        _objLocker = jView.GetComponent<MapObjectLooker>();
        _fillTime = jBuildSettings.LongTouchDuration - jBuildSettings.LongTouchProgressDelay;
    }

    [Deconstruct]
    public void Deconstruct()
    {
        jSignalOnStartLongTapMapObjectView.RemoveListener(OnStartLongPress);
        jSignalOnEndLongTapMapObjectView.RemoveListener(OnEndLongPress);
    }
    #endregion

    private void OnStartLongPress(MapObjectView target)
    {
        _objLocker.SetTarget(target);
        jView.FIll(_fillTime, OnFillCompleted);
    }

    private void OnFillCompleted()
    {
        jWorldSpaceCanvas.PushLongTapProgress(jView);
    }

    private void OnEndLongPress()
    {
        jView.StopFillAnimation();
        jWorldSpaceCanvas.PushLongTapProgress(jView);
    }
}
