﻿using Assets.NanoLib.Utilities;
using Assets.NanoLib.Utilities.Pulls;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using NanoReality.Engine.UI.Components;
using strange.extensions.mediation.impl;
using TMPro;
using UnityEngine;

namespace Assets.Scripts.Engine.UI.Views.WorldSpaceUI
{
    [RequireComponent(typeof(MapObjectLooker))]
    public class PopulationInfoView : View, IPullableObject
    {
        [SerializeField] private TextMeshProUGUI _counterLabel;

        public MapObjectLooker MapObjectLooker { get; private set; }

        protected override void Awake()
        {
            base.Awake();
            MapObjectLooker = GetComponent<MapObjectLooker>();
        }

        public void ShowIcon(MapObjectView target, int populationCount)
        {
            MapObjectLooker.SetTarget(target);
            gameObject.SetActive(true);
            _counterLabel.text = "+" + populationCount;
        }

        #region Pullable

        public object Clone()
        {
            return Instantiate(this);
        }

        public IObjectsPull pullSource { get; set; }

        public void OnPop()
        {
            MapObjectLooker.enabled = true;

            gameObject.SetActive(true);
        }

        public void OnPush()
        {
            MapObjectLooker.enabled = false;
            MapObjectLooker.SetTarget(null);
            MapObjectLooker.TargetCustomRotation = Vector3.zero;

            gameObject.SetActive(false);
        }

        public void FreeObject()
        {
            if (pullSource != null)
                pullSource.PushInstance(this);
        }

        public void DestroyObject()
        {
            Destroy(this);
        }

        #endregion
    }
}