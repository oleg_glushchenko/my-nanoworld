﻿using UnityEngine;
using Assets.NanoLib.Utilities;
using Assets.NanoLib.Utilities.Pulls;
using Assets.Scripts.GameLogic.Utilites;
using NanoReality.Engine.UI.Components;
using strange.extensions.mediation.impl;

namespace NanoReality.Engine.UI.Views.WorldSpaceUI
{
    public class NonPlayableBuildUI : View, IPullableObject
    {
        public CacheMonobehavior CacheMonobehavior
        {
            get { return _cacheMonobehavior ?? (_cacheMonobehavior = new CacheMonobehavior(this)); }
        }
        private CacheMonobehavior _cacheMonobehavior;

        /// <summary>
        /// префаб всплывающей подсказки
        /// </summary>
        [SerializeField] private TooltipSettings _hintPrefub;
        /// <summary>
        /// контейнер для показа всплывающей подсказки
        /// </summary>
        [SerializeField] private Transform _hintHolder;

        /// <summary>
        /// Начальная инициализация обьекта
        /// </summary>
        public void Init(NonPlayableBuildingView target)
        {
            NonPlayableBuildingLooker looker = CacheMonobehavior.GetCacheComponent<NonPlayableBuildingLooker>();
            looker.Target = target;
            looker.enabled = true;

            Transform tr = Instantiate(_hintPrefub.gameObject).transform;
            tr.SetParent(_hintHolder);
            TooltipSettings textTooltip = tr.GetComponent<TooltipSettings>();
            textTooltip.Init(LocalizationMLS.Instance.GetText(LocalizationKeyConstants.NONPLAYABLEBUILD_COMING_SOON));
            textTooltip.OnAnimationEndSignal.AddListener(FreeObject);
        }

        #region Pullable

        public object Clone()
        {
            return Instantiate(this);
        }

        public IObjectsPull pullSource { get; set; }

        public void OnPop()
        {
            gameObject.SetActive(true);
            enabled = true;
        }

        public void OnPush()
        {
            enabled = false;
            var looker = CacheMonobehavior.GetCacheComponent<NonPlayableBuildingLooker>();
            looker.Target = null;
            looker.enabled = false;
            gameObject.SetActive(false);
        }

        public void FreeObject()
        {
            if (pullSource != null)
                pullSource.PushInstance(this);
        }

        public void DestroyObject()
        {
            Destroy(this);
        }

        #endregion
    }
}