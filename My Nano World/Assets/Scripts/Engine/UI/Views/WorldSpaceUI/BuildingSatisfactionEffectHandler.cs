using System;
using System.Collections;
using System.Collections.Generic;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.ServiceBuildings;
using strange.extensions.mediation.impl;
using strange.extensions.signal.impl;
using UnityEngine;

namespace NanoReality.Engine.UI.UIAnimations
{
    public class BuildingSatisfactionEffectHandler : MonoBehaviour
    {
        [SerializeField] private float _happyMinPeriod;
        [SerializeField] private float _happyMaxPeriod;
        [SerializeField] private float _unhappyMinPeriod;
        [SerializeField] private float _unhappyMaxPeriod;
        [SerializeField] private float _sadMinPeriod;
        [SerializeField] private float _sadMaxPeriod;

        [SerializeField] private List<BuildingSatisfactionChangeEffect> BuildingSatisfactionChangeEffects
            = new List<BuildingSatisfactionChangeEffect>();

        private Coroutine satisfactionCoroutine;
        
        public void Start()
        {
            satisfactionCoroutine = StartCoroutine(PlaySignalWithDelay(_happyMinPeriod,
                _happyMaxPeriod - _happyMinPeriod, SatisfactionState.Happy));
            satisfactionCoroutine = StartCoroutine(PlaySignalWithDelay(_unhappyMinPeriod,
                _unhappyMaxPeriod - _unhappyMinPeriod, SatisfactionState.Unhappy));
            satisfactionCoroutine = StartCoroutine(PlaySignalWithDelay(_sadMinPeriod,
                _sadMaxPeriod - _sadMinPeriod, SatisfactionState.Sad));
        }

        public void AddBuildingSatisfactionEffect(BuildingSatisfactionChangeEffect effect)
        {
            if (!BuildingSatisfactionChangeEffects.Contains(effect))
            {
                BuildingSatisfactionChangeEffects.Add(effect);
            }
        }

        private IEnumerator PlaySignalWithDelay(float minPeriod, float delay, SatisfactionState state)
        {
            while (true)
            {
                yield return new WaitForSeconds(minPeriod);
                
                if (BuildingSatisfactionChangeEffects.Count == 0)
                {
                    continue;
                }
                foreach (var effect in BuildingSatisfactionChangeEffects)
                {
                    if (effect.gameObject.activeSelf)
                    {
                        effect.PlayEffectWithDelay(delay, state);
                    }
                }
                
                yield return new WaitForSeconds(delay);
            }
        }
    } 
}

