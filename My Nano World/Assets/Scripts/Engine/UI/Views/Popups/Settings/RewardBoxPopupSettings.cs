﻿using System;
using System.Collections.Generic;
using NanoReality.GameLogic.Quests;

namespace Assets.Scripts.Engine.UI.Views.Popups.Settings
{
    public class RewardBoxPopupSettings : BasePopupSettings
    {
        public List<IAwardAction> Awards { get; }
        public int GiftBoxType { get; }
        public bool IsEventReward { get; }

        public override Type PopupType => typeof(RewardBoxPopupView);

        public RewardBoxPopupSettings(List<IAwardAction> awards, int giftBoxType)
        {
            Awards = awards;
            GiftBoxType = giftBoxType;
        }

        public RewardBoxPopupSettings(List<IAwardAction> awards, int giftBoxType, bool isRewardType)
        {
            Awards = awards;
            GiftBoxType = giftBoxType;
            IsEventReward = isRewardType;
        }
    }
}