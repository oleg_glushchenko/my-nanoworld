﻿using System;
using System.IO;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;

namespace Assets.Scripts.Engine.UI.Views.Popups.Settings
{
    public class NotEnoughMoneyPopupSettings : BasePopupSettings
    {
        public PriceType PriceType { get; set; }
        
        public int Amount { get; set; }

        public override Type PopupType { get; } = typeof(NotEnoughMoneyPopupView);

        public NotEnoughMoneyPopupSettings(PriceType priceType, int amount)
        {
            PriceType = priceType;
            Amount = amount;
        }

        public NotEnoughMoneyPopupSettings(Price targetPrice)
        {
            PriceType = targetPrice.PriceType;
            switch (targetPrice.PriceType)
            {
                case PriceType.Soft:
                {
                    Amount = targetPrice.SoftPrice;
                    break;
                }
                case PriceType.Hard:
                {
                    Amount = targetPrice.HardPrice;
                    break;
                }
                case PriceType.Gold:
                {
                    Amount = targetPrice.GoldPrice;
                    break;
                }
                default:
                {
                    throw new InvalidDataException($"Not valid currency price type. {PriceType}");
                }
            }
        }
    }
}