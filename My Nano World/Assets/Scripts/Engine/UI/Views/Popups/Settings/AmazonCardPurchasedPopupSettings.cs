﻿using Assets.Scripts.GameLogic.Utilites;

namespace Assets.Scripts.Engine.UI.Views.Popups.Settings
{
    /// <summary>
    /// Настройки окна покупки амазон карты
    /// </summary>
    class AmazonCardPurchasedPopupSettings : BasePopupSettings
    {
        /// <summary>
        /// Заголовок окна
        /// </summary>
        public override string Title
        {
            get { return LocalizationMLS.Instance.GetText(LocalizationKeyConstants.SHARED_LABEL_SUCCESS); }
        }

        /// <summary>
        /// Сообщение окна
        /// </summary>
        public override string Message
        {
            get { return LocalizationMLS.Instance.GetText(LocalizationKeyConstants.AMAZONCARDPOPUP_MSG_PURCHASE_SUCCESS); }
        }
    }
}
