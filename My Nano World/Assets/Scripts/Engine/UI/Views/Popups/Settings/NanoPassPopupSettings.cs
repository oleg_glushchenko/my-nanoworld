using System;
using NanoReality.GameLogic.Bank;

namespace Assets.Scripts.Engine.UI.Views.Popups.Settings
{
    public class NanoPassPopupSettings : BasePopupSettings
    {
        public override Type PopupType => typeof(NanoPassPopupView);
        public Action OnSuccessfulPurchase; 
        public IBankProduct NanoPass;
        public IPurchaseProvider PurchaseProvider;
        public bool EnableBackground;

        public NanoPassPopupSettings(IBankProduct nanoPass, IPurchaseProvider provider)
        {
            NanoPass = nanoPass;
            PurchaseProvider = provider;
        }
    }
}

