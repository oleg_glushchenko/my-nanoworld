﻿using Assets.Scripts.GameLogic.Utilites;

namespace Assets.Scripts.Engine.UI.Views.Popups.Settings
{
    /// <summary>
    /// Настройки окна вылогинивания из соц.сети при игре на текущем прогрессе аккаунта
    /// </summary>
    public class AccountBindingKeepCurrentLogoutPopupSettings : BasePopupSettings
    {
        /// <summary>
         /// Название социальной сети
         /// </summary>
        private readonly string _socialName;


        /// <summary>
        /// Заголовок окна
        /// </summary>
        public override string Title { get { return LocalizationMLS.Instance.GetText(LocalizationKeyConstants.ACCOUNTBINDING_KEEP_CURRENT_LOGOUT_TITLE); } }

        /// <summary>
        /// Сообщение окна
        /// </summary>
        public override string Message
        {
            get { return string.Format(LocalizationMLS.Instance.GetText(LocalizationKeyConstants.ACCOUNTBINDING_KEEP_CURRENT_LOGOUT_TEXT), _socialName); }
        }


        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="socialName">Название социальной сети</param>
        public AccountBindingKeepCurrentLogoutPopupSettings(string socialName)
        {
            _socialName = socialName;
        }
    }
}