﻿using Assets.Scripts.GameLogic.Utilites;

namespace Assets.Scripts.Engine.UI.Views.Popups.Settings
{
    /// <summary>
    /// Настройки окна потери текущего аккаунта
    /// </summary>
    public class AccountBindingLostAnotherPopupSettings : BasePopupSettings
    {
        /// <summary>
        /// Заголовок окна
        /// </summary>
        public override string Title { get { return LocalizationMLS.Instance.GetText(LocalizationKeyConstants.ACCOUNTBINDING_LOST_ANOTHER_TITLE); } }

        /// <summary>
        /// Сообщение окна
        /// </summary>
        public override string Message
        {
            get { return LocalizationMLS.Instance.GetText(LocalizationKeyConstants.ACCOUNTBINDING_LOST_ANOTHER_TEXT); }
        }
    }
}