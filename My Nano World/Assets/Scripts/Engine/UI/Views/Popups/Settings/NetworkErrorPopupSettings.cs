﻿using System;

namespace Assets.Scripts.Engine.UI.Views.Popups.Settings
{
    public class NetworkErrorPopupSettings : BasePopupSettings
    {
        private readonly string _title;
        private readonly string _message;

        public readonly Action TryResendRequestCallback;
        public readonly Action ReloadApplicationCallback;
        
        public override string Title => _title;

        public override string Message => _message;
        
        public override Type PopupType => typeof(NetworkErrorPopup);

        public NetworkErrorPopupSettings(string title, string message, Action onResendRequest = null, Action onReloadApplicationCallback = null)
        {
            _title = title;
            _message = message;
            TryResendRequestCallback = onResendRequest;
            ReloadApplicationCallback = onReloadApplicationCallback;
        }
    }
}