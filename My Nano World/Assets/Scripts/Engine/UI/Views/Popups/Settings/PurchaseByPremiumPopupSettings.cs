﻿using System;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using strange.extensions.signal.impl;
using UniRx;
using UnityEngine;

namespace Assets.Scripts.Engine.UI.Views.Popups.Settings
{
    public class PurchaseByPremiumPopupSettings : BasePopupSettings
    {
        public override Type PopupType => typeof(PurchaseByPremiumPopupView);
        public readonly ReactiveProperty<int> Price = new ReactiveProperty<int>();
        public readonly PriceType PriceType;

        public Signal OnOperationNotActualSignal { get; }

        public PurchaseByPremiumPopupSettings(int hardCurrencyValue, Signal onOperationNotActualSignal = null)
        {
            Price.SetValueAndForceNotify(hardCurrencyValue);
            PriceType = PriceType.Hard;
            OnOperationNotActualSignal = onOperationNotActualSignal;
        }

        public PurchaseByPremiumPopupSettings(Price price, Signal onOperationNotActualSignal = null)
        {
            Price.SetValueAndForceNotify(price.GetPriceValue());
            PriceType = price.PriceType;
        }

        public void InvokePriceChangeAction(int price)
        {
            Price.SetValueAndForceNotify(price);
        }
    }
}
