﻿using System;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;

namespace Assets.Scripts.Engine.UI.Views.Popups.Settings
{
    public class UnlockAreaPopupSettings : BasePopupSettings
    {
        /// <summary>
        /// Тип окна
        /// </summary>
        public override Type PopupType
        {
            get { return typeof (UnlockAreaPopupView); }
        }

        /// <summary>
        /// Стоимость
        /// </summary>
        public Price Price { get; set; }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="price">Стоимость</param>
        public UnlockAreaPopupSettings(Price price)
        {
            Price = price;
        }
    }
}