﻿using Assets.Scripts.GameLogic.Utilites;

namespace Assets.Scripts.Engine.UI.Views.Popups.Settings
{
    /// <summary>
    /// Настройки окна о ошибке покупки на рынке
    /// </summary>
    public class TradePurchaseErrorPopupSettings : BasePopupSettings
    {
        /// <summary>
        /// Заголовок окна
        /// </summary>
        public override string Title { get { return LocalizationMLS.Instance.GetText(LocalizationKeyConstants.BUYERRORPOPUP_LABEL_SHOPPING_SYSTEM); } }

        /// <summary>
        /// Сообщение окна
        /// </summary>
        public override string Message { get { return LocalizationMLS.Instance.GetText(LocalizationKeyConstants.BUYERRORPOPUP_MSG_PRODUCT_UNAVAILABLE); } }
    }
}