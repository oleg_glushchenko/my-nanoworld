﻿using System;

namespace Assets.Scripts.Engine.UI.Views.Popups.Settings
{
    /// <summary>
    /// Настройки окна о оценивании игры
    /// </summary>
    public class RateUsPopupSettings : BasePopupSettings
    {
        /// <summary>
        /// Тип окна
        /// </summary>
        public override Type PopupType
        {
            get { return typeof(RateUsPopupView); }
        }
    }
}