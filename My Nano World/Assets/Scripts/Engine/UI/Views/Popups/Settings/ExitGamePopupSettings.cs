﻿using System;
using Assets.Scripts.GameLogic.Utilites;

namespace Assets.Scripts.Engine.UI.Views.Popups.Settings
{
    /// <summary>
    /// Настройки окна подтверждения закрытия игры
    /// </summary>
    class ExitGamePopupSettings : BasePopupSettings
    {
        /// <summary>
        /// Заголовок окна
        /// </summary>
        public override string Title
        {
            get { return LocalizationMLS.Instance.GetText(LocalizationKeyConstants.QUITGAMEPOPUP_LABEL_QUIT_GAME); }
        }

        /// <summary>
        /// Сообщение окна
        /// </summary>
        public override string Message
        {
            get { return LocalizationMLS.Instance.GetText(LocalizationKeyConstants.QUITGAMEPOPUP_MSG_REALLY_WANT_QUIT); }
        }

        /// <summary>
        /// Текст кнопки отмены
        /// </summary>
        public override string CancelButtonText
        {
            get { return LocalizationMLS.Instance.GetText(LocalizationKeyConstants.SHARED_BTN_CANCEL); }
        }

        /// <summary>
        /// Тип окна
        /// </summary>
        public override Type PopupType
        {
            get { return typeof(GameExitPopupView); }
        }
    }
}
