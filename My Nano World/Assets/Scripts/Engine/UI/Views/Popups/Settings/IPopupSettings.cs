﻿using System;

namespace Assets.Scripts.Engine.UI.Views.Popups.Settings
{
    /// <summary>
    /// Интерфейс настроек окна сообщения
    /// </summary>
    public interface IPopupSettings
    {
        /// <summary>
        /// Заголовок окна
        /// </summary>
        string Title { get; set; }

        /// <summary>
        /// Сообщение окна
        /// </summary>
        string Message { get; set; }

        /// <summary>
        /// Текст кнопки подтверждения
        /// </summary>
        string ConfirmButtonText { get; }

        /// <summary>
        /// Текст кнопки отмены
        /// </summary>
        string CancelButtonText { get; }

        /// <summary>
        /// Тип окна сообщения
        /// </summary>
        Type PopupType { get; }
    }
}