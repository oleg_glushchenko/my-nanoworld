﻿using Assets.Scripts.GameLogic.Utilites;

namespace Assets.Scripts.Engine.UI.Views.Popups.Settings
{
    /// <summary>
    /// Настройки окна успешной привязки аккаунта
    /// </summary>
    public class AccountBindingSuccessPopupSetting : BasePopupSettings
    {
        /// <summary>
        /// Название социальной сети
        /// </summary>
        private readonly string _socialName;


        /// <summary>
        /// Заголовок окна
        /// </summary>
        public override string Title { get { return LocalizationMLS.Instance.GetText(LocalizationKeyConstants.ACCOUNTBINDING_SUCCESS_LABEL); } }

        /// <summary>
        /// Сообщение окна
        /// </summary>
        public override string Message
        {
            get { return string.Format(LocalizationMLS.Instance.GetText(LocalizationKeyConstants.ACCOUNTBINDING_SUCCESS_TEXT), _socialName); }
        }


        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="socialName">Название социальной сети</param>
        public AccountBindingSuccessPopupSetting(string socialName)
        {
            _socialName = socialName;
        }
    }
}