﻿using Assets.Scripts.GameLogic.Utilites;

namespace Assets.Scripts.Engine.UI.Views.Popups.Settings
{
    /// <summary>
    /// Настройки окна ошибки привязки аккаунта
    /// </summary>
    class AccountBindingErrorPopupSettings : BasePopupSettings
    {
        /// <summary>
        /// Название социальной сети
        /// </summary>
        private readonly string _socialName;


        /// <summary>
        /// Заголовок окна
        /// </summary>
        public override string Title { get { return LocalizationMLS.Instance.GetText(LocalizationKeyConstants.LOGOUTPOPUP_LABEL_SORRY_MAYOR); } }

        /// <summary>
        /// Сообщение окна
        /// </summary>
        public override string Message
        {
            get { return string.Format(LocalizationMLS.Instance.GetText(LocalizationKeyConstants.LOGOUTPOPUP_MSG_CHANGE_ACCOUNT), _socialName); }
        }


        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="socialName">Название социальной сети</param>
        public AccountBindingErrorPopupSettings(string socialName)
        {
            _socialName = socialName;
        }
    }
}