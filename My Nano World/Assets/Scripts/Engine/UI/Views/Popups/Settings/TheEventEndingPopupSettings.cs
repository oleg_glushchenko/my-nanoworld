using System;

namespace Assets.Scripts.Engine.UI.Views.Popups.Settings
{
    public class TheEventEndingPopupSettings : BasePopupSettings
    {
        public override Type PopupType => typeof(TheEventEndingPopupSettings);
    }
}
