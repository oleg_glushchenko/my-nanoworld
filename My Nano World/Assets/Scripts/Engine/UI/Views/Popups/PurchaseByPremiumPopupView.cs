﻿using System;
using UnityEngine;
using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using NanoLib.Core.Services.Sound;
using NanoReality.Game.Tutorial;
using NanoReality.GameLogic.Services;
using strange.extensions.signal.impl;
using TMPro;
using UniRx;
using UnityEngine.UI;

namespace Assets.Scripts.Engine.UI.Views.Popups
{
	public class PurchaseByPremiumPopupView : BasePopupView
	{
		[SerializeField] private TextMeshProUGUI _countText;
		[SerializeField] private Image _priceIconImage;

		[SerializeField] private Button _yesButton;
		[SerializeField] private Button _closeButton;
		[SerializeField] private Button _closeBGButton;

		[Inject] public IHardTutorial jHardTutorial { get; set; }
		[Inject] public IIconProvider jIconProvider { get; set; }
		[Inject] public ISoundManager jSoundManager { get; set; }

		public Button YesButton => _yesButton;

		private int _currentPrice;
		private Signal _onCurrentOperationNotActual;
		private IDisposable _priceChangedDisposable;

		public override void Initialize(IPopupSettings settings)
		{
			base.Initialize(settings);

			var purchaseSettings = (PurchaseByPremiumPopupSettings) settings;

			_priceChangedDisposable = purchaseSettings.Price.Subscribe(OnChangePrice);

			_currentPrice = purchaseSettings.Price.Value;
			_countText.text = _currentPrice.ToString();
			_priceIconImage.sprite = jIconProvider.GetCurrencySprite(purchaseSettings.PriceType);

			_onCurrentOperationNotActual?.RemoveAllOnceListeners();
			_onCurrentOperationNotActual = purchaseSettings.OnOperationNotActualSignal;
			_onCurrentOperationNotActual?.AddOnce(OnOperationNotActual);
		}

		private void OnChangePrice(int price)
		{
			_currentPrice = price;
			_countText.text = _currentPrice.ToString();

			if (jHardTutorial.IsTutorialCompleted)
				CheckForZeroPrice();
		}

		public override void Show()
		{
			Show(null);
		}

		public override void Show(Action<PopupResult> callback)
		{
			base.Show(callback);

			CurrentPopupCallback = callback;
			_closeBGButton.onClick.AddListener(OnCancelPressed);
			_closeButton.onClick.AddListener(OnCancelPressed);
			jSoundManager.Play(SfxSoundTypes.AppWindow, SoundChannel.SoundFX);
			CheckForZeroPrice();
#if DISABLE_CONFIRMATION
			OnConfirmPressed();
#endif			
		}

		public override void Hide()
		{
			_priceChangedDisposable?.Dispose();

			if (_onCurrentOperationNotActual != null)
			{
				_onCurrentOperationNotActual.RemoveAllOnceListeners();
				_onCurrentOperationNotActual = null;
			}

			_closeBGButton.onClick.RemoveListener(OnCancelPressed);
			_closeButton.onClick.RemoveListener(OnCancelPressed);
			jSoundManager.Play(SfxSoundTypes.CloseWindow, SoundChannel.SoundFX);
			base.Hide();
		}

		private void CheckForZeroPrice()
		{
			if (_currentPrice <= 0)
			{
				Hide();
			}
		}

		public void OnConfirmPressed()
		{
			OnOkButtonClick();
		}

		public void OnCancelPressed()
		{
			OnCancelButtonClick();
		}

		private void OnOperationNotActual()
		{
			CurrentPopupResult = PopupResult.Cancel;
			Hide();
		}
	}
}