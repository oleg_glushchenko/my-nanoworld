﻿using System;
using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using Engine.UI.Extensions.ThereIsNoVideoPanel;
using NanoReality.Engine.UI;
using NanoReality.Engine.UI.Views;
using NanoReality.GameLogic.Services;
using TMPro;
using UnityEngine;

namespace Assets.Scripts.Engine.UI.Views.Popups
{
    public class NotEnoughCertificatesPopupView : BasePopupView
    {
        [Inject] public IAdsService jAdsService { get; set; }

        
        [SerializeField] private TextMeshProUGUI _amountText;
        [SerializeField] private AdTypePanel _adTypePanel;

        private int _certificates;
        private int _certificateCost;

        public override void Initialize(IPopupSettings settings)
        {
            base.Initialize(settings);

            var popupSettings = (NotEnoughCertificatesPopupSettings)settings;
            
            _certificates = popupSettings.Certificates;
            _certificateCost = popupSettings.CertificateCost;

            UpdateCertificateCounter();
        }

        protected override void Start()
        {
            base.Start();
            
            jAdsService.VideoWatched += OnWatchVideoAdSuccess;
            jAdsService.VideoSkipped += OnVideoSkipped;
        }

        public override void Hide()
        {
            CurrentPopupResult = PopupResult.Hide;
            
            base.Hide();
        }

        public override void Show(Action<PopupResult> callback)
        {
#if UNITY_EDITOR
            base.Show(callback);
#else
            if (!jAdsService.IsVideoLoaded)
            {
                Hide();
            }
            else
            {
                base.Show(callback);
            }
#endif
        }

        private void OnWatchVideoAdSuccess(object sender, EventArgs e)
        {
            if (jAdsService.PlayingAdType == _adTypePanel)
            {
                _certificates++;
                
                if (_certificates >= _certificateCost)
                {
                    Hide();
                }
                else
                {
                    UpdateCertificateCounter();
                }
            }
        }
        
        private void OnVideoSkipped(object sender, EventArgs e)
        {
            if (!Visible) return;
            
            Hide();
        }

        private void UpdateCertificateCounter()
        {
            _amountText.text = $"{_certificates.ToString()}/{_certificateCost.ToString()}";
        }
    }
}
