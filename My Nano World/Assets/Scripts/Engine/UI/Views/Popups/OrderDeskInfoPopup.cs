﻿using System.Collections.Generic;
using Assets.NanoLib.UI.Core.Components;
using Assets.Scripts.Engine.UI.Views.Popups;
using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using Assets.Scripts.Engine.UI.Views.Trade.Items;
using Assets.Scripts.MultiLanguageSystem.Logic;
using NanoReality.Core.Resources;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using NanoReality.GameLogic.Services;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class OrderDeskInfoPopup : BasePopupView
{
    [SerializeField] private TextMeshProUGUI _titleField;
    [SerializeField] private ProductCountCard _productInfoPrefab;
    [SerializeField] private ItemContainer _toolboxItemsContainer;
    [SerializeField] private ItemContainer _orderItemsContainer;
    [SerializeField] private Image _toolboxImage;
    [SerializeField] private Image _townCharacterImage;
    [SerializeField] private Image _farmCharacterImage;
    [SerializeField] private Image _industryCharacterImage;
    [SerializeField] private GameObject _toolboxRewardsGameObject;

    [Inject] public IResourcesManager jResourcesManager { get; set; }
    [Inject] public IIconProvider jIconProvider { get; set; }

    public override void Initialize(IPopupSettings settings)
    {
        base.Initialize(settings);

        var infoPopupSettings = settings as OrderDeskInfoPopupSettings;
        
        if (infoPopupSettings == null)
            return;
        
        _titleField.SetLocalizedText(infoPopupSettings.Title);
        
        _toolboxRewardsGameObject.SetActive(infoPopupSettings.ToolboxItems != null);
        InitializeItems(_toolboxItemsContainer, infoPopupSettings.ToolboxItems);
        InitializeItems(_orderItemsContainer, infoPopupSettings.OrderItems);

        SetupToolboxImage(infoPopupSettings.SectorType);
        SetupCharacterImage(infoPopupSettings.SectorType);
    }

    private void SetupToolboxImage(BusinessSectorsTypes sectorType)
    {
        _toolboxImage.sprite = jIconProvider.GetSpecialOrderIcon(sectorType);
    }

    private void SetupCharacterImage(BusinessSectorsTypes sectorType)
    {
        _townCharacterImage.enabled = false;
        _farmCharacterImage.enabled = false;
        _industryCharacterImage.enabled = false;
        switch (sectorType)
        {
            case BusinessSectorsTypes.Town:
                _townCharacterImage.enabled = true;
                break;
            case BusinessSectorsTypes.Farm:
                _farmCharacterImage.enabled = true;
                break;
            case BusinessSectorsTypes.HeavyIndustry:
                _industryCharacterImage.enabled = true;
                break;
        }
    }

    private void InitializeItems(ItemContainer itemContainer, List<Product> products)
    {
        if (products == null)
        {
            return;
        }
        if (!itemContainer.IsInstantiateed)
        {
            itemContainer.InstaniteContainer(_productInfoPrefab, 0);
        }
        itemContainer.ClearCurrentItems();        

        foreach (var product in products)
        {
            var itemView = itemContainer.AddItem() as ProductCountCard;
            if (itemView != null)
            {
                ProductCountCardInitData productInitData = new ProductCountCardInitData()
                {
                    ProductObject = product,
                    ProductSprite = jResourcesManager.GetProductSprite(product.ProductTypeID),
                    SpecialOrderSprite = jIconProvider.GetSpecialOrderIcon(product.SpecialOrder),
                    ShowOnlySimpleTools = true,//true
                    ProductsCount = null,
                    CurrentProductsCount = null,
                    NeedProductsCount = null,
                    IsCountNeed = false,//false
                    Type = TooltipProductType.Product
                };
                itemView.Initialize(productInitData);
            }
        }        
    }

    public void OnCancel()
    {
        OnCancelButtonClick();
    }    
}