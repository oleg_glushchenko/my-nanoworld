﻿using Assets.NanoLib.UI.Core.Signals;
using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using I2.Loc;
using NanoLib.Core.Services.Sound;
using NanoLib.UI.Core;
using NanoReality.Game.Data;
using NanoReality.Game.UI;
using NanoReality.GameLogic.Bank;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.Utilites;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

#pragma warning disable 649

namespace Assets.Scripts.Engine.UI.Views.Popups
{
    public class NotEnoughMoneyPopupView : BasePopupView
    {
        [Inject] public ShowWindowSignal jShowWindowSignal { get; set; }
        [Inject] public IUIManager jUIManager { get; set; }
        [Inject] public IBuildSettings jBuildSettings { get; set; }
        [Inject] public ISoundManager jSoundManager { get; set; }

        [SerializeField] private Sprite _softCurrencySprite;
        [SerializeField] private Sprite _goldCurrencySprite;
        [SerializeField] private Sprite _hardCurrencySprite;
        [SerializeField] private Sprite _lanternCurrencySprite;
        [SerializeField] private Image _resourceImage;
        [SerializeField] private TextMeshProUGUI _resourceAmount;
        [SerializeField] private Button _getMoreButton;
        [SerializeField] private Localize _localize;

        private PriceType _currentPriceType;

        public override void Initialize(IPopupSettings settings)
        {
            base.Initialize(settings);
            jSoundManager.Play(SfxSoundTypes.AppWindow, SoundChannel.SoundFX);
            var s = (NotEnoughMoneyPopupSettings) settings;
            _currentPriceType = s.PriceType;
            SetResourcesSprite(_currentPriceType);
            _resourceAmount.text = s.Amount.ToString();

            _localize.Term = LocalizationUtils.GetNotEnoughtMoneyText(_currentPriceType);

            if (!jBuildSettings.EnablePayments && (_currentPriceType == PriceType.Hard || _currentPriceType == PriceType.Gold))
            {
                _getMoreButton.interactable = false;
            }
            else
            {
                _getMoreButton.interactable = true;
            }
        }

        public void OnGetMoreButtonClick()
        {
            OnOkButtonClick();
        }

        public override void Hide()
        {
            if (CurrentPopupResult == PopupResult.Ok)
            {
                jUIManager.HideAll();
                switch (_currentPriceType)
                {
                    case PriceType.Soft:
                    case PriceType.Hard:
                    {
                        var type = _currentPriceType == PriceType.Hard ? BankTabType.Hard : BankTabType.Soft;
                        jShowWindowSignal.Dispatch(typeof(BankView), type);
                        break;
                    }
                    case PriceType.Gold:
                    {
                        jShowWindowSignal.Dispatch(typeof(MetroOrderDeskPanelView), null);
                        break;
                    }
                    case PriceType.Lantern:
                    {
                        jShowWindowSignal.Dispatch(typeof(BankView), PriceType.Hard);
                            break;
                    }
                }
            }
            jSoundManager.Play(SfxSoundTypes.CloseWindow, SoundChannel.SoundFX);
            base.Hide();
        }

        private void SetResourcesSprite(PriceType currentPriceType)
        {
            switch (currentPriceType)
            {
                case PriceType.Soft:
                    _resourceImage.sprite = _softCurrencySprite;
                    break;
                case PriceType.Hard:
                    _resourceImage.sprite = _hardCurrencySprite;
                    break;
                case PriceType.Gold:
                    _resourceImage.sprite = _goldCurrencySprite;
                    break;
                case PriceType.Lantern:
                    _resourceImage.sprite = _lanternCurrencySprite;
                    break;
                default:
                    _resourceImage.sprite = null;
                    break;
            }
        }
    }
}