﻿using NanoReality.Engine.UI.Views.Panels;
using NanoReality.Engine.UI;
using NanoReality.GameLogic.BuildingSystem.CityCamera.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.GameManager.Models.api;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Engine.UI.Views.Popups
{
    public class PopulationLimitPopupView : BasePopupView
    {
        [SerializeField] private Button _closeButton;

        [SerializeField] private Button _goToButton;

        [SerializeField] private Button _overlayButton;
        
        [Inject] public IGameManager jGameManager { get; set; }
        [Inject] public IGameCamera jGameCamera { get; set; }

        protected override void Awake()
        {
            base.Awake();

            _closeButton.onClick.AddListener(OnCancelButtonClick);
            _overlayButton.onClick.AddListener(OnCancelButtonClick);
            _goToButton.onClick.AddListener(OnGoToButtonClick);
        }

        private void OnGoToButtonClick()
        {
            OnOkButtonClick();

            var cityHall = jGameManager.FindMapObjectByType<ICityHall>(MapObjectintTypes.CityHall);
            if (cityHall == null) return;

            if (cityHall.CanBeUpgraded && !cityHall.IsUpgradingNow)
            {
                jGameCamera.FocusOnMapObject(cityHall);
            }
            else
            {
                var view = jGameManager.GetMapObjectView(cityHall);
                if (view != null)
                {
                    view.OnMapObjectViewTap();
                }
            }
        }
    }
}
