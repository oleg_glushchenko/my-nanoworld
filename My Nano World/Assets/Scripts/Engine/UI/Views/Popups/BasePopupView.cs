﻿using System;
using Assets.NanoLib.UI.Core.Views;
using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using NanoLib.Core.Logging;
using strange.extensions.signal.impl;

namespace Assets.Scripts.Engine.UI.Views.Popups
{
    public enum PopupResult
    {
        Cancel,
        Ok,
        Hide
    }
    
    public sealed class PopupOpenedSignal : Signal<BasePopupView> { }
    
    public sealed class PopupClosedSignal : Signal<BasePopupView> { }
    
    public abstract class BasePopupView : UIPanelView
    {
        [Inject] public PopupOpenedSignal jPopupOpenedSignal { get; set; }
        [Inject] public PopupClosedSignal jPopupClosedSignal { get; set; }

        protected Action<PopupResult> CurrentPopupCallback;
        protected PopupResult CurrentPopupResult;

        public virtual void Initialize(IPopupSettings settings)
        {
            base.Initialize(settings);
        }
        
        public override void Show()
        {            
            if (!registeredWithContext)
                base.Start();
            
            CurrentPopupResult = PopupResult.Cancel;
            CurrentPopupCallback = null;
            
            jPopupOpenedSignal.Dispatch(this);
        }
        
        public virtual void Show(Action<PopupResult> callback)
        {
            Logging.Log(LoggingChannel.Ui, $"Popup <b>{name}</b> Show");
            
            if (!registeredWithContext)
                base.Start();
            
            CurrentPopupResult = PopupResult.Cancel;
            CurrentPopupCallback = callback;
            
            jPopupOpenedSignal.Dispatch(this);
        }
        
        protected void OnCancelButtonClick()
        {
            CurrentPopupResult = PopupResult.Cancel;
            Hide();
        }
        
        protected void OnOkButtonClick()
        {
            CurrentPopupResult = PopupResult.Ok;
            Hide();
        }

        public override void Hide()
        {
            base.Hide();
            
            jPopupClosedSignal.Dispatch(this);
            CurrentPopupCallback?.Invoke(CurrentPopupResult);
        }
    }
}