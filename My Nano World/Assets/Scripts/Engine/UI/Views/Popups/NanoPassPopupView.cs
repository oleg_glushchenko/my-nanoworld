using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using NanoReality.GameLogic.Bank;
using NanoReality.GameLogic.IAP;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Engine.UI.Views.Popups
{
    public class NanoPassPopupView : BasePopupView
    {
        [Inject] public PurchaseFinishedSignal jPurchaseFinishedSignal { get; set; }

        [SerializeField] private Button _buyButton;
        [SerializeField] private Button _closeButton;
        [SerializeField] private TextMeshProUGUI _title;
        [SerializeField] private GameObject _backGround;
        [SerializeField] private TextMeshProUGUI _price;

        private IPurchaseProvider _purchaseProvider;
        private IBankProduct _nanoPass;
        private bool _pendingPurchase;
        private int _productId;
        private NanoPassPopupSettings _settings;

        public override void Initialize(IPopupSettings settings)
        {
            base.Initialize(settings);

            _settings = settings as NanoPassPopupSettings;
            _purchaseProvider = _settings?.PurchaseProvider;
            _nanoPass = _settings?.NanoPass;
            _backGround.SetActive(_settings.EnableBackground);
            _price.text = _nanoPass.Price;
        }

        public void OnEnable()
        {
            _buyButton.onClick.AddListener(OnBuyClicked);
            _closeButton.onClick.AddListener(OnCloseButton);
        }

        public void OnDisable()
        {
            _buyButton.onClick.RemoveListener(OnBuyClicked);
            _closeButton.onClick.RemoveListener(OnCloseButton);
        }

        private void OnBuyClicked()
        {
            if (_pendingPurchase) return;
            _pendingPurchase = true;
            _productId = _nanoPass.Id;
            jPurchaseFinishedSignal.AddListener(OnPurchase);
            _purchaseProvider.BuyProductClicked(_nanoPass);
            OnOkButtonClick();
        }

        private void OnPurchase(IStoreProduct product, bool success)
        {
            if (product.Id == _productId)
            {
                _pendingPurchase = false;
                jPurchaseFinishedSignal.RemoveListener(OnPurchase);

                if (success)
                {
                    _settings.OnSuccessfulPurchase?.Invoke();
                }
            }
        }

        private void OnCloseButton()
        {
            OnCancelButtonClick();
        }
    }
}