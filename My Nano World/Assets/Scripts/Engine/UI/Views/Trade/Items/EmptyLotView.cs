﻿using strange.extensions.signal.impl;
using UnityEngine;
using UnityEngine.UI;

#pragma warning disable 649

namespace Assets.Scripts.Engine.UI.Views.Trade.Items
{
    /// <summary>
    /// Представление пустого слота
    /// </summary>
    public class EmptyLotView : BaseLotView
    {
        /// <summary>
        /// Кнопка добавления товара на продажу
        /// </summary>
        [SerializeField]
        private Button _emptySlotButton;

        /// <summary>
        /// Сигнал создания нового лота (новой продажи)
        /// </summary>
        public Signal OnCreateLotSignal
        {
            get { return _createNewLotSignal; }
        }
        
        private readonly Signal _createNewLotSignal = new Signal();

        /// <summary>
        /// Обработчик нажатия кнопки создания нового лота (установить товар на продажу)
        /// </summary>
        public void OnCreateButtonClick()
        {
            _createNewLotSignal.Dispatch();
        }

        /// <summary>
        /// Установка ожидания ответа от сервера
        /// </summary>
        /// <param name="isWait">Ожидать ли ответ от сервера</param>
        public override void WaitForServer(bool isWait)
        {
            _emptySlotButton.interactable = !isWait;
        }
    }
}