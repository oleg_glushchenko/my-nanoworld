﻿using Assets.Scripts.MultiLanguageSystem.Logic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Engine.UI.Views.Trade.Items
{
    /// <summary>
    /// Представление продаваемого продукта
    /// </summary>
    public class TradeProductView : MonoBehaviour
    {
        /// <summary>
        /// Иконка продукта
        /// </summary>
        [SerializeField]
        private Image _itemIcon;

        /// <summary>
        /// Количество продукта
        /// </summary>
        [SerializeField]
        private TextMeshProUGUI _itemCountText;
        
        /// <summary>
        /// Название
        /// </summary>
        [SerializeField]
        private TextMeshProUGUI _itemText;


        /// <summary>
        /// Иконка продаваемого продукта
        /// </summary>
        public Image ItemImage { get { return _itemIcon; } }


        /// <summary>
        /// Установка данных
        /// </summary>
        /// <param name="sprite">Картинка продукта</param>
        /// <param name="amount">Количество</param>
        /// <param name="text">Название</param>
        public void SetItem(Sprite sprite, int amount, string text)
        {
            _itemIcon.sprite = sprite;
            _itemCountText.text = amount.ToString();
            _itemText.SetLocalizedText(text);
        }
    }
}