using System;
using NanoReality.Game.UI;

namespace Assets.Scripts.Engine.UI.Views.Replics
{
    public class DialogData
    {
        public readonly DialogCharacter CharacterToShow;
        public readonly string TextKey;
        public readonly DialogPosition Position;
        public readonly CharacterExpression CharacterExpression;
        public readonly bool ShowFade;
        public readonly bool ButtonEnabled;
        public readonly string ButtonText;
        public readonly bool ButtonIsReward;
        public readonly bool ShowAcceptButtons;
        public readonly string AcceptButtonText;
        public readonly string DeclineButtonText;
        public readonly Action<bool> AcceptResult;
        public readonly bool UseMessageBoxPopup;

        public DialogData(DialogCharacter characterToShow, string textKey,
            DialogPosition position = DialogPosition.Left,
            CharacterExpression characterExpression = CharacterExpression.LookAt,
            bool showDarkness = false,
            bool buttonEnabled = false,
            string buttonText = "",
            bool buttonIsReward = false, bool showAcceptButtons = false, string acceptButtonText = "",
            string declineButtonText = "", Action<bool> acceptResult = null, bool useMessageBoxPopup = false)
        {
            CharacterToShow = characterToShow;
            TextKey = textKey;
            Position = position;
            CharacterExpression = characterExpression;
            ShowFade = showDarkness;
            ButtonEnabled = buttonEnabled;
            ButtonText = buttonText;
            ButtonIsReward = buttonIsReward;
            ShowAcceptButtons = showAcceptButtons;
            AcceptButtonText = acceptButtonText;
            DeclineButtonText = declineButtonText;
            AcceptResult = acceptResult;
            UseMessageBoxPopup = useMessageBoxPopup;
        }
    }
}