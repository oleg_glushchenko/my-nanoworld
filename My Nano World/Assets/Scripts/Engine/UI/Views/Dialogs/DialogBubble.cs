﻿using Assets.NanoLib.UI.Core.Views;
using Assets.NanoLib.UtilitesEngine.CustomDebugging.Models.api;
using NanoLib.Core.Logging;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

namespace Assets.Scripts.Engine.UI.Views.Replics
{
    public class DialogBubble : UIPanelView
    {
        [SerializeField] private TextMeshProUGUI _textHolder;
        [SerializeField] private Animation _animation;
        [SerializeField] private AnimationCurve _wordSpeedMultiplier;
        [SerializeField] private AnimationCurve _wordShowSpeedMultiplier;
        [SerializeField] private float _timeCharShow = 0.07f;
        [SerializeField] private float _delayCharShowFast = 0.07f;
        [SerializeField] private float _delayCharShowShort = 0.08f;
        [SerializeField] private float _delayCharShowLong = 0.15f;
        [SerializeField] private float _delayCharShowVeryLong = 0.25f;

        Coroutine _animateTextProcess;
        Color _colorTextCache;
        float[] _delays;

        enum CharDelayID : byte
        {
            None = 0,
            Fast = 1,
            Short = 2,
            Long = 3,
            VeryLong = 4,
        }

        IDictionary<char, CharDelayID> _charsDelays = new Dictionary<char, CharDelayID>()//TODO, move to config
        {
            ['!'] = CharDelayID.VeryLong,
            ['?'] = CharDelayID.VeryLong,
            ['.'] = CharDelayID.VeryLong,
            [','] = CharDelayID.Long,
            [' '] = CharDelayID.Short,
        };

        float GetDelayDuration(CharDelayID charDelayID)
        {
            if (_delays == null)
                _delays = new[] { 0, _delayCharShowFast, _delayCharShowShort, _delayCharShowLong, _delayCharShowVeryLong };

            return _delays[(int) charDelayID];
        }

        CharDelayID GetCharDelay(char ch)
        {
            if (_charsDelays.TryGetValue(ch, out var result))
                return result;

            if (char.IsControl(ch))
                return CharDelayID.None;

            return CharDelayID.Fast;
        }

        IEnumerable<float> GetDelayes(TextMeshProUGUI text)
        {
            var info = text.textInfo;
            var chars = info.characterInfo;
            var charsCount = info.characterCount;

            var lastId = charsCount - 1;
            for (; lastId >= 0; lastId--)
                if (chars[lastId].character > 0)
                    break;

            for (int id = 0; id <= lastId; id++)
                yield return GetDelayDuration(GetCharDelay(chars[id].character));
        }

        IEnumerable<float> GetShowDelayes(TextMeshProUGUI text)
        {
            var info = text.textInfo;
            var chars = info.characterInfo;
            var charsCount = info.characterCount;

            var lastId = charsCount - 1;
            for (; lastId >= 0; lastId--)
                if (chars[lastId].character > 0)
                    break;

            for (int id = 0; id <= lastId; id++)
                yield return _timeCharShow;
        }

        IEnumerable<(float delay, float showTime)> GetDelayesByWord(TextMeshProUGUI text, IReadOnlyList<float> delays, IReadOnlyList<float> showTimes)
        {
            var words = text.textInfo.wordInfo;
            var result = delays.Zip(showTimes, (delay, showTime) => (delay, showTime)).ToArray();

            foreach (var word in words)
            {
                var toId = Mathf.Min(word.lastCharacterIndex + 1, result.Length);
                for (var id = word.firstCharacterIndex; id < toId; id++)
                    if (word.characterCount > 0)
                    {
                        var charIdPercent = (id - word.firstCharacterIndex) / (float) word.characterCount;

                        result[id].delay = delays[id] * _wordSpeedMultiplier.Evaluate(charIdPercent);
                        result[id].showTime = showTimes[id] * _wordShowSpeedMultiplier.Evaluate(charIdPercent);
                    }
            }

            return result;
        }

        IEnumerator AnimateText(TextMeshProUGUI text, IEnumerable<(float delay, float showTime)> viewData)
        {
            var infoText = text.textInfo;
            var infoCharacters = infoText.characterInfo;
            var alphas = infoText.characterInfo.Select(x => x.color.a).ToArray();
            var colorTransparent = text.color;

            var tasksParallel = new EnumerableParallel();

            colorTransparent.a = 0;

            text.color = colorTransparent;

            var id = 0;
            foreach (var (delay, showTime) in viewData)
            {
                var charInfo = infoCharacters[id];

                if (charInfo.isVisible)
                {
                    var meshIndex = charInfo.materialReferenceIndex;
                    var vertexIndex = charInfo.vertexIndex;
                    var vertexColors = infoText.meshInfo[meshIndex].colors32;
                    var _id = id;

                    tasksParallel.Add(Wait(showTime, percent => UpdateCharViewAlpha(text, vertexColors, alphas, vertexIndex, percent, _id)));
                }

                yield return Wait(delay, p => tasksParallel.Update());

                ++id;
            }

            yield return tasksParallel.WaitEnd();

            FinaizeAnimationText(text);
        }

        private void UpdateCharViewAlpha(TextMeshProUGUI text, Color32[] vertexColors, byte[] alphas, int vertexId, float percent, int id)
        {
            var alpha = (byte) (alphas[id] * percent);
            UpdateCharView(text, vertexColors, vertexId, alpha);
        }

        private void UpdateCharView(TextMeshProUGUI text, Color32[] vertexColors, int vertexId, byte alpha)
        {
            vertexColors[vertexId + 0].a = alpha;
            vertexColors[vertexId + 1].a = alpha;
            vertexColors[vertexId + 2].a = alpha;
            vertexColors[vertexId + 3].a = alpha;

            text.UpdateVertexData(TMP_VertexDataUpdateFlags.Colors32);
        }

        void PrepareAnimationText(TextMeshProUGUI textComponent, string text)
        {
            textComponent.text = text;
            _colorTextCache = textComponent.color;
            textComponent.ForceMeshUpdate(); //after set text & before any get information from TMP_Component
        }

        void FinaizeAnimationText(TextMeshProUGUI textComponent)
        {
            textComponent.color = _colorTextCache;
            StopCoroutine(_animateTextProcess);
            _animateTextProcess = null;
        }

        IEnumerator Wait(float duration, Action<float> progress = default)
        {
            var time = 0f;

            while (time < duration)
            {
                yield return null;
                time += Time.deltaTime;

                var percent = time / duration;

                if (percent < 1)
                    progress?.Invoke(percent);
            }

            progress?.Invoke(1);
        }

        void StartAnimateText(string text)
        {
            StopAnimateText();
            PrepareAnimationText(_textHolder, text);
            _animateTextProcess = StartCoroutine(AnimateText(_textHolder, GetDelayesByWord(_textHolder, GetDelayes(_textHolder).ToArray(), GetShowDelayes(_textHolder).ToArray())));
        }

        void StopAnimateText()
        {
            if (_animateTextProcess != null)
            {
                StopCoroutine(_animateTextProcess);
                FinaizeAnimationText(_textHolder);
            }
        }

        public void OnDisable()
        {
            StopAnimateText();
        }

        public void Show(string textToShow)
        {
            _animation?.Play();

            if (!_textHolder)
            {
                "TextMeshProUGUI component _textHolder == NULL; Please assign it in inspector".LogError();
            }

            StartAnimateText(textToShow);
        }

        public void SkipPrintEffect()
        {
            StopAnimateText();
            _textHolder.color = _colorTextCache;
            _textHolder.UpdateVertexData(TMP_VertexDataUpdateFlags.Colors32);
        }

        public bool IsTheEndOfText() => _animateTextProcess == null;
    }

    public class EnumerableParallel
    {
        private readonly HashSet<IEnumerator> _tasks = new HashSet<IEnumerator>();

        public void Add(IEnumerator enumerator) => _tasks.Add(enumerator);

        public void Update() => _tasks.RemoveWhere(x => !x.MoveNext());

        public IEnumerator WaitEnd()
        {
            while (_tasks.Count > 0)
            {
                Update();
                yield return null;
            }
        }

        public IEnumerator WaitEnd(Action<float> callback)
        {
            var count = _tasks.Count;

            while (_tasks.Count > 0)
            {
                Update();
                callback?.Invoke(1f - (_tasks.Count / count));

                yield return null;
            }

            callback?.Invoke(1f);
        }
    }
}
