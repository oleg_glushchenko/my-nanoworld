﻿using System.Collections;
using System.Collections.Generic;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace NanoReality.Engine.UI.Views.OrderDeskPanel
{
    /// <summary>
    /// Controller of animated conveer line in OrderDeskPanelView.
    /// </summary>
    public sealed class OrderDeskConveer : View
    {
        #region Fields

        [SerializeField] private List<RectTransform> _conveerLines;

        [SerializeField] private List<RectTransform> _conveerWheels;

        [SerializeField] private float _minXPos;

        [SerializeField] private float _maxXPos;

        [SerializeField] private float _moveSpeed;

        private Coroutine _moveCoroutine;

        #endregion

        #region Methods

        /// <summary>
        /// Starts conveer moving animation.
        /// </summary>
        /// <param name="isRight">Is move to right side.</param>
        public void StartConveer(bool isRight)
        {
            StopConveerCoroutine();

            Vector2 moveDirection = isRight ? Vector2.right : Vector2.left;
            _moveCoroutine = StartCoroutine(MoveConveerLineCoroutine(moveDirection));
        }

        /// <summary>
        /// Stops conveer moving animation.
        /// </summary>
        public void StopConveer()
        {
            StopConveerCoroutine();
        }

        private IEnumerator MoveConveerLineCoroutine(Vector3 moveDirection)
        {
            while (true)
            {
                yield return new WaitForEndOfFrame();

                foreach (RectTransform conveerLine in _conveerLines)
                {
                    RectTransform lineRectTrans = conveerLine;
                    Vector2 currentPos = lineRectTrans.anchoredPosition;
                    currentPos.x += (moveDirection.x * _moveSpeed * Time.deltaTime);

                    bool isReachEnd = currentPos.x > _maxXPos;
                    bool isReachStart = currentPos.x < _minXPos;
                    if (isReachEnd)
                    {
                        currentPos.x = _minXPos;
                    }
                    else if(isReachStart)
                    {
                        currentPos.x = _maxXPos;
                    }

                    lineRectTrans.anchoredPosition = currentPos;
                }

                foreach (RectTransform wheelTransform in _conveerWheels)
                {
                    Vector3 wheelRotation = wheelTransform.eulerAngles;
                    wheelRotation.z += (-0.5f * moveDirection.x * _moveSpeed) * Time.deltaTime;

                    wheelTransform.eulerAngles = wheelRotation;
                }
            }
        }

        private void StopConveerCoroutine()
        {
            if(_moveCoroutine != null)
                StopCoroutine(_moveCoroutine);
        }

        #endregion
    }
}