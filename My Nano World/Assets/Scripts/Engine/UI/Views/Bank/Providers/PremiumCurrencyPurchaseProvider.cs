﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.GameLogic.Utilites;
using NanoLib.Core.Logging;
using NanoLib.Core.Services.Sound;
using NanoReality.GameLogic.IAP;
using NanoReality.GameLogic.PlayerProfile.Models.Impl;
using NanoReality.GameLogic.Utilites;

namespace NanoReality.GameLogic.Bank
{
	public class PremiumCurrencyPurchaseProvider : IPurchaseProvider
	{
        private BankContext _context;
        private bool _isLocked;
        private float _fxAnimLength = 2;

		public void Initialize(BankContext context)
		{
			_context = context;
		}

		public void BuyProductClicked(IBankProduct bankProduct)
		{
			if (_isLocked) return;

			_isLocked = true;
			
			if (bankProduct is InAppShopProduct product)
			{
				_context.jPurchaseFinishedSignal.AddListener(OnPurchaseComplete);
				_context.jIapService.BuyProduct(product.StoreId);
			}
			else
			{
				Logging.LogError(LoggingChannel.Core, "The product is not premium currency product");
				_isLocked = false;
			}
		}

		public IEnumerable<IBankProduct> GetProducts()
		{
			return _context.jIapService.InAppShopProducts.Products.Cast<IBankProduct>().ToList();
		}

        private void OnPurchaseComplete(IStoreProduct product, bool isSuccess)
        {
            if (isSuccess)
            {
                var shopProduct = (InAppShopProduct) product;
                var amount = shopProduct.Reward.reward + shopProduct.Reward.bonusReward;
                _context.jSoundManager.Play(SfxSoundTypes.BuyAnyBankItem, SoundChannel.SoundFX);
                _context.jPlayerProfile.PlayerResources.AddHardCurrency(amount);
                _context.jNanoAnalytics.OnInAppPurchase(shopProduct.StoreId, shopProduct.StorePrice);
                _context.jNanoAnalytics.OnPlayerCurrenciesIncrease(PlayerResources.CurencyType.Bucks, amount,
                    AwardSourcePlace.Purchase, shopProduct.StoreId);
                _context.jShowWindowSignal.Dispatch(typeof(FxPopUpPanelView), new FxPopUpPanelView.FxPopupData(
                    LocalizationUtils.LocalizeUI(LocalizationKeyConstants.PURCHASED_OFFER), _fxAnimLength,
                    FxPanelType.Achievement));
            }

            _isLocked = false;
            _context.jPurchaseFinishedSignal.RemoveListener(OnPurchaseComplete);
        }
	}
}