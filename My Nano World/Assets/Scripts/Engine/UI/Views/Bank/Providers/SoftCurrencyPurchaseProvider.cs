﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Engine.Analytics.Model;
using Assets.Scripts.Engine.UI.Views.Popups;
using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using NanoLib.Core.Logging;
using NanoLib.Core.Services.Sound;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.PlayerProfile.Models.Impl;

namespace NanoReality.GameLogic.Bank
{
    public class SoftCurrencyPurchaseProvider : IPurchaseProvider
    {
        private BankContext _context;
        private bool _isLocked;

        private const string UINamePrefix = "Soft_";

        public void Initialize(BankContext context)
        {
            _context = context;
        }

        public void BuyProductClicked(IBankProduct bankProduct)
        {
            if (_isLocked) return;
            
            _isLocked = true;
            
            if (bankProduct is SoftCurrencyProduct product)
            {
                _context.jSignalOnUIAction.Dispatch(new UiIteractionInfo(UiActionType.Click, UINamePrefix + product.Name, UiElementType.Button));

                if (product.HardPrice > _context.jPlayerProfile.PlayerResources.HardCurrency)
                {
                    _context.jPopupManager.Show(new NotEnoughMoneyPopupSettings(PriceType.Hard, product.HardPrice - _context.jPlayerProfile.PlayerResources.HardCurrency));
                    _isLocked = false;
                }
                else
                {
                    _context.jPopupManager.Show(new PurchaseByPremiumPopupSettings(product.HardPrice), result =>
                    {
                        if (result == PopupResult.Ok)
                        {
                            BuyBundle(product);
                        }
                        else
                        {
                            _isLocked = false;
                        }
                    });
                }
            }
            else
            {
                "The product is not soft currency product".LogError();
                _isLocked = false;
            }
        }

        public IEnumerable<IBankProduct> GetProducts()
        {
            return _context.jSoftCurrencyBundlesModel.SoftCurrencyBundles.Cast<IBankProduct>().ToList();
        }

        private void BuyBundle(SoftCurrencyProduct product)
        {
            var playerResources = _context.jPlayerProfile.PlayerResources;
            if (playerResources.HardCurrency >= product.HardPrice)
            {
                _context.jServerCommunicator.PurchaseSoftCurrencyBundle(product.Id.ToString(), () =>
                {
                    _context.jSoundManager.Play(SfxSoundTypes.BuyAnyBankItem, SoundChannel.SoundFX);
                    playerResources.BuyHardCurrency(product.HardPrice);
                    _context.jNanoAnalytics.OnPlayerCurrenciesDecrease(PlayerResources.CurencyType.Bucks, product.HardPrice, CurrenciesSpendingPlace.BuySoftCurrency, product.Id.ToString());

                    playerResources.AddSoftCurrency(product.RewardCount);
                    _context.jNanoAnalytics.OnPlayerCurrenciesIncrease(PlayerResources.CurencyType.Coins, product.RewardCount, AwardSourcePlace.BuyCurrency, product.Id.ToString());

                    _context.jSoftCurrencyBoughtSignal.Dispatch(product);
                    
                    _isLocked = false;
                });
            }
            else
            {
                _context.jPopupManager.Show(new NotEnoughMoneyPopupSettings(PriceType.Hard, product.HardPrice - playerResources.HardCurrency));
                
                _isLocked = false;
            }
        }
    }
}
