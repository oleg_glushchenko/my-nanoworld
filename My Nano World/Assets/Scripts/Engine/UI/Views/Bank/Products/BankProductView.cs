﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.GameLogic.Bank
{
    public sealed class BankProductView : BankProductBase
    {
        [SerializeField] private TextMeshProUGUI _purchaseQuantity;
        [SerializeField] private TextMeshProUGUI _purchaseCost;
        [SerializeField] private TextMeshProUGUI _discountText;
        [SerializeField] private GameObject _discountLabel;
        [SerializeField] private Image _icon;
        [SerializeField] private bool _isBuyingByRealMoney;
        [SerializeField] private GameObject _oldPriceGameObject = null;
        [SerializeField] private TextMeshProUGUI _oldPriceText = null;
        
        private IBankProduct _model;

        public override event Action<IBankProduct> BuyBundleClick;

        public override void Initialize(IBankProduct model, Sprite sprite)
        {
            _model = model;
            var price = model.Price;
            var currencyQuantity = model.Amount + (string.IsNullOrEmpty(model.BonusAmount) ? string.Empty : "+" + model.BonusAmount);
            var discount = model.Discount;
            var isDiscountExist = !(string.IsNullOrEmpty(discount) || discount == "0");
            var isOldPriceExist = !(string.IsNullOrEmpty(model.OldPrice) || model.OldPrice == "0");
            _purchaseCost.text = price + (_isBuyingByRealMoney ? "$" : string.Empty);
            _purchaseQuantity.text = currencyQuantity;
            _discountLabel.gameObject.SetActive(isDiscountExist);
            
            if (_oldPriceGameObject != null)
            {
                _oldPriceGameObject.SetActive(isOldPriceExist);
                _oldPriceText.text = isOldPriceExist ? model.OldPrice : string.Empty;
            }
            
            _discountText.text = isDiscountExist ? $"+{discount}%" : string.Empty;
            _icon.sprite = sprite;
        }
        
        public void OnPurchaseHandle() //Calls from Unity
        {
            BuyBundleClick?.Invoke(_model);
        }

        #region IPoolable

        public override void Restore()
        {
        }

        public override void Retain()
        {
            gameObject.SetActive(true);
        }

        public override void Release()
        {
            BuyBundleClick = delegate { };
            gameObject.SetActive(false);
        }

        public override bool retain { get; }

        #endregion
    }
}