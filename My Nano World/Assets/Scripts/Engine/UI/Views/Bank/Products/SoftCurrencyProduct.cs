﻿using System;
using Assets.NanoLib.Utilities;
using Newtonsoft.Json;

namespace NanoReality.GameLogic.Bank
{
    [Serializable]
    public class SoftCurrencyProduct : IBankProduct
    {
        [JsonProperty("id")] 
        public Id<SoftCurrencyProduct> ProductId { get; set; }

        [JsonProperty("hard_price")] 
        public int HardPrice { get; set;  }
        
        [JsonProperty("count")] 
        public int RewardCount { get; set; }

        [JsonProperty("title")] 
        public string Name { get; set; }

        public string Discount => string.Empty;

        public string OldPrice => string.Empty;

        [JsonProperty("description")] 
        public string Description { get; set; }

        public int Id => ProductId.Value;
        public string Price => HardPrice.ToString();
        public string Amount => RewardCount.ToString();
        public string BonusAmount => string.Empty;
    }
}