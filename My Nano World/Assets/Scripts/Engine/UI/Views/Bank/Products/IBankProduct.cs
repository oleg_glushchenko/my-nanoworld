﻿namespace NanoReality.GameLogic.Bank
{
	public interface IBankProduct
	{
		int Id { get; }
		string Price { get; }
		string Amount { get; }
		string BonusAmount { get; }
		string Name { get; }
		string Discount { get; }
		string OldPrice { get; }
	}
}