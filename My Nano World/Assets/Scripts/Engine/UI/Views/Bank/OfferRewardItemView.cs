using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.GameLogic.Bank
{
    public class OfferRewardItemView : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _value;
        [SerializeField] private Image _img;

        public void InitProduct(int value, Sprite sprite)
        {
            gameObject.SetActive(true);
            _value.text = value.ToString();
            _img.sprite = sprite;
        }

        public void Reset()
        {
            _value.text = string.Empty;
            _img.sprite = null;
            gameObject.SetActive(false);
        }
    }
}