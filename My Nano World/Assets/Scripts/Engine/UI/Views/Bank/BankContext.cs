﻿using Assets.NanoLib.UI.Core.Signals;
using NanoLib.Core.Services.Sound;
using Assets.Scripts.Engine.Analytics;
using Assets.Scripts.Engine.Analytics.Model.api;
using Assets.Scripts.Engine.UI.Views.Popups;
using NanoReality.GameLogic.IAP;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using NanoReality.GameLogic.SoftCurrencyStore;

namespace NanoReality.GameLogic.Bank
{
    public sealed class BankContext
    {
        [Inject] public IIapService jIapService { get; set; }
        [Inject] public ISoundManager jSoundManager { get; set; }
        [Inject] public IPopupManager jPopupManager { get; set; }
        [Inject] public INanoAnalytics jNanoAnalytics { get; set; }
        [Inject] public IPlayerProfile jPlayerProfile { get; set; }
        [Inject] public IServerCommunicator jServerCommunicator { get; set; }
        [Inject] public ISoftCurrencyBundlesModel jSoftCurrencyBundlesModel { get; set; }
        [Inject] public SignalOnUIAction jSignalOnUIAction { get; set; }
        [Inject] public SoftCurrencyBoughtSignal jSoftCurrencyBoughtSignal { get; set; }
        [Inject] public PurchaseFinishedSignal jPurchaseFinishedSignal { get; set; }
        [Inject] public PurchaseOfferFinishedSignal jPurchaseOfferFinishedSignal { get; set; }
        [Inject] public ShowWindowSignal jShowWindowSignal { get; set; }
    }
}