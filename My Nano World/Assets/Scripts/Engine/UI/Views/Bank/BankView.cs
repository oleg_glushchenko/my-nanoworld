﻿using System;
using System.Collections.Generic;
using Assets.NanoLib.UI.Core.Views;
using NanoLib.Core.Pool;
using NanoReality.Engine.UI;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.GameLogic.Bank
{
    public class BankView : UIPanelView
    {
        [SerializeField] private Button _closeButton;
        [SerializeField] private List<BankTab> _tabs;
        [SerializeField] private RectTransform _tabsContent;

        [SerializeField] private TextMeshProUGUI _softCurrencyText;
        [SerializeField] private TextMeshProUGUI _hardCurrencyText;

        [SerializeField] private List<BankTabPanel> _bankTabPanels;
        [SerializeField] private RectTransform _offerTabPanels;
        [SerializeField] private Scrollbar _scrollbar;
        [SerializeField] private LoadingSpinnerV2 _spinner;

        public event Action<IBankProduct> BuyProductClicked;
        public TextMeshProUGUI SoftCurrencyText => _softCurrencyText;
        public TextMeshProUGUI HardCurrencyText => _hardCurrencyText;
        public RectTransform TabsContent => _tabsContent;
        public RectTransform OffersTabPanel => _offerTabPanels;
        public List<BankTabPanel> BankTabPanels => _bankTabPanels;
        public List<BankTab> Tabs => _tabs;
        public Scrollbar ScrollBar => _scrollbar;
        public LoadingSpinnerV2 Spinner => _spinner;

        public override void Show()
        {
            base.Show();

            _closeButton.onClick.AddListener(Hide);
        }

        public override void Hide()
        {
            foreach (var bankTab in _tabs) 
                DeactivateTab(bankTab);

            _closeButton.onClick.RemoveAllListeners();
            
            base.Hide();
        }

        public void InitItem(BankTab tab, int itemIndex, List<Sprite> sprites, IBankProduct product) 
        {
            var productView = tab.itemsPool.Pop();
            productView.transform.SetParent(tab.poolParent, false);
            productView.transform.SetSiblingIndex(itemIndex);

            var sprite = sprites.Count > itemIndex ? sprites[itemIndex] : null;

            productView.Initialize(product, sprite);
            productView.BuyBundleClick += OnBuyBundleClicked; //The subscription will be removed when the item releases to the pool

            tab.items.Add(productView);
        }

        public BankOfferView GetView(BankTab tab, OfferStyle style) 
        {
            var pools = tab.offersPools;
            if (!pools.ContainsKey(style))
            {
                var pool = new UnityPool<BankOfferView> {InstanceProvider = new PrefabInstanceProvider(tab.prefabs[(int) style - 1])};
                pools.Add(style, pool);
            }

            return pools[style].Pop();
        }

        public void OnBuyBundleClicked(IBankProduct product) 
        {
            BuyProductClicked?.Invoke(product);
        }

        private static void DeactivateTab(BankTab tab) 
        {
            if (tab != null)
            {
                foreach (var view in tab.items)
                {
                    switch (view)
                    {
                        case BankOfferView offerView:
                            tab.offersPools[offerView.Data.UiStyle].Push(offerView);
                            break;
                        
                        case BankProductView productView:
                            tab.itemsPool.Push(productView);
                            break;
                    }
                }

                tab.items.Clear();
            }
        }

        [Serializable]
        public class BankTab
        {
            public BankTabType type;
            public BankProductBase[] prefabs;
            public Transform poolParent;
            public List<Sprite> sprites;

            //Required only for offers
            public RectTransform itemRectTransform;
            public HorizontalLayoutGroup gridLayout;

            [NonSerialized] public List<BankProductBase> items = new List<BankProductBase>();
            [NonSerialized] public IUnityPool<BankProductView> itemsPool = new UnityPool<BankProductView>();
            [NonSerialized] public Dictionary<OfferStyle, IUnityPool<BankOfferView>> offersPools = new Dictionary<OfferStyle, IUnityPool<BankOfferView>>();
        }

        [Serializable]
        public class BankTabPanel
        {
            public BankTabType type;
            public Transform panelTransform;
        }
    }
}