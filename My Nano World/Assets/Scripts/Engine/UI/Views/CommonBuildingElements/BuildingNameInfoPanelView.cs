﻿using System;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using NanoReality.GameLogic.Utilities;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using UnityEngine.EventSystems;
using NanoLib.Services.InputService;
using Assets.Scripts.MultiLanguageSystem.Logic;
using NanoReality.GameLogic.Settings.BuildSettings.Models.Api;
using NanoReality.StrangeIoC;

namespace NanoReality.Engine.UI.Views
{
    public class BuildingNameInfoPanelView : AViewMediator
    {
        #region Inject

        [Inject] public SignalOnBeginTouch SignalOnBeginTouch { get; set; }
        
        #endregion

        [SerializeField] private TextMeshProUGUI _nameText;

        [SerializeField] private TextMeshProUGUI _descriptionText;

        [SerializeField] private GameObject _infoInner;

        [SerializeField] private RectTransform _smallBackgroundRect;

        [SerializeField] private Button _leftArrowButton;

        [SerializeField] private Button _rightArrowButton;

        [SerializeField] private Button _infoButton;

        [SerializeField] private int _titleHeight = 100;
        [SerializeField] private int _descriptionHeight = 200;
        [SerializeField] private LayoutElement _layoutElement;


        private Action _leftArrowClickCallback;
        private Action _rightArrowClickCallback;
        private Func<bool> _infoClickCallback;

        private bool IsDescriptionShowing => _infoInner.activeInHierarchy;

        public void Initialize(IMapBuilding building, Func<bool> infoClickCallback = null,
            Action leftArrowClickCallback = null, Action rightArrowClickCallback = null)
        {
            _nameText.SetLocalizedText(building?.Name);
            _descriptionText.SetLocalizedText(building?.Description);
            _infoClickCallback = infoClickCallback;
            _leftArrowClickCallback = leftArrowClickCallback;
            _rightArrowClickCallback = rightArrowClickCallback;

            LayoutRebuilder.ForceRebuildLayoutImmediate(_layoutElement.transform as RectTransform);
        }

        public void ChangeDescriptionShowing(bool isNeedShow)
        {
            if (isNeedShow != IsDescriptionShowing)
            {
                _infoInner.SetActive(isNeedShow);
                _infoButton.gameObject.SetActive(!isNeedShow);
                if (isNeedShow)
                {
                    SignalOnBeginTouch.AddListener(OnTouch);
                    _smallBackgroundRect.sizeDelta = new Vector2(_smallBackgroundRect.sizeDelta.x, _descriptionHeight);
                }
                else
                {
                    SignalOnBeginTouch.RemoveListener(OnTouch);
                    _smallBackgroundRect.sizeDelta = new Vector2(_smallBackgroundRect.sizeDelta.x, _titleHeight);
                }
            }
        }

        protected override void PreRegister()
        {
        }

        protected override void OnRegister()
        {
            _leftArrowButton.onClick.AddListener(OnLeftArrowClick);
            _rightArrowButton.onClick.AddListener(OnRightArrowClick);
            _infoButton.onClick.AddListener(OnInfoButtonClick);
        }

        protected override void OnRemove()
        {
            _leftArrowButton.onClick.RemoveListener(OnLeftArrowClick);
            _rightArrowButton.onClick.RemoveListener(OnRightArrowClick);
            _infoButton.onClick.RemoveListener(OnInfoButtonClick);
        }

        private void OnDisable()
        {
            ChangeDescriptionShowing(false);
        }

        private void OnLeftArrowClick()
        {
            ChangeDescriptionShowing(false);
            _leftArrowClickCallback.SafeRaise();
        }

        private void OnRightArrowClick()
        {
            ChangeDescriptionShowing(false);
            _rightArrowClickCallback.SafeRaise();
        }

        private void OnInfoButtonClick()
        {
            if (_infoClickCallback == null || _infoClickCallback())
            {
                ChangeDescriptionShowing(!IsDescriptionShowing);
            }
        }
        
        private void OnTouch()
        {
            ChangeDescriptionShowing(false);
        }
    }
}