﻿using System;
using Assets.NanoLib.Utilities.Pulls;
using Assets.Scripts.GameLogic.Utilites;
using Assets.Scripts.MultiLanguageSystem.Logic;
using NanoReality.GameLogic.InGameNotifications;
using strange.extensions.signal.impl;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Engine.UI.Views.Messages.Items
{
    public class MessageItem : GameObjectPullable
    {
        #region InspectorFields

        /// <summary>
        /// Текст кнопки
        /// </summary>
        [SerializeField]
        private TextMeshProUGUI _processMessageButtonText;

        /// <summary>
        /// Заголовок сообщения
        /// </summary>
        [SerializeField]
        private TextMeshProUGUI _titleText;

        /// <summary>
        /// Описание
        /// </summary>
        [SerializeField]
        private TextMeshProUGUI _descriptionText;

        #endregion

        public Image ItemIcon;
        public IInGameNotification Notification { get; private set; }
        public Button ClaimButton;

        private string _username;

        /// <summary>
        /// Сигнал о нажатии кнопки на сообщении
        /// </summary>
        public Signal<MessageItem> SignalOnUseNotificationClick = new Signal<MessageItem>();

        public void UpdateItem(IInGameNotification inGameNotification, string username, Sprite itemIcon)
        {
            Notification = inGameNotification;
            _username = username;

            if (itemIcon != null)
		        ItemIcon.sprite = itemIcon;

            SetLocaLizationText();
        }

        /// <summary>
        /// Выставляем тексты в зависимости от типа сообщения
        /// </summary>
        private void SetLocaLizationText()
        {
            switch(Notification.NotificationType)
            {
                case NotificationTypes.Gift:
                    _titleText.SetLocalizedText(LocalizationMLS.Instance.GetText(LocalizationKeyConstants.MESSAGES_ITEM_GIFT_TITLE_NEW_GIFT));
                    _descriptionText.SetLocalizedText(string.Format(LocalizationMLS.Instance.GetText(LocalizationKeyConstants.MESSAGES_ITEM_GIFT_DESC), _username));
                    _processMessageButtonText.SetLocalizedText(LocalizationMLS.Instance.GetText(LocalizationKeyConstants.MESSAGES_ITEM_BTN_GET));
                    break;
                case NotificationTypes.Help:
                    _titleText.SetLocalizedText(LocalizationMLS.Instance.GetText(LocalizationKeyConstants.MESSAGES_ITEM_HELP_TITLE_NEW_MESSAGE));
                    _descriptionText.SetLocalizedText(string.Format(LocalizationMLS.Instance.GetText(LocalizationKeyConstants.MESSAGES_ITEM_HELP_DESC), _username));
                    _processMessageButtonText.SetLocalizedText(LocalizationMLS.Instance.GetText(LocalizationKeyConstants.SHARED_BTN_SEND));
                    break;
                case NotificationTypes.System:
                    _titleText.SetLocalizedText(LocalizationMLS.Instance.GetText(LocalizationKeyConstants.MESSAGES_ITEM_SYSTEM_TITLE_SYSTEM_MESSAGE));
                    _processMessageButtonText.SetLocalizedText(LocalizationMLS.Instance.GetText(LocalizationKeyConstants.SHARED_BTN_OK));
                    break;
                case NotificationTypes.Pvp:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        /// <summary>
        /// Обработчик клика по кнопке
        /// </summary>
        public void OnProcessMessageButtonClick()
        {
            SignalOnUseNotificationClick.Dispatch(this);
        }
    }
}