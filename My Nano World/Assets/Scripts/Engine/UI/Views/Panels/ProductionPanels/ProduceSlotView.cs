using System;
using Assets.Scripts.GameLogic.Utilites;
using Assets.Scripts.MultiLanguageSystem.Logic;
using DG.Tweening;
using NanoLib.Core.Timers;
using NanoReality.Core.Resources;
using NanoReality.Engine.BuildingSystem.MapObjects;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Factories;
using strange.extensions.signal.impl;
using UnityEngine;
using UnityEngine.UI;
using NanoReality.Game.Tutorial;
using strange.extensions.mediation.impl;
using strange.extensions.pool.api;
using TMPro;

namespace NanoReality.Engine.UI.Views.Panels
{
    public enum SlotCondition
    {
        Standby,
        MakingProduct,
        Finished,
        WaitingResponse
    }

    public class ProduceSlotView : View, IPoolable
    {
        public event Action<ProduceSlotView> DragObjectPutted;
        public event Action<ProduceSlotView> DragObjectEnter;
        public event Action<ProduceSlotView> DragObjectExit;
      
        #region Fields
        
        private SlotCondition _slotCondition;
        [Inject] public ServerTimeTickSignal jServerTimeTickSignal { get; set; }
        [Inject] public IResourcesManager jResourcesManager { get; set; }
        [Inject] public IGlobalMapObjectViewSettings jGlobalMapObjectViewSettings { get; set; }
        [Inject] public IHardTutorial jHardTutorial { get; set; }
        [Inject] public ISkipIntervalsData jSkipIntervalsData { get; set; }

        public SlotCondition CurrentSlotCondition
        {
            get => _slotCondition;
            private set
            {
                _slotCondition = value;
                UpdateSlotState();
            }
        }

        public bool IsFirstInSlotQueue { get; set; }

        public IProduceSlot ProduceSlot { get; protected set; }
        
        public bool NeedToScale;

        [SerializeField] private DragReceiver _dragReceiver;

        [SerializeField] private Button _collectButton;
        [SerializeField] private Button _skipButton;

        [SerializeField] private TextMeshProUGUI _skipCostValue;
        
        [SerializeField] private LayoutElement _cachedLayoutElement;

        [SerializeField] private GameObject _slotArrow;

        [SerializeField] private NonMaskableImage _textureImage;

        [SerializeField] private GameObject _timeElement;

        [SerializeField] private TextMeshProUGUI _timeText;

        [SerializeField] private GameObject _slotIdleState;
        [SerializeField] private GameObject _slotProductionState;
        [SerializeField] private GameObject _slotProducedState;

        [SerializeField] private NonMaskableImage _productSlotBG;

        [SerializeField] private Color _previewImageColor;
        [SerializeField] private Color _defaultImageColor;

        [SerializeField] private float _previewAnimationScale;
        [SerializeField] private float _previewAnimationDuration;

        [SerializeField] private float _emptySlotScale = 1.1f;
        [SerializeField] private float _emptyAnimationDuration = 0.7f;

        public Signal<ProduceSlotView> SignalOnSkip = new Signal<ProduceSlotView>();
        public Signal<ProduceSlotView> SignalOnCollect = new Signal<ProduceSlotView>();

	    private Tween _previewSlotTween;
        private Tween _scaleSlotTween;

        private bool _isInitialized;
        private bool _needUpdateConfirmPopup;

        public Button SkipButton => _skipButton;
        public Button CollectButton => _collectButton;

        #endregion

        [PostConstruct]
        public void PostConstruct()
        {
            _dragReceiver.jSignalOnDragObjectEnter.AddListener(OnDragObjectEntered);
            _dragReceiver.jSignalOnGameObjectExit.AddListener(OnDragObjectExit);
            _dragReceiver.jSignalOnDragObjectPutted.AddListener(OnDragObjectPutted);
            _isInitialized = true;
            
            _collectButton.onClick.AddListener(CollectButtonClick);
            _skipButton.onClick.AddListener(SkipButtonClick);
        }

        [Deconstruct]
        public void Deconstruct()
        {
            _dragReceiver.jSignalOnDragObjectEnter.RemoveListener(OnDragObjectEntered);
            _dragReceiver.jSignalOnGameObjectExit.RemoveListener(OnDragObjectExit);
            _dragReceiver.jSignalOnDragObjectPutted.RemoveListener(OnDragObjectPutted);
            _collectButton.onClick.RemoveListener(CollectButtonClick);
            _skipButton.onClick.RemoveListener(SkipButtonClick);
        }

        #region DragHandler events

        private void OnDragObjectPutted(IDraggableItem item, DragReceiver receiver) => DragObjectPutted?.Invoke(this);

        private void OnDragObjectExit(IDraggableItem item, DragReceiver receiver) => DragObjectExit?.Invoke(this);

        private void OnDragObjectEntered(IDraggableItem item, DragReceiver receiver) => DragObjectEnter?.Invoke(this);

        #endregion

        protected override void Start()
        {
            base.Start();
            UpdateView();
        }
        
        public void SetSlot(IProduceSlot produceSlot, bool isNeedToScaleActiveSlots = false)
        {
            NeedToScale = isNeedToScaleActiveSlots;
            ProduceSlot = produceSlot;

            UpdateView();
        }

        public void UpdateView()
        {
            if(!_isInitialized)
                return;

            IProducingItemData producingItemData = ProduceSlot.ItemDataIsProducing;
            if (producingItemData == null)
            {
                CurrentSlotCondition = SlotCondition.Standby;
            }
            else
            {
                if (ProduceSlot.CurrentProducingTime <= 0)
                {
                    CurrentSlotCondition = ProduceSlot.IsFinishedProduce ? SlotCondition.Finished : SlotCondition.WaitingResponse;
                }
                else
                {
                    CurrentSlotCondition = SlotCondition.MakingProduct;
                }
                

                var currentProductTexture = jResourcesManager.GetProductSprite(producingItemData.OutcomingProducts);
                _textureImage.sprite = currentProductTexture;
            }

            OnTimerUpdate();
        }

        private void OnTimerUpdate()
        {
            if (IsFirstInSlotQueue && ProduceSlot != null)
            {
                UpdateTimer();
                UpdateSkipButtonInteractability();
            }
        }

        private void UpdateSkipButtonInteractability()
        {
            _skipButton.interactable = ProduceSlot.Id != 0;
        }

        private void UpdateSlotState()
        {
            if (!_isInitialized)
            {
                return;
            }

            // Debug.Log("UpdateSlotState: " + ProduceSlot.IsWaitingResponse + " " + gameObject.name + " SlotId: " + 
            //           ProduceSlot.Id + "State: " + CurrentSlotCondition, gameObject);
            
            switch (_slotCondition)
            {
                case SlotCondition.Standby:
                    _textureImage.gameObject.SetActive(false);

                    _slotIdleState.gameObject.SetActive(true);
                    _slotProductionState.gameObject.SetActive(false);
                    _slotProducedState.SetActive(false);

                    _skipButton.gameObject.SetActive(false);

                    _timeElement.SetActive(false);
                    CollectButton.gameObject.SetActive(false);
                    CollectButton.interactable = false;

                    if (NeedToScale)
                    {
                        _cachedLayoutElement.minWidth = jGlobalMapObjectViewSettings.SlotSizeSmall;
                        _cachedLayoutElement.minHeight = jGlobalMapObjectViewSettings.SlotSizeSmall;
                    }

                    break;

                case SlotCondition.MakingProduct:

                    if (ProduceSlot.ItemDataIsProducing == null)
                        break;

                    ProduceSlot.OnTimerTick += OnTimerUpdate;
                    bool isBig = IsFirstInSlotQueue;

                    _textureImage.gameObject.SetActive(true);

                    _slotIdleState.gameObject.SetActive(false);
                    _slotProductionState.gameObject.SetActive(true);

                    _slotProducedState.SetActive(false);

                    _skipButton.gameObject.SetActive(isBig);

                    _timeElement.SetActive(isBig);
                    _productSlotBG.enabled = isBig;

                    CollectButton.gameObject.SetActive(false);
                    CollectButton.interactable = true;

                    if (NeedToScale)
                    {
                        if (isBig)
                        {
                            _cachedLayoutElement.minWidth = jGlobalMapObjectViewSettings.SlotSizeBig;
                            _cachedLayoutElement.minHeight = jGlobalMapObjectViewSettings.SlotSizeBig;
                        }
                        else
                        {
                            _cachedLayoutElement.minWidth = jGlobalMapObjectViewSettings.SlotSizeSmall;
                            _cachedLayoutElement.minHeight = jGlobalMapObjectViewSettings.SlotSizeSmall;
                        }
                    }

                    break;

                case SlotCondition.Finished:

                    ProduceSlot.OnTimerTick -= OnTimerUpdate;
                    _textureImage.gameObject.SetActive(true);

                    _slotIdleState.SetActive(false);
                    _slotProductionState.SetActive(false);
                    _slotProducedState.SetActive(true);

                    _skipButton.gameObject.SetActive(false);

                    _timeElement.SetActive(false);

                    CollectButton.gameObject.SetActive(true);

                    CollectButton.interactable = true;

                    if (NeedToScale)
                    {
                        _cachedLayoutElement.minWidth = jGlobalMapObjectViewSettings.SlotSizeBig;
                        _cachedLayoutElement.minHeight = jGlobalMapObjectViewSettings.SlotSizeBig;
                    }

                    break;
                
                case SlotCondition.WaitingResponse:

                    _textureImage.gameObject.SetActive(true);

                    _slotIdleState.SetActive(false);
                    _slotProductionState.SetActive(false);
                    _slotProducedState.SetActive(true);

                    _skipButton.gameObject.SetActive(false);

                    _timeElement.SetActive(false);

                    CollectButton.gameObject.SetActive(false);

                    CollectButton.interactable = true;

                    if (NeedToScale)
                    {
                        _cachedLayoutElement.minWidth = jGlobalMapObjectViewSettings.SlotSizeBig;
                        _cachedLayoutElement.minHeight = jGlobalMapObjectViewSettings.SlotSizeBig;
                    }

                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void UpdateTimer()
        {
            _timeText.text = UiUtilities.GetFormattedTimeFromSeconds_M_S(ProduceSlot.CurrentProducingTime);

            var skipPrice = jSkipIntervalsData.GetCurrencyForTime((int) ProduceSlot.CurrentProducingTime);
            skipPrice = jHardTutorial.IsTutorialCompleted ? skipPrice : 0;

            _skipCostValue.SetLocalizedText(skipPrice == 0
                ? LocalizationMLS.Instance.GetText(LocalizationKeyConstants.FREE_LABEL)
                : skipPrice.ToString());
        }

        public void ShowProductPreview(Sprite productTexture)
        {
            _previewSlotTween?.Kill(true);
            _textureImage.sprite = productTexture;
            _textureImage.color = _previewImageColor;
            _textureImage.gameObject.SetActive(true);

            _slotArrow.SetActive(false);
            Vector3 previewSlotScale = _previewAnimationScale * Vector3.one;
            _previewSlotTween = _textureImage.transform.DOPunchScale(previewSlotScale, _previewAnimationDuration, 1, 0)
                .SetLoops(-1, LoopType.Yoyo);

            PlayScaleUpAnimation();
        }

        public void HideProductPreview()
        {
            Debug.Log("HideProductPreview");
            _textureImage.sprite = null;
            _textureImage.color = _defaultImageColor;
            _textureImage.transform.localScale = Vector3.one;

            KillScaleTween();

            if (_previewSlotTween != null)
            {
                _previewSlotTween.Kill(true);
                _previewSlotTween = null;
            }

            _textureImage.gameObject.SetActive(false);
            _slotArrow.SetActive(true);

            PlayScaleDownAnimation();
        }

        public void SkipButtonClick()
        {
            SignalOnSkip.Dispatch(this);
        }

        private void CollectButtonClick()
        {
            if (CurrentSlotCondition == SlotCondition.Finished)
            {
                CollectButton.interactable = false;
                SignalOnCollect.Dispatch(this);
            }
        }

        private void Cleanup()
        {
            IsFirstInSlotQueue = false;
            CurrentSlotCondition = SlotCondition.Standby;
            ProduceSlot.OnTimerTick -= OnTimerUpdate;
            ProduceSlot = null;
            _textureImage.sprite = null;
            SignalOnSkip = new Signal<ProduceSlotView>();

            DragObjectPutted = null;
            DragObjectEnter = null;
            DragObjectExit = null;

            CleanUp();
        }

        private void PlayScaleUpAnimation()
        {
            KillScaleTween();

            _scaleSlotTween = _slotIdleState.transform.DOScale(_emptySlotScale* Vector3.one, _emptyAnimationDuration)
                .SetEase(Ease.OutElastic)
                .SetLoops(1);
        }

        private void PlayScaleDownAnimation()
        {
            KillScaleTween();

            _scaleSlotTween = _slotIdleState.transform.DOScale(Vector3.one, _emptyAnimationDuration)
                .SetEase(Ease.OutExpo)
                .SetLoops(1);
        }

        private void CleanUp()
        {
            _slotIdleState.transform.localScale = Vector3.one;
        }

        private void KillScaleTween()
        {
            if (_scaleSlotTween != null)
            {
                _scaleSlotTween.Kill(true);
                _scaleSlotTween = null;
            }
        }

        #region IPoolable implementation

        public void Restore()
        {
            Cleanup();
        }

        public void Retain()
        {
            gameObject.SetActive(true);
        }

        public void Release()
        {
            gameObject.SetActive(false);
        }

        public bool retain { get; private set; }

        #endregion
    }
}