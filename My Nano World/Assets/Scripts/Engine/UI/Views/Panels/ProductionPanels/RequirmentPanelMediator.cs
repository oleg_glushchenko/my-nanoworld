using Assets.NanoLib.UI.Core;
using strange.extensions.mediation.impl;

namespace NanoReality.Engine.UI.Views.Panels
{
    public class RequirmentPanelMediator : UIMediator<RequirmentPanel> { }
}