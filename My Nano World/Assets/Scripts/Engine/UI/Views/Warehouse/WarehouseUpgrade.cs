using System;
using System.Collections.Generic;
using System.Linq;
using Assets.NanoLib.UI.Core.Signals;
using Assets.NanoLib.Utilities;
using Assets.Scripts.Engine.Analytics;
using Assets.Scripts.Engine.Analytics.Model;
using Assets.Scripts.Engine.Analytics.Model.api;
using Assets.Scripts.Engine.UI.Views.Popups;
using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using Assets.Scripts.Engine.UI.Views.Tooltips;
using Assets.Scripts.Engine.UI.Views.Warehouse;
using Assets.Scripts.GameLogic.BuildingSystem.Products.Models.api;
using NanoReality.Core.Resources;
using NanoReality.Game.Services.BuildingsUnlockService;
using NanoReality.GameLogic.BuildingSystem.CityCamera.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using NanoReality.GameLogic.GameManager.Models.api;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.PlayerProfile.Models.Impl;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using NanoReality.GameLogic.Services;
using NanoReality.UI.Components;
using strange.extensions.mediation.impl;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Engine.UI.Views.Warehouse
{
    public class WarehouseUpgrade : View
    {
        [SerializeField] private WarehouseRequiredItemCard[] _requiredItemCards;
        [SerializeField] private TextMeshProUGUI _nextLevelCapacityValue;
        [SerializeField] private TextMeshProUGUI _currentLevelCapacityValue;
        [SerializeField] private Button _upgradeButton;
        [SerializeField] private GameObject _upgradeButtonTransform;
        [SerializeField] private WarehouseView _warehouseView;
        [SerializeField] private GameObject _upgradePanelTransform;

        #region Injects

        [Inject] public WarehouseUpgradeContext jContext { get; set; }
        [Inject] public IPlayerProfile jPlayerProfile { get; set; }
        [Inject] public IMapObjectsData jMapObjectsData { get; set; }
        [Inject] public IGameManager jGameManager { get; set; }
        [Inject] public IProductsData jProducts { get; set; }
        [Inject] public IResourcesManager jResourcesManager { get; set; }
        [Inject] public IIconProvider jIconProvider { get; set; }
        [Inject] public IServerCommunicator jServerCommunicator { get; set; }
        [Inject] public IPopupManager jPopupManager { get; set; }
        [Inject] public IProductsData jProductsData { get; set; }
        [Inject] public IGameCamera jGameCamera { get; set; }
        [Inject] public INanoAnalytics jNanoAnalytics { get; set; }
        [Inject] public HideWindowSignal HideWindowSignal { get; set; }
        [Inject] public AddRewardItemSignal jAddRewardItemSignal { get;set; }
        [Inject] public SignalOnNeedToShowTooltip jSignalOnNeedToShowTooltip { get; set; }
        [Inject] public SignalOnNeedToHideTooltip jSignalOnNeedToHideTooltip { get; set; }
        [Inject] public SignalOnBuyMissingProductsAction jSignalOnBuyMissingProductsAction { get; set; }
        [Inject] public IBuildingsUnlockService jBuildingsUnlockService { get; set; }
        [Inject] public  SignalWarehouseCapacityUpgrade jSignalWarehouseCapacityUpgrade { get; set; }

        #endregion

        public Button UpgradeButton => _upgradeButton;
        public GameObject UpgradeButtonTransform => _upgradeButtonTransform;
        
        private IWarehouseMapObject _warehouse;
        private IWarehouseMapObject _nextLevelData;

        private Action _upgradeSuccessCallback;

        private Price _upgradeWarehousePrice;

        protected override void Start()
        {
            base.Start();
            _upgradeButton.onClick.AddListener(OnUpgradeButtonClick);
        }

        public void Init(Action upgradeSuccessCallback)
        {
            _upgradeSuccessCallback = upgradeSuccessCallback;
            _warehouse = jGameManager.FindMapObjectByType<IWarehouseMapObject>(MapObjectintTypes.Warehouse);
        }

        public void UpdatePanel()
        {
            var buildingId = _warehouse.ObjectTypeID;
            var nextBuildingLevel = _warehouse.Level + 1;
            var instanceIndex = _warehouse.InstanceIndex;

            _upgradeWarehousePrice = jBuildingsUnlockService.GetUpgradeBuildingPrice(buildingId, nextBuildingLevel, instanceIndex);
            // If price have default value, it means that building already have max level. So, just to disable upgrade sub panel.
            if (_upgradeWarehousePrice == default)
            {
                _upgradePanelTransform.SetActive(false);
                return;
            }

            ClearItems();
            InitializeProducts();
            UpdateLabels();
            UpdateUpgradeButton();
        }
        
        private void InitializeProducts()
        {
            var itemCardIndex = 0;
            var enoughResources = true;

            var buildingId = _warehouse.ObjectTypeID;
            var nextBuildingLevel = _warehouse.Level + 1;
            var instanceIndex = _warehouse.InstanceIndex;
            
            var price = jBuildingsUnlockService.GetUpgradeBuildingPrice(buildingId, nextBuildingLevel, instanceIndex);
            foreach (var product in price.ProductsPrice)
            {
                var item = _requiredItemCards[itemCardIndex];
                if (item != null)
                {
                    var current = jContext.GetProductCount(product.Key);
                    if (current < product.Value)
                        enoughResources = false;

                    item.SignalOnPointerUp.AddListener(OnPointerUp);
                    item.SignalOnPointerClick.AddListener(OnItemLongPress);

                    item.UpdateProduct(GetProductData(product.Key, product.Value), null);
                    item.OnBuyItemClicked += BuyProduct;

                    item.gameObject.SetActive(true);
                }

                if (++itemCardIndex == _requiredItemCards.Length)
                    break;
            }

            _upgradeButton.interactable = enoughResources;
        }
        
        private void ClearItems()
        {
            foreach (var item in _requiredItemCards)
            {
                item.Clear();
                item.SignalOnPointerUp.RemoveListener(OnPointerUp);
                item.SignalOnPointerClick.RemoveListener(OnItemLongPress);
                
                item.gameObject.SetActive(false);
            }
        }

        private void UpdateLabels()
        {
            _currentLevelCapacityValue.text = _warehouse.MaxCapacity.ToString();
            var nextBuildingLevelData = jMapObjectsData.GetNextBuildingLevelData(_warehouse);
            var capacityDelta = (nextBuildingLevelData.MaxCapacity - _warehouse.MaxCapacity);
            _nextLevelCapacityValue.text = $"+{capacityDelta}";
        }

        public void UpdateProduct(Id<Product> id)
        {
            var item = _requiredItemCards.FirstOrDefault(itemCard => itemCard.Model.ID == id.Value);
            if (item != null) 
                item.UpdateProduct(GetProductData(id, jContext.GetProductCount(id)), null);

            UpdateUpgradeButton();
        }

        private void UpdateUpgradeButton()
        {
            var isEnoughResources = true;
            foreach (var itemCard in _requiredItemCards)
            {
                if (itemCard.Model.ActualCount < itemCard.Model.RequiredCount)
                    isEnoughResources = false;
            }

            _upgradeButton.interactable = isEnoughResources;
        }

        private WarehouseRequiredItemModel GetProductData(Id<Product> id, int amount)
        {
            var productData = new WarehouseRequiredItemModel
            {
                ProductSprite = jResourcesManager.GetProductSprite(id.Value),
                SpecialIcon = jIconProvider.GetSpecialOrderIcon(jProducts.GetProduct(id).SpecialOrder),
                RequiredCount = amount,
                ActualCount = jPlayerProfile.PlayerResources.GetProductCount(id),
                MissingAmount = amount - jPlayerProfile.PlayerResources.GetProductCount(id),
                ItemCost = jProductsData.GetProduct(id).HardPrice * (amount - jPlayerProfile.PlayerResources.GetProductCount(id)),
                Product = jProducts.GetProduct(id),
                ID = id.Value
            };
            
            return productData;
        }

        private void OnItemLongPress(WarehouseRequiredItemCard pressedItem)
        {
            jSignalOnNeedToShowTooltip.Dispatch(new TooltipData
            {
                Type = TooltipType.ProductItems,
                ProductItem = pressedItem.Product,
                ProductAmount = pressedItem.ProductAmount,
                OnGoToClickAction = () => HideWindowSignal.Dispatch(typeof(WarehouseView)),
                OnTooltipShowAction = pressedItem.Select,
                OnTooltipHideAction = pressedItem.Deselect,
                ItemTransform = (RectTransform) pressedItem.transform
            });
        }

        private void OnPointerUp()
        {
            jSignalOnNeedToHideTooltip.Dispatch();
        }

        private void OnUpgradeButtonClick()
        {
            _warehouse.UpgradeObject(0);
            _upgradeSuccessCallback();

            UpdatePanel();
            
            ShowRewardAnimations();
            //_warehouseView.Hide();
        }

        private void ShowRewardAnimations()
        {
            var buildingId = _warehouse.ObjectTypeID;
            var level = _warehouse.Level;
            var instanceIndex = _warehouse.InstanceIndex;
            var amount = jBuildingsUnlockService.GetExperienceReward(buildingId, level, instanceIndex);
            
            var fromPosition = jGameCamera.ScreenToWorldPoint(_upgradeButton.gameObject.transform.position);
            jAddRewardItemSignal.Dispatch(new AddItemSettings(FlyDestinationType.Experience, fromPosition, amount));
            jSignalWarehouseCapacityUpgrade.Dispatch();
        }

        private void BuyProduct(WarehouseRequiredItemCard item)
        {
            var productData = item.Model;
            if (productData.ItemCost <= jPlayerProfile.PlayerResources.HardCurrency)
            {
                jPopupManager.Show(new PurchaseByPremiumPopupSettings(productData.ItemCost), result =>
                {
                    if (result == PopupResult.Ok)
                    {
                        UpdateLabels();
                        UpdateUpgradeButton();

                        item.SetEnough();
                        item.OnBuyItemClicked -= BuyProduct;

                        AddResourcesToWarehouse(productData);

                        jServerCommunicator.PurchaseProduct(jPlayerProfile.UserCity.Id, productData.ID,
                            productData.MissingAmount, () => 
                            {
                                jNanoAnalytics.OnPlayerCurrenciesDecrease(PlayerResources.CurencyType.Bucks,
                                    productData.ItemCost, CurrenciesSpendingPlace.WarehouseBuyProduct,
                                    productData.ID.ToString()); 
                            });
                    }
                });
            }
            else
            {
                jPopupManager.Show(new NotEnoughMoneyPopupSettings(PriceType.Hard, productData.ItemCost - jPlayerProfile.PlayerResources.HardCurrency));
            }
        }

        private void AddResourcesToWarehouse(WarehouseRequiredItemModel model)
        {
            jPlayerProfile.PlayerResources.BuyHardCurrency(model.ItemCost);
            jPlayerProfile.PlayerResources.AddProduct(model.ID, model.MissingAmount);
            jSignalOnBuyMissingProductsAction.Dispatch(new MissingProductsInfo(new Dictionary<Id<Product>, int> {{model.ID, model.MissingAmount}}, model.ItemCost));
        }
    }
}