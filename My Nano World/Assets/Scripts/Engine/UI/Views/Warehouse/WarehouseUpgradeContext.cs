using Assets.NanoLib.Utilities;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using NanoReality.GameLogic.PlayerProfile.Models.Api;

namespace Engine.UI.Views.Warehouse
{
    public class WarehouseUpgradeContext
    {
        [Inject] public virtual IPlayerProfile jPlayerProfile { get; set; }

        public Id<IUserCity> UserCityId => jPlayerProfile.UserCity.Id;
        public int HardCurrency => jPlayerProfile.PlayerResources.HardCurrency;
        public int GetProductCount(Id<Product> productId)
        {
            return jPlayerProfile.PlayerResources.GetProductCount(productId);
        }
    }
}