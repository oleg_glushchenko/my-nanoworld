﻿using System.Collections.Generic;
using Assets.NanoLib.UI.Core;
using Assets.NanoLib.Utilities;
using Assets.Scripts.Engine.Analytics;
using Assets.Scripts.Engine.Analytics.Model;
using Assets.Scripts.Engine.UI.Views.Hud;
using Assets.Scripts.Engine.UI.Views.Warehouse;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using NanoReality.GameLogic.Common.Controllers.Signals;

namespace NanoReality.Engine.UI.Extensions.MapObjects.Warehouse
{
    public class WarehousePanelMediator : UIMediator<WarehouseView>
    {
        [Inject] public SignalOnProductsStateChanged jSignalOnProductsStateChanged { get; set; }
        [Inject] public SignalOnBuyMissingProductsAction jSignalOnBuyMissingProductsAction { get; set; }
        [Inject] public OnHudBottomPanelIsVisibleSignal jOnHudBottomPanelIsVisibleSignal { get; set; }

        protected override void OnShown()
        {
            jSignalOnBuyMissingProductsAction.AddListener(OnBuyMissingProductSignal);
            jSignalOnProductsStateChanged.AddListener(OnProductsStateChanged);
            View.CloseButton.onClick.AddListener(Hide);
            
            View.WarehouseUpgrade.Init(() => View.UpdatePanel());
            View.WarehouseUpgrade.UpdatePanel();
            jOnHudBottomPanelIsVisibleSignal.Dispatch(false);
        }

        private void OnProductsStateChanged(Dictionary<Id<Product>, int> _)
        {
            View.UpdatePanel(false);
        }

        protected override void OnHidden()
        {
            jOnHudBottomPanelIsVisibleSignal.Dispatch(true);
            jSignalOnBuyMissingProductsAction.RemoveListener(OnBuyMissingProductSignal);
            jSignalOnProductsStateChanged.RemoveListener(OnProductsStateChanged);
            View.CloseButton.onClick.RemoveListener(Hide);

            base.OnHidden();
        }

        private void OnBuyMissingProductSignal(MissingProductsInfo productsInfo)
        {
            View.UpdatePanel();
            
            foreach (var product in productsInfo.ProductsList) 
                View.WarehouseUpgrade.UpdateProduct(product.Key);
        }
    }
}