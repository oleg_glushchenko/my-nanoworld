﻿using Assets.NanoLib.Utilities;
using strange.extensions.signal.impl;
using System.Collections.Generic;
using UnityEngine;

namespace NanoReality.Engine.UI.Views.Shared.Filters
{
    public class BaseFilterList : MonoBehaviour
    {
        public CacheMonobehavior CacheMonobehavior
        {
            get
            {
                return _cacheMonobehavior ?? (_cacheMonobehavior = new CacheMonobehavior(this));
            }
        }
        private CacheMonobehavior _cacheMonobehavior;

        /// <summary>
        /// Список фильтров
        /// </summary>
        public virtual List<Filter> Filters { get; set; }

        /// <summary>
        /// Происходит при смене состояния фильтра
        /// </summary>
        public Signal<BaseFilterList> SignalOnFilterChanged = new Signal<BaseFilterList>();

        protected virtual void Awake()
        {
            foreach (var filter in Filters)
            {
                filter.Toggle.onValueChanged.AddListener((state =>
                {
                    SignalOnFilterChanged.Dispatch(this);
                }));
            }
        }
    }
}
