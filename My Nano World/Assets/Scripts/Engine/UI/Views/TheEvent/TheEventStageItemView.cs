using System;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Engine.UI.Views.TheEvent
{
    public class TheEventStageItemView : MonoBehaviour
    {
        [SerializeField] private Button _claimButton;
        [SerializeField] private Button _goToButton;
        [SerializeField] private GameObject _claimObject;
        [SerializeField] private GameObject _goToObject;
        [SerializeField] private Animator _animator;
        [SerializeField] private Image image;

        public Action<TheEventStageItemView> OnClaimClickAction;
        public Action OnGoToClickAction;
        public Button GoToButton => _goToButton;

        public void OnClaimClick()
        {
            OnClaimClickAction?.Invoke(this);
        }

        public void AddListenerClaimButton()
        {
            _claimButton.onClick.RemoveAllListeners();
            _claimButton.onClick.AddListener(OnClaimClick);
        }

        public void AddListenerGoToButton()
        {
            _goToButton.onClick.RemoveAllListeners();
            _goToButton.onClick.AddListener(OnGoToClick);
        }

        public void EnableNormalColor()
        {
            image.material = null;
        }

        public void OnGoToClick()
        {
            OnGoToClickAction?.Invoke();
        }

        public void EnableClaimButton()
        {
            _goToObject.SetActive(false);
            _claimObject.SetActive(true);
        }

        public void Rise()
        {
            _animator.SetBool("rise", true);
            _animator.SetBool("collapse", false);
        }

        public void Collapse()
        {
            _animator.SetBool("collapse", true);
            _animator.SetBool("rise", false);
        }
    }
}