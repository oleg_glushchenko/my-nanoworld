﻿using System.Collections.Generic;
using NanoReality.Engine.UI.UIAnimations;
using UnityEngine;
using UnityEngine.UI;

namespace Engine.UI.UIAnimations
{
#pragma warning disable 649

    public class FadeAlphaEffect : BaseSpecialEffect
    {

        /// <summary>
        /// Объекты, которые нужно плавно включить
        /// </summary>
        // ReSharper disable once CollectionNeverUpdated.Local
        [SerializeField] private List<MaskableGraphic> _targetsToActivate;

        /// <summary>
        /// Объекты, которые нужно плавно отключить
        /// </summary>
        // ReSharper disable once CollectionNeverUpdated.Local
        [SerializeField] private List<MaskableGraphic> _targetsToDeactivate;

        [SerializeField] private float _speed;

        private float _startTime;

        public AnimationCurve FadeCurve;

        /// <summary>
        /// Необходимо ли возвращать непрозрачность в стартовое состояние по окончании эффекта
        /// </summary>
        public bool SetToNonOpacityAfterEffectEnds;

        public bool TurnOffDeactivatedObjects = false;

        public bool ForceFadeIn = false;

        /// <summary>
        /// Старт анимации
        /// </summary>
        public override void Play()
        {
            base.Play();

            _startTime = Time.time;

            foreach (var target in _targetsToActivate)
            {
                // если в объектах, которые нужно плавно включить, находим не активный
                if (!target.gameObject.activeInHierarchy || ForceFadeIn)
                {
                    // устанавливаем ему альфу в 0, что бы объект не появился 
                    // моментально непрозрачным
                    SetAlphaForMaskableGraphic(target, 0f);

                    // и активируем его
                    target.gameObject.SetActive(true);
                }
            }

            // для _targetsToDeactivate такой цикл не нужен: выключенный объект
            // в принципе и фейд-аутить смысла нет
        }

        // ReSharper disable once UnusedMember.Local
        private void Update()
        {
            if (!IsPlaying) return;

            var distCovered = (Time.time - _startTime + Time.deltaTime)*_speed;
            var evaluatedValue = FadeCurve.Evaluate(distCovered);
            var readyTargets = 0;

            foreach (var target in _targetsToActivate)
            {
                if (target.color.a > 0.98f)
                {
                    SetAlphaForMaskableGraphic(target, 1);
                    readyTargets++;
                }
                else
                    SetAlphaForMaskableGraphic(target, Mathf.Lerp(0, 1, evaluatedValue));
            }

            foreach (var target in _targetsToDeactivate)
            {
                if (target.color.a < 0.02f || !target.gameObject.activeInHierarchy)
                {
                    SetAlphaForMaskableGraphic(target, 0);
                    readyTargets++;
                }
                else
                    SetAlphaForMaskableGraphic(target, Mathf.Lerp(1, 0, evaluatedValue));
            }

            if (readyTargets >= _targetsToActivate.Count + _targetsToDeactivate.Count)
            {
                if (EffectMode == SpecialEffectMode.Loop)
                {
                    foreach (var target in _targetsToActivate)
                    {
                        SetAlphaForMaskableGraphic(target, 0);
                    }

                    foreach (var target in _targetsToDeactivate)
                    {
                        SetAlphaForMaskableGraphic(target, 255);
                    }
                    _startTime = Time.time;
                }
                else
                {
                    Stop();
                }            
            }
            
        }

        private void SetAlphaForMaskableGraphic(MaskableGraphic image, float newAlpha)
        {
            image.color = new Color(
                image.color.r,
                image.color.g,
                image.color.b,
                newAlpha);
        }

        public override void Stop()
        {
            base.Stop();

            if (!SetToNonOpacityAfterEffectEnds) return;

            foreach (var target in _targetsToActivate)
            {
                SetAlphaForMaskableGraphic(target, 255);
            }

            foreach (var target in _targetsToDeactivate)
            {
                SetAlphaForMaskableGraphic(target, 255);

                if(TurnOffDeactivatedObjects)
                    target.gameObject.SetActive(false);
            }
        }
    }
}