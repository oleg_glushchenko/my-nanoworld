﻿#pragma warning disable 649

namespace NanoReality.Engine.UI.UIAnimations
{
    public class UIPanelHideEffect : BaseSpecialEffect
    {
        public override void Play()
        {
            if (IsPlaying) return;

            PlayReverse();

            if (subEffects.Count == 0)
            {
                Stop();
            }
        }
    }
}