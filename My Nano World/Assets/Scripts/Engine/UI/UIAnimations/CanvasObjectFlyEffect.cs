﻿using Assets.NanoLib.UtilitesEngine;
using UnityEngine;

#pragma warning disable 649

namespace NanoReality.Engine.UI.UIAnimations
{
    public class CanvasObjectFlyEffect : BaseSpecialEffect
    {
        public RectTransform Target;
        public Vector2 StartPositionOffset;
        public Vector2 EndPositionOffset;

        public AnimationCurve CurveX;
        public AnimationCurve CurveY;

        [SerializeField]
        private float _speed;

        public float Speed
        {
            get { return _speed; }
        }

        private float _startTime;

        // начальная позиция анимируемого объекта (используется для возврата объекта в исходную позицию)
        private Vector2 _targetStartPosition;
        private Vector2 _startPosition;
        private Vector2 _endPosition;
        private bool _isInitialized;

        public override void Initialize()
        {
            if(_isInitialized) return;
            base.Initialize();

            _isInitialized = true;
            _targetStartPosition = Target.anchoredPosition;
        }

        public override void Play()
        {
            if (!_isInitialized)
            {
                Initialize();
            }
            
            _startPosition = _targetStartPosition + StartPositionOffset;
            _endPosition = _targetStartPosition + EndPositionOffset;
            Target.anchoredPosition = _startPosition;
            _startTime = Time.time;

            base.Play();
        }

        public override void PlayReverse()
        {
            if (!_isInitialized)
            {
                Initialize();
            }
            
            _endPosition = _targetStartPosition + StartPositionOffset;
            _startPosition = _targetStartPosition;
            Target.anchoredPosition = _startPosition;
            _startTime = Time.time;

            base.PlayReverse();
        }

        private void Update()
        {
            if (!IsPlaying) return;

            float distCovered = (Time.time - _startTime + Time.deltaTime) * _speed;

            float x = CurveX.Evaluate(distCovered);
            float y = CurveY.Evaluate(distCovered);

            var posX = Mathf.LerpUnclamped(_startPosition.x, _endPosition.x, x);
            var posY = Mathf.LerpUnclamped(_startPosition.y, _endPosition.y, y);

            Target.anchoredPosition = new Vector3(posX, posY, Target.anchoredPosition3D.z);

            if (Vector3.Distance(Target.anchoredPosition, _endPosition) < 0.1f)
            {
                Stop();
            }
        }
    }
}