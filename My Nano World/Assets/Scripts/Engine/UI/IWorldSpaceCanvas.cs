﻿using Assets.Scripts.Engine.BuildingSystem.MapObjects.Components;
using Assets.Scripts.Engine.UI.Extensions.BuildFx;
using Assets.Scripts.Engine.UI.Extensions.FireFx;
using Assets.Scripts.Engine.UI.Views.WorldSpaceUI;
using NanoReality.Engine.UI.Components;
using NanoReality.Engine.UI.Views.WorldSpaceUI;
using Assets.Scripts.Engine.UI.Extensions.Highlight;
using Assets.Scripts.Engine.UI.Extensions.TerrainFx;
using NanoReality.Engine.UI.Views.Panels;
using strange.extensions.signal.impl;
using UnityEngine;

namespace NanoReality.Engine.UI
{ 
    public sealed class UpdateWorldSpaceDepth : Signal { }
    
    public interface IWorldSpaceCanvas
    {
        BuildingStatesAttentionView BuildingAttentionViewPrefab { get; }
        RectTransform BuildingAttentionsParent { get; }
        Transform CitizensGiftsAttentionsParent { get; }
        ReadyProductView ReadyProductViewPrefab { get; }
        RectTransform ReadyProductsParent { get; }

        BuildingDowntimeEffect GetDowntimeItem();
        WorkInProgressEffect GetWorkInProgressItem();
        BuildingBuildEffect GetBuildEffect();
        HighlightEffect GetHighlightFxItem();
        FireEffect GetFireFxItem();

        TerrainEffect GetTerrainGroundFxItem();
        TerrainEffect GetTerrainDustFxItem();
        TerrainEffect GetTerrainWaterFxItem();
        TerrainEffect GetTerrainRoadFxItem();

        BuildingSpeedUpEffect GetSpeedFxItem();
        BuildingUpgradeEffect GetUpgradeFxItem();
        BuildingSatisfactionChangeEffect GetSatisfactionChangeItem();
        RoadFinishedEffect GetRoadFinishedItem();
        SubSectorUnlockEffect GetSubSectorUnlockItem();
        
        ConstructionProgressBar GetConstractionProgressBar();
        void RemoveConstractionProgressBar(ConstructionProgressBar bar);
        void OrderProgressBars();
                
        LockedSectorLabel GetLockedSubSectorBar();
        NonPlayableBuildUI GetNonPlayableBuildingBar();
        HintOverObjectUI GetHintOverOject();
        
        AoeHiglightParticle GetHappinessParticle();
        AoeHiglightParticle GetEnergyParticle();
        AoeHiglightParticle GetSupplyParticle();

        CitizenGiftAttentionView PopCitizenGiftAttention();
        LongTapProgressView PopLongTapProgress();

        void PushCitizenGiftAttention(CitizenGiftAttentionView attentionView);
        void PushLongTapProgress(LongTapProgressView longTapProgressView);

        /// <summary>
        /// Возвращает объект подсвечивающий начало / конец текущего, строящегося участва дороги
        /// </summary>
        /// <param name="isHead">Вернуть начало или конец дороги</param>
        MapObjectLooker GetEndRoadTile(bool isHead);

        PopulationInfoView GetPopulationInfoView();
        void ShowHint(Vector3 worldPoint, string text);
    }
}
