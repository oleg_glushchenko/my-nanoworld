using UnityEngine;

namespace Engine.UI
{
    public class AddItemSettings
    {
        public FlyDestinationType Target { get; }
        public Vector3 Position { get; }
        public RectTransform Destination { get; }
        public string AmountText { get; }
        public Sprite Sprite { get; set; }

        public AddItemSettings(FlyDestinationType target, Vector3 position, string amountText,
            Sprite sprite = null, RectTransform destination = null)
        {
            Target = target;
            Position = position;
            AmountText = amountText;
            Sprite = sprite;
            Destination = destination;
        }

        public AddItemSettings(FlyDestinationType target, Vector3 position, int itemsCount,
            Sprite sprite = null, RectTransform destination = null)
        {
            Target = target;
            Position = position;
            AmountText = itemsCount.ToString();
            Sprite = sprite;
            Destination = destination;
        }
    }

    public enum FlyDestinationType
    {
        SoftCoins,
        PremiumCoins,
        NanoGems,
        Experience,
        Warehouse,
        Workers,
        Citizens,
        Spend,
        NoTarget,
        GoldCurrency,
        FoodSupply,
        Lantern
    }
}