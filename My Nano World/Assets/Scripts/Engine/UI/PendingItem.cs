﻿using System;

namespace Assets.NanoLib.UI.Core
{
    public struct PendingItem
    {
        public Type Type;
        public Action CachedRequest;
    }
}