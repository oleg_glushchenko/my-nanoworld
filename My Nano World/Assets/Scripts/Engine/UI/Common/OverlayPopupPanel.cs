﻿using System;
using Assets.NanoLib.UI.Core.Views;
using DG.Tweening;
using NanoLib.UI.Core;
using NanoReality.Game.Tutorial;
using NanoReality.Game.Tutorial.HintTutorial;
using strange.extensions.mediation.impl;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Engine.UI
{
    public sealed class OverlayPopupPanel : View
    {
        [Inject] public IUIManager jUiManager { get; set; }
        [SerializeField] private Button _overlayButton;

        [SerializeField] private Image _gradientFadeImage;
        
        [SerializeField] private bool _needFadeEffect;

        [Inject] public IHardTutorial jHardTutorial { get; set; }
        [Inject] public IHintTutorial jHintTutorial { get; set; }

        private void OnEnable()
        {
            _overlayButton.onClick.AddListener(OnOverlayClicked);
        }

        private void OnDisable()
        {
            _overlayButton.onClick.RemoveListener(OnOverlayClicked);
        }

        private void OnOverlayClicked()
        {
            // TODO: workaround. disable overlay clicks for tutorial.
            if (!jHardTutorial.IsTutorialCompleted || jHintTutorial.IsHintTutorialActive )
            {
                return;
            }
            
            jUiManager.HideLastActivePanel();
        }
    }
}
