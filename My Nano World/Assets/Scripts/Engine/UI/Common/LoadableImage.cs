using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace Engine.UI.Common
{
    public class LoadableImage : Image
    {
        [SerializeField] private Material spinner;

        private Tweener _rotationTween;

        public Material Spinner
        {
            get => spinner;
            set => spinner = value;
        }

        public new Sprite sprite
        {
            get => base.sprite;
            set
            {
                base.material = defaultMaterial;
                SetMaterialDirty();
                base.sprite = value;
                if (value == null)
                {
                    material = spinner;
                }
            }
        }

        public void SetLoadingImage()
        {
            sprite = null;
            base.material = spinner;
        }
    }
}