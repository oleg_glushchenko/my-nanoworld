﻿using System;
using DG.Tweening;
using UnityEngine;

namespace NanoReality.Engine.UI.Common
{
    public class LoadingSpinner : MonoBehaviour
    {
        private Tweener _rotationTween;

        [SerializeField] private int _angle = 360;
        [SerializeField] private float _duration = 0.3f;
        [SerializeField] private RotateMode _mode = RotateMode.FastBeyond360;

        public void StartSpin()
        {
            transform.gameObject.SetActive(true);
            _rotationTween = transform.GetComponent<RectTransform>().DOLocalRotate(new Vector3(0, 0, _angle), _duration, _mode).SetLoops(-1);
        }

        public void StopSpin()
        {
            _rotationTween?.Kill();
            transform.gameObject.SetActive(false);
            transform.localRotation = Quaternion.identity;
        }

        private void OnDisable()
        {
            _rotationTween?.Kill();
        }
    }
}