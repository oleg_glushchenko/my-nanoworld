﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.NanoLib.UI.Core.Views;
using strange.extensions.signal.impl;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Engine.UI
{
	class ClickableRadialBackground : MonoBehaviour
	{
		[SerializeField]
		private Button _button;

		[SerializeField]
		private UIPanelView _panel;

		private void Awake()
		{
			_button.onClick.AddListener(() =>
			{
				_panel.Hide();
			});
		}

	}
}
