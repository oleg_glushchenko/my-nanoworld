﻿using NanoLib.Core.Services.Sound;
using NanoLib.Services.InputService;
using NanoReality.Engine.UI.Extensions;
using NanoReality.Engine.UI.Extensions.EditModePanels;
using NanoReality.Engine.UI.Views.Entertaiment;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.Happiness;
using NanoReality.GameLogic.Upgrade;

namespace NanoReality.Game.UI
{
    public class EntertainmentPanelMediator : BuildingPanelMediator<HappinessBuilding, EntertaimentPanelView>
    {
        [Inject] public SignalOnNonPlayableBuildTap jSignalOnNonPlayableBuildTap { get; set; }
        [Inject] public IEditModeService jEditModeService { get; set; }

        protected override void OnShown()
        {
            base.OnShown();
            jSignalOnNonPlayableBuildTap.AddListener(OnNonPlaybleViewTap);
            SetGrayscaleMode(true);
            ShowWindowSignal.Dispatch(typeof(EntertaimentAoeLayoutPanelView), CurrentModel);
            jSoundManager.Play(SfxSoundTypes.HospitalOpen, SoundChannel.SoundFX);
            View.UpgradeButton.Set(CurrentModel, OnUpgradeButtonClick);
            jHighlightController.EnableAoe(CurrentModel);
        }

        protected override void OnHidden()
        {
            base.OnHidden();
            jSoundManager.Play(SfxSoundTypes.CloseWindow, SoundChannel.SoundFX);
            HideWindowSignal.Dispatch(typeof(EntertaimentAoeLayoutPanelView));
            jSignalOnNonPlayableBuildTap.RemoveListener(OnNonPlaybleViewTap);

            if (jEditModeService.IsEditModeEnable && jEditModeService.IsAOEEnable) return;
            
            SetGrayscaleMode(false);
            jHighlightController.DisableAoeMode();
        }

        private void OnNonPlaybleViewTap(NonPlayableBuildingView nonPlayableBuildingView) => Hide();

        private void OnUpgradeButtonClick()
        {
            ShowWindowSignal.Dispatch(typeof(UpgradeWithProductsPanelView), CurrentModel);
            Hide();
        }
    }
}