﻿using System.Collections.Generic;
using Assets.NanoLib.UI.Core.Views;
using NanoReality.Engine.UI.Views.Energy;
using NanoReality.GameLogic.BuildingSystem.MapObjects;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.Happiness;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Engine.UI.Views.Entertaiment
{
    public class EntertaimentAoeLayoutPanelView : UIPanelView
    {
        [SerializeField] private Toggle _topRight;
        [SerializeField] private Toggle _topLeft;
        [SerializeField] private Toggle _bottomLeft;
        [SerializeField] private Toggle _bottomRight;
        [SerializeField] private Toggle _center;
        public Toggle TopRight => _topRight;
        public Toggle TopLeft => _topLeft;
        public Toggle BottomLeft => _bottomLeft;
        public Toggle BottomRight => _bottomRight;
        public Toggle Center => _center;
        public IAoeBuilding HappinessBuilding { get; private set; }
       
        protected override void Awake()
        {
            base.Awake();

            _topRight.onValueChanged.AddListener(b => OnToggleChanged(b, AoeLayout.TopRight));
            _topLeft.onValueChanged.AddListener(b => OnToggleChanged(b, AoeLayout.TopLeft));
            _bottomLeft.onValueChanged.AddListener(b => OnToggleChanged(b, AoeLayout.BottomLeft));
            _bottomRight.onValueChanged.AddListener(b => OnToggleChanged(b, AoeLayout.BottomRight));
            _center.onValueChanged.AddListener(b => OnToggleChanged(b, AoeLayout.Center));
        }
        
        protected override void OnDestroy()
        {
            base.OnDestroy();
            _topRight.onValueChanged.RemoveAllListeners();
            _topLeft.onValueChanged.RemoveAllListeners();
            _bottomLeft.onValueChanged.RemoveAllListeners();
            _bottomRight.onValueChanged.RemoveAllListeners();
            _center.onValueChanged.RemoveAllListeners();
        }

        public override void Initialize(object parameters)
        {
            base.Initialize(parameters);
            HappinessBuilding = (IAoeBuilding)parameters;

            SetActiveAoe(HappinessBuilding.AoeLayoutType);
        }

        public Toggle GetToggleFor(AoeLayout layout)
        {
            switch (layout)
            {
                case AoeLayout.BottomRight:
                    return _bottomRight;
                case AoeLayout.BottomLeft:
                    return _bottomLeft;
                case AoeLayout.TopLeft:
                    return _topLeft;
                case AoeLayout.TopRight:
                    return _topRight;
                case AoeLayout.Center:
                    return _center;
                default:
                    Debug.LogError($"Unexpected layout {layout}");
                    return null;
            }
        }

        private void SetActiveAoe(AoeLayout layout)
        {
            GetToggleFor(layout).isOn = true;
        }

        private void OnToggleChanged(bool isEnabled, AoeLayout aoeLayout)
        {
            if (HappinessBuilding == null)
                return;
            
            if (isEnabled && aoeLayout != HappinessBuilding.AoeLayoutType)
            {
                HappinessBuilding.SwitchAoeLayout(aoeLayout);
            }
        }
    }
}