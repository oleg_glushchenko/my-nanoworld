﻿using NanoReality.GameLogic.Quests;
using Assets.NanoLib.UI.Core;
using NanoLib.Core.Services.Sound;
using UnityEngine;
using NanoReality.GameLogic.GameManager.Models.api;
using NanoReality.GameLogic.StartApplicationSequence.Controllers.Signals;
using strange.extensions.mediation.impl;

namespace NanoReality.Engine.UI.Extensions.QuestPanel
{
    public class QuestPanelMediator : UIMediator<QuestPanelView>
    {
        [Inject] public SignalOnQuestUnlock jSignalOnQuestUnlock { get; set; }
        [Inject] public SignalOnQuestComplete jSignalOnQuestComplete { get; set; }
        [Inject] public SignalOnQuestStart jSignalOnQuestStart { get; set; }
        [Inject] public SignalOnQuestAchive jSignalOnQuestAchive { get; set; }
        [Inject] public SignalOnQuestsLoaded jSignalOnQuestsLoaded { get; set; }
        [Inject] public IGameManager jGameManager { get; set; }
        [Inject] public FinishApplicationInitSignal OnFinishApplicationInitSignal { get; set; }

        public override void OnRegister()
        {
            base.OnRegister();
            jSignalOnQuestUnlock.AddListener(UpdatePanelOnQuestChanged);
            jSignalOnQuestComplete.AddListener(UpdatePanelOnQuestChanged);
            jSignalOnQuestStart.AddListener(UpdatePanelOnQuestChanged);
            jSignalOnQuestAchive.AddListener(UpdatePanelOnQuestChanged);
            jSignalOnQuestsLoaded.AddListener(View.UpdatePanel);
            OnFinishApplicationInitSignal.AddOnce(View.UpdatePanel);
        }

        void UpdatePanelOnQuestChanged(IQuest sender)
        {
            if (sender == null) return;
            if (!jGameManager.IsApplicationLoaded)
                return;
            View.UpdatePanel();
        }
    }
}