﻿using NanoReality.Engine.Utilities;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Engine.UI.Views.QuestEvents
{
    public class QuestImageIcon : MonoBehaviour
    {
        [SerializeField]
        private Image _image;

        public void Set(Sprite sprite)
        {
            _image.sprite = sprite;
            _image.type = Image.Type.Simple;
            _image.preserveAspect = true;
            _image.enabled = true;
        }

        public void SetCustomMaterial(Material material)
        {
            _image.material = material;
        }
    }
}
