﻿using System;
using System.Collections.Generic;
using Assets.NanoLib.Utilities;
using Assets.Scripts.Engine.UI.Views.Hud;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using strange.extensions.signal.impl;
using UnityEngine;

/// <summary>
/// Класс вкладок квестов
/// </summary>
public class QuestPanelBookmarks : MonoBehaviour
{
    /// <summary>
    /// Список фильтров
    /// </summary>
    public List<QuestPanelFilter> Filters;

    /// <summary>
    /// Происходит при смене состояния фильтра
    /// </summary>
    public Signal<QuestPanelBookmarks> SignalOnBookmarkChanged = new Signal<QuestPanelBookmarks>();

    /// <summary>
    /// Текущая выбранная вкладка
    /// </summary>
    public QuestPanelFilter CurrentSelectedBookmark
    {
        get
        {
            foreach(var filter in Filters)
            {
                if(filter.QuestBookmarkItem.Toggle.isOn)
                    return filter;
            }

            return null;
        }
    }

    /// <summary>
    /// Выбираем нужную вкладку и сортируем
    /// </summary>
    public void SelectSectorType(BusinessSectorsTypes neededSector)
    {
        foreach(var filter in Filters)
        {
            if (filter.BusinessSectorsType == neededSector)
            {
                filter.QuestBookmarkItem.Toggle.isOn = true;
                filter.QuestBookmarkItem.Toggle.transform.SetAsFirstSibling();
            }
            else
            {
                filter.QuestBookmarkItem.Toggle.isOn = false;
                filter.QuestBookmarkItem.Toggle.transform.SetSiblingIndex((int)filter.BusinessSectorsType);
            }
        }
    }

    /// <summary>
    /// Активируем табу сектора по ID сектора
    /// </summary>
    public void CheckBookmarkStatusBySectorId(BusinessSectorsTypes availableSector)
    {
        foreach(var filter in Filters)
        {
            if(filter.BusinessSectorsType == availableSector)
            {
                filter.QuestBookmarkItem.gameObject.SetActive(true);
            }
        }
    }

    protected void Awake()
    {
        foreach(var filter in Filters)
        {
            filter.QuestBookmarkItem.Toggle.onValueChanged.AddListener((state =>
            {
                if(filter.QuestBookmarkItem.Toggle.isOn)
                    SignalOnBookmarkChanged.Dispatch(this);
            }));
        }
    }
}

[Serializable]
public class QuestPanelFilter
{
    public BusinessSectorsTypes BusinessSectorsType;
    public HudNotificationButton Notifications;
    public string QuestPanelTitle;
    public QuestBookmarkItem QuestBookmarkItem;
}