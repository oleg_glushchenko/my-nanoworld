using Assets.NanoLib.UI.Core;
using Assets.Scripts.Engine.UI.Views.Hud;
using NanoReality.Engine.UI.Extensions;
using NanoReality.Engine.UI.Views.Panels;
using NanoReality.GameLogic.BuildingSystem.CityCamera.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;

namespace NanoReality.GameLogic.Upgrade
{
    public sealed class UpgradePanelMediator : UIMediator<UpgradePanelView>
    {
        protected IMapBuilding CurrentModel { get; private set; }
        
        [Inject] public OnHudBottomPanelIsVisibleSignal jOnHudBottomPanelIsVisibleSignal { get; set; }
        [Inject] public OnHudButtonsIsClickableSignal jOnHudButtonsIsClickableSignal { get; set; }
        [Inject] public SignalOnMapMovedUnderTouch jSignalOnMapMovedUnderTouch { get; set; }
        [Inject] public IGameCamera jGameCamera { get; set; }

        
        protected override void Show(object param)
        {
            CurrentModel = (IMapBuilding) param;
            base.Show(param);
            FocusOnBuilding();
        }
        
        protected override void OnShown()
        {
            base.OnShown();
            jOnHudBottomPanelIsVisibleSignal.Dispatch(false);
            jOnHudButtonsIsClickableSignal.Dispatch(false);
            FocusOnBuilding();
        }

        protected override void OnHidden()
        {
            jOnHudBottomPanelIsVisibleSignal.Dispatch(true);
            jOnHudButtonsIsClickableSignal.Dispatch(true);
            base.OnHidden();
        }
        
        protected void FocusOnBuilding()
        {
            jGameCamera.FocusOnMapObject(CurrentModel);
            jSignalOnMapMovedUnderTouch.Dispatch();
        }
    }
}