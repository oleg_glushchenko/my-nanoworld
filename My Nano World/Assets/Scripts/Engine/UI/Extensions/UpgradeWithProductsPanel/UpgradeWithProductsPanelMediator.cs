using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.NanoLib.UtilitesEngine.CustomDebugging.Models.impl;
using Assets.NanoLib.Utilities;
using Assets.Scripts.Engine.Analytics;
using Assets.Scripts.Engine.Analytics.Model;
using Assets.Scripts.Engine.Analytics.Model.api;
using Assets.Scripts.Engine.UI.Views.Hud;
using Assets.Scripts.Engine.UI.Views.Popups;
using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using Assets.Scripts.Engine.UI.Views.Tooltips;
using Assets.Scripts.GameLogic.BuildingSystem.Products.Models.api;
using Assets.Scripts.GameLogic.Utilites;
using NanoLib.Core.Services.Sound;
using NanoReality.Engine.UI.Extensions;
using NanoReality.Engine.UI.Views.NotEnoughtResourcesPanel;
using NanoReality.Game.Services.BuildingsUnlockService;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using NanoReality.GameLogic.Common.Controllers.Signals;
using NanoReality.GameLogic.GameManager.Models.api;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.PlayerProfile.Models.Impl;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using NanoReality.GameLogic.TheEvent.Signals;
using strange.extensions.signal.impl;
using UnityEngine;

namespace NanoReality.GameLogic.Upgrade
{
    public class SignalOnUpgradeProductPushed : Signal { }

    public sealed class UpgradeWithProductsPanelMediator : BuildingPanelMediator<IMapBuilding, UpgradeWithProductsPanelView>
    {
        [Inject] public IMapObjectsData jMapObjectsData { get; set; }
        [Inject] public IProductsData jProductsData { get; set; }
        [Inject] public IServerCommunicator jServerCommunicator { get; set; }
        [Inject] public IPlayerProfile jPlayerProfile { get; set; }
        [Inject] public IBuildingsUnlockService jBuildingUnlockService { get; set; }
        [Inject] public INanoAnalytics jNanoAnalytics { get; set; }
        [Inject] public SignalOnBuyMissingProductsAction jSignalOnBuyMissingProductsAction { get; set; }
        [Inject] public StartSpentProductsAnimationSignal jStartSpentProductsAnimationSignal { get; set; }
        [Inject] public SignalOnUpgradeProductPushed jSignalOnUpgradeProductPushed { get; set; }
        [Inject] public OnHudBottomPanelIsVisibleSignal jOnHudBottomPanelIsVisibleSignal { get; set; }
        [Inject] public OnHudButtonsIsClickableSignal jOnHudButtonsIsClickableSignal { get; set; }
        [Inject] public SignalOnProductsStateChanged jSignalOnProductsStateChanged { get; set; }
        [Inject] public SignalOnLanternCurrencyStateChanged jSignalOnLanternCurrencyStateChanged { get; set; }
        [Inject] public SignalTheEventIsEnds jSignalTheEventIsEnds { get; set; }

        protected override void OnShown()
        {
            base.OnShown();

            jSignalOnProductsStateChanged.AddListener(OnProductsStateChanged);
            View.ItemReceived += ItemReceived;
            jSignalOnLanternCurrencyStateChanged.AddListener(OnChangeLanternCurrency);

            jOnHudBottomPanelIsVisibleSignal.Dispatch(false);
            jOnHudButtonsIsClickableSignal.Dispatch(false);
            SetGrayscaleMode(true);

            jSoundManager.Play(SfxSoundTypes.BuildingConstruction, SoundChannel.SoundFX);

            Initialize();

            if (CurrentModel is IEventBuilding)
            {
                jSignalTheEventIsEnds.AddListener(Hide);
            }
        }

        protected override void OnHidden()
        {
            base.OnHidden();

            jSignalOnProductsStateChanged.RemoveListener(OnProductsStateChanged);
            View.ItemReceived -= ItemReceived;
            jSignalOnLanternCurrencyStateChanged.RemoveListener(OnChangeLanternCurrency);

            jOnHudBottomPanelIsVisibleSignal.Dispatch(true);
            jOnHudButtonsIsClickableSignal.Dispatch(true);
            SetGrayscaleMode(false);

            if (CurrentModel is IEventBuilding)
            {
                jSignalTheEventIsEnds.RemoveListener(Hide);
            }
        }

        private void Initialize()
        {
            var model = CurrentModel;

            View.NeedToShowRewards = UpdateView(model);
            View.PanelTitle = model.Name;

            InitializeProductItems(model);
            UpdateProgressBarValue();

            if (model is IEventBuilding)
            {
                View.EnableLanternsCounter(true);
                View.SetLanternAmount(jPlayerProfile.PlayerResources.LanternCurrency);
            }
            else
            {
                View.EnableLanternsCounter(false);
            }

        }

        private void InitializeProductItems(IMapBuilding mapBuilding)
        {
            var items = View.UpgradeItems;

            var requiredProducts = mapBuilding.ProductsForUpgrade;
            if (requiredProducts != null && requiredProducts.Count > 0)
            {
                for (var itemIndex = 0; itemIndex < items.Count; itemIndex++)
                {
                    if (itemIndex >= requiredProducts.Count)
                        items[itemIndex].SetEmpty();
                    else
                    {
                        items[itemIndex].SetProduct(requiredProducts[itemIndex]);

                        items[itemIndex].DragStarted -= OnDragStartedCallback;
                        items[itemIndex].OnDragEndedSignal.RemoveListener(OnDragEndedCallback);
                        items[itemIndex].OnItemClickSignal.RemoveListener(OnItemClickCallback);

                        items[itemIndex].DragStarted += OnDragStartedCallback;
                        items[itemIndex].OnDragEndedSignal.AddListener(OnDragEndedCallback);
                        items[itemIndex].OnItemClickSignal.AddListener(OnItemClickCallback);
                    }
                }
            }
            else
            {
                foreach (var draggable in items)
                    draggable.SetEmpty();
            }
        }

        private void ItemReceived(DraggableUpgradeItem upgradeItem)
        {
            var productForUpgrade = upgradeItem.CurrentProductForUpgrade;
            if (productForUpgrade.IsLoaded) return;

            var productsPrice = new Dictionary<Id<Product>, int> { { productForUpgrade.ProductID, productForUpgrade.ProductsCount } };

            var currentProductsCount = jPlayerProfile.PlayerResources.GetProductCount(productForUpgrade.ProductID);
            if (currentProductsCount < productForUpgrade.ProductsCount)
            {
                var price = new Price
                {
                    PriceType = PriceType.Resources,
                    ProductsPrice = productsPrice
                };

                if (CurrentModel is EventBuilding building)
                {
                    jPopupManager.Show(new NotEnoughResourcesPopupSettings(price, true, building.LanternPrice), popupResult =>
                    {
                        if (popupResult == PopupResult.Ok)
                        {
                            PushProduct(upgradeItem, productsPrice);
                        }
                    });
                }
                else
                {
                    jPopupManager.Show(new NotEnoughResourcesPopupSettings(price), popupResult =>
                    {
                        if (popupResult == PopupResult.Ok)
                        {
                            PushProduct(upgradeItem, productsPrice);
                        }
                    });
                }
            }
            else
            {
                PushProduct(upgradeItem, productsPrice);
            }
        }

        private void PushProduct(DraggableUpgradeItem upgradeItem, Dictionary<Id<Product>, int> productsPrice)
        {
            var targetBuilding = CurrentModel;
            var product = upgradeItem.CurrentProductForUpgrade;
            product.IsLoaded = true;

            jPlayerProfile.PlayerResources.RemoveProduct(product.ProductID, product.ProductsCount);

            Vector3 position;

            if (targetBuilding is IEventBuilding)
            {
                var model = jGameManager.FindMapObjectByType<IMapObject>(MapObjectintTypes.EventBuilding);
                var view = jGameManager.GetMapObjectView(model);
                position = jGameCamera.WorldToScreenPoint(view.transform.position);
            }
            else
            {
                position = jGameCamera.WorldToScreenPoint(jGameManager.GetMapView(targetBuilding.SectorId).GetMapObjectById(targetBuilding.MapID).transform.position);
            }

            jStartSpentProductsAnimationSignal.Dispatch(position, productsPrice);

            jSignalOnUpgradeProductPushed.Dispatch();

            jServerCommunicator.PushUpgradeProduct(targetBuilding.GetMapIDString, product.ProductID, null);

            if (View.UpgradeItems.All(item => item.CurrentProductForUpgrade == null || item.CurrentProductForUpgrade.IsLoaded))
            {
                jSoundManager.Play(SfxSoundTypes.BuildingUpgrade, SoundChannel.SoundFX);

                if (targetBuilding is IEventBuilding)
                {
                    jGameManager.GlobalCityMap.UpgradeObject(targetBuilding.MapID);
                }
                else
                {
                    jGameManager.GetMap(targetBuilding.SectorId).UpgradeObject(targetBuilding.MapID, true);
                }

                Hide();
            }

            var updatedProduct = targetBuilding.ProductsForUpgrade.FirstOrDefault(requiredProduct => requiredProduct.ProductID == product.ProductID);
            upgradeItem.SetProduct(updatedProduct);
        }

        private bool UpdateView(IMapBuilding mapBuilding)
        {
            var buildingId = mapBuilding.ObjectTypeID;
            var currentBuildingLevel = mapBuilding.Level;
            var nextBuildingLevel = currentBuildingLevel + 1;
            var instanceIndex = mapBuilding.InstanceIndex;

            var nextLevelObject = jMapObjectsData.GetByBuildingIdAndLevel(buildingId, nextBuildingLevel);
            if (nextLevelObject == null) return false;

            switch (mapBuilding.MapObjectType)
            {
                case MapObjectintTypes.DwellingHouse:
                    var nextPopulation = jBuildingUnlockService.GetPopulationForDwelling(buildingId, nextBuildingLevel, instanceIndex);
                    var currentPopulation = jBuildingUnlockService.GetPopulationForDwelling(buildingId, currentBuildingLevel, instanceIndex);
                    View.NewPopulationCount = $"+{nextPopulation - currentPopulation}";
                    View.SetIcon(typeof(IDwellingHouseMapObject));
                    break;

                case MapObjectintTypes.ServiceBuilding:
                    View.SetIcon(typeof(IAoeBuilding));
                    break;
            }

            return true;
        }

        private void UpdateProgressBarValue()
        {
            var requiredProducts = CurrentModel.ProductsForUpgrade;

            var filledAmount = 0f;
            if (requiredProducts != null && requiredProducts.Count > 0)
            {
                var loadedProductsCount = requiredProducts.Count(product => product != null && product.IsLoaded);
                filledAmount = loadedProductsCount / (float)requiredProducts.Count;
            }

            View.SetProgressBarValue(filledAmount);
        }

        private void ShowGoToTooltip(DraggableItem targetItem)
        {
            var upgradeItem = (DraggableUpgradeItem)targetItem;
            var tooltipData = new TooltipData
            {
                ItemTransform = targetItem.ItemImage.rectTransform,
                Type = TooltipType.ProductItems,
                ProductItem = jProductsData.GetProduct(upgradeItem.CurrentProductForUpgrade.ProductID),
                Message = LocalizationMLS.Instance.GetText(LocalizationKeyConstants.SHARED_TOOLTIP_LABEL_NANO_PREM)
            };

            if (CurrentModel is EventBuilding building && upgradeItem.CurrentStatus == DraggableUpgradeItemStatus.ProductsNotEnought)
            {
                tooltipData.LanternCost = building.LanternPrice;
                tooltipData.OnBuyForLanterns += OnBuyForLanternsClick;
            }

            void OnBuyForLanternsClick()
            {
                var productForUpgrade = upgradeItem.CurrentProductForUpgrade;
                if (productForUpgrade.IsLoaded) return;

                var productsPrice = new Dictionary<Id<Product>, int> { { productForUpgrade.ProductID, productForUpgrade.ProductsCount } };

                var price = new Price
                {
                    PriceType = PriceType.Resources,
                    ProductsPrice = productsPrice
                };

                if (building.LanternPrice <= jPlayerProfile.PlayerResources.LanternCurrency)
                {

                    jServerCommunicator.PurchaseEventProduct(price.ProductsPrice.First().Key, price.ProductsPrice.First().Value);
                    jPlayerProfile.PlayerResources.AddProducts(price.ProductsPrice);
                    jPlayerProfile.PlayerResources.RemoveLanternCurrency(building.LanternPrice);

                    var products = new StringBuilder();
                    foreach (var product in price.ProductsPrice)
                    {
                        products.Append(product.Key.Value + " ");
                    }

                    jNanoAnalytics.OnPlayerCurrenciesDecrease(PlayerResources.CurencyType.Lanterns, building.LanternPrice, CurrenciesSpendingPlace.NotEnoughtResourcesBuy, products.ToString());
                    jSignalOnBuyMissingProductsAction.Dispatch(new MissingProductsInfo(price.ProductsPrice, building.LanternPrice));

                    PushProduct(upgradeItem, productsPrice);

                    return;
                }

                jPopupManager.Show(new NotEnoughMoneyPopupSettings(PriceType.Lantern, building.LanternPrice));
            }

            tooltipData.OnTooltipHideAction += OnHideToolTip; 
            tooltipData.OnTooltipShowAction += OnShowToolTip; 

            View.ProductGoToTooltip.Show(tooltipData);
        }

        private void OnHideToolTip()
        {
            View.SupplyInfoPanel.SetActive(true);
        }

        private void OnShowToolTip()
        {
            View.SupplyInfoPanel.SetActive(false);
        }

        private void HideGoToTooltip()
        {
            if (View.ProductGoToTooltip.IsVisible)
            {
                View.ProductGoToTooltip.Hide();
            }
        }

        private void OnProductsStateChanged(Dictionary<Id<Product>, int> products)
        {
            UpdateProgressBarValue();
        }

        private void OnDragStartedCallback(DraggableItem draggableItem)
        {
            View.DragReceiverImage.raycastTarget = true;

            HideGoToTooltip();
        }

        private void OnDragEndedCallback()
        {
            View.DragReceiverImage.raycastTarget = false;
        }

        private void OnItemClickCallback(DraggableItem item)
        {
            jSoundManager.Play(SfxSoundTypes.CollectItems, SoundChannel.SoundFX);

            ShowGoToTooltip(item);
        }

        private void OnChangeLanternCurrency(int amount)
        {
            if (CurrentModel is EventBuilding)
            {
                View.SetLanternAmount(amount);
            }
        }
    }
}