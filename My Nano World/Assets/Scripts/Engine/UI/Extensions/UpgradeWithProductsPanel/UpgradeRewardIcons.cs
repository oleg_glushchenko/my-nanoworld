﻿using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace NanoReality.GameLogic.Upgrade
{
    [CreateAssetMenu(fileName = "UpgradeRewardIcons", menuName = "NanoReality/UI/Upgrade reward icons", order = 8)]
    public class UpgradeRewardIcons : SerializedScriptableObject
    {
        [SerializeField] private Sprite _defaultIcon;
        [SerializeField] private Dictionary<Type, Sprite> _rewardIcons;

        public Sprite GetIcon(Type type)
        {
            return _rewardIcons.ContainsKey(type) ? _rewardIcons[type] : _defaultIcon;
        }
    }
}