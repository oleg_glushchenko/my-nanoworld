using Assets.NanoLib.UI.Core;
using strange.extensions.mediation.impl;

namespace Engine.UI.Extensions.RepairBuildingPanel
{
    public class RepairBuildingPanelMediator : UIMediator<RepairBuildingPanel> { }
}