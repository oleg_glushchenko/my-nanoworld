﻿using Assets.NanoLib.UI.Core.Views;
using NanoReality.Engine.UI.UIAnimations;
using TMPro;
using UnityEngine;
using System;
using I2.Loc;
using UnityEngine.UI;

namespace NanoReality.Engine.UI.Extensions.LoaderPanel
{
    public class LoaderPanelView : UIPanelView
    {
        [SerializeField] private HorizontalProgressBar _fillImage;
        [SerializeField] private TextMeshProUGUI _percent;
        [SerializeField] private GameObject _preloaderHolder;
        [SerializeField] private PreloaderCloudAnimation _cloudAnimation;
        [SerializeField] private Image _logoImage;
        [SerializeField] private Image _backgroundImage;
        [SerializeField] private GameObject _loadingAssetsPanel;
        [SerializeField] private TextMeshProUGUI _loadingAssetsLabel;
        [SerializeField] private TextMeshProUGUI _loadedAssetsText;
        [SerializeField] private Localize _localizeHint;

        public event Action PreloaderCloudShowEnded;

        public override void Hide()
        {
            base.Hide();
            _backgroundImage.gameObject.SetActive(false);
            _logoImage.gameObject.SetActive(false);
            _preloaderHolder.SetActive(false);
        }

        public void StartCurrentCloudAnimation(bool isReload)
        {
            _cloudAnimation.EffectStopped.AddListener(OnBeginCloudAnimationFinish);

            if (!isReload)
            {
                SetActiveUi(false);
                _cloudAnimation.gameObject.transform.SetAsFirstSibling();
            }
            else
                _cloudAnimation.gameObject.transform.SetAsLastSibling();

            _cloudAnimation.Play();
        }

        public void PlayReverseCloudAnimation()
        {
            SetActiveUi(false);
            _cloudAnimation.PlayReverse();
        }

        private void OnBeginCloudAnimationFinish(ISpecialEffect specialEffect)
        {
            _cloudAnimation.EffectStopped.RemoveListener(OnBeginCloudAnimationFinish);
            SetActiveUi(true);
            PreloaderCloudShowEnded?.Invoke();
        }

        public void SetHintsText(string hintTextKey)
        {
            _localizeHint.Term = hintTextKey;
        }

        public void UpdateProgressbar(float percent)
        {
            _fillImage.FillAmount = percent;
            _percent.text = $"{Convert.ToInt32(percent * 100)}%";
        }

        private void SetActiveUi(bool isActive)
        {
            _backgroundImage.gameObject.SetActive(isActive);
           // _logoImage.gameObject.SetActive(isActive);
            _preloaderHolder.SetActive(isActive);
        }

        public void SetActiveLoadingAssets(bool isVisible)
        {
            _loadingAssetsPanel.SetActive(isVisible);
        }
        
        public void SetLoadingAssetsText(string text)
        {
            _loadingAssetsLabel.text = text;
        }

        public void SetLoadedAssetsText(string text)
        {
            _loadedAssetsText.text = text;
        }
    }
}