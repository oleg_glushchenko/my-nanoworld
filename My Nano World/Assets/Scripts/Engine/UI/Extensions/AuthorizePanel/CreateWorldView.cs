﻿using System;
using Assets.NanoLib.UI.Core.Views;
using UnityEngine;

namespace NanoReality.Engine.UI.Extensions.Authorize
{
    public class CreateWorldView : UIPanelView
    {
        [SerializeField] private CityShortDataView _currentCity;
        [SerializeField] private Sprite _createCitySprite;
        
        public CityShortDataView CurrentCity => _currentCity;
        public Sprite CreateCitySprite => _createCitySprite;

        public event Action OnKeepCurrentButtonClick;
        public event Action NewWorldButtonClick;

        public void KeepCurrentButtonClick()
        {
            OnKeepCurrentButtonClick?.Invoke();
        }

        public void OnNewWorldButtonClick()
        {
            NewWorldButtonClick?.Invoke();
        }
    }
}