﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Engine.UI.Extensions.QuestsBuildingPanel;
using Assets.Scripts.Engine.UI.Extensions.QuestsBuildingPanel.ConditionLinks;
using Assets.Scripts.Engine.UI.Views.Popups;
using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using DG.Tweening;
using NanoReality.Engine.UI.Extensions.QuestsBuildingPanel;
using NanoReality.Engine.UI.Views.QuestEvents;
using NanoReality.Game.Tutorial;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.Quests;
using NanoReality.GameLogic.Services;
using NanoReality.GameLogic.Utilites;
using strange.extensions.mediation.impl;
using strange.extensions.signal.impl;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Engine.UI.Extensions.Hud
{
    public class QuestView : View
    {
        [SerializeField] private Button ClaimButton;
        [SerializeField] private Button CloseButton;
        [SerializeField] private Button GoToButton;

        [SerializeField] private TextMeshProUGUI HeaderTitle;
        [SerializeField] private TextMeshProUGUI RewardText;

        [SerializeField] private QuestConditionInfoView ConditionInfoView;
        [SerializeField] private CanvasGroup ConditionsCanvasGroup;
        [SerializeField] private QuestImageIcon QuestImage;
        [SerializeField] private QuestRewardItemView RewardItem;

        [SerializeField] private Image TopSprite;
        [SerializeField] private Image RewardSprite;
        [SerializeField] private Image Toggle;

        [SerializeField] private RectTransform TailRect;
        
        [SerializeField] private List<SectorSprite> SectorSprites = new List<SectorSprite>();
        [SerializeField] private List<float> TailPositions = new List<float>();

        
        public event Action OnClosedView;
        public Button Claim => ClaimButton;
        public Button Close => CloseButton;
        
        private IQuest _currentQuest;
        private ICondition _condition;

        [Inject] public IIconProvider jIconProvider { get; set; }
        [Inject] public IHardTutorial jHardTutorial { get; set; }
        [Inject] public IConditionTaskRouter jConditionTaskRouter { get; set; }

        private void OnEnable()
        {
            ClaimButton.onClick.AddListener(ClaimReward);
            CloseButton.onClick.AddListener(OnCloseButtonClick);
            GoToButton.onClick.AddListener(OnConditionToTask);
        }

        private void OnDisable()
        {
            ClaimButton.onClick.RemoveListener(ClaimReward);
            CloseButton.onClick.RemoveListener(OnCloseButtonClick);
            GoToButton.onClick.RemoveListener(OnConditionToTask);
            OnClosedView = null;
        }

        private void OnCloseButtonClick() => CloseView();

        public void SetInfo(IQuest quest, int buttonIndex)
        {
            OpenView();
            UpdateView(quest.BusinessSector);
            _currentQuest = quest;
            
            TailRect.anchoredPosition = new Vector3(TailRect.anchoredPosition.x, TailPositions[buttonIndex], 0);

            HeaderTitle.text = GetHeaderTitle(_currentQuest.QuestId.ToString());
                
            jIconProvider.SetQuestImageIcon(QuestImage, _currentQuest);

            _condition = _currentQuest.AchiveConditionsStates.FirstOrDefault();

            ConditionInfoView.SetInfo(_condition);

            bool canClaim = quest.QuestState == UserQuestStates.Achived;

            ClaimButton.gameObject.SetActive(canClaim);
            GoToButton.gameObject.SetActive(!canClaim);

            SetConditionsInteraction(jHardTutorial.IsTutorialCompleted);
            ShowRewards();
        }

        private string GetHeaderTitle(string questId)
        {
            string headerTitle = LocalizationUtils.GetSectorTypeText(_currentQuest.BusinessSector);
#if !PRODUCTION
            headerTitle += $"_{questId}";
#endif
            return headerTitle;
        }
        
        private void UpdateView(BusinessSectorsTypes sectorsType)
        {
            if (_currentQuest != null && sectorsType == _currentQuest.BusinessSector) return;

            SectorSprite sectorSprite = GetSectorSprite(sectorsType);
            TopSprite.sprite = sectorSprite.TopSprite;
            RewardSprite.sprite = sectorSprite.RewardSprite;
            Toggle.sprite = sectorSprite.Toggle;
            RewardText.color = sectorSprite.TextColor;
        }

        private SectorSprite GetSectorSprite(BusinessSectorsTypes sectorsTypes)
        {
            return SectorSprites.Find(s => s.SectorsTypes == sectorsTypes);
        }

        private void SetConditionsInteraction(bool interactive)
        {
            ConditionsCanvasGroup.blocksRaycasts = interactive;
            ConditionsCanvasGroup.interactable = interactive;
        }

        private void ClaimReward()
        {
            CloseView();
            _currentQuest.CollectAward();
        }

        private void ShowRewards()
        {
            List<IAwardAction> questRewards = _currentQuest.AwardActions;
            int rewardsCount = questRewards.Count;

            bool isActive;
            Sprite sprite;
            string countText;

            if (rewardsCount > 0)
            {
                IAwardAction reward = questRewards.FirstOrDefault();
                sprite = jIconProvider.GetSprite(reward);
                isActive = true;
                countText = reward.AwardCount.ToString();
            }
            else
            {
                sprite = null;
                isActive = false;
                countText = string.Empty;
            }

            RewardItem.SetActive(isActive);
            RewardItem.SetReward(sprite, countText);

        }

        private void OpenView()
        {
            gameObject.SetActive(true);
            var rect = gameObject.transform as RectTransform;
            rect.localScale = Vector3.zero;
            rect.DOScale(Vector3.one, .5f).SetEase(Ease.OutBounce);
        }

        public void CloseView()
        {
            var rect = gameObject.transform as RectTransform;
            rect.DOScale(Vector3.zero, .5f).SetEase(Ease.InBack).OnComplete(() =>
            {
                OnClosedView?.Invoke();
                gameObject.SetActive(false);
            });
        }

        private void OnConditionToTask()
        {
            if (_condition.IsAchived || !jConditionTaskRouter.HasTask(_condition))
            {
                return;
            }

            jConditionTaskRouter.ChargeForTask(_condition);
            jConditionTaskRouter.ExecuteChargedTask();
            CloseView();
        }

        public void SetActive(bool isActive)
        {
            gameObject.SetActive(isActive);
        }

        [Serializable]
        public class SectorSprite
        {
            public BusinessSectorsTypes SectorsTypes;
            public Sprite TopSprite;
            public Sprite RewardSprite;
            public Sprite Toggle;
            public Color TextColor;
        }
    }
}