﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Engine.UI.Extensions.QuestsBuildingPanel
{
    public sealed class QuestRewardItemView : MonoBehaviour
    {
        [SerializeField] private Image _rewardTypeImage;
        [SerializeField] private TextMeshProUGUI _countText;

        public void SetActive(bool isActive)
        {
            gameObject.SetActive(isActive);
        }

        public void SetReward(Sprite rewardSprite, string countText)
        {
            _rewardTypeImage.sprite = rewardSprite;
            _countText.text = countText;
        }
    }
}