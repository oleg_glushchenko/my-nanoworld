﻿using NanoReality.GameLogic.Quests;

namespace Assets.Scripts.Engine.UI.Extensions.QuestsBuildingPanel.ConditionLinks
{
    public interface IConditionTaskRouter
    {
        bool HasTask(ICondition condition);
        bool ChargeForTask(ICondition condition);
        void ExecuteChargedTask();
    }
}
