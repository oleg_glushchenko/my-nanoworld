﻿using Assets.Scripts.Engine.UI.Extensions.QuestsBuildingPanel.ConditionLinks.TaskLinks;
using Engine.UI.Extensions.QuestsBuildingPanel.ConditionLinks.TaskLinks;
using NanoLib.Core.Logging;
using NanoReality.GameLogic.Quests;

namespace Assets.Scripts.Engine.UI.Extensions.QuestsBuildingPanel.ConditionLinks
{
    public class ConditionTaskRouter : IConditionTaskRouter
    {
        private ITaskLink _chargedTask;

        [Inject] public IOpenBuildingsShopLink jOpenBuildingsShopLink { get; set; }
        [Inject] public IOpenProductProduceLink jOpenProductProduceLink { get; set; }
        [Inject] public IOpenOrderDeskWithSectorkLink jOpenOrderDeskWithSectorkLink { get; set; }
        [Inject] public IOpenOrderDeskLink jOpenOrderDeskLink { get; set; }
        [Inject] public IOpenAuctionLink jOpenAuctionLink { get; set; }
        [Inject] public ICityRenameLink jCityRenameLink { get; set; }
        [Inject] public ICollectCoinsFromCommercialBuildingLink jCollectCoinsFromCommercialBuildingLink { get; set; }
        [Inject] public IAuctionCoinsLink jAuctionCoinsLink { get; set; }
        [Inject] public ICollectTaxesLink jCollectTaxesLink { get; set; }
        [Inject] public IGoToLockedSectorLink jGoToLockedSectorLink { get; set; }
        [Inject] public ICoverBuildingsWithAreaOfEffectTask jCoverBuildingsWithAreaOfEffectTask { get; set; }
        [Inject] public IUpgradeBuildingTask jUpgradeBuildingTask { get; set; }
        [Inject] public IOpenBazaarLink jOpenBazaarLink { get; set; }
        [Inject] public IOpenFoodSupplyLink jOpenFoodSupplyLink { get; set; }
        [Inject] public IPopulationAchievedLink jPopulationAchievedLink { get; set; }

        public bool HasTask(ICondition condition)
        {
            return GetTaskLink(condition) != null;
        }

        public bool ChargeForTask(ICondition condition)
        {
            if (condition != null)
            {
                var task = GetTaskLink(condition);
                if (task != null && task.Setup(condition))
                {                    
                    _chargedTask = task;
                    return true;                    
                }                
            }
            
            return false;
        }

        public void ExecuteChargedTask()
        {
            if (_chargedTask != null)
            {
                _chargedTask.Execute();
                _chargedTask = null;
            }
        }

        private ITaskLink GetTaskLink(ICondition condition)
        {
            switch (condition.ConditionType)
            {
                case ConditionTypes.HaveBuilding:
                case ConditionTypes.BuildingHaveById:
                case ConditionTypes.BuildingHaveByType:
                case ConditionTypes.BuildingConstructByType:
                case ConditionTypes.BuildingConstructById:
                case ConditionTypes.BuildingConstructBySubCategory:
                case ConditionTypes.BuildingHaveBySubCategory:
                    return condition.Level > 1 ? (ITaskLink) jUpgradeBuildingTask : jOpenBuildingsShopLink;
                case ConditionTypes.ConstructBuilding:
                    return jUpgradeBuildingTask;
                case ConditionTypes.ProductProduced:
                    return jOpenProductProduceLink;
                case ConditionTypes.OrderDeskOrdersWithSectors:
                case ConditionTypes.ShuffleOrderTable:
                case ConditionTypes.ReceiveItemFromOrderTable:
                    return jOpenOrderDeskWithSectorkLink;
                case ConditionTypes.OrderDeskOrders:
                    return jOpenOrderDeskLink;
                case ConditionTypes.AuctionPurchases:                    
                case ConditionTypes.AuctionSales:
                    return jOpenAuctionLink;
                case ConditionTypes.CityRenamed:
                    return jCityRenameLink;                                        
                case ConditionTypes.CollectSoftCoinsFromCommercialBuildings:
                    return jCollectCoinsFromCommercialBuildingLink;
                case ConditionTypes.GetCoinsFromAuction:                    
                case ConditionTypes.SpendCoinsAuction:
                    return jAuctionCoinsLink;
                case ConditionTypes.CollectTaxes:                
                    return jCollectTaxesLink;
                case ConditionTypes.UnlockSectorsForConstructions:
                    return jGoToLockedSectorLink;
                case ConditionTypes.SatisfactionLevel:
                case ConditionTypes.CoverAmountBuildingsByService:
                    return jCoverBuildingsWithAreaOfEffectTask;
                case ConditionTypes.BazaarPurchaseProduct:
                case ConditionTypes.BazaarPurchaseLootBox:
                case ConditionTypes.BazaarSpendAmountGoldBars:
                    return jOpenBazaarLink;
                case ConditionTypes.FoodSupplyFillBar:
                case ConditionTypes.FoodSupplyCookAmountOfMeals:
                case ConditionTypes.FoodSupplyFillBarToFull:
                    return jOpenFoodSupplyLink;
                case ConditionTypes.ReachPopulation:
                    return jPopulationAchievedLink;
            }

            $"Link for condition type {condition.ConditionType} not found!".LogError(LoggingChannel.Quests);
            return null;
        }
    }
}