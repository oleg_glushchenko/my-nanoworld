using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Engine.UI.Extensions.QuestsBuildingPanel.ConditionLinks;
using GameLogic.Quests.Models.impl.Extensions;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.Quests;

namespace Engine.UI.Extensions.QuestsBuildingPanel.ConditionLinks.TaskLinks
{
    public class CoverBuildingsWithAreaOfEffectTask : OpenBuildingsShopLink, ICoverBuildingsWithAreaOfEffectTask
    {
        public override bool Setup(ICondition condition)
        {
            if (condition is CoverAmountBuildingsByServiceCondition coverServiceCondition)
            {
                return  SetupBuilding(coverServiceCondition.ServiceObjectTypeId());
            }
            
            if (condition is MPopulationSatisfactionCondition)
            {
                return  SetupBuilding(FindSatisfactionBuilding());
            }

            return false;
        }

        protected override bool SetupBuilding(Id_IMapObjectType buildingType)
        {
            _building = jMapObjectsData.GetByBuildingId(buildingType) as IMapBuilding;
            if (_building != null)
            {
                var buildingList = jGameManager.FindAllMapObjectsOnMapByType(_building.SectorIds.FirstOrDefault(), buildingType);
                if (buildingList == null) return base.SetupBuilding(buildingType);
                var build = buildingList.Last();
                TargetMapObjectView = jGameManager.GetMapObjectView(build);
                jCamera.FocusOnMapObject(TargetMapObjectView.MapObjectModel);
            }

            return TargetMapObjectView != null;
        }

        private int FindSatisfactionBuilding()
        {
            List<IMapObject> mapObjects = jGameManager.FindAllMapObjectsOnMapByType(BusinessSectorsTypes.Town, MapObjectintTypes.School);
            
            if (mapObjects.Count > 0)
            {
                return mapObjects[0].ObjectTypeID;
            }
            
            mapObjects = jGameManager.FindAllMapObjectsOnMapByType(BusinessSectorsTypes.Town, MapObjectintTypes.Hospital);
            
            if (mapObjects.Count > 0)
            {
                return mapObjects[0].ObjectTypeID;
            }
            
            mapObjects = jGameManager.FindAllMapObjectsOnMapByType(BusinessSectorsTypes.Town, MapObjectintTypes.Police);
            
            if (mapObjects.Count > 0)
            {
                return mapObjects[0].ObjectTypeID;
            }

            return 0;
        }
    }
}