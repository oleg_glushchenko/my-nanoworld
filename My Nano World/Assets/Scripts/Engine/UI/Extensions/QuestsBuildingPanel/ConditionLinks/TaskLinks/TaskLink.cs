﻿using NanoReality.GameLogic.Quests;

namespace Assets.Scripts.Engine.UI.Extensions.QuestsBuildingPanel.ConditionLinks
{
    public abstract class TaskLink : ITaskLink
    {
        public virtual bool Setup(ICondition condition)
        {
            return true;
        }

        public abstract void Execute();
    }
}
