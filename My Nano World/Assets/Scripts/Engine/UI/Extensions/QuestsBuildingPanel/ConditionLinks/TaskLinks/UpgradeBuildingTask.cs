using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Engine.UI.Extensions.QuestsBuildingPanel.ConditionLinks;
using Assets.Scripts.Engine.UI.Views.Warehouse;
using GameLogic.Quests.Models.impl.Extensions;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.Quests;
using Sirenix.Utilities;

namespace Engine.UI.Extensions.QuestsBuildingPanel.ConditionLinks.TaskLinks
{
    public class UpgradeBuildingTask : OpenBuildingsShopLink, IUpgradeBuildingTask
    {
        public override bool Setup(ICondition condition)
        {
            switch (condition)
            {
                case ConstructBuildingCondition upgradeCondition:
                    return SetupUpgradeBuilding(upgradeCondition.BuildingId, condition.Level, upgradeCondition.Amount, upgradeCondition.CurrentCount);
                
                case BuildingConstructByIdCondition constructByIdCondition:
                    return SetupUpgradeBuilding(constructByIdCondition.BuildingId, constructByIdCondition.Level, constructByIdCondition.RequiredCount, constructByIdCondition.СurrentCount);
                
                case BuildingConstructByTypeCondition constructByTypeCondition:
                    return SetupUpgradeBuilding(constructByTypeCondition.BuildingType, constructByTypeCondition.Level, constructByTypeCondition.RequiredNumber, constructByTypeCondition.CurrentNumber);
                
                case HaveBuildingCondition buildCondition:
                    return SetupBuildBuilding(buildCondition.BuildingTypeId);
                
                case BuildingHaveByIdCondition haveByIdCondition:
                    return SetupUpgradeBuilding(haveByIdCondition.BuildingId, haveByIdCondition.Level, haveByIdCondition.RequiredNumber, haveByIdCondition.CurrentNumber);
                
                case BuildingHaveByTypeCondition haveByTypeCondition:
                    return SetupUpgradeBuilding(haveByTypeCondition.BuildingType, haveByTypeCondition.Level, haveByTypeCondition.RequiredNumber, haveByTypeCondition.CurrentNumber);
                
                case BuildingHaveWithSubCategoryCondition buildingHaveWithSubCategoryCondition:
                    return SetupUpgradeBuilding(MapObjectintTypes.DwellingHouse, buildingHaveWithSubCategoryCondition.Level,
                        buildingHaveWithSubCategoryCondition.RequiredCount, buildingHaveWithSubCategoryCondition.CurrentNumber, buildingHaveWithSubCategoryCondition.BuildingSubCategory);
                
                case ConstructBuildingWithSubCategoryCondition constructBuildingWithSubCategoryCondition:
                    return SetupUpgradeBuilding(MapObjectintTypes.DwellingHouse, constructBuildingWithSubCategoryCondition.Level, constructBuildingWithSubCategoryCondition.RequiredCount, constructBuildingWithSubCategoryCondition.CurrentNumber, constructBuildingWithSubCategoryCondition.BuildingSubCategory);
                
                default: 
                    return false;
            }
        }

        private bool SetupBuildBuilding(Id_IMapObjectType buildingTypeId)
        {
            _building = jMapObjectsData.GetByBuildingId(buildingTypeId) as IMapBuilding;
            if (_building == null) return false;
            
            if (_building is IWarehouseMapObject)
            {
                jShowWindowSignal.Dispatch(typeof(WarehouseView), null);
                return true;
            }
            
            var buildingsOnMap = jGameManager.FindAllMapObjectsOnMapByType( _building.SectorIds.FirstOrDefault(), buildingTypeId);
            
            if (buildingsOnMap == null)
            {
                return base.SetupBuilding(buildingTypeId);
            }
            
            TargetMapObjectView = jGameManager.GetMapObjectView(buildingsOnMap.FirstOrDefault());
            jCamera.FocusOnMapObject(TargetMapObjectView.MapObjectModel);
            return true;
        }

        private bool SetupUpgradeBuilding(MapObjectintTypes buildingTypeId, int level, int requiredNumber, int currentCount, BuildingSubCategories buildingSubCategories = BuildingSubCategories.None)
        {
            var checkSubCategory = buildingSubCategories != BuildingSubCategories.None; 
            
            _building = checkSubCategory
                ? jMapObjectsData.GetByBuildingTypeAndSubCategory(buildingTypeId, buildingSubCategories) as IMapBuilding
                : jMapObjectsData.GetByBuildingType(buildingTypeId) as IMapBuilding;
            
            if (_building == null)
                return false;
            
            if (buildingTypeId == MapObjectintTypes.Warehouse)
            {
                jShowWindowSignal.Dispatch(typeof(WarehouseView), null);
                return false;
            }
            
            var buildingList = jGameManager.FindAllMapObjectsOnMapByType(_building.SectorIds.FirstOrDefault(), buildingTypeId);
            if (buildingList == null)
            {
                switch (buildingSubCategories)
                {
                    case BuildingSubCategories.Common:
                    case BuildingSubCategories.None:
                    return base.SetupBuilding(_building.ObjectTypeID);
                    default:
                    return false;
                }
            }

            var buildingsToUpgrade = buildingList
                .Where(x => (!checkSubCategory || x.SubCategory == buildingSubCategories) && (x.Level == level && !x.IsConstructed || x.Level < level && ((IMapBuilding)x).CanBeUpgraded))
                .OrderBy(b => b.Level).ToList();

            if (buildingsToUpgrade.Any())
            {
                if (buildingsToUpgrade.Count() < requiredNumber - currentCount)
                {
                    return base.SetupBuilding(_building.ObjectTypeID);
                }

                TargetMapObjectView = jGameManager.GetMapObjectView(buildingsToUpgrade.Last());
                jCamera.FocusOnMapObject(TargetMapObjectView.MapObjectModel);
                return true;
            }

            if (buildingList.Count < requiredNumber)
            {
                return base.SetupBuilding(_building.ObjectTypeID);
            }

            return true;
        }

        private bool SetupUpgradeBuilding(Id_IMapObjectType buildingTypeId, int level, int amount, int currentCount)
        {
            _building = jMapObjectsData.GetByBuildingId(buildingTypeId) as IMapBuilding;
            if (_building != null)
            {
                if (_building is IWarehouseMapObject)
                {
                    jShowWindowSignal.Dispatch(typeof(WarehouseView), null);
                    return false;
                }

                var buildingList = jGameManager.FindAllMapObjectsOnMapByType(_building.SectorIds.FirstOrDefault(), buildingTypeId);
                if (buildingList == null)
                {
                    return base.SetupBuilding(buildingTypeId);
                }

                var mapObjects = buildingList.OrderBy(b => b.Level).ToList();

                var buildingsToUpgrade = mapObjects.FindAll(x => x.Level == level && !x.IsConstructed || x.Level < level && ((IMapBuilding)x).CanBeUpgraded);

                if (buildingsToUpgrade.Any())
                {
                    if (buildingsToUpgrade.Count() < amount - currentCount)
                    {
                        return base.SetupBuilding(buildingTypeId);
                    }

                    TargetMapObjectView = jGameManager.GetMapObjectView(buildingsToUpgrade.Last());
                    jCamera.FocusOnMapObject(TargetMapObjectView.MapObjectModel);
                    return true;
                }

                if (buildingList.Count < amount)
                {
                    return base.SetupBuilding(buildingTypeId);
                }

                return true;
            }

            return false;
        }
    }
}