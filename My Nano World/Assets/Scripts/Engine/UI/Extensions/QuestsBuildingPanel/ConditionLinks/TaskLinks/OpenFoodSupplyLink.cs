﻿using System.Linq;
using Assets.Scripts.Engine.UI.Extensions.QuestsBuildingPanel.ConditionLinks;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.Quests;

namespace Engine.UI.Extensions.QuestsBuildingPanel.ConditionLinks.TaskLinks
{
    public class OpenFoodSupplyLink : OpenBuildingsShopLink, IOpenFoodSupplyLink    
    {
        [Inject] public IPlayerExperience jPlayerExperience { get; set; }
        public override bool Setup(ICondition condition)
        {
            var foodSupplies = jMapObjectsData.GetAllMapObjectsByBuildingType(MapObjectintTypes.FoodSupply)
                .Where(f => ((IMapBuilding)f).BuildingUnlockLevel <= jPlayerExperience.CurrentLevel);
            
            foreach (var foodSupply in foodSupplies)
            {
                if (SetupBuilding(foodSupply.ObjectTypeID))
                    return true;
            }

            foreach (var foodSupply in foodSupplies)
            {
                if (base.SetupBuilding(foodSupply.ObjectTypeID))
                    return true;
            }

            return false;
        }

        protected override bool SetupBuilding(Id_IMapObjectType buildingType)
        {
            _building = jMapObjectsData.GetByBuildingId(buildingType) as IMapBuilding;
            if (_building != null)
            {
                var buildingList = jGameManager.FindAllMapObjectsOnMapByType(_building.SectorIds.FirstOrDefault(), buildingType);
                if (buildingList == null) 
                    return false;
                
                var build = buildingList.Last();
                TargetMapObjectView = jGameManager.GetMapObjectView(build);
                jCamera.FocusOnMapObject(TargetMapObjectView.MapObjectModel);
            }

            return TargetMapObjectView != null;
        }
    }
}
