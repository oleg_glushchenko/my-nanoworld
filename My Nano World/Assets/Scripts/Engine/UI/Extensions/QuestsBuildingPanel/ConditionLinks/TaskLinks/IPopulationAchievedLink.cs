﻿using Assets.Scripts.Engine.UI.Extensions.QuestsBuildingPanel.ConditionLinks;

namespace Engine.UI.Extensions.QuestsBuildingPanel.ConditionLinks.TaskLinks
{
    public interface IPopulationAchievedLink : ITaskLink
    {
    }
}
