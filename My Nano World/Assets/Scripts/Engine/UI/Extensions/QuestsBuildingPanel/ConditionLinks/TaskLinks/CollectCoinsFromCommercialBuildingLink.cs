﻿using NanoReality.GameLogic.Quests;

namespace Assets.Scripts.Engine.UI.Extensions.QuestsBuildingPanel.ConditionLinks.TaskLinks
{
    public class CollectCoinsFromCommercialBuildingLink : OpenBuildingsShopLink, ICollectCoinsFromCommercialBuildingLink
    {
        public override bool Setup(ICondition condition)
        {
            var collectCondition = condition as ICollectCoinsFormCommercialBuildingsCondition;
            return collectCondition != null && SetupBuilding(collectCondition.BuildingTypeId);
        }        
    }
}
