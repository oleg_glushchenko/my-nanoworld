﻿using System.Collections.Generic;
using Assets.NanoLib.UI.Core;
using Assets.Scripts.GameLogic.DailyBonus.Impl;
using Engine.UI;
using NanoReality.Core.Resources;
using NanoReality.GameLogic.Common.Controllers.Signals;
using NanoReality.UI.Components;

namespace NanoReality.Engine.UI.Extensions.DailyBonusPanel
{
    public class DailyBonusViewMediator : UIMediator<DailyBonusView>
    {
        private DailyBonusController _controller;

        #region Inject

        [Inject] public DailyBonusContext Context { get; set; }
        [Inject] public AddRewardItemSignal jAddRewardItemSignal { get; set; }
        [Inject] public SignalOnNanoGemsDropped jSignalOnNanoGemsDropped { get; set; }

        #endregion

        #region PublicAPIForController

        public List<DailyBonusData> GetDailyBonusData => Context.GetDailyBonusData;

        public int GetCurrentDay => Context.GetCurrentBonusDay;

        public DailyBonusItem GetCurrentItem => View.GetItem(GetCurrentDay);

        public void Collect() =>
            Context.jDailyBonusData.CollectDailyBonus(GetCurrentDay + 1, Hide);

        public void GiveGems(int amount) => jSignalOnNanoGemsDropped.Dispatch(amount);

        public DailyBonusItem AddItem() => View.AddItem();

        public IResourcesManager GetTextureContainer => Context.jResourcesManager;

        public void StartAnimation(AddItemSettings itemSettings)
        {
            jAddRewardItemSignal.Dispatch(itemSettings);
        }

        #endregion
        protected override void OnHidden()

        {
            _controller = null;
            base.OnHidden();
        }

        protected override void Show(object param)
        {
            if (!Context.ShouldPanelBeShown)
                return;

            base.Show(param);
            _controller = new DailyBonusController(this);
            View.onCollect = _controller.OnCollectClick;
            View.SetButtonInteractable(Context.NextDayAvailable);
            _controller.InitializeItems(Context.CurrentCollectedDay);
            View.SetScrollPositionOnADay(Context.CurrentCollectedDay);
        }
    }
}