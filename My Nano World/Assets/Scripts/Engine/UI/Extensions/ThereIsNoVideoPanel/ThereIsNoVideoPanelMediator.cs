﻿using Assets.NanoLib.UI.Core.Views;
using strange.extensions.mediation.impl;
using strange.extensions.signal.impl;

namespace Engine.UI.Extensions.ThereIsNoVideoPanel
{
    public class SignalShowThereIsNoVideoPanel : Signal { }
    
    public class ThereIsNoVideoPanelMediator : Mediator
    {
        [Inject]
        public ThereIsNoVideoPanelView View { get; set; }
        
        [Inject]
        public SignalShowThereIsNoVideoPanel jSignalShowThereIsNoVideoPanel { get; set; }

        public override void OnRegister()
        {   
            View.CloseEvent += () => View.Hide();
            View.ShowEvent += () => jSignalShowThereIsNoVideoPanel.Dispatch();
        }
    }
}
