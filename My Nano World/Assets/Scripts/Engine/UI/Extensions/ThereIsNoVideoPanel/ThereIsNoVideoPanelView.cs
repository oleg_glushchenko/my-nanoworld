﻿using System;
using Assets.NanoLib.UI.Core.Views;
using Assets.Scripts.GameLogic.Utilites;
using Assets.Scripts.MultiLanguageSystem.Logic;
using NanoReality.StrangeIoC;
using strange.extensions.signal.impl;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Engine.UI.Extensions.ThereIsNoVideoPanel
{
	public class ThereIsNoVideoPanelView : UIPanelView
	{
		[SerializeField] 
		private Button _closeButton;
		
		[SerializeField]
		private TextMeshProUGUI _textField;

		public event Action CloseEvent;
		public event Action ShowEvent;

		protected override void Awake()
		{
			base.Awake();
			
			_closeButton.onClick.AddListener(() =>
			{
				CloseEvent?.Invoke();
			});
		}

		public override void Show()
		{
			base.Show();

			if (ShowEvent != null) ShowEvent();
			_textField.SetLocalizedText(LocalizationMLS.Instance.GetText(LocalizationKeyConstants.NO_ADS_AVAILABLE_FOR_REGION));
		}

		protected override void OnDestroy()
		{
			base.OnDestroy();

			CloseEvent = null;
			ShowEvent = null;
			_closeButton.onClick.RemoveAllListeners();
		}
	}
}
