﻿using System;
using System.Collections.Generic;
using Assets.NanoLib.UI.Core.Signals;
using Assets.NanoLib.UI.Core.Views;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Engine.UI.Extensions.EditModePanels
{
    public class LayoutEditorPanelView : UIPanelView
    {
        [Inject] public ShowWindowSignal jShowWindowSignal { get; set; }
        
        #region Inspector
        [SerializeField] private Button _closeButton;
        [SerializeField] private Button _editButton;
        [SerializeField] private Button _setActiveButton;
        [SerializeField] private List<LayoutPreview> _layoutButtons;

        public int SelectedLayoutIndex { get; set; } = -1;

        public List<LayoutPreview> LayoutButtons => _layoutButtons;
        public Button ApplyButton => _setActiveButton;
        #endregion

        public override void Show()
        {
            base.Show();
            _closeButton.onClick.RemoveAllListeners();
            _closeButton.onClick.AddListener(Hide);
            _editButton.onClick.AddListener(OnEditClick);
            _editButton.interactable = false;
            _setActiveButton.interactable = false;
        }

        public override void Hide()
        {
            base.Hide();
            _editButton.onClick.RemoveListener(OnEditClick);
            ClearLayoutButtons();
        }

        public void ClearLayoutButtons()
        {
            foreach (var layoutPreview in LayoutButtons)
            {
                layoutPreview.Clear();
            }
            _editButton.interactable = false;
        }
        
        public void SelectLayout(int index)
        {
            foreach (var layoutPreview in _layoutButtons)
            {
                layoutPreview.SetLayoutActive(index);
            }
            
            SelectedLayoutIndex = index;
            _editButton.interactable = true;
        }

        private void OnEditClick()
        {
            jShowWindowSignal.Dispatch(typeof(EditorPanelView), SelectedLayoutIndex); 
            Hide();
        }
    }
}