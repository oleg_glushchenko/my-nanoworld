﻿using System.Collections.Generic;
using System.Linq;
using Assets.NanoLib.UI.Core;
using NanoLib.Core.Logging;
using NanoReality.GameLogic.BuildingSystem.CityMap.Controllers;
using NanoReality.GameLogic.Configs;

namespace NanoReality.Engine.UI.Extensions.EditModePanels
{
    public class LayoutEditorPanelMediator : UIMediator<LayoutEditorPanelView>
    {
        [Inject] public IEditModeService jEditModeService { get; set; }
        [Inject] public SignalOnEditModeStateChange jSignalOnEditModeStateChange { get; set; }

        private List<EditModeLayout> _editModeLayouts = new List<EditModeLayout>();
        
        protected override void Show(object param)
        {
            base.Show(param);
            
            void Callback(List<EditModeLayout> data)
            {
                _editModeLayouts = data;
                InitLayoutButtons();
            }
            
            jEditModeService.GetLayouts(Callback);
        }

        protected override void OnShown()
        {
            base.OnShown();
            View.ApplyButton.onClick.AddListener(OnSetActiveClick);
        }

        protected override void OnHidden()
        {
            View.ApplyButton.onClick.RemoveListener(OnSetActiveClick);
            base.OnHidden();
        }

        private void InitLayoutButtons()
        {
            List<LayoutPreview> layoutButtons = View.LayoutButtons;

            for (int i = 0; i < _editModeLayouts.Count; i++)
            {
                if (i < layoutButtons.Count)
                {
                    var layout = _editModeLayouts[i];
                    var layoutButton = layoutButtons[i];
                    
                    layoutButton.UpdateButton(layout.LayoutId,layout.LayoutImage);
                    layoutButton.SelectLayoutClicked += SelectLayout;
                }
            }
        }

        private void SelectLayout(int index)
        {
            View.SelectLayout(index);
            View.ApplyButton.interactable = _editModeLayouts.First(l => l.LayoutId == index).IsLayoutActive;
        }

        private void OnSetActiveClick()
        {
            jSignalOnEditModeStateChange.Dispatch(true);
            jEditModeService.ApplyLayout(b =>
            {
                Logging.LogWarning($"Apply layout {b}");
                jSignalOnEditModeStateChange.Dispatch(false);
                Hide();
            }, View.SelectedLayoutIndex);
        }
    }
}
