﻿using System.Linq;
using Assets.NanoLib.UI.Core;
using Assets.NanoLib.UI.Core.Signals;
using Assets.NanoLib.UI.Core.Views;
using Assets.NanoLib.Utilities;
using Assets.Scripts.Engine.BuildingSystem.CityMap;
using Assets.Scripts.Engine.BuildingSystem.CityMap.Highlightings;
using Assets.Scripts.Engine.UI.Views.Hud;
using Assets.Scripts.Engine.UI.Views.Popups;
using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using NanoLib.Core.Logging;
using NanoLib.Core.Services.Sound;
using NanoLib.Services.InputService;
using NanoReality.Engine.UI.Views.ConstractConfirmPanel;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityCamera.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityMap.Controllers;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Impl;
using NanoReality.GameLogic.GameManager.Controllers;
using NanoReality.GameLogic.GameManager.Models.api;
using NanoReality.GameLogic.Services;
using NanoReality.GameLogic.Utilites;
using NanoReality.UI.Components;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace NanoReality.Engine.UI.Extensions.EditModePanels
{
	public class EditorPanelMediator : UIMediator<EditorPanelView>
	{
		#region Injects
		
		[Inject] public IInputController jInputController { get; set; }
		[Inject] public IConstructionController jConstructionController { get; set; }
		[Inject] public IIconProvider jIconProvider { get; set; }
		[Inject] public IGameManager jGameManager { get; set; }
		[Inject] public IGameCamera jGameCamera { get; set; }
		[Inject] public IEditModeService jEditModeService { get; set; }
		[Inject] public OnHudButtonsIsClickableSignal jOnHudButtonsIsClickableSignal { get; set; }
		[Inject] public OnHudBottomPanelIsVisibleSignal jOnHudBottomPanelIsVisibleSignal { get; set; }
		[Inject] public SignalBalancePanelViewChangeState jSignalBalancePanelViewChangeState { get; set; }
		[Inject] public SignalOnEditModeStateChange jSignalOnEditModeStateChange { get; set; }
		[Inject] public SignalOnEditModeEraseStateChange jSignalOnEditModeEraseStateChange { get; set; }
		[Inject] public SignalOnEditModeRemoveBuild jSignalOnEditModeRemoveBuild { get; set; }
		[Inject] public SignalOnCancelConstruct jSignalOnCancelConstruct { get; set; }
		[Inject] public RoadConstructSetActiveSignal jRoadConstructSetActiveSignal { get; set; }
		[Inject] public SignalReloadApplication jSignalReloadApplication { get; set; }
		[Inject] public SignalOnEditModeHappinessUpdate jSignalOnEditModeHappinessUpdate { get; set; }
		[Inject] public SignalOnEditModeStorageUpdate jSignalOnEditModeApplyStateChange { get; set; }
		[Inject] public HighlightController jHighlightController { get; set; }
		[Inject] public SignalOnShownPanel jUiPanelOpenedSignal { get; set; }
		[Inject] public SignalOnHidePanel jSignalOnHidePanel { get; set; }
		[Inject] public SignalOnEditModeAOEStateChange jSignalOnEditModeAOEStateChange { get; set; }
		[Inject] public SignalOnFinishRestoreCity jSignalOnFinishRestoreCity { get; set; }
		[Inject] public SignalOnSwipeEnd jSignalOnSwipeEnd { get; set; }

		#endregion

		private Vector3F CurrentFingerPosition => jInputController.CurrentFingerPosition.ToVector3F();

		private BusinessSectorsTypes CurrentChosenBusinessSector { get; set; } = BusinessSectorsTypes.Town;

		#region Camera

		private const string Layer = "Ground";
		private readonly Vector3 _viewportPosition = new Vector3(0.5f, 0.5f, 0f);
		private const float ZoomSpeed = 4f;
		private const float ZoomPercentage = 8f;
		private bool _isRoadContainerEnabled;
		private bool _roadCreationEnabled;
		private bool _roadRemoveEnabled;

		#endregion

		protected override void Show(object param)
		{
			base.Show(param);
			View.Lock.SetActive(true);
			
			jEditModeService.GetLayoutData((int) param, () =>
			{
				View.Lock.SetActive(false);
				SelectAllSectors();
				jSignalOnSwipeEnd.AddListener(ChangeFilterAfterCameraMove);
			});
			jOnHudBottomPanelIsVisibleSignal.Dispatch(false);
			jOnHudButtonsIsClickableSignal.Dispatch(false);
			jSignalBalancePanelViewChangeState.Dispatch(false);
			jSignalOnEditModeStateChange.Dispatch(true);
		}

		protected override void OnShown()
		{
			jSignalOnEditModeApplyStateChange.AddListener(OnStorageChange);
			jSignalReloadApplication.AddOnce(OnReloadGame);
			jSignalOnEditModeRemoveBuild.AddListener(SelectAllSectors);
			jSignalOnEditModeHappinessUpdate.AddListener(UpdateHappiness);
			jUiPanelOpenedSignal.AddListener(OnOpenAnotherPanel);
			jSignalOnHidePanel.AddListener(OnCloseAnotherPanel);

			View.EraseButton.onClick.AddListener(ChangeEraseModeState);
			View.RemoveAllButton.onClick.AddListener(OnClearMapClick);
			View.SaveButton.onClick.AddListener(OnApplyClick);
			View.CancelButton.onClick.AddListener(OnCancelClick);
			View.FilterByTownButton.onClick.AddListener(FilterByTown);
			View.FilterByFarmButton.onClick.AddListener(FilterByFarm);
			View.FilterByHeavyIndustryButton.onClick.AddListener(FilterByHeavyIndustry);
			View.BuildRoadButton.onClick.AddListener(OnConstructRoadClicked);
			View.RemoveRoadButton.onClick.AddListener(OnDestroyRoadClicked);
			View.ReturnButton.onClick.AddListener(DisableChangesState);
			View.AOEButton.onClick.AddListener(ChangeAoeState);
			jSignalOnFinishRestoreCity.AddListener(Hide);
		}

		protected override void OnHidden()
		{
			base.OnHidden();

			jSignalOnEditModeStateChange.Dispatch(false);
			jOnHudBottomPanelIsVisibleSignal.Dispatch(true);
			jOnHudButtonsIsClickableSignal.Dispatch(true);
			jSignalBalancePanelViewChangeState.Dispatch(true);

			jSignalReloadApplication.RemoveListener(OnReloadGame);
			jSignalOnEditModeRemoveBuild.RemoveListener(SelectAllSectors);
			jSignalOnEditModeHappinessUpdate.RemoveListener(UpdateHappiness);
			jSignalOnEditModeApplyStateChange.RemoveListener(OnStorageChange);
			jUiPanelOpenedSignal.RemoveListener(OnOpenAnotherPanel);
			jSignalOnHidePanel.RemoveListener(OnCloseAnotherPanel);
			jSignalOnFinishRestoreCity.RemoveListener(Hide);
			jSignalOnSwipeEnd.RemoveListener(ChangeFilterAfterCameraMove);
			
			View.AOEButton.onClick.RemoveListener(ChangeAoeState);
			View.EraseButton.onClick.RemoveListener(ChangeEraseModeState);
			View.SaveButton.onClick.RemoveListener(OnApplyClick);
			View.CancelButton.onClick.RemoveListener(OnCancelClick);
			View.RemoveAllButton.onClick.RemoveListener(OnClearMapClick);
			View.FilterByTownButton.onClick.RemoveListener(FilterByTown);
			View.FilterByFarmButton.onClick.RemoveListener(FilterByFarm);
			View.FilterByHeavyIndustryButton.onClick.RemoveListener(FilterByHeavyIndustry);
			View.BuildRoadButton.onClick.RemoveListener(OnConstructRoadClicked);
			View.RemoveRoadButton.onClick.RemoveListener(OnDestroyRoadClicked);
			View.ReturnButton.onClick.RemoveListener(DisableChangesState);
		}

		private void FilterByTown() => ChooseSector(BusinessSectorsTypes.Town);
		private void FilterByFarm() => ChooseSector(BusinessSectorsTypes.Farm);
		private void FilterByHeavyIndustry() => ChooseSector(BusinessSectorsTypes.HeavyIndustry);
		private void UpdateHappiness(int value) => View.EditModeHappinessPanel.UpdatePanel(value);
		
		private void OnStorageChange(bool value)
		{
			View.SaveButton.interactable = value;
			
			BusinessSectorsTypes currentFocusedSector = GetCurrentSectorByCameraPosition();
			bool filterAvailable = jEditModeService.LayoutData.StorageBuildingData.Any(data => data.BusinessSectorsType == currentFocusedSector);

			CurrentChosenBusinessSector = filterAvailable ? currentFocusedSector : BusinessSectorsTypes.Unsupported;
			UpdateBuildingsList();
			View.UpdateScrolls();
			
			jSignalOnCancelConstruct.RemoveListener(OnCancelRestore);
		}

		private void ChangeAoeState()
		{
			bool newState = !jEditModeService.IsAOEEnable;
			jSignalOnEditModeAOEStateChange.Dispatch(newState);
			
			if (newState)
			{
				View.ChangePhotoModeState(true, true);
			}
			
			jHighlightController.ForceChangeAoeState(newState);
		}

		private void SelectAllSectors()
		{
			CurrentChosenBusinessSector = BusinessSectorsTypes.Unsupported;
			UpdateBuildingsList();
			View.UpdateScrolls();
		}

		private void ChangeEraseModeState()
		{
			jSignalOnEditModeEraseStateChange.Dispatch(!jEditModeService.IsEraseEnable);
			View.EraseButton.Text = LocalizationUtils.LocalizeL2(jEditModeService.IsEraseEnable ? "UICommon/EDITMODE_ERASE_ON" : "UICommon/EDITMODE_ERASE_OFF");
		}

		private void ChangeFilterAfterCameraMove()
		{
			BusinessSectorsTypes currentFocusedSector = GetCurrentSectorByCameraPosition();

			if (CurrentChosenBusinessSector == currentFocusedSector) return;
			
			bool filterAvailable = jEditModeService.LayoutData.StorageBuildingData.Any(data => data.BusinessSectorsType == currentFocusedSector);

			CurrentChosenBusinessSector = filterAvailable ? currentFocusedSector : BusinessSectorsTypes.Unsupported;
			UpdateBuildingsList();
			View.UpdateScrolls();
		}

		private void ChooseSector(BusinessSectorsTypes businessSector)
		{
			CurrentChosenBusinessSector = CurrentChosenBusinessSector == businessSector? BusinessSectorsTypes.Unsupported : businessSector;
			UpdateBuildingsList();
			View.UpdateScrolls();
			FocusOnSector(businessSector);
		}

		private void UpdateBuildingsList()
		{
			View.ClearCurrentItems();

			var buildings = jEditModeService.LayoutData.StorageBuildingData;

			foreach (var storageBuildingData in buildings)
			{
				switch (storageBuildingData.BusinessSectorsType)
				{
					case BusinessSectorsTypes.Farm:
					{
						View.FilterByFarmButton.interactable = true;
						break;
					}
					case BusinessSectorsTypes.HeavyIndustry:
					{
						View.FilterByHeavyIndustryButton.interactable = true;
						break;
					}
					case BusinessSectorsTypes.Town:
					{
						View.FilterByTownButton.interactable = true;
						break;
					}
				}

				if (CurrentChosenBusinessSector == BusinessSectorsTypes.Unsupported || storageBuildingData.BusinessSectorsType == CurrentChosenBusinessSector)
				{
					storageBuildingData.Buildings.ForEach(CreateStoreItem);
				}
			}
		}

		private void CreateStoreItem(int buildingId)
		{
			var mapObject = jEditModeService.CachedMapObjectById[buildingId];

			if (mapObject is MRoad)
			{
				Logging.LogError($"simple Road was found on storage, why?! id {buildingId} bss {mapObject.SectorId}");
				return;
			}

			StorageItem item = View.AddItem();
			item.UpdateItem(jIconProvider, View._scrollSnap, mapObject);

			item.SignalOnBuyBuilding.RemoveAllListeners();
			item.SignalOnSelectChanged.RemoveAllListeners();
			item.SignalOnBuyBuilding.AddListener(RestoreBuildingFromStorage);
		}

		private void RestoreBuildingFromStorage(StorageItem item)
		{
			bool useCurrentFingerPos = item.IsDragNow || item.IsLongTap;
			jOnHudButtonsIsClickableSignal.Dispatch(false);
			Vector3F position = useCurrentFingerPos ? CurrentFingerPosition : default;
			RestoreMapObject(item.CurrentMapObject, position);
			jSoundManager.Play(SfxSoundTypes.ChangeBuildingmenu, SoundChannel.SoundFX);

			if (jEditModeService.IsEraseEnable)
			{
				ChangeEraseModeState();
			}
		}

		public void RestoreMapObject(IMapObject obj, Vector3F position = default)
		{
			jSignalOnCancelConstruct.AddListener(OnCancelRestore);
			jConstructionController.RestoreMapObject(obj, position);
		}

		private void OnCancelRestore()
		{
			if (CurrentChosenBusinessSector == BusinessSectorsTypes.Unsupported)
			{
				SelectAllSectors();
			}
			else
			{
				ChooseSector(CurrentChosenBusinessSector);
			}
			
			jSignalOnCancelConstruct.RemoveListener(OnCancelRestore);
		}

		private void FocusOnSector(BusinessSectorsTypes targetBusinessSector)
		{
			if (BusinessSectorsTypes.Unsupported == targetBusinessSector) return;
			
			CityMapView chosenMapView = jGameManager.GetMapView(targetBusinessSector);
			BusinessSectorsTypes currentFocusedSector = GetCurrentSectorByCameraPosition();
			if (currentFocusedSector == chosenMapView.BusinessSector)
				return;

			Vector2F newCameraPos = new Vector2F();

			switch (targetBusinessSector)
			{
				case BusinessSectorsTypes.Town:
					newCameraPos = new Vector2F(6, 15);
					break;
				case BusinessSectorsTypes.HeavyIndustry:
					newCameraPos = new Vector2F(-17, -19);
					break;
				case BusinessSectorsTypes.Farm:
					newCameraPos = new Vector2F(-39, 0);
					break;
			}

			jGameCamera.ZoomTo(newCameraPos, CameraSwipeMode.Fly, ZoomPercentage, ZoomSpeed, true);
		}

		private BusinessSectorsTypes GetCurrentSectorByCameraPosition()
		{
			int layer = 1 << LayerMask.NameToLayer(Layer);
			RaycastHit hit = InputTools.RaycastScene(Env.MainCamera.ViewportToScreenPoint(_viewportPosition), layer);
			ICityMap currentMap = jGameManager.GetMap(hit.point);
			return currentMap?.BusinessSector ?? BusinessSectorsTypes.Unsupported;
		}

		private void OnClearMapClick()
		{
			var settings = new ConfirmPopupSettings
			{
				Title = "",
				Message = LocalizationUtils.LocalizeL2("UICommon/EDITMODE_SURE_MSG")
			};

			jPopupManager.Show(settings, result =>
			{
				if (result == PopupResult.Ok)
				{
					jEditModeService.ClearMap(SelectAllSectors);
				}
			});
		}

		private void OnApplyClick()
		{
			View.Lock.SetActive(true);
			jEditModeService.ApplyLayout(b =>
			{
				Logging.LogWarning($"Apply layout {b}");
			});
		}

		private void OnCancelClick()
		{
			View.Lock.SetActive(true);
			jEditModeService.CancelEditMode();
		}

		#region Road

		private void DisableChangesState()
		{
			if (jEditModeService.IsAOEEnable)
			{
				ChangeAoeState();
			}

			if (_isRoadContainerEnabled)
			{
				_isRoadContainerEnabled = false;
				_roadRemoveEnabled = false;
				_roadCreationEnabled = false;
				jConstructionController.CancelChangeObject();
				jRoadConstructSetActiveSignal.Dispatch(false);
				jRoadConstructSetActiveSignal.AddOnce(isActive =>
				{
					if (isActive) return;

					_roadRemoveEnabled = false;
					_roadCreationEnabled = false;
				});
			}
		}

		private void OnConstructRoadClicked()
		{
			if (_roadRemoveEnabled)
			{
				_roadRemoveEnabled = false;
			}

			_roadCreationEnabled = !_roadCreationEnabled;
			_isRoadContainerEnabled = true;

			jRoadConstructSetActiveSignal.Dispatch(_roadCreationEnabled);
			jRoadConstructSetActiveSignal.AddOnce(isActive =>
			{
				if (isActive) return;

				_roadRemoveEnabled = false;
				_roadCreationEnabled = false;
			});

			if (_roadCreationEnabled)
			{
				jConstructionController.CreateMapObject(31, BusinessSectorsTypes.Town);
			}
			else
			{
				jConstructionController.CancelChangeObject();
			}
			View.ChangePhotoModeState(true, true);
		}

		private void OnDestroyRoadClicked()
		{
			if (_roadCreationEnabled)
			{
				_roadCreationEnabled = false;
				jConstructionController.CancelChangeObject();
			}

			_roadRemoveEnabled = !_roadRemoveEnabled;
			_isRoadContainerEnabled = true;
			jRoadConstructSetActiveSignal.Dispatch(_roadRemoveEnabled);

			jRoadConstructSetActiveSignal.AddOnce((isActive) =>
			{
				if (!isActive)
				{
					_roadRemoveEnabled = false;
					_roadCreationEnabled = false;
				}
			});
			View.ChangePhotoModeState(true, true);
		}
		
		private void OnOpenAnotherPanel(UIPanelView view)
		{
			if (view == View || jEditModeService.IsAOEEnable)
			{
				View.ChangePhotoModeBackButtonState(false);
				return;
			}

			View.ChangePhotoModeState(true, _isRoadContainerEnabled);
		}

		private void OnCloseAnotherPanel(UIPanelView view)
		{
			if (view == View || jEditModeService.IsAOEEnable)
			{
				View.ChangePhotoModeBackButtonState(true);
				return;
			}
			View.ChangePhotoModeState(false);
		} 

		private void OnReloadGame()
		{
			jSignalOnEditModeStateChange.Dispatch(false);
			Hide();
		}

		#endregion
	}
}