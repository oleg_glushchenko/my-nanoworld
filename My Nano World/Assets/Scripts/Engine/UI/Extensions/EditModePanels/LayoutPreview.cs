﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Extensions;
using Engine.UI.Views.Hud.ButtonNotifications;
using NanoReality.Core.UI;
using NanoReality.GameLogic.Configs;
using Newtonsoft.Json;
using strange.extensions.mediation.impl;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Engine.UI.Extensions.EditModePanels
{
	public class LayoutPreview : View
	{
		[SerializeField] private TextButton _layoutButton;
		[SerializeField] private Image _mapImage;

		private int _index = -1;

		public event Action<int> SelectLayoutClicked;

		public void UpdateButton(int index, string textImage)
		{
			_index = index;
			_layoutButton.Text = index.ToString();
			_layoutButton.onClick.AddListener(OnLayoutButtonClicked);

			if (textImage.IsNullOrEmpty()) return;
			return;
			
			Texture2D texture2D = new Texture2D(1, 1);

			byte[] b64_bytes = JsonConvert.DeserializeObject<byte[]>(textImage);
			//byte[] b64_bytes = Convert.FromBase64String(textImage);
			texture2D.LoadImage(b64_bytes);
			_mapImage.sprite = Sprite.Create(texture2D, new Rect(0.0f, 0.0f, texture2D.width, texture2D.height), new Vector2(0.5f, 0.5f), 100.0f);
			_mapImage.material.SetTexture("_MainTex", texture2D);
		}

		public void SetLayoutActive(int activeIndex)
		{
			_layoutButton.Text = activeIndex == _index ? "Active" : _index.ToString();
		}

		public void Clear()
		{
			_layoutButton.onClick.RemoveListener(OnLayoutButtonClicked);
			_mapImage = null;
			SelectLayoutClicked = null;
			_index = -1;
		}
		
		private void OnLayoutButtonClicked()
		{
			SelectLayoutClicked?.Invoke(_index);
		}
	}
}