using System;
using Assets.NanoLib.UtilitesEngine;
using NanoLib.Services.InputService;
using NanoReality.Engine.Utilities;
using TMPro;
using UnityEngine;

namespace Engine.UI.Extensions.TrophyInfoPanel
{
    public class TrophyInfoPanel : BuildingPanelView
    {
        [Inject] public SignalOnGroundTap jSignalOnGroundTap { get; set; }
        [Inject] public ITimerManager jTimerManager { get; set; }

        [SerializeField] private TextMeshProUGUI _taxIncome;
        [SerializeField] private TextMeshProUGUI _time;

        private ITimer _timer;

        public override void Show()
        {
            base.Show();
            jGameCamera.FocusOnMapObject(TargetBuilding);
            IsAutoClosing = true;
        }

        public override void Hide()
        {
            jSignalOnGroundTap.RemoveListener(Hide);
            _timer?.CancelTimer();
            base.Hide();
        }

        public override void Initialize(object parameters)
        {
            base.Initialize(parameters);

            jSignalOnGroundTap.AddListener(Hide);
            IsAutoClosing = true;

            if (TargetBuilding is EventBuildingReward building)
            {
                Debug.Log(building.NextRewardTime);
                Debug.Log(building.TaxBonus());
                var dateTime = DateTimeUtils.UnixStartTime.AddSeconds(building.NextRewardTime).ToLocalTime();
                var secondsLeft = (dateTime - DateTime.Now).TotalSeconds;

                _timer = jTimerManager.StartRealTimer((float)secondsLeft, null,
                    elapsed =>
                    {
                        _time.text = UiUtilities.GetFormattedTimeFromSecondsShorted((int)(secondsLeft - elapsed));
                    });

                _time.text = UiUtilities.GetFormattedTimeFromSecondsShorted((int)(secondsLeft));
                _taxIncome.text = building.TaxBonus() + "%";
            }
        }
    }
}