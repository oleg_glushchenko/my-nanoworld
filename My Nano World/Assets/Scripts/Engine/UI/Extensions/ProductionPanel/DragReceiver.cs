﻿using strange.extensions.signal.impl;
using UnityEngine;

public class DragReceiver : MonoBehaviour
{
    public Signal<IDraggableItem, DragReceiver> jSignalOnDragObjectPutted = new Signal<IDraggableItem, DragReceiver>();
	public Signal<IDraggableItem, DragReceiver> jSignalOnDragObjectEnter = new Signal<IDraggableItem, DragReceiver>();
	public Signal<IDraggableItem, DragReceiver> jSignalOnGameObjectExit = new Signal<IDraggableItem, DragReceiver>();

    public void PutObject(IDraggableItem objectToPut)
    {
        if(jSignalOnDragObjectPutted == null)
            jSignalOnDragObjectPutted = new Signal<IDraggableItem, DragReceiver>();

        jSignalOnDragObjectPutted.Dispatch(objectToPut, this);
    }

	public void OnDragObjectEnter(IDraggableItem dragObject)
	{
		if (jSignalOnDragObjectEnter == null)
			jSignalOnDragObjectEnter = new Signal<IDraggableItem, DragReceiver>();

		jSignalOnDragObjectEnter.Dispatch(dragObject, this);
	}

	public void OnDragObjectExit(IDraggableItem dragObject)
	{
		if (jSignalOnGameObjectExit == null)
			jSignalOnGameObjectExit = new Signal<IDraggableItem, DragReceiver>();

		jSignalOnGameObjectExit.Dispatch(dragObject, this);
	}
}
