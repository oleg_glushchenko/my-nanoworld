﻿using UnityEngine.EventSystems;

/// <summary>
/// Перетягиваемый итем
/// </summary>
public interface IDraggableItem : IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerDownHandler,
    IPointerUpHandler
{

}