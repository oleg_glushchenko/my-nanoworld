﻿using System;
using Assets.Scripts.Engine.UI.Extensions.ProductionPanel;
using Assets.Scripts.GameLogic.BuildingSystem.Products.Models.api;
using NanoLib.UI.Core;
using NanoReality.Core.Resources;
using NanoReality.Engine.UI.UIAnimations;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Factories;
using NanoReality.GameLogic.MinePanel;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using strange.extensions.signal.impl;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DraggableProductItem : HoverDraggableItem
{
    public event Action<DraggableProductItem> ShowRequirementsPanel;
    public event Action<DraggableProductItem> HideRequirementsPanel;

    #region Inspector Fields

    [SerializeField] private Material _grayMat;

    [SerializeField] private GameObject _lockedProductImage;

    public GameObject BackGroundHighLight;

    public bool ShowRequirementsOnLeft;

    [SerializeField] private GameObject _counterGameObject;

    [SerializeField] private TextMeshProUGUI _productCounter;

    [SerializeField] private Image _productCounterBack;

    [SerializeField] private Sprite _lessThanRequiredBackgr;

    [SerializeField] private Sprite _equalToRequiredBackgr;

    [SerializeField] private EventTrigger _eventTrigger;
    [SerializeField] private CanvasObjectFlyEffect _canvasObjectFlyEffect;

    #endregion

    #region Injections

    [Inject] public IResourcesManager jResourcesManager { get; set; }

    [Inject] public ICityPopulation jCityPopulation { get; set; }

    [Inject] public IPlayerProfile jPlayerProfile { get; set; }

    [Inject] public IProductsData jProductsData { get; set; }
    [Inject] public IUIManager jUIManager { get; set; }

    #endregion

    #region Fields
    
    public Signal<Vector3, IProducingItemData> OnDragSignal = new Signal<Vector3, IProducingItemData>();

    private IProducingItemData _producingItemData;

    public IProducingItemData ProducingItemData => _producingItemData;

    public bool IsProductsEnoughtForProduction { get; private set; }

    public bool IsAvailable { protected get; set; }

    // TODO: for unknown reasons, the script DraggableProductItem is on the nested object of draggable item. Need fix it.
    private Transform _itemTransform;

    public Transform ItemTransform => ParentTransform.parent;

    [SerializeField] private Graphic _targetGraphic;

    #endregion

    #region Methods
    
    protected override void Start()
    {
        base.Start();

        EventTrigger.Entry entryExit = new EventTrigger.Entry();
        entryExit.eventID = EventTriggerType.PointerExit;
        entryExit.callback.AddListener(eventData => { OnBackgroundExit(); });
        _eventTrigger.triggers.Add(entryExit);
        
        if(BackGroundHighLight != null)
        {
            BackGroundHighLight.SetActive(false);
        }
    }

    public void ShowRequirementsWhenItCan()
    {
        if (_canvasObjectFlyEffect != null)
        {
            _canvasObjectFlyEffect.EffectStopped.AddListener(WaitForFlyEffectFinish);
        }
        else
        {
            WaitForFlyEffectFinish(null);
        }
    }

    private void WaitForFlyEffectFinish(ISpecialEffect _)
    {
        ShowRequirements();
    }

    private void OnBackgroundEnter()
    {
        if(!IsDragNow)
            return;
        ShowRequirements();
    }

    private void ShowRequirements()
    {
        ItemTransform.SetAsLastSibling();

        if (ShowRequirementsPanel != null)
        {
            ShowRequirementsPanel.Invoke(this);
        }

        IsProductsEnoughtForProduction = IsNeedToShowProducts();

        if (BackGroundHighLight != null && !BackGroundHighLight.activeSelf)
        {
            BackGroundHighLight.SetActive(true);
        }
    }


    private void OnBackgroundExit()
    {
        if(!IsDragNow)
            return;
        // игрок драгнул объект дальше допустимого для показа требований расстояния, выключаем панель необходимых продуктов
        if (HideRequirementsPanel != null)
        {
            HideRequirementsPanel.Invoke(this);
        }

        if (BackGroundHighLight != null)
            BackGroundHighLight.SetActive(false);
    }

    public void Hide()
    {
        ItemTransform.gameObject.SetActive(false);
    }

    public void Show()
    {
        ItemTransform.gameObject.SetActive(true);
        _targetGraphic.raycastTarget = true;
    }

    private void OnDisable()
    {
        _canvasObjectFlyEffect?.EffectStopped.RemoveListener(WaitForFlyEffectFinish);
        IsDragNow = true;
        OnBackgroundExit();
    }

    public void Clear()
    {
        ShowRequirementsPanel = null;
        HideRequirementsPanel = null;
    }

    public override void SetEmpty()
    {
        base.SetEmpty();
        _producingItemData = null;
        ItemTransform.gameObject.SetActive(false);
    }

    private bool IsNeedToShowProducts()
    {
        if (_producingItemData == null)
            return true;
        
        var balanceProduct = jProductsData.GetProduct(_producingItemData.OutcomingProducts);
        var thisProductIsUnlocked = balanceProduct.UnlockLevel <= jPlayerProfile.CurrentLevelData.CurrentLevel;

        if (_productCounter == null
            && _producingItemData.IncomingProducts.Count == 0
            && !thisProductIsUnlocked)
            return true;

        var isEnough = true;
        foreach (var product in _producingItemData.IncomingProducts)
        {
            int requiredForProduction = product.Value;
            int availableProductsCount = jPlayerProfile.PlayerResources.GetProductCount(product.Key);
            if (availableProductsCount < requiredForProduction)
            {
                isEnough = false;
                break;
            }
        }

        return isEnough;
    }

    public void SetProduct(IProducingItemData producingItemData, bool availableByLevel = true, bool isAgricultural = false)
    {
       
        if(producingItemData != null)
        {
            ItemTransform.gameObject.SetActive(true);

            _producingItemData = producingItemData;
            IsAvailable = availableByLevel;

            // делаем недоступные по уровню продукты серыми
            ItemImage.material = availableByLevel ? null : _grayMat;

            if(_lockedProductImage != null)
            {
                _lockedProductImage.SetActive(!availableByLevel);
                _lockedProductImage.transform.SetAsLastSibling();
            }

            SetItem(jResourcesManager.GetProductSprite(producingItemData.OutcomingProducts), !IsAvailable);
            IsProductsEnoughtForProduction = IsNeedToShowProducts();
           
            if (_counterGameObject != null) //костыль для отслеживания необходимости показывать счетчик (только для агро)
            {
                _counterGameObject.SetActive(availableByLevel);
                SetProductCounter(jPlayerProfile.PlayerResources.GetProductCount(producingItemData.OutcomingProducts));
            }
        }
    }

    private void SetProductCounter(int current)
    {
        _productCounter.text = current.ToString();
        _productCounterBack.sprite = current > 0 ? _equalToRequiredBackgr : _lessThanRequiredBackgr;
    }

    #endregion

    #region Drag
    
    public override void OnDrag(PointerEventData eventData)
    {
        if(!IsDragNow)
            return;

        if(ProducingItemData == null || !IsAvailable) return;
        base.OnDrag(eventData);

    }
    
    public override void OnBeginDrag(PointerEventData eventData)
    {
        if(!IsAvailable) return;

        _targetGraphic.raycastTarget = false;
        base.OnBeginDrag(eventData);        
    }

    public override void OnEndDrag(PointerEventData eventData)
    {
        if(!IsAvailable) return;
        _targetGraphic.raycastTarget = true;
        base.OnEndDrag(eventData); 
    }

    public override void OnPointerUp(PointerEventData eventData)
    {
        base.OnPointerUp(eventData);
        _targetGraphic.raycastTarget = true;
        if (HideRequirementsPanel != null)
            HideRequirementsPanel.Invoke(this);

        if(BackGroundHighLight != null)
        {
            BackGroundHighLight.SetActive(false);
        }
    }

    public override void OnPointerDown(PointerEventData eventData)
    {
        base.OnPointerDown(eventData);
        OnBackgroundEnter();
    }

    private void OnApplicationPause(bool paused)
    {
        if (!paused || !gameObject.activeSelf) return;

        OnPointerUp(new PointerEventData(null)); //если игрок держит тултип и сворачивает игру, то делаем ресет (NAN-4230)
    }

    #endregion
}