﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace Assets.Scripts.Engine.UI.Extensions.ProductionPanel
{
	public class HoverDraggableItem : DraggableItem
	{
		/// <summary>
		/// Object which DragItem was hovered on in previous frame
		/// </summary>
		private DragReceiver _lastHoverTarget;

		public override void OnDrag(PointerEventData eventData)
		{
			if (!IsDragNow)
				return;
			
			base.OnDrag(eventData);

			var dragReciever = GetDragReceiver();

			if (dragReciever != _lastHoverTarget)
			{
				if (_lastHoverTarget != null)
				{
					_lastHoverTarget.OnDragObjectExit(this);
				}

				if (dragReciever != null)
				{
					dragReciever.OnDragObjectEnter(this);
				}

				_lastHoverTarget = dragReciever;
			}
			else
			{
				//hover logic may be inserted here, but it's not needed right now
			}
		}

		public override void OnEndDrag(PointerEventData eventData)
		{
			if(_lastHoverTarget != null)
			{
				_lastHoverTarget.OnDragObjectExit(this);
			}

			base.OnEndDrag(eventData);
		}
		
		void OnApplicationFocus(bool hasFocus)
		{
			if (hasFocus)
				return;
			
			_lastHoverTarget = null;
			CancelDrag();
		}
	}
}
