﻿using System;
using UnityEngine;
using Assets.NanoLib.Utilities.Pulls;
using Assets.Scripts.Engine.UI.Views.BuildingsShopPanel;
using Assets.Scripts.GameLogic.Utilites;
using Assets.Scripts.MultiLanguageSystem.Logic;
using NanoLib.Core.Logging;
using NanoReality.Engine.UI.Views.BuildingsShopPanel;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.Services;
using NanoReality.GameLogic.Utilites;
using strange.extensions.signal.impl;
using TMPro;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;

namespace NanoReality.Engine.UI.Extensions.BuildingsShopPanel
{
    public class BuildingItem : GameObjectPullable,
        IBeginDragHandler, IDragHandler, IEndDragHandler
    {
        [SerializeField] private BuildingItemTab _activeTab;
        [SerializeField] private BuildingItemTab _inactiveTab;
        [SerializeField] private TextMeshProUGUI _priceText;
        [SerializeField] private Image _priceImage;
        [SerializeField] private GameObject _priceObject;
        [SerializeField] private GameObject _maxCountBuildingsBuild;
        [SerializeField] private GameObject _requiredLevelGameObject;
        [SerializeField] private TextMeshProUGUI _requiredLevelText;
        [SerializeField] private TextMeshProUGUI _amountText;
        [SerializeField] private Material _grayscaleMaterial;
        [SerializeField] private float _dragWaitingTime = 0.5f;
        [SerializeField] private float _minDragLengthForBuying = 30;

        [Tooltip("Angle from Up to Right")]
        [SerializeField] [Range(0f, 90f)] 
        private float _dragAngleForBuying = 70f;

        [SerializeField] private Button _buyBuildingButton;
        [SerializeField] private Button _selectBuildingButton;

        [SerializeField] private Image _serviceTypeIcon;
        
        private ScrollSnap _scrollSnap;
        private float _pointDownTime;
        private bool _isDragNow;
        private bool _isPointDown;

        public bool IsDragNow => _isDragNow;
        public Button BuyBuildingButton => _buyBuildingButton;

        public bool IsLongTap =>
            _pointDownTime > 0 && !IsDragNow && Time.timeSinceLevelLoad > _pointDownTime + _dragWaitingTime;

        public IMapObject CurrentMapObject { get; private set; }
        
        public Signal<BuildingItem> SignalOnBuyBuilding = new Signal<BuildingItem>();
        
        public Signal<BuildingItem> SignalOnBuyBuildingSuccess = new Signal<BuildingItem>();

        public Signal<IMapObject> SignalOnGoToBuilding = new Signal<IMapObject>();
        
        private Sprite _currentBuildingSprite;
        
        public bool IsSelected
        {
            get => _isSelected;
            set
            {
                if (_isSelected == value) 
                    return;
                
                _isSelected = value;
                SignalOnSelectChanged.Dispatch(this);
                OnChangeSelectedState();
            }
        }

        private bool _isSelected = true;

        public Signal<BuildingItem> SignalOnSelectChanged = new Signal<BuildingItem>();
        public Signal<BuildingItem> SignalOnInfoButtonClick = new Signal<BuildingItem>();

        public Signal SignalOnPointerDown = new Signal();
        public Signal SignalOnPointerUp = new Signal();        
        
        public Price MapObjectPrice { get; private set; }

        public BuildingState BuildingState { get; set; }

        private bool _ignoreSelect;
        private IIconProvider _iconProvider;

        public void UpdateItem(IIconProvider iconProvider, BuildingsShopItemData buildingsShopItemData, ScrollSnap scroll)
        {
            _iconProvider = iconProvider;
            _scrollSnap = scroll;
            CurrentMapObject = buildingsShopItemData.MapObjectData;
            MapObjectPrice = buildingsShopItemData.NextInstancePrice;
            BuildingState = buildingsShopItemData.BuildingState;
            _currentBuildingSprite = iconProvider.GetBuildingSprite(new MapObjectId(buildingsShopItemData.MapObjectData.ObjectTypeID));
            _activeTab.BuildingImage.sprite = _currentBuildingSprite;
            _inactiveTab.BuildingImage.sprite = _currentBuildingSprite;
            
            UiUtilities.ScaleUIBuilding(buildingsShopItemData.MapObjectData.Dimensions, _activeTab.BuildingImage);
            UiUtilities.ScaleUIBuilding(buildingsShopItemData.MapObjectData.Dimensions, _inactiveTab.BuildingImage);

            UpdateItem(buildingsShopItemData.UnlockLevel, buildingsShopItemData.CurrentBuildingsCount, buildingsShopItemData.LimitBuildingCount);
        }

        private void UpdateItem(int requiredLevel, int currentBuildingsCount, int maxBuildingsCount)
        {
            SetBuildingsCount(currentBuildingsCount, maxBuildingsCount);

            Material tabMaterial = null;
            
            switch (BuildingState)
            {
                case BuildingState.MaxCount:
                    _maxCountBuildingsBuild.SetActive(true);
                    _requiredLevelGameObject.SetActive(false);
                    _priceObject.gameObject.SetActive(false);
                    tabMaterial = _grayscaleMaterial;
                    break;
                case BuildingState.UnlockAt:
                    _maxCountBuildingsBuild.SetActive(false);
                    _requiredLevelGameObject.SetActive(true);
                    _priceObject.gameObject.SetActive(false);
                    tabMaterial = _grayscaleMaterial;
                    string text = 
                        string.Format(LocalizationUtils.Localize(LocalizationKeyConstants.BUILDINGSHOP_ITEM_LABEL_UNLOCKS_AT_LVL), requiredLevel + 1);
                    _requiredLevelText.SetLocalizedText(text);
                    break;
                case BuildingState.CanBeBuild:
                    _maxCountBuildingsBuild.SetActive(false);
                    _requiredLevelGameObject.SetActive(false);
                    _priceObject.gameObject.SetActive(true);
                    SetPriceView();
                    break;
                case BuildingState.NotEnoughPower:
                    _maxCountBuildingsBuild.SetActive(false);
                    _requiredLevelGameObject.SetActive(false);
                    _priceObject.gameObject.SetActive(false);
                    tabMaterial = _grayscaleMaterial;
                    break;
                case BuildingState.Locked:
                    _maxCountBuildingsBuild.SetActive(false);
                    _requiredLevelGameObject.SetActive(true);
                    _priceObject.gameObject.SetActive(false);
                    tabMaterial = _grayscaleMaterial;
                    _requiredLevelText.Localize(LocalizationKeyConstants.NOT_AVAILABLE);
                    break;
            }
            
            _activeTab.BuildingImage.material = tabMaterial;
            _inactiveTab.BuildingImage.material = tabMaterial;

            _serviceTypeIcon.enabled = false;
        }

        private void SetBuildingsCount(int currentBuildingsCount, int maxBuildingsCount)
        {
            var buildingNameText = 
#if PRODUCTION
                CurrentMapObject.Name;
#else
                $"{CurrentMapObject.Name}_{CurrentMapObject.ObjectTypeID}";  
#endif

            _amountText.text = $"{currentBuildingsCount.ToString()}/{maxBuildingsCount.ToString()}";
            _amountText.gameObject.SetActive(maxBuildingsCount != 0);
            
            _activeTab.BuildingTitle.SetLocalizedText(buildingNameText);
            _inactiveTab.BuildingTitle.SetLocalizedText(buildingNameText);
        }

        private void SetPriceView()
        {
            if (MapObjectPrice == null)
            {
                _priceImage.sprite = null;
                _priceText.text = string.Empty;
                return;
            }

            var localizedFreePriceText = LocalizationMLS.Instance.GetText(LocalizationKeyConstants.FREE_LABEL);
            switch (MapObjectPrice.PriceType)
            {
                case PriceType.Soft:
                    _priceText.text = MapObjectPrice.SoftPrice == 0 ? localizedFreePriceText : MapObjectPrice.SoftPrice.ToString();
                    break;
                case PriceType.Gold:
                    _priceText.text = MapObjectPrice.GoldPrice == 0 ? localizedFreePriceText : MapObjectPrice.GoldPrice.ToString();
                    break;
                case PriceType.Hard:
                    _priceText.text = MapObjectPrice.HardPrice == 0 ? localizedFreePriceText : MapObjectPrice.HardPrice.ToString();
                    break;
                default:
                    Logging.LogError(LoggingChannel.Ui, $"Not valid price type [{MapObjectPrice?.ToString()}]", gameObject);
                    break;
            }

            _priceImage.sprite = _iconProvider.GetCurrencySprite(MapObjectPrice.PriceType);
        }

        private void OnChangeSelectedState()
        {
            _activeTab.gameObject.SetActive(_isSelected);
            _inactiveTab.gameObject.SetActive(!_isSelected);
            if (_isSelected)
            {
                IgnoreScrollMaskForActiveTab();
            }
            else
            {
                ReturnActiveTabToScrollMask();
            }
        }

        private void IgnoreScrollMaskForActiveTab()
        {
            _activeTab.transform.SetParent(_scrollSnap.transform.parent, true);
            const int siblingDiffernceBetweenScrollAndFiltersButtons = 1;
            _activeTab.transform.SetSiblingIndex(_scrollSnap.transform.GetSiblingIndex() 
                + siblingDiffernceBetweenScrollAndFiltersButtons + 1);
        }

        private void ReturnActiveTabToScrollMask()
        {
            _activeTab.transform.SetParent(transform, true);
            _activeTab.transform.SetAsLastSibling();
        }

        #region Pullable

        public override void OnPop()
        {
            base.OnPop();
            _buyBuildingButton.onClick.AddListener(OnClickBuyButton);
            _selectBuildingButton.onClick.AddListener(OnSelectBuildingClick);
            _serviceTypeIcon.gameObject.SetActive(false);
            IsSelected = false;
            _isDragNow = false;
            _pointDownTime = 0f;
        }

        public override void OnPush()
        {
            _buyBuildingButton.onClick.RemoveListener(OnClickBuyButton);
            _selectBuildingButton.onClick.RemoveListener(OnSelectBuildingClick);
            SignalOnSelectChanged.RemoveAllListeners();
            SignalOnSelectChanged.RemoveAllOnceListeners();
            SignalOnBuyBuilding.RemoveAllListeners();
            SignalOnBuyBuilding.RemoveAllOnceListeners();
            SignalOnBuyBuildingSuccess.RemoveAllListeners();
            SignalOnBuyBuildingSuccess.RemoveAllOnceListeners();

            SignalOnPointerDown.RemoveAllListeners();
            SignalOnPointerUp.RemoveAllListeners();

            CurrentMapObject = null;
            _currentBuildingSprite = null;
            _activeTab.BuildingImage.sprite = null;
            _inactiveTab.BuildingImage.sprite = null;
            _activeTab.BuildingImage.material = null;
            base.OnPush();
        }
        #endregion
       
        #region buy building signal dispatching 
        public void OnPointerDown(PointerEventData eventData)
        {
            _isPointDown = true;
            _pointDownTime = Time.timeSinceLevelLoad;

            SignalOnPointerDown.Dispatch();
        }

        private void OnClickBuyButton()
        {
            DispatchBuyBuilding();
            DispatchGoToBuilding();
        }  
        
        private void OnSelectBuildingClick()
        {
            if (!IsSelected)
            {
                IsSelected = true;
            }
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            _isDragNow = true;
            _scrollSnap.OnBeginDrag(eventData);      
            _scrollSnap.ScrollRect.OnBeginDrag(eventData);
        }

        public void OnDrag(PointerEventData eventData)
        {
            if (!_isDragNow)
            {
                return;
            }
            
            _scrollSnap.OnDrag(eventData);
            _scrollSnap.ScrollRect.OnDrag(eventData);
            
            var diff = eventData.position - eventData.pressPosition;
            var angle = Vector2.Angle(diff.normalized, Vector2.up);
            
            if (angle < _dragAngleForBuying && diff.magnitude > _minDragLengthForBuying)
            {
                DispatchBuyBuilding();
            }
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            _scrollSnap.OnEndDrag(eventData);
            _scrollSnap.ScrollRect.OnEndDrag(eventData);
        }

//        private void Update()
//        {
//            if (!IsLongTap)
//                return;
//
//            //TODO Keep commented for now, maybe we will use it later
//            //DispatchBuyBuilding();
//
//            OnInfoRequirementsClick();
//            _pointDownTime = 0f;
//        }

        private void DispatchBuyBuilding()
        {
            if (!_priceObject.activeInHierarchy) 
                return;
            
            SignalOnBuyBuilding.Dispatch(this);
            _isDragNow = false;
            _isPointDown = false;
            _pointDownTime = 0f;
        }

        private void DispatchGoToBuilding()
        {
            if (!_priceObject.activeInHierarchy)
            {
                SignalOnGoToBuilding.Dispatch(this.CurrentMapObject);
            }
        }
        #endregion
        
        public void PlayBounceAnimation()
        {
            var animator = GetComponent<BuildingItemAnimator>();
            if (animator != null)
            {
                animator.PlayBounceAnimation();
            }
        }
        
        public void OnBuyBuildingSuccess() => SignalOnBuyBuildingSuccess.Dispatch(this);

        public void OnInfoProductButtonClick() => SignalOnInfoButtonClick.Dispatch(this);
  
        public void OnInfoRequirementsClick()
        {
            if (!_ignoreSelect)
            {
                IsSelected = !IsSelected;
            }

            _ignoreSelect = false;
        }
    }
}
