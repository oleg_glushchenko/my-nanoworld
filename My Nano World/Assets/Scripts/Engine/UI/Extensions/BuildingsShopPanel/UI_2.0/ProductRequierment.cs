﻿using System.Collections.Generic;
using Assets.NanoLib.Utilities;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Engine.UI.Components
{
    public class ProductRequierment : MonoBehaviour
    {
        [SerializeField] private RequiermentsCounter _counter;
        [SerializeField] private Image _productIcon;

        [SerializeField] private List<GameObject> _enoughtCheckmarks = new List<GameObject>();
        [SerializeField] private List<GameObject> _notEnoughtCheckmarks = new List<GameObject>();

        public Id<Product> ProductId { get; protected set; }

        public void UpdateRequierments(Id<Product> productId, Sprite icon, int current, int need)
        {
            ProductId = productId;
            _counter.Current = current;
            _counter.Need = need;
            _productIcon.sprite = icon;

            SetActiveGOs(_enoughtCheckmarks, current >= need);
            SetActiveGOs(_notEnoughtCheckmarks, need > current);
        }

        private void SetActiveGOs(List<GameObject> list, bool isActive)
        {
            for (int i = 0; i < list.Count; i++)
            {
                list[i].SetActive(isActive);
            }
        }
    }
}