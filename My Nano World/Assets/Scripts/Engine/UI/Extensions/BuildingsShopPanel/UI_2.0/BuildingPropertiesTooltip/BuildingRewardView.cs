﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Engine.UI.Views.BuildingsShopPanel
{
    public class BuildingRewardView : MonoBehaviour
    {
        [SerializeField]
        private Image _icon;
        
        [SerializeField]
        private TextMeshProUGUI _text;

        public void SetInfo(Sprite sprite, string text)
        {
            _icon.sprite = sprite;
            _text.text = text;
        }
    }
}
