﻿using System;
using System.Collections.Generic;
using System.Linq;
using NanoReality.Engine.UI.Views.Shared.Filters;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using strange.extensions.mediation.impl;
using strange.extensions.signal.impl;
using UnityEngine;

namespace NanoReality.Engine.UI.Views.BuildingsShopPanel
{
    public class ShopSectorsFilter : View
    {
        [Inject] public IPlayerProfile jPlayerProfile { get; set; }
        
        [SerializeField] private List<ShopSectorsFilterToogle> _sectorsFilterList;
        
        public Signal<ShopSectorsFilter> SignalOnFilterChanged = new Signal<ShopSectorsFilter>();

        private BusinessSectorsTypes _lastFilter = BusinessSectorsTypes.Unsupported;

        private void OnEnable()
        {
            foreach (var filter in _sectorsFilterList)
            {
                filter.Toggle.onValueChanged.AddListener(OnSomeFilterValueChanged);
            }
        }

        private void OnDisable()
        {
            foreach (var filter in _sectorsFilterList)
            {
                filter.Toggle.onValueChanged.RemoveListener(OnSomeFilterValueChanged);
            }
        }

        private void OnSomeFilterValueChanged(bool state)
        {
            if (state && _lastFilter != CurrentSelectedCategory)
            {
                _lastFilter = CurrentSelectedCategory;

                SignalOnFilterChanged.Dispatch(this);
            }
        }
        
        public BusinessSectorsTypes CurrentSelectedCategory
        {
            get
            {
                foreach (var filter in _sectorsFilterList)
                {
                    if (filter.Toggle.isOn) return filter.SectorsFilterType;
                }

                return BusinessSectorsTypes.Unsupported;
            }
            set
            {
                foreach (var filter in _sectorsFilterList)
                {
                    if (filter.SectorsFilterType == value && !filter.Toggle.isOn)
                    {
                        filter.Toggle.isOn = true;
                        return;
                    }
                }
            }
        }

        public void DisableAllFilters()
        {
            foreach (var filter in _sectorsFilterList)
            {
                filter.Toggle.isOn = false;
            }
        }
    }
    
    [Serializable]
    public class ShopSectorsFilterToogle : Filter
    {
        public BusinessSectorsTypes SectorsFilterType;
    }
}