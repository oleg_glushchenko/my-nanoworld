﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Engine.UI.Extensions.BuildingsShopPanel
{
    public class SubCategoryFilterView : MonoBehaviour
    {
        [SerializeField] private List<SubCategoryFilter> _subCategoryFilter;
        [SerializeField] private BuildingsCategories _category;
        [SerializeField] private ToggleGroup _toggleGroup;
        
        public BuildingsCategories Category => _category;
        public BuildingSubCategories CurrentSubCategory { get; private set; } = BuildingSubCategories.None;

        public event Action<BuildingSubCategories> SubCategoryChanged;
        
        protected void Start()
        {
            _toggleGroup.SetAllTogglesOff();
            
            foreach (SubCategoryFilter filter in _subCategoryFilter)
            {
                filter.Toggle.onValueChanged.AddListener(_ =>
                {
                    filter.SelectedImage.SetActive(false);

                    if (filter.Toggle.isOn)
                    {
                        if (CurrentSubCategory == filter.SubCategory)
                        {
                            CurrentSubCategory = BuildingSubCategories.None;
                            _toggleGroup.SetAllTogglesOff();
                        }
                        else
                        {
                            CurrentSubCategory = filter.SubCategory;
                            filter.SelectedImage.SetActive(true);
                        }
                    }
                    else
                    {
                        if (CurrentSubCategory == filter.SubCategory)
                        {
                            CurrentSubCategory = BuildingSubCategories.None;
                        }
                    }

                    SubCategoryChanged?.Invoke(filter.SubCategory);
                });
            }
        }
        
        public void InitCategory(BuildingSubCategories subCategory)
        {
            SubCategoryFilter filter;
            CurrentSubCategory = subCategory;

            if (subCategory != BuildingSubCategories.None)
            {
                filter = _subCategoryFilter.Find(f => f.SubCategory == subCategory);
                filter.Toggle.Select();
                CurrentSubCategory = filter.SubCategory;
                SubCategoryChanged?.Invoke(filter.SubCategory);
            }
            else
            {
                foreach (SubCategoryFilter disableFilter in _subCategoryFilter)
                {
                    disableFilter.SelectedImage.SetActive(false);
                }
                _toggleGroup.SetAllTogglesOff();
            }
                
            
            SubCategoryChanged?.Invoke(CurrentSubCategory);
        }
        
        [Serializable]
        public class SubCategoryFilter
        {
            [SerializeField] private BuildingSubCategories _subCategory;
            [SerializeField] private Toggle _toggle;
            [SerializeField] private GameObject _selectedImage;

            public BuildingSubCategories SubCategory => _subCategory;
            public Toggle Toggle => _toggle;
            public GameObject SelectedImage => _selectedImage;
        }
    }
}

