﻿using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace NanoReality.Engine.UI.Extensions.BuildingsShopPanel
{
    public sealed class BuildingItemTab : MonoBehaviour
    {
        [SerializeField] private Image _buildingIconImage;
        [SerializeField] private TextMeshProUGUI _buildingTitleText;
        
        public Image BuildingImage => _buildingIconImage;

        public TextMeshProUGUI BuildingTitle => _buildingTitleText;
    }
}