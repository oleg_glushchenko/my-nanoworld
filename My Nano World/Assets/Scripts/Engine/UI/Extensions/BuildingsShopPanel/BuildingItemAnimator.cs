﻿using System;
using System.Collections;
using Assets.Scripts.GameLogic.TutorialAndHintsSystem.Common.TutorialLookAtController;
using DG.Tweening;
using NanoReality.Engine.UI.Extensions.BuildingsShopPanel;
using NanoReality.Game.Tutorial;
using NanoReality.GameLogic.Utilities;
using UniRx;
using UnityEngine;

namespace Assets.Scripts.Engine.UI.Views.BuildingsShopPanel
{
    [RequireComponent(typeof (BuildingItem))]
    public class BuildingItemAnimator : MonoBehaviour, IPointerTargetAnimator
    {
        [SerializeField] private RectTransform _target;
        [SerializeField] private BuildingItem _targetItem;

        [SerializeField] private float _scaleTweenDuration = 0.3f;

        [SerializeField] private float _offsetWhenDrag = 20f;

        [SerializeField] private Vector3 _scaleWhenDrag = new Vector3(1.25f, 1.25f, 1f);

        private Sequence _sequence;
        private Tweener _tweenerScale;
        private Tweener _tweenerOffset;
        private Vector3 _originalPosition;

        private void Awake()
        {
            _originalPosition = _target.localPosition;
        }
        
        private void OnEnable()
        {
            _target.gameObject.SetActive(true);
            ResetPositionAndScale();
            _targetItem.SignalOnPointerDown.AddListener(OnPointerDown);
            _targetItem.SignalOnPointerUp.AddListener(OnPointerUp);
            _targetItem.SignalOnBuyBuildingSuccess.AddListener(OnBuyBuildingSuccess);
        }

        private void OnDisable()
        {
            _sequence?.Kill(true);
            _targetItem.SignalOnPointerDown.RemoveListener(OnPointerDown);
            _targetItem.SignalOnPointerUp.RemoveListener(OnPointerUp);
            _targetItem.SignalOnBuyBuildingSuccess.RemoveListener(OnBuyBuildingSuccess);
        }

        private void OnDestroy()
        {
            _sequence?.Kill();
            _tweenerScale?.Kill();
            _tweenerOffset?.Kill();
        }

        private void OnPointerDown()
        {
            PlayPointerDownAnimation();
        }

        private void OnPointerUp()
        {
            PlayBounceAnimation();
        }

        private void OnBuyBuildingSuccess(BuildingItem item)
        {
            _target.gameObject.SetActive(false);
        }

        private void PlayPointerDownAnimation()
        {
            _sequence?.Kill(true);
            _tweenerScale = _target.DOScale(_scaleWhenDrag, _scaleTweenDuration);
            _tweenerOffset = _target.DOAnchorPosY(_target.anchoredPosition.y + _offsetWhenDrag, _scaleTweenDuration);
        }

        private Action<PointerMode, Action> _playItemAnimationCallback;
        private IDisposable _animationCoroutine;
        private PointerMode _animationPointerMode;

        public void PlayItemAnimation(Action<PointerMode, Action> onComplete, PointerMode pointerMode)
        {
            _playItemAnimationCallback = onComplete;
            _animationPointerMode = pointerMode;

            _animationCoroutine = Observable.FromCoroutine(LoopPlayBounceAnimation).Subscribe();
        }

        public void StopItemAnimation()
        {
            _animationCoroutine?.Dispose();
        }

        private IEnumerator LoopPlayBounceAnimation()
        {
            bool isShowHand = false;
            while (true)
            {
                yield return new WaitWhile(() => isShowHand);
                PlayBounceAnimation();
                yield return new WaitForSeconds(0.3f);
                isShowHand = true;
                _playItemAnimationCallback.SafeRaise(_animationPointerMode, () => { isShowHand = false;});
            }
        }

        public void PlayBounceAnimation(Action onComplete = null)
        {
            const float sequenceDuration = 0.2f;
            var targetScale = new Vector2(1.2f, 0.85f);
            ResetPositionAndScale();
            _tweenerScale?.Kill();
            _tweenerScale = null;

            _tweenerOffset?.Kill();
            _tweenerOffset = null;

            _sequence?.Kill(true);
            _sequence = DOTween.Sequence();
            var scaleUp = _target.transform.DOScale(targetScale, sequenceDuration).SetEase(Ease.InSine);
            var scaleDown = _target.transform.DOScale(Vector2.one, sequenceDuration).SetEase(Ease.OutBack);
            _sequence.Append(scaleUp).Append(scaleDown);
            _sequence.OnComplete(() => onComplete?.Invoke());
            _sequence.Play();
        }

        private void ResetPositionAndScale()
        {
            _target.localScale = Vector3.one;
            _target.localPosition = _originalPosition;
        }
    }
}