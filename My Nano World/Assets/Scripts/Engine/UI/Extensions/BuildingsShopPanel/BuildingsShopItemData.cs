﻿using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;

namespace NanoReality.Engine.UI.Views.BuildingsShopPanel
{
    public struct BuildingsShopItemData
    {
        public IMapObject MapObjectData;

        public int UnlockLevel;

        public int CurrentBuildingsCount;

        public int LimitBuildingCount;

        public Price NextInstancePrice;

        public BuildingState BuildingState;

        public override string ToString()
        {
            return $"Id: [{MapObjectData.ObjectTypeID}] Name:[{MapObjectData.Name}] UnlockLevel: [{UnlockLevel}] CurrentCount: [{CurrentBuildingsCount}] MaxCount: [{LimitBuildingCount}] Delta: [{LimitBuildingCount - CurrentBuildingsCount}]";
        }
    }

    public enum BuildingState
    {
        /// <summary>
        /// Building can be constructed
        /// </summary>
        CanBeBuild,

        /// <summary>
        /// Power plant maximum capacity reached
        /// </summary>
        NotEnoughPower,

        /// <summary>
        /// Building will be unlock at player level X
        /// </summary>
        UnlockAt,

        /// <summary>
        /// Maximum buildings count on map reached
        /// </summary>
        MaxCount,
        
        /// <summary>
        /// Building is locked by conditions
        /// </summary>
        Locked,
    }
}
