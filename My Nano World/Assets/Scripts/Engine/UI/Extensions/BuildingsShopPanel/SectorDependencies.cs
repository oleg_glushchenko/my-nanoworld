﻿using System;
using NanoReality.Engine.UI.Views.BuildingsShopPanel;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;

namespace NanoReality.Engine.UI.Extensions.BuildingsShopPanel
{
    [Serializable]
    public class SectorDependencies
    {
        public BusinessSectorsTypes SectorType;

        public BuildingsFilter CategoriesFilter;

        public void SetActiveFilters(bool newState, BuildingsCategories excludedCategory = BuildingsCategories.None)
        {
            foreach (var filter in CategoriesFilter.Filters)
            {
                if (excludedCategory == BuildingsCategories.None || excludedCategory != filter.BuildingsCategory)
                {
                    filter.Toggle.gameObject.SetActive(newState);
                }
            }
        }
    }
}
