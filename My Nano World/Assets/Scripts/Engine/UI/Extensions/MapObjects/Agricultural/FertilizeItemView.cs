﻿using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Factories;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Engine.UI.Extensions.Agricultural
{
    public class FertilizeItemView : MonoBehaviour
    {
        public DraggableCropItem FertilizeItem;
        public Button BackgroundButton;

        public void SetProduct(Canvas canvas, IProducingItemData producingItemData)
        {
            FertilizeItem.SetProduct(producingItemData);
        }

        public void SetEnabled(bool isEnabled)
        {
            BackgroundButton.interactable = isEnabled;
            FertilizeItem.IsAvailable = isEnabled;
        }
    }
}