﻿using System.Collections.Generic;
using NanoReality.Engine.UI.Common;
using NanoReality.Engine.UI.Views;
using NanoReality.GameLogic.BuildingSystem.Happiness;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Engine.UI.Extensions.MapObjects.CityHallTaxes
{
    public class CityHallTaxesView : BuildingPanelView
    {
        private const int MinHappinessBoundValue = 0; //лейбл для минимального столбца (HappyColumn)
        private const int MaxHappinessBoundValue = 100; //лейбл для максимального столбца (HappyColumn)

        [SerializeField] private TextMeshProUGUI _taxPerHour;
        [SerializeField] private TextMeshProUGUI _maxTaxes;
        [SerializeField] private TextMeshProUGUI _taxesToCollect;
        [SerializeField] private TextMeshProUGUI _populationCount;

        [SerializeField] private Button _collectButton;

        [SerializeField] private List<HappyColumn> _happinessColumns;
        [SerializeField] private BuildingUpgradeButtonView _upgradeButton;
        [SerializeField] private List<GameObject> _infoElements;
        [SerializeField] private LoadingSpinner _loadTaxesSpinner;

        public TextMeshProUGUI PopulationCount => _populationCount;
        public TextMeshProUGUI TaxPerHour => _taxPerHour;
        public TextMeshProUGUI MaxTaxes => _maxTaxes;
        public Button CollectButton => _collectButton;
        public Button UpgradeButton => _upgradeButton.Button;
        public TextMeshProUGUI TaxesToCollect => _taxesToCollect;
        public Vector2 TaxesToCollectPosition => _taxesToCollect.gameObject.transform.position;
        public BuildingUpgradeButtonView BuildingUpgradeButton => _upgradeButton;

        public LoadingSpinner LoadTaxesSpinner => _loadTaxesSpinner;

        public override void Initialize(object parameters)
        {
            base.Initialize(parameters);
            HideInfoElements();
        }

        public void OnInfoButtonClick()
        {
            foreach (var element in _infoElements)
            {
                element.SetActive(!element.activeSelf);
            }
        }

        private void HideInfoElements()
        {
            foreach (var element in _infoElements)
            {
                element.SetActive(false);
            }
        }

        public void UpdateHappinessData(IHappinessData happinessData)
        {
            //выставляем минимальные и максимальные граници для колонок
            _happinessColumns[0].SetMinBound(MinHappinessBoundValue);
            _happinessColumns[_happinessColumns.Count - 1].SetMaxBound(MaxHappinessBoundValue);

            var satisfaction = happinessData.TotalHappinessPercent;
            foreach (var column in _happinessColumns)
            {
                column.SetColumn(satisfaction);
            }
        }
    }
}