﻿using System;
using Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Models.impl;
using Assets.Scripts.GameLogic.Utilites;
using Assets.Scripts.MultiLanguageSystem.Logic;
using NanoReality.Engine.UI.Views;
using NanoReality.Game.UI;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Game.Ui
{
    public class DwellingPanelView : BuildingPanelView<MDwellingHouseMapObject>
    {
        [SerializeField] private TextMeshProUGUI _populationText;
        [SerializeField] private TextMeshProUGUI _titleText;
        [SerializeField] private BuildingUpgradeButtonView _upgradeButton;

        public Button UpgradeButton => _upgradeButton.Button;
    
        public event Action<IMapBuilding> UpgradeButtonClick;

        public override void Initialize(object parameters)
        {
            base.Initialize(parameters);

            _upgradeButton.Set(TargetBuilding, OnUpgradeButtonClick);
            _populationText.text = TargetBuilding.MaxPopulation.ToString();
            _titleText.SetLocalizedText(GetLocalizedTitle());
        }

        public void OnExitPressed()
        {
            Hide();
        }

        private void OnUpgradeButtonClick()
        {
            if (TargetBuilding == null) return;

            _upgradeButton.Button.interactable = false;
            UpgradeButtonClick?.Invoke(TargetBuilding);
        }

        private string GetLocalizedTitle()
        {
            return string.Format(LocalizationMLS.Instance.GetText(LocalizationKeyConstants.UPGRADE_PANEL_BUILDING_NAME_WITH_LEVEL),
                TargetBuilding.Name.ToUpper() + "\r\n", TargetBuilding.Level);
        }
    }
}