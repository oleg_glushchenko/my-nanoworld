﻿using System;
using Assets.NanoLib.Utilities;
using Assets.NanoLib.Utilities.Pulls;
using NanoReality.Core.Resources;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using strange.extensions.mediation.impl;
using strange.extensions.signal.impl;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace NanoReality.Engine.UI.Views.LevelUpPanel
{
    /// <summary>
    /// Товар разблокированный при левелапе
    /// </summary>
    public class LevelUpUnlokedItem : View, IPullableObject, IPointerUpHandler
    {
        [SerializeField] private Image _productImage;
        [SerializeField] private GameObject _checkMark;
        
        [SerializeField] private GameObject _buildingPlane;
        [SerializeField] private Image _buildingIconImage;

        private LongPressEventTrigger _longPress;
        
        public Id<Product> TargetProduct;

        public Signal<LevelUpUnlokedItem> OnLongPress = new Signal<LevelUpUnlokedItem>();
        public Signal SignalOnPointerUp = new Signal();

        [Inject] public IResourcesManager jResourcesManager { get; set; }

        protected override void Awake()
        {
            // инжект в авейке
            if (autoRegisterWithContext && !registeredWithContext)
                bubbleToContext(this, true, true);

            _longPress = GetComponent<LongPressEventTrigger>();

            if (_longPress != null)
                _longPress.onLongPress.AddListener(OnLongPressCallback);
        }

        /// <summary>
        /// Устанавливает товар в айтем
        /// </summary>
        /// <param name="product">айди товара</param>
        /// <exception cref="ArgumentNullException">если айди товара == null</exception>
        public void SetProduct(Id<Product> product)
        {
            if(product == null)
                throw new ArgumentNullException("product");
            _buildingPlane.SetActive(false);
            _buildingIconImage.enabled = false;
            _productImage.enabled = true;

            TargetProduct = product;
            _productImage.sprite = jResourcesManager.GetProductSprite(product.Value);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            SignalOnPointerUp.Dispatch();
            _checkMark.SetActive(false);
        }

        private void OnLongPressCallback()
        {
            OnLongPress.Dispatch(this);
            if(!_checkMark.activeSelf)
                _checkMark.SetActive(true);
        }

        #region IPullable
        public object Clone()
        {
            return Instantiate(this);
        }

        public IObjectsPull pullSource { get; set; }
        public void OnPop()
        {
            gameObject.SetActive(true);
            _checkMark.SetActive(false);
        }

        public void OnPush()
        {
            gameObject.SetActive(false);
            _productImage.sprite = null;
            TargetProduct = null;
            _checkMark.SetActive(false);
            OnLongPress.RemoveAllListeners();
            SignalOnPointerUp.RemoveAllListeners();
        }

        public void FreeObject()
        {
            if(pullSource != null)
                pullSource.PushInstance(this);
            else
            {
                DestroyObject();
            }
        }

        public void DestroyObject()
        {
            Destroy(gameObject);
        }
        #endregion

        public Id_IMapObjectType MapObjectType;

        public void SetBuilding(IMapObject mapObject, Sprite buildingSprite)
        {
            _buildingPlane.SetActive(true);
            _buildingIconImage.enabled = true;
            _productImage.enabled = false;
            MapObjectType = mapObject.ObjectTypeID;
            _buildingIconImage.sprite = buildingSprite;
            UiUtilities.ScaleUIBuilding(mapObject.Dimensions, _buildingIconImage);
        }
    }
}