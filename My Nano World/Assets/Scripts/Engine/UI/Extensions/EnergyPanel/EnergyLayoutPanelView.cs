﻿using System;
using System.Collections.Generic;
using Assets.NanoLib.UI.Core.Views;
using NanoReality.GameLogic.BuildingSystem.MapObjects;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Engine.UI.Views.Energy
{
    public class EnergyLayoutPanelView : UIPanelView
    {
        [SerializeField] private Toggle _topRight;
        [SerializeField] private Toggle _topLeft;
        [SerializeField] private Toggle _bottomLeft;
        [SerializeField] private Toggle _bottomRight;
        public Toggle TopRight => _topRight;
        public Toggle TopLeft => _topLeft;
        public Toggle BottomLeft => _bottomLeft;
        public Toggle BottomRight => _bottomRight;
     
        public IAoeBuilding PowerPlant { get; private set; }
        
        protected override void Awake()
        {
            base.Awake();
            _topRight.onValueChanged.AddListener(b => OnToggleChanged(b, AoeLayout.TopRight));
            _topLeft.onValueChanged.AddListener(b => OnToggleChanged(b, AoeLayout.TopLeft));
            _bottomLeft.onValueChanged.AddListener(b => OnToggleChanged(b, AoeLayout.BottomLeft));
            _bottomRight.onValueChanged.AddListener(b => OnToggleChanged(b, AoeLayout.BottomRight));
        }
        
        protected override void OnDestroy()
        {
            base.OnDestroy();
            _topRight.onValueChanged.RemoveAllListeners();
            _topLeft.onValueChanged.RemoveAllListeners();
            _bottomLeft.onValueChanged.RemoveAllListeners();
            _bottomRight.onValueChanged.RemoveAllListeners();
        }

        
        public override void Initialize(object parameters)
        {
            base.Initialize(parameters);
            PowerPlant = (IAoeBuilding) parameters;

            SetActiveAoe(PowerPlant.AoeLayoutType);
        }

        private Toggle GetToggleFor(AoeLayout layout)
        {
            switch (layout)
            {
                case AoeLayout.BottomRight:
                    return _bottomRight;
                case AoeLayout.BottomLeft:
                    return _bottomLeft;
                case AoeLayout.TopLeft:
                    return _topLeft;
                case AoeLayout.TopRight:
                    return _topRight;
                default:
                    Debug.LogError("Unexpected result." + layout);
                    return null;
            }
        }
        private void SetActiveAoe(AoeLayout layout)
        {
            GetToggleFor(layout).isOn = true;
        }

        public void OnToggleChanged(bool isEnabled, AoeLayout aoeLayout)
        {
            if (PowerPlant == null)
                return;
            
            if (isEnabled && aoeLayout != PowerPlant.AoeLayoutType)
            {
                PowerPlant.SwitchAoeLayout(aoeLayout);
            }
        }
    }
}