﻿using System;
using NanoLib.Services.InputService;
using Assets.NanoLib.UI.Core.Signals;
using Assets.NanoLib.UI.Core.Views;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using NanoLib.Core.Services.Sound;
using NanoReality.Engine.UI.Extensions;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.Engine.UI.Views.Energy;
using NanoReality.Engine.UI.Views.Panels;
using NanoReality.Game.Tutorial;
using NanoReality.GameLogic.Upgrade;

public class EnergyPanelMediator : BuildingPanelMediator<IPowerPlantBuilding, EnergyPanelView>
{
    [Inject] public SignalOnNonPlayableBuildTap jSignalOnNonPlayableBuildTap { get; set; }
    
    protected MapObjectintTypes TargetMapObjectTypeOfPanel => MapObjectintTypes.PowerPlant;

    protected override void OnShown()
    {
        base.OnShown();
        jSignalOnNonPlayableBuildTap.AddListener(OnNonPlaybleViewTap);
        jSignalOnMapObjectViewTap.AddListener(OnTapView);
        ShowWindowSignal.Dispatch(typeof(EnergyLayoutPanelView), CurrentModel);
        View.UpgradeButton.Set(CurrentModel, OnUpgradeButtonClick);
        SetGrayscaleMode(true);
        jHighlightController.EnableAoe(CurrentModel);
        jSoundManager.Play(SfxSoundTypes.SolarPanel, SoundChannel.SoundFX);
    }

    protected override void OnHidden()
    {
        jSignalOnNonPlayableBuildTap.RemoveListener(OnNonPlaybleViewTap);
        
        jSignalOnMapObjectViewTap.RemoveListener(OnTapView);
        
        HideWindowSignal.Dispatch(typeof(EnergyLayoutPanelView));
        SetGrayscaleMode(false);
        jHighlightController.DisableAoeMode();
        jSoundManager.Stop("turbine_factory", SoundChannel.SoundFX);
        base.OnHidden();
    }

    private void OnTapView(MapObjectView mapObjectView)
    {
        if (mapObjectView.MapObjectModel != null &&
            mapObjectView.MapObjectModel.MapObjectType == TargetMapObjectTypeOfPanel)
            return;

        Hide();
    }
    
    private void OnUpgradeButtonClick()
    {
        View.UpgradeButton.Button.interactable = false;
        Hide();
        
        ShowWindowSignal.Dispatch(typeof(UpgradePanelView), CurrentModel);
    }

    private void OnNonPlaybleViewTap(NonPlayableBuildingView nonPlayableBuildingView) => Hide();
}