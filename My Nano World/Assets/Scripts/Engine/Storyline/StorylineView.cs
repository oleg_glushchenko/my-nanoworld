﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Engine.Analytics.Model.api;
using NanoReality.GameLogic.BuildingSystem.CityCamera.Models.Api;
using strange.extensions.mediation.impl;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Engine.Storyline
{
	public sealed class StorylineView :  View
	{
		[SerializeField] private Camera _storyCamera;
		[SerializeField] private float _tabledCameraSize;
		[SerializeField] private float _phoneCameraSize;
		[SerializeField] private List<StorylineScene> _scenesList; 
		[SerializeField] private Button _skipStorylineButton; 

		private int _currentSceneIndex;
		private StorylineScene _currentLoadedScene;

		[Inject] public IGameCamera jGameCamera { get; set; }
		[Inject] public INanoAnalytics NanoAnalytics { get; set; }
		[Inject] public SignalOnStorylineCompleted SignalOnStorylineCompleted { get; set; }

		/// <summary>
		/// Starts plaing story from first scene
		/// </summary>
		public void PlayStory()
		{
			ConfigureCamera();
			_currentSceneIndex = 0;
			PlayScene();
			jGameCamera.SetActive(false);
			_skipStorylineButton.onClick.AddListener(SkipStory);
		}

		public void SkipStory()
		{
			StorylineCompleted();
		    NanoAnalytics.SkipIntroVideo();
		}
		
		private void PlayScene()
		{
			_currentLoadedScene = _scenesList[_currentSceneIndex];
			_currentLoadedScene.gameObject.SetActive(true);
			_currentLoadedScene.OnComplete.AddListener(OnSceneCompleted);
			_currentLoadedScene.PlayScene();
		}

		private void OnSceneCompleted(StorylineScene storylineScene)
		{
			_currentLoadedScene.gameObject.SetActive(false);
			storylineScene.OnComplete.RemoveListener(OnSceneCompleted);
			UnloadScene(storylineScene);
			_currentSceneIndex++;
			if(_currentSceneIndex<_scenesList.Count)
				PlayScene();
			else
				StorylineCompleted();
		}

		private void StorylineCompleted()
		{
			_currentLoadedScene.OnComplete.RemoveListener(OnSceneCompleted);
			_skipStorylineButton.onClick.RemoveListener(SkipStory);
			UnloadScene(_currentLoadedScene);
			_currentLoadedScene = null;
			
			_storyCamera.enabled = false;
			UnityEngine.Resources.UnloadUnusedAssets();

			jGameCamera.SetActive(true);
			SignalOnStorylineCompleted.Dispatch();
			Destroy(gameObject);
		}
		
		private void UnloadScene(StorylineScene scene)
		{
			Destroy(scene.gameObject);
			UnityEngine.Resources.UnloadUnusedAssets();
		}

		private void ConfigureCamera()
		{
			_storyCamera.orthographicSize = IsTabledAspect() ? _tabledCameraSize : _phoneCameraSize;
			_storyCamera.enabled = true;
		}

		private bool IsTabledAspect()
		{
			// Планшетом мы считаем только аспект 4:3
			return Mathf.Approximately((float) Math.Round((float)Screen.width / Screen.height, 2), (float) Math.Round( 4f/3f, 2));
		}
	}

}


