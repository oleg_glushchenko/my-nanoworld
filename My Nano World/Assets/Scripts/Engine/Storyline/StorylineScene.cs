﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.MultiLanguageSystem.Logic;
using strange.extensions.signal.impl;
using Spine;
using Spine.Unity;
using TMPro;
using UnityEngine;
using Event = Spine.Event;

namespace NanoReality.Engine.Storyline
{
	/// <summary>
	/// Storyline separate scene 
	/// </summary>
	public class StorylineScene : MonoBehaviour
	{

		/// <summary>
		/// Анимация спайна
		/// </summary>
		[SerializeField]
		private SkeletonAnimation _skeletonAnimation;
		
		/// <summary>
		/// Вызывается по окончанию проигрывания
		/// </summary>
		public Signal<StorylineScene> OnComplete = new Signal<StorylineScene>();

		/// <summary>
		/// Список анимаций в сцене, которые проигрываются в очереди
		/// </summary>
		[SerializeField]
		private List<SceneAnimationsConfig> _animations;

		[SerializeField]
		private List<StorylinePhraseText> _phraseTexts;

		
		/// <summary>
		/// Последний трек в очереди анимаций
		/// </summary>
		private TrackEntry _lastAnimEntry;

		private readonly Dictionary<TrackEntry, SceneAnimationsConfig> _tracks = new Dictionary<TrackEntry, SceneAnimationsConfig>();
		
		/// <summary>
		/// Проигрывает сцену
		/// </summary>
		public void PlayScene()
		{
			if (_animations.Count == 0)
				throw new IndexOutOfRangeException("Animations list is empty");

			LocalizeDialogTexts();

			_lastAnimEntry = null;
			_tracks.Clear();
			_lastAnimEntry = _skeletonAnimation.state.SetAnimation(0, _animations[0].AnimationName, false);
			_tracks.Add(_lastAnimEntry, _animations[0]);
			SubscribeCustomEvents(_animations[0]);
			
			for (int i = 1; i < _animations.Count; i++)
			{
				SceneAnimationsConfig a = _animations[i];
				_lastAnimEntry = _skeletonAnimation.state.AddAnimation(0, a.AnimationName, false, a.Delay);
				_tracks.Add(_lastAnimEntry, a);
				SubscribeCustomEvents(a);
			}
			
			_skeletonAnimation.state.Complete+= OnCompleteCallback;
		}

		private void LocalizeDialogTexts()
		{
			foreach (StorylinePhraseText phraseText in _phraseTexts.Where(phraseText => phraseText.Label != null))
			{
				phraseText.Label.Localize(phraseText.Key);
			}
		}

		private void SubscribeCustomEvents(SceneAnimationsConfig a)
		{
			if (a.EventObjects != null && a.EventObjects.Count > 0)
			{
				_lastAnimEntry.Event += CustomAnimEventCallback;
			}
		}

		/// <summary>
		/// Включаем дополнительные объекты в сцене по кастомному ивенту (тексты)
		/// </summary>
		/// <param name="trackEntry"></param>
		/// <param name="event"></param>
		private void CustomAnimEventCallback(TrackEntry trackEntry, Event @event)
		{
			SceneAnimationsConfig config = _tracks[trackEntry];
			foreach (EventObjects e in config.EventObjects.Where(e => e.EventName == @event.Data.name))
			{
				SetActiveObjectsInAnimation(e.AdditionalObjects, true);
			}
		}

		private static void SetActiveObjectsInAnimation(IEnumerable<GameObject> gos, bool isActive)
		{
			foreach (GameObject obj in gos)
			{
				obj.SetActive(isActive);
			}
		}

		/// <summary>
		/// Колбек окончания проигрывания очереди анимаций
		/// </summary>
		/// <param name="trackEntry"></param>
		private void OnCompleteCallback(TrackEntry trackEntry)
		{
			SceneAnimationsConfig config = _tracks[trackEntry];
			for (int i = 0; i < config.EventObjects.Count; i++)
			{
				if (config.EventObjects[i].IsDisabledAfterComplete)
				{
					SetActiveObjectsInAnimation(config.EventObjects[i].AdditionalObjects, false);
				}
			}
			trackEntry.Event -= CustomAnimEventCallback;
			trackEntry.Complete -= OnCompleteCallback;

			if (_lastAnimEntry != trackEntry)
				return;
			
			_lastAnimEntry = null;
			OnComplete.Dispatch(this);
		}

		/// <summary>
		/// Настройки анимации в сцене
		/// </summary>
		[Serializable]
		public struct SceneAnimationsConfig
		{
			/// <summary>
			/// Название анимации
			/// </summary>
			public string AnimationName;

			/// <summary>
			/// Задержка проигрывания анимации
			/// </summary>
			public float Delay;

			/// <summary>
			/// Дополнительные объекты, включаются при срабатывании ивента в анимаии
			/// </summary>
			public List<EventObjects> EventObjects;

		}
		
		/// <summary>
		/// Дополнительные объекты, включаются при срабатывании ивента в анимаии
		/// </summary>
		[Serializable]
		public struct EventObjects
		{
			/// <summary>
			/// Имя ивента по которому включаются объекты
			/// </summary>
			public string EventName;
				
			/// <summary>
			/// Дополнительные объекты в сцене которые включаются вместе с анимацией
			/// </summary>
			public List<GameObject> AdditionalObjects;

			/// <summary>
			/// Выключать ли объекты после анимации
			/// </summary>
			public bool IsDisabledAfterComplete;
		}

		[Serializable]
		public struct StorylinePhraseText
		{
			public TextMeshPro Label;
			public string Key;
		}
	}
}
