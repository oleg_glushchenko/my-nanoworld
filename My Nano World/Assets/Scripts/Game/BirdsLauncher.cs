﻿using UnityEngine;
using System.Collections;
using System;

namespace NanoReality.Game
{
	public class BirdsLauncher : MonoBehaviour
	{
		[SerializeField] private ParticleSystem _birdsEmitter;
		[SerializeField] private float _minDistance = 50.0f;
		[SerializeField] private float _randomPositionShift = 50.0f; 
		private ParticleSystem.Particle[] _particles;
		private Coroutine _birdLaunches = null;
		private WaitForSeconds _coroutineDellay = new WaitForSeconds(1f);

		private void OnEnable()
		{
			Initialize();
			if (!_birdsEmitter)
			{
				Debug.LogError("No particle system atached to BirdsLauncher");
				return;
			}
			_birdLaunches = StartCoroutine(LaunchPerioud());
		}

		private void OnDestroy()
		{
			if (_birdLaunches != null)
				StopCoroutine(_birdLaunches);
		}

		private IEnumerator LaunchPerioud()
		{
			while (true)
			{
				yield return _coroutineDellay;
				Launch();
			}
		}

		private bool DistanceCheck(Vector3 position)
		{
			int numParticlesAlive = _birdsEmitter.GetParticles(_particles);
			for (int i = 0; i < numParticlesAlive; i++)
			{
				if (Vector3.Distance(_particles[i].position, position) < _minDistance)
				{
					return false;
				}
			}
			return true;
		}

		private void Launch()
		{
			if (!_birdsEmitter)
				return;
			var emitParams = new ParticleSystem.EmitParams();
			emitParams.position = new Vector3(0.0f, UnityEngine.Random.Range(-_randomPositionShift, _randomPositionShift), 0.0f);			
			if (DistanceCheck(emitParams.position)) 
				_birdsEmitter.Emit(emitParams, 1);
		}

		private void Initialize()
		{
			if (_birdsEmitter == null)
				_birdsEmitter = GetComponent<ParticleSystem>();

			if (_particles == null || _particles.Length < _birdsEmitter.main.maxParticles)
				_particles = new ParticleSystem.Particle[_birdsEmitter.main.maxParticles];
		}

	}
}
