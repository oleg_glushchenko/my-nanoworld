﻿using System;
using System.Collections.Generic;
using NanoReality.Game.Tutorial;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace Assets.Scripts.GameLogic.TutorialAndHintsSystem.Common.TutorialLookAtController
{
    public class AnimatedArrow : View, ILookAtPointer
    {
        private readonly Dictionary<PointerMode, Vector3> _arrowAngles = new Dictionary<PointerMode, Vector3>
        {
            {PointerMode.Tap, Vector3.up},
            {PointerMode.TapLeft, Vector3.left},
            {PointerMode.TapRight, Vector3.right},
            {PointerMode.TapBottom, Vector3.down}
        };

        public void ShowPointer(PointerMode mode, Action onComplete = null)
        {
            switch (mode)
            {
                case PointerMode.Tap:
                case PointerMode.TapLeft:
                case PointerMode.TapRight:
                case PointerMode.TapBottom:
                    gameObject.SetActive(true);
                    SetRotation(mode);
                    break;
                default:
                    gameObject.SetActive(false);
                    break;
            }
        }

        private void SetRotation(PointerMode mode)
        {            
            Vector3 direction;
            transform.localRotation = _arrowAngles.TryGetValue(mode, out direction)                
                ? Quaternion.FromToRotation(Vector3.up, direction)
                : Quaternion.identity;
        }
    }
}