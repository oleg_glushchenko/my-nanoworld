﻿using System;
using NanoReality.Game.Tutorial;

namespace Assets.Scripts.GameLogic.TutorialAndHintsSystem.Common.TutorialLookAtController
{
    public interface IPointerTargetAnimator
    {
        void PlayItemAnimation(Action<PointerMode, Action> onComplete, PointerMode pointerMode);

        void StopItemAnimation();
    }
}
