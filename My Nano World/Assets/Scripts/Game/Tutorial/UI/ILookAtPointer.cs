﻿using System;
using NanoReality.Game.Tutorial;

namespace Assets.Scripts.GameLogic.TutorialAndHintsSystem.Common.TutorialLookAtController
{
    public interface ILookAtPointer
    {
        void ShowPointer(PointerMode mode, Action onComplete = null);
    }
}
