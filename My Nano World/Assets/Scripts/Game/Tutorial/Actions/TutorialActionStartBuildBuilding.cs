﻿using System.Collections.Generic;
using NanoReality.GameLogic.BuildingSystem.CityMap.Controllers;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;

namespace NanoReality.Game.Tutorial
{
    public class TutorialActionStartBuildBuilding : TutorialAction
    {
        [Inject]
        public SignalOnBuildStartedVerified jSignalOnBuildStartedVerified { get; set; }

        public readonly List<Id_IMapObjectType> MapObjectTypesNeedToBuild = new List<Id_IMapObjectType>();

        public override bool PreActivateIsCompletedCheck()
        {
            if (base.PreActivateIsCompletedCheck())
                return true;

            foreach (var objectType in MapObjectTypesNeedToBuild)
            {
                var mapObject = jGameManager.FindMapObjectByTypeId<IMapObject>(objectType);
                if (mapObject != null) return true;
            }

            return false;
        }

        public override void Activate()
        {
            base.Activate();

            jSignalOnBuildStartedVerified.AddListener(OnBuildStarted);
        }

        public override void DoCompleteAction(bool isCancel = false)
        {
            jSignalOnBuildStartedVerified.RemoveListener(OnBuildStarted);

            base.DoCompleteAction(isCancel);
        }

        private void OnBuildStarted(IMapObject obj)
        {
            if (MapObjectTypesNeedToBuild.Contains(obj.ObjectTypeID))
            {
                DoCompleteAction();
            }
        }
    }
}
