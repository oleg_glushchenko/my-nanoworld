﻿using Assets.NanoLib.UI.Core.Signals;
using Assets.NanoLib.UI.Core.Views;
using Assets.NanoLib.Utilities;
using Assets.Scripts.Engine.UI.Views.Popups;
using NanoLib.Core.Logging;
using NanoReality.Engine.UI.Views.NotEnoughtResourcesPanel;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Factories;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Production;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;

namespace NanoReality.Game.Tutorial
{
    public sealed class StartIndustryProductionTutorialAction : TutorialAction
    {
        [Inject] public SignalOnShownPanel jSignalOnShownPanel { get; set; }

        public IProduceSlot ProduceSlot => _productionBuilding.CurrentProduceSlots[_targetSlot];

        private Id<Product> _product;
        private int _targetSlot = -1;
        private IProductionBuilding _productionBuilding;

        public void SetMembers(Id<Product> productId, int targetSlot, IProductionBuilding productionBuilding)
        {
            _product = productId;
            _targetSlot = targetSlot;
            _productionBuilding = productionBuilding;

        }

        #region Overrides of TutorialAction

        public override bool PreActivateIsCompletedCheck()
        {
            if (base.PreActivateIsCompletedCheck())
                return true;

            return Check();
        }

        public override void Activate()
        {
            base.Activate();

            jSignalOnShownPanel.AddListener(OnShownPanel);
            _productionBuilding.StartedProduction += OnStarted;
        }

        public override void DoCompleteAction(bool isCancel = false)
        {
            base.DoCompleteAction(isCancel);

            jSignalOnShownPanel.RemoveListener(OnShownPanel);
            _productionBuilding.StartedProduction -= OnStarted;
        }

        #endregion

        private bool Check()
        {
            if (_targetSlot == -1)
                Logging.LogError(LoggingChannel.Tutorial, "Slot not set for this action");
            else if (_product == null)
                Logging.LogError(LoggingChannel.Tutorial, "Product not set for this action");
            else if (_productionBuilding == null)
                Logging.LogError(LoggingChannel.Tutorial, "Production building not set for this action");
            else
            {
                return ProduceSlot.IsFinishedProduce ||
                       ProduceSlot.ItemDataIsProducing != null && ProduceSlot.ItemDataIsProducing.OutcomingProducts == _product;
            }

            return false;
        }

        private void OnShownPanel(UIPanelView panel)
        {
            var notEnoughResourcesPanel = panel as NotEnoughResourcesPopupView;
            var buyPopupView = panel as PurchaseByPremiumPopupView;

            if (notEnoughResourcesPanel != null)
            {
                SwitchHighlight(notEnoughResourcesPanel.BuyButton.gameObject, true);
            }
            else if (buyPopupView != null)
            {
                SwitchHighlight(buyPopupView.YesButton.gameObject, true);
            }
        }

        private void OnStarted(IProduceSlot startedSlot, IProducingItemData producingItemData)
        {
            if (Check())
            {
                DoCompleteAction();
            }
        }
    }
}