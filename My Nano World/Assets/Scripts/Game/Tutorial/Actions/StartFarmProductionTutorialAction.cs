﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.NanoLib.UI.Core.Signals;
using Assets.NanoLib.UI.Core.Views;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using NanoReality.Engine.UI.Extensions.Agricultural;
using NanoReality.Game.Tutorial;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects;
using UnityEngine;

namespace Game.Tutorial.HintTutorial
{
    public class StartFarmProductionTutorialAction : TutorialAction
    {
        private int _targetProductId;
        private IEnumerable<IAgroFieldBuilding> _targetAgrofields;
        private List<IAgroFieldBuilding> _plantedAgrofields;

        private readonly Type _agrofieldPanelType = typeof(AgroFieldPanelView);
        private IAgroFieldBuilding _targetAgrofield;
        
        [Inject] public SignalOnShownPanel jSignalOnShownPanel { get; set; }
        
        public void SetBuildingModel(IEnumerable<IAgroFieldBuilding> model, int targetProductId)
        {
            _targetProductId = targetProductId;
            _targetAgrofields = model;
        }

        #region Overrides of TutorialAction

        public override bool PreActivateIsCompletedCheck()
        {
            if (ParentFunc != null && ParentFunc())
            {
                return true;
            }
            if (_targetAgrofields == null)
            {
                return false;
            }

            return _targetAgrofields.All(c => c.CurrentState != AgriculturalFieldState.Empty);
        }

        public override void Activate()
        {
            base.Activate();
            _plantedAgrofields = new List<IAgroFieldBuilding>();
            foreach (IAgroFieldBuilding agroFieldBuilding in _targetAgrofields.Where(c => c.CurrentState != AgriculturalFieldState.Empty))
            {
                _plantedAgrofields.Add(agroFieldBuilding);
            }
            
            ProcessUi();
        }

        public override void DoCompleteAction(bool isCancel = false)
        {
            base.DoCompleteAction(isCancel);
            jTutorialMapObjects.Hide();
            HideLookers();
        }

        #endregion

        private void ProcessUi()
        {
            if (jUIManager.IsViewVisible(_agrofieldPanelType))
            {
                var view = jUIManager.GetView<AgroFieldPanelView>();
                var veilParams = new VeilParams
                {
                    PointerMode = PointerMode.Drag,
                    DragTargetObject = view.gameObject,
                };

                DraggableProductItem draggableItem = view.DraggableProductItems.First(item => item.ProducingItemData.OutcomingProducts == _targetProductId);
                GameObject draggableParent = draggableItem.transform.parent.gameObject;

                SwitchHighlight(draggableParent, true, veilParams);
                draggableItem.DragEnded += OnItemDragEnded;
                
                view.OnHide.AddListener(OnAgrofieldPanelHide);
                PlantNextAgrofield();
            }
            else
            {
                IAgroFieldBuilding targetAgrofield = GetNextAgrofield();;
                if (targetAgrofield == null)
                {
                    return;
                }
                MapObjectView targetAgrofieldView = jGameManager.GetMapObjectView(targetAgrofield);
                SwitchHighlight(targetAgrofieldView.gameObject, true);

                SetAllBuildingsInteractable(false);
                targetAgrofieldView.Interactable = true;
                
                jSignalOnShownPanel.AddListener(OnPanelShown);
            }
        }
        
        private void PlantNextAgrofield()
        {
            var notPlantedAgrofield = GetNextAgrofield();
            if (notPlantedAgrofield == null)
            {
                var view = jUIManager.GetView<AgroFieldPanelView>();
                DraggableProductItem draggableItem =
                    view.DraggableProductItems.First(item => item.ProducingItemData.OutcomingProducts == _targetProductId);
                draggableItem.DragEnded -= OnItemDragEnded;

                DoCompleteAction();
                return;
            }

            jTutorialMapObjects.ShowBuildPlace(BusinessSectorsTypes.Farm, notPlantedAgrofield.MapPosition.ToVector2(),
                notPlantedAgrofield.Dimensions.ToVector2());
            
            notPlantedAgrofield.OnGrowingStart += OnGrowingStart;
        }

        private void OnPanelShown(UIPanelView targetPanel)
        {
            if (targetPanel.GetType() == typeof(AgroFieldPanelView))
            {
                ProcessUi();
                jSignalOnShownPanel.RemoveListener(OnPanelShown);
            }
        }

        private void OnAgrofieldPanelHide(UIPanelView agrofieldPanel)
        {
            agrofieldPanel.OnHide.RemoveListener(OnAgrofieldPanelHide);
            ProcessUi();
        }

        private void OnGrowingStart(IAgroFieldBuilding agrofield)
        {
            agrofield.OnGrowingStart -= OnGrowingStart;
            _plantedAgrofields.Add(agrofield);
            HideLookers();
            PlantNextAgrofield();
        }

        private void OnItemDragEnded(DraggableItem obj)
        {
            obj.DragEnded -= OnItemDragEnded;
            if (_targetAgrofields.Count() == _plantedAgrofields.Count)
            {
                DoCompleteAction();
            }
        }

        private IAgroFieldBuilding GetNextAgrofield()
        {
            return _targetAgrofields.FirstOrDefault(c => !_plantedAgrofields.Contains(c));
        }
    }
}