﻿using System;
using NanoLib.Services.InputService;
using Assets.NanoLib.UI.Core.Signals;
using Assets.NanoLib.UI.Core.Views;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using NanoReality.Game.UI;
using UnityEngine.UI;

namespace NanoReality.Game.Tutorial
{
    public class TutorialActionShowPanel : TutorialAction
    {
        #region Fields

        public Type TargetPanelType;

        #endregion

        #region Injections
        
        [Inject]
        public SignalOnMapObjectViewTap jSignalOnMapObjectViewTap { get; set; }

        [Inject]
        public SignalOnShownPanel jSignalOnShownPanel { get; set; }

        #endregion

        private Button _button;

        #region Methods

        public override bool PreActivateIsCompletedCheck()
        {
            if (base.PreActivateIsCompletedCheck()) return true;

            return jUIManager.IsViewVisible(TargetPanelType);
        }

        public override void Activate()
        {
            base.Activate();

            if (ObjectToHighlight != null)
            {
                _button = ObjectToHighlight.GetComponent<Button>();
                if (_button != null)
                {
                    _button.onClick.AddListener(OnButtonClick);
                }
            }

            jSignalOnMapObjectViewTap.AddListener(OnMapObjectViewTap);
            jSignalOnShownPanel.AddListener(OnShownPanel);
        }

        public override void Deactivate()
        {
            base.Deactivate();
            
            jSignalOnMapObjectViewTap.RemoveListener(OnMapObjectViewTap);
            jSignalOnShownPanel.RemoveListener(OnShownPanel);
            
            if (_button != null)
            {
                _button.onClick.RemoveListener(OnButtonClick);
            }
        }

        private void OnButtonClick()
        {
            HideLookers();

            jHideWindowSignal.Dispatch(typeof(DialogPanelView));
        }

        private void OnMapObjectViewTap(MapObjectView mapObjectView)
        {
            if (MapObjectView != null && mapObjectView == MapObjectView && IsDialogEnabled)
            {
                HideLookers();

                jHideWindowSignal.Dispatch(typeof(DialogPanelView));
            }
        }

        private void OnShownPanel(UIPanelView obj)
        {
            if (obj.GetType() == TargetPanelType)
                DoCompleteAction();
        }

        #endregion
    }
}