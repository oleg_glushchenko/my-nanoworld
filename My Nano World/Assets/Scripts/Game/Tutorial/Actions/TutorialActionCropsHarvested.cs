﻿using NanoReality.GameLogic.BuildingSystem.MapObjects;

namespace NanoReality.Game.Tutorial
{
    public class TutorialActionCropsHarvested: TutorialAction
    {
        private IAgroFieldBuilding _building;
        
        public void SetBuildingModel(IAgroFieldBuilding model)
        {
            _building = model;
        }

        #region Overrides of TutorialAction

        public override bool PreActivateIsCompletedCheck()
        {
            if (ParentFunc != null && ParentFunc())
                return true;

            return _building.CurrentState == AgriculturalFieldState.Empty;
        }

        public override void Activate()
        {
            base.Activate();
            
            _building.OnHarvestStarted.AddListener(OnHarvestStarted);
            _building.OnHarvestEndedVerified.AddListener(OnHarvestEndedVerified);
        }

        public override void DoCompleteAction(bool isCancel = false)
        {
            base.DoCompleteAction(isCancel);
            
            _building.OnHarvestStarted.RemoveListener(OnHarvestStarted);
            _building.OnHarvestEndedVerified.RemoveListener(OnHarvestEndedVerified);
        }

        #endregion

        private void OnHarvestStarted()
        {
            HideLookers();
        }

        private void OnHarvestEndedVerified(bool isVerified)
        {
            if (isVerified)
            {
                DoCompleteAction();
            }
        }
    }
}
