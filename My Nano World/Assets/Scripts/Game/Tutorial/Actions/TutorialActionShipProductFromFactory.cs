﻿using System.Linq;
using Assets.NanoLib.Utilities;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Controllers.Agricultural;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Production;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;

// ReSharper disable once CheckNamespace
namespace NanoReality.Game.Tutorial
{
    public class TutorialActionShipProductFromFactory : TutorialAction
    {
        public Id<Product> Product = null;

        public Id_IMapObject BuildingWithReadyProducts = null;

        [Inject]
        public OnProductProduceAndCollectSignal jOnProductProduceAndCollectSignal { get; set; }

        public override bool PreActivateIsCompletedCheck()
        {
            // если есть родительская функция, и она выполнена, игнорируя условия
            // автоматически засчитываем это действие как выполненое
            if (ParentFunc != null && ParentFunc())
                return true;

            var productionBuilding = jGameManager.CurrentUserCity.GetMapObjectInCityByID(BuildingWithReadyProducts) as
                IProductionBuilding;
            if (productionBuilding != null)
            {
                if (Product == null)
                {
                    if (productionBuilding.CurrentProduceSlots.Where(s => s.IsFinishedProduce).ToList().Count == 0)
                        return true;
                }
                else
                {
                    // если такого продукта совсем нет в слотах, значит скорей всего он уже собран
                    if (productionBuilding.CurrentProduceSlots.All(slot =>
                        slot.ItemDataIsProducing == null || slot.ItemDataIsProducing.OutcomingProducts != Product))
                        return true;
                }
            }

            return false;
        }

        public override void Activate()
        {
            base.Activate();

            jOnProductProduceAndCollectSignal.RemoveListener(OnShip);
            jOnProductProduceAndCollectSignal.AddListener(OnShip);
        }

        private void OnShip(IMapObject mapObject, Id<Product> arg2, int arg3)
        {
            if (BuildingWithReadyProducts == null)
            {
                jOnProductProduceAndCollectSignal.RemoveListener(OnShip);
                DoCompleteAction();
            }
            else
            {
                if (mapObject.MapID == BuildingWithReadyProducts)
                {
                    var productionMapObject = mapObject as IProductionBuilding;
                    if (productionMapObject != null)
                    {
                        if (Product == null)
                        {
                            if (productionMapObject.CurrentProduceSlots.Where(s => s.IsFinishedProduce).ToList()
                                    .Count == 0)
                            {
                                jOnProductProduceAndCollectSignal.RemoveListener(OnShip);
                                DoCompleteAction();
                            }
                        }
                        else
                        {
                            if (productionMapObject.CurrentProduceSlots.All(slot =>
                                slot.ItemDataIsProducing == null ||
                                slot.ItemDataIsProducing.OutcomingProducts != Product))
                            {
                                jOnProductProduceAndCollectSignal.RemoveListener(OnShip);
                                DoCompleteAction();
                            }
                        }
                    }
                }
            }
        }
    }
}