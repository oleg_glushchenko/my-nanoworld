﻿using Assets.NanoLib.UI.Core.Signals;
using Assets.NanoLib.UI.Core.Views;
using Assets.Scripts.Engine.UI.Views.Popups;
using NanoReality.Engine.UI.Extensions.Agricultural;
using NanoReality.GameLogic.BuildingSystem.MapObjects;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Factories;
namespace NanoReality.Game.Tutorial
{
    public class TutorialActionCropsReadyToHarvest: TutorialAction
    {
        [Inject] public SignalOnShownPanel jSignalOnShownPanel { get; set; }
        [Inject] public PopupOpenedSignal jPopupOpenedSignal { get; set; }
        
        private IAgroFieldBuilding _building;
        private AgroFieldPanelView _view;
        private PurchaseByPremiumPopupView _purchasePopup;
        
        public void SetBuildingModel(IAgroFieldBuilding model)
        {
            _building = model;
        }

        public override bool PreActivateIsCompletedCheck()
        {
            if (ParentFunc != null && ParentFunc())
                return true;

            return _building.CurrentState == AgriculturalFieldState.Completed;
        }

        public override void Activate()
        {
            base.Activate();

            jSignalOnShownPanel.AddListener(OnShownPanel);
            jPopupOpenedSignal.AddListener(OnShownPopup);
            _building.OnFinishGrowing.AddListener(OnFinishGrowing);
            _building.OnFinishGrowingVerified.AddListener(OnFinishGrowingVerified);
        }

        public override void DoCompleteAction(bool isCancel = false)
        {
            base.DoCompleteAction(isCancel);
            jSignalOnShownPanel.RemoveListener(OnShownPanel);
            jPopupOpenedSignal.RemoveListener(OnShownPopup);
            _building.OnFinishGrowing.RemoveListener(OnFinishGrowing);
            _building.OnFinishGrowingVerified.RemoveListener(OnFinishGrowingVerified);
        }

        private void OnShownPanel(UIPanelView panel)
        {
            UIPanelView tempView  = panel as AgroFieldPanelView;

            if (tempView == null) return;

            _view = (AgroFieldPanelView) tempView;
            
            SwitchHighlight(_view.SkipButton.gameObject, true, new VeilParams
            {
                PointerMode = PointerMode.Tap
            });
        }
        
        private void OnShownPopup(BasePopupView popupView)
        {
            if (popupView.GetType() == typeof(PurchaseByPremiumPopupView))
            {
                _purchasePopup = (PurchaseByPremiumPopupView) popupView;
                _purchasePopup.YesButton.onClick.Invoke(); ;
                 _view.СropPanel.SetActive(false);
            }
        }

        private void OnFinishGrowing(IProducingItemData obj)
        {
            jUIManager.HideAll();
            HideLookers();
            _purchasePopup = null;
        }

        private void OnFinishGrowingVerified(IProducingItemData itemData)
        {
            DoCompleteAction();
        }
    }
}
