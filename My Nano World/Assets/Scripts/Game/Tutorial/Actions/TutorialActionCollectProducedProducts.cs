﻿using System.Linq;
using Assets.NanoLib.Utilities;
using Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Controllers;
using NanoReality.Engine.BuildingSystem.MapObjects;
using NanoReality.Engine.UI.Views.Panels;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Controllers.Agricultural;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Production;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using UnityEngine;

namespace NanoReality.Game.Tutorial
{
    public sealed class TutorialActionCollectProducedProducts : TutorialAction
    {
        [Inject] public OnProductProduceAndCollectSignal jOnProductProduceAndCollectSignal { get; set; }
        [Inject] public SignalOnMineProduceSlotFinished jSignalOnMineProduceSlotFinished { get; set; }

        private IProductionBuilding _building;
        private VeilParams _veilParams;

        public void Set(IProductionBuilding building)
        {
            _building = building;
        }

        public override bool PreActivateIsCompletedCheck()
        {
            if (base.PreActivateIsCompletedCheck()) return true;

            return !HasProductsToCollect();
        }

        public override void Activate()
        {
            base.Activate();
            
            _veilParams = new VeilParams(0, 50, 0, 50);
            
            HighlightProduct();
            
            jOnProductProduceAndCollectSignal.AddListener(OnProductProduceAndCollect);
            jSignalOnMineProduceSlotFinished.AddListener(OnProduceProduced);
        }

        public override void DoCompleteAction(bool isCancel = false)
        {
            base.DoCompleteAction(isCancel);
            
            jOnProductProduceAndCollectSignal.RemoveListener(OnProductProduceAndCollect);
            jSignalOnMineProduceSlotFinished.RemoveListener(OnProduceProduced);
        }

        private bool HasProductsToCollect()
        {
            return _building != null && (_building.HasProductsToCollect() || !_building.IsProductionEmpty());
        }

        private void OnProductProduceAndCollect(IMapObject mapObject, Id<Product> product, int amount)
        {
            if (mapObject == _building && !HasProductsToCollect())
            {
                DoCompleteAction();
            }
            else
            {
                HighlightProduct();
            }
        }
        
        private void OnProduceProduced(Id_IMapObject productionBuildingId)
        {
            if (_building.MapID == productionBuildingId)
            {
                HighlightProduct();
            }
        }

        private void HighlightProduct()
        {
            var product = FindProduct();
            if (product != null)
            {
                SwitchHighlight(product, true, _veilParams);
            }
            else
            {
                HideLookers();
            }
        }
        
        private GameObject FindProduct()
        {
            ProductionMapObjectView productionMapObjectView = (ProductionMapObjectView)jGameManager.GetMapObjectView(_building);
            var readyViews = productionMapObjectView.ReadyProductsComponent.ReadyViews;

            if (readyViews.Count > 0)
            {
                ReadyProductView readyItemsArray = productionMapObjectView.ReadyProductsComponent.ReadyViews.FirstOrDefault().Value;
                return readyItemsArray.gameObject;
            }

            return null;
        }
    }
}