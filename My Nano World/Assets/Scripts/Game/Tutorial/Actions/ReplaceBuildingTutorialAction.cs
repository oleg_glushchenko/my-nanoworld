using NanoLib.Services.InputService;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using NanoReality.Engine.BuildingSystem.MapObjects;
using NanoReality.Engine.UI.Views.ConstractConfirmPanel;
using NanoReality.Game.UI;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using NanoReality.GameLogic.Services;
using UnityEngine;

namespace NanoReality.Game.Tutorial
{
    public sealed class ReplaceBuildingTutorialAction : TutorialAction
    {
        private ConstractConfirmPanelView ConstructConfirmPanel => jUIManager.GetView<ConstractConfirmPanelView>();

        private Id_IMapObject _buildingId;
        private Vector2 _replacePosition;
        private Rect? _dragLimits;

        private bool _isSelectedValidBuilding;
        private bool _isValidPosition;
        
        [Inject] public ICitySectorService JCitySectorService { get; set; }
        [Inject] public IConstructionController jConstructionController { get; set; }
        [Inject] public SignalOnMapObjectChangedSelect jSignalMapObjectChangeSelect { get; set; }
        [Inject] public SignalOnMapObjectViewMoved jMapObjectMovedSignal { get; set; }
        [Inject] public SignalOnConfirmConstructSucceed jConstructConfirmSignal { get; set; }

        public void Initialize(Id_IMapObject buildingId, Vector2 replacePosition, BusinessSectorsTypes sector, Rect? dragLimits = null)
        {
            _buildingId = buildingId;
            _replacePosition = replacePosition;
            _dragLimits = dragLimits;
            SectorID = sector;
        }
        
        public override void Activate()
        {
            base.Activate();
            
            JCitySectorService.SetDragLimits(_dragLimits);
            SetInputState(InputActionType.LongTap);
            ApplyInputState();
            
            jMapObjectMovedSignal.AddListener(OnMapObjectMoved);
            jSignalMapObjectChangeSelect.AddListener(OnMapObjectSelected);
            jConstructConfirmSignal.AddListener(OnConstructConfirmed);
            jSignalOnReleaseTouch.AddListener(OnReleaseTouch);
        }

        public override void Deactivate()
        {
            base.Deactivate();
            
            JCitySectorService.SetDragLimits(null);
            
            jMapObjectMovedSignal.RemoveListener(OnMapObjectMoved);
            jSignalMapObjectChangeSelect.RemoveListener(OnMapObjectSelected);
            jConstructConfirmSignal.RemoveListener(OnConstructConfirmed);
        }
        
        private void OnMapObjectMoved(MapObjectView mapObjectView)
        {
            _isValidPosition = mapObjectView.GridPosition == _replacePosition;
            if (!_isSelectedValidBuilding || !_isValidPosition)
            {
                HideLookers();
            }
        }
        
        private void OnMapObjectSelected(MapObjectView mapObjectView, bool isSelected)
        {
            _isSelectedValidBuilding = mapObjectView.MapId == _buildingId && isSelected;

            if (!_isSelectedValidBuilding)
                return;
            
            MoveCameraToTarget();
            HideLookers();
            SetInputState(InputActionType.Drag);
            ApplyInputState();

            jConstructConfirmPanelModel.isCancelButtonEnabled.Value = false;
            jTutorialMapObjects.ShowBuildPlace(SectorID, _replacePosition, new Vector2(mapObjectView.MapObjectModel.Dimensions.X, mapObjectView.MapObjectModel.Dimensions.Y));
        }
        private void OnConstructConfirmed()
        {
            jHideWindowSignal.Dispatch(typeof(DialogPanelView));
            HideLookers();
            DoCompleteAction();
        }
        
        private void OnReleaseTouch()
        {
            if (!_isValidPosition)
                return;
            
            SwitchHighlight(ConstructConfirmPanel.ConfirmButton.gameObject, true);
            jSignalOnReleaseTouch.RemoveListener(OnReleaseTouch);
        }

    }
}