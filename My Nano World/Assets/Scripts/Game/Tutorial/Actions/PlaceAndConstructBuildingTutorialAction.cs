using System;
using Assets.NanoLib.Utilities;
using NanoReality.Engine.UI.Views.ConstractConfirmPanel;
using NanoReality.Game.UI;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityMap.Controllers;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.Services;
using UnityEngine;

namespace NanoReality.Game.Tutorial
{
    public sealed class PlaceAndConstructBuildingTutorialAction : TutorialAction
    {
        public Rect? DragLimits;

        [Inject] public ICitySectorService JCitySectorService { get; set; }
        [Inject] public IConstructionController jConstructionController { get; set; }
        [Inject] public SignalMoveMapObjectView jSignalMoveMapObjectView { get; set; }   
        [Inject] public SignalOnConfirmConstructSucceed jConstructConfirmSignal { get; set; }

        public bool IsCancelButtonEnabled
        {
            set => _isCancelButtonEnabled = value;
        }

        private Id<IBusinessSector> _sectorType;
        private int _buildingTypeId;
        private Vector2 _position;

        private bool NeedCheckBuildingPosition => _position != default;
        
        private bool _isValidPosition;
        private bool _isCancelButtonEnabled;

        private ConstractConfirmPanelView ConstructConfirmPanel => jUIManager.GetView<ConstractConfirmPanelView>();

        public PlaceAndConstructBuildingTutorialAction Set(BusinessSectorsTypes sectorType, int buildingTypeId, Vector2 mapPosition = default(Vector2))
        {
            _sectorType = sectorType.GetBusinessSectorId();
            _buildingTypeId = buildingTypeId;
            _position = mapPosition;

            _isCancelButtonEnabled = false;

            return this;
        }

        public override void Activate()
        {
            base.Activate();
            
            HideLookers();
            JCitySectorService.SetDragLimits(DragLimits);
            jConstructConfirmPanelModel.isCancelButtonEnabled.Value = _isCancelButtonEnabled;

            jSignalMoveMapObjectView.AddListener(OnMapObjectMove);
            jConstructConfirmSignal.AddListener(OnConstructConfirmed);
            jSignalOnReleaseTouch.AddListener(OnReleaseTouch);
        }
        
        public override void Deactivate()
        {
            base.Deactivate();
            
            JCitySectorService.SetDragLimits(null);
            
            jSignalMoveMapObjectView.RemoveListener(OnMapObjectMove);
            jConstructConfirmSignal.RemoveListener(OnConstructConfirmed);
            jSignalOnReleaseTouch.RemoveListener(OnReleaseTouch);
        }

        private void OnReleaseTouch()
        {
            if (!_isValidPosition || !NeedCheckBuildingPosition)
                return;
            
            SwitchHighlight(ConstructConfirmPanel.ConfirmButton.gameObject, true);
            jSignalOnReleaseTouch.RemoveListener(OnReleaseTouch);
        }

        private void OnMapObjectMove(Vector2Int newPos, IMapObject mapObjectModel, Action callback = null)
        {
            HideLookers();
            _isValidPosition = CheckValidPosition(newPos, mapObjectModel);
        }

        private bool CheckValidPosition(Vector2Int newPos, IMapObject mapObjectModel)
        {
            if (mapObjectModel == null)
                return false;

            if (mapObjectModel.SectorId != _sectorType || mapObjectModel.ObjectTypeID != _buildingTypeId)
                return false;
            
            if (NeedCheckBuildingPosition && newPos != _position)
                return false;

            return jConstructionController.IsConstructionAvailable;
        }

        private void OnConstructConfirmed()
        {
            if (!_isValidPosition) return;
            
            jHideWindowSignal.Dispatch(typeof(DialogPanelView));
            DoCompleteAction();
        }
    }
}