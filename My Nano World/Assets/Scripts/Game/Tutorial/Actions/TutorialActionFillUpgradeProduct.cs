﻿using NanoReality.GameLogic.Upgrade;

namespace NanoReality.Game.Tutorial
{
    public sealed class TutorialActionFillUpgradeProduct : TutorialAction
    {
        [Inject] public SignalOnUpgradeProductPushed jSignalOnUpgradeProductPushed { get; set; }

        public override void Activate()
        {
            base.Activate();

            jSignalOnUpgradeProductPushed.AddOnce(OnUpgradeProductPushed);
        }

        private void OnUpgradeProductPushed()
        {
            DoCompleteAction();
        }
    }
}