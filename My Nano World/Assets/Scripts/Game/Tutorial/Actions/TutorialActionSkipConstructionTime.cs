﻿using NanoLib.Services.InputService;
using Assets.NanoLib.UI.Core.Signals;
using Assets.NanoLib.UI.Core.Views;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using Assets.Scripts.Engine.UI.Views.Popups;
using NanoLib.Core.Logging;
using NanoReality.Game.UI;
using NanoReality.GameLogic.BuildingSystem.CityMap.Controllers;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;

namespace NanoReality.Game.Tutorial
{
    public class TutorialActionSkipConstructionTime : TutorialAction
    {
        [Inject] public SignalOnMapObjectViewTap jSignalOnMapObjectViewTap { get; set; }
        [Inject] public SignalOnFastFinishButtonActivated jSignalOnFastFinishButtonActivated { get; set; }
        [Inject] public SignalOnShownPanel jSignalOnShownPanel { get; set; }
        [Inject] public SignalOnMapObjectBuildFinished jSignalOnMapObjectBuildFinished { get; set; }
        [Inject] public PopupOpenedSignal jPopupOpenedSignal { get; set; }
  
        private int _buildingTypeId;
        private bool _needArrow;
        private VeilParams _veilParams;
        private IMapBuilding _model;
        private MapObjectView _mapObjectView;
        
        #region Overrides of TutorialAction

        public override bool PreActivateIsCompletedCheck()
        {
            if (base.PreActivateIsCompletedCheck()) return true;

            _model = jGameManager.FindMapObjectByTypeId<IMapBuilding>(_buildingTypeId);

            if (_model == null)
            {
                Logging.LogWarning(LoggingChannel.Tutorial, $"There is no map object model with building type id: {_buildingTypeId}");

                return true;
            }

            if (_model.IsConstructed)
            {
                return true;
            }
            
            _mapObjectView = jGameManager.GetMapObjectView(_model);

            if (_mapObjectView == null)
            {
                Logging.LogWarning(LoggingChannel.Tutorial, $"There is not map object view with building type id: {_buildingTypeId}");

                return true;
            }

            return false;
        }

        public override void Activate()
        {
            SetCamera(_mapObjectView.MapObjectModel);

            InteractableBuilding = _mapObjectView;
            
            base.Activate();
            
            _veilParams = new VeilParams
            {
                PointerMode = _needArrow ? PointerMode.Tap : PointerMode.Invisible
            };

            SwitchHighlight(_mapObjectView.gameObject, true, _veilParams);

            jSignalOnMapObjectViewTap.AddListener(OnMapObjectViewTap);
            jSignalOnFastFinishButtonActivated.AddListener(OnFastFinishButtonActivated);
            jSignalOnShownPanel.AddListener(OnPopupOpened);
            jSignalOnMapObjectBuildFinished.AddListener(OnMapObjectBuildFinished);
        }

        public override void Deactivate()
        {
            base.Deactivate();
            jSignalOnMapObjectViewTap.RemoveListener(OnMapObjectViewTap);
            jSignalOnFastFinishButtonActivated.RemoveListener(OnFastFinishButtonActivated);
            jSignalOnShownPanel.RemoveListener(OnPopupOpened);
            jSignalOnMapObjectBuildFinished.RemoveListener(OnMapObjectBuildFinished);
            jPopupOpenedSignal.RemoveListener(OnPopupOpened);
        }

        #endregion

        public void Init(int buildingTypeId, bool needArrow = false)
        {
            _buildingTypeId = buildingTypeId;
            _needArrow = needArrow;
            
            Init();

            IsBuildingsNotInteractable = true;
        }

        private void OnMapObjectViewTap(MapObjectView mapObjectView)
        {
            if (mapObjectView != _mapObjectView)
                return;

            jHideWindowSignal.Dispatch(typeof(DialogPanelView));
            jPopupOpenedSignal.AddListener(OnPopupOpened);
            mapObjectView.Interactable = false;
            
            InputState.CurrentState = InputActionType.None;
            ApplyInputState();
        }

        private void OnFastFinishButtonActivated(MapObjectView mapObjectView)
        {
            if (mapObjectView == _mapObjectView)
            {
                SwitchHighlight(mapObjectView.ProgressBarComponent.ConstructionProgressBar.SpeedUpButton.gameObject, true, _veilParams);
            }
        }

        private void OnPopupOpened(UIPanelView view)
        {
            var panelView = view as PurchaseByPremiumPopupView;
            if (panelView != null)
            {
                panelView.YesButton.onClick.Invoke();
                jPopupOpenedSignal.RemoveListener(OnPopupOpened);
                //SwitchHighlight(panelView.YesButton.gameObject, true, _veilParams);
            }
        }

        private void OnMapObjectBuildFinished(IMapObject mapObject)
        {
            if (mapObject == _model)
            {
                DoCompleteAction();
            }
        }
    }
}
