﻿using System;
using Assets.NanoLib.Utilities;
using NanoReality.GameLogic.BuildingSystem.CityMap.Controllers;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using UnityEngine;


namespace NanoReality.Game.Tutorial
{
	public class TutorialActionPlaceBuilding : TutorialAction
	{
		[Inject]
		public IConstructionController jConstructionController { get; set; }

		[Inject]
		public SignalMoveMapObjectView jSignalMoveMapObjectView { get; set; }


        public override void Activate()
        {
            base.Activate();

            jSignalMoveMapObjectView.RemoveListener(OnMapObjectMove);
			jSignalMoveMapObjectView.AddListener(OnMapObjectMove);
		}
		
		public override void DoCompleteAction(bool isCancel = false)
		{
			jSignalMoveMapObjectView.RemoveListener(OnMapObjectMove);

			base.DoCompleteAction(isCancel);
		}

		public override bool PreActivateIsCompletedCheck()
		{
			// если есть родительская функция, и она выполнена, игнорируя условия
			// автоматически засчитываем это действие как выполненое
			if (ParentFunc != null && ParentFunc())
				return true;

			return jConstructionController.IsConstructionAvailable;
		}

		private void OnMapObjectMove(Vector2Int newPos, IMapObject obj, Action callback = null)
		{
			if (jConstructionController.IsConstructionAvailable)
			{
				DoCompleteAction();
			}
		}
	}
}