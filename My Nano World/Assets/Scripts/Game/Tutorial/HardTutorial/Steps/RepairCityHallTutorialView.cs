using System;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using Engine.UI.Extensions.RepairBuildingPanel;
using I2.Loc;
using NanoLib.Services.InputService;
using NanoReality.Game.UI;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;

namespace NanoReality.Game.Tutorial
{
    [TutorialStep(StepType.RepairCityHall)]
    public sealed class RepairCityHallTutorialView : HardTutorialView
    {
        private RepairBuildingPanel RepairPanel => jUIManager.GetView<RepairBuildingPanel>();

        protected override void AddTutorialActions()
        {
            base.AddTutorialActions();
            
            var cityHall = (IMapBuilding) jGameManager.FindMapObjectOnMapByType(BusinessSectorsTypes.Global, MapObjectintTypes.CityHall);
            if (IsBuildingAlreadyRepaired(cityHall))
            {
                return;
            }

            AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_DIALOG_ASSISTANT_CHECK_TAXES, DialogCharacter.Girl);

            TutorialAction showRepairPanelAction = AddOpenMapObjectPanelAction(FindCityHall(), typeof(RepairBuildingPanel))
                .SetCamera(cityHall)
                .SetInputState(InputActionType.Tap);

            showRepairPanelAction.OnComplete += () =>
            {
                AddPressButtonAction(RepairPanel.RepairButton).OnComplete += () =>
                {
                    TutorialActionShowPanel waitPopupOpenTutorialAction = AddWaitForPanelShowAction(typeof(FxPopUpPanelView));
                    waitPopupOpenTutorialAction.OnComplete += () =>
                    {
                        AddWaitForPanelCloseAction(typeof(FxPopUpPanelView));
                    };
                };
            };
        }

        private static bool IsBuildingAlreadyRepaired(IMapBuilding cityHall) => cityHall != null && !cityHall.IsLocked;

        private MapObjectView FindCityHall() => FindMapObjectViewOnMapByType(BusinessSectorsTypes.Global, MapObjectintTypes.CityHall);
    }
}