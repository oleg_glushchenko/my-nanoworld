﻿using System;
using Assets.Scripts.Engine.UI.Views.Hud;
using Assets.Scripts.GameLogic.Utilites;
using NanoReality.Engine.UI.Views.Hud;
using NanoReality.Game.UI;
using NanoReality.Game.UI.BuildingsShopPanel;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.MinePanel;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Game.Tutorial
{
    [TutorialStep(StepType.BuildSmelter)]
    public sealed class BuildSmelterTutorialView : HardTutorialView
    {
        private readonly int SmelterBuildingId = 97;
        private readonly Vector2 SmelterCameraPosition = new Vector2(6, 15);
        private readonly Vector2 SmelterBuildingPosition = new Vector2(6, 15);
        private readonly BusinessSectorsTypes SectorType = BusinessSectorsTypes.HeavyIndustry;
        private readonly BuildingsCategories BuildingCategory = BuildingsCategories.Metal;
        private readonly BuildingSubCategories SubCategory = BuildingSubCategories.Common;

        protected override void AddTutorialActions()
        {
            base.AddTutorialActions();
            
            if (HasBuildingOnMap(SectorType, SmelterBuildingId, 0, SmelterBuildingPosition))
            {
                return;
            }

            var hudView = jUIManager.GetView<HudView>();


            HudBuildingsPanelView hudBuildingsPanel = hudView.HudBuildingsPanel;
            Button industrialButton = hudBuildingsPanel.IndustryBuildingsButton;
            Action<bool> industrialAnimation = hudBuildingsPanel.DoIndustryAnimation;

            BuildingsShopPanelMediator.CategoryData = new BuildingShopData(BusinessSectorsTypes.HeavyIndustry, BuildingsCategories.Factories, BuildingSubCategories.Common);
            TutorialActionShowPanel pressBuildingsShopAction = AddPressBuildingsShopAction(industrialButton,industrialAnimation);

            pressBuildingsShopAction.SetCameraInSector(SmelterCameraPosition.ToVector2F(), SectorType);

            pressBuildingsShopAction.SetDialog(
                new DialogData(LocalizationKeyConstants.TUTORIAL_REPLIC_CONSTRUCT_SMELTER, DialogCharacter.Engineer));
            pressBuildingsShopAction.OnActivate += () =>
            {
                jHideWindowSignal.Dispatch(typeof(MinePanelView));
            };
            pressBuildingsShopAction.OnComplete += () =>
            {
                BuildingsShopPanelMediator.CategoryData = null;
            };

            var buildAction = AddBuildBuildingActions(SectorType, SmelterBuildingId, BuildingCategory, SubCategory, SmelterBuildingPosition);
            buildAction.OnComplete += () => { AddWaitForSecondsAction(2.0f); };
        }
        
        public override bool IsAllowedBuildPosition(Vector2 gridPosition)
        {
            return gridPosition.Equals(SmelterBuildingPosition);
        }
    }
}
