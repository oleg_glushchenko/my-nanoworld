﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using I2.Loc;
using NanoLib.Services.InputService;
using NanoReality.Game.UI;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Factories;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Production;
using NanoReality.GameLogic.Configs;
using NanoReality.GameLogic.MinePanel;

namespace NanoReality.Game.Tutorial
{
    [TutorialStep(StepType.AddMineProductionSlot)]
    public sealed class AddMineProductionSlotTutorialView : HardTutorialView
    {
        private MinePanelView MinePanelView => jUIManager.GetView<MinePanelView>();

        protected override void AddTutorialActions()
        {
            if (!jUIManager.IsViewVisible(typeof(MinePanelView)))
            {
                base.AddTutorialActions();
            }
            
            BusinessSectorsTypes businessSectorsType = ActionData[0].BusinessSectorsType;
            
            MapObjectView mine = FindMapObjectViewOnMapByType(businessSectorsType, MapObjectintTypes.Mine, 0);
            if (mine == null)
            {
                jDebug.LogError($"Mine not found. Tutorial Step: {GetType().Name} skipped.");
                return;
            }

            var mineModel = (IProductionBuilding)mine.MapObjectModel;
            
            IProduceSlot produceSlot = mineModel.CurrentProduceSlots.First(c => !c.IsUnlocked);
            produceSlot.UnlockPremiumPrice = 0;


            AddWaitForSecondsAction(0.5f).SetCamera(mineModel);
            AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_DIALOG_ENGENEER_HELLO, DialogCharacter.Engineer);
            
            TutorialActionShowPanel actionOpenMinePanel = AddOpenMapObjectPanelAction(mine, typeof(MinePanelView));
            actionOpenMinePanel.SetInputState(InputActionType.Tap);
            actionOpenMinePanel.SetCamera(mineModel);
            actionOpenMinePanel.OnComplete += () => MinePanelView.AddNewSlotButton.interactable = false;

            AddWaitForSecondsAction(1.5f);
            
            AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_DIALOG_MINE_ADD_SLOT, DialogCharacter.Engineer).OnComplete += () =>
                AddPressButtonAction(MinePanelView.BuyNewSlotButton).OnComplete += () =>
                    {
                        MinePanelView.BuyNewSlotButton.interactable = false;
                        MinePanelView.AddNewSlotButton.interactable = false;
                    };
        }
        
        protected override List<TutorialActionData> GetLocalActionData()
        {
            List<TutorialActionData> localData = new List<TutorialActionData>();

            TutorialActionData buildAction = new TutorialActionData();
            buildAction.BusinessSectorsType = BusinessSectorsTypes.HeavyIndustry;
            buildAction.BuildingData = new List<TutorialBuildingData>()
            {
                new TutorialBuildingData(0, 0, MapObjectintTypes.Mine)
            };

            localData.Add(buildAction);

            return localData;
        }
    }
}
