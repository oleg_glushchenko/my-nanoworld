﻿using System.Collections.Generic;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using GameLogic.Taxes.Service;
using I2.Loc;
using NanoLib.Services.InputService;
using NanoReality.Engine.UI.Extensions.MapObjects.CityHallTaxes;
using NanoReality.Game.UI;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.Configs;

namespace NanoReality.Game.Tutorial
{
    [TutorialStep(StepType.CollectTaxes)]
    public class CollectTaxesTutorialView : HardTutorialView
    {
        [Inject] public ICityTaxesService jCityTaxesService { get; set; }

        protected override void AddTutorialActions()
        {
            base.AddTutorialActions();

            BusinessSectorsTypes buildingSector = ActionData[0].BusinessSectorsType;
            MapObjectintTypes buildingType = ActionData[0].BuildingData[0].MapObjectType;
            
            MapObjectView cityHallView = FindMapObjectViewOnMapByType(buildingSector, buildingType);

            AddWaitForSecondsAction(1f).SetCamera(cityHallView.MapObjectModel);

            AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_DIALOG_ASSISTANT_HAPPY_TAXES, DialogCharacter.Girl,
                DialogPosition.Left, CharacterExpression.Idle);
            AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_DIALOG_ASSISTANT_CHECK_TAXES, DialogCharacter.Girl,
                DialogPosition.Left, CharacterExpression.Idle);

            AddOpenMapObjectPanelAction(cityHallView, typeof(CityHallTaxesView)).SetInputState(InputActionType.Tap).OnComplete += () =>
            {
                if (jCityTaxesService.CityTaxes.CurrentCoins == 0)
                    AddWaitForSecondsAction(3f).OnComplete += HideCityView;
                else
                    AddPressButtonAction(jUIManager.GetView<CityHallTaxesView>().CollectButton).OnComplete += HideCityView;
            };
        }

        private void HideCityView()
        {
            jHideWindowSignal.Dispatch(typeof(CityHallTaxesView));
        }
        
        protected override List<TutorialActionData> GetLocalActionData()
        {
            List<TutorialActionData> localData = new List<TutorialActionData>();

            TutorialActionData buildAction = new TutorialActionData
            {
                BusinessSectorsType = BusinessSectorsTypes.Global,
                BuildingData = new List<TutorialBuildingData>() {new TutorialBuildingData(-1, -1, MapObjectintTypes.CityHall)}
            };

            localData.Add(buildAction);

            return localData;
        }
    }
}