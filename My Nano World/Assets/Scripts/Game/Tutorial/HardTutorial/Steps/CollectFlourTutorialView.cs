using NanoReality.Engine.UI.Views.Panels;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Production;

namespace NanoReality.Game.Tutorial
{
    [TutorialStep(StepType.CollectFlour)]
    public sealed class CollectFlourTutorialView : HardTutorialView
    {
        private readonly MapObjectintTypes MapObjectType = MapObjectintTypes.Factory;
        private readonly BusinessSectorsTypes SectorType = BusinessSectorsTypes.Farm;

        protected override void AddTutorialActions()
        {
            base.AddTutorialActions();
            
            var mine = FindMapObjectViewOnMapByType(SectorType, MapObjectType, 0);
            ((IProductionBuilding) mine.MapObjectModel).MaximalSlotsCount = 2;

            if(jUIManager.IsViewVisible(typeof(FactoryPanelView)))
            {
                var produceSlots = jUIManager.GetView<FactoryPanelView>().ProduceSlotViews;

                AddPressButtonAction(produceSlots[0].CollectButton);

                AddWaitForSecondsAction(0.5f).OnComplete += () => jHideWindowSignal.Dispatch(typeof(FactoryPanelView));
            }
            else
            {
                var collectProduct = CreateAction<TutorialActionCollectProducedProducts>();
                collectProduct.Init();
                collectProduct.SetCamera(mine.MapObjectModel);
                collectProduct.Set(mine.MapObjectModel as IProductionBuilding);

                ActionsToDoList.Add(collectProduct);

                AddWaitForSecondsAction(0.5f);
            }
        }
    }
}