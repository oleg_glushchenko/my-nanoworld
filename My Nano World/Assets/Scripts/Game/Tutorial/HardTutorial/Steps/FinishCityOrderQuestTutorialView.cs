﻿using Assets.NanoLib.Utilities;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.Quests;

namespace NanoReality.Game.Tutorial
{
    [TutorialStep(StepType.FinishCityOrderQuest)]
    public sealed class FinishCityOrderQuestTutorialView : HardTutorialView
    {

        private readonly Id<IQuest> _cityOrderQuestId = new Id<IQuest>(3);

        protected override void AddTutorialActions()
        {
            base.AddTutorialActions();
            
            AddHudQuestButtonAction(_cityOrderQuestId, BusinessSectorsTypes.Town);
        }
    }
}