﻿using I2.Loc;
using NanoReality.Engine.UI.Extensions.MapObjects.CityHallTaxes;
using NanoReality.Game.UI;

namespace NanoReality.Game.Tutorial
{
    [TutorialStep(StepType.CollectTaxesFinish)]
    public class CollectTaxesFinishTutorialView : HardTutorialView
    {
        protected override void AddTutorialActions()
        {
            base.AddTutorialActions();
            
            AddWaitForPanelCloseAction(typeof(CityHallTaxesView)).OnComplete += () => 
                AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_DIALOG_ASSISTANT_TAXES_REMINDER, 
                    DialogCharacter.Girl, DialogPosition.Left, CharacterExpression.Attention);
        }
    }
}
