using System;
using Assets.NanoLib.Utilities;
using Assets.Scripts.Engine.UI.Views.Hud;
using Assets.Scripts.GameLogic.Utilites;
using NanoReality.Engine.UI.Views.ConstractConfirmPanel;
using NanoReality.Engine.UI.Views.Hud;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Game.Tutorial
{
    [TutorialStep(StepType.BuildAgroPowerPlant)]
    public sealed class BuildAgroPowerPlantTutorialView : HardTutorialView
    {
        private readonly BuildingsCategories BuildingCategory = BuildingsCategories.Energy;
        private readonly BusinessSectorsTypes BuildingSector = BusinessSectorsTypes.Farm;
        private readonly BuildingSubCategories SubCategory = BuildingSubCategories.Common;
        private readonly int PowerPlantId = 147;
        private readonly Vector2F BuildPowePlantCameraPosition = new Vector2F(18, 6);
        private readonly Vector2 BuildingGridPosition = new Vector2(18, 6);

        private string ConstructDwellingDialog = LocalizationKeyConstants.TUTORIAL_REPLIC_CONSTRUCT_RESIDENTAL;
        private string BuildPlaceDwellingDialog = LocalizationKeyConstants.TUTORIAL_REPLIC_PLACE_DWELLING_TO_MARKED_ZONE;

        protected override void AddTutorialActions()
        {
            base.AddTutorialActions();
            
            if (HasBuildingOnMap(BuildingSector, PowerPlantId, 0, BuildingGridPosition))
            {
                Debug.LogError($"Power plant already exists. TutorialStep: {GetType().Name} skipped.");
                return;
            }

            HudView hudView = jUIManager.GetView<HudView>();

            HudBuildingsPanelView hudBuildingsPanel = hudView.HudBuildingsPanel;
            Button agroBuildingsButton = hudBuildingsPanel.FarmBuildingsButton;
            Action<bool> agroButtonAnimation = hudBuildingsPanel.DoAgroAnimation;

            TutorialActionShowPanel pressBuildingsShopAction = AddPressBuildingsShopAction(agroBuildingsButton,agroButtonAnimation, ConstructDwellingDialog);
            pressBuildingsShopAction.SetCameraInSector(BuildPowePlantCameraPosition, BuildingSector);

            AddBuildBuildingActions(BuildingSector, PowerPlantId, BuildingCategory, SubCategory, BuildingGridPosition, null, BuildPlaceDwellingDialog);

            AddWaitForConstructionFinishedAction(BuildingSector, PowerPlantId, BuildingGridPosition);
        }
        
        public override bool IsAllowedBuildPosition(Vector2 gridPosition)
        {
            return gridPosition.Equals(BuildingGridPosition);
        }
    }
}