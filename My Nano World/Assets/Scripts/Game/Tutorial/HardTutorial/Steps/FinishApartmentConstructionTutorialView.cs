﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using DG.Tweening;
using I2.Loc;
using NanoLib.Services.InputService;
using NanoReality.Game.UI;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.Configs;
using NanoReality.GameLogic.Upgrade;
using UnityEngine;

namespace NanoReality.Game.Tutorial
{
    [TutorialStep(StepType.FinishApartmentConstruction)]
    public sealed class FinishApartmentConstructionTutorialView : HardTutorialView
    {
        private const int ProductData = 0;
        private const int BuildDataAction = 1;

        protected override void AddTutorialActions()
        {
            base.AddTutorialActions();

            int ironOreId = ActionData[ProductData].ProduceProductData[0].Id;
            int productAmount = ActionData[ProductData].ProduceProductData[0].Amount;
            int buildingId = ActionData[BuildDataAction].BuildingData[0].Id;
            BusinessSectorsTypes buildingSector = ActionData[BuildDataAction].BusinessSectorsType;
            MapObjectintTypes mapObjectType = ActionData[BuildDataAction].BuildingData[0].MapObjectType;
            Vector2 buildingGridPosition = ActionData[BuildDataAction].Positions[0];
            int level = ActionData[BuildDataAction].BuildingData[0].Level;

            if (HasBuildingOnMap(buildingSector, buildingId, level, buildingGridPosition))
            {
                jDebug.LogError($"Dwelling already constructed. Tutorial Step: {GetType().Name} skipped.");
                return;
            }

            if (!jPlayerProfile.PlayerResources.HasEnoughProduct(ironOreId, productAmount))
            {
                jDebug.LogError($"Not enough resources for construction. Tutorial Step: {GetType().Name} skipped.");
                return;
            }
            
            MapObjectView buildingView = jGameManager.FindMapObjectViewOnMapByType(buildingSector, mapObjectType, buildingId);

            AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_DIALOG_ASSISTANT_HAVE_IRON_ORE, DialogCharacter.Girl)
                .SetCamera(buildingView.MapObjectModel);

            AddPressBuildingAttentionAction(buildingView, false, typeof(UpgradeWithProductsPanelView))
                .SetCamera(buildingView.MapObjectModel).SetInputState(InputActionType.Tap)
                .OnComplete += HighlightFirstUpgradeItem;

            AddUpgradeBuildingActions();
        }

        private void HighlightFirstUpgradeItem()
        {
            var upgradePanel = jUIManager.GetActiveView<UpgradeWithProductsPanelView>();
            if (upgradePanel == null)
                return;
            
            DraggableUpgradeItem targetUpgradeItem = upgradePanel.UpgradeItems.FirstOrDefault();
            if (targetUpgradeItem != null)
            {
                targetUpgradeItem.transform.DOPunchScale(0.3f * Vector3.one, 1f);
            }
        }
        
        protected override List<TutorialActionData> GetLocalActionData()
        {
            List<TutorialActionData> localData = new List<TutorialActionData>();

            TutorialActionData produceAction = new TutorialActionData
            {
                BusinessSectorsType = BusinessSectorsTypes.HeavyIndustry, ProduceProductData = new List<TutorialProduceProductData>()
            };

            TutorialProduceProductData data = new TutorialProduceProductData
            {
                Id = 157,  Amount = 2
            };
            produceAction.ProduceProductData.Add(data);

            TutorialActionData buildAction = new TutorialActionData
            {
                Positions = new List<Vector2>() {new Vector2(1, 8)},
                BusinessSectorsType = BusinessSectorsTypes.Town,
                BuildingData = new List<TutorialBuildingData>() {new TutorialBuildingData(145, 1, MapObjectintTypes.DwellingHouse)}
            };

            localData.Add(produceAction);
            localData.Add(buildAction);

            return localData;
        }
    }
}
