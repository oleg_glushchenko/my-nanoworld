﻿using System.Linq;
using Assets.Scripts.Engine.UI.Views.CityOrderDeskPanel;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;

namespace NanoReality.Game.Tutorial
{
    [TutorialStep(StepType.CollectCityOrder)]
    public class CollectCityOrderTutorialView : HardTutorialView
    {
        protected override void AddTutorialActions()
        {
            base.AddTutorialActions();
            
        }
    }
}
