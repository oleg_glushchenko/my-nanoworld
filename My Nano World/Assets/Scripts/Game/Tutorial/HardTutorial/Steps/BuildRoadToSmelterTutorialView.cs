using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using Assets.Scripts.Engine.UI.Views.Hud;
using Assets.Scripts.GameLogic.Utilites;
using NanoReality.Engine.UI.Views.ConstractConfirmPanel;
using NanoReality.Engine.UI.Views.Hud;
using NanoReality.Game.UI;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Game.Tutorial
{
    [TutorialStep(StepType.BuildSmelterRoad)]
    public sealed class BuildRoadToSmelterTutorialView : HardTutorialView
    {
        private float _buildCityRoadCameraZoom = 0.9f;

        private readonly List<Vector2> RoadGridPositions = new List<Vector2>
        {
            new Vector2(9, 20),
            new Vector2(9, 19),
            new Vector2(9, 18),
            new Vector2(9, 17),
            new Vector2(9, 16),
            new Vector2(9, 15)
        };

        private DialogCharacter Character => DialogCharacter.Engineer;

        private readonly BusinessSectorsTypes SectorType = BusinessSectorsTypes.HeavyIndustry;
        private readonly int MainRoadLevel = 200;

        protected override void AddTutorialActions()
        {
            base.AddTutorialActions();
            
            if (IsRoadAlreadyExists())
            {
                return;
            }
            
            MapObjectView cityRoadView = FindMapObjectViewOnMapByType(SectorType, MapObjectintTypes.Road, MainRoadLevel);
            IMapObject cityRoadModel = cityRoadView.MapObjectModel;

            HudView hudView = jUIManager.GetView<HudView>();
            HudRoadPanelView roadPanel = hudView.HudRoadPanel;


            Button openConstructRoadButton = roadPanel.OpenConstructRoadPanelButton;
            Button constructRoadButton = roadPanel.ConstuctRoadButton;
            Action<bool> highlightOpenRoadPanel = roadPanel.PlayOpenRoadConstructButtonAnimation;
            GameObject highlightRoadButtonParticles = roadPanel.GameObjectParticlesRoad;
            
            ///////////////////////////////////////////////////////////////////////////////////////

            AddOnlyDialogAction(LocalizationKeyConstants.SOFT_TUTORIAL_REPLIC_LEVEL_4_CONNECT_CINEMA_TO_ROAD, Character,
                characterExpression: CharacterExpression.Idle);
            ///////////////////////////////////////////////////////////////////////////////////////

            var pressHudRoadPanel = AddPressButtonAction(openConstructRoadButton);
            pressHudRoadPanel.SetCamera(cityRoadModel);


            pressHudRoadPanel.OnActivate += () =>
            {
                openConstructRoadButton.enabled = true;
                highlightOpenRoadPanel(true);
                highlightRoadButtonParticles.SetActive(true);
            };
            pressHudRoadPanel.OnComplete += () =>
            {
                highlightOpenRoadPanel(false);
                highlightRoadButtonParticles.SetActive(false);
                constructRoadButton.enabled = true;
            };

            ///////////////////////////////////////////////////////////////////////////////////////

            var cameraLookAtObject = cityRoadModel;
            var pressNextRoadButtonAction = AddPressButtonAction(constructRoadButton);
            pressNextRoadButtonAction.SetCamera(cameraLookAtObject);

            ///////////////////////////////////////////////////////////////////////////////////////

            var buildRoadAction = AddBuildRoadAction(RoadGridPositions, BusinessSectorsTypes.HeavyIndustry);
            buildRoadAction.SetCamera(cameraLookAtObject);
        }
        
        public override bool IsAllowedBuildPosition(Vector2 gridPosition)
        {
            return RoadGridPositions.Contains(gridPosition);
        }
        
        private bool IsRoadAlreadyExists()
        {
            foreach (Vector2 roadGridPosition in RoadGridPositions)
            {
                if (!HasBuildingOnMap(SectorType, 31, 0, roadGridPosition))
                {
                    return false;
                }
            }

            return true;
        }
    }
}