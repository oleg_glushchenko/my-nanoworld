using NanoReality.Engine.Storyline;
using UnityEngine.UI;

namespace NanoReality.Game.Tutorial
{
    [TutorialStep(StepType.Storyline)]
    public sealed class StorylineTutorialView : HardTutorialView
    {
        private TutorialActionWaitForSecond _waitStoryLineCompleteAction;
        [Inject] public SignalOnStorylineCompleted jCompletedStorylineSignal { get; set; }
        [Inject] public SignalStartPlayingStoryline jPlayStorylineSignal { get; set; }
        [Inject] public GraphicRaycaster jRaycaster { get; set; }
        
        protected override void AddTutorialActions()
        {
            base.AddTutorialActions();
            
            _waitStoryLineCompleteAction = AddWaitForSecondsAction(float.MaxValue);
            _waitStoryLineCompleteAction.OnActivateCompleted += () => jRaycaster.enabled = true;
            _waitStoryLineCompleteAction.OnActivate += StartStoryLine;
        }

        private void StartStoryLine()
        {
            // jCompletedStorylineSignal.AddOnce(OnStorylineCompleted);
            // jPlayStorylineSignal.Dispatch();
            // OnStorylineCompleted();
            _waitStoryLineCompleteAction.DoCompleteAction();
        }
    }
}