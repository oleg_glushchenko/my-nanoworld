﻿using Assets.Scripts.GameLogic.Utilites;
using NanoReality.Engine.UI.Extensions.MapObjects.CityHallTaxes;
using NanoReality.Game.UI;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;

namespace NanoReality.Game.Tutorial
{
    [TutorialStep(StepType.UpgradeCityHall)]
    public sealed class UpgradeCityHallTutorialView : HardTutorialView
    {
        protected override void AddTutorialActions()
        {
            base.AddTutorialActions();
            
            var cityHallMapObjectView =
                jGameManager.FindMapObjectViewOnMapByType(BusinessSectorsTypes.Global, MapObjectintTypes.CityHall);

            AddOnlyDialogAction(LocalizationKeyConstants.SOFT_TUTORIAL_REPLIC_LEVEL_6_FARM_OD_TOOLS,
                DialogCharacter.Girl);
            AddOpenMapObjectPanelAction(cityHallMapObjectView, typeof(CityHallTaxesView))
                .SetCamera(cityHallMapObjectView.MapObjectModel)
                .OnComplete+=()=> AddPressButtonAction(jUIManager.GetView<CityHallTaxesView>().UpgradeButton);
        }
    }
}