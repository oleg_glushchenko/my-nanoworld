using Assets.NanoLib.Utilities;
using NanoReality.Engine.UI.Views.LevelUpPanel;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.Quests;

namespace NanoReality.Game.Tutorial
{
    [TutorialStep(StepType.FinishAgroQuest)]
    public sealed class FinishAgroQuestTutorialView : HardTutorialView
    {
        private readonly Id<IQuest> AgroQuestId = new Id<IQuest>(24);

        protected override void AddTutorialActions()
        {
            base.AddTutorialActions();
            
            AddHudQuestButtonAction(AgroQuestId, BusinessSectorsTypes.Farm);
            AddWaitForPanelCloseAction(typeof(LevelUpPanelView), false);
            AddWaitForSecondsAction(2f);
        }
    }
}