﻿using System;
using NanoReality.Game.Services;
using UnityEngine;

namespace NanoReality.Game.Tutorial
{
    public interface IHardTutorial : IGameService
    {
        void UpdateStep();

        bool IsTutorialCompleted { get; }

        bool IsBuildingApprovedByTutorial(Vector2 position);

        void StartTutorial();

        /// <summary>
        /// Complete current tutorial step.
        /// </summary>
        void CompleteCurrentStep();
        
        /// <summary>
        /// Immediately change tutorial step.
        /// </summary>
        /// <param name="stepType">Required TutorialStep.</param>
        /// <param name="successCallback">On server success callback.</param>
        void JumpStep(StepType stepType, Action successCallback);
        
        void SkipTutorial(Action successCallback = null);
    }
}
