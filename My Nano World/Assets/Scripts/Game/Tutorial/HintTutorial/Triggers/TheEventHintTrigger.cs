using Assets.NanoLib.UI.Core.Signals;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.TheEvent;
using NanoReality.GameLogic.TheEvent.Signals;

namespace NanoReality.Game.Tutorial.HintTutorial
{
    public sealed class TheEventHintTrigger : HintTrigger
    {
        [Inject] public UserLevelUpSignal jLevelUpSignal { get; set; }
        [Inject] public IPlayerProfile jPlayerProfile { get; set; }
        [Inject] public SignalPreEventEndHintTrigger jSignalPreEventEndHintTrigger { get; set; }
        [Inject] public ITheEventService jITheEventService { get; set; }

        public int TriggerUserLevel { get; set; }

        protected override void OnActivate()
        {
            jLevelUpSignal.AddListener(OnLevelUp);
            jSignalPreEventEndHintTrigger.AddListener(InvokeTrigger);
        } 

        protected override void OnDeactivate()
        {
            jLevelUpSignal.RemoveListener(OnLevelUp);
            jSignalPreEventEndHintTrigger.RemoveListener(InvokeTrigger);
        }

        private void OnLevelUp()
        {
            if (TriggerUserLevel == jPlayerProfile.CurrentLevelData.CurrentLevel && jITheEventService.MainStateOfEvent())
            {
                PushHintTriggeredEvent();
            }
        }

        private void InvokeTrigger()
        {
            if (jPlayerProfile.CurrentLevelData.CurrentLevel >= TriggerUserLevel)
            {
                PushHintTriggeredEvent();
            }
        }

        protected override void IsCompleteOnActivate()
        {
            if (jPlayerProfile.CurrentLevelData.CurrentLevel >= TriggerUserLevel && jITheEventService.MainStateOfEvent())
            {
                PushHintTriggeredEvent();
            }
        }
    }
}
