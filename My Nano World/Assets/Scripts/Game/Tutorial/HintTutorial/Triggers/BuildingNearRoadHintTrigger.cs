using System;
using Assets.NanoLib.UI.Core.Signals;
using Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Models.impl;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.GameManager.Models.api;

namespace NanoReality.Game.Tutorial.HintTutorial
{
    public sealed class BuildingNearRoadHintTrigger : HintTrigger
    {
        [Inject] public SignalOnBuildingRoadConnectionChanged SignalOnBuildingRoadConnectionChanged { get; set; }
        [Inject] public SignalOnMapObjectBuildFinished jSignalOnMapObjectBuildFinished { get; set; }
        [Inject] public IGameManager jGameManager { get; set; }
        [Inject] public PlayerOpenedPanelSignal jSignalPlayerOpenedPanel { get; set; }

        public int TargetBuildingId { get; set; }
        
        protected override void OnActivate()
        {
            jSignalOnMapObjectBuildFinished.AddListener(OnMapBuildingSynced);
            SignalOnBuildingRoadConnectionChanged.AddListener(OnMapBuildingSynced);
        }

        protected override void OnDeactivate()
        {
            jSignalOnMapObjectBuildFinished.RemoveListener(OnMapBuildingSynced);
            SignalOnBuildingRoadConnectionChanged.RemoveListener(OnMapBuildingSynced);
        }

        private void OnMapBuildingSynced(IMapObject building)
        {
            if (building.ObjectTypeID == TargetBuildingId && building.IsConnectedToGeneralRoad && building.IsConstructed)
            {
                jSignalPlayerOpenedPanel.Dispatch();
                PushHintTriggeredEvent();
            }
        }

        protected override void IsCompleteOnActivate()
        {
            var findMapObjectByTypeId = jGameManager.FindMapObjectByTypeId<MapBuilding>(TargetBuildingId);
            if (findMapObjectByTypeId != null)
            {
                OnMapBuildingSynced(findMapObjectByTypeId);
            }
        }
    }
  // public sealed class HintScriptableObject
    // {
    //     [ValueDropdown("myVectorValues")]
    //     public Vector3 MyVector;
    //
    //     // The selectable values for the dropdown, with custom names.
    //     private ValueDropdownList<Type> myVectorValues = new ValueDropdownList<Type>()
    //     {
    //         {"Forward",	Vector3.forward	},
    //         {"Back",	Vector3.back	},
    //         {"Up",		Vector3.up		},
    //         {"Down",	Vector3.down	},
    //         {"Right",	Vector3.right	},
    //         {"Left",	Vector3.left	},
    //     };
    // }
    //
    // public sealed class HintItem
    // {
    //     public HintType Type;
    //     public 
    // }
  
}