﻿namespace NanoReality.Game.Tutorial.HintTutorial
{
    public enum HintType
    {
        None = 0,

        StartSeaport = 1,
        SendSeaportOrder = 2,
        SeaportOrderDialog = 3,
        SeaportFinal = 4,

        BuildFirstFarmHint = 5,
        BuildSecondFarmHint = 6,
        ProduceWheatHint = 7,
        SkipFirstHarvestWheatHint = 8,
        SkipSecondHarvestWheatHint = 9,
        CollectWheat = 10,

        SendMetroHint = 11,
        GetMetroRewardHint = 12,
        CloseMetroHint = 13,
        CookFoodSupplyHint = 14,
        FeedFoodSupplyHint = 15,
        GiftFoodSupplyHint = 16,
        TheEventHint1 = 17,
        TheEventHint2 = 18,
        TheEventHint3 = 19,

        //Unused
        
        Happiness,
        HappinessAoe
    }
}