﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using NanoLib.Services.InputService;
using NanoReality.Game.Tutorial;
using NanoReality.Game.Tutorial.HintTutorial;
using NanoReality.Game.UI;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.OrderDesk;
using UnityEngine;

namespace Game.Tutorial.HintTutorial
{
    [HintStep(HintType.GetMetroRewardHint)]
    public sealed class MetroGetRewardHintStep : HintTutorialStep
    {
        private MapObjectView _metroView;
        private const int MetroId = 135;

        protected override void AddTutorialActions()
        {
            _metroView = jGameManager.FindMapObjectViewOnMapByType(BusinessSectorsTypes.HeavyIndustry,
                MapObjectintTypes.CityOrderDesk, MetroId);

            if (jUIManager.IsViewVisible(typeof(MetroOrderDeskPanelView)))
            {
                GetReward();
                return;
            }

            AddOpenMapObjectPanelAction(_metroView, typeof(MetroOrderDeskPanelView))
                .SetInputState(InputActionType.Tap).SetCamera(_metroView.MapObjectModel)
                .OnComplete += GetReward;
        }

        private void GetReward()
        {
            MetroOrderDeskPanelView view = jUIManager.GetView<MetroOrderDeskPanelView>();
            MetroTrainView train = view.OrderTrainViews.FirstOrDefault();
            IEnumerable<MetroSlotItemView> orderPanels = train.OrderSlotViews;
            MetroSlotItemView firstRewardSlot = orderPanels.FirstOrDefault();

            view.BalancePanelView.SetButtonsAndTogglesEnabled(false);

            view.CloseButton.interactable = false;

            var waitForMetroDoorOpen = CreateAction<WaitForMetroDoorOpenAction>();
            ActionsToDoList.Add(waitForMetroDoorOpen);
            waitForMetroDoorOpen.SetMetroSlotModel(firstRewardSlot.Model);
            waitForMetroDoorOpen.OnComplete += () =>
            {
                if (firstRewardSlot.Model.IsRewardItem)
                {
                    AddPressButtonAction(firstRewardSlot.SlotButton).OnComplete += (() =>
                    {
                        view.CloseButton.interactable = true;
                        view.BalancePanelView.SetButtonsAndTogglesEnabled(true);
                    });
                }
            };
        }
    }
}
