using Assets.NanoLib.UI.Core.Signals;
using Assets.NanoLib.UI.Core.Views;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using I2.Loc;
using NanoReality.Engine.UI.Views.ConstractConfirmPanel;
using NanoReality.Engine.UI.Views.LevelUpPanel;
using NanoReality.Engine.UI.Views.Panels;
using NanoReality.Game.Tutorial;
using NanoReality.Game.Tutorial.HintTutorial;
using NanoReality.Game.UI;
using NanoReality.Game.UI.BuildingsShopPanel;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.UI.Components;

namespace Game.Tutorial.HintTutorial
{
    [HintStep(HintType.Happiness)]
    public sealed class HappinessHintStep : HintTutorialStep
    {
        private const BuildingsCategories BuildingCategory = BuildingsCategories.Entertaiment;
        private const BusinessSectorsTypes BuildingSector = BusinessSectorsTypes.Town;
        private const BuildingSubCategories SubCategory = BuildingSubCategories.Common;
        private const int TheatreBuildingId = 69;

        [Inject] public HideBalancePanelTooltipSignal jHideBalancePanelTooltipSignal { get; set; }
        [Inject] public SignalOnHidePanel jSignalOnHidePanel { get; set; }

        protected override void AddTutorialActions()
        {
            base.AddTutorialActions();
            
            AddWaitForPanelCloseAction(typeof(LevelUpPanelView), false);
            AddWaitForSecondsAction(3.0f);

            AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_HINT_HAPPINESS_INTRO, DialogCharacter.Girl)
                .SetDialogAcceptButtons(true, ScriptLocalization.Tutorial.TUTORIAL_HINT_SHOW,
                    string.Empty, OnUserAcceptHint, true);
        }

        private void OnUserAcceptHint(bool isAccepted)
        {
            if (!isAccepted)
            {
                ForceCompleteStep();
                return;
            }
            
            var balancePanelView = jUIManager.GetView<BalancePanelView>();
            
            HighlightPressButtonTutorialAction highlightPressButtonTutorialAction = AddPressButtonAction(balancePanelView.HappinessTooltipButton,
                veilParams:new VeilParams { PointerMode = PointerMode.TapBottom });
            highlightPressButtonTutorialAction.OnComplete += () =>
            {
                TutorialActionWaitForSecond tutorialActionWaitForSecond = AddWaitForSecondsAction(3.0f);

                tutorialActionWaitForSecond.OnComplete += () =>
                {
                    jUIManager.HideAll();
                    TutorialActionMessage happinessMessage = AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_HINT_BUILD_HAPPINESS, DialogCharacter.Girl);
                    happinessMessage.SetDialogAcceptButtons(true, ScriptTerms.Tutorial.TUTORIAL_HINT_SHOW,
                        string.Empty, OnUserAcceptBuildTheatre, true);
                };
            };
        }

        private void OnUserAcceptBuildTheatre(bool isAccepted)
        {
            jHideBalancePanelTooltipSignal.Dispatch();
            
            if (!isAccepted)
            {
                ForceCompleteStep();
                return;
            }
            
            jSignalOnHidePanel.AddListener(OnHideBuildingsShopPanel);
            jUiPanelOpenedSignal.AddListener(OnShowBuildingsShopPanel);
            
            TutorialAction addBuildBuildingActions = AddBuildBuildingActions(BuildingSector, TheatreBuildingId, BuildingCategory, SubCategory);
            addBuildBuildingActions.IsLookerCollidersActive = false;

            var placeTocConstructAction = GetActiveAction<PlaceAndConstructBuildingTutorialAction>();
            if (placeTocConstructAction != null)
            {
                placeTocConstructAction.IsCancelButtonEnabled = true;
                placeTocConstructAction.IsLookerCollidersActive = false;

                placeTocConstructAction.OnActivate += () =>
                {
                    jSignalOnHidePanel.RemoveListener(OnHideBuildingsShopPanel);
                    jSignalOnHidePanel.AddListener(OnHideConstructConfirmPanel);
                };
            }

            AddWaitForConstructionFinishedAction(BuildingSector, TheatreBuildingId);
            
            AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_HINT_REPLACE_HAPPINESS_BUILDING, DialogCharacter.Girl);
        }

        private void OnShowBuildingsShopPanel(UIPanelView obj)
        {
            if (!(obj is BuildingsShopPanelView))
                return;
            
            jUiPanelOpenedSignal.RemoveListener(OnShowBuildingsShopPanel);
            ((BuildingsShopPanelView)obj).EnableBuildingFiltersToggles(false);
        }

        private void OnHideBuildingsShopPanel(UIPanelView obj)
        {
            if (!(obj is BuildingsShopPanelView))
                return;
            
            jSignalOnHidePanel.RemoveListener(OnHideBuildingsShopPanel);
            ((BuildingsShopPanelView)obj).EnableBuildingFiltersToggles(true);

            ForceCompleteStep();
        }

        private void OnHideConstructConfirmPanel(UIPanelView view)
        {
            switch (view)
            {
                case BuildingsShopPanelView shopView:
                    shopView.EnableBuildingFiltersToggles(true);
                    break;
                case ConstractConfirmPanelView _:
                    jSignalOnHidePanel.RemoveListener(OnHideConstructConfirmPanel);
                    ForceCompleteStep();
                    break;
            }
        }
    }
}