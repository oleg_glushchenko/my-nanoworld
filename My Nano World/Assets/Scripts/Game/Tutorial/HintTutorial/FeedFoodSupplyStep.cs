﻿using System.Linq;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using I2.Loc;
using NanoLib.Services.InputService;
using NanoReality.Game.Tutorial;
using NanoReality.Game.Tutorial.HintTutorial;
using NanoReality.Game.UI;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using UnityEngine;

namespace Game.Tutorial.HintTutorial
{
    [HintStep(HintType.FeedFoodSupplyHint)]
    public sealed class FeedFoodSupplyStep : HintTutorialStep
    {
        [Inject] public SignalOnNeedToDisableTooltip jSignalOnNeedToDisableTooltip { get; set; }

        private const BusinessSectorsTypes BuildingSector = BusinessSectorsTypes.Global;

        protected override void AddTutorialActions()
        {
            if (jUIManager.IsViewVisible(typeof(FoodSupplyPanelView)))
            {
                FeedFoodSupply();
                return;
            }
            
            MapObjectView buildingView = jGameManager.FindMapObjectViewOnMapByType(BuildingSector,
                MapObjectintTypes.FoodSupply);

            AddOpenMapObjectPanelAction(buildingView, typeof(FoodSupplyPanelView))
                .SetInputState(InputActionType.Tap).SetCamera(buildingView.MapObjectModel)
                .OnComplete += FeedFoodSupply;
        }
        
        private void FeedFoodSupply()
        {
            AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_DIALOG_FOOD_SUPPLY_ZONES,
                DialogCharacter.Farmer);
            
            FoodSupplyPanelView view = jUIManager.GetView<FoodSupplyPanelView>();

            var feedingKeyValuePair = view.TabToggles.FirstOrDefault((t) => t.Value is FoodSupplyFeedingPanelController);

            FoodSupplyFeedingPanelView feedingPanelView = ((FoodSupplyFeedingPanelController) feedingKeyValuePair.Value).View;

            AddPressToggleAction(feedingKeyValuePair.Key).OnComplete += () => { jSignalOnNeedToDisableTooltip.Dispatch(false); };

            AddPressButtonAction(feedingPanelView.FeedingItemViews.First().SelectItemButton);

            AddWaitForPopupShowAction(typeof(WarehouseSelectionPopupView)).OnComplete += () =>
            {
                WarehouseSelectionPopupView warehouseSelectionPopupView = jPopupManager.GetActivePopupByType<WarehouseSelectionPopupView>();

                WarehouseSelectionProductView warehouseSelectionProductView = warehouseSelectionPopupView.VisibleProductViews.FirstOrDefault();

                if (warehouseSelectionProductView == null) return;
                AddPressButtonAction(warehouseSelectionProductView.SelectButton);
                AddPressButtonAction(warehouseSelectionPopupView.ConfirmButton);
                AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_DIALOG_FOOD_SUPPLY_FEED,
                    DialogCharacter.Farmer);
                AddPressButtonAction(feedingPanelView.FeedCitizensButton);
            };
        }
    }
}