using System.Linq;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using I2.Loc;
using NanoLib.Services.InputService;
using NanoReality.Game.Tutorial;
using NanoReality.Game.Tutorial.HintTutorial;
using NanoReality.Game.UI;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;

namespace Game.Tutorial.HintTutorial
{
    [HintStep(HintType.SkipFirstHarvestWheatHint)]
    public sealed class SkipHarvestTutorialHintStep : HintTutorialStep
    {
        private const BusinessSectorsTypes SectorType = BusinessSectorsTypes.Farm;
        private const MapObjectintTypes BuildingType = MapObjectintTypes.AgriculturalField;
        private bool _isDialogShowed;

        protected override void AddTutorialActions()
        {
            base.AddTutorialActions();

            var agrofields = FindMapObjectsViewOnMapByType(SectorType, BuildingType, 0);
            var field = agrofields.FirstOrDefault(c => ((IAgroFieldBuilding) c.MapObjectModel).GrowingTimeLeft > 0);

            if (field == null) return;

            AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_DIALOG_FARM_SKIP_PLOTS,
                DialogCharacter.Farmer, DialogPosition.Left, CharacterExpression.Idle);
            var action = ActionCropsReadyToHarvest(field);
            ActionsToDoList.Add(action);
        }

        private TutorialActionCropsReadyToHarvest ActionCropsReadyToHarvest(MapObjectView agrofield)
        {
            var tutorialActionCropsReadyToHarvest = CreateAction<TutorialActionCropsReadyToHarvest>();
            tutorialActionCropsReadyToHarvest.SetBuildingModel(agrofield.MapObjectModel as IAgroFieldBuilding);
            tutorialActionCropsReadyToHarvest.SetInputState(InputActionType.Tap);
            tutorialActionCropsReadyToHarvest.Init(agrofield.gameObject);
            tutorialActionCropsReadyToHarvest.SetCamera(agrofield.MapObjectModel);
            tutorialActionCropsReadyToHarvest.IsBuildingsNotInteractable = true;
            tutorialActionCropsReadyToHarvest.InteractableBuilding = agrofield;
            return tutorialActionCropsReadyToHarvest;
        }
    }
}