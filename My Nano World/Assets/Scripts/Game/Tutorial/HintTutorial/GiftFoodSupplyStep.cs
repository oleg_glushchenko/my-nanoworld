﻿using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using I2.Loc;
using NanoReality.Engine.BuildingSystem.MapObjects.Components;
using NanoReality.Engine.UI.Views.WorldSpaceUI;
using NanoReality.Game.Attentions;
using NanoReality.Game.CitizenGifts;
using NanoReality.Game.Tutorial;
using NanoReality.Game.Tutorial.HintTutorial;
using NanoReality.Game.UI;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;

namespace Game.Tutorial.HintTutorial
{
    [HintStep(HintType.GiftFoodSupplyHint)]
    public sealed class GiftFoodSupplyStep : HintTutorialStep
    {
        private IMapBuilding _buildWithGift;
        private CitizenGiftAttentionView _giftView;
            
        [Inject] public ICityGiftsService jCityGiftsService { get; set; }  
        [Inject] public IBuildingAttentionsController jBuildingAttentionsController { get; set; }
        
        protected override void AddTutorialActions()
        {
            if (jUIManager.IsViewVisible(typeof(FoodSupplyPanelView)))
            {
                FoodSupplyPanelView view = jUIManager.GetView<FoodSupplyPanelView>();
                
                AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_DIALOG_FOOD_SUPPLY_NOTICE,
                    DialogCharacter.Farmer).OnComplete += () =>
                {
                    view.BalancePanelView.SetButtonsAndTogglesEnabled(true);
                    view.Hide();
                    TryToClaimGift();
                };
                return;
            }

            TryToClaimGift();

        }

        private void TryToClaimGift()
        {
            _giftView = GetGift();

            if (_giftView == null)
            {
                WaitForGift();
                return;
            }

            ClaimGift(); 
        }

        private void WaitForGift()
        {
            AddWaitForFoodSupplyGifts().OnComplete += () =>
            {
                _giftView = GetGift();
                
                if (_giftView == null) return;

                ClaimGift();
            }; 
        }

        private CitizenGiftAttentionView GetGift()
        {
            _buildWithGift = jCityGiftsService.GetBuildWithGift();

            if (_buildWithGift == null) return null;

            MapObjectView mapObjectView = jGameManager.GetMapObjectView(_buildWithGift);
            CitizenGiftBuildingComponent giftComponent = mapObjectView.GetCachedComponent<CitizenGiftBuildingComponent>();
            return giftComponent.GetFirstGiftView();
        }

        private void ClaimGift()
        {
            HighlightPressButtonTutorialAction action = AddPressButtonAction(_giftView.Claim);
                
            action.SetCamera(_buildWithGift);

            AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_DIALOG_FOOD_SUPPLY_GIFTS,
                DialogCharacter.Farmer);
            AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_DIALOG_FOOD_SUPPLY_HAVE_FUN,
                DialogCharacter.Farmer);
        }
    }
}