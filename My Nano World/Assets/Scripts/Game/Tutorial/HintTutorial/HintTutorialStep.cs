using UnityEngine;

namespace NanoReality.Game.Tutorial.HintTutorial
{
    public abstract class HintTutorialStep : TutorialViewBase
    {
        public void StartTutorialStep()
        {
            BaseInit();
            AddTutorialActions();
            ProcessNextTutorialAction();
        }

        protected virtual void AddTutorialActions()
        {
            jUIManager.HideAll(true);
        }

        public void ForceCompleteStep()
        {
            ActionsToDoList.Clear();
            ProcessNextTutorialAction();
        }
        
        public virtual bool IsAllowedBuildPosition(Vector2 gridPosition)
        {
            return true;
        }
    }
}