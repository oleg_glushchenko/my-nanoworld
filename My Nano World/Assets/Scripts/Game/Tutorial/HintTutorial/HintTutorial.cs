using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Assets.Scripts.Engine.UI.Views.Hud;
using Game.Tutorial.HintTutorial;
using NanoLib.Core.Logging;
using NanoLib.Services.InputService;
using NanoLib.UI.Core;
using NanoReality.Core.Resources;
using NanoReality.Engine.BuildingSystem.CityCamera;
using NanoReality.Engine.UI.Views.Panels;
using NanoReality.Game.Attentions;
using NanoReality.Game.Services.BuildingsUnlockService;
using NanoReality.GameLogic.Configs;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using NanoReality.GameLogic.Settings.GamePrefs;
using NanoReality.GameLogic.TheEvent;
using strange.extensions.injector.api;
using Object = UnityEngine.Object;
using Vector2 = UnityEngine.Vector2;

namespace NanoReality.Game.Tutorial.HintTutorial
{
    public sealed class HintTutorial : IHintTutorial
    {
        #region Injections
        
        [Inject] public IServerCommunicator jServerCommunicator { get; set; }
        [Inject] public IGamePrefs jGamePrefs { get; set; }
        [Inject] public IUIManager jUIManager { get; set; }
        [Inject] public IGameCameraModel jGameCameraModel { get; set; }
        [Inject] public IInjectionBinder jInjectionBinder { get; set; }
        [Inject] public TutorialMapObjects jTutorialMapObjects { get; set; }
        [Inject] public IBuildingsUnlockService jBuildingsUnlockService { get; set; }
        [Inject] public IGameConfigData jGameConfigData { get; set; }
        [Inject] public ITheEventService jITheEventService { get; set; }

        #region Signals

        [Inject] public HintTriggeredSignal jHintTriggeredSignal { get; set; }
        [Inject] public TutorialStepCompleteSignal jHintTutorialStepComplete { get; set; }
        [Inject] public SignalOnFinishTutorial jFinishHardTutorialSignal { get; set; }
        [Inject] public InteractBuildingAttentionsSignal jInteractBuildingAttentionsSignal{ get; set; }
        [Inject] public AnaliticsTutorialStepCompletedSignal jAnaliticsTutorialStepCompletedSignal { get; set; }
        [Inject] public GraphicQualityChangedSignal jGraphicQualityChangedSignal { get; set; }

        #endregion
        
        #endregion

        #region Fields

        private HintTutorialStep _activeHintStep;
        
        private readonly Queue<HintType> _triggeredStepsQueue = new Queue<HintType>();
        
        private readonly Dictionary<HintType, Type> _boundHintStepsTypes = new Dictionary<HintType, Type>();
        private readonly Dictionary<HintType, bool> _stepsCompletionStatus = new Dictionary<HintType, bool>();
        private readonly List<HintTrigger> _boundTriggers = new List<HintTrigger>();
        private readonly HashSet<HintTrigger> _activeTriggers = new HashSet<HintTrigger>();

        private const string TutorialQualitySettingsShown = "QualitySettingsWindowWasShown";

        #endregion

        private readonly HashSet<Type> _availableHintStepTypes = new HashSet<Type>
        {
            typeof(StartSeaportHintStep),
            typeof(SendSeaportOrderHintTutorialStep),
            typeof(ShuffleSeaportHintStep),
            typeof(FinalSeaportHintStep),
            typeof(BuildFirstFarmStep),
            typeof(BuildSecondFarmStep),
            typeof(ProduceWheatTutorialHintStep),
            typeof(SkipHarvestTutorialHintStep),
            typeof(SkipSecondHarvestTutorialHintStep),
            typeof(CollectWheatTutorialHintStep),
            typeof(MetroOrderHintStep),
            typeof(MetroGetRewardHintStep),
            typeof(CloseMetroHintStep),
            typeof(CookFoodSupplyStep),
            typeof(FeedFoodSupplyStep),
            typeof(GiftFoodSupplyStep),
            typeof(TheEventTutorialHintStep1),
            typeof(TheEventTutorialHintStep2),
            typeof(TheEventTutorialHintStep3),
            
            //Unused
            
            typeof(HappinessHintStep),
            typeof(AoeHintStep)
        };

        public bool IsHintTutorialActive => _activeHintStep != null;

        public void StartTutorial()
        {
            SetHintTriggers();
            ActivateHintTriggers();
        }

        public void SkipTutorial(Action successCallback = null)
        {
            _activeTriggers.Clear();
            _boundTriggers.Clear();
            _triggeredStepsQueue.Clear();

            List<HintType> keys = _stepsCompletionStatus.Keys.Where(key => !_stepsCompletionStatus[key]).ToList();
            foreach (HintType key in keys)
            {
                _stepsCompletionStatus[key] = true;
                SaveCompletedStep(key, null);
            }
            
            FinishCurrentHint();
            
            successCallback?.Invoke();
        }

        public void UpdateStepsStatus(List<HintStepStatus> statusData)
        {
            _stepsCompletionStatus.Clear();

            foreach (HintStepStatus stepStatus in statusData)
                _stepsCompletionStatus.Add(stepStatus.StepType, stepStatus.IsCompleted);
        }

        private void SaveCompletedStep(HintType hintType, Action<List<HintStepStatus>> callback)
        {
            jAnaliticsTutorialStepCompletedSignal.Dispatch(StepType.None, hintType);
            jServerCommunicator.HintTutorialStepCompleted(hintType, callback);
        }

        public bool IsHintCompleted(HintType hintType)
        {
            return _stepsCompletionStatus.Keys.Contains(hintType) && _stepsCompletionStatus[hintType];
        }

        private void SetHintTriggers()
        {
            var data = jGameConfigData.TutorialConfigDate.HintStepActionData;

            if (data == null) return;

            int metroBuildingId = 0;
            int foodSupplyBuildingId = 0;
            int seaportHintBuildingId = 0;
            int farmBuildingId = 0;

            foreach (var actionData in data)
            {
                switch (actionData.StepId)
                {
                    case (int) HintType.StartSeaport:
                        seaportHintBuildingId = actionData.ActionData[1].BuildingData[0].Id;
                        break;
                    case (int) HintType.SendMetroHint:
                        metroBuildingId = actionData.ActionData[0].BuildingData[0].Id;
                        break;
                    case (int) HintType.CookFoodSupplyHint:
                        foodSupplyBuildingId = actionData.ActionData[0].BuildingData[0].Id;
                        break;
                    case (int) HintType.BuildFirstFarmHint:
                        farmBuildingId = actionData.ActionData[1].BuildingData[0].Id;
                        break;
                }
            }

            int seaportHintLevel = jBuildingsUnlockService.GetBuildingUnlockLevel(seaportHintBuildingId);
            int metroHintLevel = jBuildingsUnlockService.GetBuildingUnlockLevel(metroBuildingId);
            int foodSupplyHintLevel = jBuildingsUnlockService.GetBuildingUnlockLevel(foodSupplyBuildingId);
            int farmHintLevel = jBuildingsUnlockService.GetBuildingUnlockLevel(farmBuildingId);

            var seaportInitialTrigger = jInjectionBinder.GetInstance<UserLevelUpHintTrigger>();
            seaportInitialTrigger.HintType = HintType.StartSeaport;
            seaportInitialTrigger.TriggerUserLevel = seaportHintLevel;

            var seaportShuffleTrigger = jInjectionBinder.GetInstance<UserLevelUpHintTrigger>();
            seaportShuffleTrigger.HintType = HintType.SeaportOrderDialog;
            seaportShuffleTrigger.TriggerUserLevel = seaportHintLevel;

            var seaportPurchaseTrigger = jInjectionBinder.GetInstance<UserLevelUpHintTrigger>();
            seaportPurchaseTrigger.HintType = HintType.SendSeaportOrder;
            seaportPurchaseTrigger.TriggerUserLevel = seaportHintLevel;

            var seaportFinalTrigger = jInjectionBinder.GetInstance<UserLevelUpHintTrigger>();
            seaportFinalTrigger.HintType = HintType.SeaportFinal;
            seaportFinalTrigger.TriggerUserLevel = seaportHintLevel;

            var farmHintTrigger = jInjectionBinder.GetInstance<UserLevelUpHintTrigger>();
            farmHintTrigger.HintType = HintType.BuildFirstFarmHint;
            farmHintTrigger.TriggerUserLevel = farmHintLevel;

            var buildSecondFarmHintHintTrigger = jInjectionBinder.GetInstance<UserLevelUpHintTrigger>();
            buildSecondFarmHintHintTrigger.HintType = HintType.BuildSecondFarmHint;
            buildSecondFarmHintHintTrigger.TriggerUserLevel = farmHintLevel;

            var produceWheatHintTrigger = jInjectionBinder.GetInstance<UserLevelUpHintTrigger>();
            produceWheatHintTrigger.HintType = HintType.ProduceWheatHint;
            produceWheatHintTrigger.TriggerUserLevel = farmHintLevel;

            var startHarvestHintTrigger = jInjectionBinder.GetInstance<UserLevelUpHintTrigger>();
            startHarvestHintTrigger.HintType = HintType.SkipFirstHarvestWheatHint;
            startHarvestHintTrigger.TriggerUserLevel = farmHintLevel;

            var nextHarvestHintTrigger = jInjectionBinder.GetInstance<UserLevelUpHintTrigger>();
            nextHarvestHintTrigger.HintType = HintType.SkipSecondHarvestWheatHint;
            nextHarvestHintTrigger.TriggerUserLevel = farmHintLevel;

            var collectWheatHintTrigger = jInjectionBinder.GetInstance<UserLevelUpHintTrigger>();
            collectWheatHintTrigger.HintType = HintType.CollectWheat;
            collectWheatHintTrigger.TriggerUserLevel = farmHintLevel;

            var sendMetroHintTrigger = jInjectionBinder.GetInstance<UserLevelUpHintTrigger>();
            sendMetroHintTrigger.HintType = HintType.SendMetroHint;
            sendMetroHintTrigger.TriggerUserLevel = metroHintLevel;

            var getRewardMetroHintTrigger = jInjectionBinder.GetInstance<UserLevelUpHintTrigger>();
            getRewardMetroHintTrigger.HintType = HintType.GetMetroRewardHint;
            getRewardMetroHintTrigger.TriggerUserLevel = metroHintLevel;

            var сloseMetroHintHintTrigger = jInjectionBinder.GetInstance<UserLevelUpHintTrigger>();
            сloseMetroHintHintTrigger.HintType = HintType.CloseMetroHint;
            сloseMetroHintHintTrigger.TriggerUserLevel = metroHintLevel;

            var cookFoodSupplyHintTrigger = jInjectionBinder.GetInstance<UserLevelUpHintTrigger>();
            cookFoodSupplyHintTrigger.HintType = HintType.CookFoodSupplyHint;
            cookFoodSupplyHintTrigger.TriggerUserLevel = foodSupplyHintLevel;

            var feedFoodSupplyHintTrigger = jInjectionBinder.GetInstance<UserLevelUpHintTrigger>();
            feedFoodSupplyHintTrigger.HintType = HintType.FeedFoodSupplyHint;
            feedFoodSupplyHintTrigger.TriggerUserLevel = foodSupplyHintLevel;

            var giftFoodSupplyHintTrigger = jInjectionBinder.GetInstance<UserLevelUpHintTrigger>();
            giftFoodSupplyHintTrigger.HintType = HintType.GiftFoodSupplyHint;
            giftFoodSupplyHintTrigger.TriggerUserLevel = foodSupplyHintLevel;

            AddEventTriggers();

            _boundTriggers.Add(seaportInitialTrigger);
            _boundTriggers.Add(seaportShuffleTrigger);
            _boundTriggers.Add(seaportPurchaseTrigger);
            _boundTriggers.Add(seaportFinalTrigger);
            _boundTriggers.Add(farmHintTrigger);
            _boundTriggers.Add(buildSecondFarmHintHintTrigger);
            _boundTriggers.Add(produceWheatHintTrigger);
            _boundTriggers.Add(startHarvestHintTrigger);
            _boundTriggers.Add(nextHarvestHintTrigger);
            _boundTriggers.Add(collectWheatHintTrigger);
            _boundTriggers.Add(sendMetroHintTrigger);
            _boundTriggers.Add(getRewardMetroHintTrigger);
            _boundTriggers.Add(сloseMetroHintHintTrigger);
            _boundTriggers.Add(cookFoodSupplyHintTrigger);
            _boundTriggers.Add(feedFoodSupplyHintTrigger);
            _boundTriggers.Add(giftFoodSupplyHintTrigger);
        }

        private void AddEventTriggers()
        {
            var theEventInitialTrigger = jInjectionBinder.GetInstance<TheEventHintTrigger>();
            theEventInitialTrigger.HintType = HintType.TheEventHint1;

            var theEventSecondTrigger = jInjectionBinder.GetInstance<TheEventHintTrigger>();
            theEventSecondTrigger.HintType = HintType.TheEventHint2;

            var theEventThirdTrigger = jInjectionBinder.GetInstance<TheEventHintTrigger>();
            theEventThirdTrigger.HintType = HintType.TheEventHint3;

            if (jITheEventService.EventDataBase != null)
            {
                int theEventHintLevel = jITheEventService.EventDataBase.UnlockLevel;
                theEventInitialTrigger.TriggerUserLevel = theEventHintLevel;
                theEventSecondTrigger.TriggerUserLevel = theEventHintLevel;
                theEventThirdTrigger.TriggerUserLevel = theEventHintLevel;
            }

            _boundTriggers.Add(theEventInitialTrigger);
            _boundTriggers.Add(theEventSecondTrigger);
            _boundTriggers.Add(theEventThirdTrigger);
        }

        private void ActivateHintTriggers()
        {
            foreach (HintTrigger trigger in _boundTriggers)
            {
                HintType type = trigger.HintType;
                
                bool hintCompleted = IsHintCompleted(type);
                if (hintCompleted)
                {
                    Logging.Log(LoggingChannel.Tutorial, $"Hint Tutorial Trigger not activated. It is already completed. Step:<b>{type}</b>");
                    continue;
                }

                Logging.Log(LoggingChannel.Tutorial, $"<b>Hint Tutorial Trigger</b> activated. Step:{type} TriggerType:<b>{trigger}</b> ");
                trigger.Activate();
                _activeTriggers.Add(trigger);
            }
        }
        
        private void BindTutorialSteps()
        {
            foreach (Type tutorialStepType in _availableHintStepTypes)
            {
                var stepAttribute = (HintStepAttribute)tutorialStepType.GetCustomAttributes().FirstOrDefault(c => c is HintStepAttribute);
                if (stepAttribute == null)
                {
                    throw new Exception($"Tutorial hint step <b>{tutorialStepType.Name}</b> hasn't HintStepAttribute in class declaration.");
                }
                
                HintType step = stepAttribute.Step;
                
                _boundHintStepsTypes.Add(step, tutorialStepType);
                jInjectionBinder.Bind(tutorialStepType).To(tutorialStepType);
            }
        }

        private void ResolveTutorialSteps()
        {
            foreach (Type tutorialStepType in _availableHintStepTypes)
            {
                IInjectionBinding binding = jInjectionBinder.GetBinding(tutorialStepType);
                jInjectionBinder.ResolveBinding(binding, tutorialStepType);
            }
        }

        public bool IsBuildingApprovedByHintTutorial(Vector2 position)
        {
            return !IsHintTutorialActive || _activeHintStep.IsAllowedBuildPosition(position);
        }
        
        private void ResolveFinishHintTutorial()
        {
            // TODO: workaround.
            jGameCameraModel.IsNeedToLockSwipeProperty.Value = false;      

            var findObjectOfType = Object.FindObjectOfType<InputLayerChecker>();
            findObjectOfType.CurrentInputState = InputState.CreateDefault();

            jUIManager.GetView<HudView>().SetButtonsAndTogglesEnabled(true);
            jUIManager.GetView<BalancePanelView>().SetButtonsAndTogglesEnabled(true);

            jInteractBuildingAttentionsSignal.Dispatch(true);
        }
        
        [PostConstruct]
        public void PostConstruct()
        {
            jHintTriggeredSignal.AddListener(OnHintTriggered);
            jHintTutorialStepComplete.AddListener(OnHintStepCompleted);
            jFinishHardTutorialSignal.AddListener(OnFinishHardTutorial);
            
            BindTutorialSteps();
        }

        [Deconstruct]
        public void Deconstruct()
        {
            jHintTriggeredSignal.RemoveListener(OnHintTriggered);
            jHintTutorialStepComplete.RemoveListener(OnHintStepCompleted);
            jFinishHardTutorialSignal.RemoveListener(OnFinishHardTutorial);
            
            ResolveTutorialSteps();
        }
        
        private void StartStep(HintType hintStepType)
        {
            Type stepType = _boundHintStepsTypes[hintStepType];
            var hintStep = (HintTutorialStep)jInjectionBinder.GetInstance(stepType);
            if (hintStep == null)
            {
                Logging.LogError(LoggingChannel.Tutorial, "Can't instantiate hint step.");
                return;
            }

            _activeHintStep = hintStep;
            _activeHintStep.ActionData = jGameConfigData.TutorialConfigDate.HintStepActionData?.FirstOrDefault(a => a.StepId == (int)hintStepType)?.ActionData;
            _activeHintStep.StartTutorialStep();
            
            Logging.Log(LoggingChannel.Tutorial, $"Tutorial hint step: <b>{hintStep}</b> started");
        }
        
        private void OnHintStepCompleted(TutorialViewBase hintStep)
        {
            if (!(hintStep is HintTutorialStep))
            {
                return;
            }

            if (hintStep != _activeHintStep)
            {
                Logging.LogError(LoggingChannel.Tutorial, "Completed hint step isn't equal to current active hint step.");
                return;
            }

            Logging.Log(LoggingChannel.Tutorial, $"Tutorial hint step {hintStep.GetType().Name} completed");
            _activeHintStep.Deactivate();
            _activeHintStep = null;

            var type = HintType.None;
            foreach (KeyValuePair<HintType, Type> valuePair in _boundHintStepsTypes.Where(valuePair => valuePair.Value == hintStep.GetType()))
            {
                type = valuePair.Key;
            }

            if (type == HintType.None)
            {
                Logging.LogError(LoggingChannel.Tutorial, "Unknown tutorial hint step finished. Aborting finish sequence.");
                return;
            }
            
            SaveCompletedStep(type, UpdateStepsStatus);

            if (_triggeredStepsQueue.Any())
            {
                HintType nextHintStepType = _triggeredStepsQueue.Dequeue();
                StartStep(nextHintStepType);
            }
            else
            {
                ResolveFinishHintTutorial();
            }
        }

        private void OnHintTriggered(HintTrigger hintTrigger)
        {
            HintType hintType = hintTrigger.HintType;
            hintTrigger.Deactivate();
            if (_activeHintStep == null)
            {
                jGameCameraModel.IsNeedToLockSwipeProperty.Value = true; 
                StartStep(hintType);
            }
            else
            {
                _triggeredStepsQueue.Enqueue(hintType);
            }
        }

        private void FinishCurrentHint()
        {
            jTutorialMapObjects.SetActive(false);
            _activeHintStep?.ForceCompleteStep();
        }

        private void OnFinishHardTutorial()
        {
            if (jGamePrefs.HasKey(TutorialQualitySettingsShown) && jGamePrefs.GetBool(TutorialQualitySettingsShown))
                return;

            jGamePrefs.SetBool(TutorialQualitySettingsShown, true);
            jGraphicQualityChangedSignal.Dispatch(true);
        }
    }
}