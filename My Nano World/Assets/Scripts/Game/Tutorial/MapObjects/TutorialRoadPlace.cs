﻿using System.Collections.Generic;
using Assets.Scripts.Engine.BuildingSystem.CityMap;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.GameManager.Models.api;
using NanoReality.GameLogic.Settings.BuildSettings.Models.Api;
using UnityEngine;

namespace NanoReality.Game.Tutorial
{
	public class TutorialRoadPlace : BaseTutorialMapObject
	{
		[SerializeField] private GameObject _gameObjectStart;
		[SerializeField] private GameObject _gameObjectEnd;
		[SerializeField] private Transform _transformStart;
		[SerializeField] private Transform _transformEnd;
		
		[SerializeField] private SpriteRenderer _roadSprite;
		
		[Inject] public ICityMapSettings jCityMapSettings { get; set; }
		[Inject] public IGameManager jGameManager { get; set; }
		
		public GameObject GameObjectStart => _gameObjectStart;

		public GameObject GameObjectEnd => _gameObjectEnd;

		private BusinessSectorsTypes _currentSector;
		private Vector3 _currentDirection;


		public Vector3 StartPoint;
		public Vector3 EndPoint;
		public BusinessSectorsTypes SectorType;
		
		private readonly Dictionary<Vector3, Vector3> _directionRotations = new Dictionary<Vector3, Vector3>()
		{
			{ new Vector3(0, 0, -1), new Vector3(0, 0, 0)},
			{ new Vector3(0, 0, 1), new Vector3(180, 0, 0)},
			{ new Vector3(1, 0, 0), new Vector3(180, 90, 180)},
			{ new Vector3(-1, 0, 0), new Vector3(0, 90, 180)},
		};
		
		private readonly Dictionary<Vector3, Vector3> _directionPositions = new Dictionary<Vector3, Vector3>
		{
			{ new Vector3(0, 0, -1), new Vector3(0, 0, 1)},
			{ new Vector3(-1, 0, 0), new Vector3(1, 0, 0)},
		};

		public void Show(BusinessSectorsTypes citySector, Vector2 gridStartPosition, Vector2 gridEndPosition)
		{
			_currentSector = citySector;
			_currentDirection = new Vector3(gridEndPosition.x - gridStartPosition.x, 0, gridEndPosition.y - gridStartPosition.y);
			
			ActivateAndUpdatePositions(gridStartPosition, gridEndPosition);
		}
		
		#region privates

		private void ActivateAndUpdatePositions(Vector2 gridStartPosition, Vector2 gridEndPosition)
		{
			_gameObject.SetActive(true);

			CityMapView currentSectorView = jGameManager.GetMapView(_currentSector.GetBusinessSectorId());
			var sectorViewPivot = currentSectorView.transform.position;
			
			var startPosition = new Vector2(sectorViewPivot.x, sectorViewPivot.z) + jCityMapSettings.GridCellScale * gridStartPosition;
			var endPosition = new Vector2(sectorViewPivot.x, sectorViewPivot.z) + jCityMapSettings.GridCellScale * gridEndPosition;

			var worldStartPosition = new Vector3(startPosition.x, 0, startPosition.y);
			var worldEndPosition = new Vector3(endPosition.x, 0, endPosition.y);

			float roadGridLength = _currentDirection.magnitude + 1;
			Vector3 normalizedDirection = _currentDirection.normalized;
			
			_directionPositions.TryGetValue(normalizedDirection, out Vector3 gridPositionOffset);

			transform.localScale = jCityMapSettings.GridCellScale * Vector3.one;
			_roadSprite.size = new Vector2(roadGridLength, 1);

			Vector3 worldSpacePositionOffset = gridPositionOffset * jCityMapSettings.GridCellScale;
			
			transform.position = worldStartPosition + worldSpacePositionOffset;
			transform.eulerAngles = _directionRotations[normalizedDirection];
			
			_transformEnd.position = worldEndPosition;
			_transformStart.position = worldStartPosition;
		}
		
		#endregion
	}
}