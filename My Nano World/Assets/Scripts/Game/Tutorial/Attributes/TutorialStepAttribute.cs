using System;
using NanoReality.Game.Tutorial.HintTutorial;

namespace NanoReality.Game.Tutorial
{
    public sealed class TutorialStepAttribute : Attribute
    {
        public readonly StepType Step;

        public TutorialStepAttribute(StepType step)
        {
            Step = step;
        }
    }
    
    public sealed class HintStepAttribute : Attribute
    {
        public readonly HintType Step;

        public HintStepAttribute(HintType step)
        {
            Step = step;
        }
    }
}