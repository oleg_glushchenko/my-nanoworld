﻿using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using NanoReality.Engine.BuildingSystem.MapObjects;
using strange.extensions.signal.impl;

namespace NanoReality.Game.Tutorial
{
    public class SignalOnFinishTutorial : Signal { }
    
    public class StartTutorialSignal : Signal<StepType> { }
    
    public class SignalTutorialRoadCreate : Signal<RoadMapObjectView> { }
    
    public class SignalTutorialRoadRemove : Signal<RoadMapObjectView> { }
    
    public class SignalTutorialOnConfirmConstruct : Signal<MapObjectView> { }
    
    public class SignalTutorialHowToAnimationEnded: Signal { }

    public class SignalSoftAndHardTutorialCommonStateChanged : Signal<bool> {}
    
    public sealed class TutorialActionCompleteSignal : Signal<TutorialAction> { }
    
    public sealed class TutorialStepCompleteSignal : Signal<TutorialViewBase> { }
}