using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;

namespace NanoReality.Game.Tutorial
{
    [CreateAssetMenu(menuName = "Assets/NanoReality/Tutorial/TutorialFlowSettings", fileName = "TutorialFlowNew", order = 0)]
    public sealed class TutorialFlowSettings : SerializedScriptableObject
    {
        [SerializeField] private List<TutorialStepData> _tutorialSteps;

        public IEnumerable<TutorialStepData> TutorialSteps => _tutorialSteps;
        
        public TutorialStepData GetStepData(StepType step)
        {
            return _tutorialSteps.FirstOrDefault(c => c.Step == step);
        }
    }
}