using UnityEngine.Advertisements;

namespace NanoReality.Game.Advertisements
{
    public struct AdsWatchResult
    {
        public string PlacementId;
        public ShowResult Result;

        public AdsWatchResult(string placementId, ShowResult result)
        {
            PlacementId = placementId;
            Result = result;
        }
    }
}