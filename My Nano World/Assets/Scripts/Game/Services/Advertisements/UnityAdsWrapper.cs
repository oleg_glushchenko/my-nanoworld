using System;
using NanoLib.Core.Logging;
using UniRx;
using UnityEngine.Advertisements;

namespace NanoReality.Game.Advertisements
{
    public sealed class UnityAdsWrapper : IAdsWrapper, IUnityAdsListener
    {
        private readonly string _unityAdsPlacementId;
        private readonly string _unityAdsId;
        private readonly bool _useTestAds;

        public bool IsVideoLoaded => Advertisement.IsReady(_unityAdsPlacementId);
        public event EventHandler VideoStarted;
        public event EventHandler VideoLoaded;
        public event EventHandler VideoWatched;
        public event EventHandler VideoSkipped;

        public UnityAdsWrapper(bool useTestAds, string unityAdsPlacementId, string unityAdsId, bool debugAds, bool consent)
        {
            _unityAdsPlacementId = unityAdsPlacementId;
            _unityAdsId = unityAdsId;
            _useTestAds = useTestAds;
            
            var gdprMetaData = new MetaData("gdpr");
            gdprMetaData.Set("consent", consent.ToString());
            Advertisement.SetMetaData(gdprMetaData);
            
            Advertisement.debugMode = debugAds;
        }

        public void Initialize()
        {
            Advertisement.AddListener(this);
            Advertisement.Initialize(_unityAdsId, _useTestAds, true);
        }

        public void Dispose()
        {
            Advertisement.RemoveListener(this);
        }

        public void EnableDebug(bool enable)
        {
            Advertisement.debugMode = enable;
        }

        public void ShowVideo()
        {
            Advertisement.Show(_unityAdsPlacementId);
        }

        public void LoadVideo()
        {
            Logging.Log(LoggingChannel.Ads,$"LoadVideo _unityAdsPlacementId : {_unityAdsPlacementId}  IsVideoLoaded {IsVideoLoaded}");
            
            Advertisement.Load(_unityAdsPlacementId);
        }

        public void OnUnityAdsReady(string placementId)
        {
            MainThreadDispatcher.Send((c) =>
            {
                Logging.Log(LoggingChannel.Ads,$"OnUnityAdsReady placementId : {placementId}");
                VideoLoaded?.Invoke(this, EventArgs.Empty);
            }, null);
        }

        public void OnUnityAdsDidError(string message)
        {
            MainThreadDispatcher.Send((c) =>
            {
                Logging.LogError(LoggingChannel.Ads,$"UnityAds watch error. Message: {message}");
            }, null);
        }

        public void OnUnityAdsDidStart(string placementId)
        {
            MainThreadDispatcher.Send((c) =>
            {
                Logging.Log(LoggingChannel.Ads,$"OnUnityAdsDidStart placementId : {placementId}");
                VideoStarted?.Invoke(this, EventArgs.Empty);
            }, null);
        }

        public void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
        {
            MainThreadDispatcher.Send((c) =>
            {
                if (showResult == ShowResult.Finished) 
                {
                    VideoWatched?.Invoke(this, EventArgs.Empty);
                } 
                else if (showResult == ShowResult.Skipped) 
                {
                    VideoSkipped?.Invoke(this, EventArgs.Empty);
                } 
                else if (showResult == ShowResult.Failed) 
                {
                    Logging.LogError (LoggingChannel.Ads,"UnityAds finish watching error.");
                }
            }, null);
        }
    }
}