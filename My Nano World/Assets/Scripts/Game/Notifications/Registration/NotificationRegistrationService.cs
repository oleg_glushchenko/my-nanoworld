﻿using System;
using NanoReality.GameLogic.BuildingSystem.CityMap.Controllers;
using NanoReality.GameLogic.BuildingSystem.MapObjects;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Controllers.Agricultural;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Controllers.Factories;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Factories;
using NanoReality.GameLogic.Utilites;

namespace NanoReality.Game.Notifications
{
    public class NotificationRegistrationService : INotificationRegistrationService
    {
        [Inject] public OnChangeGrowingState jOnChangeGrowingState { get; set; }
        [Inject] public StartProductionSignal jStartProductionSignal { get; set; }
        [Inject] public ProductReadySignal jProductReadySignal { get; set; }
        [Inject] public SignalOnBuildStartedVerified jSignalOnBuildStartedVerified { get; set; }
        [Inject] public SignalOnMapObjectUpgradedOnMap jSignalOnMapObjectUpgradedOnMap { get; set; }
        [Inject] public SignalOnMapObjectBuildFinished jOnMapObjectBuildingFinishedSignal { get; set; }

        public event Action<NotificationData> AddPushNotification;
        public event Action<Type, int> CancelPushNotification;

        private const int MinimumPermittedSecondsToShow = 300;
        
        public void Init()
        {
            jOnChangeGrowingState.AddListener(OnAgroFieldStateChange);
            jStartProductionSignal.AddListener(RegisterProductionNotification);
            jProductReadySignal.AddListener(CancelNotification);
            jSignalOnBuildStartedVerified.AddListener(RegisterConstructNotification);
            jSignalOnMapObjectUpgradedOnMap.AddListener(RegisterUpgradeNotification);
            jOnMapObjectBuildingFinishedSignal.AddListener(CancelNotification);
        }

        public void Dispose()
        {
            jOnChangeGrowingState.RemoveListener(OnAgroFieldStateChange);
            jStartProductionSignal.RemoveListener(RegisterProductionNotification);
            jProductReadySignal.RemoveListener(CancelNotification);
            jSignalOnBuildStartedVerified.RemoveListener(RegisterConstructNotification);
            jSignalOnMapObjectUpgradedOnMap.RemoveListener(RegisterUpgradeNotification);
            jOnMapObjectBuildingFinishedSignal.RemoveListener(CancelNotification);
        }
        
        private void RegisterProductionNotification(IProduceSlot produceSlot)
        {
            var (id, timeToShow, productId, type) = GetNotificationData(produceSlot);
            var message = new object[] {LocalizationUtils.GetProductText(productId)};

            RegisterNotification(LocalNotificationType.ProductionComplete, id, timeToShow, type, message, message);
        }

        private void OnAgroFieldStateChange(IAgroFieldBuilding agroField)
        {
            switch (agroField.CurrentState)
            {
                case AgriculturalFieldState.FirstGrowthPhase:
                    RegisterHarvestNotification(agroField);
                    break;
        
                case AgriculturalFieldState.Completed:
                    CancelNotification(agroField);
                    break;
            }
        }
        
        private void RegisterHarvestNotification(IAgroFieldBuilding agroField)
        {
            var (id, timeToShow, productId, type) = GetNotificationData(agroField);
            var message = new object[] {LocalizationUtils.GetProductText(productId)};

            RegisterNotification(LocalNotificationType.HarvestReady, id, timeToShow, type, message, message);
        }

        private void RegisterConstructNotification(IMapObject mapObject)
        {
            var (id, timeToShow, _, _, type) = GetNotificationData(mapObject);
            var message = new object[] {LocalizationUtils.GetBuildingText(id)};
            
            RegisterNotification(LocalNotificationType.BuildingConstructed, id, timeToShow, type, message, message);
        }

        private void RegisterUpgradeNotification(IMapObject mapObject)
        {
            var (id, timeToShow, level, name, type) = GetNotificationData(mapObject);
            var title = new object[] {LocalizationUtils.GetBuildingText(id), level};
            var message = new object[] {name, level};
            
            RegisterNotification(LocalNotificationType.BuildingConstructed, id, timeToShow, type, title, message);
        }
        
        private void RegisterNotification(LocalNotificationType notificationType, int id, int timeToShow, Type objectType, object[] title, object[] message)
        {
            if (timeToShow <= MinimumPermittedSecondsToShow) return;
            
            var notificationData = new NotificationData(id, objectType, notificationType, timeToShow);
            notificationData.SetTitleParams(title);
            notificationData.SetDescriptionParams(message);
	        
            AddPushNotification?.Invoke(notificationData);
        }

        private void CancelNotification(IProduceSlot produceSlot, Id_IMapObject mapId)
        {
            var (id, _, _, type) = GetNotificationData(produceSlot);

            CancelPushNotification?.Invoke(type, id);
        }
        
        private void CancelNotification(IAgroFieldBuilding agroField)
        {
            var (id, _, _, type) = GetNotificationData(agroField);

            CancelPushNotification?.Invoke(type, id);
        }
        
        private void CancelNotification(IMapObject mapObject)
        {
            var (id, _, _, _, type) = GetNotificationData(mapObject);

            CancelPushNotification?.Invoke(type, id);
        }
        
        private static (int id, int time, int productId, Type type) GetNotificationData(IProduceSlot produceSlot) => 
            (produceSlot.Id, (int)produceSlot.CurrentProducingTime, produceSlot.ItemDataIsProducing.OutcomingProducts, produceSlot.GetType());

        private static (int id, int time, int productId, Type type) GetNotificationData(IAgroFieldBuilding agroField) =>
            (agroField.MapID, (int)agroField.CurrentGrowingPlant.ProducingTime, agroField.CurrentGrowingPlant.OutcomingProducts, agroField.GetType());

        private static (int id, int time, int level, string name, Type type) GetNotificationData(IMapObject mapObject) =>
            (mapObject.ObjectTypeID, mapObject.CurrentConstructTime, mapObject.Level, mapObject.Name, mapObject.GetType());
    }
}