using System;
using UnityEngine;

namespace NanoReality
{
    [Obsolete]
    public static class Env
    {
        private static Camera _mainCamera;

        [Obsolete]
        public static Camera MainCamera
        {
            get
            {
                if (_mainCamera == null)
                {
                    _mainCamera = Camera.main;
                }

                return _mainCamera;
            }
            private set => _mainCamera = value;
        }
    }
}