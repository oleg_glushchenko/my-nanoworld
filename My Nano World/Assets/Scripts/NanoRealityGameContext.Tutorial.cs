﻿using Assets.NanoLib.UI.Core.Signals;
using Game.Tutorial.HintTutorial;
using NanoReality.Engine.Storyline;
using NanoReality.Game.Tutorial;
using NanoReality.Game.Tutorial.HintTutorial;
using NanoReality.GameLogic.BuildingSystem.CityMap.Controllers;
using NanoReality.GameLogic.Common.Controllers.Signals;
using UnityEngine;

namespace NanoReality
{
    public partial class NanoRealityGameContext
    {
        public TutorialFlowSettings TutorialFlowSettings { get; set; }
        
        private void TutorialBinds()
        {
            // Storyline

            injectionBinder.Bind<TutorialFlowSettings>().ToValue(TutorialFlowSettings).ToSingleton();
            
            commandBinder.Bind<SignalStartPlayingStoryline>();
            commandBinder.Bind<SignalOnStorylineCompleted>();
            
            // Core
            
            injectionBinder.Bind<IHardTutorial>().To<HardTutorial>().ToSingleton();
            injectionBinder.Bind<IHintTutorial>().To<HintTutorial>().ToSingleton();

            injectionBinder.Bind<UserLevelUpHintTrigger>().To<UserLevelUpHintTrigger>();
            injectionBinder.Bind<TheEventHintTrigger>().To<TheEventHintTrigger>(); 
            injectionBinder.Bind<BuildingNearRoadHintTrigger>().To<BuildingNearRoadHintTrigger>();
            commandBinder.Bind<HintTriggeredSignal>();
            
            injectionBinder.Bind<TutorialPanelsProvider>().To<TutorialPanelsProvider>().ToSingleton();
            
            // Services
            
            injectionBinder.Bind<TutorialAction>().To<TutorialAction>();
            injectionBinder.Bind<HighlightPressButtonTutorialAction>().To<HighlightPressButtonTutorialAction>();
            injectionBinder.Bind<HighlightToggleTutorialAction>().To<HighlightToggleTutorialAction>();
            injectionBinder.Bind<HighlightBuildingInShopTutorialAction>().To<HighlightBuildingInShopTutorialAction>();
            injectionBinder.Bind<TutorialActionBuildRoad>().To<TutorialActionBuildRoad>();
            injectionBinder.Bind<TutorialActionPlaceBuilding>().To<TutorialActionPlaceBuilding>();
            injectionBinder.Bind<TutorialActionStartBuildBuilding>().To<TutorialActionStartBuildBuilding>();
            injectionBinder.Bind<TutorialActionWaitForPanelClose>().To<TutorialActionWaitForPanelClose>();
            injectionBinder.Bind<TutorialActionWaitForPopupClose>().To<TutorialActionWaitForPopupClose>();
            injectionBinder.Bind<TutorialActionMessage>().To<TutorialActionMessage>();
            injectionBinder.Bind<TutorialActionShowPanel>().To<TutorialActionShowPanel>();
            injectionBinder.Bind<TutorialActionWaitForPopupShow>().To<TutorialActionWaitForPopupShow>();
            injectionBinder.Bind<TutorialActionUpgradeBuilding>().To<TutorialActionUpgradeBuilding>();
            injectionBinder.Bind<TutorialActionHarvestItemFromMine>().To<TutorialActionHarvestItemFromMine>();
            injectionBinder.Bind<TutorialActionWaitForFoodSupplyGifts>().To<TutorialActionWaitForFoodSupplyGifts>();
            injectionBinder.Bind<TutorialActionWaitForFoodSupplyLoaded>().To<TutorialActionWaitForFoodSupplyLoaded>();
            injectionBinder.Bind<StartIndustryProductionTutorialAction>().To<StartIndustryProductionTutorialAction>();
            injectionBinder.Bind<TutorialActionFinishFactoryOrMineProduction>().To<TutorialActionFinishFactoryOrMineProduction>();
            injectionBinder.Bind<TutorialActionShipProductFromFactory>().To<TutorialActionShipProductFromFactory>();
            injectionBinder.Bind<TutorialActionVideoWatched>().To<TutorialActionVideoWatched>();
            injectionBinder.Bind<TutorialActionWaitForClouds>().To<TutorialActionWaitForClouds>();
            injectionBinder.Bind<TutorialActionWaitForSecond>().To<TutorialActionWaitForSecond>();
            injectionBinder.Bind<TutorialActionStartHarvestCrops>().To<TutorialActionStartHarvestCrops>();
            injectionBinder.Bind<TutorialActionFillUpgradeProduct>().To<TutorialActionFillUpgradeProduct>();
            injectionBinder.Bind<TutorialActionStartPlaceBuilding>().To<TutorialActionStartPlaceBuilding>();
            injectionBinder.Bind<RepairBuildingTutorialAction>().To<RepairBuildingTutorialAction>();
            injectionBinder.Bind<TutorialActionPlantCrops>().To<TutorialActionPlantCrops>();
            injectionBinder.Bind<StartFarmProductionTutorialAction>().To<StartFarmProductionTutorialAction>();
            injectionBinder.Bind<WaitForMetroDoorOpenAction>().To<WaitForMetroDoorOpenAction>();
            injectionBinder.Bind<TutorialActionCropsReadyToHarvest>().To<TutorialActionCropsReadyToHarvest>();
            injectionBinder.Bind<TutorialActionCropsHarvested>().To<TutorialActionCropsHarvested>();
            injectionBinder.Bind<TutorialActionWaitForOrderTruck>().To<TutorialActionWaitForOrderTruck>();
            injectionBinder.Bind<TutorialActionWaitForQuestsPanelShown>().To<TutorialActionWaitForQuestsPanelShown>();
            injectionBinder.Bind<WaitFinishConstructionTutorialAction>().To<WaitFinishConstructionTutorialAction>();
            injectionBinder.Bind<TutorialActionCollectProducedProducts>().To<TutorialActionCollectProducedProducts>();
            injectionBinder.Bind<TutorialActionCollectCityOrderReward>().To<TutorialActionCollectCityOrderReward>();
            injectionBinder.Bind<TutorialActionSkipConstructionTime>().To<TutorialActionSkipConstructionTime>();
            injectionBinder.Bind<PlaceAndConstructBuildingTutorialAction>().To<PlaceAndConstructBuildingTutorialAction>();
            injectionBinder.Bind<ReplaceBuildingTutorialAction>().To<ReplaceBuildingTutorialAction>();
            injectionBinder.Bind<TutorialActionWaitForCondition>().To<TutorialActionWaitForCondition>();
            injectionBinder.Bind<CollectProductionPanelProductTutorialAction>().To<CollectProductionPanelProductTutorialAction>();
            injectionBinder.Bind<TutorialActionBuildingAttention>().To<TutorialActionBuildingAttention>();
            injectionBinder.Bind<TutorialActionWaitForLevelUp>().To<TutorialActionWaitForLevelUp>();
            injectionBinder.Bind<HoldDragItemTutorialAction>().To<HoldDragItemTutorialAction>();
            injectionBinder.Bind<CompleteSeaportOrderTutorialAction>().To<CompleteSeaportOrderTutorialAction>();

			// Signals

			commandBinder.Bind<SignalOnFinishTutorial>();
            commandBinder.Bind<StartTutorialSignal>();
            commandBinder.Bind<SignalOnTutorialStepStarted>();
            commandBinder.Bind<SignalOnFastFinishButtonActivated>();
            commandBinder.Bind<SignalOnRewardClaimed>();
            commandBinder.Bind<SignalOnSkipPriceChanged>();
            commandBinder.Bind<SignalTutorialRoadCreate>();
            commandBinder.Bind<SignalTutorialRoadRemove>();
            commandBinder.Bind<SignalTutorialOnConfirmConstruct>();
            commandBinder.Bind<SignalOnTutorialStepChanged>();
            commandBinder.Bind<SignalTutorialHowToAnimationEnded>();
            commandBinder.Bind<SignalSoftAndHardTutorialCommonStateChanged>();
            commandBinder.Bind<TutorialStepCompleteSignal>();
            commandBinder.Bind<TutorialActionCompleteSignal>();
            commandBinder.Bind<SignalLaunchSoftTutorial>();
            commandBinder.Bind<SignalSoftTutorialCompleted>();
            commandBinder.Bind<SignalSoftTutorialStarted>();
            commandBinder.Bind<AnaliticsTutorialStepCompletedSignal>();
            commandBinder.Bind<SignalOnFireworksStart>();

            // TODO: workaround.
            TutorialMapObjects tutorialMapObjects = Object.FindObjectOfType<TutorialMapObjects>();
            injectionBinder.Bind<TutorialMapObjects>().ToValue(tutorialMapObjects).ToSingleton();
        }
    }
}