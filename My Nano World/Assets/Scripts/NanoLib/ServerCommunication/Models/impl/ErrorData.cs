﻿namespace Assets.NanoLib.ServerCommunication.Models
{
	public class ErrorData : IErrorData
	{
		public int HttpErrorCode { get; set; }
		public string ErrorMessage { get; set; }
		public string URL { get; set; }
		public string Response { get; }
		public RequestMethod Method { get; }

		public ErrorData(int httpErrorCode, string errorMessage, RequestData requestData)
		{
			HttpErrorCode = httpErrorCode;
			ErrorMessage = errorMessage;
			URL = requestData.ToString();
			Response = requestData.WebRequest?.downloadHandler?.text;
			Method = requestData.RequestMethod;
		}
	}
}