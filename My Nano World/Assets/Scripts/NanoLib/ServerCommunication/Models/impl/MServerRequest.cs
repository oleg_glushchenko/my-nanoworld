﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Assets.NanoLib.UtilitesEngine;
using Assets.NanoLib.Utilities.DataManager.Models.api;
using Assets.Scripts.Extensions;
using NanoLib.Core.Logging;
using NanoReality.Engine.Settings.BuildSettings;
using NanoReality.GameLogic.ServerCommunication;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using UniRx;
using UnityEngine;
using UnityEngine.Networking;
using Logging = NanoLib.Core.Logging.Logging;

namespace Assets.NanoLib.ServerCommunication.Models
{
	public class MServerRequest : IServerRequest
    {
        #region Inject
        [Inject] public ILocalDataManager jLocalDataManager { get; set; }
        [Inject] public IServerCommunicator jServerCommunicator { get; set; }
        [Inject] public NetworkSettings jNetworkSettings { get; set; }
        [Inject] public ITimerManager jTimerManager { get; set; }
        [Inject] public SignalOnAddRequestToQueue jSignalOnAddRequestToQueue { get; set; }
        [Inject] public SignalOnReciveAnswerFromServer jSignalOnReceiveAnswerFromServer { get; set; }

        #endregion

        private string _serverUrl;
        private RequestData _requestInProcess;
        private readonly Queue<RequestData> _requestQueue = new Queue<RequestData>();
        private readonly Queue<RequestData> _hashRequestsQueue = new Queue<RequestData>();
        
        private LocalReuqestDataKeeper LocalRequestDataKeeper => jLocalDataManager.GetData<LocalReuqestDataKeeper>();
        private bool IsRequestInProcess => _requestInProcess != null;

        public event Action<RequestData> ResponseSent;
        public event Action<RequestData> ResponseReceived;
        public event Action<RequestData> AddToQueue;

        #region Simple queries using queue

        public void SendRequest(RequestMethod requestMethod, Dictionary<string, string> parameters, 
			string url, RequestCompleteDelegate successCallback, Action<IErrorData> requestErrorCallback, 
			Dictionary<string, string> headers = null)
        {
	        Logging.Log(LoggingChannel.Network, $"<b><color=#9966ff>Request Added</color> {requestMethod} {url}</b>");
            
            var data = new RequestData(requestMethod, parameters, url, successCallback, requestErrorCallback, jTimerManager.CurrentUTC,  headers);
            _requestQueue.Enqueue(data);

#if UNITY_EDITOR
            jSignalOnAddRequestToQueue.Dispatch(data);
#endif
	        
            SendRequestFromQueue();
        }

        public void SendRequest(RequestMethod requestMethod, string url, RequestCompleteDelegate successCallback,
	        Action<IErrorData> requestErrorCallback, Dictionary<string, string> headers = null)
        {
	        SendRequest(requestMethod, new Dictionary<string, string>(0), url, successCallback, requestErrorCallback, headers);
        }

	    public void SendRequest(RequestMethod requestMethod, string url, Dictionary<string, LazyParameter<string>> parameters, RequestCompleteDelegate successCallback, 
			Action<IErrorData> requestErrorCallback, Dictionary<string, string> headers = null, Func<string> urlAdditionalParams = null)
		{
			var data = new RequestData(requestMethod, parameters, url, successCallback, requestErrorCallback, jTimerManager.CurrentUTC, headers, urlAdditionalParams);

			Logging.Log(LoggingChannel.Network, $"<b><color=#9966ff>Request Added:</color> {requestMethod} {url}</b>");

			_requestQueue.Enqueue(data); 

#if UNITY_EDITOR
			jSignalOnAddRequestToQueue.Dispatch(data);
#endif

			SendRequestFromQueue();
		}
	    
        private void SendRequestFromQueue()
        {
	        if (_requestQueue.Count == 0 || IsRequestInProcess) return;

            RequestData data = _requestQueue.Dequeue();
            _requestInProcess = data;

            Observable.FromCoroutine(() => EnumeratorSendRequest(data)).Subscribe();
        }

        private IEnumerator EnumeratorSendRequest(RequestData data)
        {
	        string urlPath = data.GetURL();
            yield return null;

            UnityWebRequest request;
			string queryInfoString;
			
			if (data.Parameters != null)
			{
				request = CreateRequest(data.RequestMethod, data.UTC0, data.Parameters, urlPath, data.Headers);
				queryInfoString = GetQueryInfoString(data.RequestMethod, data.Parameters, request.url);
			}
			else
			{
				request = CreateRequest(data.RequestMethod, data.UTC0, data.LazyParametrs, urlPath, data.Headers);
				queryInfoString = GetQueryInfoString(data.RequestMethod, data.LazyParametrs, request.url);
			}
	        
	        request.timeout = jNetworkSettings.NetworkTimeoutRequestTime;

	        var stopWatch = Stopwatch.StartNew();
            UnityWebRequestAsyncOperation operation = request.SendWebRequest();
            
            LogServerRequest(queryInfoString);
            ResponseSent?.Invoke(data);
            yield return operation;

            if (jNetworkSettings.SimulateNetwork)
            {
	            yield return new WaitForSeconds(jNetworkSettings.SimulateNetworkDelay);
            }
            
            stopWatch.Stop();
            data.WebRequest = request;
            data.Duration = stopWatch.ElapsedMilliseconds;
            data.StatusCode = (int)operation.webRequest.responseCode;
            
            LogServerResponse(data);
            HandleRequest(data);
        }

        public void ResendLastRequest()
        {
	        Observable.FromCoroutine(() => EnumeratorSendRequest(_requestInProcess)).Subscribe();
        }

        public void StopAndClearRequestQueue()
        {
            _requestQueue.Clear();
            _requestInProcess = null;
        }

	    public void Restart()
	    {
		    StopAndClearRequestQueue();
		    _requestInProcess = default;
	    }
	    
        #endregion

        #region ETag Queries
        
        public void SendRequestHash(RequestMethod requestMethod, Dictionary<string, string> parameters, string url, RequestCompleteDelegate successCallback, Action<IErrorData> requestErrorCallback)
        {
	        if (string.IsNullOrEmpty(url))
	        {
		        Logging.Log(LoggingChannel.Network, "Request url is null or empty.");
		        return;
	        }
	        
	        var data = new RequestData(url, requestMethod, jTimerManager.CurrentUTC, successCallback, requestErrorCallback, parameters);
	        
	        _hashRequestsQueue.Enqueue(data);
	        SendHashRequestFromQueue();
        }

        private void SendHashRequestFromQueue()
        {
	        if (IsRequestInProcess || _hashRequestsQueue.Count == 0)
	        {
		        return;
	        }
	        
	        _requestInProcess = _hashRequestsQueue.Dequeue();
	        Observable.FromCoroutine(() => EnumeratorSendRequestHash(_requestInProcess)).Subscribe();
        }
        
        private IEnumerator EnumeratorSendRequestHash(RequestData requestData)
        {
	        RequestMethod requestMethod = requestData.RequestMethod;
	        Dictionary<string, string> parameters = requestData.Parameters;
	        string urlPath = requestData.GetURL();
	        
            LocalRequestData localData = LocalRequestDataKeeper.GetLocalRequestData(urlPath);
            
            string localHash = localData.Hash;
            string serverHash = localData.PreloadedHash;
            int answerSecretHashCode = localData.AnswerSecretHash;
            int localSecretHashCode = localData.Answer.GetHashCode();
	            
            if (answerSecretHashCode == localSecretHashCode)
            {
	            if (!string.IsNullOrEmpty(serverHash) && serverHash == localHash)
	            {
#if UNITY_EDITOR
		            requestData.StatusCode = 200;
		            requestData.ServerAnswer = localData.Answer;
		            jSignalOnReceiveAnswerFromServer.Dispatch(requestData);
#endif
		            requestData.SuccessCallback(localData.Answer);
		            _requestInProcess = default;
		            SendRequestFromQueue();
		            SendHashRequestFromQueue();
		            
		            yield break;
	            }
            }
	        
            UnityWebRequest request = CreateRequest(requestMethod, jTimerManager.CurrentUTC, parameters, urlPath);
	        if (!string.IsNullOrEmpty(localHash))
	        {
		        request.SetRequestHeader("If-None-Match", localHash);
	        }

	        var requestTime = Stopwatch.StartNew();

            yield return request.SendWebRequest();
            
            // Network delay simulation.
            if (jNetworkSettings.SimulateNetwork)
            {
	            yield return new WaitForSeconds(jNetworkSettings.SimulateNetworkDelay);
            }
            
            requestTime.Stop();
            requestData.WebRequest = request;
            requestData.Duration = requestTime.ElapsedMilliseconds;
            
            LogServerResponse(requestData);

            if (request.isHttpError)
            {
	            HandleErrorRequest(requestData);
            }

            switch ((int) request.responseCode)
            {
	            case ServerResponseCodes.RedirectionNotModified:
	            {
		            HandleRequest(requestData);
		            break;
	            }

	            case ServerResponseCodes.SuccessOK:
	            case ServerResponseCodes.SuccessCreated:
	            case ServerResponseCodes.LogicGiftError:
	            {
		            localData.Url = urlPath;
		            localData.Hash = request.GetResponseHeader("Etag")?.Replace("W/", "").Replace("\"", "");
		            localData.PreloadedHash = localData.Hash;
		            localData.Answer = request.downloadHandler.text;
		            localData.AnswerSecretHash = localData.Answer.GetHashCode();
		            
		            HandleRequest(requestData);
		            break;
	            }

	            default:
	            {
		            HandleErrorRequest(requestData);
		            break;
	            }
            }

            request.Dispose();
        }

        #endregion

        private void HandleRequest(RequestData requestData)
        {
	        if (!IsRequestInProcess) return;
	        
	        var request = requestData.WebRequest;
	        try
	        {
		        if (request.isHttpError)
		        {
			        HandleErrorRequest(requestData);
			        return;
		        }

		        if (!ServerResponseCodes.IsSuccess((int) request.responseCode))
		        {
			        HandleErrorRequest(requestData);
			        return;
		        }

		        requestData.SuccessCallback?.Invoke(request.downloadHandler.text);
		        _requestInProcess = default;

#if UNITY_EDITOR
		        requestData.StatusCode = (int) request.responseCode;
		        requestData.ServerAnswer = request.downloadHandler?.text;
		        
		        jSignalOnReceiveAnswerFromServer.Dispatch(requestData);
#endif

		        request.Dispose();

		        SendHashRequestFromQueue();
		        SendRequestFromQueue();
	        }
	        catch (Exception exception)
	        {
		        exception.LogError(LoggingChannel.Network);
		        HandleErrorRequest(requestData);
	        }
        }
        
        private void HandleErrorRequest(RequestData data)
        {
	        UnityWebRequest request = data.WebRequest;
	        var errorData = new ErrorData((int) request.responseCode, request.error, data);
	        
	        request.Dispose();
	        data.RequestErrorCallback(errorData);
        }

        private static void LogServerRequest(object obj)
        {
	        var stringBuilder = new StringBuilder();
	        stringBuilder.Append("<color=#9966ff><b>Request Sent:</b></color> ");
	        stringBuilder.Append(obj);
            
	        Logging.Log(LoggingChannel.Network, stringBuilder.ToString());
        }

        private void LogServerResponse(RequestData requestData)
        {
	        var stringBuilder = new StringBuilder();
	        stringBuilder.Append($"<b><color=#9966ff>Request Received:</color> <color=#b3b300>{requestData.RequestMethod}</color> {requestData.GetURL()} </b> Code:<b>{requestData.StatusCode}</b> ");
	        AddTimeInMilliseconds(stringBuilder, requestData.Duration);
	        stringBuilder.Append($"Text: " + requestData.WebRequest?.downloadHandler.text);
	        // Uncomment it for check response headers.
	        //LogServerResponseHeaders(stringBuilder, request);

	        Logging.Log(LoggingChannel.Network, stringBuilder.ToString());
	        ResponseReceived?.Invoke(requestData);
        }

        private static void LogServerResponseHeaders(StringBuilder stringBuilder, UnityWebRequest request)
        {
	        var headers = request.GetResponseHeaders();

	        if (headers != null && headers.Count > 0)
	        {
		        stringBuilder.AppendLine("ResponseHeaders:");
		        foreach (var header in headers)
		        {
			        stringBuilder.Append(header.Key);
			        stringBuilder.Append(": ");
			        stringBuilder.AppendLine(header.Value);
		        }
	        }
        }
        
        private static void AddTimeInMilliseconds(StringBuilder stringBuilder, float elapsedMilliseconds)
        {
	        if (elapsedMilliseconds <= 0) return;

	        const string more500Color = "#FF0000";
	        const string more250Color = "#FF4500";
	        const string less250Color = "#008000";
            
	        string color = elapsedMilliseconds >= 500 ? more500Color : elapsedMilliseconds >= 250 ? more250Color : less250Color;
            
	        stringBuilder.Append($" <b><color={color}>Time: {elapsedMilliseconds} ms</color></b>");
        }
        
        private string Md5Sum(string strToEncrypt)
        {
	        UTF8Encoding ue = new UTF8Encoding();
	        byte[] bytes = ue.GetBytes(strToEncrypt);

	        // encrypt bytes
	        System.Security.Cryptography.MD5CryptoServiceProvider md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
	        byte[] hashBytes = md5.ComputeHash(bytes);

	        // Convert the encrypted bytes back to a string (base 16)
	        string hashString = "";

	        for (int i = 0; i < hashBytes.Length; i++)
	        {
		        hashString += Convert.ToString(hashBytes[i], 16).PadLeft(2, '0');
	        }

	        return hashString.PadLeft(32, '0');
        }
	    
        private string GetRequestHash(string token, long time)
        {
	        return Md5Sum(Md5Sum(token + time));
        }

        #region Common things

        private string GetQueryInfoString(RequestMethod requestMethod, Dictionary<string, string> parameters, string url)
        {
	        StringBuilder builder = new StringBuilder();

	        builder.Append($"<b><color=#9966ff>{requestMethod}</color> {url}</b>");
	        builder.Append("?");
	        builder.Append(MakeRequestParametersString(parameters));
	        return builder.ToString();
        }

        private string GetQueryInfoString(RequestMethod requestMethod, Dictionary<string, LazyParameter<string>> parameters, string url)
        {
	        StringBuilder builder = new StringBuilder();

	        builder.Append($"<b><color=#9966ff>{requestMethod}</color> {url}</b>");
	        builder.Append("?");
	        builder.Append(MakeRequestParametersString(parameters));
	        return builder.ToString();
        }

        private string MakeRequestParametersString(Dictionary<string, string> parameters)
        {
	        if (parameters == null || parameters.Count == 0)
		        return " ";

	        StringBuilder builder = new StringBuilder("");

	        foreach (var parameter in parameters)
		        builder.Append(parameter.Key).Append("=").Append(WWW.EscapeURL(parameter.Value)).Append("&");

	        if (builder.Length > 0)
		        builder = builder.Remove(builder.Length - 1, 1);

	        return builder.ToString();
        }

        private string MakeRequestParametersString(Dictionary<string, LazyParameter<string>> parameters)
        {
	        if (parameters == null || parameters.Count == 0)
		        return " ";

	        StringBuilder builder = new StringBuilder("");

	        foreach (var parameter in parameters)
		        builder.Append(parameter.Key).Append("=").Append(WWW.EscapeURL(parameter.Value.GetValue())).Append("&");

	        if (builder.Length > 0)
		        builder = builder.Remove(builder.Length - 1, 1);

	        return builder.ToString();
        }

        /// <returns>форма запроса</returns>
        private WWWForm CreateFormWithParameters(Dictionary<string, string> parameters)
        {
	        var formData = new WWWForm();

	        foreach (var parameter in parameters)
		        formData.AddField(parameter.Key, parameter.Value);

	        return formData;
        }

        private WWWForm CreateFormWithParameters(Dictionary<string, LazyParameter<string>> parameters)
        {
	        var formData = new WWWForm();

	        foreach (var parameter in parameters)
		        formData.AddField(parameter.Key, parameter.Value.GetValue());

	        return formData;
        }

        private UnityWebRequest CreateRequest(RequestMethod requestMethod, long serverTimeUtc, Dictionary<string, string> parameters,
	        string url, Dictionary<string, string> headers = null)
        {
	        UnityWebRequest request;

	        string serverUrl = GetServerUrl() + url;
	        switch (requestMethod)
	        {
		        case RequestMethod.GET:
			        var urlE = serverUrl;
			        if (parameters != null && parameters.Count > 0)
				        urlE += "?" + MakeRequestParametersString(parameters);
			        request = UnityWebRequest.Get(urlE);
			        request.SetRequestHeader("Content-Type", "text/plain");
			        break;

		        case RequestMethod.POST:
			        request = UnityWebRequest.Post(serverUrl, CreateFormWithParameters(parameters));
			        request.SetRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			        break;

		        case RequestMethod.PUT:
			        request = UnityWebRequest.Put(serverUrl, MakeRequestParametersString(parameters));
			        break;

		        case RequestMethod.DELETE:
			        request = UnityWebRequest.Put(serverUrl, MakeRequestParametersString(parameters));
			        request.method = UnityWebRequest.kHttpVerbDELETE;
			        break;

		        default:
			        throw new ArgumentOutOfRangeException("requestMethod", requestMethod, null);
	        }

	        if (!string.IsNullOrEmpty(jServerCommunicator.AccessToken))
	        {
		        request.SetRequestHeader("access-token", jServerCommunicator.AccessToken);
	        }

	        request.SetRequestHeader("time", serverTimeUtc.ToString());

	        var hash = GetRequestHash(jServerCommunicator.AccessToken, serverTimeUtc);
	        request.SetRequestHeader("hash", hash);

	        if (headers != null)
	        {
		        foreach (var header in headers)
		        {
			        request.SetRequestHeader(header.Key, header.Value);
		        }
	        }

	        return request;
        }

        private UnityWebRequest CreateRequest(RequestMethod requestMethod, long serverTimeUtc, Dictionary<string, LazyParameter<string>> parameters,
	        string url, Dictionary<string, string> headers = null)
        {
	        UnityWebRequest request;

	        string serverUrl = GetServerUrl() + url;
	        switch (requestMethod)
	        {
		        case RequestMethod.GET:
			        string urlE = serverUrl;
			        if (parameters != null && parameters.Count > 0)
				        urlE += "?" + MakeRequestParametersString(parameters);
			        request = UnityWebRequest.Get(urlE);
			        request.SetRequestHeader("Content-Type", "text/plain");
			        break;

		        case RequestMethod.POST:
			        request = UnityWebRequest.Post(serverUrl, CreateFormWithParameters(parameters));
			        request.SetRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			        break;

		        case RequestMethod.PUT:
			        request = UnityWebRequest.Put(serverUrl,
				        MakeRequestParametersString(parameters));
			        break;

		        case RequestMethod.DELETE:
			        request = UnityWebRequest.Put(serverUrl,
				        MakeRequestParametersString(parameters));
			        request.method = UnityWebRequest.kHttpVerbDELETE;
			        break;

		        default:
			        throw new ArgumentOutOfRangeException("requestMethod", requestMethod, null);
	        }

	        if (!string.IsNullOrEmpty(jServerCommunicator.AccessToken))
	        {
		        request.SetRequestHeader("access-token", jServerCommunicator.AccessToken);
	        }

	        request.SetRequestHeader("time", serverTimeUtc.ToString());

	        string hash = GetRequestHash(jServerCommunicator.AccessToken, serverTimeUtc);
	        request.SetRequestHeader("hash", hash);

	        if (headers != null)
	        {
		        foreach (KeyValuePair<string, string> header in headers)
		        {
			        request.SetRequestHeader(header.Key, header.Value);
		        }
	        }

	        return request;
        }
        
        private string GetServerUrl()
        {
	        if (!string.IsNullOrEmpty(_serverUrl))
	        {
		        return _serverUrl;
	        }
	        
	        string customServerUrl;
	        if (jNetworkSettings.UseCustomServer)
	        {
		        customServerUrl = PlayerPrefs.GetString("custom_server");
		        if (customServerUrl.IsNullOrEmpty())
		        {
			        customServerUrl = jNetworkSettings.ServerAddress;
			        $"Not found custom server url in PlayerPrefs. Set Server Url: {customServerUrl}".Log(LoggingChannel.Saves);
		        }
	        }
	        else
	        {
		        customServerUrl = jNetworkSettings.ServerAddress;
	        }

	        _serverUrl =  customServerUrl;
	        return _serverUrl;
        }

        #endregion
    }
}
