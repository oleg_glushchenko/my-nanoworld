﻿using System;
using System.Collections.Generic;

// ReSharper disable once CheckNamespace
using UnityEngine;


namespace Assets.NanoLib.ServerCommunication.Models
{
    public enum RequestMethod
    {
        GET,
        POST,
        PUT,
        DELETE
    }
    
    public delegate void RequestCompleteDelegate(string responseText);
    
    public interface IServerRequest
    {
	    event Action<RequestData> ResponseSent;
	    event Action<RequestData> ResponseReceived;
	    
	    void SendRequest(RequestMethod requestMethod, Dictionary<string, string> parameters, string url,
            RequestCompleteDelegate successCallback, Action<IErrorData> requestErrorCallback,
            Dictionary<string, string> headers = null);
        
        void SendRequest(RequestMethod requestMethod, string url,
	        RequestCompleteDelegate successCallback, Action<IErrorData> requestErrorCallback,
	        Dictionary<string, string> headers = null);
        
		void SendRequest(RequestMethod requestMethod, string url, Dictionary<string, LazyParameter<string>> parameters,
			RequestCompleteDelegate successCallback, Action<IErrorData> requestErrorCallback, 
			Dictionary<string, string> headers = null, Func<string> urlAdditionalParams = null);
		
        void SendRequestHash(RequestMethod requestMethod, Dictionary<string, string> parameters, string url,
	        RequestCompleteDelegate successCallback, Action<IErrorData> requestErrorCallback);

        void ResendLastRequest();
        
        void StopAndClearRequestQueue();

	    void Restart();
    }
}