﻿namespace Assets.NanoLib.Utilities.DataManager.Models.api

{
    /// <summary>
    /// Represent interface to manage data in local storage on device
    /// </summary>
    public interface ILocalDataManager
    {
        /// <summary>
        /// Set root path and root name, and invoke load method
        /// </summary>
        /// <param name="rootPath">Path to folder contained local data</param>
        /// <param name="rootName">Object name wich contains local data</param>
        void Init(string rootPath, string rootName);

        /// <summary>
        /// Save data in file in local storage, if file doesn't exist, create a new one
        /// </summary>
        void SaveData();

        /// <summary>
        /// Load data from file in local storage, if file doesn't exist, create empty instance of data
        /// </summary>
        void LoadData();

        /// <summary>
        /// Adding data to data root
        /// </summary>
        /// <param name="data">Data objects must be marked as [Serializeble]</param>
        void AddData(object data);
        
        /// <summary>
        /// Checks if Data present in data object
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        bool isContains(object data);

        /// <summary>
        /// Remove data from data object
        /// </summary>
        /// <param name="data"></param>
        void Remove(object data);

        /// <summary>
        /// Delete data file from device and clear data object
        /// </summary>
        void ClearData();

        /// <summary>
        /// Returns first entries of T in data object
        /// </summary>
        /// <typeparam name="T">Type of searching data</typeparam>
        /// <returns></returns>
        T GetData<T>();

        /// <summary>
        /// Returs all entries of T in data object
        /// </summary>
        /// <typeparam name="T">Type of searching data</typeparam>
        /// <returns></returns>
        T[] GetDataArray<T>();

    }
} 


