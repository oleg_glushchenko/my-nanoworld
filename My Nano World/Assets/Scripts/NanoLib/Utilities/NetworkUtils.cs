using System;
using System.Collections;
using NanoLib.Core.Logging;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.Networking;

namespace NanoLib.Utilities
{
    public static class NetworkUtils
    {
        private static readonly Dictionary<string, Sprite> _cachedSprites = new Dictionary<string, Sprite>();
        
        private const string OfferImagesLocation = @"https://mnwnanoreality.fra1.digitaloceanspaces.com/Offers/";
        
        public static void DownloadOfferSprite(int id, Action<Sprite> callback =null)
        {
            DownloadSprite($"{OfferImagesLocation}{id}.png", callback);
        }
        
        public static void DownloadSprite(string uri, Action<Sprite> callback =null)
        {
            Observable.FromCoroutine(() => Download(uri, callback)).Subscribe();
        }

        private static IEnumerator Download(string uri, Action<Sprite> callback)
        {
            if (_cachedSprites.TryGetValue(uri, out var cachedSprite))
            {
                callback?.Invoke(cachedSprite);
                yield break;
            }

            using var request = UnityWebRequestTexture.GetTexture(uri);

            yield return request.SendWebRequest();

            if (request.result != UnityWebRequest.Result.Success)
            {
                ($"Image {uri} not found, Error : {request.error}").LogError();
            }
            else
            {
                var texture = ((DownloadHandlerTexture) request.downloadHandler).texture;
                var sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0, 0));

                $"Sprite from URI {uri} loaded successfully".Log(LoggingChannel.Network);

                callback?.Invoke(sprite);

                if (!_cachedSprites.ContainsKey(uri))
                {
                    _cachedSprites.Add(uri, sprite);
                }
            }
        }
    }
}