﻿using System;
using Newtonsoft.Json;

namespace Assets.NanoLib.Utilities.Serializers
{
    public interface IStringSerializer
    {
        /// <summary>
        /// Serializes the specified object to a JSON string.
        /// </summary>
        /// <param name="value">The object to serialize.</param>
        /// <returns>A JSON string representation of the object.</returns>
        string Serialize(object value);

        
        /// <summary>
        /// Deserializes the JSON to the specified .NET type.
        /// </summary>
        /// <typeparam name="T">The type of the object to deserialize to.</typeparam>
        /// <param name="value">The JSON to deserialize.</param>
        /// <returns>The deserialized object from the Json string.</returns>
        object Deserialize(string value, Type T);

        TType Deserialize<TType>(string value);

        /// <summary>
        /// Deserializes the JSON to the specified .NET type.
        /// </summary>
        /// <param name="value">The JSON to deserialize.</param>
        /// <param name="resultAction">Action for result</param>
        void DeserializeAsync<TResult>(string value, Action<TResult> resultAction);

        /// <summary>
        /// Deserializes the JSON to the specified .NET type.
        /// </summary>
        /// <typeparam name="T">The type of the object to deserialize to.</typeparam>
        /// <param name="value">The JSON to deserialize.</param>
        /// <returns>The deserialized object from the Json string.</returns>
        object Deserialize(string value, Type T, JsonSerializerSettings settings);
    }
}
