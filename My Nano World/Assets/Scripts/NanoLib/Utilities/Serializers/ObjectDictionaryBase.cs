﻿using System;
using System.Collections.Generic;

using UnityEngine;

namespace nano_lib.Utilities.Serializers
{
    [Serializable]
    /// <summary>
    /// Для сериализации в инспектор Dictionary. Для каждого типа нужно
    /// наследоватся и пометить новий клас как [Serializable].
    /// Пример:
    ///  
    /// [Serializable]
    /// private class XXX : ObjectDictionaryBase<int, int> {}
    ///   
    ///  [SerializebleField]
    ///  private XXX _xxx;
    /// 
    /// </summary>
    public abstract class ObjectDictionaryBase<TObjectKey, TObjectType>
    {
        [SerializeField]
        private List<TObjectKey> _keys;

        [SerializeField]
        private List<TObjectType> _values;

        public List<TObjectKey> Keys
        {
            get { return _keys ?? (_keys = new List<TObjectKey>()); }
        }

        public List<TObjectType> Values
        {
            get { return _values ?? (_values = new List<TObjectType>()); }
        }

        public TObjectType this [TObjectKey key]
        {
            get { return Values[GetIndex(key)]; }

            set { Values[GetIndex(key)] = value; }
        }

        public bool ContainsKey(TObjectKey key)
        {
            return Keys.Contains(key);
        }

        public int Count()
        {
            return Values.Count;
        }

        public void Add(TObjectKey key, TObjectType objectType)
        {
            Keys.Add(key);
            Values.Add(objectType);
        }

        public void Remove(TObjectKey key, TObjectType objectType)
        {
            Keys.Remove(key);
            Values.Remove(objectType);
        }

        private int GetIndex(TObjectKey key)
        {
            if (Keys.Contains(key) == false)
            {
                Add(key, default(TObjectType));
            }

            return Keys.IndexOf(key);
        }
    }
}