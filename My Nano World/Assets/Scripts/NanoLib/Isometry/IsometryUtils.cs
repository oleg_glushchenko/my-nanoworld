﻿using System.Collections.Generic;
using UnityEngine;

namespace NanoReality.Isometry
{
    /// <summary>
    /// Provides isometric depth sorting algorithm.
    /// Based on:
    /// https://shaunlebron.github.io/IsometricBlocks/
    /// https://mazebert.com/2013/04/18/isometric-depth-sorting/
    /// </summary>
    public static class IsometryUtils
    {
        /// <summary>
        /// Sorts objects via isometric depth sorting algorithms. Returns max depth value.
        /// </summary>
        /// <param name="objects"></param>
        /// <returns>max depth value</returns>
        public static int SortObjects<T>(IList<T> objects) where T : class, IIsometryObject
        {
            if (objects.Count <= 0) return 0;

            for (var i = 0; i < objects.Count; i++)
            {
                var isometryObject = objects[i];
                isometryObject.ObjectsBehind.Clear();
            }

            // Determine dependencies for the topological graph sort
            for (int i = 0; i < objects.Count; i++)
            {
                if (objects[i].IgnoreDepthSort) continue;
                for (int j = i + 1; j < objects.Count; j++)
                {
                    if (objects[j].IgnoreDepthSort) continue;
                    var a = (T)GetFrontObject(objects[i], objects[j]);

                    if (a == objects[i])
                    {
                        objects[i].ObjectsBehind.Add(objects[j]);
                    }
                    else if (a == objects[j])
                    {
                        objects[j].ObjectsBehind.Add(objects[i]);
                    }
                }
                objects[i].IsVisited = false;
            }

            // Do a topological sort on the dependency graph
            int sortDepth = 0;
            for (var i = 0; i < objects.Count; i++)
            {
                var isometryObject = objects[i];
                if (isometryObject.IgnoreDepthSort) continue;
                SetObjectDepth(isometryObject, ref sortDepth);
            }

            // Apply isometric depth to objects
            for (int i = 0; i < objects.Count; i++)
            {
                objects[i].SortObject();
            }

            return sortDepth;
        }

        // Recursively sets depth for the object and objects behind it.
        private static void SetObjectDepth(IIsometryObject isometryObject, ref int sortDepth)
        {
            if (!isometryObject.IsVisited)
            {
                isometryObject.IsVisited = true;

                for (int i = 0; i < isometryObject.ObjectsBehind.Count; ++i)
                {
                    if (isometryObject.ObjectsBehind[i] == null) break;
                    SetObjectDepth(isometryObject.ObjectsBehind[i], ref sortDepth);
                    isometryObject.ObjectsBehind[i] = null;
                }

                if (!isometryObject.IgnoreDepthSort)
                {
                    isometryObject.IsoDepth = sortDepth;
                    sortDepth += Mathf.RoundToInt((isometryObject.IsoSize.x + isometryObject.IsoSize.y) * 0.5f * Mathf.Sin(Mathf.Deg2Rad * 30f));
                }
            }
        }

        /// <summary>
        /// Determines if objects overlap each other on screen.
        /// If they do, then return the object that will appear in front.
        /// Otherwise, returns null.
        /// </summary>
        /// <param name="a">object</param>
        /// <param name="b">object</param>
        /// <returns></returns>
        private static IIsometryObject GetFrontObject(IIsometryObject a, IIsometryObject b)
        {
            if (DoObjectsOverlap(a, b))
            {
                var box1 = a.IsoBox;
                var box2 = b.IsoBox;

                // test for intersection x-axis
                // (lower x value is in front)
                if (box1.XMin >= box2.XMax) return b;
                if (box2.XMin >= box1.XMax) return a;

                // test for intersection y-axis
                // (lower y value is in front)
                if (box1.YMin >= box2.YMax) return b;
                if (box2.YMin >= box1.YMax) return a;

                // test for intersection z-axis
                // (higher z value is in front)
                if (box1.ZMin >= box2.ZMax) return b;
                if (box2.ZMin >= box1.ZMax) return a;
            }

			return null;
        }

        /// <summary>
        /// Returns true if two hexagons(objects) overlap.
        /// </summary>
        /// <param name="a">object</param>
        /// <param name="b">object</param>
        /// <returns></returns>
        private static bool DoObjectsOverlap(IIsometryObject a, IIsometryObject b)
        {
            var hex1 = a.IsoHexagon;
            var hex2 = b.IsoHexagon;

            return
                // test if x regions intersect.
                !(hex1.XMin >= hex2.XMax || hex2.XMin >= hex1.XMax) &&
                // test if y regions intersect.
                !(hex1.YMin >= hex2.YMax || hex2.YMin >= hex1.YMax) &&
                // test if h regions intersect.
                !(hex1.HMin >= hex2.HMax || hex2.HMin >= hex1.HMax);
        }
    }


    public struct IsoVertex
    {
        public readonly float X;
        public readonly float Y;
        public readonly float H;
        public readonly float V;

        public IsoVertex(float x, float y, float z)
        {
            var isoX = x + z;
            var isoY = y + z;
            X = isoX;
            Y = isoY;
            H = (isoX - isoY) * Mathf.Cos(Mathf.Deg2Rad * 30f);
            V = (isoX + isoY) * 0.5f;
        }
    }

    public struct IsoCube
    {
        public readonly IsoVertex RightDown;
        public readonly IsoVertex LeftDown;
        public readonly IsoVertex BackDown;
        public readonly IsoVertex FrontDown;
        public readonly IsoVertex RightUp;
        public readonly IsoVertex LeftUp;
        public readonly IsoVertex BackUp;
        public readonly IsoVertex FrontUp;

        public IsoCube(Vector3 p, Vector3 s)
        {
            RightDown = new IsoVertex(p.x + s.x, p.y, p.z);

            LeftDown = new IsoVertex(p.x, p.y + s.y, p.z);

            BackDown = new IsoVertex(p.x + s.x, p.y + s.y, p.z);

            FrontDown = new IsoVertex(p.x, p.y, p.z);

            RightUp = new IsoVertex(p.x + s.x, p.y, p.z + s.z);

            LeftUp = new IsoVertex(p.x, p.y + s.y, p.z + s.z);

            BackUp = new IsoVertex(p.x + s.x, p.y + s.y, p.z + s.z);

            FrontUp = new IsoVertex(p.x, p.y, p.z + s.z);
        }
    }

    public struct IsoBox
    {
        public readonly float XMin;
        public readonly float XMax;
        public readonly float YMin;
        public readonly float YMax;
        public readonly float ZMin;
        public readonly float ZMax;

        public IsoBox(Vector3 position, Vector3 size)
        {
            XMin = position.x;
            XMax = position.x + size.x;
            YMin = position.y;
            YMax = position.y + size.y;
            ZMin = position.z;
            ZMax = position.z + size.z;
        }
    }

    public struct IsoHexagon
    {
        public readonly float XMin;
        public readonly float XMax;
        public readonly float YMin;
        public readonly float YMax;
        public readonly float HMin;
        public readonly float HMax;

        public IsoHexagon(Vector3 position, Vector3 size)
        {
            var cube = new IsoCube(position, size);

            XMin = cube.FrontDown.X;
            XMax = cube.BackUp.X;
            YMin = cube.FrontDown.Y;
            YMax = cube.BackUp.Y;
            HMin = cube.LeftDown.H;
            HMax = cube.RightDown.H;
        }
    }
}
