﻿using Assets.NanoLib.UI.Core.Components;
using Assets.NanoLib.UtilitesEngine;
using Assets.NanoLib.Utilities.Pulls;
using strange.extensions.signal.impl;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Assets.NanoLib.UI.Extensions
{
    public class SignalOnNextPageShow : Signal<int> { }

    public class SignalOnPrevPageShow : Signal<int> { }

    /// <summary>
    /// Вызывается, когда обновляется кол-во страниц
    /// </summary>
    public class SignalOnPaginatorInited : Signal<int> { }
    
    /// <summary>
    /// Вызывается, когда переключается страница
    /// </summary>
    public class SignalOnPaginatorSwitched : Signal<bool, bool> { }

    /// <summary>
    /// Scroll snap for page by page scroll effect, based on width related GameObject.
    /// Opional. Can change pages with buttons and show current page with "dots" paginator
    /// </summary>
    [AddComponentMenu("UI/Extensions/Horizontal Scroll Snap")]
    public class HorizontalScrollSnap : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler
    {
        public SignalOnNextPageShow SignalOnNextPageShow
        {
            get { return _signalOnNextPageShow; }
            set { _signalOnNextPageShow = value; }
        }

        public SignalOnPrevPageShow SignalOnPrevPageShow
        {
            get { return _signalOnPrevPageShow; }
            set { _signalOnPrevPageShow = value; }
        }

        public SignalOnPaginatorInited SignalOnPaginatorInited
        {
            get { return _signalOnPaginatorInited; }
            set { _signalOnPaginatorInited = value; }
        }

        public SignalOnPaginatorSwitched SignalOnPaginatorSwitched
        {
            get { return _signalOnPaginatorSwitched; }
            set { _signalOnPaginatorSwitched = value; }
        }
        private Transform _screensContainer;

        private int _screens = 0;

        private System.Collections.Generic.List<Vector3> _positions;
        private Vector3 _lerpTarget;
        private bool _lerp;

        private bool _isInited = false;

        [Tooltip("The gameobject that contains toggles which suggest pagination. (optional)")] public ItemContainer
            Pagination;

        [Tooltip("Toggle prefab that indicates current page")] public Toggle
            PrefabPageIndicator;

        [Tooltip("Button to go to the next page. (optional)")] public GameObject NextButton;
        [Tooltip("Button to go to the previous page. (optional)")] public GameObject PrevButton;

        private SignalOnPrevPageShow _signalOnPrevPageShow = new SignalOnPrevPageShow();
        private SignalOnNextPageShow _signalOnNextPageShow = new SignalOnNextPageShow();
        private SignalOnPaginatorInited _signalOnPaginatorInited = new SignalOnPaginatorInited();
        private SignalOnPaginatorSwitched _signalOnPaginatorSwitched = new SignalOnPaginatorSwitched();

        private bool _startDrag = true;
        private int _currentScreen;
        private int _currentPagesCount;

        private Vector3 _containerStartPosition;

        public void OnEnable()
        {
            if (NextButton)
                NextButton.GetComponent<Button>().onClick.AddListener(NextScreen);

            if (PrevButton)
                PrevButton.GetComponent<Button>().onClick.AddListener(PrevScreen);
        }

        public void OnDisable()
        {
            if (NextButton)
                NextButton.GetComponent<Button>().onClick.RemoveListener(NextScreen);

            if (PrevButton)
                PrevButton.GetComponent<Button>().onClick.RemoveListener(PrevScreen);
        }

        // Use this for initialization
        public void InitScreen(int totalPagesCount)
        {
            _screensContainer = gameObject.transform.GetChild(0);

            if (!_isInited)
            {
                _containerStartPosition = _screensContainer.localPosition;
            }
            
            //сбрасываем контейнер на первую страницу, для избежания бага с некорректно отображаемыми элементами
            _currentScreen = 0;
            _screensContainer.localPosition = _containerStartPosition;

            _isInited = true;

            float width = gameObject.transform.parent.GetComponent<RectTransform>().rect.width;
            
            DistributePages();
            InitPaginator(totalPagesCount);

            _screens = totalPagesCount;
            _lerp = false;
            _positions = new System.Collections.Generic.List<Vector3>();
            
            //init positions of screen based on width of _screensContainer
            if (_screens > 0)
            {
                for (int i = 0; i < _screens; ++i)
                {
                    _positions.Add(_containerStartPosition + Vector3.left * (width * i));
                }
            }

            UpdatePagination(_currentScreen);
        }

        //changes the bullets on the bottom of the page - pagination
        public void UpdatePagination(int currentScreen)
        {
            Transform scrollView = Pagination.transform.GetChild(0);
            if (scrollView)
            {
                for (int i = 0; i < scrollView.transform.childCount; i++)
                {
                    scrollView.transform.GetChild(i).GetComponent<Toggle>().isOn = (currentScreen == i);
                }
                _signalOnPaginatorSwitched.Dispatch(currentScreen > 0, currentScreen < _currentPagesCount - 1);
            }
        }

        public void InitPaginator(int newTotalPagesCount)
        {
            _currentPagesCount = newTotalPagesCount;

            if (!Pagination.IsInstantiateed)
            {
                var gameObjectPullable = PrefabPageIndicator.GetComponent<GameObjectPullable>();
                if (gameObjectPullable == null)
                    gameObjectPullable = PrefabPageIndicator.gameObject.AddComponent<GameObjectPullable>();
                Pagination.InstaniteContainer(gameObjectPullable, 0);
            }

            int dotsCount = Pagination.CurrentItems.Count;

            if (dotsCount < newTotalPagesCount)
                for (int i = dotsCount; i < newTotalPagesCount; i++)
                    Pagination.AddItem();
            else
                for (int i = dotsCount; i > newTotalPagesCount; i--)
                    Pagination.RemoveItem(i - 1);

            _signalOnPaginatorInited.Dispatch(newTotalPagesCount);
        }

        void Update()
        {
            if (_lerp && _screens > 1)
            {
                _screensContainer.localPosition = Vector3.Lerp(_screensContainer.localPosition, _lerpTarget,
                    13f*Time.deltaTime);
                if (Vector3.Distance(_screensContainer.localPosition, _lerpTarget) < 0.1f)
                {
                    _lerp = false;
                    _screensContainer.localPosition = _lerpTarget;
                }

                //change the info bullets at the bottom of the screen. Just for visual effect
                if (Vector3.Distance(_screensContainer.localPosition, _lerpTarget) < 10f)
                {
                    UpdatePagination(_currentScreen);
                }
            }
        }

        private void NextScreen()
        {
            _lerp = true;
            if (_currentScreen < _screens - 1)
            {
                _lerpTarget = _positions[++_currentScreen];

                UpdatePagination(_currentScreen);
                _signalOnNextPageShow.Dispatch(_currentScreen);
            }
        }

        private void PrevScreen()
        {
            _lerp = true;
            if (_currentScreen > 0)
            {
                _lerpTarget = _positions[--_currentScreen];

                UpdatePagination(_currentScreen);
                _signalOnPrevPageShow.Dispatch(_currentScreen);
            }
        }

        //used for changing between screen resolutions
        private void DistributePages()
        {
            int _offset = 0;
            int _step = Screen.width;
            int _dimension = 0;

            int currentXPosition = 0;

            for (int i = 0; i < _screensContainer.transform.childCount; i++)
            {
                RectTransform child =
                    _screensContainer.transform.GetChild(i).gameObject.GetComponent<RectTransform>();
                currentXPosition = _offset + i*_step;
                child.anchoredPosition = new Vector2(currentXPosition, 0f);
                child.sizeDelta = new Vector2(gameObject.GetComponent<RectTransform>().sizeDelta.x,
                    gameObject.GetComponent<RectTransform>().sizeDelta.y);
            }

            _dimension = currentXPosition + _offset*-1;

            _screensContainer.GetComponent<RectTransform>().offsetMax = new Vector2(_dimension, 0f);
        }

        #region Interfaces

        public void OnBeginDrag(PointerEventData eventData)
        {
            _lerp = false;
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            _startDrag = true;
            if (eventData.delta.normalized.x <= -0.3f)
            {
                NextScreen();
            }
            else if (eventData.delta.normalized.x >= 0.3f)
            {
                PrevScreen();
            }
            else
            {
                _lerp = true;
            }
        }

        public void OnDrag(PointerEventData eventData)
        {
            _lerp = false;
            if (_screens == 1)
                return;

            _screensContainer.localPosition += new Vector3(eventData.delta.magnitude * Mathf.Sign(eventData.delta.x), 0, 0);
            if (_startDrag)
            {
                OnBeginDrag(eventData);
                _startDrag = false;
            }
        }

        #endregion
    }
}