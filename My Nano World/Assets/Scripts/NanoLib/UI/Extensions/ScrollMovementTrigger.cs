﻿
using UnityEngine;
using UnityEngine.UI;

namespace Assets.nano_lib.UI.Extensions
{
    /// <summary>
    /// Change ScrollRect movement type: if scroll view size is greater than container size - Elastic otherwise - Clamped
    /// also calculate Scrollview size for flexible Grid Layout, it calculates wrong with ContentSizeFitter
    /// </summary>
    [RequireComponent(typeof(ScrollRect))]
    public class ScrollMovementTrigger : MonoBehaviour
    {
        [Tooltip("If true - lock width of inner scrollview to current scroll rect, otherwise - lock height, available only for Grid Layout")]
        public bool LockWidth;

         [Tooltip("Expand scrollview to container size if it less than container size")]
        public bool ExpandScrollView;

        public bool IsScrollOverContainer
        {
            get { return _isScrollOverContainer; }
        }
			

        private RectTransform _containerTransform;

        private RectTransform _scrollViewTransform;

        private ScrollRect _scrollRect;

        private bool _isScrollOverContainer;

        private int _itemsPerContainerSize;

        private int _currentHorizontalItemsCount;

        private int _currentVerticalItemsCount;

        void Awake()
        {
            _containerTransform = gameObject.GetComponent<RectTransform>();
            _scrollRect = gameObject.GetComponent<ScrollRect>();
            _scrollViewTransform = transform.GetChild(0).GetComponent<RectTransform>();
        }

        void OnEnable()
        {
           ChangeMovementTypeIfNeeded();
        }

        void LateUpdate()
        {
            var activeChildsCount = 0;
            _currentHorizontalItemsCount = 0;
            _currentVerticalItemsCount = 0;

            Vector2 firstPosition = Vector2.zero; 

            for (int i = 0; i < transform.GetChild(0).childCount; i++)
            {
                if (i == 0)
                    firstPosition = transform.GetChild(0).GetChild(i).transform.position;
                if (transform.GetChild(0).GetChild(i).gameObject.activeInHierarchy)
                {
                    activeChildsCount++;
                    if (firstPosition.x == transform.GetChild(0).GetChild(i).position.x)
                        _currentHorizontalItemsCount++;
                    if (firstPosition.y == transform.GetChild(0).GetChild(i).position.y)
                        _currentVerticalItemsCount++;
                }
            }
               ChangeMovementTypeIfNeeded();
        }

        private void ChangeMovementTypeIfNeeded()
        {
            GridLayoutGroup gridLayoutGroup = transform.GetChild(0).GetComponent<GridLayoutGroup>();

            float scrollWidth = 0f, scrollHeight = 0f;

            if (gridLayoutGroup)
            {
                Vector2 totalItemDimensions = new Vector2(gridLayoutGroup.cellSize.x + gridLayoutGroup.spacing.x,
                    gridLayoutGroup.cellSize.y + gridLayoutGroup.spacing.y);
                //reset alognment for correct set of sizeDelta
                var tmpAlignment = gridLayoutGroup.childAlignment;
                gridLayoutGroup.childAlignment = TextAnchor.UpperLeft;

                if (!LockWidth)
                {
                    scrollWidth = ((totalItemDimensions.x*_currentVerticalItemsCount) +
                                   gridLayoutGroup.padding.horizontal);
                    if (_containerTransform.rect.width > scrollWidth && ExpandScrollView)
                        scrollWidth = _containerTransform.rect.width;
                    scrollHeight = _containerTransform.rect.height;
                }
                else
                {
                    scrollHeight = ((totalItemDimensions.y*_currentHorizontalItemsCount) +
                                    gridLayoutGroup.padding.vertical);
                    if (_containerTransform.rect.height > scrollHeight && ExpandScrollView)
                        scrollHeight = _containerTransform.rect.height;
                    scrollWidth = _containerTransform.rect.width;
                }

                _scrollViewTransform.sizeDelta = new Vector2(scrollWidth - _containerTransform.rect.width, scrollHeight - _containerTransform.rect.height);
                gridLayoutGroup.childAlignment = tmpAlignment;
            }
            else
            {
                scrollWidth = _scrollViewTransform.rect.width;
                scrollHeight = _scrollViewTransform.rect.height;
            }

            if (_containerTransform.rect.width < scrollWidth || _containerTransform.rect.height < scrollHeight)
            {
                _scrollRect.enabled = true;
                _isScrollOverContainer = true;
            }
            else
            {
                _scrollRect.enabled = false;
                _isScrollOverContainer = false;
            }
        }
    }
}
