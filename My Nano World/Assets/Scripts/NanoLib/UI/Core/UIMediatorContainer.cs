﻿using System;
using System.Collections.Generic;
using Assets.NanoLib.UI.Core.Signals;
using NanoLib.Core.Logging;
using strange.extensions.mediation.impl;

namespace NanoLib.UI.Core
{
    public class UIMediatorContainer : MediatorContainer
    {
        [Inject] public ShowWindowSignal ShowWindowSignal { get; set; }
        [Inject] public HideWindowSignal HideWindowSignal { get; set; }

        private readonly Dictionary<Type, IUIMediator> _mediators = new Dictionary<Type, IUIMediator>();

        [PostConstruct]
        public void PostConstruct()
        {
            ShowWindowSignal.AddOnce(InitMediators);
            HideWindowSignal.AddListener(HandleHide);
        }

        [Deconstruct]
        protected override void OnDestroy()
        {
            base.OnDestroy();
            
            ShowWindowSignal.RemoveListener(HandleShow);
            HideWindowSignal.RemoveListener(HandleHide);

            foreach (var mediatorPair in _mediators) 
                mediatorPair.Value.Hide();

            _mediators.Clear();
        }
        
        private void InitMediators(Type type, object args)
        {
            var components = GetComponents<IUIMediator>();
            foreach (var mediator in components) 
                _mediators.Add(mediator.GetViewType(), mediator);

            ShowWindowSignal.AddListener(HandleShow);
            HandleShow(type, args);
        }
        
        private void HandleShow(Type type, object args)
        {
            if (!_mediators.ContainsKey(type))
            {
                $"There is no such type of panel {type.FullName} in the dictionary".LogError(LoggingChannel.Ui);
                return;
            }

            try
            {
                _mediators[type].HandleShow(type, args);
            }
            catch (Exception exception)
            {
                exception.LogError(LoggingChannel.Ui);
            }
        }

        private void HandleHide(Type type)
        {
            if (!_mediators.ContainsKey(type))
            {
                $"There is no such type of panel {type.FullName} in the dictionary".LogError(LoggingChannel.Ui);
                return;
            }

            try
            {
                _mediators[type].Hide(type);
            }
            catch (Exception exception)
            {
                exception.LogError(LoggingChannel.Ui);
            }
        }
    }
}