﻿using strange.extensions.mediation.impl;

namespace NanoReality.StrangeIoC
{
    public class AMediator<T> : Mediator where T : View
    {
        [Inject]
        public T View { get; set; }
    }
}
