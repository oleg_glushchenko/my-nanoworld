﻿using System;
using System.Collections.Generic;
using NanoReality.Game.UI.FlyingBalanceItemPanel;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace NanoLib.UI.Core
{
    public interface IUIManager
    {
        List<KeyValuePair<Type, View>> ActiveUi { get; }
        
        RectTransform OverlayCanvasTransform { get; }
        FlyingBalanceItemPanel FlyingPanel { get; }
        
        /// <summary>
        /// Gets UiPanelView from Cache or creates panel instance.
        /// </summary>
        /// <typeparam name="T">Returns view instance.</typeparam>
        /// <returns></returns>
        T GetView<T>() where T : View;
        
        void CloseView<T>() where T : View;

        T GetActiveView<T>() where T : View;
        
        void HideAll(bool isHardClose = false);
        
        bool IsNotDefaultViewVisible();
        
        bool IsViewVisible(Type type);
        
        void LoadView(Type type);
        
        /// <summary>
        /// Set view on the top layer in panels hierarchy.
        /// Generic variation of SetViewLastSibling(Type).
        /// </summary>
        /// <typeparam name="T">Type inherited from View.</typeparam>
        void SetViewLastSibling<T>();

        /// <summary>
        /// Set view on the top layer in panels hierarchy.
        /// </summary>
        /// <param name="viewType">Type inherited from View.</param>
        void SetViewLastSibling(Type viewType);

        void HideLastActivePanel();
    }
}
