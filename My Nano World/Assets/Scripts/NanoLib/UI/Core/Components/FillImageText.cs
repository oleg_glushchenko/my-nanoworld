using System;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace NanoLib.UI.Components
{
    public sealed class FillImageText : UIBehaviour
    {
        [SerializeField] private TextMeshProUGUI _targetText;
        [SerializeField] private Image _targetFillImage;
        [Tooltip("Minimal text width when it visible")]
        [SerializeField] private float _minimalTextWidth;
        [SerializeField] private bool _fillImageText;
        private RectTransform _imageRectTransform;
        private RectTransform _textRectTransform;
        private IDisposable _subscription;

        protected override void Awake()
        {
            base.Awake();
            _imageRectTransform = (RectTransform) _targetFillImage.transform;
            _textRectTransform = (RectTransform) _targetText.transform;
            _subscription = _targetFillImage.ObserveEveryValueChanged(c => c.fillAmount).Subscribe(OnFillAmountChanged);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            _subscription?.Dispose();
        }

        private void OnFillAmountChanged(float fillAmount)
        {
            var imageRectSize = _imageRectTransform.rect;
            var filledImageWidth = imageRectSize.width * fillAmount;


            bool isTextVisible = filledImageWidth >= _minimalTextWidth;
            _targetText.gameObject.SetActive(isTextVisible);

            // If the object is invisible, then it makes no sense to change its RectTransform and redraw the parent Canvas.
            if (!isTextVisible)
            {
                return;
            }

            if (_fillImageText)
            {
                var textSizeDelta = _textRectTransform.sizeDelta;
                textSizeDelta.x = filledImageWidth;
                _textRectTransform.sizeDelta = textSizeDelta;
            }
            
            var textPosition = _textRectTransform.anchoredPosition;
            textPosition.x = filledImageWidth;
            _textRectTransform.anchoredPosition = textPosition;
        }
    }
}