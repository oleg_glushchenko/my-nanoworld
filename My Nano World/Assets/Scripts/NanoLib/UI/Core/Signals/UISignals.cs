﻿using System;
using Assets.NanoLib.UI.Core.Views;
using NanoReality.GameLogic.Achievements.impl;
using strange.extensions.signal.impl;
using UnityEngine;


namespace Assets.NanoLib.UI.Core.Signals
{
    // TODO: need push new user level value.
    public class UserLevelUpSignal : Signal { }

    public class SignalOnShownPanel : Signal<UIPanelView> { }
    public class SignalOnHidePanel : Signal<UIPanelView> { }

	public class PlayerOpenedPanelSignal : Signal { }
    public class SignalOnRewardClaimed : Signal<Achievement, Transform> { }
    public class SignalOnSkipPriceChanged : Signal<int> { };
    public class SignalDisableStandardUICanvas : Signal { }
    public class SignalBlockExitGamePopup : Signal { }
    public class SignalUnBlockExitGamePopup : Signal { }
    public class WindowClosedSignal : Signal<Type>{}
    public class WindowOpenedSignal : Signal<Type>{}
    
    /// <summary>
    /// Sets panel of give type on the top of panels hierarchy.
    /// </summary>
    public class SetWindowLastSiblingSignal : Signal<Type> {}
    public class HideWindowSignal : Signal<Type> {}
    public class ShowWindowSignal : Signal<Type, object> {}
    public class InitFlyingPanelSignal: Signal {}
}