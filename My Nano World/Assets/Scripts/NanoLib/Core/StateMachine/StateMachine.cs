﻿using System;
using System.Collections.Generic;
using System.Linq;
using NanoLib.Core.Logging;

namespace NanoLib.Core.StateMachine
{
    public sealed class StateMachine : IStateMachine
    {
        private readonly List<IState> _stateList;
        private IState _currentState;

        public IState CurrentState => _currentState;

        public StateMachine()
        {
            _stateList = new List<IState>();
        }

        public void Start()
        {
            var initialState = _stateList.FirstOrDefault();
            if(initialState == null)
                throw new ArgumentException("[StateMachine] StateMachine not contains any state.");

            ApplyState(initialState);
        }

        public void Stop()
        {
            CurrentState?.OnStateExit();
            _currentState = null;
        }

        public void Dispose()
        {
            Stop();
            _currentState = null;
            _stateList.Clear();
        }

        public IStateMachine AddNewState(IState state)
        {
            _stateList.Add(state);

            return this;
        }

        private void ApplyState(IState state)
        {
            _currentState?.OnStateExit();

            _currentState = state;
            _currentState.OnStateEnter();
        }

        public void HandleEvent(IStateMachineEvent transitionEvent)
        {
            var comparedTransition = CurrentState.Transitions.FirstOrDefault(c => c.TriggerEvent.EventIdentifier.Equals(transitionEvent.EventIdentifier));
            if (comparedTransition == null)
            {
                Logging.Logging.LogError(LoggingChannel.Core,
                    $"[StateMachine] Cant do transition from state: {CurrentState.GetType().Name} using event {transitionEvent.EventName}. Transition didn't bind");
                return;
            }

            var nextState = comparedTransition.NextState;
            
            Logging.Logging.Log(LoggingChannel.Core, $"[StateMachine] Handle event: [{transitionEvent.EventName}] from: [{CurrentState.GetType().Name}] to: [{nextState.GetType().Name}]");
            
            ApplyState(nextState);
        }
    }
}