namespace NanoLib.Core.StateMachine
{
    public sealed class StateTransition : IStateTransition
    {
        public IState PreviousState { get; }

        public IState NextState { get; }

        public IStateMachineEvent TriggerEvent { get; }

        public StateTransition(IState parentState, IState targetState,
            IStateMachineEvent triggerEvent)
        {
            PreviousState = parentState;
            NextState = targetState;
            TriggerEvent = triggerEvent;
        }
    }
}