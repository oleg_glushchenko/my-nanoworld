namespace NanoLib.Core.StateMachine
{
    public interface IStateTransition
    {
        IState PreviousState { get; }

        IState NextState { get; }

        IStateMachineEvent TriggerEvent { get; }
    }
}