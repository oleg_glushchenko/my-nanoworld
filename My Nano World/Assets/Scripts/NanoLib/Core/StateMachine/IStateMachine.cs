namespace NanoLib.Core.StateMachine
{
    public interface IStateMachine
    {
        IState CurrentState { get; }

        IStateMachine AddNewState(IState state);

        void Start();

        void Stop();
        
        void Dispose();

        void HandleEvent(IStateMachineEvent transitionEvent);
    }
}