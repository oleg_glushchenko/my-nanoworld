using System;

namespace Assets.NanoLib.Utilities.Pulls
{
    [Obsolete("Use IUnityPool instead")]
    public interface IObjectPullWithLevel : IObjectsPull
    {
        int Level { get; set; }
    }
}