﻿using UnityEngine;

namespace Assets.NanoLib.Utilities.Pulls
{
    /// <summary>
    /// Base Monobehavior implementation of IPullableObject
    /// </summary>
    public class GameObjectPullable : MonoBehaviour, IPullableObject
    {
        /// <summary>
        /// Caching game object to avoid extra colls GetComponent
        /// </summary>
        public virtual GameObject CacheGameObject
        {
            get { return _cacheGameObject ?? (_cacheGameObject = gameObject); }
        }

        private GameObject _cacheGameObject;


        public virtual object Clone()
        {
            return Instantiate(this);
        }


        /// <summary>
        /// Source to pull in which object back after using
        /// </summary>
        public virtual IObjectsPull pullSource { get; set; }

        /// <summary>
        /// Calls before poping from pull. You can initialize custom object settings here
        /// </summary>
        public virtual void OnPop()
        {
            if (CacheGameObject == null)
                Debug.LogWarning("Can't get gameObject, seems to object has been destroyed just now");
            else
            {
                this.enabled = true;
                CacheGameObject.SetActive(true);
            }
        }

        /// <summary>
        /// Calls after returning to pull. In this function you should reset object for next using
        /// </summary>
        public virtual void OnPush()
        {
            if (CacheGameObject == null)
                Debug.LogWarning("Can't get gameObject, seems to object has been destroyed just now");
            else
            {
                this.enabled = false;
                CacheGameObject.SetActive(false);
            }
        }

        /// <summary>
        /// Return object to source pull
        /// </summary>
        public virtual void FreeObject()
        {
            if (pullSource != null)
                pullSource.PushInstance(this);
            else
            {
                DestroyObject();
            }
        }

        /// <summary>
        /// Calls when pull must be cleaned up
        /// </summary>
        public virtual void DestroyObject() => Destroy(gameObject);
    }

}

