﻿using System;

namespace Assets.NanoLib.Utilities.Pulls
{
    /// <summary>
    /// Reprsent base pullable object
    /// </summary>
    [Obsolete("Use IPoolable with IUnityPool instead")]
    public interface IPullableObject:ICloneable
    {
        /// <summary>
        /// Source to pull in which object back after using
        /// </summary>
        IObjectsPull pullSource { get; set; }

        /// <summary>
        /// Calls before poping from pull. You can initialize custom object settings here
        /// </summary>
        void OnPop();

        /// <summary>
        /// Calls after returning to pull. In this function you should reset object for next using
        /// </summary>
        void OnPush();

        /// <summary>
        /// Return object to source pull
        /// </summary>
        void FreeObject();

        /// <summary>
        /// Calls when pull must be cleaned up
        /// </summary>
        void DestroyObject();


    }
}
