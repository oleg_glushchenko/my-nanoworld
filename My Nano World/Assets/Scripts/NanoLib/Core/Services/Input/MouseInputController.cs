﻿using System;
using strange.extensions.mediation.impl;
using strange.extensions.signal.impl;
using UnityEngine;
using UnityEngine.EventSystems;

namespace NanoLib.Services.InputService
{
    /// <summary>
    /// Контроллер управления мышью
    /// </summary>
    public class MouseInputController : View, IInputController
    {
        /// <summary>
        /// Сигнал тапа (щелчка)
        /// float - время удержания тапа
        /// GameObject - текущий выбранный объект
        /// </summary>
        public Signal<float, GameObject> OnTapSignal { get; private set; }
        
        [SerializeField]
        private float _swipeTolerance = 0.02f;

        /// <summary>
        /// Текущая позиция тача
        /// </summary>
        private Vector3 _currentTouchPosition;

        /// <summary>
        /// Позиция последнего нажатия
        /// </summary>
        private Vector3 _startTouchPosition;

        /// <summary>
        /// Стартовое время нажатия (для определения режима выбора здания)
        /// </summary>
        private DateTime _startTouchTime;
        
        /// <summary>
        /// true в тех случаях, когда в данный момент происходит свайп. Необходимо для предотвращения тапа во время свайпа
        /// </summary>
        private bool _atLeastOneSwipeHappened;

        private bool _isPinchStarted = false;

        private float _lastPinchEndTime;

        public Vector3 CurrentTouchPosition => _currentTouchPosition;

        public Vector3 CurrentFingerPosition => Input.mousePosition;

        private float PinchValue => Input.GetAxis("Mouse ScrollWheel");

        private bool IsHavePinch => Math.Abs(PinchValue) > 0 && 
                                    _lastPinchEndTime < Time.timeSinceLevelLoad - 0f;

        private EventSystem EventSystemManager => EventSystem.current;

        /// <summary>
        /// Инициализация
        /// </summary>
        protected override void Start()
        {
            base.Start();
            ClearAll();
        }

        /// <summary>
        /// Инициализация после инжектов
        /// </summary>
        [PostConstruct]
        public void PostConstruct()
        {
            OnTapSignal = new Signal<float, GameObject>();
        }
        
        /// <summary>
        /// Очистка состояний управления
        /// </summary>
        private void ClearAll()
        {
            _startTouchPosition = Vector3.zero;
            _currentTouchPosition = Vector3.zero;
            _atLeastOneSwipeHappened = false;
            _startTouchTime = DateTime.MaxValue;
        }

        /// <summary>
        /// Проверка события тача в текущий момент
        /// </summary>
        /// <param name="callback">Коллбек обработки (параметры: начальное время нажатия, было ли это первое касание, было ли нажатие на UI)</param>
        public void CheckForTouch(Action<DateTime, bool, bool> callback)
        {
            if (!Input.GetMouseButton(0)) return;

            callback(_startTouchTime, _atLeastOneSwipeHappened, EventSystemManager.IsPointerOverGameObject());
        }

        /// <summary>
        /// Проверка события начала тача
        /// </summary>
        /// <param name="callback">Коллбек обработки (параметры: координаты нажатия, было ли нажатие на UI)</param>
        public void CheckForTouchDown(Action<Vector3, bool> callback)
        {
            // было ли начато нажатие
            if (!Input.GetMouseButtonDown(0)) return;

            // запоминаем где начали нажимать
            _startTouchPosition = Input.mousePosition;
            _currentTouchPosition = _startTouchPosition;

            // запоминаем время нажатия
            _startTouchTime = DateTime.Now;

            // т.к. мы только что прикоснулись к экрану - ни одного свайпа ещё 100% не произошло
            _atLeastOneSwipeHappened = false;

            callback(_startTouchPosition, EventSystemManager.IsPointerOverGameObject());
        }

        /// <summary>
        /// Проверка события окончания тача
        /// </summary>
        /// <param name="callback">Коллбек окончания нажатия</param>
        public void CheckForTouchRelease(Action<bool> callback)
        {
            if (!Input.GetMouseButtonUp(0)) return;

            bool isSwipe = _atLeastOneSwipeHappened;
           
            if (!isSwipe)
            {
                OnTapSignal.Dispatch((float)(DateTime.Now - _startTouchTime).TotalMilliseconds, EventSystemManager.currentSelectedGameObject);
            }

            ClearAll();

            callback.Invoke(isSwipe);
        }

           /// <summary>
        /// Просчитываем щипок
        /// </summary>
        /// <param name="callback">Коллбек обработки (параметр: смещение "щипка")</param>
        public void CalculatePinch(Action<float> callback)
        {
            if (IsHavePinch)
            {
                _isPinchStarted = true;
                callback(PinchValue);
            }
        }

        public void CalculatePitchEnd(Action callback)
        {
            if (_isPinchStarted && !Input.GetKey(KeyCode.LeftShift) && !IsHavePinch)
            {
                _lastPinchEndTime = Time.timeSinceLevelLoad;
                _isPinchStarted = false;
                callback();
            }
        }

        public void OnMapMovedUnderTouch()
        {
            _atLeastOneSwipeHappened = true;
        }

        /// <summary>
        /// Обработка свайпа
        /// </summary>
        /// <param name="callback">Коллбек обработки (параметры: начальные координаты свайпа, координаты смещения, было ли это первое касание)</param>
        public void CalculateSwipe(Action<Vector3, Vector3> callback)
        {
            // Свайп возможен только при одном таче, и если тач был начат не на UI элементе
            if (!Input.GetMouseButton(0))
            {
                return;
            }
            
            _currentTouchPosition = Input.mousePosition;
            
            var delta = InputTools.ConvertDisplacement(_startTouchPosition - _currentTouchPosition);

            if (!_atLeastOneSwipeHappened && (Math.Abs(delta.x) > _swipeTolerance || Math.Abs(delta.z) > _swipeTolerance))
            {
                // мы уже знаем что произошел свайп
                _atLeastOneSwipeHappened = true;

                // сбрасываем значение долгого тача
                _startTouchTime = DateTime.Now;
            }

            // после вычисления разницы в следующий раз отталкиваться будем уже от текущего значения тача,
            // что бы не возвращаться каждый раз в "первое начало" тача
            _startTouchPosition = _currentTouchPosition;

            var convertedPosition = new Vector3(delta.x, delta.y, delta.z);
            callback(_startTouchPosition, convertedPosition);
        }
    }
}