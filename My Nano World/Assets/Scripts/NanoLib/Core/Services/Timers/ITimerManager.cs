﻿using System;

namespace Assets.NanoLib.UtilitesEngine
{
    public interface ITimerManager
    {
        /// <summary>
        /// Returns current server time.
        /// </summary>
        long CurrentUTC { get; }
        double SessionLength { get; }
        
        void Init(long utc);

        void StartSyncServerTimeTimer();

        void OnHideUnity(bool isGameShown);
        
        ITimer StartRealTimer(float durationTime, Action onFinishedAction = null, Action<float> onTickAction = null, float tickDuration = 1f);

        ITimer StartServerTimer(float durationTime, Action onFinishedAction, Action<float> onTickAction = null);

        ITimer StartServerTimer(long endTimerUtc, Action onFinished, Action<float> onTickCallback = null);

        void Dispose();
    }
}
