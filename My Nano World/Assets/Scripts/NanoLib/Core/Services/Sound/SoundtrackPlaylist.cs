﻿using System;
using System.Collections.Generic;
using System.Linq;
#if UNITY_EDITOR
using Sirenix.OdinInspector;
#endif
using UnityEngine;
using Random = UnityEngine.Random;

namespace NanoLib.Core.Services.Sound
{
    [Serializable]
    public sealed class SoundtrackPlaylist
    {
        [SerializeField] private string _name;
        [SerializeField] private string _folder;
        [SerializeField] private List<string> _trackNamesList;

        public string Name => _name;

        public List<string> TrackNamesList => _trackNamesList;
        private int _current = 0;

        public string GetNext
        {
            get
            {
                if (_current + 1 >= _trackNamesList.Count )
                {
                    _current = 0;
                    return _trackNamesList[_current];
                }
                return _trackNamesList[++_current];
            }
        }

        public string GetFirst => _trackNamesList.FirstOrDefault();

        public string GetRandomNext
        {
            get
            {
                _current = Random.Range(0, _trackNamesList.Count);
                return _trackNamesList[_current];
            }
        }

        public const string GameScenePlaylistName = "GameScenePlaylist";

        #if UNITY_EDITOR
        [Button(ButtonSizes.Large), GUIColor(1,0.6507647f,0)]
        [PropertyOrder(-1)]
        private void ValidateTrackList()
        {
            var soundClips = Resources.LoadAll<AudioClip>(_folder.Remove(0, "Assets/Resources/".Length));
            _trackNamesList.Clear();
            foreach (var soundClip in soundClips)
            {
                _trackNamesList.Add(soundClip.name);
            }
        }
        #endif
    }
}