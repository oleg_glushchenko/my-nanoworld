using System;
using System.Collections.Generic;
using NanoLib.Core.Services.Sound;
using DG.Tweening;
using NanoLib.Core.Logging;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Audio;

namespace NanoLib.SoundManager
{
    [CreateAssetMenu(fileName = "SoundClipsStorage", menuName = "NanoReality/Settings/SoundClipsStorage")]
    public class SoundSettings : SerializedScriptableObject
    {
        [SerializeField] private Dictionary<SoundChannel, AudioMixerGroup> _channelGroupBindings;
        [SerializeField] private Dictionary<SoundChannel, string> _soundChannelsMixerNames;
        [SerializeField] private Dictionary<SoundChannel, float> _fadeDurations;
        [SerializeField] private SoundClipsStorage _soundClipsStorage;
        [SerializeField] private SoundSource _soundSourcePrefab;
        [SerializeField] private AudioMixer _mixer;
        [SerializeField] private float _channelMuteFadeDuration;
        [SerializeField] private float _musicDefaultVolume = -7.4f;
        [SerializeField] private float _soundDefaultVolume = -21f;

        private bool _isSoundMuted;
        private bool _isMusicMuted;
        private bool _isSfxMuted;
        private float _currentMusicVolume;
        private float _currentSfxVolume;

        private const int MinVolume = -80;

        private const string SoundChannelName = "SoundVol";
        private const string MusicChannelName = "MusicVol";
        private const string SfxChannelName = "SfxVol";

        private readonly Dictionary<SoundChannel, bool> _mutedChannels = new Dictionary<SoundChannel, bool>();
        private readonly Dictionary<SoundChannel, int> _defaultSoundChannelVolume = new Dictionary<SoundChannel, int>();

        private readonly Dictionary<SoundChannel, bool> _soloChannelsState = new Dictionary<SoundChannel, bool>
        {
            {SoundChannel.None, false},
            {SoundChannel.Music, false},
            {SoundChannel.SoundFX, false},
        };

        public SoundSource SoundSourcePrefab => _soundSourcePrefab;
        public SoundClipsStorage SoundClipsStorage => _soundClipsStorage;

        // TODO: remove it asap.
        [NonSerialized] private bool _initialized;
        
        public void Init()
        {
            if (_initialized)
            {
                return;
            }
            
            SoundClipsStorage.InitializeCache();
            foreach (KeyValuePair<SoundChannel, string> soundChannelsMixerName in _soundChannelsMixerNames)
            {
                SoundChannel soundChannel = soundChannelsMixerName.Key;
                string soundGroupMixerName = soundChannelsMixerName.Value;
                if (!_channelGroupBindings.TryGetValue(soundChannel, out AudioMixerGroup soundGroup))
                {
                    $"Mixer channel groups not contains SoundChannel [{soundChannel}].".LogError();
                    continue;
                }

                switch (soundChannel)
                {
                    case SoundChannel.SoundFX:
                        _defaultSoundChannelVolume.Add(soundChannel, (int)_soundDefaultVolume);
                        break;
                    case SoundChannel.Music:
                        _defaultSoundChannelVolume.Add(soundChannel, (int)_musicDefaultVolume);
                        break;
                    case SoundChannel.None:
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
                
            }

            _initialized = true;
        }

        public AudioMixerGroup GetAudioMixerGroup(SoundChannel channel)
        {
            if (!_channelGroupBindings.TryGetValue(channel, out var audioGroup))
            {
                $"[Sound] AudioMixerGroup for sound channel [{channel}] haven't bind.".LogWarning();
                return null;
            }

            return audioGroup;
        }

        public void SetChannelActive(SoundChannel channel, bool isOn)
        {
            int targetVolume = isOn ? _defaultSoundChannelVolume[channel] : MinVolume;
            string mixerChannelName = _soundChannelsMixerNames[channel];

            _mutedChannels[channel] = !isOn;

            void OnVolumeChangedTween(float volume)
            {
                _mixer.SetFloat(mixerChannelName, volume);
            }

            DOTween.To(() =>
            {
                _mixer.GetFloat(mixerChannelName, out float volume);
                return volume;
            }, OnVolumeChangedTween, targetVolume, _fadeDurations[channel]);
        }

        public void SetChannelSoloState(SoundChannel channel, bool isSolo)
        {
            if (!_soloChannelsState.ContainsKey(channel))
            {
                $"[Sound] There is no channel [{channel}]".LogError();
            }

            _soloChannelsState[channel] = isSolo;
        }

        public bool GetChannelSoloState(SoundChannel channel)
        {
            if (!_soloChannelsState.TryGetValue(channel, out bool isSolo))
            {
                $"[Sound] There is no channel [{channel}]".LogError();
            }

            return isSolo;
        }

        public float GetFadeDuration(SoundChannel channel)
        {
            if (!_fadeDurations.TryGetValue(channel, out float fadeDuration))
            {
                $"[Sound] There is no fade duration for channel: [{channel}]".LogWarning();
                return 0;
            }

            return fadeDuration;
        }

        public bool IsMuted(SoundChannel channel)
        {
            return _mutedChannels[channel];
        }
    }
}