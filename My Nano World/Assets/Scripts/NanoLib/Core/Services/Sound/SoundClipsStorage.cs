using System;
using System.Collections.Generic;
using System.Linq;
using MobileConsole;
using NanoLib.Core.Services.Sound;
using Sirenix.OdinInspector;
using UnityEngine;

namespace NanoLib.Core.Services.Sound
{
    [CreateAssetMenu(fileName = "SoundClipsStorage", menuName = "NanoReality/Settings/SoundClipsStorage")]
    public class SoundClipsStorage : SerializedScriptableObject
    {
        [SerializeField] private List<SoundtrackPlaylist> _playlists;
        [SerializeField] private string _playlistsPath;
        [SerializeField] private string _soundsPath;
        
        private static readonly char[] Separators = {'_', ' '};

        private readonly Dictionary<string, AudioClip> _cachedAudioClips = new Dictionary<string, AudioClip>();

        public void InitializeCache()
        {
            AudioClip[] soundClips = Resources.LoadAll<AudioClip>(_soundsPath.Remove(0, "Assets/Resources/".Length));
            AudioClip[] musicClips = Resources.LoadAll<AudioClip>(_playlistsPath.Remove(0, "Assets/Resources/".Length));
            
            foreach (AudioClip audioClip in soundClips)
            {
                // to match naming style in SFX enum
                string clipName = UpgradeName(audioClip.name);
                if (_cachedAudioClips.ContainsKey(clipName))
                {
                    Debug.LogWarning($"[Sound] AudioClip {clipName} is already contained in cached audio clips list.");
                    continue;
                }
                
                _cachedAudioClips.Add(clipName, audioClip);
            }
            
            foreach (AudioClip audioClip in musicClips)
            {
                string clipName = audioClip.name;
                if (_cachedAudioClips.ContainsKey(clipName))
                {
                    Debug.LogWarning($"[Sound] AudioClip {clipName} is already contained in cached audio clips list.");
                    continue;
                }
                
                _cachedAudioClips.Add(clipName, audioClip);
            }
        }
        
        private static string UpgradeName(string input)
        {
            string[] tempArray = input.Split(Separators);
	        
            for (var i = 0; i < tempArray.Length; i++)
            {
                int wordLength = tempArray[i].Length;
                if (wordLength > 0)
                    tempArray[i] = tempArray[i][0].ToUpper() + (wordLength > 1 ? tempArray[i].Substring(1) : string.Empty);
            }

            return string.Join(string.Empty, tempArray);
        }

        private void OnDestroy()
        {
            _cachedAudioClips.Clear();
        }

        public AudioClip GetClip(string clipName)
        {
            if (_cachedAudioClips.TryGetValue(clipName, out AudioClip targetClip))
            {
                return targetClip;
            }

            Debug.Log($"[Sound] AudioClip: {clipName} not found in cached audio clips list.");
            return null;
        }
        
        public AudioClip GetSfxClip(SfxSoundTypes clip)
        {
            string clipName = Enum.GetName(typeof(SfxSoundTypes), clip);
            
            if (clipName != null && _cachedAudioClips.TryGetValue(clipName, out AudioClip targetClip))
            {
                return targetClip;
            }

            Debug.Log($"[Sound] AudioClip: {clipName} not found in cached audio clips list.");
            return null;
        }

        public SoundtrackPlaylist GetPlayList(string playlistName)
        {
            SoundtrackPlaylist playlist = _playlists.Find(c => c.Name == playlistName);
            
            if (playlist != null)
            {
                return _playlists.Find(c => c.Name == playlistName);
            }
            
            Debug.LogError($"[Sound] There is no such playlist {playlistName}");
            return null;
        }
    }
}