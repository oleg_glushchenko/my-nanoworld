﻿using System;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using UnityEngine;
using strange.extensions.pool.api;
using UnityEngine.Audio;

namespace NanoLib.Core.Services.Sound
{
    public sealed class SoundSource : MonoBehaviour, IPoolable
    {
        [SerializeField] private AudioSource _audioSource;
        private float _fadeDuration;
        private Tween _fadeTween;

        /// <summary>
        /// Output channel of audio source.
        /// </summary>
        public SoundChannel SoundChannel { get; set; }
        
        /// <summary>
        /// Audio clip name.
        /// Empty string if it's null.
        /// </summary>
        public string ClipName;

        public bool Mute
        {
            get => _audioSource.mute;
            set
            {
                if(_audioSource.mute == value) return;

                float endVolume = value ? 0 : 1;
                if (_audioSource.mute && !value)
                {
                    _audioSource.volume = 0;
                    _audioSource.mute = false;
                }

                FadeToVolume(endVolume, () => _audioSource.mute = value);
            }
        }

        /// <summary>
        /// Calls when audio clip stop playing.
        /// Not called when looped clip ends.
        /// </summary>
        public event Action<SoundSource> Finished;

        private void Awake()
        {
            _audioSource = GetComponent<AudioSource>();
        }
        
        public void Play(AudioClip clip, AudioMixerGroup mixerGroup, bool isLoop = false,  float fadeDuration = 0)
        {
            _fadeDuration = fadeDuration;
            _audioSource.volume = 0;
            FadeToVolume(1);
            _audioSource.outputAudioMixerGroup = mixerGroup;
            _audioSource.loop = isLoop;
            _audioSource.clip = clip;
            _audioSource.Play();

            if (!isLoop)
            {
                DOVirtual.DelayedCall(clip.length, Stop);
            }
        }

        private void FadeToVolume(float endVolume, Action onComplete = null)
        {
            _fadeTween = DOTween.To(() => _audioSource.volume, newVolume => _audioSource.volume = newVolume, endVolume, _fadeDuration).OnComplete(() => onComplete?.Invoke());
        }

        public void StopFaded()
        {
            FadeToVolume(0, Stop);
        }
        
        public void Stop()
        {
            _audioSource.Stop();
            _fadeTween?.Kill();
            FinishPlaying();
        }

        private void FinishPlaying()
        {
            Finished?.Invoke(this);
        }

        #region IPoolable implementation

        public bool retain { get; private set; }
        
        public void Restore()
        {

        }

        public void Release()
        {
            retain = false;
            ResetParams();
            gameObject.SetActive(false);
        }

        public void Retain()
        {
            retain = true;
            ResetParams();
            gameObject.SetActive(true);
        }

        #endregion

        private void ResetParams()
        {
            _audioSource.clip = null;
            _audioSource.outputAudioMixerGroup  = null;
            Finished = null;
        }
    }
}
