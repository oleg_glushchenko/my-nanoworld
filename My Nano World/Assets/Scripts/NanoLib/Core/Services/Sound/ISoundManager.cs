﻿using System;
using NanoLib.Core.Services.Sound;

namespace NanoLib.Core.Services.Sound
{
    public interface ISoundManager
    {
        SoundSource Play(string audioClipName, SoundChannel channel = SoundChannel.None, bool isLooped = false, 
            Action<string> finishedCallback = null);
        
        SoundSource Play(SfxSoundTypes sfxClip, SoundChannel channel = SoundChannel.None, bool isLooped = false, 
            Action<string> finishedCallback = null);
        
        /// <summary>
        /// Use it in cases when you want play track in solo mode in certain sound channel(some game window, popup soundtrack).
        /// It mute other sound sources in channel
        /// </summary>
        /// <param name="audioClipName"></param>
        /// <param name="channel"></param>
        /// <param name="isLooped"></param>
        /// <param name="finishedCallback"></param>
        SoundSource PlaySolo(SfxSoundTypes sfxClip, SoundChannel channel = SoundChannel.None, bool isLooped = false,
            Action<string> finishedCallback = null);
        
        void StopChannel(SoundChannel channel);

        void Stop(SfxSoundTypes sfxClip,  SoundChannel channel = SoundChannel.None);
        
        void Stop(string audioClipName, SoundChannel channel);

        void StopSolo(SfxSoundTypes sfxClip, SoundChannel channel);
        
        /// <summary>
        /// Start play playlist.
        /// </summary>
        /// <param name="playlistName">Name</param>
        /// <param name="useRandom">Play tracks in random order</param>
        void PlayPlaylist(string playlistName, bool useRandom = false);

        /// <summary>
        /// Stops playing the current playlist.
        /// </summary>
        void StopPlaylist();
        
        void SetChannelActive(bool isOn, SoundChannel channel);
    }

    public enum SoundChannel
    {
        None = 0,
        Music = 1,
        SoundFX = 2
    }
}