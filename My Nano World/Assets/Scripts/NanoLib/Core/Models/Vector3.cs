﻿using System;
using UnityEngine;

namespace Assets.NanoLib.Utilities
{
    [Serializable]
    public struct Vector3F
    {
        public Vector3F(float X, float Y, float Z)
        {
            this.X = X;
            this.Y = Y;
            this.Z = Z;
        }

        public float X;
        public float Y;
        public float Z;

        public int IntX
        {
            get { return this[0]; }
            set { this[0] = value; }
        }

        public int IntY
        {
            get { return this[1]; }
            set { this[1] = value; }
        }

        public int IntZ
        {
            get { return this[2]; }
            set { this[2] = value; }
        }

        public Vector3F ConverterVect3(Vector3 vector3Pos)
        {
            return new Vector3F(vector3Pos.x, vector3Pos.y, vector3Pos.z);
        }

        public float Magnitude {
            get { return (float) Math.Sqrt( X*X + Y*Y + Z*Z); }
        }

        public float SqrMagnitude
        {
            get { return (X * X + Y * Y + Z * Z); }
        }

        public Vector3F Normalized
        {
            get
            {
                return new Vector3F(X/Magnitude,Y/Magnitude,Z/Magnitude);
            }
        }

        public void Normalize()
        {
            this = Normalized;
        }

        public override string ToString()
        {
            return X + " " + Y + " " + Z;
        }

        #region static variables
        public static Vector3F One
        {
            get
            {
                return  new Vector3F(1, 1, 1);
            }
        }

        public static Vector3F UnitX 
        {
            get
            {
                return new Vector3F(1, 0, 0);
            }
        }

        public static Vector3F UnitY
        {
            get
            {
                return new Vector3F(0, 1, 0);
            }
        }

        public static Vector3F UnitZ
        {
            get
            {
                return new Vector3F(0, 0, 1);
            }
        }

        public static Vector3F Zero
        {
            get
            {
                return new Vector3F(0, 0, 0);
            }
        }
        #endregion

        public int this[int index]    
        {
            get
            {
                if (index == 0)
                {
                    return (int) X;
                }
                else if (index == 1)
                {
                    return (int) Y;
                }
                else if (index == 2)
                {
                    return (int) Z;
                }
                else
                {
                    throw new ArgumentException();
                }
            }
            set
            {
                if (index == 0)
                {
                    X=value;
                }
                else if (index == 1)
                {
                    Y=value;
                }
                else if (index == 2)
                {
                    Z=value;
                }
                else
                {
                    throw new ArgumentException();
                }
            }
        }

        public static Vector3F operator +(Vector3F v1, Vector3F v2)
        {
            return new Vector3F(v1.X + v2.X, v1.Y + v2.Y, v1.Z + v2.Z);
        }

        public static Vector3F operator -(Vector3F v1, Vector3F v2)
        {
            return new Vector3F(v1.X - v2.X, v1.Y - v2.Y, v1.Z - v2.Z);
        }

        public static bool operator ==(Vector3F v1, Vector3F v2)
        {
            return v1.Equals(v2);
        }

        public static bool operator !=(Vector3F v1, Vector3F v2)
        {
            return !v1.Equals(v2);
        }

        public override bool Equals(object obj)
        {

            if (obj is Vector3F)
            {
                var vec = (Vector3F) obj;
                return X == vec.X && Y == vec.Y && Z ==vec.Z;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public static Vector3F operator *(Vector3F v1, float d)
        {
            return new Vector3F(v1.X * d, v1.Y * d, v1.Z * d);
        }

        public static Vector3F operator *(float d, Vector3F v1)
        {
            return new Vector3F(v1.X * d, v1.Y * d, v1.Z * d);
        }

        public static Vector3F operator /(Vector3F v1, float d)
        {
            return new Vector3F(v1.X / d, v1.Y / d, v1.Z / d);
        }
    }
}
