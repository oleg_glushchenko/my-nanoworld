using NanoReality.StrangeIoC;
using NanoReality.GameLogic.IAP;
using NanoLib.Utilities;

namespace NanoReality.GameLogic.Common
{
    public class DownloadOffersImagesCommand : AGameCommand
    {
        [Inject] public IIapService jIapService { get; set; }

        public override void Execute()
        {
            base.Execute();

            if (jIapService.InAppShopOffers == null)
            {
                jIapService.InitOffersData(Download);
            }
            else
            {
                Download();;
            }

            Retain();
        }

        private void Download()
        {
            int offersCount = jIapService.InAppShopOffers.Products.Length;

            if (offersCount == 0)
            {
                Release();
                return;
            }

            int loadedSpritesCount = 0;

            foreach (var item in jIapService.InAppShopOffers.Products)
                NetworkUtils.DownloadOfferSprite(item.IconId, (_) =>
                {
                    loadedSpritesCount++;
                    if (loadedSpritesCount == offersCount)
                    {
                        Release();
                    }
                });
        }
    }
}