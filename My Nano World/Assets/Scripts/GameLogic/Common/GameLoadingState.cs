namespace NanoReality.GameLogic.Common
{
    public enum GameLoadingState
    {
        Startup,
        PostLoadingActions,
        GameReady
    }
}