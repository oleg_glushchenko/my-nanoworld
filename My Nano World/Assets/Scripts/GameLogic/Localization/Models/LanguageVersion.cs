﻿using Newtonsoft.Json;

namespace NanoReality.GameLogic.Localization.Models
{
    public class LanguageVersion
    {
        [JsonProperty("lang")]
        public string Language { get; set; }

        [JsonProperty("ver")]
        public int Version { get; set; }
    }
}
