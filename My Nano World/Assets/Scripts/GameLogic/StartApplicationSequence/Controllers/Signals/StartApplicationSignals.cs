﻿using NanoReality.Engine.UI.Extensions.LoaderPanel;
using strange.extensions.signal.impl;

namespace NanoReality.GameLogic.StartApplicationSequence.Controllers.Signals
{
    public class StartInitApplicationSignal:Signal { }

    public class PostLoadingActionsCompleteSignal : Signal { }
    
    public class FinishApplicationInitSignal : Signal { }
    public class SignalGameInitialized : Signal { }
    public class SignalUserCreated : Signal<bool> { }
    
    public class HidePreloaderAfterGameLaunchSignal : Signal { }
    
    public class SignalPostLoadingAction : Signal { }
    
    public class UpdateTickSignal : Signal { }
}

