﻿using NanoLib.Core.Services.Sound;
using NanoLib.SoundManager;
using NanoReality.Engine.BuildingSystem.CityCamera;
using NanoReality.Engine.BuildingSystem.MapObjects;
using NanoReality.GameLogic.Settings.GameSettings.Models.Api;
using NanoReality.GameLogic.StartApplicationSequence.Controllers.Signals;
using NanoReality.StrangeIoC;
using UnityEngine;


namespace NanoReality.GameLogic.StartApplicationSequence
{
    /// <summary>
    /// Initialize here entities that require initialized UserProfile, BuildSetting IGlobalMapObjectViewSettings
    /// </summary>
    public class CommandSetGlobalSettings : AGameCommand
    {
        [Inject] public IGlobalMapObjectViewSettings ViewSettings { get; set; }

        [Inject] public IGameCameraModel jGameCameraModel { get; set; }

        [Inject] public SoundSettings jSoundSettings { get; set; }

        [Inject] public IGameSettings jGameSettings { get; set; }

        public override void Execute()
        {
	        base.Execute();
	        
            ViewSettings.SetGlobalShaderParams(jGameCameraModel.MaxZoom, jGameCameraModel.MinZoom);

            jSoundSettings.SetChannelActive(SoundChannel.SoundFX,jGameSettings.IsEffectsEnabled);
            jSoundSettings.SetChannelActive(SoundChannel.Music, jGameSettings.IsMusicEnabled);

            Debug.developerConsoleVisible = false;
        }
    }
}