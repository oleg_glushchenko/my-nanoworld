using NanoReality.GameLogic.ServerCommunication.Models.Api;
using NanoReality.GameLogic.TheEvent.Signals;
using NanoReality.StrangeIoC;

namespace NanoReality.GameLogic.StartApplicationSequence.Controllers.Commands
{
    public class LoadTheEventDataCommand : AGameCommand
    {
        [Inject] public IServerCommunicator jServerCommunicator { get; set; }
        [Inject] public SignalTheEventDataIsLoaded jDataIsLoaded { get; set; }

        public override void Execute()
        {
            base.Execute();
            Retain();
            jServerCommunicator.GetEventBaseData(baseData =>
            {
                jDataIsLoaded.Dispatch(baseData);
                Release();
            }, Release);
        }
    }
}

