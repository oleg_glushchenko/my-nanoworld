﻿using System.Collections.Generic;
using Assets.NanoLib.UtilitesEngine;
using Assets.Scripts.Engine.UI.Views.Popups;
using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using NanoLib.Core.Logging;
using NanoReality.Game.Analytics;
using NanoReality.Game.Data;
using NanoReality.GameLogic.Configs;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using NanoReality.GameLogic.Settings.GamePrefs;
using NanoReality.GameLogic.StartApplicationSequence.Controllers.Signals;
using NanoReality.StrangeIoC;
using strange.extensions.signal.impl;
using UnityEngine;

namespace NanoReality.GameLogic.StartApplicationSequence.Controllers.Commands
{
    /// <summary>
    /// Dispatched when device was authorized on server and received player id
    /// </summary>
    public class AuthorizationSuccessSignal : Signal<int> {}
    
    public class AuthorizationStartedSignal : Signal { }

    public class AuthorizeCommand : AGameCommand
    {
        [Inject] public IServerCommunicator jServerCommunicator { get; set; }
        [Inject] public IPopupManager jPopupManager { get; set; }
        [Inject] public IBuildSettings jBuildSettings { get; set; }
        [Inject] public LinksConfigsHolder jLinksHolder { get; set; }
        [Inject] public AuthorizationStartedSignal jAuthorizationStartedSignal { get; set; }
        [Inject] public AuthorizationSuccessSignal jAuthorizationSuccessSignal { get; set; }
        [Inject] public ITimerManager jTimerManager { get; set; }
        [Inject] public IGamePrefs jGamePrefs { get; set; }
        [Inject] public SignalUserCreated jSignalUserCreated { get; set; }
        [Inject] public IAnalyticsManager jAnalyticsManager { get; set; }

        public override void Execute()
        {
            base.Execute();
            
            Retain();
            jAuthorizationStartedSignal.Dispatch();
            jServerCommunicator.Authorize(OnAuthorizeCompleted);
        }

        private void OnAuthorizeCompleted(Dictionary<string, string> dictionary)
        {
            var buildVersion = dictionary["version"];
            var playerId = int.Parse(dictionary["id"]);
            var time = long.Parse(dictionary["time"]);

            jAnalyticsManager.InitAnalyticManager(playerId);
            jTimerManager.Init(time);
            jGamePrefs.DeviceId = dictionary["device_id"];
            
#if PRODUCTION && !UNITY_EDITOR
            if (IsBuildDeprecated(buildVersion)) return;
#endif

            jSignalUserCreated.Dispatch(string.IsNullOrEmpty(dictionary["isNewUser"]));
            jAuthorizationSuccessSignal.Dispatch(playerId);
            
            Release();
        }

        private bool IsBuildDeprecated(string serverVer)
        {
            if (!jBuildSettings.CheckBuildVersion) return false;

            float.TryParse(Application.version, out var currentVersion);
            float.TryParse(serverVer, out var serverVersion);

            Logging.Log(LoggingChannel.Core, $"<b>CheckBuildVersion</b> current: {currentVersion} server: {serverVer}");

            if (currentVersion >= serverVersion) return false;

            jPopupManager.Show(new UpgradeApplicationPopupSettings(), () =>
            {
#if UNITY_IOS
                Application.OpenURL(jLinksHolder.AppleAppUrl);
#elif UNITY_ANDROID
                Application.OpenURL(jLinksHolder.GoogleAppUrl);
#endif
                Application.Quit();
            });

            return true;
        }
    }
}