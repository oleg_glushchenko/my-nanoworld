﻿using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using NanoReality.GameLogic.StartApplicationSequence.Controllers.Signals;
using Assets.Scripts.Engine.Analytics.Model.api;
using NanoReality.StrangeIoC;
using strange.extensions.signal.impl;
using UnityEngine;

namespace NanoReality.GameLogic.StartApplicationSequence.Controllers.Commands
{
	public class SignalOnPlayerProfileLoaded : Signal<IPlayerProfile> {}
	
    public class CommandLoadPlayerProfile : AGameCommand
    {
	    [Inject] public IServerCommunicator jServerCommunicator { get; set; }
	    [Inject] public IPlayerProfile jPlayerProfile { get; set; }
	    [Inject] public IPlayerExperience jPlayerExperience { get; set; }
	    [Inject] public SignalOnPlayerProfileLoaded jSignalOnPlayerProfileLoaded { get; set; }

	    public override void Execute()
        {
	        base.Execute();
	        
            Retain();
	        
            jServerCommunicator.LoadUserProfile(jPlayerProfile, onProfileWasLoaded);
        }
        
        private void onProfileWasLoaded(IPlayerProfile profile) 
        {
	        PlayerPrefs.SetInt("PlayerID", profile.Id.Value);

	        jPlayerProfile = profile;
	        
	        jPlayerExperience.CurrentExperience = profile.CurrentExp;
	        jPlayerExperience.CurrentLevel = profile.CurrentLevel;
	        jPlayerExperience.Init();
	        
	        profile.Init();

	        jSignalOnPlayerProfileLoaded.Dispatch(profile);

            Release();
        }
    }
}
