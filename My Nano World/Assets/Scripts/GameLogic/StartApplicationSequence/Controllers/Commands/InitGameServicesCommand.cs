﻿using System.Collections.Generic;
using DG.Tweening;
using GameLogic.Bazaar.Service;
using GameLogic.Taxes.Service;
using NanoReality.Engine.UI.Extensions.EditModePanels;
using NanoReality.Game.Analytics;
using NanoReality.Game.CitizenGifts;
using NanoReality.Game.CityStatusEffects;
using NanoReality.Game.FoodSupply;
using NanoReality.Game.Notifications;
using NanoReality.Game.Services.OrderDesk.SeaportOrderDesk;
using NanoReality.Game.Production;
using NanoReality.Game.Services;
using NanoReality.Game.Tutorial;
using NanoReality.GameLogic.IAP;
using NanoReality.GameLogic.OrderDesk;
using NanoReality.GameLogic.Services;
using NanoReality.GameLogic.TheEvent;
using NanoReality.StrangeIoC;

namespace NanoReality.GameLogic.StartApplicationSequence.Controllers.Commands
{
    public class InitGameServicesCommand : AGameCommand
    {
        [Inject] public IFoodSupplyService jFoodSupplyService { get; set; }
        [Inject] public IProductService jProductService { get; set; }
        [Inject] public IBuildingService jBuildingService { get; set; }
        [Inject] public ICitySectorService jCitySectorService { get; set; }
        [Inject] public IBazaarService jBazaarService { get; set; }
        [Inject] public IEditModeService jEditModeService { get; set; }
        [Inject] public ICityTaxesService jCityTaxesService { get; set; }
        [Inject] public ICityStatusEffectService jCityStatusEffectService { get; set; }
        [Inject] public ICityGiftsService jCityGiftsService { get; set; }
        [Inject] public IProductionService jProductionService { get; set; }
        [Inject] public ISocialNetworkService jSocialNetworkService { get; set; }
        [Inject] public IHardTutorial jHardTutorial { get; set; }
        [Inject] public ISeaportOrderDeskService jSeaportOrderDeskService { get; set; }
        [Inject] public IMetroOrderDeskService jMetroOrderDeskService { get; set; }
        [Inject] public IAdsService jAdsService { get; set; }
        [Inject] public ITheEventService jITheEventService { get; set; }

#if UNITY_ANDROID || UNITY_IOS
        [Inject] public ILocalNotificationService jLocalNotificationService { get; set; }
        [Inject] public INotificationRegistrationService jNotificationRegistrationService { get; set; }
#endif
        [Inject] public IIapService jIapService { get; set; }

        private List<IGameService> _services = new List<IGameService>();

        public override void Execute()
        {
            base.Execute();
            
            _services = new List<IGameService>
            {
                jCitySectorService,
                jCityStatusEffectService,
                jFoodSupplyService,
                jCityGiftsService,
                jProductionService,
                jProductService,
                jBuildingService,
                jBazaarService,
#if UNITY_ANDROID || UNITY_IOS
                jLocalNotificationService,
                jNotificationRegistrationService,
#endif
                jCityTaxesService,
                jSeaportOrderDeskService,
                jSocialNetworkService,
                jMetroOrderDeskService,
                jIapService,
                jHardTutorial,
        //        jAnalyticsManager,
                jAdsService,
                jEditModeService,
                jITheEventService
            };

            foreach (var service in _services) 
                service.Init();

            Retain();
            
            DOVirtual.DelayedCall(5.0f, Release);
        }
    }
}