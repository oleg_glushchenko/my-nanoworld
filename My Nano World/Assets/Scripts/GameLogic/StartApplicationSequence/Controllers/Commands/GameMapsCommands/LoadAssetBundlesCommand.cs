using System;
using System.Collections;
using Assets.Scripts.Engine.UI.Views.Popups;
using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using NanoLib.AssetBundles;
using NanoReality.Core.Resources;
using NanoReality.Game.Data;
using NanoReality.GameLogic.Settings.GameSettings.Models.Api;
using NanoReality.StrangeIoC;
using UniRx;
using UnityEngine;

namespace NanoReality.GameLogic.Common.Controllers.Commands
{
    public sealed class LoadAssetBundlesCommand : AGameCommand
    {
        [Inject] public IPopupManager jPopupManager { get; set; }
        [Inject] public IGameSettings jGameSettings { get; set; }
        [Inject] public IBuildSettings jBuildSettings { get; set; }
        [Inject] public BundleLoadedSignal jBundleLoadedSignal { get; set; }
        [Inject] public SignalReloadApplication jSignalReloadApplication { get; set; }
        [Inject] public BundlesLoadingStartedSignal jBundlesLoadingStartedSignal { get; set; }
        [Inject] public BundlesLoadingFinishedSignal jBundlesLoadingFinishedSignal { get; set; }

        private ResourcesPolicyType Policy => jBuildSettings.ResourcesPolicy != ResourcesPolicyType.LocalResources && jGameSettings.IsHighQualityGraphic
            ? ResourcesPolicyType.RemoteAssetBundles
            : jBuildSettings.ResourcesPolicy;
        
        private string ManifestAssetBundleName => AssetBundlesNames.ManifestAssetBundleName;
        
        public override void Execute()
        {
            base.Execute();

            switch (Policy)
            {
                case ResourcesPolicyType.LocalAssetBundles:
                    Retain();
                    StartLoadingLocalAssetBundles();
                    break;
                
                case ResourcesPolicyType.RemoteAssetBundles:
                    Retain();
                    StartLoadingRemoteAssetBundles();
                    break;

                default:
                    FinishBundlesLoading();
                    break;
            }
        }
        
        private void StartLoadingLocalAssetBundles()
        {
            Observable.FromCoroutine(() => LoadLocalBundle(ManifestAssetBundleName, LoadOrDownloadAssetBundles)).Subscribe();
        }
        
        private void StartLoadingRemoteAssetBundles()
        {
            Observable.FromCoroutine(() => DownloadRemoteBundle(ManifestAssetBundleName, onCompleteCallback: LoadOrDownloadAssetBundles)).Subscribe();
        }

        private void LoadOrDownloadAssetBundles(AssetBundle manifestBundle)
        {
            var manifest = manifestBundle.LoadAsset<AssetBundleManifest>(AssetBundlesNames.ManifestFileName);
            var contentAssetBundlesNames = manifest.GetAllAssetBundles();

            Observable.FromCoroutine(LoadAssetBundles).Subscribe();

            IEnumerator LoadAssetBundles()
            {
                StartBundlesLoading(contentAssetBundlesNames.Length);

                for (var i = 0; i < contentAssetBundlesNames.Length;)
                {
                    var bundleName = contentAssetBundlesNames[i];
                    
                    void OnBundleLoaded(AssetBundle loadedBundle)
                    {
                        if (loadedBundle == null) return;

                        i++;
                        jBundleLoadedSignal.Dispatch(i);
                    }

                    if (Policy == ResourcesPolicyType.RemoteAssetBundles)
                        yield return DownloadRemoteBundle(bundleName, manifest.GetAssetBundleHash(bundleName), OnBundleLoaded);
                    else
                        yield return LoadLocalBundle(bundleName, OnBundleLoaded);
                }

                yield return new WaitForEndOfFrame();

                AssetBundleManager.Unload(ManifestAssetBundleName);
                FinishBundlesLoading();
            }
        }

        private IEnumerator DownloadRemoteBundle(string bundleName, Hash128 hash = default, Action<AssetBundle> onCompleteCallback = null)
        {
            var downloadOperation = new AssetBundleWebRequestOperation<AssetBundle>(bundleName, hash, onCompleteCallback, ShowNetworkErrorMessage);
            yield return downloadOperation.Download(AssetBundleManager.DownloadAssetBundle);
        }

        private IEnumerator LoadLocalBundle(string bundleName, Action<AssetBundle> onSuccessLoaded = null)
        {
            AssetBundle bundle = null;
            yield return AssetBundleManager.LoadLocalAssetBundle(bundleName, assetBundle => bundle = assetBundle);

            if (bundle != null)
            {
                onSuccessLoaded?.Invoke(bundle);
                yield break;
            }

            ShowNetworkErrorMessage($"Can't load bundle [{bundleName}] from StreamingAssets");
        }

        private void StartBundlesLoading(int bundlesCount)
        {
            jBundlesLoadingStartedSignal.Dispatch(bundlesCount);
        }

        private void FinishBundlesLoading()
        {
            Release();
            
            jBundlesLoadingFinishedSignal.Dispatch(true);
            jGameSettings.GraphicWasChanged = false;
        }

        private void ShowNetworkErrorMessage(string message)
        {
            var popupSettings = new NetworkErrorPopupSettings("Asset Bundles Loading Error", message, onReloadApplicationCallback: OnReloadApplication);
            jPopupManager.Show(popupSettings);

            void OnReloadApplication()
            {
                jSignalReloadApplication.Dispatch();
                Release();
            }
        }
    }
}