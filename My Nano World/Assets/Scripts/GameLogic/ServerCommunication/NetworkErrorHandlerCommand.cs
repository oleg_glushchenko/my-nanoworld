﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.NanoLib.ServerCommunication.Models;
using Assets.Scripts.Engine.Analytics.Model.api;
using Assets.Scripts.Engine.UI.Views.Popups;
using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using Assets.Scripts.GameLogic.Utilites;
using I2.Loc;
using NanoLib.Core.Logging;
using NanoReality.Engine.Settings.BuildSettings;
using NanoReality.GameLogic.ServerCommunication;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using NanoReality.StrangeIoC;
using strange.extensions.signal.impl;

namespace NanoReality.Game.Network.Commands
{
    public class SignalOnRequestError : Signal<IErrorData> { }

    public class NetworkErrorHandlerCommand : AGameCommand
    {
        private readonly Stack<RequestData> _lastReceivedRequests = new Stack<RequestData>();
        private const int MAX_LAST_REQUESTS_STACK_COUNT = 10;

        [Inject] public IServerRequest jServerRequest { get; set; }
        [Inject] public IServerCommunicator jServerCommunicator { get; set; }

        [Inject] public SignalReloadApplication jSignalReloadApplication { get; set; }
        [Inject] public SignalOnRequestError jSignalOnRequestError { get; set; }
        [Inject] public INanoAnalytics jNanoAnalytics { get; set; }
        [Inject] public IPopupManager jPopupManager { get; set; }
        [Inject] public NetworkSettings jNetworkSettings { get; set; }

        public override void Execute()
        {
            base.Execute();
            jServerRequest.ResponseReceived += OnResponseReceived;
            jServerCommunicator.ErrorReceived += OnErrorReceived;
        }

        public override void Release()
        {
            base.Release();
            jServerRequest.ResponseReceived -= OnResponseReceived;
            jServerCommunicator.ErrorReceived -= OnErrorReceived;
        }

        private void OnResponseReceived(RequestData requestData)
        {
            if (_lastReceivedRequests.Count >= MAX_LAST_REQUESTS_STACK_COUNT)
            {
                _lastReceivedRequests.Pop();
            }

            _lastReceivedRequests.Push(requestData);

            if (!IsPoorConnection())
            {
                return;
            }

            var logMessageDetails = new StringBuilder("Poor internet connection. Last requests: ");
            foreach (var request in _lastReceivedRequests)
            {
                logMessageDetails.Append($"\nTime:<b>{request.Duration}s</b> {request.URL}");
            }

            logMessageDetails.Append($"\nMedian Time: <b>{GetAverageRequestsDuration()}s</b>");

            Logging.LogError(LoggingChannel.Core, logMessageDetails.ToString());

            var title = LocalizationMLS.Instance.GetText(LocalizationKeyConstants.POORCONNECTION_LABEL_ERROR);
            var message = $"Localization/{LocalizationKeyConstants.POORCONNECTION_MSG_RESTART_GAME}";

            var popupSettings = new NetworkErrorPopupSettings(title, message, null, OnReloadClicked);

            jPopupManager.Show(popupSettings);
            jServerRequest.StopAndClearRequestQueue();
        }

        private bool IsPoorConnection()
        {
            Stack<RequestData> receivedRequests = _lastReceivedRequests;
            if (receivedRequests.Count < MAX_LAST_REQUESTS_STACK_COUNT)
            {
                return false;
            }

            float averageRequestDuration = GetAverageRequestsDuration();
            return averageRequestDuration >= jNetworkSettings.PoorConnectionAverageMaxLatency;
        }

        private float GetAverageRequestsDuration()
        {
            Stack<RequestData> receivedRequests = _lastReceivedRequests;
            List<RequestData> sortedRequestsList = receivedRequests.ToList().OrderBy(c => c.Duration).ToList();
            int count = sortedRequestsList.Count;

            int middleRequestIndex;
            if (sortedRequestsList.Count % 2 == 0)
            {
                middleRequestIndex = count / 2;
            }
            else
            {
                middleRequestIndex = count / 2 - 1;
            }
            
            return sortedRequestsList[middleRequestIndex].Duration / 1000f;
        }

        private void OnErrorReceived(IErrorData errorData)
        {
            Logging.LogError(LoggingChannel.Network, $"Request error Url:{errorData.URL} ErrorCode:{errorData.HttpErrorCode}, " +
                                                     $"ErrorMessage:{errorData.ErrorMessage}, Response:{errorData.Response}");
            jNanoAnalytics.SendRequestError(errorData);
            IPopupSettings popupSettings = null;

            string title = string.Empty;
            string message = string.Empty;

            switch (errorData.HttpErrorCode)
            {
                case ServerResponseCodes.RedirectionTooManyRequests:
                case ServerResponseCodes.Continue:
                {
                    // We can resend only GET requests.
                    if (errorData.Method == RequestMethod.GET)
                    {
                        jServerRequest.ResendLastRequest();
                        return;
                    }

                    title = LocalizationMLS.Instance.GetText(LocalizationKeyConstants.NOCONNECTIONPOPUP_LABEL_NO_CONNECTION);
                    message = "Localization/" + LocalizationKeyConstants.NOCONNECTIONPOPUP_MSG_NO_CONNECTION;

                    break;
                }

                case ServerResponseCodes.NoInternetConnection:
                case ServerResponseCodes.ClientErrorRequestTimeout:
                case ServerResponseCodes.ResponseUnknownError:
                    title = LocalizationMLS.Instance.GetText(LocalizationKeyConstants.NOCONNECTIONPOPUP_LABEL_NO_CONNECTION);
                    message = "Localization/" + LocalizationKeyConstants.NOCONNECTIONPOPUP_MSG_NO_CONNECTION;
                    break;
                
                case ServerResponseCodes.ClientErrorForbidden:
                    if (errorData.Response.Contains("Token invalid"))
                    {
                        title = $"{LocalizationMLS.Instance.GetText(LocalizationKeyConstants.ERRORRESTARTPOPUP_LABEL_ERROR)} {errorData.HttpErrorCode}";
                        message = "Localization/" + LocalizationKeyConstants.SERVERERROR_TEXT_POPUP;
                    }
                    else if(errorData.Response.Contains("You have been blocked on the server"))
                    {
                        title = $"{LocalizationManager.GetTranslation(ScriptLocalization.UICommon.BANING_TEXT_TITLE)} {errorData.HttpErrorCode}";
                        message = ScriptTerms.UICommon.BANING_TEXT_BODY;
                    }
                    break;

                case ServerResponseCodes.ClientErrorUnauthorized:
                {
                    if (string.IsNullOrEmpty(jServerCommunicator.AccessToken))
                    {
                        title = $"{LocalizationMLS.Instance.GetText(LocalizationKeyConstants.ERRORRESTARTPOPUP_LABEL_ERROR)} {errorData.HttpErrorCode}";
                        message = "Localization/" + LocalizationKeyConstants.SERVERERROR_TEXT_POPUP;
                    }
                    else
                    {
                        popupSettings = new DualConnectionPopupSettings();
                    }

                    break;
                }

                case ServerResponseCodes.LogicInAppPurchaseError:
                    title = LocalizationMLS.Instance.GetText(LocalizationKeyConstants.BILLINGERRORPOPUP_LABEL_BILLING_ERROR);
                    message = "Localization/" + LocalizationKeyConstants.BILLINGERRORPOPUP_MSG_BILLING_ERROR;
                    break;

                case ServerResponseCodes.LogicTechnicalMaintenance:
                {
                    title = ScriptLocalization.Localization.ERROR_MAINTENANCE_TITLE;
                    message = ScriptTerms.Localization.ERROR_MAINTENANCE_DESCRIPTION;
                    break;
                }
                
                case ServerResponseCodes.LogicWrongTime: // different time on client and server
                case ServerResponseCodes.PoorConnection: // poor internet connection
                case ServerResponseCodes.ResponseTimeout: // timeout
                case ServerResponseCodes.SuccessOK: // request timeout for some reason in UnityWebRequest
                case ServerResponseCodes.ResponseIsNotYetProcessed:
                    title = LocalizationMLS.Instance.GetText(LocalizationKeyConstants.POORCONNECTION_LABEL_ERROR);
                    message ="Localization/" +  LocalizationKeyConstants.POORCONNECTION_MSG_RESTART_GAME;
                    break;

                default: // any other error
                    title = $"{LocalizationMLS.Instance.GetText(LocalizationKeyConstants.ERRORRESTARTPOPUP_LABEL_ERROR)} {errorData.HttpErrorCode}";
                    message = "Localization/" + LocalizationKeyConstants.SERVERERROR_TEXT_POPUP;
                    break;
            }

            jSignalOnRequestError.Dispatch(errorData);

            if (popupSettings == null)
            {
                popupSettings = new NetworkErrorPopupSettings(title, message, OnTryContinueClicked, OnReloadClicked);
            }

            jPopupManager.Show(popupSettings);
        }

        private void OnReloadClicked()
        {
            jSignalReloadApplication.Dispatch();
        }

        private void OnTryContinueClicked()
        {
            jServerRequest.ResendLastRequest();
        }
    }
}
