﻿using NanoReality.GameLogic.BuildingSystem.Happiness;
using strange.extensions.signal.impl;

namespace NanoReality.GameLogic.ServerCommunication.Controllers
{

    /// <summary>
    /// Вызывается когда с сервера приходит новые значения счастья
    /// </summary>
    public class UpdatePopulationSatisfactionSignal : Signal<IHappinessData> { }
}
