﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.NanoLib.ServerCommunication.Models;
using Assets.NanoLib.Utilities;
using NanoReality.GameLogic.BuildingSystem.MapObjects;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.GameManager.Models.api;
using Newtonsoft.Json;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;

namespace NanoReality.GameLogic.ServerCommunication.Models.Api
{
    #region classes for server answers

    public class Buildings
    {
        [JsonProperty("exit_main_road")]
        public List<Id_IMapObject> ExitToMainRoadBuildings { get; set; }

        [JsonProperty("not_exit_main_road")]
        public List<Id_IMapObject> NotExitToMainRoadBuildings { get; set; }
    }

    public class CreateRoadResponse
    {
        [JsonProperty("roads")]
        public List<Id_IMapObject> Roads { get; set; }

        [JsonProperty("buildings")]
        public Buildings Buildings { get; set; }
    }

    public class DeleteRoadResponse
    {
        [JsonProperty("exit_main_road")]
        public List<Id_IMapObject> ExitToMainRoadBuildings { get; set; }

        [JsonProperty("not_exit_main_road")]
        public List<Id_IMapObject> NotExitToMainRoadBuildings { get; set; }
    }

    public class SyncBuildingResponse
    {
        /// <summary>
        /// Building model state from server.
        /// </summary>
        [JsonProperty("model")]
        public IMapObject Building;
    }

    public class MoveMapObjectResponce
    {
        /// <summary>
        /// Building model state from server.
        /// </summary>
        [JsonProperty("model")]
        public IMapObject MapObject;
    }

    #endregion

    public partial class ServerCommunicator
    {
        private class SubCommunicatorBuildingBaseData
        {   
            private readonly IServerCommunicator _serverCommunicator;

            public SubCommunicatorBuildingBaseData(IServerCommunicator serverCommunicator)
            {
                _serverCommunicator = serverCommunicator;
            }

            public void DeleteRoad(List<Id_IMapObject> ids,
                Action<DeleteRoadResponse> onSuccess, Action<IErrorData> onRequestError, IGameManager gameManager)
            {
                var header = new Dictionary<string, string>
                {
                    {"city-id", gameManager.CurrentUserCity.Id.Value.ToString()}
                };

                var data = "[" + string.Join(",", ids.Select(id => id.Value.ToString()).ToArray()) + "]";

                var parameters = new Dictionary<string, string>
                {
                    {"ids", data}
                };

                RequestCompleteDelegate complete = responceText =>
                {
                    var response =
                        (DeleteRoadResponse) _serverCommunicator.jStringSerializer.Deserialize(responceText, typeof (DeleteRoadResponse));
                    onSuccess(response);// 0, response.ExitToMainRoadBuildings, response.NotExitToMainRoadBuildings);
                };

                _serverCommunicator.jServerRequest.SendRequest(RequestMethod.GET, parameters, "/v1/user/road/extra/4",
                    complete, onRequestError, header);
            }

            public void BuildingSkip(Func<string> GetMapId, Action onSuccess, Action<IErrorData> onRequestError)
            {
                var parameters =
					new Dictionary<string, LazyParameter<string>>
                    {
						{"action",new LazyParameter<string>("skip")},
						{"id", new LazyParameter<string>(GetMapId)}
                    };

				RequestCompleteDelegate complete = responceText =>
                {		
					onSuccess();
				};

				_serverCommunicator.jServerRequest.SendRequest(RequestMethod.PUT,  "/v1/user/building/", parameters,
					complete, onRequestError,null, GetMapId);
			}

            public void PushUpgradeProduct(Func<string> mapId, Id<Product> productID, Action onSuccess, Action<IErrorData> onRequestError)
            {
                var parameters = new Dictionary<string, LazyParameter<string>>
                {
                    {"action", new LazyParameter<string>("upgrade_products")},
                    {"product_id", new LazyParameter<string>(productID.Value.ToString())}
                };

                void Complete(string responseText)
                {
                    onSuccess?.Invoke();
                }

                _serverCommunicator.jServerRequest.SendRequest(RequestMethod.PUT, "/v1/user/building/", parameters, Complete, onRequestError, null, mapId);
            }

            public void MoveMapObject(Func<string> mapId, Vector2F newPosition, Action<MoveMapObjectResponce> onSuccess, Action<IErrorData> onRequestError)
            {
				var parameters = new Dictionary<string, LazyParameter<string>>
                {
					{"action", new LazyParameter<string>("moving")},
					{"x", new LazyParameter<string>(newPosition.intX.ToString())},
					{"y", new LazyParameter<string>(newPosition.intY.ToString())}
                };

                RequestCompleteDelegate complete = responseText =>
                {
                    var responce =
                        _serverCommunicator.jStringSerializer.Deserialize(responseText, typeof(MoveMapObjectResponce))
                            as MoveMapObjectResponce;
                    onSuccess(responce);
                };

				_serverCommunicator.jServerRequest.SendRequest(RequestMethod.PUT,  "/v1/user/building/", parameters,
					complete, onRequestError, null, mapId);
            }

            public void SwitchAoeLayout(Func<string> mapId, AoeLayout aoeLayout, bool isEnegry, Action<MoveMapObjectResponce> onSuccsess
                , Action<IErrorData> onRequestError)
            {
                const string url = "/v1/user/player-building/";

                RequestCompleteDelegate completeDelegate = responceText =>
                {
                    _serverCommunicator.jStringSerializer.DeserializeAsync(responceText, onSuccsess);
                };

                var parameter = new Dictionary<string,  LazyParameter<string>>
                {
                    {"type", new LazyParameter<string>(((int) aoeLayout).ToString())},

                };

                RequestMethod method = isEnegry ? RequestMethod.PUT : RequestMethod.GET;

                _serverCommunicator.jServerRequest.SendRequest(method, url, parameter, completeDelegate, onRequestError, null, mapId);
            }
        }
    }
}