﻿using System;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using Assets.NanoLib.ServerCommunication.Models;
using UnityEngine;


namespace NanoReality.GameLogic.ServerCommunication.Models.Api
{
    public partial class ServerCommunicator 
    {
        /// <summary>
        /// Load user profile from the session on server
        /// </summary>
        public void LoadUserProfile(IPlayerProfile profile, Action<IPlayerProfile> callback = null)
        {
            RequestCompleteDelegate onProfileLoaded = responseText =>
            {
                jStringSerializer.DeserializeAsync(responseText, callback);
            };

            jServerRequest.SendRequest(RequestMethod.GET, null, "/v1/user", onProfileLoaded, OnRequestError);
        }

        /// <summary>
        /// Gives a list of all levels that have been add in the admin panel
        /// </summary>
        public void LoadUserLevels(IUserLevelsData userLevelsData, Action<IUserLevelsData> callback = null)
        {
            RequestCompleteDelegate onLevelsLoaded = (responseText) =>
            {
                void OnComplete(MUserLevelsBalanceData data)
                {
                    callback?.Invoke(data);
                }

                jStringSerializer.DeserializeAsync<MUserLevelsBalanceData>(responseText, OnComplete);
            };

            jServerRequest.SendRequestHash(RequestMethod.GET, null, "/v1/game/level", onLevelsLoaded, OnRequestError);
        }
    }
}
