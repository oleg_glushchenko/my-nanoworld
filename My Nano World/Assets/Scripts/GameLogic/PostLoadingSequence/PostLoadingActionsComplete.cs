using System.Linq;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.Common;
using NanoReality.GameLogic.GameManager.Models.api;
using NanoReality.GameLogic.StartApplicationSequence.Controllers.Signals;
using NanoReality.StrangeIoC;

namespace NanoReality.GameLogic.PostLoadingSequence
{
    public class PostLoadingActionsComplete : AGameCommand
    {
        [Inject] public PostLoadingActionsCompleteSignal jPostLoadingActionsCompleteSignal { get; set; }
        [Inject] public GameLoadingModel jGameLoadingModel { get; set; }
        
        [Inject] public IGameManager jGameManager { get; set; }

        public override void Execute()
        {
            base.Execute();
            StartBuildingTimers();
            
            jGameLoadingModel.LoadingState.Value = GameLoadingState.GameReady;
            jPostLoadingActionsCompleteSignal.Dispatch();
        }

        private void StartBuildingTimers()
        {
            foreach (BusinessSectorsTypes businessSectorsTypes in BusinessSectorExtensions.GetBuildingSectors()
                .Where(c => c != BusinessSectorsTypes.Global))
            {
                var map = jGameManager.GetMap(businessSectorsTypes);
                foreach (IMapObject mapObject in map.MapObjects)
                {
                    mapObject.InitializeBuildingTimers();
                }
            }

            foreach (IMapObject mapObject in jGameManager.GlobalCityMap.MapObjects)
            {
                mapObject.InitializeBuildingTimers();
            }
        }
    }
}