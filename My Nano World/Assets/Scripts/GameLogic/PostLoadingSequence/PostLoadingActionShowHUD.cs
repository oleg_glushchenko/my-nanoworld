using Assets.NanoLib.UI.Core.Signals;
using Engine.UI;
using NanoReality.StrangeIoC;

namespace NanoReality.GameLogic.PostLoadingSequence
{
    public class PostLoadingActionShowHUD : AGameCommand
    {
        [Inject] public UIFilter jUIFilter { get; set; }
        [Inject] public ShowWindowSignal jShowWindowSignal { get; set; }
        [Inject] public InitFlyingPanelSignal jInitFlyingPanelSignal { get; set; }

        public override void Execute()
        {
            base.Execute();
            
            foreach (var type in jUIFilter.StartupShow.Keys)
            {
                jShowWindowSignal.Dispatch(type, null);
            }
            
            jInitFlyingPanelSignal.Dispatch();
        }
    }
}