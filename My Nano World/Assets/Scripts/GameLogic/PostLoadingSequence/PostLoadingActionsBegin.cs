using NanoReality.GameLogic.Common;
using NanoReality.StrangeIoC;

namespace NanoReality.GameLogic.PostLoadingSequence
{
    public class PostLoadingActionsBegin : AGameCommand
    {
        [Inject] public GameLoadingModel jGameLoadingModel { get; set; }

        public override void Execute()
        {
            base.Execute();
            jGameLoadingModel.LoadingState.Value = GameLoadingState.PostLoadingActions;
        }
    }
}