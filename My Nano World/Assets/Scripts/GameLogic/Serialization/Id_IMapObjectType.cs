﻿using System;
using Assets.NanoLib.Utilities;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;

[Serializable]
public sealed class Id_IMapObjectType : Id<IMapObjectType>
{
	public Id_IMapObjectType(int i) : base(i)
	{
	}

	public Id_IMapObjectType()
	{
	}

	public static implicit operator Id_IMapObjectType(long x)
	{
		return new Id_IMapObjectType((int) x);
	}
	
	public static implicit operator Id_IMapObjectType(int x)
	{
		return new Id_IMapObjectType(x);
	}

	public static implicit operator int(Id_IMapObjectType x)
	{
		return x.Value;
	}

	public static implicit operator Id_IMapObjectType(string x)
	{
		int result;
		if (!int.TryParse(x, out result))
		{
			throw new Exception("failed to parse id from string '" + x + "'");
		}

		;
		return new Id_IMapObjectType(result);
	}
}