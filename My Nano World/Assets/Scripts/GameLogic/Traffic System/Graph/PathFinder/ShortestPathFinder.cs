﻿using System.Collections.Generic;

namespace NanoReality.GameLogic.TrafficSystem.Graph
{
    /// <summary>
    /// A* search algorithm
    /// https://en.wikipedia.org/wiki/A*_search_algorithm
    /// </summary>
    public class ShortestPathFinder<TVertex, TEdge> : IPathFinder<TVertex, TEdge> where TVertex : IVertex where TEdge : IEdge<TVertex>
    {
        private readonly HashSet<TVertex> _closed = new HashSet<TVertex>();
        private readonly HashSet<TVertex> _open = new HashSet<TVertex>();
        private readonly Dictionary<TVertex, TVertex> _cameFrom = new Dictionary<TVertex, TVertex>();
        private readonly Dictionary<TVertex, float> _gScore = new Dictionary<TVertex, float>();
        private readonly Dictionary<TVertex, float> _fScore = new Dictionary<TVertex, float>();

        #region Implementation of IPathFinder<TVertex,TEdge>

        public IList<TVertex> FindPath(TVertex start, TVertex end, IGraph<TVertex, TEdge> graph)
        {
            // The set of nodes already evaluated
            _closed.Clear();

            // The set of currently discovered nodes that are not evaluated yet.
            // Initially, only the start node is known.
            _open.Clear();
            _open.Add(start);

            // For each node, which node it can most efficiently be reached from.
            // If a node can be reached from many nodes, cameFrom will eventually contain the
            // most efficient previous step.
            _cameFrom.Clear();

            // For each node, the cost of getting from the start node to that node.
            // Map with default value of Infinity (!if not in map)
            _gScore.Clear();

            // The cost of going from start to start is zero.
            _gScore[start] = 0f;

            // For each node, the total cost of getting from the start node to the goal
            // by passing by that node. That value is partly known, partly heuristic.
            // Map with default value of Infinity (!if not in map)
            _fScore.Clear();

            // For the first node, that value is completely heuristic.
            _fScore[start] = HeuristicCostEstimate(start, end);

            while (_open.Count > 0)
            {
                var current = GetVertexWithLowestCost();

                if (current.Equals(end))
                {
                    return ReconstructPath(current);
                }

                _open.Remove(current);
                _closed.Add(current);

                foreach (var edge in graph.GetEdges(current))
                {
                    var neighbor = edge.End;

                    if (_closed.Contains(neighbor))
                    {
                        continue; // Ignore the neighbor which is already evaluated.
                    }

                    if (!_open.Contains(neighbor)) // Discover a new node
                    {
                        _open.Add(edge.End); 
                    }

                    // The distance from start to a neighbor
                    var tentativeGScore = _gScore[current] + edge.Length;
                    var neighborGScore = _gScore.ContainsKey(neighbor) ? _gScore[neighbor] : float.PositiveInfinity;
                    if (tentativeGScore >= neighborGScore)
                    {
                        continue; // This is not a better path.
                    }

                    // This path is the best until now. Record it!
                    _cameFrom[neighbor] = current;
                    _gScore[neighbor] = tentativeGScore;
                    _fScore[neighbor] = neighborGScore + HeuristicCostEstimate(neighbor, end);
                }
            }

            return null;
        }

        #endregion

        private static float HeuristicCostEstimate(TVertex start, TVertex goal)
        {
            return UnityEngine.Vector3.Distance(start.WorldPosition, goal.WorldPosition);
        }

        private TVertex GetVertexWithLowestCost()
        {
            float lowestCost = float.PositiveInfinity;
            TVertex vertexWithLowestCost = default(TVertex);

            foreach (var vertex in _open)
            {
                var cost = _fScore[vertex];
                if (cost <= lowestCost)
                {
                    vertexWithLowestCost = vertex;
                    lowestCost = cost;
                }
            }

            return vertexWithLowestCost;
        }

        private IList<TVertex> ReconstructPath(TVertex current)
        {
            List<TVertex> totalPath = new List<TVertex>{current};

            while (_cameFrom.ContainsKey(current))
            {
                current = _cameFrom[current];
                totalPath.Add(current);
            }

            totalPath.Reverse();

            return totalPath;
        }
    }
}
