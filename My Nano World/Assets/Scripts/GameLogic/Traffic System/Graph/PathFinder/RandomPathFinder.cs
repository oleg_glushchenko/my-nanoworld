﻿using System.Collections.Generic;
using UnityEngine;

namespace NanoReality.GameLogic.TrafficSystem.Graph
{
    public class RandomPathFinder<TVertex, TEdge> : IPathFinder<TVertex, TEdge> where TVertex : IVertex where TEdge : IEdge<TVertex>
    {
        private readonly HashSet<TVertex> _visitedVertices = new HashSet<TVertex>();

        #region Implementation of IPathFinder<TVertex,TEdge>

        public IList<TVertex> FindPath(TVertex start, TVertex end, IGraph<TVertex, TEdge> graph)
        {
            _visitedVertices.Clear(); // помечаем все ноды как непосещенные

            var path = new List<TVertex>();

            FindPathInDepth(start, end, graph, path); // пробуем найти путь

            path.Add(start); // добавляем стартовую ноду, как начало пути

            path.Reverse();

            return path;
        }

        #endregion

        private bool FindPathInDepth(TVertex start, TVertex end, IGraph<TVertex, TEdge> graph, ICollection<TVertex> path)
        {
            if (start.Equals(end)) // если достигли конца
            {
                return true; // путь найден
            }

            _visitedVertices.Add(start); // помечаем вершину как пройденную

            if (Random.value >= 0.5f) // случайно выбираем обход графа, слева на право, или наоборот
            {
                var edges = graph.GetEdges(start);
                for (var i = 0; i < edges.Count; i++)
                {
                    var edge = edges[i];
                    // если существует ребро между нодами, и мы не посещали ноду по этому ребру, идем внутрь
                    if (!_visitedVertices.Contains(edge.End))
                    {
                        var isFound = FindPathInDepth(edge.End, end, graph, path); // пробуем сделать шаг и найти путь из следующей ноды
                        if (isFound) // если нашли путь из следующей ноды
                        {
                            path.Add(edge.End); // добавляем в путь эту ноду
                            return true; //и выходим из поиска
                        }
                    }
                }
            }
            else
            {
                var edges = graph.GetEdges(start);
                for (var i = edges.Count - 1; i >= 0; i--)
                {
                    var edge = edges[i];
                    // если существует ребро между нодами, и мы не посещали ноду по этому ребру, идем внутрь
                    if (!_visitedVertices.Contains(edge.End))
                    {
                        var isFound = FindPathInDepth(edge.End, end, graph, path); // пробуем сделать шаг и найти путь из следующей ноды
                        if (isFound) // если нашли путь из следующей ноды
                        {
                            path.Add(edge.End); // добавляем в путь эту ноду
                            return true; //и выходим из поиска
                        }
                    }
                }
            }

            // если мы перебрали все ребра и не нашли путь из текущей вершины, то его не существует
            return false;
        }
    }
}
