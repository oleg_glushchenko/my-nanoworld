﻿using NanoReality.Engine.BuildingSystem.MapObjects;
using NanoReality.GameLogic.TrafficSystem.Graph;
using UnityEngine;
using Assets.NanoLib.Utilities;

namespace NanoReality.GameLogic.TrafficSystem.Graph
{
    public abstract class VertexBase : IVertex
    {
        public VertexBase(RoadMapObjectView roadView, Vector3 offset, Directions direction)
        {
            RoadView = roadView;
            MapPosition = roadView.GridPosition;
            MatrixPosition = roadView.MapObjectModel.MapPosition;
            WorldPosition = new Vector3(MapPosition.x, 0, MapPosition.y) + offset;
            Direction = direction;

            IsBlocked = false;
        }

        #region Implementation of IVertex

        public Vector3 WorldPosition { get; private set; }

        public Vector2F MatrixPosition { get; private set; }

        #endregion

        public RoadMapObjectView RoadView { get; private set; }
        public Vector2 MapPosition { get; private set; }

        /// <summary>
        /// Direction of an adjacent road to the node according to right side driveway rules
        /// </summary>
        public Directions Direction { get; private set; }

        public bool IsBlocked { get; set; }

        public void SetOffset(Vector3 offset)
        {
            WorldPosition = new Vector3(MapPosition.x, 0, MapPosition.y) + offset;
        }

        public override string ToString()
        {
            return string.Format("[{0},{1}, D{2}]", MatrixPosition.intX, 
                MatrixPosition.intY, (int)Direction);
        }
    }
}