﻿using System.Collections.Generic;
using Assets.NanoLib.Utilities;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.StrangeIoC;
using UnityEngine;
using NanoReality.Engine.BuildingSystem.MapObjects;

namespace NanoReality.GameLogic.TrafficSystem
{
    public class TrafficManager : AViewMediator, ITrafficManager
    {
        [SerializeField]
        private TrafficConfig _trafficConfig;

        [SerializeField]
        private List<TrafficController> _trafficControllers;
        
        public TrafficConfig TrafficConfig { get { return _trafficConfig; } }

        protected override void PreRegister()
        {
        }

        protected override void OnRegister()
        {
        }

        protected override void OnRemove()
        {
        }

        public TrafficController GetTrafficController(Id<IBusinessSector> businessSectorId)
        {
            foreach (var controller in _trafficControllers)
            {
                if (controller.BusinessSectorId == businessSectorId)
                {
                    return controller;
                }
            }

            return null;
        }

        #region Testing code move by path
        #if UNITY_EDITOR
        [Header("-------------------------------------")]
        [Header("        FIELDS FOR TESTING CODE      ")]
        [Header("-------------------------------------")]
        [Header("-------------------------------------")]
        [Header("        Testing move by path         ")]
        [Header("-------------------------------------")]
        [SerializeField] private BusinessSectorsTypes _sectorType;
        [SerializeField] private List<Vector2Int> _points;
        [SerializeField] private List<Directions> _directions;
        [SerializeField] private List<float> _pauses;
        [SerializeField] private LocomotorType _locomotorType;
        [Header("Create new unit or use one from the city")]
        [SerializeField] private bool _isNeedCreateNew;
        [Header("Set bool value to true == click button")]
        [SerializeField] private bool _applyTestLocomotiveMove;
        [SerializeField] private bool _applyTestCitymanMove;

        [Inject]
        public MoveCitizenSignal jMoveCitizenSignal { get; set; }

        [Inject]
        public MoveVehicleSignal jMoveVehicleSignal { get; set; }

        private void Update()
        {   
            if (_applyTestCitymanMove || _applyTestLocomotiveMove)
            {
                var path = new List<PathPoint>();
                for(int i = 0; i < _points.Count; i++)
                {
                    path.Add(new PathPoint(_points[i], _directions[i], _pauses[i]));
                }
                if (_applyTestLocomotiveMove)
                {
                    jMoveVehicleSignal.Dispatch(new LocomotiveMoveParametrs(_sectorType,
                        path, _isNeedCreateNew, OnMoveApplyCallback, _locomotorType));
                }
                if (_applyTestCitymanMove)
                {
                    jMoveCitizenSignal.Dispatch(new CitizenMoveParametrs(_sectorType,
                        path, _isNeedCreateNew, OnMoveApplyCallback));
                }
                _applyTestLocomotiveMove = false;
                _applyTestCitymanMove = false;
            }
        }

        private void OnMoveApplyCallback(bool isSucces, int id)
        {
            
        }
        #endif
        #endregion
    }
}
