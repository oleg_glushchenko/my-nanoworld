﻿using UnityEngine;
using NanoReality.Engine.BuildingSystem.MapObjects;
using NanoReality.GameLogic.TrafficSystem.Graph;

namespace NanoReality.GameLogic.TrafficSystem
{
    public class RoadGraphNode : VertexBase
	{
        private const int NoneIdValue = -1;

        public int _occupiedId;

        /// <summary>
        /// Узел графа дорог. Перекресток или тупик
        /// </summary>
        public RoadGraphNode(RoadMapObjectView roadView, Vector3 offset, Directions direction)
            :base(roadView, offset, direction)
        {
            _occupiedId = NoneIdValue;
        }

        public bool IsOccupied { get { return _occupiedId >= 0; } }

        public bool IsOccupiedBy(Locomotor locomotor)
	    {
            return locomotor.Id == _occupiedId;
	    }

        public void MakeOccupation(Locomotor locomotor)
        {
            if (IsOccupied)
            {
                Debug.LogWarning("Point " + RoadView.MapObjectModel.MapPosition +
                    " is already occupied");
            }
            _occupiedId = locomotor.Id;
        }

        public void MakeFree(Locomotor locomotor)
        {
            if (!IsOccupied)
            {
                Debug.LogWarning("Point " + RoadView.MapObjectModel.MapPosition +
                    " is already free");
            }
            _occupiedId = NoneIdValue;
        }
	}
}