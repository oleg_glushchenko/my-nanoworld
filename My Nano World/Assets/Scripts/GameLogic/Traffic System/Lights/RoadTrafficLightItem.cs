﻿using System.Collections.Generic;
using NanoReality.GameLogic.TrafficSystem.Road;
using UnityEngine;

namespace NanoReality.GameLogic.TrafficSystem.Lights
{
    public class RoadTrafficLightItem : RoadPropsItem
    {
        [SerializeField]
        private List<Renderer> _lights;
        [SerializeField]
        private Material _greenLightMaterial;
        [SerializeField]
        private Material _redLightMaterial;

        public void ShowGreenLight()
        {
            ShowLight(_greenLightMaterial);
        }

        public void ShowRedLight()
        {
            ShowLight(_redLightMaterial);
        }

        private void ShowLight(Material material)
        {
            foreach (var lightRenderer in _lights)
            {
                lightRenderer.material = material;
            }
        }
    }
}
