﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace NanoReality.GameLogic.TrafficSystem.Road
{
    [Serializable]
    public class RoadPropsTransform
    {
        public Vector3 Offset;
        public Vector3 Rotation;
    }

    [Serializable]
    public class RoadLampParams
    {
        public GameObject Prefab;
        public RoadPropsTransform Transform;
        public List<RoadTileType> TileTypes;
        public List<RoadMarkingsType> MarkingsTypes;
    }

    [Serializable]
    public class RoadPropsParams
    {
        public GameObject Prefab;
        public RoadPropsTransform LeftTop;
        public RoadPropsTransform LeftBottom;
        public RoadPropsTransform RightTop;
        public RoadPropsTransform RightBottom;
    }

    [CreateAssetMenu(fileName = "RoadPropsConfig", menuName = "Traffic System/Road Props Config", order = 999)]
    public class RoadPropsConfig : ScriptableObject
    {
        [SerializeField]
        private List<RoadPropsParams> _trafficLights;

        [SerializeField]
        private List<RoadPropsParams> _signs;

        [SerializeField]
        private List<RoadLampParams> _lamps;

        public RoadPropsParams GetRandomTrafficLightParams()
        {
            if (_trafficLights.Count > 0)
            {
                return _trafficLights[UnityEngine.Random.Range(0, _trafficLights.Count)];
            }

            return null;
        }

        public RoadPropsParams GetRandomSignParams()
        {
            if (_signs.Count > 0)
            {
                return _signs[UnityEngine.Random.Range(0, _signs.Count)];
            }

            return null;
        }

        public RoadLampParams GetLampParams(RoadTileType tileType, RoadMarkingsType markingsType)
        {
            foreach (var lamp in _lamps)
            {
                if (lamp.TileTypes.Contains(tileType) && lamp.MarkingsTypes.Contains(markingsType))
                {
                    return lamp;
                }
            }

            return null;
        }
    }
}
