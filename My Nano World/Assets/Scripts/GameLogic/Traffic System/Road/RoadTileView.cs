﻿using System;
using System.Collections.Generic;
using NanoReality.Engine.BuildingSystem.MapObjects;
using UnityEngine;

namespace NanoReality.GameLogic.TrafficSystem.Road
{
    [RequireComponent(typeof(MeshFilter))]
    [RequireComponent(typeof(MeshRenderer))]
    public class RoadTileView : MonoBehaviour
    {
        [SerializeField]
        private RoadTileConfig _roadTileConfig;

        private Transform _transform;

        public MeshFilter MeshFilter { get; private set; }

        public MeshRenderer Renderer { get; private set; }

        private void Awake()
        {
            MeshFilter = GetComponent<MeshFilter>();
            Renderer = GetComponent<MeshRenderer>();
            _transform = GetComponent<Transform>();
        }

        public void Show()
        {
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }

        public void UpdateRoadTile(RoadMapObjectView view)
        {
            MeshFilter.mesh = _roadTileConfig.GetTileMesh(view.RoadTileType, view.RoadMarkingsType);
            _transform.localRotation = Quaternion.Euler(_roadTileConfig.GetTileRotation(view.RoadTileType));

            if (view.RoadMarkingsType != RoadMarkingsType.DividerEnd)
            {
                return;
            }

            if (view.RoadTileType == RoadTileType.Straight_LB_RT)
            {
                if (view.Adjacent[Directions.RightTop].RoadTileType.IsCrossroads())
                {
                    Rotate(180f);
                }
            }
            if (view.RoadTileType == RoadTileType.Straight_LT_RB)
            {
                if (view.Adjacent[Directions.RightBottom].RoadTileType.IsCrossroads())
                {
                    Rotate(180f);
                }
            }
        }

        private void Rotate(float angle)
        {
            _transform.Rotate(Vector3.up, angle, Space.Self);
        }
    }

    /// <summary>
    /// LT-Left Top, LB- Left Bottom, RT- Right Top, RB - right bottom
    /// </summary>
    public enum RoadTileType
    {
        Straight_LB_RT,
        Straight_LT_RB,

        Deadlock_LB,
        Deadlock_RB,
        Deadlock_LT,
        Deadlock_RT,

        Turn_LB_LT,
        Turn_LB_RB,
        Turn_LT_RT,
        Turn_RB_RT,

        Intersection,

        Tside_LB_LT_RB,
        Tside_LT_LB_RT,
        Tside_RT_LT_RB,
        Tside_RB_LB_RT,

        CityEnter
    }

    public enum RoadTileShape
    {
        Straight,
        Deadlock,
        Turn,
        Crossroads
    }

    /// <summary>
    /// Special comparer for RoadTileShape enum. This will avoid all the casting when working with dictionaries.
    /// See: https://stackoverflow.com/questions/26280788/dictionary-enum-key-performance
    /// </summary>
    public struct RoadTileShapeComparer : IEqualityComparer<RoadTileShape>
    {
        public bool Equals(RoadTileShape x, RoadTileShape y)
        {
            return x == y;
        }

        public int GetHashCode(RoadTileShape obj)
        {
            return (int)obj;
        }
    }

    public enum RoadMarkingsType
    {
        Zebra,
        WhiteLine,
        YellowDashLine,
        DividerFull,
        DividerEnd
    }

    public static class RoadTileExtensions
    {
        public static RoadTileShape ToShape(this RoadTileType type)
        {
            switch (type)
            {
                case RoadTileType.Straight_LB_RT:
                case RoadTileType.Straight_LT_RB:
                    return RoadTileShape.Straight;
                case RoadTileType.Deadlock_LB:
                case RoadTileType.Deadlock_LT:
                case RoadTileType.Deadlock_RB:
                case RoadTileType.Deadlock_RT:
                    return RoadTileShape.Deadlock;
                case RoadTileType.Turn_LB_LT:
                case RoadTileType.Turn_LB_RB:
                case RoadTileType.Turn_LT_RT:
                case RoadTileType.Turn_RB_RT:
                    return RoadTileShape.Turn;
                case RoadTileType.CityEnter:
                case RoadTileType.Intersection:
                case RoadTileType.Tside_LB_LT_RB:
                case RoadTileType.Tside_LT_LB_RT:
                case RoadTileType.Tside_RB_LB_RT:
                case RoadTileType.Tside_RT_LT_RB:
                    return RoadTileShape.Crossroads;
            }
            throw new NotImplementedException(type.ToString());
        }

        public static bool IsCrossroads(this RoadTileType type)
        {
            return type.ToShape() == RoadTileShape.Crossroads;
        }

        public static bool IsDeadlock(this RoadTileType type)
        {
            return type.ToShape() == RoadTileShape.Deadlock;
        }

        public static bool IsStraight(this RoadTileType type)
        {
            return type.ToShape() == RoadTileShape.Straight;
        }

        public static bool IsTurn(this RoadTileType type)
        {
            return type.ToShape() == RoadTileShape.Turn;
        }

        public static bool IsDivider(this RoadMarkingsType type)
        {
            return type == RoadMarkingsType.DividerEnd || type == RoadMarkingsType.DividerFull;
        }

        public static Directions Invert(this Directions directions)
        {
            switch (directions)
            {
                case Directions.LeftTop:
                    return Directions.RightBottom;
                case Directions.LeftBottom:
                    return Directions.RightTop;
                case Directions.RightTop:
                    return Directions.LeftBottom;
                case Directions.RightBottom:
                    return Directions.LeftTop;
            }
            throw new NotImplementedException(directions.ToString());
        }

        public static Directions ToAdjacentRoadToEdge(this Directions start, Directions end)
        {
            if (start == Directions.LeftTop && end == Directions.LeftBottom || 
                start == Directions.LeftBottom && end == Directions.LeftTop)
            {
                return Directions.LeftBottom;
            }

            if (start == Directions.LeftBottom && end == Directions.RightBottom ||
                start == Directions.RightBottom && end == Directions.LeftBottom)
            {
                return Directions.RightBottom;
            }
            
            if (start == Directions.RightBottom && end == Directions.RightTop ||
                start == Directions.RightTop && end == Directions.RightBottom)
            {
                return Directions.RightTop;
            }
            
            if (start == Directions.RightTop && end == Directions.LeftTop ||
                start == Directions.LeftTop && end == Directions.RightTop)
            {
                return Directions.LeftTop;
            }

            // undefined
            return end;
        }
    }
}
