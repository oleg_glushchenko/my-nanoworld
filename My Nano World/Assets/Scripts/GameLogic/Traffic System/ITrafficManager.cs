﻿using Assets.NanoLib.Utilities;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;

namespace NanoReality.GameLogic.TrafficSystem
{
    public interface ITrafficManager
    {
        TrafficConfig TrafficConfig { get; }

        TrafficController GetTrafficController(Id<IBusinessSector> businessSectorId);
    }
}
