﻿﻿using NanoReality.GameLogic.TrafficSystem.Graph;
using UnityEngine;
using NanoReality.GameLogic.TrafficSystem.Domain;

namespace NanoReality.GameLogic.TrafficSystem.Citizens
{
    public class CitizenView : GraphMovePersonBase<SidewalkVertex, SidewalkEdge, SidewalkGraph>
    {
        [SerializeField]
        private CitizenModelView _modelView;
        [SerializeField]
        private Vector2 _speedRange = new Vector2(0.1f, 0.2f);

        #region GraphMovePersonBase

        public override void Init(TrafficConfig config, SidewalkGraph graph,
            IPathFinder<SidewalkVertex, SidewalkEdge> pathFinderForRandomMoving, 
            IPathFinder<SidewalkVertex, SidewalkEdge> pathFinderForMovingByPath, int id)
        {
            BaseSpeed = Random.Range(_speedRange.x, _speedRange.y);
            base.Init(config, graph, pathFinderForRandomMoving, pathFinderForMovingByPath, id);
        }

        public void SetRandomInCity()
        {
            SetCurrentVertex(GetRandomReachableVertex());
            SetRandomDestination();
        }

        protected override string NameForLog { get { return "Citizen"; } }

        protected override bool UseSmoothStop { get { return false; } }
      
        protected override void OnDeactivating()
        {
            _modelView.StopWalking();
        }

        protected override void SetNextVertex(SidewalkVertex vertex)
        {
            NextVertex = vertex;

            if (NextVertex != null)
            {
                CurrentEdge = Graph.GetEdge(CurrentVertex, NextVertex);
            }
        }

        protected override void SetSide(DirectionVectorType direction)
        {
            _modelView.PlayWalking(direction, CurrentSpeed);
        }

        protected override bool CanMoveToVertex(SidewalkVertex vertex)
        {
            if (CurrentVertex == null || vertex == null || CurrentEdge == null)
            {
                return false;
            }
            if (!SuitableForStay(vertex))
            {
                return false;
            }
            if (CurrentEdge.IsBlocked)
            {
                return GetDistanceToVertex(CurrentVertex) > CurrentEdge.Length * 0.1f;
            }
            
            return true;
        }

        protected override void DefaultInTargetLogicUpdate()
        {
            SetRandomDestination();
        }

        protected override void StoppingLogicUpdate()
        {
            if (_modelView.IsWalking)
            {
                UpdateSprite(NextVertex);
                _modelView.StopWalking();
            }
        }

        protected override bool SuitableForStay(SidewalkVertex vertex)
        {
            return true;
        }

        #endregion
    }
}