﻿namespace NanoReality.GameLogic.TrafficSystem
{
    public enum LocomotorType
    {
        Car,
        Bus,
        Truck
    }
}