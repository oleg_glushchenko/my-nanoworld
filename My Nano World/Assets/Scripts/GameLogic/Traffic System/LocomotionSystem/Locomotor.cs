﻿using UnityEngine;
using Assets.NanoLib.Utilities;
using System;
using System.Collections.Generic;
using strange.extensions.signal.impl;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.TrafficSystem.Graph;
using NanoReality.GameLogic.TrafficSystem.Domain;

namespace NanoReality.GameLogic.TrafficSystem
{
	/// <summary>
	/// Класс машины на дороге
	/// </summary>
    public class Locomotor : GraphMovePersonBase<RoadGraphNode, RoadGraphEdge, RoadsGraph>
    {
        [Serializable]
        public struct SectorAnimationsData
        {
            public int SectorId;
            public AnimationClip ExitClip;
            public AnimationClip EnterClip;
        }

         #region Signals

        public Signal<Locomotor> SignalOnExitFromCity  = new Signal<Locomotor>();
       
        #endregion

        [SerializeField] private LocomotorType _type = LocomotorType.Car;
        private Dictionary<Id<IBusinessSector>, SectorAnimationsData> _sectorAnimationsDictionary = new Dictionary<Id<IBusinessSector>, SectorAnimationsData>();
        private bool _isGoingToExitFromCity;
        private RoadGraphEdge _previousEdge;
        private RoadGraphEdge _nextEdge;
        public int _routsLeftBeforeExit;

        protected Dictionary<DirectionVectorType, GameObject> ViewsDirections = new Dictionary<DirectionVectorType, GameObject>();

        [SerializeField]
        protected GameObject FrontView;
        [SerializeField]
        protected GameObject BackView;
        [SerializeField]
        protected GameObject RightView;
        [SerializeField]
        protected GameObject LeftView;
        [SerializeField]
        private GameObject _leftToDown;
        [SerializeField]
        private GameObject _leftToUp;
        [SerializeField]
        private GameObject _rightToDown;
        [SerializeField]
        private GameObject _rightToUp;
        [SerializeField]
        private GameObject _downToLeft;
        [SerializeField]
        private GameObject _downToRight;
        [SerializeField]
        private GameObject _upToRight;
        [SerializeField]
        private GameObject _upToLeft;
        [SerializeField]
        protected List<SectorAnimationsData> SectorAnimations = new List<SectorAnimationsData> ();

        public Id<IBusinessSector> BusinessSectorId { get; set; }

        public LocomotorType Type { get { return _type; } }

       
        #region GraphMovePersonBase

        public override void Init(TrafficConfig config, RoadsGraph graph, 
            IPathFinder<RoadGraphNode, RoadGraphEdge> pathFinderForRandomMoving,
            IPathFinder<RoadGraphNode, RoadGraphEdge> pathFinderForMovingByPath, int id)
        {
            base.Init(config, graph, pathFinderForRandomMoving, pathFinderForMovingByPath, id);
            _routsLeftBeforeExit = config.GetRandomRoutsAmount();
            _isGoingToExitFromCity = false;
            _previousEdge = null;
            _nextEdge = null;
        }

        protected override string NameForLog { get { return "Locomotor"; } }

        protected override bool UseSmoothStop { get { return Config.StopVehicleGradually; } }

        protected override void OnDeactivating()
        {
            SetCurrentVertex(null);
            SetNextVertex(null);
            
            CurrentEdge = null;
            _previousEdge = null;
            _nextEdge = null;
            
            SignalOnExitFromCity.RemoveAllListeners();
            _isGoingToExitFromCity = false;
            IsAnimationPlaying = false;
            CacheMonobehavior.GetCacheComponent<Animation>().Stop();
        }

        protected override void OnOccupiedVertexChanging(RoadGraphNode previusOccupied, RoadGraphNode newOccupied)
        {
            base.OnOccupiedVertexChanging(previusOccupied, newOccupied);

            if (previusOccupied != null)
            {
                if (previusOccupied.IsOccupiedBy(this))
                {
                    previusOccupied.MakeFree(this);
                }
            }
                        
            if (newOccupied != null)
            {
                if (!newOccupied.IsOccupied)
                {
                    newOccupied.MakeOccupation(this);
                }
            }
        }

        protected override void SetNextVertex(RoadGraphNode node)
        {
            if (NextVertex != null)
            {
                if (NextVertex.IsOccupiedBy(this))
                {
                    NextVertex.MakeFree(this);
                }
            }

            NextVertex = node;

            if (NextVertex != null)
            {
                _previousEdge = CurrentEdge;
                CurrentEdge = Graph.GetEdge(CurrentVertex, NextVertex);

                var vertex = GetPathVertex(2);
                if (vertex != null)
                {
                    _nextEdge = Graph.GetEdge(NextVertex, vertex);
                }
            }
        }

        protected override void SetSide(DirectionVectorType direction)
        {
            //ViewsDirections can have same value in difference keys, so need
            //turn off all values than turn on selected value
            foreach (var dir in ViewsDirections)
            {
                dir.Value.SetActive(false);
            }
            if (ViewsDirections.ContainsKey(direction))
            {
                ViewsDirections[direction].SetActive(true);
            }
        }

        protected override bool CanMoveToVertex(RoadGraphNode vertex)
        {
            if (CurrentVertex == null || vertex == null)
            {
                return false;
            }

            if (!SuitableForStay(vertex))
            {
                return false;
            }

            if (vertex.IsBlocked)
            {
                if (CurrentEdge != null)
                {
                    return CurrentEdge.IsCrossroad || GetDistanceToVertex(vertex) < CurrentEdge.Length;
                }
                return false;
            }

            return true;
        }

        protected override void StoppingLogicUpdate()
        {
        }

        protected override bool SuitableForStay(RoadGraphNode vertex)
        {
            return !vertex.IsOccupied || vertex.IsOccupiedBy(this);
        }

        #endregion
       
        public void SetOnRoadStart(Id<IBusinessSector> sectorId)
        {
            PlayEnterAnimation(sectorId);
        }

        public void SetOnRandomPoint()
        {
            SetCurrentVertex(GetRandomReachableVertex());
            SetDestination(Graph.CityExitVertex);
            var nextVertex = GetPathVertex();
            UpdateSprite(nextVertex);
        }

        protected virtual void Awake()
        {
            ViewsDirections.Add (DirectionVectorType.Right, FrontView);
            ViewsDirections.Add (DirectionVectorType.Left, BackView);
            ViewsDirections.Add (DirectionVectorType.Down, RightView);
            ViewsDirections.Add (DirectionVectorType.Up, LeftView);
            ViewsDirections.Add (DirectionVectorType.LeftToUp, _leftToUp);
            ViewsDirections.Add (DirectionVectorType.LeftToDown, _leftToDown);
            ViewsDirections.Add (DirectionVectorType.UpToLeft, _upToLeft);
            ViewsDirections.Add (DirectionVectorType.UpToRight, _upToRight);
            ViewsDirections.Add (DirectionVectorType.RightToUp, _rightToUp);
            ViewsDirections.Add (DirectionVectorType.RightToDown, _rightToDown);
            ViewsDirections.Add (DirectionVectorType.DownToLeft, _downToLeft);
            ViewsDirections.Add (DirectionVectorType.DownToRight, _downToRight);

            for (int i = 0; i < SectorAnimations.Count; i++)
            {
                var anim = SectorAnimations [i];
                _sectorAnimationsDictionary.Add (new Id<IBusinessSector> (anim.SectorId), anim);
            }
        }

        protected virtual void PlayEnterAnimation(Id<IBusinessSector> sectorId)
        {
            SetSide(DirectionVectorType.Zero);

            IsAnimationPlaying = true;
            var clip = _sectorAnimationsDictionary [sectorId].EnterClip;
            CacheMonobehavior.GetCacheComponent<Animation> ().Play (clip.name);
        }

        protected virtual void PlayExitAnimation(Id<IBusinessSector> sectorId)
        {
            SetCurrentVertex(null);
            SetSide(DirectionVectorType.Zero);
            
            IsAnimationPlaying = true;
            var clip = _sectorAnimationsDictionary[sectorId].ExitClip;
            CacheMonobehavior.GetCacheComponent<Animation>().Play(clip.name);
        }

        private void OnStartAnimationEnded()
        {
            SetCurrentVertex(Graph.CityEnterVertex);
            SetDestination(Graph.CityExitVertex);
        }

        /// <summary>
        /// Устанавливает новую цель для авто, и запускает движение к цели
        /// </summary>
        private void SetDestinationFromCity()
        {
            var node = Graph.CityExitVertex;
            var graph = Graph;
            if (!graph.ReachableVertices.Contains (node))
                throw new ArgumentException ("Node is unreacheble");
            SetDestination(node);
        }

        protected override void DefaultInTargetLogicUpdate()
        {
            // доехали до выезда из города
            if (_isGoingToExitFromCity)
            {
                PlayExitAnimation(BusinessSectorId);
            }
            else if (_routsLeftBeforeExit > 0)
            {
                SetRandomDestination(); // выбираем новую цель
                _routsLeftBeforeExit--;
            }
            else // если машина отъездила нужно кол-во путей
            {
                // едим из города
                SetDestinationFromCity();
                _isGoingToExitFromCity = true;
                IsAnimationPlaying = false;
            }
        }

        protected override void UpdateSprite(RoadGraphNode vertex)
        {
            if (vertex == null)
            {
                return;
            }
            
            var targetDirection = GetDirectionWithTurning(vertex);
            if (!targetDirection.IsNotZero())
            {
                return;
            }
            
            SetSide(targetDirection);
            
            LastDirection = GetDirection(vertex);
        }

        private DirectionVectorType GetDirectionWithTurning(RoadGraphNode targetVertex)
        {
            var currentDirection = GetDirection(targetVertex);
            
            if (_previousEdge != null && CurrentEdge != null)
            {
                var previousDirection =
                    GetDirection(_previousEdge.Start.WorldPosition, _previousEdge.End.WorldPosition);

                if (currentDirection != previousDirection)
                {
                    var distance = Vector3.Distance(CurrentEdge.Start.WorldPosition, CurrentPosition);
                    var turningDistance = CurrentEdge.Length * Config.TurningDistance;

                    if (distance <= turningDistance)
                    {
                        return previousDirection.AddDirection(currentDirection);
                    }
                }
            }

            if (CurrentEdge != null && _nextEdge != null)
            {
                var nextDirection =
                    GetDirection(_nextEdge.Start.WorldPosition, _nextEdge.End.WorldPosition);

                if (currentDirection != nextDirection)
                {
                    var distance = Vector3.Distance(CurrentPosition, CurrentEdge.End.WorldPosition);
                    var turningDistance = CurrentEdge.Length * Config.TurningDistance;

                    if (distance <= turningDistance)
                    {
                        return currentDirection.AddDirection(nextDirection);
                    }
                }
            }
            
            return currentDirection;
        }

		public void OnExitAnimationEnd()
		{
			IsAnimationPlaying = false;
			SignalOnExitFromCity.Dispatch (this);
		}

		public void OnEnterAnimationEnd()
		{
			IsAnimationPlaying = false;
			OnStartAnimationEnded ();
		}
	}
}