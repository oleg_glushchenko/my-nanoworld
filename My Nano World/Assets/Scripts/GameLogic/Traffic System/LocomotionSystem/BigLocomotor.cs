﻿using Assets.NanoLib.Utilities;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.TrafficSystem.Domain;

namespace NanoReality.GameLogic.TrafficSystem
{
    public class BigLocomotor : Locomotor
    {
        private RoadGraphNode _backNode;

        protected override void OnOccupiedVertexChanging(RoadGraphNode previusOccupied, RoadGraphNode newOccupied)
        {
            base.OnOccupiedVertexChanging(previusOccupied, newOccupied);
            FreeBackNodeIfNeed();
            _backNode = previusOccupied;
            if (_backNode != null)
            {
                _backNode.MakeOccupation(this);
            }
        }

        protected override void PlayExitAnimation(Id<IBusinessSector> sectorId)
        {
            base.PlayExitAnimation(sectorId);
            
            FreeBackNodeIfNeed();
        }

        protected override void OnDeactivating()
        {
            base.OnDeactivating();
            SetSide(DirectionVectorType.Zero);
            FreeBackNodeIfNeed();
        }

        private void FreeBackNodeIfNeed()
        {
            if (_backNode != null && _backNode.IsOccupiedBy(this))
            {
                _backNode.MakeFree(this);
            }
        }
    }
}