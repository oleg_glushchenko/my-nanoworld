﻿using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Impl;
using Newtonsoft.Json;

namespace GameLogic.Taxes.Service
{
    public sealed class CollectTaxesNetworkResponse
    {
        [JsonProperty("coins")]
        public int CollectedCoins;
        
        [JsonProperty("city_taxes")]
        public CityTaxes CityTaxes;
    }
}