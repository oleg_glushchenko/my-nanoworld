﻿using System;
using NanoReality.Game.Services;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Impl;

namespace GameLogic.Taxes.Service
{
    public interface ICityTaxesService : IGameService
    {
        CityTaxes CityTaxes { get; }
        
        event Action TaxesChanged;
        event Action<int> TaxesCollected;

        bool AnyTaxesAccrued { get; }
        
        void Collect();
        void LoadNewTaxes(Action onSuccess = null);
    }
}