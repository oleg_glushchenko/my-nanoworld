﻿using NanoReality.GameLogic.BuildingSystem.GlobalCityMap.Models;
using NanoReality.GameLogic.Common.Controllers.Signals;
using NanoReality.GameLogic.GameManager.Models.api;
using NanoReality.GameLogic.Quests;
using NanoReality.GameLogic.Utilites;
using NanoReality.GameLogic.Utilities;

public class RepairBuildingCondition : Condition, IRepairBuildingCondition
{
    [Inject]
    public IGameManager jGameManager { get; set; }

    public Id_IMapObjectType BuildingToRepairID { get; set; }

    [Inject]
    public SignalOnBuildingRepaired jSignalOnBuildingRepaired { get; set; }
    
    public override string Description
    {
        get { return LocalizationUtils.SafeFormat(base.Description, LocalizationUtils.GetBuildingText(BuildingToRepairID)); }
    }

    public override void InitServerBalanceData()
    {
        int tmp;
        if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_1"), out tmp))
            BuildingToRepairID = tmp;
    }

    public override void InitFromPrototype(ICondition condition)
    {
        base.InitFromPrototype(condition);
        var prototype = condition as IRepairBuildingCondition;

        if (prototype != null)
            BuildingToRepairID = prototype.BuildingToRepairID;
    }

    /// <summary>
    /// Возвращает новый инстанс кондишена
    /// </summary>
    protected override ICondition GetNewConditionInstance
    {
        get { return jInjectionBinder.GetInstance<IRepairBuildingCondition>(); }
    }

    /// <summary>
    /// Копирует переменные в копию объекта
    /// </summary>
    /// <param name="target"></param>
    protected override void CopyMembers(ICondition target)
    {
        base.CopyMembers(target);

        var repairBuildingCondition = target as IRepairBuildingCondition;
        if (repairBuildingCondition == null)
            return;

        repairBuildingCondition.BuildingToRepairID = BuildingToRepairID;
    }

    public override Requirement GetConditionRequirement()
    {
        return new Requirement(BuildingToRepairID.Value, 1, IsAchived ? 1 : 0);
    }

    public override void StartEvaluateCondition()
    {
        jSignalOnBuildingRepaired.AddListener(OnBuildingRepaired);
    }

    public void OnBuildingRepaired(Id_IMapObjectType repairedBuildingID, Id_IMapObject mapID)
    {
        if (repairedBuildingID == BuildingToRepairID)
        {
            IsAchived = true;
            jSignalOnBuildingRepaired.RemoveListener(OnBuildingRepaired);
        }
    }

    public override void InitLocalStateData()
    {
    }
}
