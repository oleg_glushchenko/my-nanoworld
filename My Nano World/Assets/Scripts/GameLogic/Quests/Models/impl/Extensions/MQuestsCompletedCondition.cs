﻿using System.Collections.Generic;
using System.Linq;
using NanoReality.GameLogic.Quests;
using NanoReality.GameLogic.Utilities;
using NanoReality.Engine.UI.Extensions.QuestPanel;
using NanoReality.GameLogic.Utilites;
using UnityEngine;
/// <summary>
/// Условие прохождения N квестов. Используется для доступности квеста
/// </summary>
public class MQuestsCompletedCondition : Condition, IQuestsCompletedCondition
{
    [Inject]
    public IUserQuestData jUserQuestData { get; set; }

    [Inject] public SignalOnQuestComplete jSignalOnQuestComplete { get; set; }
	[Inject] public AchievementConditionUpdateSignal jAchievementConditionUpdateSignal { get; set; }

    public int CountOfCompletedQuests { get; set; }

    private int _currentCount;

    public override string Description =>  LocalizationUtils.SafeFormat(base.Description, _currentCount, CountOfCompletedQuests);

    public override void InitServerBalanceData()
    {
        int countOfCompletedQuests;
        if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_1"), out countOfCompletedQuests))
            CountOfCompletedQuests = countOfCompletedQuests;
    }


    public override void InitFromPrototype(ICondition condition)
    {
        base.InitFromPrototype(condition);
        var goldProt = condition as IQuestsCompletedCondition;

        if (goldProt != null)
            CountOfCompletedQuests = goldProt.CountOfCompletedQuests;
    }

    /// <summary>
    /// Возвращает новый инстанс кондишена
    /// </summary>
    protected override ICondition GetNewConditionInstance
    {
        get { return jInjectionBinder.GetInstance<IQuestsCompletedCondition>(); }
    }

    /// <summary>
    /// Копирует переменные в копию объекта
    /// </summary>
    /// <param name="target"></param>
    protected override void CopyMembers(ICondition target)
    {
        base.CopyMembers(target);
        var goldProt = target as IQuestsCompletedCondition;
        if (goldProt == null) return;
        goldProt.CountOfCompletedQuests = CountOfCompletedQuests;
    }

    public override Requirement GetConditionRequirement()
    {
        return new Requirement(0, CountOfCompletedQuests, _currentCount);
    }

	private void OnQuestCompleted(IQuest quest)
    {
        _currentCount = jUserQuestData.UserQuestsData.Count(q => q.QuestState == UserQuestStates.Completed);

        if (_currentCount >= CountOfCompletedQuests)
        {
            _currentCount = CountOfCompletedQuests;
            IsAchived = true;
            jSignalOnQuestComplete.RemoveListener(OnQuestCompleted);
        }
		else 
			jAchievementConditionUpdateSignal.Dispatch();
    }

    /// <summary>
    /// Входая точка вычисления условия
    /// </summary>
    public override void StartEvaluateCondition()
    {
        jSignalOnQuestComplete.AddListener(OnQuestCompleted);
		OnQuestCompleted(null);
    }

    public override void InitLocalStateData()
    {
    }
}