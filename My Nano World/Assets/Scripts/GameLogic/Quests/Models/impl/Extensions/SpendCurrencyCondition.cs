﻿using NanoReality.Game.Utilities;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.Common.Controllers.Signals;
using NanoReality.GameLogic.Utilites;

namespace NanoReality.GameLogic.Quests
{
    public class SpendCurrencyCondition : Condition, ISpendCurrencyCondition
    {
        public PriceType PriceType { get; set; }
        public int RequiredCount { get; set; }

        public override string Description => GetDescription();

        private int _currentAmount;

        [Inject] public AchievementConditionUpdateSignal jAchievementConditionUpdateSignal { get; set; }
        [Inject] public SignalOnCurrencyStateChanged jSignalOnCurrencyStateChanged { get; set; }

        public override void InitServerBalanceData()
        {
            if (int.TryParse(GetParameterValue("value_1"), out int tmp))
                PriceType = PriceUtility.GetPriceType(tmp);

            if (int.TryParse(GetParameterValue("value_2"), out tmp))
                RequiredCount = tmp;
        }

        public override void InitServerStatesData()
        {
            base.InitServerStatesData();

            if (int.TryParse(GetParameterValue("value_1"), out int tmp))
                PriceType = PriceUtility.GetPriceType(tmp);

            if (int.TryParse(GetParameterValue("value_2"), out tmp))
                _currentAmount = tmp;
        }

        public override void InitLocalStateData()
        {
        }

        public override void InitAchievedStateData()
        {
            base.InitAchievedStateData();

            _currentAmount = RequiredCount;
        }

        public override Requirement GetConditionRequirement()
        {
            return new Requirement((int) PriceType, RequiredCount, _currentAmount);
        }

        public override void StartEvaluateCondition()
        {
            if (AchievementCheck())
            {
                IsAchived = true;
            }
            else
            {
                jSignalOnCurrencyStateChanged.AddListener(OnCurrencyStateChanged);
            }
        }

        public override void InitFromPrototype(ICondition condition)
        {
            base.InitFromPrototype(condition);
            var castedConditions = (ISpendCurrencyCondition) condition;
            RequiredCount = castedConditions.RequiredCount;
            PriceType = castedConditions.PriceType;
        }

        protected override ICondition GetNewConditionInstance => jInjectionBinder.GetInstance<ISpendCurrencyCondition>();

        protected override void CopyMembers(ICondition target)
        {
            base.CopyMembers(target);
            var castedConditions = (ISpendCurrencyCondition) target;
            castedConditions.RequiredCount = RequiredCount;
            castedConditions.PriceType = PriceType;
        }

        private void OnCurrencyStateChanged(int value, PriceType type)
        {
            if (type != PriceType || value >= 0)
            {
                return;
            }

            _currentAmount -= value;

            if (AchievementCheck())
            {
                IsAchived = true;
                jSignalOnCurrencyStateChanged.RemoveListener(OnCurrencyStateChanged);
            }
            else
            {
                jAchievementConditionUpdateSignal.Dispatch();
            }
        }

        private bool AchievementCheck()
        {
            return _currentAmount >= RequiredCount;
        }

        private string GetDescription()
        {
            return LocalizationUtils.SafeFormat(base.Description,
                _currentAmount > RequiredCount ? RequiredCount : _currentAmount,
                RequiredCount.ToString(), LocalizationUtils.GetPriceTypeKey(PriceType));
        }
    }
}