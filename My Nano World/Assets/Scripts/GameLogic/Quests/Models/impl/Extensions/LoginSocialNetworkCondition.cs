﻿using Assets.Scripts.Engine.Analytics;
using NanoReality.GameLogic.Quests;
using NanoReality.GameLogic.SocialCommunicator.api;


public class LoginSocialNetworkCondition : Condition, ILoginSocialNetworkCondition 
{
	
	[Inject]
	public SignalOnSocialConnectAction jSignalOnSocialConnectAction { get; set; }

	[Inject(SocialNetworkCommunicatorTypes.Primary)]
	public ISocialNetworkCommunicator PrimarySocial { get; set; }
	
	[Inject(SocialNetworkCommunicatorTypes.Secondary)]
	public ISocialNetworkCommunicator SecondarySocial { get; set; }
	
	protected override ICondition GetNewConditionInstance { get { return jInjectionBinder.GetInstance<ILoginSocialNetworkCondition>(); } }
	
	public override void InitServerBalanceData()
	{
		// do nothing here
	}

	public override Requirement GetConditionRequirement()
	{
		return new Requirement(0, 1, IsAchived ? 1 : 0);
	}

	public override void StartEvaluateCondition()
	{
		jSignalOnSocialConnectAction.AddListener(OnAutorize);
	}

	private void OnAutorize(AuthorizeResult authorizeResult)
	{
		if (authorizeResult.IsAuthorized)
		{
			jSignalOnSocialConnectAction.RemoveListener(OnAutorize);
			IsAchived = true;
		}
	}

	public override void InitLocalStateData()
	{
		IsAchived = PrimarySocial.IsUserAuthorized || SecondarySocial.IsUserAuthorized;
	}

	
}
