﻿using NanoReality.Engine.UI.Extensions.UISignals;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.Quests;
using NanoReality.GameLogic.Utilities;


/// <summary>
/// Условие переименования города
/// </summary>
public class CityRenamedCondition : Condition, ICityRenamedCondition
{
    public int RenamesNeed { get; set; }

    [Inject] public AchievementConditionUpdateSignal jAchievementConditionUpdateSignal { get; set; }
    [Inject] public OnCityNameChangedOnServerSignal OnCityNameChangedOnServerSignal { get; set; }

    // TODO: need replace to global game constants.
    private const string _defaultCityName = "NanoCity";

    private int _renamesCount;

    public override void InitServerBalanceData()
    {
        if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_1"), out int renamesNeed))
        {
            RenamesNeed = renamesNeed;
        }
    }

    public override void InitServerStatesData()
    {
        base.InitServerStatesData();
        int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_1"), out _renamesCount);
    }

    public override void InitAchievedStateData()
    {
        base.InitAchievedStateData();

        _renamesCount = RenamesNeed;
    }

    public override void InitFromPrototype(ICondition condition)
    {
        base.InitFromPrototype(condition);

        if (condition is ICityRenamedCondition prot)
            RenamesNeed = prot.RenamesNeed;
    }

    protected override ICondition GetNewConditionInstance => jInjectionBinder.GetInstance<ICityRenamedCondition>();

    protected override void CopyMembers(ICondition target)
    {
        base.CopyMembers(target);
        var prot = target as ICityRenamedCondition;
        if (prot == null) return;
        prot.RenamesNeed = RenamesNeed;
    }
		
    public override Requirement GetConditionRequirement()
    {
        return new Requirement(0, RenamesNeed, _renamesCount);
    }

    public override void StartEvaluateCondition()
    {
        // PreEvaluate condition check.
        if (jPlayerProfile.UserCity.CityName != _defaultCityName)
        {
            IsAchived = true;
            return;
        }
        
        OnCityNameChangedOnServerSignal.AddListener(OnCityRename);
    }

    private void OnCityRename()
    {
        _renamesCount++;
        CheckForComplete();
    }

    private void CheckForComplete()
    {
        if (_renamesCount >= RenamesNeed)
        {
            _renamesCount = RenamesNeed;
            IsAchived = true;
            OnCityNameChangedOnServerSignal.RemoveListener(OnCityRename);
        }

        jAchievementConditionUpdateSignal.Dispatch();
    }

    public override void InitLocalStateData()
    {
        _renamesCount += CityStatistics.CityRenamesCount;
        CheckForComplete();
    }
}