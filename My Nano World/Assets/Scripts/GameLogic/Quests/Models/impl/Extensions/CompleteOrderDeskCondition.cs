﻿using Assets.NanoLib.Utilities;
using NanoReality.Game.Services.OrderDesk.SeaportOrderDesk;
using NanoReality.Game.UI;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.GameManager.Models.api;
using NanoReality.GameLogic.OrderDesk;
using NanoReality.GameLogic.Orders;
using NanoReality.GameLogic.Quests;
using NanoReality.GameLogic.Utilites;
using NanoReality.GameLogic.Utilities;
using UnityEngine;

namespace GameLogic.Quests.Models.impl.Extensions
{
    public class CompleteOrderDeskCondition : Condition, ICompleteOrderDeskCondition
    {
        [Inject] public AchievementConditionUpdateSignal jAchievementConditionUpdateSignal { get; set; }
        [Inject] public SignalSeaportOrderSent jSignalSeaportOrderSent { get; set; }
        [Inject] public MetroOrderShippingSignal jMetroOrderShippingSignal { get; set; }
        [Inject] public IGameManager jGameManager { get; set; }

        public int RequiredCompletedOrdersAmount { get; set; }
        public BusinessSectorsTypes BusinessSector { get; set; }

        private int _currentCompletedOrdersAmount;
    
        public override string Description => LocalizationUtils.SafeFormat(
            base.Description, 
            Mathf.Min(_currentCompletedOrdersAmount, RequiredCompletedOrdersAmount).ToString(), 
            RequiredCompletedOrdersAmount.ToString(),
            LocalizationUtils.GetBuildingText(GetObjectTypeID()));
        
        protected override ICondition GetNewConditionInstance => jInjectionBinder.GetInstance<ICompleteOrderDeskCondition>();

        public override void InitServerBalanceData()
        {
            if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_1"), out var tmp))
            {
                RequiredCompletedOrdersAmount = tmp;
            }

            if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_2"), out tmp))
            {
                BusinessSector = (BusinessSectorsTypes)tmp;
            }
        }

        public override void InitServerStatesData()
        {
            base.InitServerStatesData();

            if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_1"), out var tmp))
            {
                _currentCompletedOrdersAmount = tmp;
            }

            if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_2"), out tmp))
            {
                BusinessSector = (BusinessSectorsTypes)tmp;
            }
        }

        public override void InitFromPrototype(ICondition condition)
        {
            base.InitFromPrototype(condition);
            var prototype = (ICompleteOrderDeskCondition)condition;
            RequiredCompletedOrdersAmount = prototype.RequiredCompletedOrdersAmount;
            BusinessSector = prototype.BusinessSector;
        }

        public override Requirement GetConditionRequirement()
        {
            return new Requirement(0, RequiredCompletedOrdersAmount, _currentCompletedOrdersAmount);
        }
    
        public override void InitLocalStateData() { }

        public override void StartEvaluateCondition()
        {
            if (AchievementCheck())
            {
                IsAchived = true;
            }
            else
            {
                AddListeners();
            }
        }
        
        public override void InitAchievedStateData()
        {
            base.InitAchievedStateData();

            _currentCompletedOrdersAmount = RequiredCompletedOrdersAmount;
        }
        
        protected override void CopyMembers(ICondition target)
        {
            base.CopyMembers(target);
            var prototype = (ICompleteOrderDeskCondition)target;
            prototype.RequiredCompletedOrdersAmount = RequiredCompletedOrdersAmount;
            prototype.BusinessSector = BusinessSector;
        }
    
        private bool AchievementCheck()
        {
            return _currentCompletedOrdersAmount >= RequiredCompletedOrdersAmount;
        }

        private void SeaportOrderComplete(ISeaportOrderSlot order)
        {
            _currentCompletedOrdersAmount++;
            if (AchievementCheck())
            {
                IsAchived = true;
                RemoveListeners();
                jAchievementConditionUpdateSignal.Dispatch();
            }
        }
        
        private Id_IMapObjectType GetObjectTypeID()
        {
            switch (BusinessSector)
            {
                case BusinessSectorsTypes.HeavyIndustry:
                    return jGameManager.FindMapObjectOnMapByType(BusinessSector, MapObjectintTypes.OrderDesk).ObjectTypeID;
                case BusinessSectorsTypes.Town:
                    return jGameManager.FindMapObjectOnMapByType(BusinessSector, MapObjectintTypes.CityOrderDesk).ObjectTypeID;
            }

            return -1;
        }
    
        private void OnMetroOrderShipping(int trainId, NetworkMetroOrderTrain train)
        {
            _currentCompletedOrdersAmount++;
            if (AchievementCheck())
            {
                IsAchived = true;
                RemoveListeners();
                jAchievementConditionUpdateSignal.Dispatch();
            }
        }

        private void AddListeners()
        {
            switch (BusinessSector)
            {
                case BusinessSectorsTypes.HeavyIndustry:
                    jSignalSeaportOrderSent.AddListener(SeaportOrderComplete);
                    break;
                case BusinessSectorsTypes.Town:
                    jMetroOrderShippingSignal.AddListener(OnMetroOrderShipping);
                    break;
            }
        }

        private void RemoveListeners()
        {
            switch (BusinessSector)
            {
                case BusinessSectorsTypes.HeavyIndustry:
                    jSignalSeaportOrderSent.RemoveListener(SeaportOrderComplete);
                    break;
                case BusinessSectorsTypes.Town:
                    jMetroOrderShippingSignal.RemoveListener(OnMetroOrderShipping);
                    break;
            }
        }
    }
}
