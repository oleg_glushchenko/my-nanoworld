﻿using GameLogic.Quests.Models.api.Extensions;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.Quests;
using NanoReality.GameLogic.Utilites;

namespace GameLogic.Quests.Models.impl.Extensions
{
    public class BuildingConstructByTypeCondition : Condition, IBuildingConstructByTypeCondition
    {
        public new MapObjectintTypes BuildingType { get; set; }
        public int RequiredNumber { get; set; }
        public int CurrentNumber { get; private set; }
        public override string Description => GetDescription();

        [Inject] public SignalBuildFinishVerificated jSignalBuildFinishVerificated { get; set; }
        [Inject] public AchievementConditionUpdateSignal jAchievementConditionUpdateSignal { get; set; }
        [Inject] public IMapObjectsData jMapObjectsData { get; set; }

        public override void InitServerBalanceData()
        {
            if (int.TryParse(GetParameterValue("value_1"), out int tmp))
                BuildingType = (MapObjectintTypes)tmp;
            
            if (int.TryParse(GetParameterValue("value_2"), out tmp))
                Level = tmp;
            
            if (int.TryParse(GetParameterValue("value_3"), out tmp))
                RequiredNumber = tmp;
        }
        
        public override void InitServerStatesData()
        {
            base.InitServerStatesData();

            if (int.TryParse(GetParameterValue("value_1"), out int tmp))
                BuildingType = (MapObjectintTypes)tmp;
            
            if (int.TryParse(GetParameterValue("value_2"), out tmp))
                Level = tmp;
            
            if (int.TryParse(GetParameterValue("value_3"), out tmp))
                CurrentNumber = tmp;
        }
        
        public override void InitLocalStateData() { }
        
        public override void InitAchievedStateData()
        {
            base.InitAchievedStateData();

            CurrentNumber = RequiredNumber;
        }

        public override Requirement GetConditionRequirement()
        {
            return new Requirement((int)BuildingType, RequiredNumber, CurrentNumber);
        }

        public override void StartEvaluateCondition()
        {
            if (AchievementCheck())
            {
                IsAchived = true;
            }
            else
            {
                jSignalBuildFinishVerificated.AddListener(OnBuildFinishVerificated);
            }
        }

        public override void InitFromPrototype(ICondition condition)
        {
            base.InitFromPrototype(condition);
            var castedConditions = (IBuildingConstructByTypeCondition) condition;
            RequiredNumber = castedConditions.RequiredNumber;
            BuildingType = castedConditions.BuildingType;
            Level = castedConditions.Level;
        }

        protected override ICondition GetNewConditionInstance => jInjectionBinder.GetInstance<IBuildingConstructByTypeCondition>();

        protected override void CopyMembers(ICondition target)
        {
            base.CopyMembers(target);
            var castedTarget = (IBuildingConstructByTypeCondition) target;
            castedTarget.RequiredNumber = RequiredNumber;
            castedTarget.BuildingType = BuildingType;
            castedTarget.Level = Level;
        }
        
        private void OnBuildFinishVerificated(IMapObject mapObject)
        {
            if (mapObject.MapObjectType == BuildingType && mapObject.Level == Level && mapObject.IsConstructed)
            {
                CurrentNumber++;
                if (AchievementCheck())
                {
                    IsAchived = true;
                    jSignalBuildFinishVerificated.RemoveListener(OnBuildFinishVerificated);
                }
                else
                {
                    jAchievementConditionUpdateSignal.Dispatch(); 
                }
            }
        }

        private bool AchievementCheck()
        {
            return CurrentNumber >= RequiredNumber;
        }

        private string GetDescription()
        {
            int conditionTextType = Level < 2 ? 0 : 1;

            var baseDescription =
                LocalizationUtils.GetQuestsConditionText((int) ConditionType, conditionTextType);
            
            return LocalizationUtils.SafeFormat(
                baseDescription,
                CurrentNumber.ToString(),
                RequiredNumber.ToString(),
                LocalizationUtils.GetMapObjectTypeText(BuildingType),
                (Level + (jMapObjectsData.GetByBuildingType(BuildingType).MapObjectType == MapObjectintTypes.DwellingHouse ? 0 : 1)).ToString(), Level);
        }
    }
}
