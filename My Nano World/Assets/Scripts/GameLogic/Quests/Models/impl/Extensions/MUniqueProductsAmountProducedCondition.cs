﻿using Assets.NanoLib.Utilities;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Controllers.Agricultural;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using NanoReality.GameLogic.Quests;
using NanoReality.GameLogic.Utilities;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.Utilites;

public class MUniqueProductsAmountProducedCondition : Condition, IUniqueProductsAmountProducedCondition
{
    public int UniqueProductsCountToAchieve { get; set; }

    private int _currentProducedProducts;

	[Inject] public AchievementConditionUpdateSignal jAchievementConditionUpdateSignal { get; set; }
    [Inject] public OnProductProduceAndCollectSignal jOnProductProduceAndCollectSignal { get; set; }

    public override string Description
    {
        get { return LocalizationUtils.SafeFormat(base.Description, _currentProducedProducts, UniqueProductsCountToAchieve); }
    }
    
    /// <summary>
    /// Инициализация данных
    /// </summary>
    public override void InitServerBalanceData()
    {
        int tmp;
        if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_1"), out tmp))
            UniqueProductsCountToAchieve = tmp;
    }

    public override void InitAchievedStateData()
    {
        base.InitAchievedStateData();

        _currentProducedProducts = UniqueProductsCountToAchieve;
    }

    public override void InitFromPrototype(ICondition condition)
    {
        base.InitFromPrototype(condition);
        var prototype = condition as IUniqueProductsAmountProducedCondition;
        if (prototype != null)
            UniqueProductsCountToAchieve = prototype.UniqueProductsCountToAchieve;
    }

    /// <summary>
    /// Возвращает новый инстанс кондишена
    /// </summary>
    protected override ICondition GetNewConditionInstance { get { return jInjectionBinder.GetInstance<IUniqueProductsAmountProducedCondition>(); } }

    /// <summary>
    /// Копирует переменные в копию объекта
    /// </summary>
    /// <param name="target"></param>
    protected override void CopyMembers(ICondition target)
    {
        base.CopyMembers(target);
        var cond = target as IUniqueProductsAmountProducedCondition;
        if (cond == null) return;
        cond.UniqueProductsCountToAchieve = UniqueProductsCountToAchieve;
    }


    public override Requirement GetConditionRequirement()
    {
        return new Requirement(0, UniqueProductsCountToAchieve, 0);
    }

    /// <summary>
    /// Входая точка вычисления условия
    /// </summary>
    public override void StartEvaluateCondition()
    {
        jOnProductProduceAndCollectSignal.AddListener(OnProductProduced);
    }


	private void OnProductProduced(IMapObject mapObject, Id<Product> productID, int count)
    {
		if (count == 0)
			return;
        _currentProducedProducts += count;
		if (!CheckIsAchieved())
			jAchievementConditionUpdateSignal.Dispatch();
    }

    private bool CheckIsAchieved()
    {
        if (_currentProducedProducts >= UniqueProductsCountToAchieve)
        {
            IsAchived = true;
            _currentProducedProducts = UniqueProductsCountToAchieve;
            jOnProductProduceAndCollectSignal.RemoveListener(OnProductProduced);
			return true;
        }
		return false;
    }

    public override void InitLocalStateData()
    {
        OnProductProduced(null, 0, CityStatistics.TotalProducedProducts);
    }
}
