﻿using System.Linq;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.GameManager.Models.api;
using NanoReality.GameLogic.Utilites;
using NanoReality.GameLogic.Utilities;

namespace NanoReality.GameLogic.Quests
{
    /// <summary>
    /// Условие разблокировки по построенным зданиям типа N
    /// </summary>
    public class BuildBuildingByTypeCondition : Condition, IBuildBuildingByTypeCondition
    {
        [Inject] public AchievementConditionUpdateSignal jAchievementConditionUpdateSignal { get; set; }

        /// <summary>
        /// Количество зданий которые нужно построить
        /// </summary>
        public int Amout { get; set; }

        /// <summary>
        /// Текущее количество построенных зданий
        /// </summary>
        public int CurrentCount { get; set; }
      
        public override string Description =>
            LocalizationUtils.SafeFormat(
                base.Description,
                CurrentCount,
                Amout,
                LocalizationUtils.GetMapObjectTypeText(BuildingType),
                Level + (BuildingType == MapObjectintTypes.DwellingHouse ? 0 : 1));

        public override void InitServerBalanceData()
        {
            int tmp;
            if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_1"), out tmp))
                BuildingType = (MapObjectintTypes)tmp;
            
            if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_2"), out tmp))
                Level = tmp;

            if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_3"), out tmp))
                Amout = tmp;
            
        }

        public override void InitServerStatesData()
        {
            base.InitServerStatesData();
            int tmp;
            if(int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_3"), out tmp))
            {
                CurrentCount = tmp;
            }
        }

        public override void InitAchievedStateData()
        {
            base.InitAchievedStateData();

            CurrentCount = Amout;
        }

        public override void InitFromPrototype(ICondition condition)
        {
            base.InitFromPrototype(condition);
            var buildingProt = condition as IBuildBuildingByTypeCondition;
            if (buildingProt != null)
            {
                BuildingType = buildingProt.BuildingType;
                Level = buildingProt.Level;
                Amout = buildingProt.Amout;
                CurrentCount = buildingProt.CurrentCount;
            }
        }


        /// <summary>
        /// Возвращает новый инстанс кондишена
        /// </summary>
        protected override ICondition GetNewConditionInstance { get { return jInjectionBinder.GetInstance<IBuildBuildingByTypeCondition>(); } }

        /// <summary>
        /// Копирует переменные в копию объекта
        /// </summary>
        /// <param name="target"></param>
        protected override void CopyMembers(ICondition target)
        {
            base.CopyMembers(target);
            var buildCond = target as IBuildBuildingByTypeCondition;
            if (buildCond == null) return;
            buildCond.BuildingType = BuildingType;
            buildCond.Level = Level;
            buildCond.Amout = Amout;
            buildCond.CurrentCount = CurrentCount;
        }

        [Inject]
        public IMapObjectsData MapObjectsData { get; set; }

        public override Requirement GetConditionRequirement()
        {
            return new Requirement((int)BuildingType, Amout, CurrentCount);
        }

        /// <summary>
        /// Ссылка на сигнал который происходит при постройке здания
        /// </summary>
        [Inject]
        public SignalOnMapObjectBuildFinished jOnMapObjectBuildingFinishedSignal { get; set; }
        
        [Inject]
        public IGameManager jGameManager { get; set; }
        
        /// <summary>
        /// Входная точка вычисления условия
        /// </summary>
        public override void StartEvaluateCondition()
        {
            jOnMapObjectBuildingFinishedSignal.AddListener(OnBuidingUpdate);
        }

        /// <summary>
        /// Вызывается при постройке или апгрейде здания в игре
        /// </summary>
        /// <param name="mapObject"></param>
        private void OnBuidingUpdate(IMapObject mapObject)
        {
            if (mapObject.MapObjectType == BuildingType && mapObject.Level >= Level)
            {
                CurrentCount++;
                if (!CheckForAchiev())
                    jAchievementConditionUpdateSignal.Dispatch();
            }
        }

        private bool CheckForAchiev()
        {
            if (CurrentCount >= Amout)
            {
                CurrentCount = Amout;
                IsAchived = true;
                jOnMapObjectBuildingFinishedSignal.RemoveListener(OnBuidingUpdate);
                return true;
            }
            return false;
        }

        public override void InitLocalStateData()
        {
            CurrentCount = 0;
			foreach (var map in jPlayerProfile.UserCity.CityMaps)
            {
                CurrentCount += map.MapObjects.Count(b => b.MapObjectType == BuildingType && b.Level == Level && b.IsConstructed);
            }
            CheckForAchiev();
        }
    }
}
