﻿using Assets.NanoLib.UtilitesEngine.CustomDebugging.Models.api;
using NanoReality.GameLogic.Achievements;
using NanoReality.GameLogic.Common.Controllers.Signals;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.Quests;
using NanoReality.GameLogic.Utilites;
using NanoReality.GameLogic.Utilities;
using strange.framework.impl;

/// <summary>
/// Условие наличия софт валюты. Используется для доступности квеста
/// </summary>
public class MGoldGainedCondition : Condition, IGoldGainedCondition
{
    #region Inject
    
	[Inject] public AchievementConditionUpdateSignal jAchievementConditionUpdateSignal { get; set; }
    [Inject] public IDebug jDebug { get; set; }
    #endregion


    #region Signals

    [Inject] public SignalOnSoftCurrencyStateChanged SignalOnSoftCurrencyStateChanged { get; set; }

    #endregion

    public int CountGoldToAchieve { get; set; }

    private int _currentGold;
    
    public override string Description
    {
        get { return LocalizationUtils.SafeFormat(base.Description, _currentGold, CountGoldToAchieve); }
    }

    public override void InitServerBalanceData()
    {
        int countGoldToAchieve;
        if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_1"), out countGoldToAchieve))
            CountGoldToAchieve = countGoldToAchieve;
        jDebug.Log("Count gold to achive: " + CountGoldToAchieve, true);
    }

    public override void InitAchievedStateData()
    {
        base.InitAchievedStateData();

        _currentGold = CountGoldToAchieve;
    }


    public override void InitFromPrototype(ICondition condition)
    {
        base.InitFromPrototype(condition);
        var goldProt = condition as IGoldGainedCondition;

        if (goldProt != null)
            CountGoldToAchieve = goldProt.CountGoldToAchieve;
    }

    /// <summary>
    /// Возвращает новый инстанс кондишена, с помощью <see cref="Binder"/>
    /// </summary>
    protected override ICondition GetNewConditionInstance { get { return jInjectionBinder.GetInstance<IGoldGainedCondition>(); } }

    /// <summary>
    /// Копирует переменные в копию объекта
    /// </summary>
    /// <param name="target"></param>
    protected override void CopyMembers(ICondition target)
    {
        base.CopyMembers(target);
        var goldProt = target as IGoldGainedCondition;
        if (goldProt == null) return;
        goldProt.CountGoldToAchieve = CountGoldToAchieve;
    }

    public override Requirement GetConditionRequirement()
    {
        return new Requirement(0, CountGoldToAchieve, _currentGold);
    }

    private void OnGoldChanged(int currentGold)
    {
		if (_currentGold == currentGold)
			return;
        _currentGold = currentGold;

        if (currentGold >= CountGoldToAchieve)
        {
            _currentGold = CountGoldToAchieve;
            IsAchived = true;
            SignalOnSoftCurrencyStateChanged.RemoveListener(OnGoldChanged);
        }
		else 
			jAchievementConditionUpdateSignal.Dispatch();
    }

    /// <summary>
    /// Входая точка вычисления условия
    /// </summary>
    public override void StartEvaluateCondition()
    {
        SignalOnSoftCurrencyStateChanged.AddListener(OnGoldChanged);
    }

    public override void InitLocalStateData()
    {
    }
}
