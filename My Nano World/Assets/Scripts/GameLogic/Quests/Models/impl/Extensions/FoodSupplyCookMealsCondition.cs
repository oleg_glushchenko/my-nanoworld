﻿using GameLogic.Quests.Models.api.Extensions;
using NanoReality.Game.FoodSupply;
using NanoReality.GameLogic.Quests;
using NanoReality.GameLogic.Utilites;
using NanoReality.GameLogic.Utilities;
using UnityEngine;

namespace GameLogic.Quests.Models.impl.Extensions
{
    public class FoodSupplyCookMealsCondition : Condition, IFoodSupplyCookMealsCondition
    {
        [Inject] public IFoodSupplyService jFoodSupplyService { get; set; }
        
        private int _currentCookMealsAmount;

        private bool IsConditionComplete => _currentCookMealsAmount >= RequiredCookMealsAmount;
        
        public int RequiredCookMealsAmount { get; set; }
        
        public override string Description => 
            LocalizationUtils.SafeFormat(base.Description, Mathf.Clamp(_currentCookMealsAmount, 0, RequiredCookMealsAmount).ToString(), RequiredCookMealsAmount.ToString());
        
        public override void InitServerBalanceData()
        {
            if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_1"), out var value))
            {
                RequiredCookMealsAmount = value;
            }
        }

        public override void InitServerStatesData()
        {
            base.InitServerStatesData();

            if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_1"), out var value))
            {
                _currentCookMealsAmount = value;
            }
        }

        public override Requirement GetConditionRequirement()
        {
            return new Requirement(0, RequiredCookMealsAmount, _currentCookMealsAmount);
        }

        public override void StartEvaluateCondition()
        {
            if (IsConditionComplete)
            {
                IsAchived = true;
            }
            else
            {
                jFoodSupplyService.FoodSupplyProductCooked += FoodSupplyProductCooked;
            }
        }

        public override void InitLocalStateData()
        {
        }
        
        public override void InitAchievedStateData()
        {
            base.InitAchievedStateData();

            _currentCookMealsAmount = RequiredCookMealsAmount;
        }
        
        public override void InitFromPrototype(ICondition condition)
        {
            base.InitFromPrototype(condition);
            
            var castedConditions = (IFoodSupplyCookMealsCondition) condition;
            RequiredCookMealsAmount = castedConditions.RequiredCookMealsAmount;
        }

        protected override ICondition GetNewConditionInstance => jInjectionBinder.GetInstance<IFoodSupplyCookMealsCondition>();

        protected override void CopyMembers(ICondition target)
        {
            base.CopyMembers(target);
            
            var castedTarget = (IFoodSupplyCookMealsCondition) target;
            castedTarget.RequiredCookMealsAmount = RequiredCookMealsAmount;
        }
        
        private void FoodSupplyProductCooked(int productId)
        {
            _currentCookMealsAmount++;

            if (IsConditionComplete)
            {
                IsAchived = true;
                jFoodSupplyService.FoodSupplyProductCooked -= FoodSupplyProductCooked;
            }
        }
    }
}
