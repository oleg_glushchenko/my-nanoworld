﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Engine.BuildingSystem.CityMap;
using Assets.Scripts.GameLogic.Utilites;
using GameLogic.Quests.Models.api.Extensions;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.GameManager.Models.api;
using NanoReality.GameLogic.Quests;
using NanoReality.GameLogic.Utilites;
using NanoReality.GameLogic.Utilities;

namespace GameLogic.Quests.Models.impl.Extensions
{
    public class BuildingHaveByTypeCondition : Condition, IBuildingHaveByTypeCondition
    {
        public new MapObjectintTypes BuildingType { get; set; }
        public int RequiredNumber { get; set; }
        public int CurrentNumber { get; private set; }
        public override string Description =>
            LocalizationUtils.SafeFormat(
                base.Description,
                CurrentNumber.ToString(),
                RequiredNumber.ToString(),
                LocalizationUtils.GetMapObjectTypeText(BuildingType),
                Level + (jMapObjectsData.GetByBuildingType(BuildingType).MapObjectType == MapObjectintTypes.DwellingHouse ? 0 : 1));
   
        [Inject] public SignalBuildFinishVerificated jSignalBuildFinishVerificated { get; set; }
        [Inject] public AchievementConditionUpdateSignal jAchievementConditionUpdateSignal { get; set; }
        [Inject] public IMapObjectsData jMapObjectsData { get; set; }

        public override void InitServerBalanceData()
        {
            if (int.TryParse(GetParameterValue("value_1"), out int tmp))
                BuildingType = (MapObjectintTypes)tmp;
            
            if (int.TryParse(GetParameterValue("value_2"), out tmp))
                Level = tmp;
            
            if (int.TryParse(GetParameterValue("value_3"), out tmp))
                RequiredNumber = tmp;
        }
        
        public override void InitServerStatesData()
        {
            base.InitServerStatesData();

            if (int.TryParse(GetParameterValue("value_1"), out int tmp))
                BuildingType = (MapObjectintTypes)tmp;
            
            if (int.TryParse(GetParameterValue("value_2"), out tmp))
                Level = tmp;
            
            if (int.TryParse(GetParameterValue("value_3"), out tmp))
                CurrentNumber = tmp;
        }
        
        public override void InitLocalStateData() { }
        
        public override void InitAchievedStateData()
        {
            base.InitAchievedStateData();

            CurrentNumber = RequiredNumber;
        }

        public override Requirement GetConditionRequirement()
        {
            return new Requirement((int)BuildingType, RequiredNumber, CurrentNumber);
        }

        public override void StartEvaluateCondition()
        {
            RecalculateCurrentCount();
            if (AchievementCheck())
            {
                IsAchived = true;
            }
            else
            {
                jSignalBuildFinishVerificated.AddListener(OnBuildFinishVerificated);
            }
        }
        
        public override void InitFromPrototype(ICondition condition)
        {
            base.InitFromPrototype(condition);
            var castedConditions = (IBuildingHaveByTypeCondition) condition;
            RequiredNumber = castedConditions.RequiredNumber;
            BuildingType = castedConditions.BuildingType;
            Level = castedConditions.Level;
        }

        protected override ICondition GetNewConditionInstance => jInjectionBinder.GetInstance<IBuildingHaveByTypeCondition>();

        protected override void CopyMembers(ICondition target)
        {
            base.CopyMembers(target);
            var castedTarget = (IBuildingHaveByTypeCondition) target;
            castedTarget.RequiredNumber = RequiredNumber;
            castedTarget.BuildingType = BuildingType;
            castedTarget.Level = Level;
        }
        
        private void OnBuildFinishVerificated(IMapObject mapObject)
        {
            if (mapObject.MapObjectType == BuildingType && mapObject.IsConstructed)
            {
                RecalculateCurrentCount();
                if (AchievementCheck())
                {
                    IsAchived = true;
                    jSignalBuildFinishVerificated.RemoveListener(OnBuildFinishVerificated);
                }
                else
                {
                    jAchievementConditionUpdateSignal.Dispatch(); 
                }
            }
        }

        private bool AchievementCheck()
        {
            return CurrentNumber >= RequiredNumber;
        }
        
        private void RecalculateCurrentCount()
        {
            CurrentNumber = 0;
            foreach (ICityMap map in jPlayerProfile.UserCity.CityMaps)
            {
                int count = map.MapObjects.Count(b => b.MapObjectType == BuildingType && b.Level >= Level && b.IsConstructed);
                CurrentNumber += count;
            }
        }
    }
}
