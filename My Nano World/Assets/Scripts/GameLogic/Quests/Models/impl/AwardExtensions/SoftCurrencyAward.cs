﻿using Assets.Scripts.Engine.Analytics.Model.api;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.PlayerProfile.Models.Impl;
using NanoReality.GameLogic.Utilities;

namespace NanoReality.GameLogic.Quests
{
    public class SoftCurrencyAward : AwardAction, ISoftCurrencyAward
    {
        public override int AwardCount { get { return SoftCurrencyCount; } }

        public override AwardProduct GetAwardProduct {
            get
            {
                return new AwardProduct(AwardType, SoftCurrencyCount.ToString());
            }
        }

        public int SoftCurrencyCount { get; set; }


        [Inject] public IPlayerProfile jPlayerProfile { get; set; }
        [Inject] public INanoAnalytics jNanoAnalytics { get; set; }

        public override void Execute(AwardSourcePlace sourcePlace, string source)
        {
            base.Execute(sourcePlace, source);
            jPlayerProfile.PlayerResources.AddSoftCurrency(SoftCurrencyCount);
            jNanoAnalytics.OnPlayerCurrenciesIncrease(PlayerResources.CurencyType.Coins, SoftCurrencyCount, sourcePlace, source);
        }

        public override void InitServerData()
        {
            base.InitServerData();
            SoftCurrencyCount = int.Parse(SerializebleKeyValuePair.GetValue(ServerParams, "value_1"));
        }


        public override string GetAwardDescriptionText(IAwardActionDescription baseDescription)
        {
            return string.Format(baseDescription.Description, SoftCurrencyCount);
        }
    }
}
