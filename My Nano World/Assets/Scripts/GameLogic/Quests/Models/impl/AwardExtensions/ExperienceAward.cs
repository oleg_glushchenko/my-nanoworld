﻿using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.Utilities;


namespace NanoReality.GameLogic.Quests
{
    public class ExperienceAward : AwardAction, IExperienceAward
    {
        public override int AwardCount { get { return Experience; } }

        public override AwardProduct GetAwardProduct
        {
            get
            {
                return new AwardProduct(AwardType, Experience.ToString());
            }
        }

        public int Experience { get; set; }


        [Inject]
        public IPlayerProfile jPlayerProfile { get; set; }

        public override void Execute(AwardSourcePlace sourcePlace, string source)
        {
            base.Execute(sourcePlace, source);
            jPlayerProfile.CurrentLevelData.AddExperience(Experience, true);
        }

        public override void InitServerData()
        {
            base.InitServerData();
            Experience = int.Parse(SerializebleKeyValuePair.GetValue(ServerParams, "value_1"));
        }


        public override string GetAwardDescriptionText(IAwardActionDescription baseDescription)
        {
            return string.Format(baseDescription.Description, Experience);
        }
    }
}
