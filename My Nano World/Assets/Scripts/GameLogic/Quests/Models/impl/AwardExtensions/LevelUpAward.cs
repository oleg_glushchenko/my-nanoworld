﻿using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.Utilities;

namespace NanoReality.GameLogic.Quests
{
    public class LevelUpAward : AwardAction, ILevelUpAward
    {
        public override int AwardCount { get { return LevelUpCount; } }

        public override AwardProduct GetAwardProduct
        {
            get
            {
                return new AwardProduct(AwardType, LevelUpCount.ToString());
            }
        }

        public int LevelUpCount { get; set; }

        [Inject]
        public IPlayerProfile jPlayerProfile { get; set; }

        public override void Execute(AwardSourcePlace sourcePlace, string source)
        {
            base.Execute(sourcePlace, source);
            jPlayerProfile.CurrentLevelData.LevelUp(LevelUpCount);

        }

        public override void InitServerData()
        {
            base.InitServerData();
            LevelUpCount = int.Parse(SerializebleKeyValuePair.GetValue(ServerParams, "value_1"));
        }

        public override string GetAwardDescriptionText(IAwardActionDescription baseDescription)
        {
            return string.Format(baseDescription.Description, LevelUpCount);
        }
    }
}
