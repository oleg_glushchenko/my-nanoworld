﻿using System.Collections.Generic;
using Assets.NanoLib.Utilities;
using NanoReality.GameLogic.StartApplicationSequence.Models.api;

namespace NanoReality.GameLogic.Quests
{

    /// <summary>
    /// Объект контейнер хранящий список квестов в игре
    /// </summary>
    public interface IQuestBalanceData : IGameBalanceData
    {
        /// <summary>
        /// Список квестов в игре
        /// </summary>
        List<IQuest> QuestBalance { get; set; }

        /// <summary>
        /// Данные по уровню (на котором откроетя квест игроку) и сектору к которому квест относится (фарм, хеви, и тд.)
        /// </summary>
        List<QuestBalanceData.QuestLevelDataContainer> QuestLevelsData { get; set; }

        /// <summary>
        /// Поиск квеста по его айди
        /// </summary>
        /// <param name="questId">айди квеста</param>
        /// <returns>возвращает квест по его айди, или нулл, если не найдет такой квест</returns>
        IQuest GetQuest(Id<IQuest> questId);
    }
}
