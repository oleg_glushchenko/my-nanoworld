﻿using NanoReality.GameLogic.Quests;

interface ICityRenamedCondition : ICondition
{
    int RenamesNeed { get; set; }
}
