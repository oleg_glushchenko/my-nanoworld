﻿using NanoReality.GameLogic.Quests;

interface IAuctionPurchaseCondition : ICondition
{
    int PurchaseCount { get; set; }
}