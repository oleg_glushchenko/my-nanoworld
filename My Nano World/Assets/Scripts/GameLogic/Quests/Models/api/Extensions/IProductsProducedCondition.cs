﻿using Assets.NanoLib.Utilities;
using Assets.Scripts.GameLogic.Quests.Models.api.Extensions;
using NanoLib.Core;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using NanoReality.GameLogic.Quests;

public interface IProductsProducedCondition : IConditionWithProgress
{
    IntValue ProductType { get; set; }
    int Amount { get; set; }
}
