﻿using NanoReality.GameLogic.Quests;
   
public interface IOrderDeskCondition : ICondition
{
    int OrdersCompleteCount { get; set; }
}