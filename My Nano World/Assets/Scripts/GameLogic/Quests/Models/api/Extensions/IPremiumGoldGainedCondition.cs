﻿using NanoReality.GameLogic.Quests;

public interface IPremiumGoldGainedCondition : ICondition
{
    int CountPremiumGoldToAchieve { get; set; }
}
