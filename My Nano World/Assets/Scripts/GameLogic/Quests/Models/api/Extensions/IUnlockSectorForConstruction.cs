﻿using NanoReality.GameLogic.Quests;

/// <summary>
/// Условие очистки сектора для застройки
/// </summary>
interface IUnlockSectorForConstruction : ICondition
{
    int Amount { get; set; }
}
