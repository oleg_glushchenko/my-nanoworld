﻿using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.Quests;

namespace GameLogic.Quests.Models.api.Extensions
{
    public interface IBuildingHaveWithSubCategoryCondition : ICondition
    {
        BuildingSubCategories BuildingSubCategory { get; set; }
        int RequiredCount { get; set; }
    }
}
