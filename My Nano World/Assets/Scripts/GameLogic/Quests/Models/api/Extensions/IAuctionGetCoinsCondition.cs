﻿using NanoReality.GameLogic.Quests;

interface IAuctionGetCoinsCondition : ICondition
{
    int GetCoinsCount { get; set; }
}
