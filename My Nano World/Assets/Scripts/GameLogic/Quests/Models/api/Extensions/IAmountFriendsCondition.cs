﻿using NanoReality.GameLogic.Quests;

/// <summary>
/// Условия наличия ко-ва друзей в игре
/// </summary>
interface IAmountFriendsCondition : ICondition
{
    int AmountFriends { get; set; }
}