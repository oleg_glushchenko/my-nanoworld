﻿using NanoReality.GameLogic.Quests;

interface IAuctionSpendCoinsCondition: ICondition
{
    int SpendCoinsCount { get; set; }
}