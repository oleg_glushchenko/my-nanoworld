﻿namespace NanoReality.GameLogic.Quests
{
    /// <summary>
    /// Условие разблокировки по построенным зданиям N типа
    /// </summary>
    public interface IBuildBuildingByTypeCondition : ICondition
    {
        /// <summary>
        /// Количество зданий которые нужно построить
        /// </summary>
        int Amout { get; set; }

        /// <summary>
        /// Текущее количество построенных зданий
        /// </summary>
        int CurrentCount { get; set; }
    }
}
