﻿using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.Quests;

namespace GameLogic.Quests.Models.api.Extensions
{
    public interface IShuffleOrderTableCondition : ICondition
    {
        int BuildingTypeId { get; set; }
        BusinessSectorsTypes BusinessSector { get; set; }
    }
}
