﻿using NanoReality.GameLogic.Quests;

/// <summary>
/// Условие завершения N исследований
/// </summary>
public interface IResearchCondition : ICondition
{
}
