﻿using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;

namespace NanoReality.GameLogic.Quests
{
    public interface ISpendCurrencyCondition : ICondition
    {
        PriceType PriceType { get; set; }
        int RequiredCount { get; set; }
    }
}
