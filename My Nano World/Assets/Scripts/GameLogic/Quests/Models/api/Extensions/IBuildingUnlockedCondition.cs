﻿using NanoReality.GameLogic.Quests;

public interface IBuildingUnlockedCondition : ICondition
{
    Id_IMapObjectType UnlockedBuildingID { get; set; }
}
