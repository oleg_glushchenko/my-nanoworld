﻿namespace NanoReality.GameLogic.Quests
{
    /// <summary>
    /// Условие набора уровня игрока
    /// </summary>
    public interface IUserLevelAchievedCondition : ICondition
    {
        /// <summary>
        /// Уровень который нужно набрать
        /// </summary>
        int TargetUserLevel { get; set; }
    }
}