﻿namespace NanoReality.GameLogic.Quests
{
    /// <summary>
    /// Награда опыта игрока
    /// </summary>
    public interface IExperienceAward : IAwardAction
    {
        /// <summary>
        /// Кол-во опыта которое получит игрок
        /// </summary>
        int Experience { get; set; }
    }
}
