﻿namespace NanoReality.GameLogic.Quests
{
    /// <summary>
    /// Награда премиум кристаллов
    /// </summary>
    public interface INanoGemsAward : IAwardAction
    {
        /// <summary>
        /// Кол-во
        /// </summary>
        int NanoGemsCount { get; set; }
    }
}
