using UnityEngine.Purchasing;

namespace NanoReality.GameLogic.IAP
{
    public class StoreProductEvent : IStoreProduct
    {
        public string StoreId { get; }
        public ProductType ProductType { get; }
        public int Id { get; }
        public string Price { get; }

        public StoreProductEvent(string storeId, ProductType type, int id)
        {
            StoreId = storeId;
            ProductType = type;
            Id = id;
        }
    }
}