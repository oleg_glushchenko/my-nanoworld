﻿using strange.extensions.signal.impl;
using UnityEngine.Purchasing;

namespace NanoReality.GameLogic.IAP
{
	public class PurchaseFinishedSignal : Signal<IStoreProduct, bool> { }

	public class PurchaseOfferFinishedSignal : Signal { }
}