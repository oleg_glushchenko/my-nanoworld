﻿using System;
using NanoReality.Game.Services;

namespace NanoReality.GameLogic.IAP
{
	public interface IIapService : IGameService
	{
		InAppShopData InAppShopProducts { get; }
		InAppShopOffersData InAppShopOffers { get; }
        bool OffersUpdating { get;}
        void BuyProduct(string productId);
        void AddProduct(IStoreProduct product);
		void UpdateOffers();
		void InitOffersData(Action onComplete);
	}
}