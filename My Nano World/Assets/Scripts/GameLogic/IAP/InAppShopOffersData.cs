using Newtonsoft.Json;

namespace NanoReality.GameLogic.IAP
{
    [System.Serializable]
    public sealed class InAppShopOffersData
    {
        [JsonProperty("offers")]
        public InAppShopOffer[] Products;
    }
}
