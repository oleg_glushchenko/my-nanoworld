﻿using System;
using Assets.Scripts.GameLogic.Utilites;
using I2.Loc;
using NanoLib.Core.Logging;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;

namespace NanoReality.GameLogic.Utilites
{
    public static class LocalizationUtils
    {
        public static string SafeFormat(string format, params object[] args)
        {
            try
            {
                return string.Format(format, args);
            }
            catch (Exception exception)
            {
                $"Format \"{format}\" has more arguments than provided.".LogError();
                exception.LogException();

                return format;
            }
        }
        
        public static string GetMapObjectTypeText(MapObjectintTypes type)
        {
            return LocalizeL2(LocalizationKeyConstants.GetMapObjectTypeKey(type));
        } 
        
        public static string GetMapObjectCategoryText(MapObjectintTypes type)
        {
            return LocalizeL2(LocalizationKeyConstants.GetMapObjectCategoryKey(type));
        }
        
        public static string GetBuildingText(Id_IMapObjectType type)
        {
            return LocalizeL2(LocalizationKeyConstants.GetBuildingKey(type));
        }
        
        public static string GetMapObjectSubCategoryKey(BuildingSubCategories type)
        {
            return LocalizeL2($"UICommon/BUILDING_SHOP_FILTER_{type.ToString().ToUpper()}"); 
        }
        
        public static string GetPriceTypeKey(PriceType type)
        {
            return LocalizeL2($"UICommon/PRICE_TYPE_{type.ToString().ToUpper()}"); 
        }

        public static string GetProductText(int productType)
        {
            return LocalizeL2($"Product/OBJECT_PRODUCT_{productType}");
        }

        public static string GetQuestsConditionText(int productType, int textType = -1)
        {
            if (textType < 0)
            {
                return LocalizeL2($"Quests/OBJECT_CONDITION_DSCR_{productType}");
            }
            else
            {
                return LocalizeL2($"Quests/OBJECT_CONDITION_DSCR_{productType}_{textType}"); 
            }
        }

        public static string GetProductSourceText(Id_IMapObjectType buildingTypeId)
        {
            return Localize(LocalizationKeyConstants.SHARED_TOOLTIP_GET_FROM, GetBuildingText(buildingTypeId));
        }

        public static string  GetNotEnoughtMoneyText(PriceType priceType)
        {
            switch (priceType)
            {
                case PriceType.Hard:
                    return "UICommon/NOTENOUGHRES_MSG_NOT_ENOUGH_MONEY";
                    break;
                case PriceType.Gold:
                    return "UICommon/NEED_GOLD";
                    break;
                case PriceType.Soft:
                    return "UICommon/NEED_SOFT";
                    break;
                case PriceType.Lantern:
                    return "UICommon/NEED_LANTERN";
                    break;
                default:
                    return "UICommon/NOT_ENOUGH_CURRENCY_TEXT";
                    
            }

            return null;
        }

        public static string GetProductGoToSourceText(int buildingTypeId)
        {
            return Localize(LocalizationKeyConstants.TOOLTIP_BTN_GO_TO_BUILDING, GetBuildingText(buildingTypeId));
        }

        public static string GetSectorTypeText(BusinessSectorsTypes sectorId)
        {
            return Localize(LocalizationKeyConstants.GetSectorTypeKey(sectorId));
        }

        public static string GetQuestInfoDescription(int questId)
        {
            return LocalizeL2($"Quests/QUEST_DESCRIPTION_{questId}");
        }

        public static string GetQuestDescription(int questId)
        {
            return Localize(string.Format(LocalizationKeyConstants.Objects.QUEST_DSCR, questId));
        }
        
        public static string GetQuestTitle(int questId)
        {
            return LocalizeL2($"Quests/OBJECT_QUEST_{questId}");
        }
        
        public static string GetQuestDialogText(int questId)
        {
            return Localize(string.Format(LocalizationKeyConstants.Objects.QUEST_MSG_ON_COMPLETE, questId));
        }
        
        public static string GetLanguageText(string languageId, bool isArabic)
        {
            return LocalizeL2WithOverrideLanguage($"UICommon/LANGUAGE_TYPE_{languageId.ToUpper()}", isArabic ? "Arabic" : null);
        }

        public static string GetQuestText(int questId)
        {
            return Localize(LocalizationKeyConstants.GetQuestKey(questId));
        }

        public static string GetQuestEventText(int eventId)
        {
            return Localize(LocalizationKeyConstants.GetQuestEventKey(eventId));
        }

        public static string Localize(string key)
        {
            return LocalizationManager.GetTranslation($"Localization/{key}");
        }

        public static string LocalizeUI(string key)
        {
            return LocalizationManager.GetTranslation($"UICommon/{key}");
        }

        public static string GetUITerm(string key)
        {
            return $"UICommon/{key}";
        }

        public static string LocalizeL2WithOverrideLanguage(string key, string overrideLanguage = null)
        {
            return LocalizationManager.GetTranslation(key, true, 0,  true, false, null, overrideLanguage);
        }
        
        //TODO: remove Localize and use LocalizeL2
        public static string LocalizeL2(string key)
        {
            return LocalizationManager.GetTranslation(key); 
        }

        public static string Localize(string key, params object[] args)
        {
            return SafeFormat(LocalizationMLS.Instance.GetText(key), args);
        }
    }
}