﻿namespace NanoReality.GameLogic.Settings.GamePrefs
{
    public interface IGamePrefs
    {
        string DeviceId { get; set; }

        bool HasKey(string key);
        
        string GetString(string key);
        void SetString(string key, string value);
        
        int GetInt(string key);
        void SetInt(string key, int value);
        
        float GetFloat(string key);
        void SetFloat(string key, float value);

        bool GetBool(string key);
        void SetBool(string key, bool value);

        T GetObject<T>(string key) where T: class, new();
        void SetObject<T>(string key, T value) where T: class, new();
        
        void Save();

        void DeleteAll();
    }
}
