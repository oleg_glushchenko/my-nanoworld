﻿using System;
using System.Text;
using NanoLib.Core.Logging;
using NanoReality.Engine.Settings.BuildSettings;
using NanoReality.GameLogic.GameManager;
using Newtonsoft.Json;
using UnityEngine;

namespace NanoReality.GameLogic.Settings.GamePrefs
{
    /// <summary>
    /// Wrapper for UnityEngine.PlayerPrefs
    /// </summary>
    public class GamePrefs : IGamePrefs
    {
        [Inject] public NetworkSettings jNetworkSettings { get; set; }
        [Inject] public SignalOnApplicationQuit jSignalOnApplicationQuit { get; set; }
        [Inject] public SignalOnApplicationPause SignalOnApplicationPause { get; set; }

        #region Implementation of IGamePrefs

        public string DeviceId
        {
            get => PlayerPrefs.GetString(jNetworkSettings.CurrentServerName + ".device_id");
            set
            {
                PlayerPrefs.SetString(jNetworkSettings.CurrentServerName + ".device_id", value);
                PlayerPrefs.Save();
            }
        }

        public bool HasKey(string key)
        {
            key = ConvertToDeviceUniqueKey(key);
            return PlayerPrefs.HasKey(key);
        }

        public string GetString(string key)
        {
            key = ConvertToDeviceUniqueKey(key);
            return PlayerPrefs.GetString(key);
        }

        public void SetString(string key, string value)
        {
            key = ConvertToDeviceUniqueKey(key);
            PlayerPrefs.SetString(key, value);
        }

        public int GetInt(string key)
        {
            key = ConvertToDeviceUniqueKey(key);
            return PlayerPrefs.GetInt(key, 0);
        }

        public void SetInt(string key, int value)
        {
            key = ConvertToDeviceUniqueKey(key);
            PlayerPrefs.SetInt(key, value);
        }

        public float GetFloat(string key)
        {
            key = ConvertToDeviceUniqueKey(key);
            return PlayerPrefs.GetFloat(key, 0f);
        }

        public void SetFloat(string key, float value)
        {
            key = ConvertToDeviceUniqueKey(key);
            PlayerPrefs.SetFloat(key, value);
        }
        
        public bool GetBool(string key)
        {
            key = ConvertToDeviceUniqueKey(key);
            var intValue = PlayerPrefs.GetInt(key, default);
            return Convert.ToBoolean(intValue);
        }

        public void SetBool(string key, bool value)
        {
            key = ConvertToDeviceUniqueKey(key);
            var intValue = Convert.ToInt32(value);
            PlayerPrefs.SetInt(key, intValue);
        }

        public T GetObject<T>(string key) where T : class, new()
        {
            var json = PlayerPrefs.GetString(key, string.Empty);
            var value = JsonConvert.DeserializeObject<T>(json) ?? new T();

            $"Loaded {key} with value {json}, parsed to object {value}".Log(LoggingChannel.Saves);
            return value;
        }

        public void SetObject<T>(string key, T value) where T : class, new()
        {
            var json = JsonConvert.SerializeObject(value);
            PlayerPrefs.SetString(key, json);

            $"Saved {key} with value {json} to PlayerPrefs".Log(LoggingChannel.Saves);
        }

        public void Save()
        {
            PlayerPrefs.Save();
        }

        public void DeleteAll()
        {
            PlayerPrefs.DeleteAll();
        }

        #endregion

        [PostConstruct]
        public void PostConstruct()
        {
            jSignalOnApplicationQuit.AddListener(Save);
            SignalOnApplicationPause.AddListener(OnApplicationPause);
        }

        private void OnApplicationPause(bool pause)
        {
            if (pause)
            {
                Save();
            }
        }

        private string ConvertToDeviceUniqueKey(string key)
        {
            var deviceId = DeviceId;
            
            var stringBuilder = new StringBuilder(deviceId);
            stringBuilder.Append(".");
            stringBuilder.Append(key);
            
            var result = stringBuilder.ToString();
            
            return result;
        }
    }
}