﻿using NanoReality.Game.UI.SettingsPanel;

namespace NanoReality.GameLogic.Settings.GameSettings.Models.Api
{
    public interface IGameSettings 
    {
        bool IsMusicEnabled { get; set; }
        bool IsEffectsEnabled { get; set; }
        bool IsPushNotificationsEnabled { get; set; }
        bool IsHighQualityGraphic { get; set; }
        bool GraphicWasChanged { get; set; }

        bool IsEnabled(SettingsToggleType type);
        void SetEnabled(SettingsToggleType type, bool value);
    }
}