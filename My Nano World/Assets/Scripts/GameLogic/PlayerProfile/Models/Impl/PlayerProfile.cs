﻿using System;
using System.Collections.Generic;
using Assets.NanoLib.UtilitesEngine;
using Assets.NanoLib.Utilities;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using NanoReality.Game.Tutorial;
using NanoReality.GameLogic.Configs;
using Newtonsoft.Json;

namespace NanoReality.GameLogic.PlayerProfile.Models.Api
{
    public class PlayerProfile :  IPlayerProfile
    {
        [Inject] public IPlayerExperience CurrentLevelData { get; set; }
        [Inject] public IPlayerResources PlayerResources { get; set; }
        [Inject] public ITimerManager jTimerManager { get; set; }
        [Inject] public IGameConfigData jGameConfigData { get; set; }
        
        private ITimer _dailyBonusTimer;
        private float _startDailyBonusTime;
        private ITimer _burgersTimer;
        private double _burgersCooldownFinishSeconds;
        private int _dailyBonusDay;
        
        #region JSON Data
        
        [JsonProperty("id")]
        public Id<IPlayerProfile> Id { get; set; }

        [JsonProperty("level")]
        public int CurrentLevel { get; set; }
        
        [JsonProperty("experience")]
        public int CurrentExp { get; set; }

        [JsonProperty("nano_coins")] 
        public int NanoCoins { get; private set; }

        [JsonProperty("lanterns")]
        public int Lanterns { get; private set; }

        [JsonProperty("premium_coins")] 
        private int PremiumCoins{ get; set; }

        [JsonProperty("gold")] 
        private int Gold { get; set; }

        [JsonProperty("nano_gems")] 
        private int NanoGems{ get; set; }

        [JsonProperty("max_trade_slots")] 
        public int AvailableTradeSlots { get; set; }

        [JsonProperty("user_products")]
        private Dictionary<Id<Product>, int> _warehouseProducts;
        
        [JsonProperty("tutorial_step")]
        public StepType TutorialStep { get; set; }
        
        [JsonProperty("city")]
        public IUserCity UserCity { get; set; }
        
        [JsonProperty("seconds_since_registration")]
        public int SecondsSinceRegistration { get; set; }
        
        [JsonProperty("social_networks")]
        public Dictionary<string, string> SocialAccounts { get; set; }
        
        [JsonProperty("watch_video")]
        public Dictionary<AdTypePanel, AdStructData> AdData { get; set; }
        
        [JsonProperty("daily_bonus_collected_at")]
        public int DailyBonusCollectedAt { get; set; }
        
        [JsonProperty("daily_bonus_day")]
        public int DailyBonusDay
        {
            get => _dailyBonusDay;
            set => _dailyBonusDay = value >= 30 ? 0 : value;
        }

        [JsonProperty("is_premium")]
        public bool IsPremiumUser { get; set; }

        #endregion
        
        private float DailyBonusTime { get; set; }
        
        public bool IsDailyBonusCollectedToday => TimeSpan.FromSeconds(DailyBonusTime) <= TimeSpan.FromHours(24) && DailyBonusCollectedAt != 0;

        [PostConstruct]
        public void PostConstructInit()
        {
            _warehouseProducts = new Dictionary<Id<Product>, int>();
            SocialAccounts ??= new Dictionary<string, string>();
        }
        
        public void Init()
        {
            PlayerResources.InitPlayerResources(NanoCoins, PremiumCoins, NanoGems, Gold, Lanterns, _warehouseProducts);
            DailyBonusCollectedAt = DailyBonusDay == 0 ? 0 : DailyBonusCollectedAt;

            StartDailyBonusTiming();
        }

        public void UpdateUser()
        {
            PlayerResources.InitPlayerResources(NanoCoins, PremiumCoins, NanoGems, Gold, Lanterns, _warehouseProducts);
        }

        private void StartDailyBonusTiming()
        {
            _dailyBonusTimer?.CancelTimer();
            _startDailyBonusTime = jGameConfigData.DailyBonusInterval - DailyBonusCollectedAt;
            _dailyBonusTimer = jTimerManager.StartServerTimer(_startDailyBonusTime, OnFinishDailyBonusTiming, OnDailyBonusTimerTick);
        }

        public void ResetDailyBonusTimer()
        {
            DailyBonusCollectedAt = 1;
            _dailyBonusTimer?.CancelTimer();
            _dailyBonusTimer = null;
            StartDailyBonusTiming();
        }

        private void OnFinishDailyBonusTiming()
        {
            DailyBonusDay = 0;
            ResetDailyBonusTimer();
        }

        private void OnDailyBonusTimerTick(float currTime)
        {
            DailyBonusTime = currTime + DailyBonusCollectedAt;
        }
    }
}