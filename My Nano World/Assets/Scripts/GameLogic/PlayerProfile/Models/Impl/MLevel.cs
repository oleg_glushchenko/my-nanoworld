﻿using Assets.NanoLib.Utilities;
using Newtonsoft.Json;


public class MLevel 
{
    /// <summary>
    /// experience for level up
    /// </summary>
    [JsonProperty("experience")]
    public int Experience { get; set; }

    [JsonProperty("id")]
    public Id<MLevel> ID { get; set; }

    [JsonProperty("gift_box_type")]
    public int GiftBoxType { get; set; }
}
