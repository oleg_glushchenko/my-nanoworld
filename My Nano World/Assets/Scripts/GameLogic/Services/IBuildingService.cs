﻿using System;
using System.Collections.Generic;
using Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Models.impl;
using NanoReality.Engine.UI.Views.BuildingsShopPanel;
using NanoReality.Game.Services;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.ServerCommunication.Models.Api;

namespace NanoReality.GameLogic.Services
{
    public interface IBuildingService : IGameService
    {
        BuildingsShopItemData GetBuildingShopItemData(IMapObject mapObject);

        void SyncBuilding(MapBuilding mapObject, Action<SyncBuildingResponse> successCallback);

        void FinalizeMapObjectConstruction(IMapBuilding mapObject, Action<SyncBuildingResponse> successCallback = null);

        void ConstructMapObject(IMapObject building, Action<int> successCallback = null);
        
        void ConstructRoad(int businessSectorId, int type, List<IMapObject> roadsList, Action<CreateRoadResponse> onSuccess = null);

        /// <summary>
        /// Returns construction time for next level of concrete building instance.
        /// </summary>
        /// <param name="buildingMapId"></param>
        /// <returns></returns>
        int GetBuildingConstructionTime(int buildingMapId);

        int GetBuildingConstructionTime(int buildingId, int buildingLevel, int instanceIndex);

        int GetBuildingConstructionReward(int buildingMapId);
        
        /// <summary>
        /// Returns time multiplier value for specified object depends on how much objects with the same types are present on the map
        /// </summary>
        int GetConstructionTime(IMapObject mapObject);
    }
}
