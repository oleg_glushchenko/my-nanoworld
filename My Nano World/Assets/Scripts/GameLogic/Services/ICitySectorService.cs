using System.Collections.Generic;
using GameLogic.BuildingSystem.CityArea;
using NanoReality.Game.Services;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Impl;
using UnityEngine;

namespace NanoReality.GameLogic.Services
{
    public interface ICitySectorService : IGameService
    {
        bool ContainsDisableAreas(BusinessSectorsTypes sectorType, Vector2Int position, int objectTypeId);

        void UpdateLockedMap(MapSize mapSize, List<CityLockedSector> lockedSectors, BusinessSectorsTypes businessSectorsTypes);

        bool ContainsDisabledAreas(Vector2Int leftBottomPos, Vector2Int dimension, IReadOnlyList<ISectorArea> areas,
            int objectTypeId);
        IReadOnlyList<ISectorArea> SectorAreas(BusinessSectorsTypes sectorType);
        IReadOnlyList<CityLockedSector> LockedSectorsByCityType(BusinessSectorsTypes sectorType);
        
        void SetDragLimits(Rect? newLimits);

        bool IsInsideOfDragArea(Vector2Int position, Vector2Int dimensions);
    }
}
