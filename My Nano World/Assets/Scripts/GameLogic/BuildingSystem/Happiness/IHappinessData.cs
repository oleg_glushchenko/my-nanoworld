﻿namespace NanoReality.GameLogic.BuildingSystem.Happiness
{
    public interface IHappinessData
    {
        int TotalHappinessPercent { get; }
        void UpdateData(int happinessPercent);
    }
}
