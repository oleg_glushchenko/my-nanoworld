﻿using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Factories;
using strange.extensions.signal.impl;
using Assets.NanoLib.Utilities;

namespace NanoReality.GameLogic.BuildingSystem.MapObjects.Controllers.Factories
{
    /// <summary>
    /// Вызывается при отгрузке продукта из слота в склад
    /// </summary>
    public class FactoryShipSlotSignal : Signal<Id_IMapObject, Id<IProduceSlot>> { }

    /// <summary>
    /// Вызывается при окончании разработки продукта
    /// </summary>
    public class SignalFactoryProduceSlotFinished : Signal<Id_IMapObject> { }

    /// <summary>
    /// Вызывается при окончании разработки продукта
    /// </summary>
    public class ProductReadySignal : Signal<IProduceSlot, Id_IMapObject> { }
    
    /// <summary>
    /// Call when producing panel opens or closes to apply filter to ready products
    /// </summary>
    public class ApplyFilterToReadyProducts : Signal<Id_IMapObject, bool> { }

    public class StartProductionSignal : Signal<IProduceSlot> { }

    /// <summary>
    /// Убаляем итем продукта
    /// </summary>
    public class RemoveProductSlotSignal : Signal<IProduceSlot> { }

    /// <summary>
    /// Вызывыается при сборе продукта над зданием (перед отправкой на сервер)
    /// Используется для блокирования повторного сбора продукта
    /// </summary>
    public class CollectProductAboveBuildingStartSignal : Signal<IProduceSlot> { }
    
    /// <summary>
    /// Вызывыается после сборе продукта над зданием (после ответа от сервера)
    /// Используется для обновления стутуса слотов
    /// </summary>
    public class CollectProductAboveBuildingCompleteSignal : Signal<IProduceSlot> { }
}
