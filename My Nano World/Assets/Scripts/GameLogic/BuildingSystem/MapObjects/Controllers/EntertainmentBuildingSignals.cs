﻿using strange.extensions.signal.impl;

namespace NanoReality.GameLogic.BuildingSystem.MapObjects.Controllers
{
    public class StartEntertainmentProductionSignal : Signal<Id_IMapObject>
    {
    }

    public class EndEntertainmentProductionSignal : Signal<Id_IMapObject>
    {

    }

    public class SignalEntertainmentBuildingNeedToShipMoney : Signal<Id_IMapObject>
    {

    }
}
