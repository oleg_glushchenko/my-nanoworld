﻿namespace NanoReality.GameLogic.BuildingSystem.MapObjects
{
    public enum AgriculturalFieldState
    {
        Empty = 0,
        FirstGrowthPhase = 1,
        SecondGrowthPhase = 2,
        Completed = 3
    }
}