﻿using Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Models.impl;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;

namespace Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Models.api.CityOrderDesks
{
    public class CityOrderDeskMapBuilding : MapBuilding, ICityOrderDeskMapBuilding
    {
        public override bool IsNeedRoad => false;
        
        public override MapObjectintTypes MapObjectType
        {
            get => MapObjectintTypes.CityOrderDesk;
            set { }
        }
        
        public override object Clone()
        {
            var result = Binder.GetInstance<ICityOrderDeskMapBuilding>();
            CopyMembersTo(result);
            return result;
        }
        
        public override bool IsFunctional()
        {
            return true;
        }
    }
}