﻿namespace NanoReality.GameLogic.BuildingSystem.MapObjects.Models.impl.AgriculturalFields
{
    public enum PlantType
    {
        Wheat,
        Rice,
        Maize,
        Rye,
        Tomato,
        Carrot,
        Salad,
        Cucumber,
        SugarCane,
        Broccoli,
        Eggplant,
        Tea,
        Cola,
        Strawberries,
        Blueberries
    }
}
