﻿using System;
using System.Collections.Generic;
using System.Linq;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using UnityEngine;

namespace NanoReality.GameLogic.BuildingSystem.MapObjects
{
    public static class AoeUtilities
    {
        /// <summary>
        /// Возвращает оффсет позиции центра аое, относительно здания
        /// </summary>
        /// <param name="layout"></param>
        /// <param name="buildingSize"></param>
        /// <param name="aoeSize"></param>
        /// <returns></returns>
        public static Vector2 GetAoeOffsetPosition(AoeLayout layout, Vector2 buildingSize, Vector2 aoeSize)
        {
            var aoePosition = Vector3.zero;

            switch (layout)
            {
                case AoeLayout.BottomRight:
                    aoePosition = new Vector2(0, buildingSize.y - aoeSize.y);
                    break;
                case AoeLayout.BottomLeft:
                    aoePosition = new Vector2(buildingSize.y - aoeSize.y, buildingSize.x - aoeSize.x);
                    break;
                case AoeLayout.TopRight:
                    aoePosition = Vector2.zero;
                    break;
                case AoeLayout.TopLeft:
                    aoePosition = new Vector2(buildingSize.x - aoeSize.x, 0);
                    break;
                case AoeLayout.Center:
                    aoePosition = new Vector2((buildingSize.x - aoeSize.x) / 2, (buildingSize.y - aoeSize.y) / 2);
                    break;
                default:
                    Debug.LogError($"Unexpected aoe layout: {layout}");
                    break;
            }

            return aoePosition;
        }

        public static AoeLayout GetPreviousAoeLayout(this AoeLayout layout)
        {
            switch (layout)
            {
                case AoeLayout.BottomRight:
                    return AoeLayout.TopRight;
                case AoeLayout.TopRight:
                    return AoeLayout.TopLeft;
                case AoeLayout.TopLeft:
                    return AoeLayout.BottomLeft;
                case AoeLayout.BottomLeft:
                    return AoeLayout.BottomRight;
            }

            return layout;
        }

        public static bool IsGridCellCoveredByAoe(AoeLayout currentLayout, Vector2 aoeAreaPivot, Vector2 aoeAreaSize, IMapObject mapObject)
        {
            Vector2 dimensions = mapObject.Dimensions.ToVector2();
            Vector2 mapObjectPosition = mapObject.MapPosition.ToVector2();

            // Need swipe layout size in this case.
            if (currentLayout == AoeLayout.TopRight || currentLayout == AoeLayout.BottomLeft)
            {
                aoeAreaSize = new Vector2(aoeAreaSize.y, aoeAreaSize.x);
            }

            bool xPositionValid = false;
            bool yPositionValid = false;
            for (int xCounter = 0; xCounter < dimensions.x; xCounter++)
            {
                for (int yCounter = 0; yCounter < dimensions.y; yCounter++)
                {
                    var cellPosition = mapObjectPosition + new Vector2(xCounter, yCounter);
                    xPositionValid = cellPosition.x >= aoeAreaPivot.x &&
                                         cellPosition.x < aoeAreaPivot.x + aoeAreaSize.x;
                    yPositionValid = cellPosition.y >= aoeAreaPivot.y &&
                                         cellPosition.y < aoeAreaPivot.y + aoeAreaSize.y;
                    if (!xPositionValid || !yPositionValid)
                    {
                        break;
                    }
                }
                
                if (!xPositionValid || !yPositionValid)
                {
                    break;
                }
            }

            return xPositionValid && yPositionValid;
        }

        public static bool IsBuildingCovered(IEnumerable<AoeInfoData> aoePoints, IMapObject checkingBuilding)
        {
            Vector2 dimensions = checkingBuilding.Dimensions.ToVector2();
            Vector2 mapObjectPosition = checkingBuilding.MapPosition.ToVector2();
            IEnumerable<AoeInfoData> allSectorAoeAreas = aoePoints.ToList();

            bool isAllCellsInAoe = false;
            for (int xCounter = 0; xCounter < dimensions.x; xCounter++)
            {
                for (int yCounter = 0; yCounter < dimensions.y; yCounter++)
                {
                    var cellPosition = mapObjectPosition + new Vector2(xCounter, yCounter);
                    bool isCoveredByAnyAoe = false;
                    foreach (AoeInfoData pivotSizeValue in allSectorAoeAreas)
                    {
                        isCoveredByAnyAoe = isCoveredByAnyAoe || IsPointInArea(pivotSizeValue.PivotPosition, pivotSizeValue.Size, cellPosition);
                    }

                    isAllCellsInAoe = isCoveredByAnyAoe;

                    if (isAllCellsInAoe == false)
                    {
                        break;
                    }
                }

                if (isAllCellsInAoe == false)
                {
                    break;
                }
            }

            return isAllCellsInAoe;
        }

        public static bool IsPointInArea(Vector2 areaPivot, Vector2 areaSize, Vector2 checkedPointPosition)
        {
            bool xPositionValid;
            bool yPositionValid;
            
            xPositionValid = checkedPointPosition.x >= areaPivot.x &&
                             checkedPointPosition.x < areaPivot.x + areaSize.x;
            yPositionValid = checkedPointPosition.y >= areaPivot.y &&
                             checkedPointPosition.y < areaPivot.y + areaSize.y;

            return xPositionValid && yPositionValid;
        }

        public static Vector2 CalculateAoeAreaPivot(Vector2 mapObjectPosition, Vector2 mapObjectDimensions, AoeLayout aoeLayout, Vector2 aoeAreaSize)
        {
            Vector2 areaZonePivot;

            switch (aoeLayout)
            {
                case AoeLayout.TopLeft:
                {
                    var x = (mapObjectPosition.x + mapObjectDimensions.x) - aoeAreaSize.x;
                    areaZonePivot = new Vector2(x, mapObjectPosition.y);
                    break;
                }

                case AoeLayout.BottomLeft:
                {
                    // Need swipe layout size in this case.
                    var x = (mapObjectPosition.x + mapObjectDimensions.x) - aoeAreaSize.y;
                    var y = (mapObjectPosition.y  + mapObjectDimensions.y)- aoeAreaSize.x;
                    areaZonePivot = new Vector2(x, y);
                    break;
                }

                case AoeLayout.TopRight:
                {
                    areaZonePivot = mapObjectPosition;
                    break;
                }

                case AoeLayout.BottomRight:
                {
                    var y = (mapObjectPosition.y + mapObjectDimensions.y) - aoeAreaSize.y;
                    areaZonePivot = new Vector2(mapObjectPosition.x, y);
                    break;
                }
                
                case AoeLayout.Center:
                {
                    var y = mapObjectPosition.y - (aoeAreaSize.y - mapObjectDimensions.y) / 2;
                    var x = mapObjectPosition.x - (aoeAreaSize.x - mapObjectDimensions.x) / 2;
                    areaZonePivot = new Vector2(x, y);
                    break;
                }

                default:
                    throw new ArgumentOutOfRangeException(nameof(aoeLayout), aoeLayout, null);
            }

            return areaZonePivot;
        }

        /// <summary>
        /// Need for correct handling size for some layouts.
        /// </summary>
        /// <param name="layout"></param>
        /// <param name="aoeSize"></param>
        /// <returns></returns>
        public static Vector2 ConvertAreaToRealSize(AoeLayout layout, Vector2 aoeSize)
        {
            if (layout == AoeLayout.TopRight || layout == AoeLayout.BottomLeft)
            {
                aoeSize = new Vector2(aoeSize.y, aoeSize.x);
            }

            return aoeSize;
        }
    }

    /// <summary>
    /// Helper class for keeping information of AOE instance.
    /// </summary>
    public struct AoeInfoData
    {
        /// <summary>
        /// Layout type.
        /// </summary>
        public AoeLayout Layout;
        
        /// <summary>
        /// Grid position.
        /// </summary>
        public Vector2 PivotPosition;
        
        /// <summary>
        /// In grid cells..
        /// </summary>
        public Vector2 Size;

        public AoeInfoData(AoeLayout layout, Vector2 pivot, Vector2 size)
        {
            Layout = layout;
            PivotPosition = pivot;
            Size = size;
        }
    }
}