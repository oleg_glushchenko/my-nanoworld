﻿using Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Models.impl;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;

namespace NanoReality.GameLogic.BuildingSystem.MapObjects.Models.impl
{
    public class TradingMarketBuilding : MapBuilding, ITradingMarketBuilding
    {
        public override MapObjectintTypes MapObjectType
        {
            get => MapObjectintTypes.TradingMarket;
            set { }
        }

        public override bool IsFunctional()
        {
            return !IsLocked;
        }

        public override object Clone()
        {
            var result = Binder.GetInstance<ITradingMarketBuilding>();
            CopyMembersTo(result);
            return result;
        }
    }
}