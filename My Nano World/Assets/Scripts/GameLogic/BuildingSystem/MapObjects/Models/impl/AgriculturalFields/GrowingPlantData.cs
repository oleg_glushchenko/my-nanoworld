﻿using Assets.NanoLib.Utilities;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.AgriculturalFields;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
namespace NanoReality.GameLogic.BuildingSystem.MapObjects.Models.impl.AgriculturalFields
{
    /// <summary>
    /// Сельскохозяйственная культура
    /// </summary>
    public class GrowingPlantData : IGrowingPlantData
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        //[JsonProperty("plant_id")]
        public Id<Product> PlantId { get; set; }

        /// <summary>
        /// Необходимое количество единиц культуры для засеивания поля
        /// </summary>
        //[JsonProperty("in_amount")]
        public int IncomingAmount { get; set; }

        /// <summary>
        /// Количество единиц культуры получаемое с урожая
        /// </summary>
        //[JsonProperty("out_amount")]
        public int OutcomingAmount { get; set; }

        /// <summary>
        /// Врямя полного созревания культуры
        /// </summary>
        //[JsonProperty("producing_time")]
        public int GrowingTime { get; set; }

        /// <summary>
        /// Тип культуры
        /// </summary>
        //[JsonProperty("plant_type")]
        public PlantType PlantType { get; set; }

        /// <summary>
        /// Уровень, необходимый для разблокировки культуры
        /// </summary>
        //[JsonProperty("level_opend")]
        public int LevelOpend { get; set; }

        /// <summary>
        /// Количество занимаемого места в таре для сбора урожая
        /// </summary>
        //[JsonProperty("quantity")]
        public int Quantity { get; set; }
    }
}
