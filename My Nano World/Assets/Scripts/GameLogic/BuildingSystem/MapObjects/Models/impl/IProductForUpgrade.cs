﻿using Assets.NanoLib.Utilities;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;

public interface IProductForUpgrade
{
    Id<Product> ProductID { get; set; }

    int ProductsCount { get; set; }

    bool IsLoaded { get; set; }
}