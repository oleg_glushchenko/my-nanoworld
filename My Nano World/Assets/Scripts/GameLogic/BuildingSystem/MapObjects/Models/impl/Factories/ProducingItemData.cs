﻿using System.Collections.Generic;
using Assets.NanoLib.Utilities;
using NanoLib.Core;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Factories;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using NanoReality.GameLogic.Configs;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.Game.Tutorial;
using Newtonsoft.Json;


namespace NanoReality.GameLogic.BuildingSystem.MapObjects.Models.impl.Factories
{
    public class ProducingItemData : IProducingItemData
    {
        /// <summary>
        /// Dictionary of goods needed to produce new good
        /// </summary>
        [JsonProperty("incoming_goods")]
        public Dictionary<Id<Product>, int> IncomingProducts { get; set; }

        /// <summary>
        /// Good to produce
        /// </summary>
        [JsonProperty("out_coming_good")]
        public IntValue OutcomingProducts { get; set; }

        /// <summary>
        /// Count to produce
        /// </summary>
        [JsonProperty("amount")]
        public int Amount { get; set; }

        [JsonProperty("producing_time")]
        public float ProducingTime { get; set; }
        
        [Inject] public IPlayerProfile jPlayerProfile { get; set; }

        /// <summary>
        /// Возвращает список продуктов, которых не хватает для производства
        /// </summary>
        /// <returns>список продуктов, которых не хватает для производства</returns>
        public Dictionary<Id<Product>, int> GetNotEnoughProductsList()
        {
            var result = new Dictionary<Id<Product>, int>();

            foreach (var product in IncomingProducts)
            {
                // если такого продукта никогда не было в ресурсах или сейчас он равен 0
				if (jPlayerProfile.PlayerResources.GetProductCount(product.Key) == 0)
                {
                    result.Add(product.Key, product.Value);
                    continue;
                }

				if(jPlayerProfile.PlayerResources.GetProductCount(product.Key) < product.Value)
                {
					result.Add(product.Key, product.Value - jPlayerProfile.PlayerResources.GetProductCount(product.Key));
                }
            }

            return result;
        }

        public ProducingItemData()
        {
            if (IncomingProducts == null)
                IncomingProducts = new Dictionary<Id<Product>, int>();
        }
        
        public object Clone()
        {
            return new ProducingItemData
            {
                IncomingProducts = new Dictionary<Id<Product>, int>(IncomingProducts),
                OutcomingProducts = OutcomingProducts,
                Amount = Amount,
                ProducingTime = ProducingTime
            };
        }
    }
}