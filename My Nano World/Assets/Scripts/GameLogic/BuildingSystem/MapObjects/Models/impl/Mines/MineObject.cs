﻿using Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Controllers;
using Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Models.api.Mines;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Factories;
using GameLogic.BuildingSystem.MapObjects.Models.impl;

namespace Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Models.impl.Mines
{
    public class MineObject : ProductionBuilding, IMineObject
    {
        public override MapObjectintTypes MapObjectType
        {
            get => MapObjectintTypes.Mine;
            set { }
        }
        
        [Inject] public SignalMineShipSlot jSignalMineShipSlot { get; set; }
        [Inject] public SignalOnMineProduceSlotFinished jSignalOnMineProduceSlotFinished { get; set; }

        public override object Clone()
        {
            IMineObject result = Binder.GetInstance<IMineObject>();
            CopyMembersTo(result);
            return result;
        }

        protected override void FinishVerifyProduceGood(IProduceSlot sender)
        {
            jSignalMineShipSlot.Dispatch(MapID, sender);
            jSignalOnMineProduceSlotFinished.Dispatch(MapID);
        }

        protected override float QueueProductionTime()
        {
	        return 0;
        }
        
        protected override void RemoveResources(IProducingItemData cachedStartProducingItemData) { }
    }
}
