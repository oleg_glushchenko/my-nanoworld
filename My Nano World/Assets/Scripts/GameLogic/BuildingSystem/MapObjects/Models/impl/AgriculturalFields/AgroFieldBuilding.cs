﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.NanoLib.UtilitesEngine;
using Assets.NanoLib.Utilities;
using Assets.Scripts.Engine.Analytics;
using Assets.Scripts.Engine.Analytics.Model;
using Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Models.impl;
using Assets.Scripts.GameLogic.BuildingSystem.Products.Models.api;
using NanoLib.Core.Services.Sound;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Controllers.Agricultural;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Factories;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.impl.Factories;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using Newtonsoft.Json;
using strange.extensions.signal.impl;
using NanoReality.GameLogic.Common.Controllers.Signals;
using NanoReality.GameLogic.PlayerProfile.Models.Impl;

namespace NanoReality.GameLogic.BuildingSystem.MapObjects
{
    public class AgroFieldBuilding : MapBuilding, IAgroFieldBuilding
    {
        [Inject] public IProductsData jProductsData { get; set; }
        [Inject] public OnChangeGrowingState jOnChangeGrowingState { get; set; }
        [Inject] public OnProductProduceAndCollectSignal jOnProductProduceAndCollectSignal { get; set; }
        [Inject] public SignalOnStartProduceGoodOnFactory jSignalOnStartProduceGoodOnFactory { get; set; }
        [Inject] public SignalOnSkipProduction jSignalOnSkipProduction { get; set; }
        [Inject] public SignalOnNanoGemsDropped jSignalOnNanoGemsDropped { get; set; }
        
        public event Action<IAgroFieldBuilding> OnGrowingStart;
        public event Action<IAgroFieldBuilding> OnGrowingStartVerified;
        
        public Signal OnHarvestStarted { get; } = new Signal();
        public Signal<bool> OnHarvestEnded { get; } = new Signal<bool>();
        public Signal<bool> OnHarvestEndedVerified { get; } = new Signal<bool>();

        public Signal<IProducingItemData> OnFinishGrowing { get; } = new Signal<IProducingItemData>();
        public Signal<IProducingItemData> OnFinishGrowingVerified { get; } = new Signal<IProducingItemData>();

        private bool _timerStarted;
        private ITimer _timer;

        private const float SecondGrowthPhasePercent = 50f;

        [JsonProperty("rules")]
        private AgroFieldProductionRules _jsonData = new AgroFieldProductionRules();

        public override MapObjectintTypes MapObjectType
        {
            get => MapObjectintTypes.AgriculturalField;
            set { }
        }
        
        public IProducingItemData CurrentGrowingPlant { get; set; }

        [JsonProperty("current_producing_good")]
        private CurrentProducingGood currentProducingGood = new CurrentProducingGood();
        
        public AgriculturalFieldState CurrentState
        {
            get
            {
                if(CurrentGrowingPlant == null) 
                    return AgriculturalFieldState.Empty;

                if (CurrentGrowingPlant.ProducingTime <= 0 || GrowingTimeLeft <= 0f)
                    return AgriculturalFieldState.Completed;

                var growthPercent = (CurrentGrowingPlant.ProducingTime - GrowingTimeLeft) * 100f / CurrentGrowingPlant.ProducingTime;
                return growthPercent < SecondGrowthPhasePercent 
                    ? AgriculturalFieldState.FirstGrowthPhase 
                    : AgriculturalFieldState.SecondGrowthPhase;
            }
        }
        
        public float GrowingTimeLeft { get; set; }

        /// <summary>
        /// Общее время, прошедшее с начала выращивания
        /// </summary>
        private float _totalGrowingTime;
        
        public bool IsHarvesting { get; set; }
        
        public List<IProducingItemData> AvailableAgroProducts
        {
            get => _jsonData.AvaliableProducingGoods;
            set => _jsonData.AvaliableProducingGoods = value;
        }
        
        public override void InitFromPrototype(IMapObject prototype)
        {
            base.InitFromPrototype(prototype);

            var obj = (IAgroFieldBuilding)prototype;

            AvailableAgroProducts = obj.AvailableAgroProducts;
            
            if (currentProducingGood != null && currentProducingGood.IncomingGoods != null)
            {
                CurrentGrowingPlant = new ProducingItemData();
                CurrentGrowingPlant.Amount = currentProducingGood.Amount;
                CurrentGrowingPlant.ProducingTime = (int)currentProducingGood.FinishTime;
                CurrentGrowingPlant.IncomingProducts = new Dictionary<Id<Product>, int>();
                CurrentGrowingPlant.IncomingProducts.Add(currentProducingGood.ProductId, currentProducingGood.Amount);
                CurrentGrowingPlant.OutcomingProducts = currentProducingGood.ProductId;
                GrowingTimeLeft = CurrentGrowingPlant.ProducingTime;
            }
            
            AvailableAgroProducts.Sort(CompareProducingItemData);
        }
        
        private int CompareProducingItemData(IProducingItemData a, IProducingItemData b)
        {
            return jProductsData.GetProduct(a.OutcomingProducts).UnlockLevel.CompareTo(
                jProductsData.GetProduct(b.OutcomingProducts).UnlockLevel);
        }

        public override void InitializeBuildingTimers()
        {
            base.InitializeBuildingTimers();
            
            if (GrowingTimeLeft > 0 && CurrentGrowingPlant != null)
            {
                _timer?.CancelTimer();
                _timer = null;

                _timerStarted = true;
                
                _timer = jTimerManager.StartServerTimer(GrowingTimeLeft, OnFinishGrow, OnGrowTick);

                return;
            }
            
            _timerStarted = false;
        }

        public void StartHarvesting()
        {
            if (IsHarvesting) return;
            Harvest();
        }

        protected override void CopyMembersTo(IMapObject result)
        {
            base.CopyMembersTo(result);
            var obj = (AgroFieldBuilding)result;
            obj._jsonData = _jsonData;
            obj.currentProducingGood = currentProducingGood;
            obj.GrowingTimeLeft = GrowingTimeLeft;
            obj.AvailableAgroProducts = AvailableAgroProducts;
            
            AvailableAgroProducts.Sort(CompareProducingItemData);
        }

        public override object Clone()
        {
            var result = Binder.GetInstance<IAgroFieldBuilding>();
            CopyMembersTo(result);
            return result;
        }

        #region Overrides of MapBuilding

        public override bool IsFunctional()
        {
            return base.IsFunctional() && !IsHarvesting;
        }

        #endregion

        public void StartGrowing(IProducingItemData plant)
        {
            if (IsHarvesting) return;

            if (plant == null)
            {
                return;
            }

            if (CurrentState != AgriculturalFieldState.Empty)
            {
                return;
            }

            KeyValuePair<Id<Product>, int> pl = plant.IncomingProducts.First();

            if (AvailableAgroProducts.Count(p => p.IncomingProducts.Keys.Contains(pl.Key)) == 0)
            {
                return;
            }

            SendStartGrowingToServer(plant, pl);
        }

        private void SendStartGrowingToServer(IProducingItemData plant, KeyValuePair<Id<Product>, int> pl)
        {
            jServerCommunicator.StartGrowing (GetMapIDString, pl.Key, () =>
            {
                OnGrowingStartVerified?.Invoke(this);
            });
            jSignalOnStartProduceGoodOnFactory.Dispatch(new ProductionStartInfo(pl.Key.Value, BalanceName, plant));

            jSoundManager.Play(SfxSoundTypes.PlotPlanting, SoundChannel.SoundFX);

            float timeToProduce = plant.ProducingTime;
            var prod = new Dictionary<Id<Product>, int>
            {
                {pl.Key, pl.Value}
            };
            jPlayerProfile.PlayerResources.RemoveProducts(prod);
            CurrentGrowingPlant = (ProducingItemData)plant.Clone();
            GrowingTimeLeft = timeToProduce;
            _totalGrowingTime = 0f;
            jOnChangeGrowingState.Dispatch(this);
            _timer = jTimerManager.StartServerTimer(timeToProduce, OnFinishGrow, OnGrowTick);
            
            OnGrowingStart?.Invoke(this);
        }
        
        protected virtual void ClearField()
        {
            GrowingTimeLeft = 0;
            CurrentGrowingPlant = null;
            jOnChangeGrowingState.Dispatch(this);
        }

        private void OnGrowTick(float time)
        {
            var state = CurrentState;
            
            if(CurrentGrowingPlant == null) return;

            GrowingTimeLeft = CurrentGrowingPlant.ProducingTime - time - _totalGrowingTime;

            if (!_timerStarted)
            {
                _timerStarted = true;
            }

            if (state != CurrentState)
            {
                jOnChangeGrowingState.Dispatch(this);
            }
        }

        private void OnFinishGrow()
        {
            if (CurrentGrowingPlant != null)
            {
                jServerCommunicator.VerifyGrowing(GetMapIDString, _ =>
                {
                    OnFinishGrowingVerified.Dispatch(CurrentGrowingPlant);
                });
                
                OnFinishGrowing.Dispatch(CurrentGrowingPlant);
            }

            _timerStarted = false;
            _timer = null;
            GrowingTimeLeft = 0;
            jOnChangeGrowingState.Dispatch(this);
        }
        
        private void Harvest()
        {
            if (IsHarvesting) return;
            
            if (CurrentGrowingPlant == null)
            {
				Debug.LogWarning("Harvest: nothing to harvest", true);
                return;
            }

            if (CurrentState != AgriculturalFieldState.Completed)
            {
                Debug.LogWarning("Harvest: harvest is not ready yet");
                return;
            }

			//уведомляем сервер
			jServerCommunicator.Harvesting (GetMapIDString, GameManager.CurrentUserCity.Id, nanoGems =>
            {
                IsHarvesting = false;
                if (nanoGems > 0)
                    jSignalOnNanoGemsDropped.Dispatch(nanoGems);
                OnHarvestEndedVerified.Dispatch(true);
            });

            Debug.Log("StartHarvesting: on client done. Field: " + MapID, true);
            IsHarvesting = true;
            OnHarvestStarted.Dispatch();

            var outcomingProduct = new KeyValuePair<Id<Product>, int>(CurrentGrowingPlant.IncomingProducts.First().Key, CurrentGrowingPlant.Amount);
            jPlayerProfile.PlayerResources.AddProduct(outcomingProduct.Key, outcomingProduct.Value);
			
			ClearField();
            jSoundManager.Play(SfxSoundTypes.PlotHarvesting, SoundChannel.SoundFX);

            CurrentGrowingPlant = null;
			jOnChangeGrowingState.Dispatch(this);

			OnHarvestEnded.Dispatch(true);

			var product = jProductsData.GetProduct (outcomingProduct.Key.Value);
			Debug.Log ("Harvest done on field: " + MapID + ", product: " + outcomingProduct.Key + ", count: " + outcomingProduct.Value + 
				", experience: " + (product.ExperienceUp * outcomingProduct.Value), true);
			jPlayerProfile.CurrentLevelData.AddExperience (product.ExperienceUp, true);
            jNanoAnalytics.OnExperienceUpdate(AwardSourcePlace.AgriculturalHarvest, product.ProductTypeID.Value.ToString(), product.ExperienceUp);

            // сигнализируем о производстве продуктов (для ачивок)
            jOnProductProduceAndCollectSignal.Dispatch(this, outcomingProduct.Key, outcomingProduct.Value);

            // выключение режима сбора урожая будет происходить по окончани анимаци
            OnHarvestEnded.Dispatch(true);
        }

        /// <summary>
        /// Пересчитывает переменные времени, необходимо вызывать после буста (изменения GrowingTimeLeft)
        /// </summary>
        public void RefreshTimerTimeout()
        {
            if (_timer == null)
                return;
            
            var state = CurrentState;
            
            _timer?.CancelTimer();
            _timer = null;

            _totalGrowingTime = CurrentGrowingPlant.ProducingTime - GrowingTimeLeft;
            
            _timer = jTimerManager.StartServerTimer(GrowingTimeLeft, OnFinishGrow, OnGrowTick);

            if (state != CurrentState) jOnChangeGrowingState.Dispatch(this);
        }

        public void SkipGrowingTimer(int hardCost)
        {
            if (GrowingTimeLeft <= 0) return;

            jServerCommunicator.SkipAgriculturalGrowing(GameManager.CurrentUserCity.Id, GetMapIDString, res => { });

            jSignalOnSkipProduction.Dispatch(new SkipProductionInfo(BalanceName, ObjectTypeID.ToString(),
                CurrentGrowingPlant, hardCost));

            jPlayerProfile.PlayerResources.BuyHardCurrency(hardCost);
            jNanoAnalytics.OnPlayerCurrenciesDecrease(PlayerResources.CurencyType.Bucks, hardCost, CurrenciesSpendingPlace.SkipGrowingTimer,
                ObjectTypeID.ToString());

            GrowingTimeLeft = 0;
            RefreshTimerTimeout();
        }

        private class AgroFieldProductionRules
        {
            [JsonProperty("available_producing_goods")]
            public List<IProducingItemData> AvaliableProducingGoods = new List<IProducingItemData>();
        }
        
        private class CurrentProducingGood
        {
            [JsonProperty("product_id")]
            public int ProductId { get; set; }
            
            [JsonProperty("done_at")]
            public float FinishTime { get; set; }
            
            [JsonProperty("incoming_goods")]
            public Dictionary<Id<Product>, int> IncomingGoods { get; set; }
            
            [JsonProperty("amount")]
            public int Amount { get; set; }

            /// <summary>
            /// (25 - неудобренное поле, 26 - поле с удобрением)
            /// </summary>
            [JsonProperty("type")]
            public int Type { get; set; }
        }
    }
}