﻿using System;
using Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Models.api.Mines;
using GameLogic.BuildingSystem.MapObjects.Models.impl;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Factories;
using Newtonsoft.Json;
using UnityEngine;

namespace Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Models.impl
{
    public class PowerPlantBuilding : AoeBuilding, IPowerPlantBuilding
    {
        [JsonProperty("electricity_distance_x")] 
        public int AoeAreaX { get; set; }

        [JsonProperty("electricity_distance_y")] 
        public int AoeAreaY { get; set; }
        
        public override bool IsNeedRoad => true;
        protected override bool IsEnergy => true;

        public override bool IsCoveringBuilding(IMapObject mapObject)
        {
            return mapObject is IMineObject || mapObject is IFactoryObject;
        }
        
        public override void InitFromPrototype(IMapObject prototype)
        {
            base.InitFromPrototype(prototype);

            var casted = (PowerPlantBuilding)prototype;
            AoeAreaX = Convert.ToInt32(casted.AoeSize.x);
            AoeAreaY = Convert.ToInt32(casted.AoeSize.y);
        }
        
        public override object Clone()
        {
            var result = Binder.GetInstance<IPowerPlantBuilding>();
            CopyMembersTo(result);
            return result;
        }
        
        public override MapObjectintTypes MapObjectType
        {
            get => MapObjectintTypes.PowerPlant;
            set { }
        }
        
        protected override void CopyMembersTo(IMapObject result)
        {
            base.CopyMembersTo(result);

            var casted = (PowerPlantBuilding)result;
            casted.AoeAreaX = AoeAreaX;
            casted.AoeAreaY = AoeAreaY;
        }
        
        protected override void SyncServerModel(IMapBuilding model)
        {
            base.SyncServerModel(model);
            
            var building = (PowerPlantBuilding)model;

            AoeAreaX = building.AoeAreaX;
            AoeAreaY = building.AoeAreaY;
        }
        
        public override void MoveMapObject(Vector2Int oldPosition, Vector2Int newPosition)
        {
            base.MoveMapObject(oldPosition, newPosition);
            if (AoeConnected == null)
            {
                Debug.LogError($"AoeConnected for PowerPlant is null for [{MapID}]");
                return;
            }
            foreach (int connectedBuildingId in AoeConnected)
            {
                var affectedBuilding = jGameManager.FindMapObjectById<IMapObject>(connectedBuildingId);
                if (!(affectedBuilding is IBuildingWithElectricity))
                {
                    Debug.LogError($"Type of building is not electricity [{affectedBuilding.GetType().Name}] [{affectedBuilding.MapID}]");
                }
                affectedBuilding.IsVerificated = false;
            }
        }
    }
}
