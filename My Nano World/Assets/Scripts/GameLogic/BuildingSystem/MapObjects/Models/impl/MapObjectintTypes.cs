﻿using System;

namespace NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api
{
    public enum MapObjectintTypes
    {
        None = 0,
        DwellingHouse = 1,
        Factory = 2,
        Warehouse = 3,
        Road = 4,
        CityHall = 5,
        AgriculturalField = 6,
        HeadQuarter = 7,
        School = 10,
        FoodSupply = 11,
        PowerPlant = 12,

        Mine = 14,
        EntertainmentBuilding = 15,
        Police = 16,
        [Obsolete] Cinema = 17,

        OrderDesk = 19,
        Decoration = 20,
        [Obsolete] Laboratory = 21,
        CityOrderDesk = 22,
        [Obsolete] Museum = 23,

        PowerStation = 24,
        [Obsolete] Casino = 25,
        [Obsolete] Burger = 26,
        TradingShop = 27,
        TradingMarket = 28,
        IntercityRoad = 29,
        Quests = 30,
        
        GroundBlock = 31, // invisible object on map used to block tile from building anything on it
        
        ServiceBuilding = 32,// firestation, police, ambulance, waterstation etc.
        Hospital = 33,
        EventBuilding = 34,
        EventBuildingReward = 35
    }
}
