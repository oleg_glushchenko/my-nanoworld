﻿using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using Newtonsoft.Json;

namespace Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Models.impl
{
    public class MCityHall : MapBuilding, ICityHall
    {
        [Inject] public ICityPopulation jCityPopulation { get; set; }

        [JsonProperty("max_taxes")]
        public int MaxTimeToGenerateTaxes { get; set; }

        [JsonProperty("limit_dwelling")]
        public int LimitOfPopulation { get; set; }

        public int MaxPopulation
        {
            get
            {
                if (IsUpgradingNow)
                {
                    var prevLevel = jMapObjectsData.GetPreviousBuildingLevelData(this);
                    if (prevLevel != null)
                    {
                        return prevLevel.LimitOfPopulation;
                    }
                }
                return LimitOfPopulation;
            }
        }

        public int TotalPopulation => jCityPopulation.GetTotalCityPopulation();

        public int PopulationCapacity => jCityPopulation.GetPopulationCapacity();

        #region MapBuilding

        public override MapObjectintTypes MapObjectType
        {
            get => MapObjectintTypes.CityHall;
            set { }
        }

        public override object Clone()
        {
            var hall = Binder.GetInstance<ICityHall>();
            CopyMembersTo(hall);
            return hall;
        }

        public override void InitFromPrototype(IMapObject prototype)
        {
            base.InitFromPrototype(prototype);

            var building = prototype as ICityHall;
            if (building != null)
            {
                MaxTimeToGenerateTaxes = building.MaxTimeToGenerateTaxes;
                LimitOfPopulation = building.LimitOfPopulation;
            }
        }

        protected override void CopyMembersTo(IMapObject result)
        {
            base.CopyMembersTo(result);
            var cityHall = (MCityHall) result;
            cityHall.MaxTimeToGenerateTaxes = MaxTimeToGenerateTaxes;
            cityHall.LimitOfPopulation = LimitOfPopulation;
        }

        #endregion
        
        public bool CanFitMorePeople(int amountOfPeople)
        {
            int total = PopulationCapacity + amountOfPeople ;
            return total <= MaxPopulation;
        }
    }
}
