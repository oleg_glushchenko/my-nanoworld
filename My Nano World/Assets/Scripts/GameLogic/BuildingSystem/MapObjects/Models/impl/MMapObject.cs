﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Assets.NanoLib.UtilitesEngine;
using Assets.NanoLib.UtilitesEngine.CustomDebugging.Models.api;
using Assets.NanoLib.Utilities;
using Assets.NanoLib.Utilities.Pulls;
using Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Models.impl;
using Assets.Scripts.GameLogic.Utilites;
using GameLogic.BuildingSystem.MapObjects.Models.api.Core;
using I2.Loc;
using NanoLib.Core.Services.Sound;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityMap.Controllers;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.GameManager.Models.api;
using Newtonsoft.Json;
using strange.extensions.injector.api;
using strange.extensions.signal.impl;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using NanoReality.Game.Tutorial;
using UnityEngine;

namespace NanoReality.GameLogic.BuildingSystem.MapObjects.Impl
{
    /// <summary>
    /// Base implementation of IMapObject
    /// </summary>
    public class MMapObject : IMapObject, IMovableObject
    {
        #region JSON DATA

        [Inject] public SignalOnBuildingRoadConnectionChanged SignalOnBuildingRoadConnectionChanged { get; set; }

        [Inject] public SignalBuildFinishVerificated jSignalBuildFinishVerificated { get; set; }

        #endregion

        #region IMapObject implementation

        /// <summary>
        /// Object Type ID (unique for every map object type in game)
        /// </summary>
        [JsonProperty("building_id")]
        public Id_IMapObjectType ObjectTypeID { get; set; }
        
        [JsonProperty("building_level")]
        public int Level { get; set; }

        [JsonProperty("instance_index")]
        public int InstanceIndex { get; set; }
        
        [JsonProperty("building_logic_type")]
        public virtual MapObjectintTypes MapObjectType { get; set; }

        /// <summary>
        /// Grid cell start position
        /// </summary>
        [JsonProperty("building_map_id")]
        public Id_IMapObject MapID { get; set; }

        /// <summary>
        /// Айди сектора в котором находится здание
        /// </summary>
        public Id<IBusinessSector> SectorId { get; set; }

        [JsonProperty("building_name")]
        public string BalanceName { get; set; }

        public string Name
        {
            get
            {
                string localizedName =
                    LocalizationManager.GetTranslation($"Building/OBJECT_BUILDING_{ObjectTypeID.Value}");
#if !PRODUCTION
                // Return building name with building instance id for debug purposes.
                return $"{localizedName} _{ObjectTypeID?.Value}_{InstanceIndex}_{MapID?.Value}";
#endif
                return localizedName;
            }
        }

        public string Description =>
            LocalizationMLS.Instance.GetText(string.Format(LocalizationKeyConstants.Objects.BUILDING_DSCR,
                ObjectTypeID.Value));

        [JsonProperty("sector_ids")]
        public List<BusinessSectorsTypes> SectorIds { get; set; }

        [JsonProperty("categories")]
        public List<BuildingsCategories> Categories { get; set; }
        
        [JsonProperty("building_subcategory_id")]
        public BuildingSubCategories SubCategory { get; set; }

        /// <summary>
        /// Grid cell start position
        /// </summary>
        [JsonProperty("building_position")]
        public Vector2F MapPosition { get; set; }

        /// <summary>
        /// Object dementions (X - height, Y - widht)
        /// </summary>
        [JsonProperty("dimensions")]
        public Vector2F Dimensions { get; set; }
	    
        [JsonProperty("offset")]
        public int ConstructionOffset { get; set; }

        /// <summary>
        /// Время до окончания строительства в секундах
        /// При значении 0 строительство закончено, но не подтверждено (нужно отправить запрос на сервер для подтверждения)
        /// При значении -1 строительство полностью закончено
        /// </summary>
        [JsonProperty("building_build_time")]
        protected int currentBuildTime { get; set; }

        [JsonProperty("open_popup_sound")]
        public string OpenPopupSound { get; set; }
        
        [JsonProperty("exit_to_main_road")]
        private bool _isConnectedToGeneralRoad;

        private MapObjectId _id;

        public MapObjectId Id
        {
            get
            {
                if (_id.Equals(default))
                    _id = new MapObjectId(ObjectTypeID, Level);

                if (_id.Level != Level || _id.Id != ObjectTypeID)
                    _id = new MapObjectId(ObjectTypeID, Level);
                
                return _id;
            }
        }

        [JsonProperty("rules")]
        public object Rules { get; set; }

        /// <summary>
        /// После окончания постройки объекта локально - по умолчанию у него нет электричества и подключения к дороге.
        /// Это свойство становится false при окончании постройки локально, и true, когда пришел ответ с сервера с 
        /// данными о подключениях. Таким образом свойство показывает: достоверны ли данные об электричестве и дороге 
        /// у здания
        /// </summary>
        public bool IsVerificated { get; set; } = true;

        public bool IsConnectedToGeneralRoad
        {
            get => _isConnectedToGeneralRoad;
            set
            {
                var f = _isConnectedToGeneralRoad != value;
                _isConnectedToGeneralRoad = value;

                if (!f && value) 
                    return;
                SignalOnBuildingRoadConnectionChanged?.Dispatch (this);
                EventOnBuildingRoadConnectionChanged.Dispatch (this);
            }
        }
        
        public virtual bool IsNeedRoad => false;

        public virtual bool IsSkipped { get; set; }

        #endregion

		public virtual string GetMapIDString() => MapID.Value.ToString();

        /// <summary>
        /// Построенно ли здание
        /// </summary>
        public bool IsConstructed => currentBuildTime < 0;

        public bool IsConstructFinished { get; set; }

        public bool IsFunctionalConstructionComplete => Level > 0;

        //если мы запустили игру - постройка должна идти не с начала
		private int _startTime;

        /// <summary>
        /// Находится ли сейчас здание в процессе апгрейда
        /// </summary>
        public bool IsUpgradingNow => currentBuildTime >= 0 && IsFunctionalConstructionComplete;

        /// <summary>
        /// Возвращает время окончания текущего строительства
        /// </summary>
        public int CurrentConstructTime => currentBuildTime;

        #region Injects

        [Inject] public IInjectionBinder Binder { get; set; }
        [Inject] public ITimerManager jTimerManager { get; set; }
        [Inject] public IDebug Debug { get; set; }
        [Inject] public IGameManager GameManager { get; set; }
        [Inject] public IMapObjectsData jMapObjectsData { get; set; }
        [Inject] public IMapObjectsGenerator jMapObjectsGenerator { get; set; }
        [Inject] public ISoundManager jSoundManager { get; set; }
        [Inject] public IHardTutorial jHardTutorial { get; set; }
        [Inject] public SignalMoveMapObjectView SignalMoveMapObjectView { get; set; }
        [Inject] public SignalOnMapObjectBuildFinished OnMapObjectBuildingFinishedSignal { get; set; }
        [Inject] public SignalOnMapObjectRemoved SignalOnMapObjectRemoved { get; set; }

        #endregion
        
		#region Events

		public Signal<IMapObject> EventOnMapObjectBuildStarted => _eventOnMapObjectBuildStarted;
        protected readonly Signal<IMapObject> _eventOnMapObjectBuildStarted = new Signal<IMapObject> ();
        
		public Signal<IMapObject> EventOnMapObjectBuildFinished => _eventOnMapObjectBuildFinished;
        protected readonly Signal<IMapObject> _eventOnMapObjectBuildFinished = new Signal<IMapObject> ();

        public Signal<IMapObject> EventOnMapObjectBuildMoved => _eventOnMapObjectBuildMoved;
        protected readonly Signal<IMapObject> _eventOnMapObjectBuildMoved = new Signal<IMapObject> ();

		public Signal<IMapObject> EventOnBuildingRoadConnectionChanged => _eventOnBuildingRoadConnectionChanged;
        protected readonly Signal<IMapObject> _eventOnBuildingRoadConnectionChanged = new Signal<IMapObject> ();
        
        public Signal<Id_IMapObject, IMapObject> EventOnCommitBuilding => _eventOnCommitBuilding;
        protected readonly Signal<Id_IMapObject, IMapObject> _eventOnCommitBuilding = new Signal<Id_IMapObject, IMapObject>();

        #endregion
        
        protected ITimer BuildTimer;

        [OnDeserialized]
        internal void OnDeserialized(StreamingContext context) => OnDeserialized();
        

        /// <summary>
        /// Called after object is deserialized from json string
        /// </summary>
        protected virtual void OnDeserialized()
        {
            if (SectorIds == null)
            {
                SectorIds = new List<BusinessSectorsTypes>(6);
            }
            
            if (Categories == null)
            {
                Categories = new List<BuildingsCategories>(6);
            }
        }
        
        public virtual void OnInitOnMap(IMapObject prototype)
        {
            InitFromPrototype(prototype);
            IsConstructFinished = IsConstructed;
        }

        public virtual void OnPostInitObject()
        {
        }

        /// <summary>
        /// Копирует данные из прототипа в текущую модель
        /// </summary>
        /// <param name="prototype">прототип для инициализации текущей модели</param>
        public virtual void InitFromPrototype(IMapObject prototype)
        {
            BalanceName = prototype.BalanceName;
            Dimensions = prototype.Dimensions;
            ConstructionOffset = prototype.ConstructionOffset;
            SectorIds = prototype.SectorIds;
            Level = prototype.Level;
            Categories = prototype.Categories;
            OpenPopupSound = prototype.OpenPopupSound;
            Rules = prototype.Rules;
        }

        /// <summary>
        /// Инициализирует свойства здания, необходимые для функционирования здания.
        /// Не вызывается в режиме предпросмотра (посещение города друга).
        /// </summary>
        public virtual void InitializeBuildingTimers()
        {
            if (currentBuildTime > 0)
            {
                StartBuildTimer(currentBuildTime);
            }
            else if (currentBuildTime == 0)
            {
                FinishBuildingLocal();
            }
        }

        public virtual void OnMapObjectViewAdded()
        {
            
        }

        /// <summary>
        /// Calls before game send move building request to server.
        /// </summary>
        /// <param name="oldPosition"></param>
        /// <param name="newPosition"></param>
        public virtual void MoveMapObject(Vector2Int oldPosition, Vector2Int newPosition)
        {
            SignalMoveMapObjectView.Dispatch(newPosition, this, null);
            jSoundManager.Play(SfxSoundTypes.MoveBuildings, SoundChannel.SoundFX);
            _eventOnMapObjectBuildMoved.Dispatch(this);
        }
        
        public virtual void RestoreMapObjectPosition()
        {
            SignalMoveMapObjectView.Dispatch(this.MapPosition.ToVector2Int(), this, null);
            _eventOnMapObjectBuildMoved.Dispatch(this);
        }

        public virtual void MoveMapObjectVerificated(MoveMapObjectResponce result)
        {

        }

        public void StartConstruction(int constructionTime)
        {
            OnPostInitObject();
            StartBuildTimer(constructionTime);
            // TODO: workaround, need replace it to some MapObjectViewComponent.
            if (MapObjectType != MapObjectintTypes.Road)
            {
                jSoundManager.Play(SfxSoundTypes.BuildingChosenPlace, SoundChannel.SoundFX);
            }
        }

        public void CommitBuilding(Id_IMapObject id)
        {
            var oldId = MapID;
            MapID = id;

            EventOnCommitBuilding.Dispatch(oldId, this);
        }
        
        protected void StartBuildTimer(int buildingTimeSeconds)
        {
            _startTime = buildingTimeSeconds;

            currentBuildTime = buildingTimeSeconds;
            EventOnMapObjectBuildStarted.Dispatch (this);

            if (buildingTimeSeconds > 0)
            {
                BuildTimer = jTimerManager.StartServerTimer((float)buildingTimeSeconds, FinishBuildingLocal, OnTickBuildTimer);
                return;
            }
            
            FinishBuildingLocal();
        }

        /// <summary>
        /// Вызывается каждый тик таймера постройки
        /// </summary>
        /// <param name="currTime"></param>
        protected virtual void OnTickBuildTimer(float currTime)
        {
            currentBuildTime = _startTime - (int)currTime;
        }
        
        /// <summary>
        /// Вызывается по окончанию постройки локально
        /// </summary>
        protected virtual void FinishBuildingLocal()
        {
            // отмечаем, что строительство полностью закончено
            currentBuildTime = -1;
            FinishBuilding();
            BuildTimer?.CancelTimer();
            BuildTimer = null;
        }

        /// <summary>
        /// Вызывается по окончанию постройки
        /// </summary>
        protected virtual void FinishBuilding()
        {
            var prototype = jMapObjectsGenerator.GetMapObjectModel(ObjectTypeID, Level) as MapBuilding;
            InitFromPrototype(prototype);
            
            OnMapObjectBuildingFinishedSignal.Dispatch(this);
            EventOnMapObjectBuildFinished.Dispatch (this);
        }

        /// <summary>
        /// Вызывается после подтверждения окончания постройки здания сервером
        /// </summary>
        /// <param name="result"></param>
        protected virtual void BuildFinishVerified(SyncBuildingResponse result)
        {
            IsVerificated = true;
            IsConstructFinished = true;
            IsConnectedToGeneralRoad = result.Building.IsConnectedToGeneralRoad;
            
            jSignalBuildFinishVerificated.Dispatch(this);
        }

        /// <summary>
        /// Производит улучшение здания на 1 уровень
        /// </summary>
        public virtual void UpgradeObject(int upgradeTime)
        {
            jSoundManager.Play(SfxSoundTypes.BuildingUpgrade, SoundChannel.SoundFX);
            StartBuildTimer(upgradeTime);
        }

        public virtual void RemoveMapObject()
        {
            SignalOnMapObjectRemoved.Dispatch(this);
        }
			
        #region IPullableObject
        
        public virtual object Clone()
        {
            var result = Binder.GetInstance<IMapObject>(); //new MMapObject();
            CopyMembersTo(result);
            return result;
        }
        
        protected virtual void CopyMembersTo(IMapObject result)
        {
            result.BalanceName = BalanceName;
            result.ObjectTypeID = ObjectTypeID;
            result.InstanceIndex = InstanceIndex;
            result.MapID = MapID;
            result.MapPosition = new Vector2F(MapPosition.X, MapPosition.Y);
            result.Dimensions = new Vector2F(Dimensions.X, Dimensions.Y);
            result.ConstructionOffset = ConstructionOffset;
            result.SectorIds = SectorIds;
            result.MapObjectType = MapObjectType;
            result.Level = Level;
            result.IsConnectedToGeneralRoad = IsConnectedToGeneralRoad;
            result.Categories = Categories;
            result.OpenPopupSound = OpenPopupSound;
            result.Rules = Rules;
        }

        public IObjectsPull pullSource { get; set; }

        public virtual void OnPop()
        {

        }

        public virtual void OnPush()
        {

        }

        public virtual void FreeObject()
        {
            pullSource?.PushInstance(this);
        }

        public virtual void DestroyObject()
        {

        }

        #endregion
    }
}
