﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.NanoLib.Utilities;
using Assets.NanoLib.Utilities.DataManager.Models.api;
using NanoLib.Core.Logging;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using NanoReality.GameLogic.StartApplicationSequence.Models.api;
using Newtonsoft.Json;

namespace NanoReality.GameLogic.BuildingSystem.MapObjects.Impl
{
    [Serializable]
    public class MapObjectsData : IMapObjectsData
    {
        [Inject] public IMapObjectsGenerator jMapObjectsGenerator { get; set; }

        [JsonProperty("buildings")] public List<IMapObject> MapObjects { get; set; }

        private readonly Dictionary<Id<IBusinessSector>, List<IMapObject>> _mapObjectsBySectorId = new Dictionary<Id<IBusinessSector>, List<IMapObject>>();
        private readonly Dictionary<MapObjectintTypes, List<IMapObject>> _mapObjectsByTypes = new Dictionary<MapObjectintTypes, List<IMapObject>>();
        private readonly Dictionary<int, List<IMapObject>> _mapObjectsByBalanceId = new Dictionary<int, List<IMapObject>>();
        private readonly Dictionary<int, CategoryType> _mapObjectsByCategory = new Dictionary<int, CategoryType>();

        public MapObjectsData()
        {
            MapObjects = new List<IMapObject>();
        }

        public bool IsPresentBuildingIdAndLevel(Id_IMapObjectType buildingId, int levelId)
        {
            return GetByBuildingIdAndLevel(buildingId, levelId) != null;
        }

        public IMapObject GetByBuildingId(Id_IMapObjectType buildingId)
        {
            if (_mapObjectsByBalanceId.TryGetValue(buildingId, out var list)) return list.Last();
            ("MapObjectsData: Cannot find building balance id: " + buildingId.Value).LogWarning(LoggingChannel.Map);
            return null;
        }

        public IMapObject GetByBuildingIdAndLevel(Id_IMapObjectType buildingId, int levelId)
        {
            List<IMapObject> list;
            if (!_mapObjectsByBalanceId.TryGetValue(buildingId, out list))
            {
                ("MapObjectsData: Cannot find building balance id: " + buildingId.Value).LogWarning(LoggingChannel.Map);
                return null;
            }

            if (list.Count > levelId)
            {
                var mapObject = list[levelId];
                if (mapObject.Level == levelId) return mapObject;
            }

            for (var i = 0; i < list.Count; i++)
            {
                if (list[i].Level == levelId) return list[i];
            }

            $"MapObjectsData: Cannot find building balance id: {buildingId.Value} with level: {levelId}".LogWarning(LoggingChannel.Map);
            return null;
        }

        public IMapObject GetByBuildingType(MapObjectintTypes buildingType)
        {
            var list = GetAllMapObjectsByBuildingType(buildingType);
            return list != null && list.Count > 0 ? list[0] : null;
        }
        
        public IMapObject GetByBuildingTypeAndSubCategory(MapObjectintTypes buildingType, BuildingSubCategories subCategories)
        {
            var list = GetAllMapObjectsByBuildingType(buildingType);
            return list != null && list.Count > 0 ? list.FirstOrDefault(e => e.SubCategory == subCategories) : null;
        }

        public List<IMapObject> GetAllMapObjectsForSector(Id<IBusinessSector> sectorId)
        {
            if (!_mapObjectsBySectorId.TryGetValue(sectorId, out List<IMapObject> list))
            {
                ("MapObjectsData: Cannot find any map object with sector id: " + sectorId.Value).LogWarning(LoggingChannel.Map);
                return null;
            }

            return list;
        }

        public List<IMapObject> GetAllMapObjectsByBuildingType(MapObjectintTypes buildingType)
        {
            List<IMapObject> list;
            if (!_mapObjectsByTypes.TryGetValue(buildingType, out list))
            {
                ("MapObjectsData: Cannot find building type: " + buildingType).LogWarning(LoggingChannel.Map);
                return null;
            }

            return list;
        }

        public BuildingSubCategories GetSubCategoryByBuildingId(Id_IMapObjectType buildingId)
        {
            if (_mapObjectsByCategory.TryGetValue(buildingId, out CategoryType categoryType))
                return categoryType.SubCategory;

            ("MapObjectsData: Cannot find building balance id: " + buildingId.Value).LogWarning(LoggingChannel.Map);
            return BuildingSubCategories.None;
        }

        public T GetMapObjectData<T>(Id_IMapObjectType objectTypeId, int level) where T : class, IMapObject
        {
            return GetByBuildingIdAndLevel(objectTypeId, level) as T;
        }

        public T GetMapObjectData<T>(T building, int level) where T : class, IMapObject
        {
            return GetByBuildingIdAndLevel(building.ObjectTypeID, level) as T;
        }

        public T GetPreviousBuildingLevelData<T>(T building) where T : class, IMapBuilding
        {
            return building.Level > 0 ? GetMapObjectData<T>(building.ObjectTypeID, building.Level - 1) : null;
        }

        public T GetNextBuildingLevelData<T>(T building) where T : class, IMapBuilding
        {
            return building.IsUpgradable ? GetMapObjectData<T>(building.ObjectTypeID, building.Level + 1) : null;
        }

        private static int ComparisonByLevel(IMapObject x, IMapObject y)
        {
            return x.Level.CompareTo(y.Level);
        }

        private void Cache(IMapObjectsData data)
        {
            MapObjects = data.MapObjects;

            _mapObjectsBySectorId.Clear();
            _mapObjectsByTypes.Clear();
            _mapObjectsByBalanceId.Clear();

            foreach (var mapObject in MapObjects)
            {
                Cache(mapObject);
            }

            foreach (var entry in _mapObjectsByBalanceId)
            {
                entry.Value.Sort(ComparisonByLevel);
            }
        }

        private void Cache(IMapObject mapObject)
        {
            for (var i = 0; i < mapObject.SectorIds.Count; i++)
            {
                var id = mapObject.SectorIds[i].GetBusinessSectorId();
                List<IMapObject> sectorList;

                if (_mapObjectsBySectorId.TryGetValue(id.Value, out sectorList))
                {
                    sectorList.Add(mapObject);
                }
                else
                {
                    _mapObjectsBySectorId.Add(id, new List<IMapObject> {mapObject});
                }
            }

            List<IMapObject> typeList;
            if (_mapObjectsByTypes.TryGetValue(mapObject.MapObjectType, out typeList))
            {
                typeList.Add(mapObject);
            }
            else
            {
                _mapObjectsByTypes.Add(mapObject.MapObjectType, new List<IMapObject> {mapObject});
            }

            List<IMapObject> balanceList;
            if (_mapObjectsByBalanceId.TryGetValue(mapObject.ObjectTypeID, out balanceList))
            {
                balanceList.Add(mapObject);
            }
            else
            {
                _mapObjectsByBalanceId.Add(mapObject.ObjectTypeID, new List<IMapObject> {mapObject});
            }

            CategoryType categoryType;
            if (_mapObjectsByCategory.TryGetValue(mapObject.ObjectTypeID, out categoryType))
            {
                categoryType.Categories = mapObject.Categories;
            }
            else
            {
                CategoryType category = new CategoryType(mapObject.Categories, mapObject.SubCategory);
                _mapObjectsByCategory.Add(mapObject.ObjectTypeID, category);
            }
        }

        #region IGameBalanceData

        [JsonIgnore] public string DataHash { get; set; }

        [JsonIgnore] [NonSerialized] private Action<IGameBalanceData> _gameBalanceUpdateCallback;

        public void LoadData(IServerCommunicator serverCommunicator, ILocalDataManager localDataManager, Action<IGameBalanceData> callback)
        {
            _gameBalanceUpdateCallback = callback;
            serverCommunicator.LoadMapObjects(OnLoadedMapObjects);
        }

        private void OnLoadedMapObjects(IMapObjectsData data)
        {
            Cache(data);

            foreach (IMapObject mapObjectPrototype in MapObjects)
            {
                jMapObjectsGenerator.AddObjectModel(mapObjectPrototype);
            }

            _gameBalanceUpdateCallback?.Invoke(this);
        }

        #endregion

        private class CategoryType
        {
            public List<BuildingsCategories> Categories = new List<BuildingsCategories>();
            public BuildingSubCategories SubCategory = BuildingSubCategories.None;

            public CategoryType(List<BuildingsCategories> mapObjectCategories, BuildingSubCategories mapObjectSubCategory)
            {
                Categories = mapObjectCategories;
                SubCategory = mapObjectSubCategory;
            }
        }
    }
}