﻿using System.Collections.Generic;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;

namespace NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api
{
    public static class MapObjectExtensions
    {
        /// <summary>
        /// Types of buildings that player can't build.
        /// </summary>
        private static readonly HashSet<MapObjectintTypes> NotAvailableForBuildTypes = new HashSet<MapObjectintTypes>
        {
            MapObjectintTypes.Road,
            MapObjectintTypes.CityHall,
            MapObjectintTypes.Warehouse,
            MapObjectintTypes.OrderDesk,
            MapObjectintTypes.CityOrderDesk,
            MapObjectintTypes.HeadQuarter
        };

        public static bool IsAvailableBuildingType(this IMapObject mapObj)
        {
            return !NotAvailableForBuildTypes.Contains(mapObj.MapObjectType);
        }
    }
}