﻿using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using Newtonsoft.Json;

namespace Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Models.impl
{
    public class WarehouseMapObject : MapBuilding, IWarehouseMapObject
    {
        [JsonProperty("max_capacity")] 
        public int MaxCapacity { get; private set; }
        
        public int CurrentCapacity => jPlayerProfile.PlayerResources.ProductsCount;
        
        public override MapObjectintTypes MapObjectType => MapObjectintTypes.Warehouse;

        public override void InitFromPrototype(IMapObject prototype)
        {
            base.InitFromPrototype(prototype);
            
            var warehouse = (WarehouseMapObject) prototype;
            MaxCapacity = warehouse.MaxCapacity;

            jPlayerProfile.PlayerResources.InitProducts(null, MaxCapacity);
        }

        public override object Clone()
        {
            var result = Binder.GetInstance<IWarehouseMapObject>();
            CopyMembersTo(result);
            
            return result;
        }

        protected override void CopyMembersTo(IMapObject result)
        {
            base.CopyMembersTo(result);
            
            var warehouse = (WarehouseMapObject) result;
            warehouse.MaxCapacity = MaxCapacity;
        }
    }
}