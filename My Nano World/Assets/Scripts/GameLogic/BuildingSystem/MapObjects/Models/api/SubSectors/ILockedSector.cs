﻿using GameLogic.BuildingSystem.CityArea;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;

namespace NanoReality.GameLogic.BuildingSystem.MapObjects.Impl
{
    public interface ILockedSector : ICitySector
    {
        Price Price { get; }
        bool Unlockable { get; set; }
        
        int X { get; }

        int Y { get; }

        int Width { get; }
        
        int Height { get; }
    }
}