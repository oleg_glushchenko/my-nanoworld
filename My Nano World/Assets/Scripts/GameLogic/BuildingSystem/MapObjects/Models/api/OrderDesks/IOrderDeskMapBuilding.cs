﻿using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;

namespace Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Models.api.OrderDesks
{
    public interface IOrderDeskMapBuilding : IMapBuilding
    {
        BusinessSectorsTypes OrderDeskSectorType { get; }
    }
}
