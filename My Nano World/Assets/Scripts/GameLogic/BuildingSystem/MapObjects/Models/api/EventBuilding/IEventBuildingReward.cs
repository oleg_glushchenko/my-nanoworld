using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;

public interface IEventBuildingReward : IMapBuilding
{
    long NextRewardTime { get; set; }
    int TaxBonus();
    EventBuildingReward.EventStatusEffect[] Effects { get; set; }
}
