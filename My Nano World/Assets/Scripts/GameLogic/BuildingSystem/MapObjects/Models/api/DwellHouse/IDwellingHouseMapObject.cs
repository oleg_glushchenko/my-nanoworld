﻿using System.Collections.Generic;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.Happiness;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.ServiceBuildings;

namespace NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api
{
    public interface IDwellingHouseMapObject : IMapBuilding, IBuildingNeedHappiness
    {
        /// <summary>
        /// Max amount of people that building can fit
        /// </summary>
        int MaxPopulation { get; }
    }
}