﻿using Assets.Scripts.Engine.Analytics.Model;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Production;
using strange.extensions.signal.impl;

namespace NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Factories
{
	public interface IFactoryObject : IProductionBuilding { }

	public class SignalOnStartProduceGoodOnFactory : Signal<ProductionStartInfo> {}
}