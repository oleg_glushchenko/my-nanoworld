﻿using NanoReality.GameLogic.BuildingSystem.City.Models.Api;

namespace NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api
{
    public interface IQuestsBuilding : IMapBuilding
    {
        BusinessSectorsTypes QuestsSectorType { get; }
        
        int AchievedQuestsCount { get; }
        int ActiveQuestsCount { get; }
        int FirstAchievedQuestGiftBoxType { get; }
    }
}
