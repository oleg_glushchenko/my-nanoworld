using strange.extensions.signal.impl;

namespace NanoReality.GameLogic.BuildingSystem.MapObjects.Api
{
    /// <summary>
    /// Происходит при старте постройки здания
    /// </summary>
    public class BuildingConstructionStartedSignal : Signal<IMapObject> { }

    /// <summary>
    /// Calls when building or it upgrade finished.
    /// </summary>
    public class SignalOnMapObjectBuildFinished : Signal<IMapObject> { }

    /// <summary>
    /// Вызывается при изменении состояния подключения к дороге
    /// </summary>
    public class SignalOnBuildingRoadConnectionChanged : Signal<IMapObject> { }

    public class SignalBuildFinishVerificated : Signal<IMapObject> { }
}