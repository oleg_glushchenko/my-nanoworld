﻿using UnityEngine;
using System.Collections;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using Assets.NanoLib.Utilities;
using Assets.NanoLib.Utilities.Pulls;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using System;
using System.Collections.Generic;

public interface IDecorationMapObject : IMapBuilding
{
    
}
