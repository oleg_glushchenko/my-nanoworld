﻿using System;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using strange.extensions.signal.impl;
using System.Collections.Generic;
using NanoReality.Game.CitizenGifts;

namespace NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api
{

	/// <summary>
	/// Вызывается при старте апгрейда здания, в параметре здание после апгрейда
	/// </summary>
	public class SignalOnMapBuildingsUpgradeStarted : Signal<IMapBuilding> {}

    /// <summary>
    /// Triggered when received upgrade success response from server
    /// </summary>
    public class SignalOnMapBuildingUpgradeVerificated : Signal<IMapBuilding> {}
	
	/// <summary>
	/// Triggred when the building was verificated
	/// </summary>
	public class MapBuildingVerifiedSignal : Signal<IMapBuilding> { }

    /// <summary>
    /// Представляет собой базовый класс здания. Все здания имеют цену, потребляют энергию
    /// </summary>
    public interface IMapBuilding : IMapObject
    {
	    /// <summary>
        /// Used for buildings that need to repair(CityHall etc).
        /// </summary>
        bool IsLocked { get; set; }

        /// <summary>
	    /// True if has next upgrade level.
	    /// </summary>
	    bool IsUpgradable { get; }

        /// <summary>
	    /// True if can be upgraded at the moment
	    /// </summary>
	    bool CanBeUpgraded { get; }

        IProductsToUnlock ProductsToUnlock { get; set; }

        List<IProductForUpgrade> ProductsForUpgrade { get; set; }

        /// <summary>
	    /// Return player level when a buildng can be unlocked (repaired)
	    /// </summary>
	    int BuildingUnlockLevel { get; }

	    List<CitizenGift> AvailableGifts { get; set; }

	    event Action CitizenGiftChanged;

        /// <summary>
	    /// Verificates building with server and syncs its data
	    /// </summary>
	    void VerifyBuilding();
	    
        /// <summary>
        /// Пропускает таймер постройки/апгрейда за премиум валюту
        /// </summary>
        void SkipConstructionTimer();

        /// <summary>
        /// возвращает true если все условия правильной работы здания соблюдены
        /// </summary>
        /// <returns>true если все условия правильной работы здания соблюдены
        /// </returns>
        bool IsFunctional();
    }
}
