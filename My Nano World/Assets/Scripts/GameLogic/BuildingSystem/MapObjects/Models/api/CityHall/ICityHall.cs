﻿namespace NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api
{
    public interface ICityHall : IMapBuilding
    {
        /// <summary>
        /// Maximum time (in seconds) to generate taxes before reaching storage capacity
        /// </summary>
        int MaxTimeToGenerateTaxes { get; }

        /// <summary>
        /// Limit of population that City Hall can fit
        /// </summary>
        int LimitOfPopulation { get; }
        
        /// <summary>
        /// Total amount of population that city can fit in existing functional buildings
        /// </summary>
        int TotalPopulation { get; }
        
        /// <summary>
        /// Current population capacity that city can fit
        /// </summary>
        int PopulationCapacity { get; }
        
        /// <summary>
        /// Maximum amount of population available to player that City Hall can fit at the moment
        /// (may differ from LimitOfPopulation when building is upgrading)
        /// </summary>
        int MaxPopulation { get; }

        /// <summary>
        /// Returns true if city can fit given amount of extra people
        /// </summary>
        /// <param name="amountOfPeople"></param>
        /// <returns></returns>
        bool CanFitMorePeople(int amountOfPeople);
    }
}
