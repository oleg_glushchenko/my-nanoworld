﻿namespace NanoReality.GameLogic.BuildingSystem.MapObjects.Api
{
    /// <summary>
    /// Empty object, just blockin cell
    /// </summary>
    public interface IGroundBlock : IMapObject
    {
        
    }
}
