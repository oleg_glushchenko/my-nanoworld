﻿using System.Collections.Generic;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Factories;
using System;

namespace NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Production
{
    public interface IProductionBuilding : IMapBuilding
    {
        int MaximalSlotsCount { get; set; }
        int AvailableSlotsCount { get; set; }
        List<IProducingItemData> AvailableProducingGoods { get; set; }
        List<IProduceSlot> CurrentProduceSlots { get; set; }

        event Action<IProduceSlot, IProducingItemData> StartedProduction;
        event Action<IProduceSlot, IProducingItemData> OnConfirmFinishProduction;
        event Action<IProduceSlot, IProducingItemData> OnFinishProduction;
        event Action<IProduceSlot, IProducingItemData> OnSlotShipped;
        
        bool HasEmptySlot();
        int CalculateSlotUnlockPrice();
        bool IsProductionEmpty();
        bool HasProductsToCollect();
        bool IsProduced();
        void ShipProduct(IProduceSlot produceSlot, Action<IProduceSlot> onSuccess);
        void SkipSlot(IProduceSlot produceSlot, Action<IProduceSlot> callback);
        void StartProduceGood(IProducingItemData producingItemData, IProduceSlot produceSlot, Action<int> onSuccess = null);
        void UnlockSlot(IProduceSlot produceSlot);
    }
}