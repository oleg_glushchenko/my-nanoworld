﻿using System;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;

namespace NanoReality.GameLogic.BuildingSystem.MapObjects.Api
{
    public interface IMapObjectsGenerator
    {
        IMapObject GetMapObjectModel(Id_IMapObjectType ID, int level);

        void AddObjectModel(IMapObject prototype);

        MapObjectView GetMapObjectView(MapObjectId id);
	    
        void GetMapObjectView(MapObjectId id, Action<MapObjectView> callback);

        void Clear();

        MapObjectId CalculateBuildingVisualStyleLevel(MapObjectId id);
    }
}
