﻿using Assets.NanoLib.Utilities;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.impl.AgriculturalFields;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;

namespace NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.AgriculturalFields
{
    /// <summary>
    /// Сельскохозяйственная культура
    /// </summary>
    public interface IGrowingPlantData
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        Id<Product> PlantId { get; set; }

        /// <summary>
        /// Необходимое количество единиц культуры для засеивания поля
        /// </summary>
        int IncomingAmount { get; set; }

        /// <summary>
        /// Количество единиц культуры получаемое с урожая
        /// </summary>
        int OutcomingAmount { get; set; }

        /// <summary>
        /// Врямя полного созревания культуры
        /// </summary>
        int GrowingTime { get; set; }

        /// <summary>
        /// Тип культуры
        /// </summary>
        PlantType PlantType { get; set; }

        /// <summary>
        /// Уровень, необходимый для разблокировки культуры
        /// </summary>
        int LevelOpend { get; set; }

        /// <summary>
        /// Количество занимаемого места в таре для сбора урожая
        /// </summary>
        int Quantity { get; set; }
    }
}
