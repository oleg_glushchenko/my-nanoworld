﻿namespace NanoReality.GameLogic.BuildingSystem.MapObjects.Models.ServiceBuildings
{
    public enum ServiceBuildingType
    {
        None = 0,
        School = 1,
        Power = 2,
        Hospital = 3,
        Police = 4
    }
}