﻿using System.Collections.Generic;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Impl;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.ServiceBuildings;
using Newtonsoft.Json;

namespace GameLogic.BuildingSystem.CityAoe
{
    public class CityAoeData
    {
        [JsonProperty("services")]
        public Dictionary<int, List<ServiceBuildingType>> AoeData { get; private set; }
        
        [JsonProperty("connected_buildings")]
        public Dictionary<int, List<int>> ConnectedToServices { get; set; }

        [JsonProperty("city_happiness")]
        public int Happiness;

        [JsonProperty("taxes")]
        public CityTaxes Taxes;
    }
    
    public class AoeData
    {
        [JsonProperty("police")]
        public AoeMapBuildingData[] PoliceBuildings { get; private set; }

        [JsonProperty("school")]
        public AoeMapBuildingData[] SchoolServiceBuildings { get; private set; }

        [JsonProperty("hospital")]
        public AoeMapBuildingData[] HospitalServiceBuildings { get; private set; }
        
        [JsonProperty("power")]
        public AoeMapBuildingData[] PowerServiceBuildings { get; private set; }
    }
    
    public class AoeMapBuildingData
    {
        [JsonProperty("source")]
        public int ServiceId { get; private set; }

        [JsonProperty("targets")]
        public HashSet<int> CoveredBuildings { get; private set; }

        /// <summary>
        /// Map Id of affected by some service building.
        /// </summary>
        [JsonProperty("building_id")]
        public int BuildingId;
        
        /// <summary>
        /// List of service building that have affection on target building
        /// </summary>
        [JsonProperty("service_ids")]
        public HashSet<int> ServiceBuildings { get; private set; }
    }
}