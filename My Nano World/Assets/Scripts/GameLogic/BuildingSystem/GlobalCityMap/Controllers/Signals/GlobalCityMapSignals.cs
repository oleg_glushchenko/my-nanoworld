﻿using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.GlobalCityMap.Models;
using strange.extensions.signal.impl;

namespace NanoReality.GameLogic.BuildingSystem.GlobalCityMap.Controllers
{
    /// <summary>
    /// Сигнал вызывается когда модель глобальной карты проинициализирована и готова к использованию
    /// </summary>
    public class SignalOnInitGlobalCityMap : Signal<IGlobalCityMap> { }
    
    public sealed class CityMapViewInitializedSignal : Signal<BusinessSectorsTypes> {}
}
