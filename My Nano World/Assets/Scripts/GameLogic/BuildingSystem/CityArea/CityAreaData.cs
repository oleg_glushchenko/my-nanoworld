using System;
using System.Collections.Generic;
using Assets.NanoLib.Utilities.DataManager.Models.api;
using GameLogic.BuildingSystem.CityArea;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using NanoReality.GameLogic.StartApplicationSequence.Models.api;
using Newtonsoft.Json;

namespace NanoReality.GameLogic.BuildingSystem.CityArea
{
    public class CityAreaData : ICityAreaData
    {
        [JsonProperty("areas")]
        public List<ISectorArea> Areas { get; private set; }

        public string DataHash { get; set; }

        public void LoadData(IServerCommunicator serverCommunicator, ILocalDataManager localDataManager, Action<IGameBalanceData> callback)
        {
            serverCommunicator.LoadCityAreaData(this, data =>
            {
                Areas = data.Areas ?? new List<ISectorArea>();

                if (callback != null) callback(data);
            });
        }
    }
}