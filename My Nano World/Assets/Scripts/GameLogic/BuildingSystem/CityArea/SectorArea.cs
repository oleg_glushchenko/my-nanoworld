using System.Collections.Generic;
using GameLogic.BuildingSystem.CityArea;
using Newtonsoft.Json;

namespace NanoReality.GameLogic.BuildingSystem.CityArea
{
    public class SectorArea : CitySector, ISectorArea
    {
        [JsonProperty("buildings_id")]
        public List<Id_IMapObjectType> Buildings { get; private set; }

        public bool IsDisabled => Buildings == null || Buildings.Count <= 0;

        public bool ContainsBuildingTypeId(Id_IMapObjectType type)
        {
            return !IsDisabled && Buildings.Contains(type);
        }
    }
}
