﻿using System;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.Quests;

public interface IPlayerExperience
{
    IUserLevelsData ObjectUserLevelsesBalanceData { get; }

    int CurrentLevel { get; set; }

    int CurrentExperience { get; set; }
    
    event Action<int> LevelChanged;
    event Action<int> ExperienceChanged;
    
    void Init();

    void AddExperience(int experienceToAdd, bool autoLevelUp);

    void LevelUp(int levelCount);
}