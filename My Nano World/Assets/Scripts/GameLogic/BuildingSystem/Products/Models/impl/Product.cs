using Assets.NanoLib.Utilities;
using NanoLib.Core;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.Utilites;
using Newtonsoft.Json;

namespace NanoReality.GameLogic.BuildingSystem.Products.Models.Api
{
    /// <summary>
    /// Player's product object class
    /// </summary>
    public class Product 
    {
        /// <summary>
        /// contains product ID
        /// </summary>
        [JsonProperty("id")]
        public IntValue ProductTypeID { get; set; }

        /// <summary>
	    /// product's name set in admin panel (balance data)
	    /// </summary>
	    [JsonProperty("name")]
	    public string Name { get; set; }

        /// <summary>
        /// contains level needed for unlock item
        /// </summary>
        [JsonProperty("player_level")]
        public int UnlockLevel { get; set; }

        [JsonProperty("hard_price")]
        public int HardPrice { get; set; }

        /// <summary>
        /// contains section id of product category
        /// </summary>
        [JsonProperty("bss_id")]
        public Id<IBusinessSector> SectorID { get; set; }

        /// <summary>
        /// Experience for one unit of product production
        /// </summary>
        [JsonProperty("exp_up")]
        public int ExperienceUp { get; set; }

        /// <summary>
        /// Used to determine special items (0 - special item)
        /// </summary>
        [JsonProperty("level")]
        public int Level { get; set; }

        /// <summary>
        /// Building type id where the product can be acquired
        /// </summary>
        [JsonProperty("goto")]
        public int BuildingTypeId { get; private set; }
	    
	    /// <summary>
	    /// If product is acquired from toolbox in order desk then value will be other than None
	    /// </summary>
	    [JsonProperty("bss_order")]
	    public SpecialOrderType SpecialOrder { get; private set; }

	    /// <summary>
	    /// contains name of the product
	    /// </summary>
	    public string ProductName => LocalizationUtils.GetProductText(ProductTypeID);
	    
	    public static int ParseProductName(string name)
	    {
		    var split = name.Split('_');
		    return int.Parse(split[0]);
	    }
    }

	public enum SpecialOrderType
	{
		None,
		Town = 1,
		Heavy = 3,
		Farm = 5
	}
}
