using System.Collections.Generic;
using Assets.NanoLib.UI.Core.Signals;
using GameLogic.Taxes.Service;
using NanoLib.Core.Services.Sound;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityMap.Controllers;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.GameManager.Models.api;

namespace NanoReality.GameLogic.City.Models.Impl
{
    public class MCityPopulation : ICityPopulation
    {
        [Inject] public IGameManager jGameManager { get; set; }
        [Inject] public SignalOnMovedMapObjectOnServer jSignalMoveMapObject { get; set; }
        [Inject] public SignalOnMapObjectBuildFinished jSignalOnMapObjectBuildFinished { get; set; }
        [Inject] public SignalOnCityPopulationUpdated SignalOnCityPopulationUpdated { get; set; }
        [Inject] public SignalOnBuildingRoadConnectionChanged SignalOnBuildingRoadConnectionChanged { get; set; }
        [Inject] public SignalBuildFinishVerificated jSignalBuildFinishVerified { get; set; }
        [Inject] public UserLevelUpSignal jUserLevelUpSignal { get; set; }
        [Inject] public IMapObjectsData jMapObjectsData { get; set; } 
        [Inject] public ISoundManager jSoundManager { get; set; }

        public int GetPopulationCapacity()
        {
            var buildings = GetTownSectorDwellingBuildings();
             

            if (buildings == null)
            {
                return 0;
            }
            
            int result = 0;
            
            for (int i = 0; i < buildings.Count; i++)
            {
                result += ((IDwellingHouseMapObject)buildings[i]).MaxPopulation;
            }

            return result;
        }

        /// <summary>
        /// Returns total amount of people in the city (depends on citizen in dwellhouses)
        /// </summary>
        /// <returns></returns>
        public int GetTotalCityPopulation()
        {
            var result = 0;
            
            var buildings = GetTownSectorDwellingBuildings();
            if (buildings == null)
                return result;
            
            foreach (var building in buildings)
            {
                var dwellingHouse = (IDwellingHouseMapObject) building;
                result += dwellingHouse.IsUpgradingNow
                    ? jMapObjectsData.GetPreviousBuildingLevelData(dwellingHouse).MaxPopulation
                    : dwellingHouse.MaxPopulation;
            }

            return result;
        }

        [PostConstruct]
        public void PostConstruct()
        {
            jSignalMoveMapObject.AddListener(mapObject =>
            {
                if (mapObject.MapObjectType == MapObjectintTypes.DwellingHouse)
                    SignalOnCityPopulationUpdated.Dispatch();
            });

            jSignalBuildFinishVerified.AddListener((mapObject =>
            {
                if (mapObject.MapObjectType == MapObjectintTypes.Road)
                    return;
                if (mapObject.MapObjectType == MapObjectintTypes.DwellingHouse ||
                    mapObject.MapObjectType == MapObjectintTypes.CityHall)
                {
                    jSoundManager.Play(SfxSoundTypes.PopulationIncrease, SoundChannel.SoundFX);
                    SignalOnCityPopulationUpdated.Dispatch();
                }
            }));
            
            SignalOnBuildingRoadConnectionChanged.AddListener(mapObject =>
            {
                if (mapObject.MapObjectType == MapObjectintTypes.DwellingHouse && !mapObject.IsUpgradingNow)
                    SignalOnCityPopulationUpdated.Dispatch();
            });

            jUserLevelUpSignal.AddListener(() =>
            {
                SignalOnCityPopulationUpdated.Dispatch();
            });
        }

        private List<IMapObject> GetTownSectorDwellingBuildings()
        {
            return jGameManager.FindAllMapObjectsOnMapByType(BusinessSectorsTypes.Town, MapObjectintTypes.DwellingHouse);
        }
    }
}
