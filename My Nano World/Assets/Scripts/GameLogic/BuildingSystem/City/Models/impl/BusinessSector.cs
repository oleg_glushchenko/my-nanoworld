﻿using System;
using Assets.NanoLib.Utilities;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using Newtonsoft.Json;

namespace NanoReality.GameLogic.BuildingSystem.City.Models.Impl
{
    [Serializable]
    public class BusinessSector : IBusinessSector
    {
        /// <summary>
        /// contains type of the region
        /// </summary>
        [JsonProperty("id")]
        public Id<IBusinessSector> BusinessSectorType { get; set; }

        /// <summary>
        /// contains name of the current region
        /// </summary>
        [JsonProperty("name")]
        public string BusinessSectorName { get; set; }
    }
}
