﻿using System.Collections.Generic;
using System.Linq;
using Assets.NanoLib.Utilities;
using Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Models.impl;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using NanoReality.GameLogic.BuildingSystem.GlobalCityMap.Models;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using Newtonsoft.Json;

namespace NanoReality.GameLogic.BuildingSystem.CityMap.Models.Impl
{
    /// <summary>
    /// Represents base city interface
    /// </summary>
    public class MUserCity : IUserCity
    {
        /// <summary>
        /// name of the current city
        /// </summary>
        [JsonProperty("name")]
        public string CityName { get; set; }

        /// <summary>
        /// Global city map with PowerStation, Casino etc.
        /// </summary>
        public IGlobalCityMap GlobalCityMap { get; set; }
        


        /// <summary>
        /// Список карт секторов в городе
        /// </summary>
        [JsonIgnore]
        public List<ICityMap> CityMaps { get; set; }
        
        [JsonProperty("id")]
        public Id<IUserCity> Id { get; set; }
        
		[JsonProperty("stat")]
        public CityStatistics CityStatistics { get; set; }
        
        [JsonProperty("happiness")]
        public int Happiness { get; set; }

        public MUserCity()
        {
            CityMaps = new List<ICityMap>();
            CityStatistics = new CityStatistics();
        }

        /// <summary>
        /// Получить сектор города по идентификатору
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ICityMap GetBusinessSectorById(BusinessSectorsTypes id)
        {
	        return CityMaps?.FirstOrDefault(city => city.BusinessSector == id);
        }

        public IMapObject GetMapObjectInCityByID(Id_IMapObject mapObjectID)
        {
            for (int i = 0, CityMapsCount = CityMaps.Count; i < CityMapsCount; i++)
			{
				var map = CityMaps [i];
				var mapObj = map.MapObjects.Find (o => o.MapID == mapObjectID);
				if (mapObj != null)
					return mapObj;
			}

            return null;
        }

        public IMapObject GetMapObjectInCityByType(MapObjectintTypes mapObjectType)
        {
            for (int i = 0, CityMapsCount = CityMaps.Count; i < CityMapsCount; i++)
			{
				var map = CityMaps [i];
				var mapObj = map.MapObjects.Find (o => o.MapObjectType == mapObjectType);
				if (mapObj != null)
					return mapObj;
			}

            return null;
        }
    }
}
