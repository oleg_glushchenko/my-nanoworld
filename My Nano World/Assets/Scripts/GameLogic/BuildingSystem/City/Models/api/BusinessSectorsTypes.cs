namespace NanoReality.GameLogic.BuildingSystem.City.Models.Api
{
    public enum BusinessSectorsTypes
    {
        Unsupported = -1,
        Global = 0,
        Town = 1,
        HeavyIndustry = 3,
        Farm = 5,
    }
}