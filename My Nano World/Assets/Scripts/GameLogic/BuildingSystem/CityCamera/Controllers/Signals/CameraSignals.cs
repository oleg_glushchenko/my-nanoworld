﻿using Assets.NanoLib.Utilities;
using strange.extensions.signal.impl;
using NanoReality.GameLogic.BuildingSystem.CityCamera.Models.Api;

namespace NanoReality.GameLogic.BuildingSystem.CityCamera.Controllers
{
	/// <summary>
	/// Signal to zoom camera
	/// </summary>
	public class StartCameraAutoZoomSignal : Signal<float, float, bool>
	{

	}

	/// <summary>
    /// Сигнал от объекта камеры, о том, что изменились координаты камеры
    /// </summary>
    public class SignalOnCameraSwiped : Signal<Vector3F>
    {

    }

	public class InertiaCameraSignal : Signal<Vector3F>
	{

	}

    public class ZoomCameraSignal : Signal<float>
    {

    }

	/// <summary>
    /// Signal to move camera on some coords
    /// </summary>
	public class SignalOnAutoSwipeCamera : Signal<Vector3F, CameraSwipeMode, float, bool>
    {

    }

    public class SignalOnManualSwipe : Signal { }

    public class SignalOnManualZoom : Signal { }
	
	public class SignalEnableCamera : Signal { }
	public class SignalDisableCamera : Signal { }
}