﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using Assets.NanoLib.UtilitesEngine.CustomDebugging.Models.api;
using NanoLib.Services.InputService;
using Assets.NanoLib.Utilities;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using NanoLib.Core.Logging;
using NanoLib.Core.Logging.Settings;
using NanoReality.Engine.BuildingSystem.CityCamera;
using NanoReality.Engine.BuildingSystem.GlobalCityMap;
using NanoReality.GameLogic.BuildingSystem.CityCamera.Controllers;
using NanoReality.GameLogic.BuildingSystem.CityCamera.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.GameManager.Models.api;
using NanoReality.GameLogic.Utilities;
using UniRx;
using UnityEngine;

namespace NanoReality.GameLogic.BuildingSystem.CityCamera.Models.Impl
{
    public class MGameCamera : IGameCamera
    {
        #region Injects

        [Inject] public IGameManager jGameManager { get; set; }
        [Inject] public GameCameraSettings jGameCameraSettings { get; set; }
        [Inject] public SignalOnPinch jSignalOnPinch { get; set; }
        [Inject] public SignalOnPinchEnd jSignalOnPinchEnd { get; set; }
        [Inject] public SignalCameraSwipe jSignalCameraSwipe { get; set; }
        [Inject] public SignalOnTouchNotSwipeEnd jSignalOnTouchNotSwipeEnd { get; set; }
        [Inject] public SignalOnCameraSwiped jSignalOnSwipeCamera { get; set; }
        [Inject] public InertiaCameraSignal jInertiaOnSwipeCamera { get; set; }
        [Inject] public ZoomCameraSignal jSignalOnZoomCamera { get; set; }
        [Inject] public StartCameraAutoZoomSignal jStartCameraAutoZoomSignal { get; set; }
        [Inject] public SignalOnAutoSwipeCamera jSignalOnAutoSwipeCamera { get; set; }
        [Inject] public IGameCameraModel jGameCameraModel { get; set; }

        #endregion

        private bool InZoomRange => jGameCameraModel.CurrentZoomProperty.Value > jGameCameraSettings.MinZoomRange 
                                    && jGameCameraModel.CurrentZoomProperty.Value < jGameCameraSettings.MaxZoomRange;
      
        private CameraBounds _bounds;
        private float _height;
        private float _minPosX;
        private float _maxPosX;
        private float _minPosZ;
        private float _maxPosZ;
        private bool _isInitialized;
        private bool _isZoomEnabled;
        private Vector3F _lastSwipeOffset;
        private IDisposable _disposable;
        private Camera _camera;

        public MGameCamera()
        {
            _isInitialized = false;
            _isZoomEnabled = true; // по умолчанию зум включен
        }

        [PostConstruct]
        public void PostConstruct()
        {
            jSignalCameraSwipe.AddListener(Swipe);
            jSignalOnTouchNotSwipeEnd.AddListener(SwipeEnd);
            jSignalOnPinch.AddListener(Zoom);
            jSignalOnPinchEnd.AddListener(ZoomFinish);
        }

        public void Init(CameraBounds bounds, Vector3F position, float angle, Camera camera)
        {
            _bounds = bounds;
            _camera = camera;
            _height = position.Y;
            jGameCameraModel.PositionProperty.Value = position;
            jGameCameraModel.AngleDegreesProperty.Value = angle;
            jGameCameraModel.CurrentZoomProperty.Value = camera.orthographicSize;
            jGameCameraModel.MaxZoomProperty.Value = jGameCameraSettings.MaxZoom;
            jGameCameraModel.MinZoomProperty.Value = jGameCameraSettings.MinZoom;
            jGameCameraModel.DefaultZoomProperty.Value = jGameCameraSettings.DefaultZoom;
            jGameCameraModel.CurrentZoomPercentProperty.Value = (jGameCameraModel.CurrentZoomProperty.Value - jGameCameraModel.MinZoomProperty.Value) 
                                                     / (jGameCameraModel.MaxZoomProperty.Value - jGameCameraModel.MinZoomProperty.Value);

            _isInitialized = true;
        }

        /// <summary>
        /// Вкючаем или выключаем возможность зума
        /// </summary>
        /// <param name="state">true - включаем zoom, false - выключаем zoom</param>
        public void SetZoomStatus(bool state)
        {
            _isZoomEnabled = state;
        }

        /// <summary>
        /// calculate current zoom (taking into account sensivity)
        /// </summary>
        public void Zoom(float zoomStrenght)
        {
            if (!_isInitialized || jGameCameraModel.IsNeedToLockSwipeProperty.Value || !_isZoomEnabled) return;

            jGameCameraModel.CurrentZoomProperty.Value += zoomStrenght * jGameCameraSettings.ZoomSensitivity;
            jGameCameraModel.CurrentZoomProperty.Value = GetClampZoom(jGameCameraModel.CurrentZoomProperty.Value, true);

            jSignalOnZoomCamera.Dispatch(jGameCameraModel.CurrentZoomProperty.Value);
            Swipe(Vector3F.Zero); // for cases when zoom (in/out) can cause issues with bounds
        }

        private void ZoomFinish()
        {
            if (!_isInitialized) return;

            AutoZoom(jGameCameraModel.CurrentZoomProperty.Value, jGameCameraSettings.AutoZoomDefaultSpeed);
        }

        private void UpdateShaderOnSizeChange(float currant, float min, float max)
        {
            Shader.SetGlobalFloat("_CameraSize", Mathf.InverseLerp(min, max, currant ));
        }

        private float GetClampZoom(float zoom, bool canUseZoomOverflow)
        {
            var currentMinZoom = jGameCameraModel.MinZoomProperty.Value;
            var currentMaxZoom = jGameCameraModel.MaxZoomProperty.Value;
            
            if (canUseZoomOverflow)
            {
                currentMinZoom = jGameCameraSettings.MinZoomOverflow;
                currentMaxZoom = jGameCameraSettings.MaxZoomOverflow;
            }

            UpdateShaderOnSizeChange(zoom, currentMinZoom, currentMaxZoom);
            
            return Mathf.Clamp(zoom, currentMinZoom, currentMaxZoom); //make sure that current zoom is between allowed min/max thresholds
        }

        /// <summary>
        /// Calculate current auto zoom
        /// </summary>
        /// <param name="zoomStrenght">zoom distance</param>
        /// <param name="speed"></param>
        /// <param name="hardAutoZoom"></param>
        public void AutoZoom(float zoomStrenght, float speed = -1, bool hardAutoZoom = false)
        {
            StopInertia();
            jGameCameraModel.CurrentZoomProperty.Value =
                GetClampZoom(zoomStrenght, false); //make sure that current zoom is between allowed min/max thresholds
            CheckPositionThresholds();
            jStartCameraAutoZoomSignal.Dispatch(zoomStrenght, speed, hardAutoZoom);
        }

        /// <summary>
        /// Move camera to some position and zoom it (object on map)
        /// </summary>
        public void ZoomTo(Vector2F swipePosition, CameraSwipeMode mode, float zoomStrenght, float speed = -1,
            bool hardAutoSwipe = false)
        {
            AutoZoom(zoomStrenght, speed);
            SwipeTo(swipePosition, mode, speed, hardAutoSwipe);
        }

        public void ZoomTo(IMapObject target, CameraSwipeMode mode, float zoomStrenght, float speed = -1,
            bool hardAutoSwipe = false)
        {
            var pos = GetMapObjectPosition(target);

            AutoZoom(zoomStrenght, speed);
            SwipeTo(pos, mode, speed, hardAutoSwipe);
        }

        public void ZoomTo(AGlobalCityBuildingView view, CameraSwipeMode mode, float zoomPercent, float speed = -1,
            bool hardAutoSwipe = false)
        {
            var zoom = Mathf.Lerp(jGameCameraModel.MinZoomProperty.Value, jGameCameraModel.MaxZoomProperty.Value, 0.5f);
            var worldPosition = view.WorldPosition;
            Vector2F pos = new Vector2F(worldPosition.x + 2, worldPosition.z + 2);
            AutoZoom(zoom);
            SwipeTo(pos, CameraSwipeMode.Fly);
        }

        public void FocusOnMapObject(IMapObject mapObject)
        {
            if (!InZoomRange)
            {
                AutoZoom(jGameCameraSettings.NormalZoom, 10);
            }
            
            var position = GetMapObjectPosition(mapObject);
            SwipeTo(position, CameraSwipeMode.Fly, 10f, true);
        }

        public Vector2F GetMapObjectPosition(IMapObject mapObject)
        {
            if (mapObject == null)
            {
                return new Vector2F(0f, 0f);
            }

            var mapObjectView = jGameManager.GetMapObjectView(mapObject);
            var worldPosition = mapObjectView.WorldPosition;
            var depthOffset = MapObjectGeometry.GetHeightOffset(worldPosition.y, jGameCameraModel.AngleDegreesProperty.Value);
            worldPosition.x += depthOffset;
            worldPosition.z += depthOffset;

            return new Vector2F(worldPosition.x, worldPosition.z) +
                   mapObjectView.WorldSpaceDimensions.ToVector2F() / 2f;
        }

        /// <summary>
        /// calculate swipe direction along with swipe sensivity
        /// </summary>
        public void Swipe(Vector3F swipeDirection)
        {
            if (!_isInitialized || jGameCameraModel.IsNeedToLockSwipeProperty.Value)
            {
                return;
            }

            StopInertia();

            var swipeSensitivity = Mathf.Lerp(jGameCameraSettings.SwipeSensitivityMin,
                jGameCameraSettings.SwipeSensitivityMax, jGameCameraModel.CurrentZoomPercentProperty.Value);
            _lastSwipeOffset = swipeDirection * swipeSensitivity;
            jGameCameraModel.PositionProperty.Value += _lastSwipeOffset;
            CheckPositionThresholds();
            jSignalOnSwipeCamera.Dispatch(jGameCameraModel.PositionProperty.Value);
        }

        private void SwipeEnd()
        {
            if (!_isInitialized || _lastSwipeOffset == Vector3F.Zero)
            {
                return;
            }

            StartInertia(_lastSwipeOffset);
        }

        private void StartInertia(Vector3F lastSwipeOffset)
        {
            StopInertia();
            _disposable = Observable.FromCoroutine(() => InertiaCoroutine(lastSwipeOffset)).Subscribe();
        }

        private void StopInertia()
        {
            _disposable?.Dispose();
            
            _lastSwipeOffset = Vector3F.Zero;
        }

        private IEnumerator InertiaCoroutine(Vector3F lastSwipeOffset)
        {
            var waitForInertialLag = new WaitForSeconds(jGameCameraSettings.InertialLag);
            
            while (true)
            {
                lastSwipeOffset *= jGameCameraSettings.InertialDamp;
                var threshold = jGameCameraSettings.InertialThreshold;
                
                if (Mathf.Abs(lastSwipeOffset.X) < threshold &&
                    Mathf.Abs(lastSwipeOffset.Y) < threshold &&
                    Mathf.Abs(lastSwipeOffset.Z) < threshold)
                {
                    StopInertia();
                    CheckPositionThresholds();
                    jInertiaOnSwipeCamera.Dispatch(jGameCameraModel.PositionProperty.Value);
                    yield break;
                }

                jGameCameraModel.PositionProperty.Value += lastSwipeOffset;
                CheckPositionThresholds();
                jInertiaOnSwipeCamera.Dispatch(jGameCameraModel.PositionProperty.Value);
                yield return waitForInertialLag;
            }
        }

        /// <summary>
        /// find minimum and maximum values for camera borders
        /// </summary>
        private void CalculateCameraMinMaxBorders(ICityMap cityMap)
        {
            var grid = cityMap.jCityGrid;

            var listX = new List<float>();
            var listZ = new List<float>();

            listX.Add(grid[0][0].GlobalPosition.X);
            listX.Add(grid[0][grid.Height - 1].GlobalPosition.X);
            listX.Add(grid[grid.Width - 1][0].GlobalPosition.X);
            listX.Add(grid[grid.Width - 1][grid.Height - 1].GlobalPosition.X);

            listZ.Add(grid[0][0].GlobalPosition.Y);
            listZ.Add(grid[0][grid.Height - 1].GlobalPosition.Y);
            listZ.Add(grid[grid.Width - 1][0].GlobalPosition.Y);
            listZ.Add(grid[grid.Width - 1][grid.Height - 1].GlobalPosition.Y);

            var angleOffset = MapObjectGeometry.GetHeightOffset(_height, jGameCameraModel.AngleDegreesProperty.Value);

            _minPosX = listX.Min() - angleOffset;
            _maxPosX = listX.Max() - angleOffset;

            _minPosZ = listZ.Min() - angleOffset;
            _maxPosZ = listZ.Max() - angleOffset;
        }

        /// <summary>
        /// check whether camera position limits were reached
        /// </summary>
        private void CheckPositionThresholds()
        {
            if (_bounds == null)
            {
                return;
            }

            var offset = GetGroundOffset();
            var position = new Vector3(jGameCameraModel.PositionProperty.Value.X + offset, 0, jGameCameraModel.PositionProperty.Value.Z + offset);
            var clamped = _bounds.ClampByBorders(position, jGameCameraModel.CurrentZoomProperty.Value);
            jGameCameraModel.PositionProperty.Value = new Vector3F(clamped.x - offset, jGameCameraModel.PositionProperty.Value.Y, clamped.z - offset);
        }

        /// <summary>
        /// move camera to some position
        /// </summary>
        public void SwipeTo(Vector3F swipeDirection, CameraSwipeMode mode, float speed = -1, bool hardAutoSwipe = false)
        {
            Logging.Log(LoggingChannel.Camera, $"Swipe camera to: {swipeDirection} Mode: {mode}");
            StopInertia();
            jGameCameraModel.PositionProperty.Value = swipeDirection;
            jSignalOnAutoSwipeCamera.Dispatch(jGameCameraModel.PositionProperty.Value, mode, speed, hardAutoSwipe);
        }

        /// <summary>
        /// Move camera to some position (object on map)
        /// </summary>
        public void SwipeTo(Vector2F swipePosition, CameraSwipeMode mode, float speed = -1, bool hardAutoSwipe = false)
        {
            var xzOffset = GetGroundOffset();
            var newPos = new Vector3F(swipePosition.X - xzOffset, _height, swipePosition.Y - xzOffset);
            SwipeTo(newPos, mode, speed, hardAutoSwipe);
        }

        private float GetGroundOffset()
        {
            var length = _height / Mathf.Tan((jGameCameraModel.AngleDegreesProperty.Value) * Mathf.Deg2Rad);
            var xzOffset = (length / Mathf.Cos(45 * Mathf.Deg2Rad)) / 2;
            return xzOffset;
        }

        /// <summary>
        /// Align the camera to some position of city
        /// </summary>
        /// <param name="cityMap"></param>
        /// <param name="align">Camera align position</param>
        /// <param name="mode">Camera swipe mode</param>
        /// <param name="speed"></param>
        public void AlignCamera(ICityMap cityMap, CameraAlignPosition align, CameraSwipeMode mode, float speed = -1)
        {
            CalculateCameraMinMaxBorders(cityMap);

            float x = jGameCameraModel.PositionProperty.Value.X, y = jGameCameraModel.PositionProperty.Value.Y, z = jGameCameraModel.PositionProperty.Value.Z;

            switch (align)
            {
                case CameraAlignPosition.Top:
                    x = _maxPosZ;
                    z = _maxPosZ;
                    break;

                case CameraAlignPosition.Left:
                    x = _minPosZ;
                    z = _maxPosX;
                    break;

                case CameraAlignPosition.Right:
                    x = _maxPosZ;
                    z = _minPosX;
                    break;

                case CameraAlignPosition.Bottom:
                    x = _minPosX;
                    z = _minPosZ;
                    break;

                case CameraAlignPosition.Center:
                    x = (_minPosX + _maxPosX) / 2.0f;
                    z = (_minPosZ + _maxPosZ) / 2.0f;
                    break;
            }

            SwipeTo(new Vector3F(x, y, z), mode, speed);
        }

        public void SetGrayscaleActive(bool value)
        {
            jGameCameraModel.GrayscaleActiveProperty.Value = value;
        }

        public Vector3 WorldToScreenPoint(Vector3 position)
        {
            return _camera.WorldToScreenPoint(position);
        }

        public Vector3 ScreenToWorldPoint(Vector3 position)
        {
            return _camera.ScreenToWorldPoint(position);
        }

        public Vector3 WorldToViewportPoint(Vector3 position)
        {
            return _camera.WorldToViewportPoint(position);
        }

        public Vector3 ScreenToViewportPoint(Vector3 position)
        {
            return _camera.ScreenToViewportPoint(position);
        }
        
        public void SetActive(bool value)
        {
            _camera.enabled = value;
        }

        public bool RenderTheWholeObject(Bounds bounds)
        {
            Plane[] planes = GeometryUtility.CalculateFrustumPlanes(_camera);
            return MapObjectGeometry.TestPlanesAABBInside(planes, bounds.min, bounds.max);
        }
    }
}