using Newtonsoft.Json;
using UnityEngine;

namespace NanoReality.Game.Data
{
    /// <summary>
    /// Information about area of effect zone from aoe building.
    /// </summary>
    public sealed class AoeBuildingZone
    {
        /// <summary>
        /// Id of map object on the sector map.
        /// </summary>
        [JsonProperty("id")]
        public int MapObjectId;

        /// <summary>
        /// Left bottom point of aoe zone rect.
        /// </summary>
        [JsonProperty("point2")]
        public Vector2 Point1;

        /// <summary>
        /// Right top point of aoe zone rect.
        /// </summary>
        [JsonProperty("point1")]
        public Vector2 Point2;
    }
}