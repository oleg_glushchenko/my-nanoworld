﻿using System;
using System.Collections.Generic;
using Assets.NanoLib.Utilities;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Impl;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using Newtonsoft.Json;
using strange.extensions.signal.impl;
using UnityEngine;


namespace NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api
{

    /// <summary>
    /// Вызывается когда строится несколько объектов на карте одновременно (тайлы дороги)
    /// </summary>
    public class SignalOnMountObjects:Signal<List<IMapObject>> { }


    /// <summary>
    /// Вызывается когда удаляется несколько тайлов одороги одновременно
    /// </summary>
    public class SignalOnDeleteRoads : Signal<List<Id_IMapObject>> { }


    /// <summary>
    /// Represent base city map interface
    /// </summary>
    public interface  ICityMap
    {

        /// <summary>
        /// Список зданий на карте сектора
        /// </summary>
        List<IMapObject> MapObjects { get; set; }

        /// <summary>
        /// Размер карты
        /// </summary>
        MapSize MapSize { get; set; }

        /// <summary>
        /// City ID where map located
        /// </summary>
        Id<IUserCity> UserCityId { get; set; }

        /// <summary>
        /// Business sector where map located
        /// </summary>
        BusinessSectorsTypes BusinessSector { get; set; }

        /// <summary>
        /// Залоченные сектора
        /// </summary>
        CitySubLockedMap SubLockedMap { get; set; }


        /// <summary>
        /// City Grid
        /// </summary>
        ICityGrid jCityGrid { get; set; }

        /// <summary>
        /// Возвращает среднюю позицию всех зданий на карте
        /// </summary>
        /// <returns></returns>
        Vector2F GetAverageBuildingsPosition();

        /// <summary>
        /// Ищет первый объект с типом <param name="type"></param>
        /// </summary>
        /// <param name="type">тип здания который нужно найти</param>
        /// <returns></returns>
        IMapObject FindMapObject(MapObjectintTypes type);
        
        /// <summary>
        /// Finds a map object by type id
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        IMapObject FindMapObject(Id_IMapObjectType type);

        /// <summary>
        /// Ищет все объекты заданного типа
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        List<IMapObject> FindAllMapObjects(MapObjectintTypes type);

        /// <summary>
        /// Finds all map object with type id
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        List<IMapObject> FindAllMapObjects(Id_IMapObjectType type);

        /// <summary>
        /// Search a map object using grid coords
        /// </summary>
        /// <param name="position">grid cell coordinate</param>
        /// <returns>object on map</returns>
        IMapObject GetMapObject(Vector2F position);

        /// <summary>
        /// Search a map object using object ID
        /// </summary>
        /// <param name="mapId">grid cell coordinate</param>
        /// <returns>object on map</returns>
        IMapObject GetMapObject(Id_IMapObject mapId);

        /// <summary>
        /// Set objects on map from MapObjects List
        /// </summary>
        /// <param name="onInitCallback"></param>
        void InitMap(Action onInitCallback = null);
        void RestoreMap(Action onInitCallback = null);

        /// <summary>
        /// Перемещает объект на карте (с изменением занятых клеток)
        /// </summary>
        /// <param name="oldPosition">Старая позиция объекта</param>
        /// <param name="newPosition">Новая позиция объекта</param>
        /// <param name="mapObjectId">айди объекта</param>
        /// <returns></returns>
        void MoveMapObject(Vector2Int oldPosition, Vector2Int newPosition, MMapObject mapObject);

        /// <summary>
        /// Строит объект на карте
        /// </summary>
        /// <param name="mapObject">объект который нужно построить</param>
        /// <param name="toVector2F">позиция где он должен быть построен</param>
        void MountObject(IMapObject mapObject, Vector2Int toVector2F);

        void RestoreObject(IMapObject mapObject, Vector2Int toVector2F, Action callback);

        /// <summary>
        /// Строит объект на карте
        /// </summary>
        /// <param name="mapObject">объект который нужно построить</param>
        void MountRoads(List<IMapObject> mapObject);

        /// <summary>
        /// Начинает апгрейд здания на карте
        /// </summary>
        /// <param name="mapId"></param>
        /// <param name="withProducts"></param>
        void UpgradeObject(Id_IMapObject mapId, bool withProducts = false);

        /// <summary>
        /// Ищет объект по его айди
        /// </summary>
        /// <param name="mapId">айди объекта который нужно найти на карте</param>
        /// <returns></returns>
        IMapObject GetMapObjectById(Id_IMapObject mapId);
        
        T GetMapObjectById<T>(int buildingMapId) where T : IMapObject;

        /// <summary>
        /// Return amount of objects by type.
        /// </summary>
        /// <param name="mapObjectType"></param>
        /// <returns></returns>
        int GetMapObjectsCountByType(MapObjectintTypes mapObjectType);

        /// <summary>
        /// Удаляет объект с карты
        /// </summary>
        /// <param name="mapObject">Айди объекта который нужно удалить</param>
        void RemoveMapObjectById(Id_IMapObject mapObject);

        /// <summary>
        /// Удаляет объект с карты
        /// </summary>
        /// <param name="position">Позиция объяекта на карте, который нужно удалить</param>
        void RemoveMapObjectOnCell(Vector2F position);

        /// <summary>
        /// Удаялет объект с карты
        /// </summary>
        /// <param name="mapObject">Объект который нужно удалить</param>
        void RemoveMapObject(IMapObject mapObject);


       /// <summary>
       /// Удаляет тайлы дороги с карты
       /// </summary>
       /// <param name="road"></param>
       /// <param name="complete"></param>
        void DeleteRoad(List<Id_IMapObject> road, Action complete);
        
        /// <summary>
        /// Remove all objects from a map
        /// </summary>
        void ClearMap();

        /// <summary>
        /// Remove only views from a map
        /// </summary>
        void ClearMapView();
    }


    /// <summary>
    /// Размер карты
    /// </summary>
    public class MapSize
    {
        /// <summary>
        /// Ширина в тайлах
        /// </summary>
        [JsonProperty("width")]
        public int Width;

        /// <summary>
        /// Высота в тайлах
        /// </summary>
        [JsonProperty("height")]
        public int Height;
    }



}
