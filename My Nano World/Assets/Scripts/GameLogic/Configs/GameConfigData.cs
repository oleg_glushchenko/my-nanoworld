﻿using System;
using System.Collections.Generic;
using Assets.NanoLib.Utilities.DataManager.Models.api;
using NanoReality.Game.FoodSupply;
using NanoReality.GameLogic.OrderDesk;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using NanoReality.GameLogic.StartApplicationSequence.Models.api;
using Newtonsoft.Json;

namespace NanoReality.GameLogic.Configs
{
    public class GameConfigData : IGameConfigData
    {
        [JsonProperty("taxes_city_hall_icon_appear")]
        public float CollectedCoinsToShow { get; private set; }

        [JsonProperty("metro")]
        public MetroConfig MetroConfig { get; private set; }

        [JsonProperty("food_supply")]
        public FoodSupplyConfig FoodSupplyConfig { get; private set; }
        
        [JsonProperty("ads_limit")]
        public int AdsLimit { get; private set; }

        [JsonProperty("souk")]
        public SoukConfig SoukConfig { get; private set; }
        
        [JsonProperty("tutorial")]
        public TutorialConfigDate TutorialConfigDate { get; private set; }
        
        [JsonProperty("layout")]
        public List<EditModeLayoutData> EditModeData { get; private set; }
        
        // TODO: need to configure this parameter on server side.
        public int DailyBonusInterval => 24 * 3600;

        public string DataHash { get; set; }
        
        public void LoadData(IServerCommunicator serverCommunicator, ILocalDataManager localDataManager, Action<IGameBalanceData> callback)
        {
            serverCommunicator.LoadGameConfigData(this, callback);
        }
    }
}