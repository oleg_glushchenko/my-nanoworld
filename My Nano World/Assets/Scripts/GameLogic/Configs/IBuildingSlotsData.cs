﻿using System.Collections.Generic;
using NanoReality.GameLogic.StartApplicationSequence.Models.api;

namespace NanoReality.GameLogic.Configs
{
    public interface IBuildingSlotsData : IGameBalanceData
    {
        List<BuildingSlotData> BuildingSlotsDataList { get; }
    }
}