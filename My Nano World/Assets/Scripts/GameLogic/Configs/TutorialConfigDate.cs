﻿using System.Collections.Generic;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using Newtonsoft.Json;
using UnityEngine;

namespace NanoReality.GameLogic.Configs
{
	public class TutorialConfigDate
	{
		[JsonProperty("time_produce")]
		public int ProductionTime { get; private set; }
        
		[JsonProperty("step_action")]
		public List<TutorialStepActionData> HardStepActionData { get; private set; }
		
		[JsonProperty("hint_step_action")]
		public List<TutorialStepActionData> HintStepActionData { get; private set; }
	}
	
	public class TutorialStepActionData
	{
		[JsonProperty("step_id")]
		public int StepId { get; private set; }
        
		[JsonProperty("action")]
		public List<TutorialActionData> ActionData { get; private set; }
	}
    
	public class TutorialActionData
	{
		[JsonProperty("type")]
		public TutorialActionType ActionType { get; set; }
        
		[JsonProperty("bss_id")]
		public BusinessSectorsTypes BusinessSectorsType { get; set; }
        
		[JsonProperty("limit")]
		public List<Rect> TutorialDragLimit { get; set; }
        
		[JsonProperty("point")]
		public List<Vector2> Positions { get; set; }
		
		[JsonProperty("building")]
		public List<TutorialBuildingData> BuildingData { get; set; }
		
		[JsonProperty("product")]
		public List<TutorialProduceProductData> ProduceProductData { get; set; }
		
		[JsonProperty("quest_id")]
		public int QuestId { get; set; }
	}

	public class TutorialProduceProductData
	{
		[JsonProperty("id")] 
		public int Id { get; set; }
		
		[JsonProperty("slot")] 
		public int Slot { get; set; }	
		
		[JsonProperty("amount")] 
		public int Amount { get; set; }
		
		[JsonProperty("time_produce")]
		public int ProductionTime { get; set; }

		[JsonProperty("building_type")] 
		public MapObjectintTypes MapObjectType { get; set; }
	}

	public class TutorialBuildingData
	{
		[JsonProperty("id")]
		public int Id { get; private set; }
		
		[JsonProperty("level")]
		public int Level { get; private set; }

		[JsonProperty("building_type")]
		public MapObjectintTypes MapObjectType { get; private set; }
		
		[JsonProperty("category_id")]
		public List<BuildingsCategories> Category { get; set; }
		
		[JsonProperty("subcategory_id")]
		public BuildingSubCategories SubCategory { get; private set; }

		public TutorialBuildingData(int id, int level, MapObjectintTypes mapObjectType = MapObjectintTypes.None,
			List<BuildingsCategories> buildingsCategory = null, BuildingSubCategories buildingSubCategory = BuildingSubCategories.None)
		{
			Id = id;
			Level = level;
			MapObjectType = mapObjectType;
			Category = buildingsCategory;
			SubCategory = buildingSubCategory;
		}
	}

	public enum TutorialActionType
	{
		Default = 0,
		Build = 1,
		Move = 2,
		Find = 3,
		MoveCamera =4,
	}
}