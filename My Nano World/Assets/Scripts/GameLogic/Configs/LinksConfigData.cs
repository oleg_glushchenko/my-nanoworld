using Newtonsoft.Json;
using System;

namespace NanoReality.GameLogic.Configs
{
    public class LinksConfigData
    {
        [JsonProperty("contact_email")]
        public string ContactEmail { get; private set; }

        [JsonProperty("privacy_policy_url")]
        public LanguageDependentLinks PrivacyPolicyUrl { get; private set; }

        [JsonProperty("terms_of_use_url")]
        public LanguageDependentLinks TermsOfUseUrl { get; private set; }

        [JsonProperty("apple_app_url")]
        public string AppleAppUrl { get; private set; }

        [JsonProperty("google_app_url")]
        public string GoogleAppUrl { get; private set; }

        [Serializable]
        public class LanguageDependentLinks
        {
            [JsonProperty("en")]
            public string English;

            [JsonProperty("ar")]
            public string Arabic;
        }
    }
}
