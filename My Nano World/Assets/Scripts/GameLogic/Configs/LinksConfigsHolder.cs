
namespace NanoReality.GameLogic.Configs
{
    public class LinksConfigsHolder
    {
        public string ContactEmail { get; private set; }

        public string PrivacyPolicyUrl { get; private set; }

        public string TermsOfUseUrl { get; private set; }

        public string AppleAppUrl { get; private set; }

        public string GoogleAppUrl { get; private set; }

        public void SetLinks(LinksConfigData links)
        {
            ContactEmail = links.ContactEmail;
            AppleAppUrl = links.AppleAppUrl;
            GoogleAppUrl = links.GoogleAppUrl;

            switch (LocalizationMLS.Instance.Language)
            {
                case "en":
                    PrivacyPolicyUrl = links.PrivacyPolicyUrl.English;
                    TermsOfUseUrl = links.TermsOfUseUrl.English;
                    break;
                case "ar":
                    PrivacyPolicyUrl = links.PrivacyPolicyUrl.Arabic;
                    TermsOfUseUrl = links.TermsOfUseUrl.Arabic;
                    break;
            }
        }
    }
}
