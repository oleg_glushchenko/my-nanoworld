﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.GameLogic.DailyBonus.Api;
using NanoReality.GameLogic.Quests;

namespace Assets.Scripts.GameLogic.DailyBonus.Impl
{
    public class AwardActionData// : IAwardAction
    {
        public AwardActionTypes ActionType { get; set; }

        public List<DefaultActionParam> ActionParams { get; set; } 
    }
}
