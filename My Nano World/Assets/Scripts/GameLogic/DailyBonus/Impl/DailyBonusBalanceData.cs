﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.NanoLib.Utilities.DataManager.Models.api;
using Assets.Scripts.Engine.Analytics;
using Assets.Scripts.Engine.Analytics.Extensions.CurrencyInfo;
using Assets.Scripts.GameLogic.DailyBonus.Api;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.Quests;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using NanoReality.GameLogic.StartApplicationSequence.Models.api;
using Newtonsoft.Json;
using NanoReality.GameLogic.Common.Controllers.Signals;

namespace Assets.Scripts.GameLogic.DailyBonus.Impl
{
    public class DailyBonusBalanceData : IDailyBonusBalanceData
    {
        [JsonProperty("days")]
        public List<DailyBonusData> DailyBonusDatas { get; set; }

        [Inject]
        public IPlayerProfile jPlayerProfile { get; set; }

        /// <summary>
        /// analytics signal
        /// </summary>
        [Inject]
        public SignalOnActionWithCurrency jSignalOnActionWithCurrency { get; set; }
        
        [Inject]
        public SignalOnDailyBonusCollected jSignalOnDailyBonusCollected { get; set; }

        public string DataHash { get; set; }
        
        private IServerCommunicator _serverCommunicator;

        public void LoadData(IServerCommunicator serverCommunicator, ILocalDataManager localDataManager, Action<IGameBalanceData> callback)
        {
            _serverCommunicator = serverCommunicator;
            serverCommunicator.LoadDailyBonusBalanceData(this, data =>
            {
                DailyBonusDatas = data.DailyBonusDatas;

                foreach (var bonusData in DailyBonusDatas)
                {
                    foreach (var actionData in bonusData.AwardActionDatas)
                        actionData.InitServerData();
                }

                callback(this);
            });
        }

        public void CollectDailyBonus(DailyBonusData dailyBonusData, Action onSuccess)
        {
            _serverCommunicator.CollectDailyBonus(dailyBonusData.Day, rewards =>
            {
                foreach (IAwardAction awardAction in rewards)
                {
                    awardAction.Execute(AwardSourcePlace.DailyBonus, "Day: " + dailyBonusData.Day);

                    jSignalOnActionWithCurrency.Dispatch(CurrencyInfo.GetInfoFromAwardType(awardAction,
                        CurrencyIncomesActions.DailyBonus, "Day: " + dailyBonusData.Day));
                }
                
                jSignalOnDailyBonusCollected.Dispatch(dailyBonusData);
                
                jPlayerProfile.ResetDailyBonusTimer();               

                if (onSuccess != null)
                    onSuccess();
            });
        }

        public void CollectDailyBonus(int day, Action onSuccess)
        {
            CollectDailyBonus(GetDailyBonusDataByDay(day == 0 ? 1 : day), onSuccess);
        }

        public DailyBonusData GetDailyBonusDataByDay(int day)
        {
            return DailyBonusDatas.FirstOrDefault(data => data.Day == day);
        }
    }
}
