﻿using System.Collections.Generic;
using NanoReality.GameLogic.Quests;
using Newtonsoft.Json;

namespace Assets.Scripts.GameLogic.DailyBonus.Impl
{
    /// <summary>
    /// Данные о награде за определенный день в дейли бонусе
    /// </summary>
    public class DailyBonusData 
    {
        [JsonProperty("day")]
        public int Day { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("award-actions")]
        public List<IAwardAction> AwardActionDatas { get; set; } 
    }
}
