﻿using System;
using System.Collections.Generic;
using Assets.NanoLib.Utilities;
using Assets.NanoLib.Utilities.DataManager.Models.api;
using NanoReality.GameLogic.Achievements.api;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using NanoReality.GameLogic.StartApplicationSequence.Models.api;
using Newtonsoft.Json;

namespace NanoReality.GameLogic.Achievements.impl
{
    public class MAchievementsData : IAchievementsData
    {
        /// <summary>
        /// Список балансовых ачивментов
        /// </summary>
        [JsonProperty("achievements")]
        public List<Achievement> Achievements { get; set; }

        /// <summary>
        /// Возвращает объект ачивмента по ID
        /// </summary>
        /// <param name="ID">ID необходимого ачивмента</param>
        /// <returns>Объект ачивмента</returns>
        public Achievement GetAchievementByID(Id<Achievement> ID)
        {
            foreach (var achievement in Achievements)
                if (achievement.AchievementID == ID)
                    return achievement;

            return null;
        }

        public string DataHash { get; set; }

        public void LoadData(IServerCommunicator serverCommunicator, ILocalDataManager localDataManager,
            Action<IGameBalanceData> callback)
        {
            serverCommunicator.LoadAchievementsData( achievementsList =>
            {
                Achievements = achievementsList;
                foreach (var achievement in Achievements)
                {
                    achievement.InitServerData();
                }
                callback?.Invoke(this);
            });
        }
    }
}