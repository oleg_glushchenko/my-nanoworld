﻿using NanoReality.GameLogic.Achievements;
using strange.extensions.signal.impl;
using System.Collections.Generic;
using NanoReality.GameLogic.Quests;

/// <summary>
/// Сигнал об успешной загрузке пользовательских ачивок
/// </summary>
public class SignalOnUserAchievementsDataLoaded : Signal<IUserAchievementsData>{ }

/// <summary>
/// Сигнал, оповещающий об открытии ачивки
/// </summary>
public class SignalOnUserAchievementUnlocked : Signal{ }

public class AnalyticsAchievementUnlock : Signal<IUserAchievement> { }

/// <summary>
/// Сигнал об успешном забирании пользователем награды за ачивмент
/// </summary>
public class SignalOnAwardTaken : Signal<IUserAchievement, List<IAwardAction>> { }

/// <summary>
/// Сигнал об изменении количества достижений
/// </summary>
public class AchievementCompletedSignal : Signal { }

public class AchievementConditionUpdateSignal : Signal { }


public class SignalOnUserAchievmentsInit : Signal { }

public class ShowAchievementsPanelSignal : Signal { }

public class SignalOnAchievementAttentionTap : Signal { }