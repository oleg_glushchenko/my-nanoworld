using System;
using System.Collections;
using System.Collections.Generic;
using Assets.NanoLib.Utilities;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using NanoReality.GameLogic.InGameNotifications;
using strange.extensions.signal.impl;
using UnityEngine.SocialPlatforms;

namespace NanoReality.GameLogic.InGameNotifications
{
    /// <summary>
    /// Менеджер для управления нотификациями на клиенте
    /// </summary>
    public interface IInGameNotificationsManager
    {
    }
}

/// <summary>
/// Сигнал вызыватся когда клиент получает новые нотификации
/// </summary>
public class SignalOnRecivedNewInGameNotifications : Signal<List<IInGameNotification>> { }
