using System;
using Newtonsoft.Json;

namespace NanoReality.GameLogic.TheEvent.Data
{
    [Serializable]
    public class FinishEventData
    {
        [JsonProperty("product_id")]
        public string ProductStoreId { get; set; }

        [JsonProperty("day")]
        public int Day { get; set; }

        [JsonProperty("price")]
        public float Price { get; set; }
    }
}