using System;
using NanoReality.GameLogic.Data;
using Newtonsoft.Json;

namespace NanoReality.GameLogic.TheEvent.Data
{
    [Serializable]
    public class ClaimStageRewardData
    {
        [JsonProperty("reward")]
        public RewardData[] Rewards;

        [JsonProperty("loot_box_type")]
        public int LootBoxType;
    }
}
