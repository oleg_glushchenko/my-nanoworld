using NanoReality.GameLogic.Quests;
using UnityEngine;

namespace NanoReality.GameLogic.TheEvent
{
    public class DisplayRewardInfo : IAwardAction
    {
        public Sprite Sprite;
        public int AwardId { get; }
        public AwardActionTypes AwardType { get; set; }
        public int AwardCount { get; set; }
        public NanoReality.GameLogic.Quests.AwardProduct GetAwardProduct { get; }

        /// This class do not require the method below
        public void InitServerData()
        {
            throw new System.NotImplementedException();
        }

        /// This class do not require the method below
        public void Execute(AwardSourcePlace sourcePlace, string source)
        {
            throw new System.NotImplementedException();
        }

        /// This class do not require the method below
        public string GetAwardDescriptionText(IAwardActionDescription baseDescription)
        {
            throw new System.NotImplementedException();
        }
    }
}