﻿using strange.extensions.mediation.impl;
using strange.extensions.signal.impl;

namespace NanoReality.GameLogic.GameManager
{
    public class SignalOnApplicationPause : Signal<bool> {}

    /// <summary>
    /// Класс колбеков ивентов от движка
    /// </summary>
    public class ApplicationEventsView : View
    {
        [Inject]
        public SignalOnApplicationPause SignalOnApplicationPause { get; set; }



        void OnApplicationPause(bool pauseStatus)
        {
            if(SignalOnApplicationPause != null)
                SignalOnApplicationPause.Dispatch(pauseStatus);
        }




    }
}

