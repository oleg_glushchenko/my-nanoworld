﻿using System.Collections.Generic;
using Assets.NanoLib.Utilities;
using Assets.Scripts.Engine.BuildingSystem.CityMap;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using NanoReality.Engine.BuildingSystem.CityCamera;
using NanoReality.Engine.BuildingSystem.GlobalCityMap;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using NanoReality.GameLogic.BuildingSystem.GlobalCityMap.Models;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using UnityEngine;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;

namespace NanoReality.GameLogic.GameManager.Models.api
{
    /// <summary>
    /// Гейм. менджер. Отвечает за загрузку карт/городов. Предоставляет данные о текущей загруженной карте, городе
    /// </summary>
    public interface IGameManager
    {
        /// <summary>
        /// Ссылка на камеру в городе
        /// </summary>
        IGameCameraModel CurrentGameCamera { get; set; }

        /// <summary>
        /// Текущий загруженный город
        /// </summary>
        IUserCity CurrentUserCity { get; set; }

        /// <summary>
        /// Айди профиля пользователя, город которого загружен
        /// </summary>
        Id<IPlayerProfile> CurrentProfileId { get; set; }

        Dictionary<BusinessSectorsTypes, CityMapView> CityMapViews { get; }

        /// <summary>
        /// Текущая выделенная вьюшка объекта на карте
        /// </summary>
        MapObjectView CurrentSelectedObjectView { get; set; }

        /// <summary>
        /// Возвращает модель глобальной карты города
        /// </summary>
        IGlobalCityMap GlobalCityMap { get; }

        /// <summary>
        /// Возвращает представление глобальной карты города
        /// </summary>
        GlobalCityMapView GlobalCityMapView { get; }

        /// <summary>
        /// Разрешено ли выделение объектов на карте
        /// </summary>
        bool IsMapObjectSelectEnable { get; set; }

        /// <summary>
        /// Загружена ли игра
        /// </summary>
        bool IsApplicationLoaded { get; }

        /// <summary>
        /// Возвращает загруженную карту по айди сектора
        /// </summary>
        /// <param name="sectorId">айди сектора</param>
        /// <returns></returns>
        ICityMap GetMap(Id<IBusinessSector> sectorId);
        
        ICityMap GetMap(BusinessSectorsTypes sectorId);

        /// <summary>
        /// Возвращает загруженную карту по мировой координате
        /// </summary>
        /// <param name="worldPosition"></param>
        /// <returns></returns>
        ICityMap GetMap(Vector3 worldPosition);


        CityMapView GetMapView(Id<IBusinessSector> sectorId);        
        

        CityMapView GetMapView(BusinessSectorsTypes sectorId);

        /// <summary>
        /// Returns a map object view of given model
        /// </summary>
        /// <param name="mapObject"></param>
        /// <returns></returns>
        MapObjectView GetMapObjectView(IMapObject mapObject);

        /// <summary>
        /// Finds a map object view in specified sector by type, returns first encountered
        /// </summary>
        /// <param name="sectorType"></param>
        /// <param name="objectType"></param>
        /// <returns></returns>
        MapObjectView FindMapObjectViewOnMapByType(BusinessSectorsTypes sectorType, MapObjectintTypes objectType);
        
        /// <summary>
        /// Finds all map object views in specified sector by type, filters by id
        /// </summary>
        /// <param name="sectorType"></param>
        /// <param name="objectType"></param>
        /// <param name="objectId"></param>
        /// <returns></returns>
        MapObjectView FindMapObjectViewOnMapByType(BusinessSectorsTypes sectorType, MapObjectintTypes objectType, int objectId);

        /// <summary>
        /// Finds a map object view in all secotrs including global, returns first encountered
        /// </summary>
        /// <param name="objectType"></param>
        /// <returns></returns>
        T FindMapObjectView<T>(MapObjectintTypes objectType) where T: MapObjectView;

        /// <summary>
        /// Finds a map object in specified sector by type, returns first encountered
        /// </summary>
        /// <param name="sectorType"></param>
        /// <param name="objectType"></param>
        /// <returns></returns>
        IMapObject FindMapObjectOnMapByType(BusinessSectorsTypes sectorType, MapObjectintTypes objectType);
        
        /// <summary>
        /// Finds all map object views in specified sector by type, filters by id
        /// </summary>
        /// <param name="sectorType"></param>
        /// <param name="objectType"></param>
        /// <param name="objectId"></param>
        /// <returns></returns>
        IMapObject FindMapObjectOnMapByType(BusinessSectorsTypes sectorType, MapObjectintTypes objectType, int objectId);

        /// <summary>
        /// Finds all map object in specified sector by type
        /// </summary>
        /// <param name="sectorType"></param>
        /// <param name="objectType"></param>
        /// <returns></returns>
        List<IMapObject> FindAllMapObjectsOnMapByType(BusinessSectorsTypes sectorType, MapObjectintTypes objectType);
        
        /// <summary>
        /// Finds all map object in specified sector by type id
        /// </summary>
        /// <param name="sectorType"></param>
        /// <param name="objectTypeId"></param>
        /// <returns></returns>
        List<IMapObject> FindAllMapObjectsOnMapByType(BusinessSectorsTypes sectorType, Id_IMapObjectType objectTypeId);
        

        List<IMapObject> FindAllMapObjectsOnMapByType(MapObjectintTypes objectType);
        
        /// <summary>
        /// Finds a map object by type in all sectors including global, returns first encountered
        /// </summary>
        /// <param name="objectType"></param>
        /// <returns></returns>
        T FindMapObjectByType<T>(MapObjectintTypes objectType) where T: class, IMapObject;

        /// <summary>
        /// Finds a map object by type id in all sectors including global, returns first encountered
        /// </summary>
        /// <param name="typeId"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        T FindMapObjectByTypeId<T>(Id_IMapObjectType typeId) where T : class, IMapObject;

        /// <summary>
        /// Finds map object by it unique instance id.
        /// </summary>
        /// <param name="buildingId"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        T FindMapObjectById<T>(int buildingId) where T : class, IMapObject;

        List<IMapObject> GetMapObjectsById(int buildingId);

        /// <summary>
        /// Регестрирует вьюшку карты
        /// </summary>
        /// <param name="view"></param>S
        void RegisterMapView(CityMapView view);

        int GetMapObjectsCountInCity(int buildingId);

        /// <summary>
        /// Регестрирует вьюшку глобальной карты
        /// </summary>
        /// <param name="view"></param>
        void RegisterGlobalCityMapView(GlobalCityMapView view);

        /// <summary>
        /// Clears all maps, not global
        /// </summary>
        void ClearMapsView();

        /// <summary>
        /// Clears everything
        /// </summary>
        void ClearAll();

        void DeselectRoads();
        
        void UpdateRoads();

        void RestoreCityMapFromServer();
    }
}

