﻿using GameLogic.BuildingSystem.CityAoe;
using NanoReality.GameLogic.GameManager.Models.api;
using NanoReality.StrangeIoC;

namespace NanoReality.GameLogic.GameManager.Controllers
{
    public class InitCityMapCommand : AGameCommand
    {
        private int _cityMapsToLoad;

        [Inject] public LoadCityMapCommandData CommandInData { get; set; }

        [Inject] public IGameManager jGameManager { get; set; }
        [Inject] public ICityAoeService jCityAoeService { get; set; }
        
        [Inject] public SignalOnFinishLoadCityMapSequence jSignalOnFinishLoadCityMapSequence { get; set; }

        public override void Execute()
        {
            base.Execute();
            
            Retain();

            jGameManager.CurrentUserCity.CityMaps.Clear();
            foreach (var loadedCityMap in CommandInData.LoadedCityMaps)
            {
                loadedCityMap.Value.BusinessSector = loadedCityMap.Key;
                jGameManager.CurrentUserCity.CityMaps.Add(loadedCityMap.Value);
            }
            
            jGameManager.CurrentProfileId = CommandInData.PlayerProfileId;

            _cityMapsToLoad = CommandInData.LoadedCityMaps.Count;

            foreach (var loadedCityMap in CommandInData.LoadedCityMaps)
            {
                loadedCityMap.Value.InitMap(OnCityMapInitFinished);
            }
        }

        private void OnCityMapInitFinished()
        {
            _cityMapsToLoad--;

            if (_cityMapsToLoad > 0)
            {
                return;
            }

            jSignalOnFinishLoadCityMapSequence.Dispatch(CommandInData);
            
            jCityAoeService.Init();
            jCityAoeService.OnCityAoeDataReceived += OnCityAoeDataReceived;
            jCityAoeService.LoadAoeData();
        }

        private void OnCityAoeDataReceived(CityAoeData cityAoeData)
        {
            jCityAoeService.OnCityAoeDataReceived -= OnCityAoeDataReceived;
            Release();
        }
    }
}
