﻿using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.GlobalCityMap.Controllers;
using NanoReality.GameLogic.GameManager.Models.api;
using NanoReality.StrangeIoC;

namespace NanoReality.GameLogic.GameManager.Controllers
{
    public class InitGlobalCityMapCommand : AGameCommand
    {
        [Inject] public LoadCityMapCommandData jCommandInData { get; set; }
        [Inject] public IGameManager jGameManager { get; set; }
        [Inject] public CityMapViewInitializedSignal jCityMapViewInitializedSignal { get; set; }

        public override void Execute()
        {
            base.Execute();
            
            jCityMapViewInitializedSignal.AddOnce(OnCityMapViewInitialized);
            var globalMap = jCommandInData.GlobalCityMap;

            globalMap.Init(jCommandInData.PlayerProfileId, jCommandInData.CityId, BusinessSectorsTypes.Global.GetBusinessSectorId());

            jGameManager.CurrentUserCity.GlobalCityMap = globalMap;
            
            Retain();
        }

        private void OnCityMapViewInitialized(BusinessSectorsTypes obj)
        {
            if (obj != BusinessSectorsTypes.Global)
            {
                return;
            }
            
            Release();  
        }
    }
}
