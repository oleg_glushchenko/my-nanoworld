﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.Quests;
using Newtonsoft.Json;

namespace GameLogic.Bazaar.Model.impl
{
    public class SoukData
    {
        [JsonProperty("bottom_tradeline")]
        public SoukBottomOffer[] BottomTradeItems { get; set; }

        [JsonProperty("top_tradeline")]
        public SoukTopTradeline TopTradeItems { get; set; }

        [JsonProperty("loot_boxes")]
        public BazaarLootBox[] LootBoxes { get; set; }

        [JsonProperty("expire_time")]
        public long ExpireTime;

        [JsonProperty("shuffle_price")]
        public int ShufflePrice;
    }

    public sealed class SoukBottomOffer
    {
        [JsonProperty("id")]
        public int Id;

        [JsonProperty("currency")]
        public int Currency;
        
        [JsonProperty("product_price")]
        public int PriceValue;
        
        [JsonProperty("products")]
        public Dictionary<int, int> Products;

        [JsonProperty("sold_out")]
        public bool SoldOut;

        public bool SingleItemOffer => Products.Count == 1;

        private Price _cachedPrice;
        
        public Price GetPrice()
        {
            if (_cachedPrice == default)
            {
                switch (Currency)
                {
                    case 0:
                        _cachedPrice = new Price() { PriceType = PriceType.Soft, SoftPrice = PriceValue};
                        break;
                    case 1:
                        _cachedPrice = new Price() { PriceType = PriceType.Hard, HardPrice = PriceValue};
                        break;
                    case 2:
                        _cachedPrice = new Price() { PriceType = PriceType.Gold, GoldPrice = PriceValue};
                        break;
                }
            }

            return _cachedPrice;
        }
    }
    
    public class SoukTopTradeline
    {
        [JsonProperty("slots_limit")]
        public int SlotLimit { get; set; }

        [JsonProperty("slots")]
        public SoukTopSlot[] SlotOffers { get; set;}
    }
    
    public class BazaarLootBox
    {
        [JsonProperty("reward")] 
        public BazaarLootBoxReward BoxReward;

        /// <summary>
        /// Reward box rarity type.
        /// </summary>
        [JsonProperty("box_type")] 
        public int Id;
        
        [JsonProperty("price")]
        public int BoxPrice { get; set; }
    }

    public class BazaarLootBoxReward
    {
        [JsonProperty("products")]
        public SoukTopSlot[] Products { get; set;}
        
        [JsonProperty("nano_bucks")]
        public int NanoBucks { get; set; }
        
        [JsonProperty("nano_coins")]
        public int NanoCoins { get; set; }

        public List<IAwardAction> ToAwardActions()
        {
            List<IAwardAction> resultList = new List<IAwardAction>();

            foreach (SoukTopSlot product in Products)
            {
                resultList.Add(new GiveProductAward { Product = product.ProductId, Amount = product.Count, AwardType = AwardActionTypes.Resources});
            }
            

            if (NanoBucks > 0)
            {
                resultList.Add(new PremiumCurrencyAwardActionAction { CurrencyCount = NanoBucks, AwardType = AwardActionTypes.PremiumCurrency });
            }

            if (NanoCoins > 0)
            {
                resultList.Add(new SoftCurrencyAward { SoftCurrencyCount = NanoCoins, AwardType = AwardActionTypes.SoftCurrency });
            }
            
            return resultList;
        }
    }
}