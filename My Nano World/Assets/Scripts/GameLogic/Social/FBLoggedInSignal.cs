using strange.extensions.signal.impl;

namespace NanoReality.GameLogic.SocialCommunicator
{
    public class FBLoggedInSignal : Signal<bool> { }
}
