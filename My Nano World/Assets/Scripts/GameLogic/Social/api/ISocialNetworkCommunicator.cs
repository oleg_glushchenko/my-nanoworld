﻿using System;
using strange.extensions.signal.impl;
using UnityEngine;
using UnityEngine.SocialPlatforms;
using UnityEngine.SocialPlatforms.Impl;

namespace NanoReality.GameLogic.SocialCommunicator.api
{
    /// <summary>
    /// Точка доступа ко всем социальным сетям в игре
    /// </summary>
    public interface ISocialNetworkCommunicator
    {
        Signal<ISocialNetworkCommunicator> OnInitializedSignal { get; }
        
        /// <summary>
        /// Профиль игрока в этой соц. сети
        /// </summary>
        ILocalUser ActiveProfile { get; }

        /// <summary>
        /// Возвращает true, если юзер авторизирован в соц. сети
        /// </summary>
        bool IsUserAuthorized { get; }

        string SocialToken { get; }

        /// <summary>
        /// Возвращает название соц. сети
        /// </summary>
        string SocialNetworkName { get; }

        SocialNetworkName SocialNetworkType { get; }

        void Init();

        /// <summary>
        /// Авторизация в этой соц. сети
        /// </summary>
        /// <param name="callback"></param>
        void Authorize(Action<AuthorizeResult> callback = null);

        /// <summary>
        /// Разлогинивает юзера из соц. сети
        /// </summary>
        /// <param name="callback"></param>
        void Logout(Action<bool, ISocialNetworkCommunicator> callback = null);

        /// <summary>
        /// Репорт ачивки в соц. сеть
        /// </summary>
        /// <param name="achivmentId">айди ачивки</param>
        /// <param name="progress">прогресс</param>
        /// <param name="callback">коллбек после репорта</param>
        void ReportAchivmentProgress(string achivmentId, double progress,
            Action<string, bool, ISocialNetworkCommunicator> callback = null);

        /// <summary>
        /// Публикует сообщение на странице пользователя
        /// </summary>
        /// <param name="message">Текст сообщения</param>
        /// <param name="callback"></param>
        void PostMessage(string message, Action<bool, ISocialNetworkCommunicator> callback = null);

        /// <summary>
        /// Публикует сообщение на странице пользователя
        /// </summary>
        /// <param name="message">Текст сообщения</param>
        /// <param name="texture2D">Изображение в сообщении</param>
        /// <param name="callback"></param>
        void PostMessage(string message, Texture2D texture2D, Action<bool, ISocialNetworkCommunicator> callback = null);
        
        /// <summary>
        /// Публикует сообщение на странице пользователя
        /// </summary>
        /// <param name="message">Текст сообщения</param>
        /// <param name="imgUrl">Ссылка на изображение в сообщении</param>
        /// <param name="callback"></param>
        void PostMessage(string message, string imgUrl, Action<bool, ISocialNetworkCommunicator> callback = null);


        /// <summary>
        /// Загружает список друзей текущего игрока
        /// </summary>
        /// <param name="callback"></param>
        void LoadUserFriends(Action<bool, IUserProfile[], ISocialNetworkCommunicator> callback = null);


        /// <summary>
        /// Вызывает UI для приглашение друзей в игру
        /// </summary>
        /// <param name="inivteMessage">Сообщение которые отправится в приглашении</param>
        /// <param name="callback"></param>
        void InviteFriends(string inivteMessage, Action<bool, ISocialNetworkCommunicator> callback = null);
    }

    public sealed class MockSocialNetworkCommunicator : ISocialNetworkCommunicator
    {
        public Signal<ISocialNetworkCommunicator> OnInitializedSignal { get; }
        public ILocalUser ActiveProfile { get; private set; }
        public bool IsUserAuthorized { get; }
        public string SocialToken { get; }
        public string SocialNetworkName { get; }
        public SocialNetworkName SocialNetworkType { get; }

        public MockSocialNetworkCommunicator()
        {
            OnInitializedSignal = new Signal<ISocialNetworkCommunicator>();
            ActiveProfile = new LocalUser();
        }
        
        public void Init()
        {
            Debug.Log("SocialCommunicator.Init");
        }

        public void Authorize(Action<AuthorizeResult> callback = null)
        {
            Debug.Log("SocialCommunicator.Authorize");
        }

        public void Logout(Action<bool, ISocialNetworkCommunicator> callback = null)
        {
            Debug.Log("SocialCommunicator.Logout");
        }

        public void ReportAchivmentProgress(string achivmentId, double progress, Action<string, bool, ISocialNetworkCommunicator> callback = null)
        {
            Debug.Log("SocialCommunicator.ReportAchivmentProgress");
        }

        public void PostMessage(string message, Action<bool, ISocialNetworkCommunicator> callback = null)
        {
            Debug.Log("SocialCommunicator.PostMessage");
        }

        public void PostMessage(string message, Texture2D texture2D, Action<bool, ISocialNetworkCommunicator> callback = null)
        {
            Debug.Log("SocialCommunicator.PostMessage");
        }

        public void PostMessage(string message, string imgUrl, Action<bool, ISocialNetworkCommunicator> callback = null)
        {
            Debug.Log("SocialCommunicator.PostMessage");
        }

        public void LoadUserFriends(Action<bool, IUserProfile[], ISocialNetworkCommunicator> callback = null)
        {
            Debug.Log("SocialCommunicator.LoadUserFriends");
        }

        public void InviteFriends(string inivteMessage, Action<bool, ISocialNetworkCommunicator> callback = null)
        {
            Debug.Log("SocialCommunicator.InviteFriends");
        }
    }

    public enum SocialNetworkCommunicatorTypes
    {
        Primary,
        Secondary,
        Tertiary
    }

    public enum SocialNetworkName
    {
        Facebook,
        GooglePlay,
        Twitter,
        GameCenter
    }

    /// <summary>
    /// Вызывается после авторизации профиля в соц. сети. 
    /// Первый параметр - состояние авторизации юзером (да/нет)
    /// Второй параметр - авторизованный профиль. 
    /// Третий параметр - коммуникатор соц. сети в которой авторизовался юзер
    /// </summary>
    public class SignalOnAuthorizeSocialProfile : Signal<AuthorizeResult> { }

  
    /// <summary>
    /// Базовый профайл соц. сети
    /// </summary>
    public class BaseUserProfile : ILocalUser
    {

        /// <summary>
        /// Имя юзера
        /// </summary>
        public virtual string userName { get;  set; }

        /// <summary>
        /// Id юзера в соц. сети
        /// </summary>
        public virtual string id { get; set; }

        /// <summary>
        /// Этот пользователь друг, залогинившемуся пользователю
        /// </summary>
        public virtual bool isFriend { get; set; }

        /// <summary>
        /// Состояние юзера в сети
        /// </summary>
        public virtual UserState state { get; set; }

        /// <summary>
        /// Аватарка
        /// </summary>
        public virtual Texture2D image { get;  set; }

        /// <summary>
        /// Пол игрока
        /// </summary>
        public virtual string Gender { get; set; }

        /// <summary>
        /// Год рождения
        /// </summary>
        public virtual int BirthYear { get; set; }

        /// <summary>
        /// Родной город
        /// </summary>
        public virtual string Hometown { get; set; }

        /// <summary>
        /// Авторизация этого пользователя в соц. сети и загрузка его профиля
        /// </summary>
        /// <param name="callback"></param>
        public virtual void Authenticate(Action<bool> callback)
        {
			throw new NotImplementedException("Authenticate");
        }

        public void Authenticate(Action<bool, string> callback)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Загрузить список друзей этого юзера
        /// </summary>
        /// <param name="callback"></param>
        public virtual void LoadFriends(Action<bool> callback)
        {
			throw new NotImplementedException("LoadFriends");
        }

        /// <summary>
        /// Список друзей этого юзера
        /// </summary>
        public virtual IUserProfile[] friends { get; set; }

        /// <summary>
        /// Залогинен ли юзер
        /// </summary>
        public virtual bool authenticated { get; set; }

        /// <summary>
        /// Этот юзер несовершенолетен?
        /// </summary>
        public virtual bool underage { get; set; }
    }


    /// <summary>
    /// Структура для ивента авторизации
    /// </summary>
    public struct AuthorizeResult
    {
        public bool IsAuthorized;
        public ILocalUser AuthorizedUser;
        public ISocialNetworkCommunicator SocialNetworkCommunicator;


        public AuthorizeResult(bool result, ILocalUser authorizedUser, ISocialNetworkCommunicator social)
        {
            IsAuthorized = result;
            AuthorizedUser = authorizedUser;
            SocialNetworkCommunicator = social;
        }

    }

}

