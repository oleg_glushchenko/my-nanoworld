﻿using System;
using Assets.NanoLib.UtilitesEngine.CustomDebugging.Models.api;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.SocialCommunicator.api;
using strange.extensions.signal.impl;
using UnityEngine;
using UnityEngine.SocialPlatforms;

namespace NanoReality.GameLogic.SocialCommunicator.impl
{
    /// <summary>
    /// Врапер для коммуникации с google play сервисами
    /// </summary>
    public class GooglePlaySocialNetworkCommunicator : ISocialNetworkCommunicator
    {
        private const string GooglePlayServicesAuthorizedKey = "GooglePlayServicesAuthorized";
        
        public Signal<ISocialNetworkCommunicator> OnInitializedSignal { get; private set; }

        public string SocialToken { get { return ""; } }
        
        [Inject] public IDebug Debug { get; set; }
        [Inject] public SignalOnAuthorizeSocialProfile jSignalOnAuthorizeSocialProfile { get; set; }
        [Inject] public IPlayerProfile jPlayerProfile { get; set; }

        /// <summary>
        /// Профиль игрока в этой соц. сети
        /// </summary>
        public ILocalUser ActiveProfile
        {
            get
            {
                return Social.Active.localUser;
            } 
        }

        /// <summary>
        /// Возвращает true, если юзер авторизирован в соц. сети
        /// </summary>
        public bool IsUserAuthorized 
        {
            get
            {
                return Social.Active.localUser.authenticated;
            } 
        }

        public string SocialNetworkName { get { return "google_play_services"; } }
        
        public SocialNetworkName SocialNetworkType { get { return api.SocialNetworkName.GooglePlay; } }
        
        public GooglePlaySocialNetworkCommunicator()
        {
            OnInitializedSignal = new Signal<ISocialNetworkCommunicator>();
        }

        public void Init()
        {
            Debug.Log("Initializing Google Play plugin");

            if (PlayerPrefs.HasKey(GooglePlayServicesAuthorizedKey) && 
                jPlayerProfile.SocialAccounts.ContainsKey(SocialNetworkName))
            {
                Social.localUser.Authenticate(successAuthorize =>
                {
                    Debug.Log("Google Play Autologin result: " + successAuthorize);
                    Debug.Log("Google Play ActiveProfile: " + (ActiveProfile == null ? "null" : ActiveProfile.userName), true);
                    OnInitializedSignal.Dispatch(this);
                });
            }
            else
            {
                OnInitializedSignal.Dispatch(this);
            }
        }

        /// <summary>
        /// Авторизация в этой соц. сети
        /// </summary>
        /// <param name="callback"></param>
        public void Authorize(Action<AuthorizeResult> callback = null)
        {
            Social.localUser.Authenticate(result =>
            {
                Debug.Log("Autorize google play result: "+  result);
                if (result)
                {
                    PlayerPrefs.SetInt(GooglePlayServicesAuthorizedKey, 1);
                    PlayerPrefs.Save();
                    Debug.Log("Google Play User: " + ActiveProfile.userName, true);
                }
                var authorizeResult = new AuthorizeResult(result, Social.localUser, this);
                if (callback != null) callback(authorizeResult);
                jSignalOnAuthorizeSocialProfile.Dispatch(authorizeResult);
            });
        }


        /// <summary>
        /// Разлогинивает юзера из соц. сети
        /// </summary>
        /// <param name="callback"></param>
        public void Logout(Action<bool, ISocialNetworkCommunicator> callback = null)
        {
            PlayerPrefs.DeleteKey(GooglePlayServicesAuthorizedKey);
            PlayerPrefs.Save();
            Debug.Log("Google Play logout: " + !IsUserAuthorized, true);

            callback?.Invoke(!IsUserAuthorized, this);
        }

        /// <summary>
        /// Репорт ачивки в соц. сеть
        /// </summary>
        /// <param name="achivmentId">айди ачивки</param>
        /// <param name="progress">прогресс</param>
        /// <param name="callback">коллбек после репорта</param>
        public void ReportAchivmentProgress(string achivmentId, double progress, Action<string,bool,ISocialNetworkCommunicator> callback = null)
        {
            Social.ReportProgress(achivmentId, progress, (result =>
            {
                    callback?.Invoke(achivmentId, result, this);
            }));
        }

        /// <summary>
        /// Публикует сообщение на странице пользователя
        /// </summary>
        /// <param name="message">Текст сообщения</param>
        /// <param name="callback"></param>
        public void PostMessage(string message, Action<bool, ISocialNetworkCommunicator> callback = null)
        {
            if (callback != null) callback(false, this);
            throw new NotImplementedException("Google play services not support posting");
        }

        /// <summary>
        /// Публикует сообщение на странице пользователя
        /// </summary>
        /// <param name="message">Текст сообщения</param>
        /// <param name="texture2D">Изображение в сообщении</param>
        /// <param name="callback"></param>
        public void PostMessage(string message, Texture2D texture2D, Action<bool, ISocialNetworkCommunicator> callback = null)
        {
            if (callback != null) callback(false, this);
            throw new NotImplementedException("Google play services not support posting");
        }

        /// <summary>
        /// Публикует сообщение на странице пользователя
        /// </summary>
        /// <param name="message">Текст сообщения</param>
        /// <param name="imgUrl">Ссылка на изображение в сообщении</param>
        /// <param name="callback"></param>
        public void PostMessage(string message, string imgUrl, Action<bool, ISocialNetworkCommunicator> callback = null)
        {
            if (callback != null) callback(false, this);
            throw new NotImplementedException("Google play services not support posting");
        }

        /// <summary>
        /// Загружает список друзей текущего игрока
        /// </summary>
        /// <param name="callback"></param>
        public void LoadUserFriends(Action<bool, IUserProfile[], ISocialNetworkCommunicator> callback = null)
        {
            throw new NotImplementedException("Google play services not support friends list");
        }


        /// <summary>
        /// Вызывает UI для приглашение друзей в игру
        /// </summary>
        /// <param name="inivteMessage">Сообщение которые отправится в приглашении</param>
        /// <param name="callback"></param>
        public void InviteFriends(string inivteMessage, Action<bool, ISocialNetworkCommunicator> callback = null)
        {
            throw new NotImplementedException("Google play services not support friends invite");
        }
    }
}
