﻿using Assets.NanoLib.Utilities;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;

public interface IProductsToUnlockItem
{
    Id<Product> ProductID { get; set; }

    int Count { get; set; }
}