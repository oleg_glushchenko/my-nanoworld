﻿using UnityEngine;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using SimpleJSON;
using System.Text;
using Assets.Scripts.Extensions;
using NanoReality.GameLogic.Localization.Models.api;

public class MLSUtility  
{
	private static TextAsset[] _allLanguageAssets;

    public class MLSLanguagesContainer
	{
		public List<CultureInfo>	AvailableLanguages;
		
		public List<string>		AvailableLanguagesEnglisName;
		
		
		public MLSLanguagesContainer()
		{
			AvailableLanguages = new List<CultureInfo>();
			AvailableLanguagesEnglisName = new List<string>();
		}
		
		public void ConfigurationDone()
		{
			AvailableLanguagesEnglisName.Sort();
		}
	}
	
	public static MLSLanguagesContainer GetLanguagesContainer()
	{
		var res = new MLSLanguagesContainer();

		CultureInfo en = new CultureInfo("en", false);
		CultureInfo ar = new CultureInfo("ar", false);
		res.AvailableLanguages.Clear();
		res.AvailableLanguages.Add(en);
		res.AvailableLanguages.Add(ar);

		res.ConfigurationDone();
		return res;
	}

}
