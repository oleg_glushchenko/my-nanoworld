﻿using System.Collections.Generic;
using ArabicSupport;
using MultiLanguageSystem;
using TMPro;

namespace Assets.Scripts.MultiLanguageSystem.Logic
{
    public static class TextMeshProExtensions
    {
        public static void SetLocalizedText(this TMP_Text textField, string text)
        {
            // if (LocalizationMLS.Instance.IsRtlLanguage)
            // {
            //     textField.isRightToLeftText = true;
            //     if (!string.IsNullOrEmpty(text))
            //     {
            //         var rtlText = ArabicFixer.Fix(text, true, false);
            //         text = Utility.ReverseText(rtlText);
            //         textField.SetArabicAlignment();
            //     }
            // }
            // else
            // {
            //     textField.isRightToLeftText = false;
            //     textField.SetLatinicAlignment();
            // }
            textField.text = text;
        }

        /// <summary>
        /// Sets localized text by key from LocalizationMLS
        /// </summary>
        /// <param name="textField">text component object</param>
        /// <param name="key">localization key</param>
        public static void Localize(this TMP_Text textField, string key)
        {
            textField.SetLocalizedText(LocalizationMLS.Instance.GetText(key));
        }

        private static readonly Dictionary<TextAlignmentOptions, TextAlignmentOptions> _arabicAlignments =
            new Dictionary<TextAlignmentOptions, TextAlignmentOptions>
            {
                {TextAlignmentOptions.Left, TextAlignmentOptions.Right},
                {TextAlignmentOptions.TopLeft, TextAlignmentOptions.TopRight},
                {TextAlignmentOptions.BottomLeft, TextAlignmentOptions.BottomRight},
                {TextAlignmentOptions.MidlineLeft, TextAlignmentOptions.MidlineRight},
            };

        private static readonly Dictionary<TextAlignmentOptions, TextAlignmentOptions> _latinicAlignments =
            new Dictionary<TextAlignmentOptions, TextAlignmentOptions>
            {
                {TextAlignmentOptions.Right, TextAlignmentOptions.Left},
                {TextAlignmentOptions.TopRight, TextAlignmentOptions.TopLeft},
                {TextAlignmentOptions.BottomRight, TextAlignmentOptions.BottomLeft},
                {TextAlignmentOptions.MidlineRight, TextAlignmentOptions.MidlineLeft}
            };

        public static void SetArabicAlignment(this TMP_Text textField)
        {
            TextAlignmentOptions arabicAlignment;
            if (_arabicAlignments.TryGetValue(textField.alignment, out arabicAlignment))
            {
                textField.alignment = arabicAlignment;
            }
        }

        public static void SetLatinicAlignment(this TMP_Text textField)
        {
            TextAlignmentOptions latinicAlignments;
            if (_latinicAlignments.TryGetValue(textField.alignment, out latinicAlignments))
            {
                textField.alignment = latinicAlignments;
            }
        }

    }
}
