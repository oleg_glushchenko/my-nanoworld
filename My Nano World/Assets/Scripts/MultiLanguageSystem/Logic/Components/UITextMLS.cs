using UnityEngine;
using System.Collections;
using System.Diagnostics;
using MultiLanguageSystem;
using TMPro;
using UnityEngine.UI;
using Debug = UnityEngine.Debug;

[System.Serializable]
public class UITextMLS : MonoBehaviour 
{
	public string LocalizedKey = "";
	public bool KeySetted = false;
    TextMeshProUGUI _text;
	public bool	IsRichText = true;
	public Utility.ETextProperty[] TextProperties;

	void Awake()
	{
		_text = GetComponent<TextMeshProUGUI>();
	}
	
	void Start () 
	{
		if (LocalizationMLS.Instance != null)
		{
			LocalizationMLS.Instance.OnChangedLanguage += OnChangeLanguage;

			if (LocalizationMLS.Instance.IsInited)
			{
				OnChangeLanguage();
			}
		}
	}
	
    void OnDestroy()
    {
		if (!LocalizationMLS.IsQuitting)
		{
			if (LocalizationMLS.Instance != null)
			{
				LocalizationMLS.Instance.OnChangedLanguage -= OnChangeLanguage;
			}
		}
    }

	void OnChangeLanguage()
	{
		if (!LocalizationMLS.IsQuitting)
		{
			if (LocalizationMLS.Instance != null)
			{
				if (LocalizedKey == "")
				{
					Debug.LogWarning(string.Format("No key assigned for {1}/{0}", gameObject.name, transform.parent != null ? transform.parent.gameObject.name : ""));
				}

				Utility.SetValueText(_text, Utility.StringProperty(LocalizationMLS.Instance.GetText(LocalizedKey, IsRichText), TextProperties));
			}
		}
	}
}