﻿using System;
using System.Collections;
using Assets.Scripts.MultiLanguageSystem.Logic;
using TMPro;
using UnityEngine;

namespace MultiLanguageSystem
{
	/// <summary>
	/// help functions
	/// </summary>
	public class Utility
	{
		public enum ETextProperty
		{
			None,
			Upper,
			Lower,
			Trim
		}
		public static string StringProperty( string val, params ETextProperty[] textProperties )
		{
			if( textProperties != null )
			{
				foreach( var textProterty in textProperties )
				{
					switch( textProterty )
					{
						case ETextProperty.Upper:
							val = val.ToUpper();
							break;
						case ETextProperty.Lower:
							val = val.ToLower();
							break;
						case ETextProperty.Trim:
							val = val.Trim();
							break;
					}
				}
			}
			return val;
		}
		public static void SetValueText( UnityEngine.UI.Text obj, object val )
		{
			if( obj != null )
			{
				if( val != null )
				{
					obj.text = val.ToString();
				}
				else
				{
					obj.text = "";
				}
			}
			else
			{
				Debug.LogWarning( "Utility::SetValueText obj - NULL\n"+Environment.StackTrace );
			}
		}
		public static void SetValueText(TextMeshProUGUI obj, object val)
		{
			if(obj != null)
			{
				if(val != null)
				{
					obj.SetLocalizedText(val.ToString());
				}
				else
				{
					obj.text = "";
				}
			}
			else
			{
				Debug.LogWarning("Utility::SetValueText obj - NULL\n" + Environment.StackTrace);
			}
		}
		public static void SetValueImage( UnityEngine.UI.RawImage obj, Texture val )
		{
			if( obj != null )
			{
				obj.texture = val;
			}
			else
			{
				Debug.LogWarning( "Utility::SetValueImage obj - NULL\n"+Environment.StackTrace );
			}
		}
		public class TimeValuesString
		{
			public TimeValuesString() {}
			public string None = "None";
			public string D = "d";
			public string H = "h";
			public string M = "m";
			public string S = "s";
			public string MS = "ms";
			public string Delimeter = ".";
		}
		public static string CovertTimeFromSecond( double time, bool useMS = false, TimeValuesString stringval = null )
		{
			if (stringval == null)
			{
				stringval = new TimeValuesString();
			}
			if( time == 0 )
			{
				return stringval.None;
			}

			string resultString = "";

			TimeSpan timeSpan = TimeSpan.FromSeconds( time );
			int days = timeSpan.Days;
			int hours = timeSpan.Hours;
			int minutes = timeSpan.Minutes;
			int seconds = timeSpan.Seconds;
			int ms = timeSpan.Milliseconds;
			if( days > 0 )
			{
				resultString = days + stringval.D + " ";
			}
			if( hours > 0 )
			{
				resultString += hours + stringval.H + " ";
			}
			if( minutes > 0 )
			{
				resultString += minutes + stringval.M + " ";
			}
			if( seconds > 0 )
			{
				resultString += seconds + stringval.S;
			}
			if (useMS)
			{
				if (ms > 0)
				{
					resultString += stringval.Delimeter + ms + stringval.MS;
				}
			}
			return resultString;
		}
	
		public static string ReverseText(string source)
		{
			var output = new char[source.Length];
			for (var i = 0; i < source.Length; i++)
			{
				output[output.Length - 1 - i] = source[i];
			}
			return new string(output);
		}
	}
	public static class AnimationExtensions
	{
		public static IEnumerator WhilePlaying( this Animation animation, System.Action calback )
		{
			do
			{
				yield return null;
			} 
			while ( animation.isPlaying );
			if (calback != null)
			{
				calback();
			}
		}
	}
}