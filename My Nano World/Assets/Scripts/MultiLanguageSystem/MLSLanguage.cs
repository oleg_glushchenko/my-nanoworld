﻿using UnityEngine;
using System.Collections;
using System.Globalization;
using System.Collections.Generic;
using NanoReality.GameLogic.Localization.Models.api;
using SimpleJSON;

[System.Serializable]
public class MLSLanguage
{
	public void CultureUpdate()
	{
		LanguageCulture = CultureInfo.GetCultureInfo(LngName);
	}

	public string LngName;
    public int LngVersion;
	public CultureInfo LanguageCulture;
	public List<MLSObject> LanguageObjects;
	
	public MLSLanguage( JSONClass jClass )
	{
		LngName = jClass["lng"];
	    LngVersion = jClass["ver"].AsInt;
		LanguageCulture = CultureInfo.GetCultureInfo(LngName);
		LanguageObjects = new List<MLSObject>();

		foreach (var obj in jClass["objs"].AsArray)
		{
			LanguageObjects.Add((MLSObject)obj);
		}
	}

	public MLSLanguage(ILanguageData data)
	{
		LngName = data.LanguageName;
	    LngVersion = data.LanguageVersion;
        LanguageCulture = CultureInfo.GetCultureInfo(LngName);
		LanguageObjects = new List<MLSObject>();

		foreach (var obj in data.LanguageObjects)
		{
			LanguageObjects.Add(new MLSObject((EMLSObjectType)(obj.Type), obj.Key, obj.Value));
		}
	}
	
	public MLSLanguage( string languageName )
	{
		LngName = languageName.Trim();
		LanguageCulture = CultureInfo.GetCultureInfo(LngName);
		LanguageObjects = new List<MLSObject>();
	}
	
	public void Add( EMLSObjectType type, string key, string val)
	{
		LanguageObjects.Add(new MLSObject(type, key, val));
	}
	
	public List<string> KeysList = new List<string>();
	public  List<string> GenerateListOfKeys()
	{
		KeysList.Clear();
		foreach (var o in LanguageObjects)
		{
			KeysList.Add(o.Key);
		}
		return KeysList;
	}
}