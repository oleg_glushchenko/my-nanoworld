﻿using MultiLanguageSystem;

public partial class LocalizationKeys 
{
	public const string StartTurnMessage = "START_TURN";
	public const string BestWordLabel = "BEST_WORD";
	public const string MainScreenFun = "MAIN_SCREEN_FUN";


	const string TimeKey = "TIME_";
	public static Utility.TimeValuesString GetTimeLocalizeClass()
	{
		Utility.TimeValuesString sclass = new Utility.TimeValuesString();
		
		sclass.None = LocalizationMLS.Instance.GetText(TimeKey+"NONE");
		sclass.D = LocalizationMLS.Instance.GetText(TimeKey+"D");
		sclass.H = LocalizationMLS.Instance.GetText(TimeKey+"H");
		sclass.M = LocalizationMLS.Instance.GetText(TimeKey+"M");
		sclass.S = LocalizationMLS.Instance.GetText(TimeKey+"S");
		sclass.MS = LocalizationMLS.Instance.GetText(TimeKey+"MS");
		sclass.Delimeter = LocalizationMLS.Instance.GetText(TimeKey+"DELIMETER");
		
		return sclass;
	}
	
	public const string WORLDS_RES			= "WORLDS_RES";
	public const string DISPLAY_HINT		= "DISPLAY_HINT";
}