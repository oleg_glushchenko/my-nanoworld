﻿using System;
using System.Collections;
using System.Globalization;
using NanoLib.Services.InputService;
using Assets.NanoLib.Utilities.DataManager.Models.api;
using Assets.Scripts.Engine.UI.Views.Popups;
using NanoLib.Core.Logging;
using NanoLib.Core.Logging.Settings;
using NanoLib.SoundManager;
using NanoLib.UI.Core;
using NanoReality.Core.Resources;
using NanoReality.Engine.BuildingSystem.MapObjects.ConstractAnimations;
using NanoReality.Engine.Settings.BuildSettings;
using NanoReality.Engine.UI;
using strange.extensions.context.api;
using strange.extensions.context.impl;
using NanoReality.GameLogic.BuildingSystem.CityCamera;
using NanoReality.GameLogic.Services;
using NanoReality.GameLogic.StartApplicationSequence.Controllers.Signals;
using NanoReality.Game.Tutorial;
using UnityEngine;

namespace NanoReality
{
    /// <summary>
    /// Входная точка приложения, здесь создается контекст и запускается приложение
    /// </summary>
    public class NanoRealityGameRoot : ContextView
    {
        [SerializeField] private NetworkSettings _networkSettings;
        [SerializeField] private GameCameraSettings gameCameraSettings;
        [SerializeField] private TutorialFlowSettings _tutorialFlowSettings;
        [SerializeField] private SoundSettings _soundSettings;
        
        public AppsFlyerObjectScript AppsFlyerObjectScript;
        public MouseInputController MouseInputController;
        public TouchInputController TouchInputController;

        public WorldSpaceCanvas WorldSpaceCanvas;
        public UIManager UIManagerObject;
        public ConstractAnimationsContainer ConstractAnimationsContainer;
        public PopupsManager PopupManager;
        public IconProvider IconProvider;

        private NanoRealityGameContext _gameContext;

        private UpdateTickSignal _updateTickSignal;

        private DateTime _onApplicationPauseTime;

        #region Overrides of ContextView

        protected virtual void Awake()
        {
            InitializeLogger();
            
			// This strange block of code fix application crash on devices with arabic system language.
            var localeIdentifier = CultureInfo.InstalledUICulture;
            if (localeIdentifier.Name == "ar_SA")
            {
                new UmAlQuraCalendar();
            }

            _onApplicationPauseTime = DateTime.Now;
            
            //Instantiate the context, passing it this instance.
            _gameContext = new NanoRealityGameContext(this, ContextStartupFlags.MANUAL_MAPPING)
            {
                BuildSettings = ScriptableObjectsManager.Get<BuildSettings>().ConvertToBuildSettings(),
                NetworkSettings = _networkSettings,
                MapObjectViewSettings = ScriptableObjectsManager.Get<GlobalMapObjectViewSettings>(),
                WorldSpaceCanvasPrefab =  WorldSpaceCanvas,
                ConstractAnimationsContainer =  ConstractAnimationsContainer,
                UIManagerObject = UIManagerObject,
                PopupManager =  PopupManager,
                IconProvider = IconProvider,
                CityMapSettings = ScriptableObjectsManager.Get<CityMapSettings>(),
                GameCameraSettings = gameCameraSettings,
                TutorialFlowSettings = _tutorialFlowSettings,
                SoundSettings = _soundSettings,
                AppsFlyerScriptableObject = AppsFlyerObjectScript
            };
            context = _gameContext;

#if UNITY_EDITOR || UNITY_STANDALONE
           TouchInputController.enabled = false;
           _gameContext.InputController = MouseInputController;
#elif UNITY_ANDROID || UNITY_IOS
            MouseInputController.enabled = false;
            _gameContext.InputController = TouchInputController;
#endif
            
            context.Start();
        }

        private IEnumerator Start()
        {
            //LogClientInfo();
            yield return new WaitForEndOfFrame();
            _gameContext.StartApplication();

            _updateTickSignal = _gameContext.injectionBinder.GetInstance<UpdateTickSignal>();
        }

        #endregion

        private void Update()
        {
            if (_updateTickSignal != null)
            {
                _updateTickSignal.Dispatch();
            }
        }

        private void OnApplicationPause(bool pauseStatus)
        {
            if (pauseStatus)
            {
                _onApplicationPauseTime = DateTime.Now;

                SaveLocalData();
            }
            else
            {
                var elapsedTime = DateTime.Now - _onApplicationPauseTime;

                var pauseTimeout = _gameContext.BuildSettings.ApplicationPauseTimeout;
                $"Application pause time: {elapsedTime.TotalSeconds}. Pause timeout: {pauseTimeout}".Log(LoggingChannel.Core);

                if (Math.Abs(elapsedTime.TotalSeconds) >= pauseTimeout)
                {
                    $"Application was in pause for over {pauseTimeout} seconds, restarting the game...".Log(LoggingChannel.Core);
                    _gameContext.injectionBinder.GetInstance<SignalReloadApplication>().Dispatch();
                }
            }
        }

        private void OnApplicationQuit()
        {
            _gameContext.SignalOnApplicationQuit.Dispatch();

            SaveLocalData();
            
            AssetBundleManager.Reset();
            ScriptableObjectsManager.UnloadAll();
            Resources.UnloadUnusedAssets();
            AtlasManager.Reset();
        }

        private void SaveLocalData()
        {
            if (_gameContext != null && _gameContext.injectionBinder != null)
            {
                var localDataManager = _gameContext.injectionBinder.GetInstance<ILocalDataManager>();
                if (localDataManager != null)
                {
                    localDataManager.SaveData();
                }
            }
        }
        
        private static void InitializeLogger()
        {
#if ENABLE_LOG
            Debug.unityLogger.logEnabled = true;

            var settings = ScriptableObjectsManager.Get<LoggingSettings>();
            Logging.Initialize(settings.Channels);

            Logging.Log(LoggingChannel.Core,$"Initialized logs channels: {Logging.ActiveChannels}");
#else
            Debug.unityLogger.logEnabled = false;
#endif
        }
    }
}
