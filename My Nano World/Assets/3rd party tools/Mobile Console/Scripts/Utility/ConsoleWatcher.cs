﻿using UnityEngine;

namespace MobileConsole
{
    public class ConsoleWatcher : MonoBehaviour
    {
        private static ConsoleWatcher _instance;
        [SerializeField] private GameObject _mobileConsoleLink;

        private void Awake()
        {
            if (_instance == null)
            {
                _instance = this;
                Instantiate(_mobileConsoleLink, Vector3.zero, Quaternion.identity);
            }
            else if (_instance != this)
            {
                Destroy(gameObject);
            }

            DontDestroyOnLoad(gameObject);
        }
    }
}